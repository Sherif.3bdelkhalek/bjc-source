# Yesha`yah (Ésaïe) (Es.)

Signification : Yah a sauvé

Auteur : Yesha`yah (Ésaïe)

Thème : Le Mashiah d'Israël

Date de rédaction : 8ème siècle av. J.-C.

Prophète en Israël, Yesha`yah fut une figure marquante en raison du contenu et de l'impact de son message. Véritable porte-parole d'Elohîm, il parla de la ruine morale d'Israël, de la déportation à Babel (Babylone) et des jugements d'Elohîm sur son peuple. Il prophétisa également sur le retour de l'exil, la restauration finale et la reconstruction de Yeroushalaim (Jérusalem). Plus qu'aucun autre livre, les écrits de Yesha`yah annoncent clairement la naissance du Mashiah (Christ), son service, sa mission rédemptrice, son sacrifice et son futur règne millénaire.

L'autorité et l'exactitude de ses prophéties ont été une source d'édification au fil des siècles.

## Chapitre 1

### Prophéties concernant Yéhouda (Juda)

1:1	La vision de Yesha`yah, fils d'Amots, qu'il a vue sur Yéhouda et Yeroushalaim, au jour d'Ouzyah, de Yotham, d'Achaz, et de Yehizqiyah, rois de Yéhouda.
1:2	Cieux, écoutez ! Et toi, Terre, prête l'oreille ! Car YHWH parle. J'ai nourri des enfants, je les ai élevés, mais ils se sont rebellés contre moi.
1:3	Le bœuf connaît son possesseur, et l'âne la crèche de son maître. Israël n'a pas de connaissance, mon peuple n'a pas d'intelligence.
1:4	Ah ! Nation pécheresse, peuple lourd d'iniquités, postérité de méchants, enfants qui ne font que se corrompre ! Ils ont abandonné YHWH, ils ont irrité par leur mépris le Saint<!--Voir commentaire en Ac. 3:14.--> d'Israël, ils se sont retirés en arrière.
1:5	Pourquoi serez-vous encore frappés ? Vous ajouterez l'apostasie ! La tête entière est malade, et tout le cœur est languissant.
1:6	Depuis la plante du pied jusqu'à la tête, il n'y a en lui rien de sain : blessures, meurtrissures et plaies fraîches n'ont été ni pansées, ni bandées, ni adoucies par l'huile.
1:7	Votre terre n'est que désolation, vos villes sont brûlées par le feu, des étrangers dévorent votre sol sous vos yeux, c'est la désolation comme une destruction d'étrangers.
1:8	La fille de Sion est restée comme une cabane dans une vigne, comme une cabane dans un champ de concombres, comme une ville assiégée.
1:9	Si YHWH Tsevaot ne nous avait laissé un petit reste, nous serions devenus comme Sodome, nous ressemblerions à Gomorrhe<!--Ro. 9:29.-->.
1:10	Écoutez la parole de YHWH, chefs de Sodome, prêtez l'oreille à la torah de notre Elohîm, peuple de Gomorrhe !
1:11	Qu'ai-je à faire de la multitude de vos sacrifices ? dit YHWH. Je suis rassasié des holocaustes de béliers et de la graisse des veaux, je ne prends pas plaisir au sang des taureaux, ni des agneaux, ni des boucs<!--1 S. 15:22 ; Os. 8:13 ; Mt. 9:13.-->.
1:12	Quand vous venez pour être vus de mes faces, qui a demandé de votre main ceci : piétiner mes parvis ?
1:13	Ne continuez pas à faire venir des offrandes vaines. L'encens est une abomination pour moi, quant aux nouvelles lunes, aux shabbats et aux convocations des assemblées, je ne supporte pas la méchanceté avec les assemblées solennelles.
1:14	Mon âme hait vos nouvelles lunes et vos fêtes solennelles : elles sont un fardeau pour moi, je suis las de les supporter.
1:15	Quand vous étendez vos paumes, je me cache les yeux. Quand vous multipliez vos prières, je n’écoute pas : vos mains sont pleines de sang<!--Es. 59:1-3 ; Mi. 3:4.-->.
1:16	Lavez-vous, purifiez-vous, ôtez de devant mes yeux la méchanceté de vos actions, cessez de faire le mal !
1:17	Apprenez à faire le bien, recherchez la justice, redressez l'oppresseur, défendez l'orphelin, combattez pour la veuve !

### Invitation à l'obéissance ; restauration de la justice de YHWH

1:18	Venez maintenant, dit YHWH, et débattons nos droits. Si vos péchés sont comme le cramoisi, ils deviendront blancs comme la neige. S'ils sont rouges comme l'écarlate, ils deviendront comme la laine.
1:19	Si vous consentez et écoutez, vous mangerez les bonnes choses de la terre.
1:20	Mais si vous refusez et si vous êtes rebelles, vous serez dévorés par l'épée, car la bouche de YHWH a parlé.
1:21	Comment la cité fidèle est-elle devenue une prostituée ? Elle était pleine de droiture et la justice y habitait, mais maintenant elle est pleine de meurtriers !
1:22	Ton argent s'est changé en scories, ta boisson est coupée d'eau.
1:23	Les chefs de ton peuple sont rebelles et compagnons des voleurs. Tous aiment les pots-de-vin<!--Les présents.-->, ils courent après les récompenses. Ils ne défendent pas l'orphelin et la cause de la veuve ne vient pas jusqu'à eux.
1:24	C'est pourquoi, – déclaration du Seigneur, de YHWH Tsevaot, le Puissant d'Israël – ah ! Je me consolerai de mes adversaires, et je me vengerai de mes ennemis.
1:25	Je retournerai ma main contre toi : je fondrai tes scories comme avec de la lessive, et j'écarterai tout ton étain.
1:26	Je ferai retourner tes juges comme la première fois, et tes conseillers comme au commencement<!--Dans le royaume messianique, le gouvernement théocratique sera restauré et la fonction des juges sera rétablie (voir le livre des Shoftim (Juges) ; Mt. 19:28 ; 1 Co. 6:2-3).-->. Après cela, on t'appellera cité de la justice, ville fidèle.
1:27	Sion sera rachetée par le jugement, et ceux qui y retourneront, par la justice.
1:28	Mais les rebelles et les pécheurs seront détruits ensemble, et ceux qui abandonnent YHWH seront consumés.
1:29	Car on sera honteux à cause des térébinthes que vous avez désirés, et vous rougirez à cause des jardins que vous avez choisis<!--Des cultes idolâtres avaient lieu autour des térébinthes et dans des jardins (De. 16:21 ; Es. 57:4-5, 65:3 ; Jé. 2:20 ; Ez. 20:28 ; Os. 4:13).-->.
1:30	Car vous serez comme le térébinthe dont le feuillage tombe, et comme un jardin qui n'a pas d'eau.
1:31	Le fort deviendra de l'étoupe, son œuvre une étincelle : tous les deux brûleront ensemble, et il n'y aura personne pour éteindre.

## Chapitre 2

### Vision du règne messianique

2:1	Paroles de Yesha`yah, fils d'Amots, ce qu'il a vu au sujet de Yéhouda et de Yeroushalaim.
2:2	Il arrivera dans les derniers jours<!--Voir Ge. 49:1-2.-->, que la montagne de la maison de YHWH sera établie au sommet des montagnes. Elle s'élèvera au-dessus des collines et toutes les nations y afflueront.
2:3	Et beaucoup de peuples iront et diront : Venez et montons à la montagne de YHWH, à la maison de l'Elohîm de Yaacov ! Il nous enseignera ses voies, et nous marcherons dans ses sentiers. Car la torah sortira de Sion, et la parole de YHWH sortira de Yeroushalaim.
2:4	Il jugera au milieu de beaucoup de peuples, il réprimandera des nations puissantes et lointaines. De leurs épées elles forgeront des hoyaux, de leurs lances des serpes : une nation ne lèvera plus l'épée contre une nation, on n'apprendra plus la guerre.
2:5	Venez, maison de Yaacov, et marchons dans la lumière de YHWH.

### L'orgueilleux abaissé au jour de YHWH

2:6	Car tu as rejeté ton peuple, la maison de Yaacov, car ils se sont remplis d'orient en pratiquant le spiritisme comme les Philistins, et ils applaudissent les enfants des étrangers<!--De. 18:8-13 ; Os. 13:2 ; Mi. 5:11-13.-->.
2:7	Sa terre est remplie d'argent et d'or, et il n'y a pas de fin à ses trésors. Sa terre est remplie de chevaux, et il n'y a pas de fin à ses chars.
2:8	Sa terre est remplie de faux elohîm : ils se prosternent devant l'ouvrage de leurs mains et devant ce que leurs doigts ont fabriqué.
2:9	Et l'être humain est courbé, l'homme est humilié, ne leur pardonne pas !
2:10	Entre dans les rochers et cache-toi dans la poussière, à cause de la frayeur de YHWH, et à cause de la gloire de sa majesté<!--Ap. 6:15-16.-->.
2:11	Les yeux hautains des humains seront abaissés et les hommes qui s'élèvent seront humiliés, YHWH sera seul haut élevé en ce jour-là.
2:12	Car il y a un jour pour YHWH Tsevaot contre tout ce qui est orgueilleux et hautain, contre tout ce qui s'élève, pour l'abaisser,
2:13	contre tous les cèdres du Liban, hauts et élevés, et contre tous les chênes de Bashân,
2:14	contre toutes les hautes montagnes, et contre toutes les collines élevées,
2:15	contre toutes les hautes tours, et contre toutes les murailles fortes,
2:16	contre tous les navires de Tarsis, et contre toutes les peintures de plaisance.
2:17	Et l'arrogance des humains sera humiliée, et les hommes qui s'élèvent seront abaissés :
2:18	YHWH seul sera élevé en ce jour-là. Quant aux faux elohîm, ils tomberont tous.
2:19	Et l'on entrera dans les cavernes des rochers et dans les trous de la Terre<!--Lu. 23:30 ; Ap. 6:14-17.-->, à cause de la frayeur de YHWH et à cause de sa gloire magnifique, quand il se lèvera pour faire trembler la Terre.
2:20	En ce jour-là, les humains jetteront aux taupes et aux chauves-souris leurs faux elohîm d'argent et leurs faux elohîm d'or, qu'ils s'étaient faits pour se prosterner devant eux.
2:21	Ils entreront dans les crevasses<!--Ex. 33:22.--> des rochers et dans les creux des rochers, à cause de la frayeur de YHWH, et à cause de sa gloire magnifique, quand il se lèvera pour faire trembler la Terre.
2:22	Cessez d'être avec l'être humain, dans les narines duquel il n'y a qu'un souffle, car en quoi compte-t-il ?

## Chapitre 3

### Le péché, cause de dissolution nationale

3:1	Car voici, le Seigneur, YHWH Tsevaot, va ôter de Yeroushalaim et de Yéhouda tout appui et toute ressource, toute ressource de pain et toute ressource d'eau,
3:2	l'homme vaillant et l'homme de guerre, le juge et le prophète, le devin et l'ancien,
3:3	le chef de cinquante au visage exalté, le conseiller, le sage en arts magiques ayant du discernement et du charme.
3:4	Je leur donnerai de jeunes hommes pour chefs, et des dévergondés domineront sur eux.
3:5	Le peuple sera opprimé : homme contre homme, homme contre son compagnon. Le jeune homme se comportera avec arrogance envers le vieillard, et l'homme de rien contre l'honorable.
3:6	Oui, un homme saisira son frère dans la maison de son père : Tu as un manteau, sois notre chef ! Et prends en main ces ruines !
3:7	Ce jour même il répondra : Je ne suis pas médecin, et dans ma maison il n'y a ni pain ni manteau. Ne m'établissez pas chef du peuple.
3:8	Oui, Yeroushalaim a trébuché, et Yéhouda est tombée. Oui, leurs langues et leurs actions sont contre YHWH, pour se rebeller aux yeux de sa gloire.
3:9	L'aspect de leur visage témoigne contre eux, ils publient leur péché comme Sodome, ils ne le cachent pas. Malheur à leur âme, car ils ont attiré le mal sur eux !
3:10	Dites au juste qu’il sera heureux, car il mangera le fruit de ses œuvres.
3:11	Malheur au méchant, misère ! car il sera traité selon le produit de ses mains.
3:12	Quant à mon peuple, il a pour oppresseur des enfants, et des femmes dominent sur lui. Mon peuple, ceux qui te conduisent t'égarent, ils corrompent le chemin dans lequel tu marches.
3:13	YHWH se présente pour plaider, il se tient debout pour juger les peuples.
3:14	YHWH entre en jugement avec les anciens de son peuple et avec ses chefs : Vous avez brûlé la vigne ! Ce que vous avez volé au pauvre est dans vos maisons !
3:15	Pourquoi écrasez-vous mon peuple et broyez-vous les faces des pauvres ? – déclaration d'Adonaï YHWH Tsevaot.

### Les filles hautaines de Sion

3:16	YHWH dit aussi : Parce que les filles de Sion sont hautaines, et qu'elles marchent le cou tendu et les yeux pleins de convoitise, parce qu'elles marchent avec une fière démarche faisant du bruit avec leurs pieds,
3:17	Adonaï rendra chauve le sommet de la tête des filles de Sion, YHWH découvrira leur nudité.
3:18	En ce temps-là, Adonaï ôtera l'ornement des anneaux de cheville, et les filets et les croissants,
3:19	les pendants d'oreilles, les bracelets et les voiles,
3:20	les parures de la tête, les chaînettes de chevilles et les ceintures, les maisons de l’âme et les amulettes,
3:21	les anneaux et les bagues qui leur pendent sur le nez,
3:22	les robes de fête et les larges tuniques, les manteaux et les sacs,
3:23	les miroirs et les chemises fines, les turbans et les voiles.
3:24	Et il arrivera qu'au lieu du parfum, il y aura de la pourriture ; au lieu de ceintures, des cordes ; au lieu d'une chevelure arrangée avec art, la calvitie ; au lieu d'un vêtement riche, une ceinture de sac ; au lieu de la beauté, le stigmate.
3:25	Tes hommes tomberont par l'épée et ta force par la guerre.
3:26	Ses portes gémiront et mèneront deuil. Vidée, elle s'assiéra par terre.

## Chapitre 4

### Vision du règne messianique<!--Es. 11:1-16.-->

4:1	Ce jour-là, sept femmes saisiront un seul homme, et diront : Nous mangerons notre pain, et nous nous vêtirons de nos habits, seulement que nous soyons appelées de ton nom ! Enlève notre insulte !
4:2	En ce jour-là, le germe de YHWH<!--Yéhoshoua (Jésus) est le « germe » de YHWH (Es. 4:2) et le germe de David (Jé. 23:5 ; Za. 3:8, 6:12). Ce germe a été placé par la vertu du Saint-Esprit dans le sein d'une vierge (Es. 7:14 ; Lu. 1:34-35) et l'enfant qui naquit d'elle fut appelé « Fils d'Elohîm » tout en étant le El Shaddaï (El Tout-Puissant). Il existe de toute éternité en forme d'Elohîm (Jn. 1:1 ; Es. 9:5), mais il a été fait chair pour nous sauver (Jn. 1:14 ; 1 Ti. 3:16). C'est le plus grand des miracles et la démonstration de sa divinité, de sa sagesse et de son amour envers les humains.--> sera plein de noblesse et de gloire, et le fruit de la Terre plein de grandeur et d'excellence pour les rescapés d'Israël.
4:3	Il arrivera que ceux qui resteront à Sion, ceux qui seront laissés à Yeroushalaim, seront appelés saints, ceux de Yeroushalaim seront inscrits parmi les vivants<!--Es. 10:20-22 ; Ro. 9:27, 11:5.-->.
4:4	Quand Adonaï aura lavé les excréments des filles de Sion et purifié Yeroushalaim des sangs qui sont au milieu d'elle, par l'Esprit de jugement et par l'Esprit qui consume,
4:5	YHWH créera, sur toute l'étendue du Mont Sion et sur ses assemblées, une nuée avec une fumée pendant le jour, et une splendeur de feu flamboyant pendant la nuit, car la gloire se répandra partout.
4:6	Elle deviendra un tabernacle pour ombrage, de jour, contre la chaleur, un lieu de refuge et d'abri contre la tempête et la pluie<!--Ap. 21:3.-->.

## Chapitre 5

### Israël, vigne de YHWH

5:1	Je chanterai maintenant pour mon bien-aimé le cantique de mon bien-aimé sur sa vigne. La vigne de mon bien-aimé était devenue le fils de la corne fertile<!--2 S. 22:3 ; Lu. 1:69.-->.
5:2	Il l'environna d'une haie, en ôta les pierres, et y planta des vignes de choix. Il bâtit une tour au milieu d'elle, et il y creusa aussi une cuve. Il espérait qu'elle produirait des raisins, mais elle produisit des grappes sauvages<!--Lu. 13:6-9.-->.
5:3	Maintenant, habitants de Yeroushalaim et hommes de Yéhouda, jugez, je vous prie, entre moi et ma vigne.
5:4	Qu'y avait-il encore à faire à ma vigne que je ne lui aie fait ? Pourquoi, quand j'ai espéré qu'elle produirait des raisins, a-t-elle produit des grappes sauvages ?
5:5	Maintenant je vous dirai ce que je vais faire à ma vigne : j'enlèverai sa haie pour qu'elle soit brûlée, je ferai une brèche dans sa clôture pour qu'elle soit piétinée.
5:6	J’en ferai une ruine, elle ne sera plus taillée, ni cultivée : les ronces et les épines y croîtront et je donnerai mes ordres aux nuages afin qu'ils ne laissent plus tomber de pluie sur elle.
5:7	Oui, la vigne de YHWH Tsevaot, c'est la maison d'Israël, et les hommes de Yéhouda, la plante de ses délices. Il avait espéré de la droiture et voici l’effusion de sang ! De la justice, et voici des cris de détresse !

### Six malheurs en punition de l'infidélité d'Israël

5:8	Malheur à ceux qui ajoutent maison à maison, et qui joignent champ à champ, jusqu'à ce qu'il n'y ait plus d'espace et qu'ils habitent seuls au milieu de la terre.
5:9	YHWH Tsevaot a dit à mes oreilles : En vérité, beaucoup de maisons, quoique grandes et belles, deviendront un objet d'épouvante, sans habitants.
5:10	Car dix arpents de vigne ne produiront qu'un bath, et un omer de semence ne produira qu'un épha.
5:11	Malheur à ceux qui se lèvent tôt le matin pour courir après les boissons fortes, qui s’attardent au crépuscule, enflammés par le vin !
5:12	La harpe et le luth, le tambourin, la flûte et le vin sont dans leurs festins, mais ils ne regardent pas l'œuvre de YHWH et ils ne voient pas le travail de ses mains.
5:13	C'est pourquoi mon peuple sera emmené captif, parce qu'il n'a pas de connaissance<!--2 R. 24:14-16 ; Os. 4:6.-->. Sa gloire, ce sont des hommes affamés, et sa multitude sera desséchée par la soif.
5:14	C'est pourquoi le shéol élargit son âme et ouvre sa bouche sans limite. Sa splendeur y descend, ainsi que sa foule, son vacarme et celui qui exulte.
5:15	Les humains seront abattus, les hommes seront humiliés, et les yeux des hautains seront humiliés.
5:16	Et YHWH Tsevaot sera élevé par le jugement, et le El saint sera sanctifié par la justice.
5:17	Les agneaux paîtront comme dans leurs pâturages, et les étrangers dévoreront les désolations des hommes gras.
5:18	Malheur à ceux qui tirent l'iniquité avec des cordes de vanité, et le péché avec les traits d'un chariot,
5:19	et qui disent : Qu'il se hâte, qu'il fasse vite son œuvre, afin que nous la voyions ! Que le conseil du Saint d'Israël s'avance et vienne, afin que nous le connaissions !
5:20	Malheur à ceux qui disent que le mal est bien et que le bien est mal<!--Mi. 7:2.-->, qui font de la ténèbre une lumière et de la lumière une ténèbre, qui font de l'amertume une douceur et de la douceur une amertume !
5:21	Malheur à ceux qui sont sages à leurs propres yeux et qui, en face d'eux-mêmes, se croient intelligents !
5:22	Malheur à ceux qui sont forts pour boire le vin et vaillants pour mêler des boissons fortes,
5:23	qui justifient le méchant pour des pots-de-vin et qui détournent les justes de la justice !
5:24	C'est pourquoi, comme une langue de feu dévore le chaume et que l'herbe sèche tombe dans la flamme, leur racine sera comme la pourriture, et leur fleur sera détruite comme la poussière, parce qu'ils ont rejeté la torah de YHWH Tsevaot, et ils ont méprisé la parole du Saint d'Israël.
5:25	C'est pourquoi la colère de YHWH s'enflamme contre son peuple, il étend sa main sur lui, et il le frappe. Les montagnes tremblent, et leurs cadavres ont été mis en pièces au milieu des rues. Malgré tout cela, sa colère ne se détourne pas, mais sa main est encore étendue.
5:26	Il élève une bannière pour les nations éloignées, il les siffle des extrémités de la Terre : les voici qui se hâtent et arrivent très vite.
5:27	Personne n'est épuisé, personne ne chancelle, personne ne sommeille ni ne dort. La ceinture de leurs reins ne sera pas déliée et la courroie de leurs sandales ne sera pas rompue.
5:28	Leurs flèches sont aiguisées et tous leurs arcs tendus. Les sabots de leurs chevaux ressemblent à des cailloux, et leurs roues à un vent d'orage.
5:29	Leur rugissement est comme celui d'une lionne, ils rugissent comme des lionceaux : ils grondent et saisissent la proie, ils l'emportent et personne ne vient à son secours.
5:30	En ce jour-là, on mènera un bruit sur lui, semblable au mugissement de la mer. En regardant la Terre, on ne verra que ténèbres et détresse, et la lumière sera obscurcie par les nuées.

## Chapitre 6

### Révélation de YHWH à Yesha`yah

6:1	L'année de la mort du roi Ouzyah, je vis Adonaï assis sur un trône haut et élevé. Les pans de sa robe remplissaient le temple<!--2 Ch. 26:23.-->.
6:2	Les séraphins se tenaient au-dessus de lui. Chacun d'eux avait six ailes : deux pour se couvrir les faces, deux pour se couvrir les pieds et deux pour voler.
6:3	Celui-ci appelait celui-là et disait : Saint, saint, saint est YHWH Tsevaot ! Toute la Terre est pleine de sa gloire !
6:4	Les fondements des seuils furent ébranlés par la voix de celui qui criait et la maison fut remplie de fumée.
6:5	Je dis : Malheur à moi ! Oui, je suis perdu, oui, je suis un homme aux lèvres impures, et j'habite au milieu d'un peuple aux lèvres impures. Oui, mes yeux ont vu le Roi, YHWH Tsevaot<!--Jg. 13:21-22.-->.
6:6	Mais l'un des séraphins vola vers moi, tenant à la main un charbon ardent, qu'il avait pris sur l'autel avec des pincettes.
6:7	Il toucha ma bouche et dit : Voici, ceci a touché tes lèvres : ton iniquité est détournée, et la propitiation est faite pour ton péché.
6:8	J'entendis la voix d'Adonaï qui disait : Qui enverrai-je et qui marchera pour nous ? Je dis : Me voici, envoie-moi !

### Mission de Yesha`yah

6:9	Il dit : Va et dis à ce peuple<!--Mt. 13:14.--> : Entendre vous entendrez et vous ne comprendrez pas. Voir vous verrez et vous ne saurez pas.
6:10	Engraisse le cœur de ce peuple, rends ses oreilles pesantes et bouche-lui les yeux, de peur qu'il ne voie de ses yeux, qu'il n'entende de ses oreilles, que son cœur ne comprenne, qu'il ne se convertisse et ne soit guéri<!--Mt. 13:15 ; Mc. 4:12 ; Jn. 12:40 ; Ac. 28:27.-->.
6:11	Je dis : Jusqu'à quand, Adonaï ? Il dit : Jusqu'à ce que les villes soient dévastées et sans habitants, que les maisons soient sans êtres humains, et que le sol soit dévasté et en désolation,
6:12	jusqu'à ce que YHWH ait éloigné les êtres humains et qu'une grande partie de la terre soit abandonnée.
6:13	S'il reste encore en elle un dixième, il reviendra pour être brûlé, comme le térébinthe et le chêne, dont la souche reste quand ils sont abattus : la semence sainte en sera la souche<!--Ro. 11:17-25.-->.

## Chapitre 7

### Retsin et Pékach complotent contre Yéhouda

7:1	Et il arriva du temps d'Achaz, fils de Yotham, fils d'Ouzyah, roi de Yéhouda, que Retsin, roi de Syrie, et Pékach, fils de Remalyah, roi d'Israël, montèrent contre Yeroushalaim pour lui faire la guerre, mais ils ne purent faire la guerre contre elle.
7:2	On informa la maison de David en disant : Les Syriens ont établi leur camp sur le territoire d'Éphraïm. Le cœur d'Achaz, et le cœur de son peuple furent ébranlés comme les arbres des forêts qui sont ébranlés par le vent.
7:3	YHWH dit à Yesha`yah : Sors maintenant au-devant d'Achaz, toi et Shear-Yashoub, ton fils, vers l'extrémité de l'aqueduc de l'étang supérieur, sur la route du champ du foulon.
7:4	Dis-lui : Prends garde à toi, sois tranquille ! N'aie pas peur et que ton cœur ne faiblisse pas devant ces deux bouts de tisons fumants, devant l'ardente colère de Retsin et de la Syrie ainsi que du fils de Remalyah !
7:5	Parce que la Syrie, Éphraïm et le fils de Remalyah se sont consultés ensemble pour te faire du mal, en disant :
7:6	Montons contre Yéhouda pour l’effrayer, brisons-le, et établissons pour roi en son sein le fils de Tabeel !
7:7	Ainsi parle Adonaï YHWH : Cela ne tiendra pas, cela n'aura pas lieu.
7:8	Car la tête de la Syrie c'est Damas, et le chef de Damas c'est Retsin. Encore 75 ans, Éphraïm sera brisé et ne sera plus un peuple.
7:9	Et la tête d'Éphraïm c'est la Samarie, et le chef de la Samarie c'est le fils de Remalyah. Si vous ne croyez pas, certainement vous ne serez pas affermis.

### Annonce de la naissance d'Immanou-El (Emmanuel)

7:10	YHWH parla encore à Achaz, en disant :
7:11	Demande pour toi un signe à YHWH, ton Elohîm, soit dans les profondeurs du shéol, soit dans les lieux les plus élevés.
7:12	Achaz dit : Je ne demanderai rien, et je ne tenterai pas YHWH.
7:13	Il dit : Écoutez, s’il vous plaît, maison de David ! Est-ce trop peu pour vous de lasser les hommes, que vous lassiez aussi mon Elohîm ?
7:14	C'est pourquoi Adonaï lui-même vous donnera un signe : Voici, la vierge<!--Le mot hébreu « almah » signifie aussi « jeune fille ».--> deviendra enceinte, elle enfantera un fils et appellera son Nom Immanou-El<!--Le nom « Immanou-El » vient de l'hébreu « `Immanuw'el » qui signifie « El est avec nous ». Mt. 28:20 : « Et voici, je suis avec vous tous les jours jusqu'à l'achèvement de l'âge ». Yéhoshoua (Jésus) est Immanou-El, Elohîm avec nous jusqu'à l'achèvement de l'âge (fin des temps).-->.
7:15	Il mangera de la crème et du miel, jusqu'à ce qu'il sache rejeter le mal et choisir le bien.
7:16	Mais avant que le garçon sache rejeter le mal et choisir le bien, le sol des deux rois devant qui tu ressens une aversion sera abandonné.

### Prophétie sur l'imminente invasion de Yéhouda<!--2 Ch. 28:1-20.-->

7:17	YHWH fera venir contre toi, contre ton peuple, et contre la maison de ton père, des jours tels qu’il n’en est pas venu depuis le jour où Ephraïm s’est séparé de Yéhouda : le roi d'Assyrie.
7:18	Et il arrivera qu'en ce jour-là, YHWH sifflera les mouches qui sont à l'extrémité des ruisseaux d'Égypte, et les abeilles qui sont de la terre d'Assyrie.
7:19	Elles viendront et se poseront sur toutes les vallées désertes, sur les fentes des rochers, sur tous les buissons et sur tous les pâturages.
7:20	En ce jour-là, Adonaï rasera avec un rasoir qu'il aura loué au-delà du fleuve, avec le roi d'Assyrie, la tête et les poils des pieds, et il enlèvera aussi la barbe<!--2 R. 16:5-9.-->.
7:21	Il arrivera, en ce jour-là, qu'un homme nourrira une jeune vache et deux brebis.
7:22	Il arrivera qu'en raison de l'abondance du lait qu'elles produiront, on se nourrira de la crème. Car tous ceux qui seront restés sur la terre se nourriront de la crème et du miel.
7:23	Il arrivera, en ce jour-là, que tout lieu où il y aura 1 000 vignes, valant 1 000 sicles d'argent, sera réduit en ronces et en épines.
7:24	On y entrera avec des flèches et avec l'arc, car toute la terre sera ronces et épines.
7:25	Quant à toutes les montagnes cultivées à la houe, on ne craindra plus de voir des ronces et des épines. Mais on y lâchera les bœufs, et la brebis en foulera le sol.

## Chapitre 8

### Annonce de la défaite de Damas et de la Samarie

8:1	YHWH me dit : Prends pour toi une grande table et écris dessus avec un style du mortel : Maher-Shalal-Hash-Baz (Vite au butin, en hâte au pillage !).
8:2	Je pris avec moi des témoins fidèles : Ouriyah, le prêtre, et Zekaryah, fils de Yeberekyahou.
8:3	Je m'étais approché de la prophétesse, elle devint enceinte et enfanta un fils. Et YHWH me dit : Appelle-le du nom de Maher-Shalal-Hash-Baz (Vite au butin, en hâte au pillage !).
8:4	Car avant que le garçon sache dire : Mon père ! Ma mère ! On enlèvera la puissance de Damas et le butin de Samarie, devant le roi d'Assyrie.
8:5	YHWH me parla encore en disant :
8:6	Parce que ce peuple a rejeté les eaux de Siloé qui coulent doucement, et qu'il s'est réjoui au sujet de Retsin, et du fils de Remalyah,
8:7	à cause de cela, voici, Adonaï va faire monter contre eux les puissantes et grandes eaux du fleuve : le roi d'Assyrie et toute sa gloire. Il s'élèvera partout au-dessus de son canal, et il se répandra sur toutes ses rives.
8:8	Il passera à travers Yéhouda, il débordera et inondera, il atteindra jusqu'au cou. Et les étendues de ses ailes rempliront la largeur de ta terre, Immanou-El !

### Exhortation aux disciples de YHWH à rester fidèles

8:9	Soyez méchants, peuples ! Et vous serez brisés. Prêtez l'oreille, toutes les contrées lointaines de la Terre ! Ceignez-vous ! Et vous serez brisés. Ceignez-vous ! Et vous serez brisés.
8:10	Dressez vos plans<!--Le mot hébreu signifie aussi « prendre conseil ».--> et ils échoueront ! Dites une parole, et elle ne s'accomplira pas ! Car El est avec nous.
8:11	Car ainsi m'a parlé YHWH, quand sa main me saisit et qu'il me corrigea afin de ne pas marcher dans la voie de ce peuple, en disant :
8:12	Ne dites pas : conspiration, toutes les fois que ce peuple dit conspiration ! Ne craignez pas ce qu'il craint, ne le redoutez pas !
8:13	C'est YHWH Tsevaot<!--YHWH des armées. Voir 1 S. 1:3.-->, que vous devez sanctifier<!--Voir 1 Pi. 3:15 ; Ap. 19.-->, c'est lui que vous devez craindre et redouter.
8:14	Il deviendra le sanctuaire, mais aussi la pierre d'achoppement<!--YHWH s'est présenté comme une pierre d'achoppement et un rocher de scandale. En Es. 44:8, il affirme ne pas connaître d'autre rocher que lui. Yesha`yah n'est pas le seul prophète à qui le Seigneur s'est révélé comme étant une pierre et un rocher. Dans le Ps. 118:22, il est dit : « La pierre que les bâtisseurs avaient rejetée est devenue la tête de l'angle ». Daniye'l et Zekaryah (Zacharie) ont également prophétisé au sujet de cette pierre : « Tu regardais cela, jusqu'à ce qu'une pierre se détacha sans l'aide d'une main, frappa les pieds en fer et en argile de la statue et les brisa. Alors le fer, l'argile, le cuivre, l'argent et l'or, furent brisés ensemble et devinrent comme la paille de l'aire en été, que le vent transporte çà et là et nulle trace n'en fut retrouvée. Mais la pierre qui avait frappé la statue devint une grande montagne et remplit toute la terre. » (Da. 2:34-35). « Car voici, quant à la pierre que j'ai mise devant Yéhoshoua. Sur cette pierre qui n'est qu'une, il y a sept yeux. Voici, je graverai moi-même ce qui doit y être gravé, – déclaration de YHWH Tsevaot. J'ôterai en un jour l'iniquité de cette terre. » (Za. 3:9). Ces prophéties se sont accomplies en Yéhoshoua ha Mashiah (Jésus-Christ), l'Agneau d'Elohîm qui ôte le péché du monde (Jn. 1:29). Le Seigneur s'est d'ailleurs clairement identifié à la pierre angulaire, affirmant ainsi sa divinité (Lu. 20:17-19). En Mt. 16:18, il s'est présenté comme le rocher inébranlable sur lequel il allait bâtir son Assemblée (Église). De plus, il est à noter que dans le livre d'Apocalypse, l'Agneau possède sept yeux comme la pierre vue par Zekaryah (Ap. 5:6). Ces sept yeux sont aussi les sept lampes du chandelier d'or que Zekaryah et Yohanan avaient également vues (Za. 4:2 ; Ap. 4:5). Or le chiffre 7 symbolise la plénitude et la perfection divines. Yesha`yah prophétisa encore en ces termes : « Voici, j'établirai en Sion une pierre, une pierre éprouvée, angulaire, précieuse, une fondation établie. Celui qui croira ne se hâtera pas. » (Es. 28:16). Les écrits de la nouvelle alliance attestent l'accomplissement de cette prophétie en Yéhoshoua ha Mashiah, notamment par la bouche de Paulos (Paul) et de Petros (Pierre) : « Ayant été édifiés sur le fondement des apôtres et des prophètes, et Yéhoshoua Mashiah lui-même étant la pierre angulaire. » (Ep. 2:20). « Car personne ne peut poser un autre fondement que celui qui est déjà posé, lequel est Yéhoshoua ha Mashiah. » (1 Co. 3:11). « Vous approchant de lui, pierre vivante, rejetée en effet par les humains, mais choisie et précieuse devant Elohîm. Vous aussi, comme des pierres vivantes, vous êtes édifiés, maison spirituelle, sainte prêtrise, afin d'offrir des sacrifices spirituels, agréables à Elohîm par le moyen de Yéhoshoua Mashiah. » (1 Pi. 2:4-5). Voir Ps. 42:10.-->, le rocher de scandale pour les deux maisons d'Israël, le filet et le piège pour les habitants de Yeroushalaim.
8:15	Beaucoup parmi eux trébucheront, ils tomberont et se briseront, ils seront pris au piège et capturés.
8:16	Enveloppe ce témoignage, scelle cette torah parmi mes disciples.
8:17	Je m'attends à YHWH, qui cache ses faces à la maison de Yaacov, et je regarde à lui.
8:18	Me voici, avec les enfants que YHWH m'a donnés : nous sommes des signes et des miracles en Israël, de la part de YHWH Tsevaot, qui habite sur la montagne de Sion<!--Hé. 2:13.-->.
8:19	Si l'on vous dit : Consultez ceux qui évoquent les morts et ceux qui ont un esprit de divination, qui grommellent et murmurent ! Un peuple doit-il consulter les morts en faveur des vivants et non pas son Elohîm ?
8:20	À la torah et au témoignage ! Si l'on ne parle pas ainsi, il n'y aura certainement pas d'aurore pour le peuple.
8:21	Il sera errant sur la terre, accablé et affamé, et il arrivera que lorsqu'il aura faim, il s'irritera, maudira son roi et son Elohîm et tournera les yeux en haut.
8:22	Puis il regardera vers la Terre, et voici : détresse et ténèbre, assombrissement, angoisse, obscurité, bannissement.
8:23	Mais les ténèbres ne seront pas autant qu'elles avaient été là où il y a de la détresse. Si au commencement Elohîm affligea légèrement la terre de Zebouloun et la terre de Nephthali, dans l'avenir, il couvrira de gloire la route de la mer, au-delà du Yarden<!--Jourdain.-->, la Galilée des nations.

## Chapitre 9

### Annonce de la naissance et du règne du Mashiah (Christ)

9:1	Le peuple qui marchait dans la ténèbre voit une grande lumière, et la lumière resplendit sur ceux qui habitaient la terre de l'ombre de la mort<!--Mt. 4:15-16.-->.
9:2	Tu multiplies la nation, tu lui accordes de grandes joies, ils se réjouissent devant toi, comme on se réjouit à la moisson, comme on se réjouit quand on partage le butin.
9:3	Car tu as brisé le joug dont il était chargé et le bâton qui frappait ses épaules, la verge de celui qui l'opprimait comme au jour de Madian.
9:4	Car toute sandale de celui qui marche avec bruit et tous les manteaux roulés dans le sang brûlent et deviennent le carburant pour le feu.
9:5	Car un enfant nous est né, un Fils nous a été donné<!--Yéhoshoua ha Mashiah (Jésus-Christ) est 100% Elohîm et 100% humain. Il existe depuis toute éternité en tant qu'Elohîm. Il est devenu homme au moment de son incarnation (Ph. 2:5-7).-->, et le gouvernement sera sur son épaule : on l'appellera du nom de Miracle<!--Le mot hébreu veut aussi dire « Admirable ».-->, Conseiller, El Gibbor<!--Da. 11:36 ; Jé. 32:18. El Gibbor signifie « El Puissant ». Es. 10:21 ; Jos. 22:22 ; 2 S. 22:32.-->, Père d'éternité<!--Philippos (Philippe), disciple de Yéhoshoua ha Mashiah (Jésus-Christ) voulait rencontrer le Père. Il posa au Seigneur cette question : « Seigneur, montre-nous le Père, et cela nous suffit. » (Jn. 14:8). Yéhoshoua lui répondit : « Je suis depuis si longtemps avec vous et tu ne m'as pas connu, Philippos ! » (Jn. 14:9).-->, Prince de paix<!--Jg. 6:24.-->,
9:6	pour accroître le gouvernement, pour la paix sans fin sur le trône de David et sur son royaume, pour l'affermir et le soutenir par le droit et par la justice, dès maintenant et pour toujours<!--Lu. 1:32-33.-->. Voilà ce que fera le zèle de YHWH Tsevaot.

### Jugement sur le royaume du nord

9:7	Adonaï envoie une parole à Yaacov, et elle tombe sur Israël<!--Ge. 32:29.-->.
9:8	Tout le peuple la connaîtra, Éphraïm et les habitants de Samarie, avec orgueil et grandeur du cœur, ils disent :
9:9	Des briques sont tombées ? Nous bâtirons en pierres de taille. Des sycomores ont été coupés ? Nous les changerons en cèdres.
9:10	YHWH élèvera contre eux les ennemis de Retsin, et il armera les ennemis d'Israël :
9:11	la Syrie à l'orient, et les Philistins à l'occident. Ils dévoreront Israël à pleine bouche. Malgré tout cela, sa colère ne se détourne pas et sa main est encore étendue.
9:12	Parce que le peuple ne revient pas à celui qui le frappe, et il ne cherche pas YHWH Tsevaot.
9:13	À cause de cela YHWH retranchera d'Israël en un seul jour la tête et la queue, la branche de palmier et le roseau.
9:14	L’ancien, l’élevé des faces, c’est la tête, et le prophète qui enseigne le mensonge, c'est la queue.
9:15	Car les conducteurs de ce peuple l'égarent<!--1 Ti. 4:1 ; Tit. 1:10.--> et ceux qui sont conduits par eux sont engloutis.
9:16	C'est pourquoi Adonaï ne se réjouira pas de leurs jeunes hommes, il n'aura pas compassion de leurs orphelins et de leurs veuves, car tous sont des athées et des méchants, et toute bouche ne profère que des infamies. Malgré tout cela, sa colère ne s'apaise pas et sa main est encore étendue.
9:17	Car la méchanceté consume comme un feu, elle dévore les ronces et les épines. Elle embrase l'épaisseur de la forêt, d'où s'élèvent des colonnes de fumée.
9:18	Par la fureur de YHWH Tsevaot, la terre a été brûlée et le peuple est devenu le carburant pour le feu : aucun homme n'a compassion de son frère.
9:19	On pille à droite et l'on a faim, on dévore à gauche et l'on n'est pas rassasié. Chaque homme mange la chair de son bras.
9:20	Menashè contre Éphraïm, Éphraïm contre Menashè, ils s'unissent contre Yéhouda. Avec tout cela, sa colère ne s'est pas détournée, et sa main reste étendue !

## Chapitre 10

10:1	Malheur à ceux qui décrètent des ordonnances iniques, et à ceux qui écrivent pour ordonner l'oppression,
10:2	pour refuser la justice aux pauvres et ravir le droit aux malheureux de mon peuple, afin d'avoir les veuves pour leur butin, et de piller les orphelins !
10:3	Que ferez-vous au jour du châtiment, et de la dévastation qui viendra de loin ? Vers qui fuirez-vous pour avoir du secours et où laisserez-vous votre gloire<!--Os. 9:7 ; Mt. 24:17-21 ; Lu. 19:41-44.--> ?
10:4	Ceux qui ne seront pas courbés parmi les prisonniers, tomberont parmi les morts. Avec tout cela, sa colère ne s'est pas détournée, et sa main reste étendue !

### Jugement sur l'Assyrie

10:5	Malheur à l'Assyrie, verge de ma colère ! Le bâton dans sa main, c'est ma colère.
10:6	Je l'ai envoyé contre une nation athée, je lui ai donné mes ordres contre le peuple de ma colère, pour arracher le butin, pour piller au pillage, pour le fouler aux pieds comme la boue des rues.
10:7	Mais lui, il ne l’imagine pas ainsi, son cœur ne pense pas ainsi, car il a à cœur de détruire et de retrancher des nations, non en petit nombre.
10:8	Car il dit : Mes princes ne sont-ils pas tous rois ?
10:9	Kalneh n'est-elle pas comme Karkemiysh ? Hamath n'est-elle pas comme Arpad ? Et Samarie n'est-elle pas comme Damas ?
10:10	De même que ma main a atteint les royaumes des faux elohîm, où il y avait plus d'images gravées qu'à Yeroushalaim et à Samarie,
10:11	ne traiterai-je pas Yeroushalaim et ses faux elohîm, tout comme j'ai traité Samarie avec ses idoles ?
10:12	Mais il arrivera que, quand Adonaï aura achevé toute son œuvre sur la montagne de Sion et à Yeroushalaim, je punirai le roi d'Assyrie pour le fruit de son cœur orgueilleux, et pour la beauté de ses yeux hautains.
10:13	Parce qu'il dit : C'est par la force de ma main que j'ai agi, c'est par ma sagesse, car je suis intelligent. J'ai reculé les bornes des peuples, et j'ai pillé ce qu'ils avaient de plus précieux. Comme un homme vaillant, j'ai fait descendre ceux qui étaient assis.
10:14	Ma main a trouvé les richesses des peuples comme on trouve un nid. Comme on rassemble des œufs délaissés, ainsi ai-je rassemblé toute la Terre. Personne n'a remué l'aile, ni ouvert le bec, ni poussé un cri.
10:15	La hache se glorifie-t-elle contre celui qui taille avec ? Ou la scie s'élève-t-elle contre celui qui la manie ? Comme si la verge faisait mouvoir celui qui la lève, et que le bâton se levait comme s'il n'était pas du bois !
10:16	C'est pourquoi Seigneur, YHWH Tsevaot, enverra la maigreur sur ses hommes gras. Et sous sa gloire s'allumera un embrasement, tel l'embrasement d'un feu.
10:17	La lumière d'Israël deviendra un feu, et son Saint une flamme qui brûlera et dévorera ses épines et ses ronces en un jour !
10:18	Il consumera la gloire de sa forêt et de son verger, depuis l'âme jusqu'à la chair. Elle deviendra comme lorsque fond un malade.
10:19	Le reste des arbres de sa forêt pourra être compté, et un garçon en écrirait le nombre.

### Conversion et délivrance du reste d'Israël

10:20	Il arrivera en ce jour-là, que le reste d'Israël et les rescapés de la maison de Yaacov ne s'appuieront plus sur celui qui les frappait, mais ils s'appuieront avec confiance sur YHWH, le Saint d'Israël.
10:21	Le reste<!--Ro. 9:27.--> retournera, le reste de Yaacov, vers El Gibbor<!--Ge. 49:24 ; Es. 9:5 ; Jé. 32:18 ; Mt. 1:23.-->.
10:22	Oui, même si ton peuple, Israël, devenait comme le sable de la mer, un reste retournera à lui<!--Ro. 9:27.-->. La destruction est décrétée, elle fera déborder la justice.
10:23	Car la destruction qu'il a décrétée, Adonaï YHWH Tsevaot, va l'exécuter au milieu de toute la terre.
10:24	C'est pourquoi ainsi parle Adonaï YHWH Tsevaot : Mon peuple, qui habite en Sion, n'aie pas peur du roi d'Assyrie ! Il te frappe de la verge et il lève son bâton sur toi comme faisait l'Égypte.
10:25	Mais encore un peu de temps, un peu de temps, et le châtiment cessera, puis ma colère se tournera contre lui pour l'exterminer.
10:26	YHWH Tsevaot lèvera le fouet contre lui tout comme il a frappé Madian au rocher d'Oreb, et il lèvera encore son bâton sur la mer, comme il l'a levé sur le chemin d'Égypte.
10:27	Il arrivera en ce jour-là que son fardeau sera ôté de dessus ton épaule et son joug de dessus ton cou. Et le joug sera détruit en présence de l'onction.

### Défaite des Assyriens<!--Es. 35-36, 37:7.-->

10:28	Il marche sur Ayyat, traverse Migron et il met ses bagages à Micmash.
10:29	Ils passent le gué, ils couchent à Guéba. Ramah est effrayée, Guibea de Shaoul prend la fuite.
10:30	Pousse des cris, fille de Gallim ! Malheur à toi Anathoth ! Prends garde Laïs !
10:31	Madména se disperse, les habitants de Guébim se sauvent en foule.
10:32	Encore un jour d'arrêt à Nob, et il menace de sa main la montagne de la fille de Sion, la colline de Yeroushalaim.
10:33	Voici, le Seigneur, YHWH Tsevaot, brisera les rameaux avec force : ceux qui sont de hautes statures seront coupés, et les plus élevés seront abaissés.
10:34	Il taillera avec le fer les lieux les plus épais de la forêt, et le Liban tombera sous le Puissant.

## Chapitre 11

### Rétablissement du règne de David par le Mashiah

11:1	Une verge sortira du tronc d'Isaï, et un rejeton de ses racines<!--Mt. 1:6-16 ; Lu. 1:31-32 ; Ro. 15:12 ; Ap. 5:5.--> portera du fruit.
11:2	L'Esprit de YHWH reposera sur lui, Esprit de sagesse et de discernement, Esprit de conseil et de force, Esprit de connaissance et de crainte de YHWH<!--Es. 61:1 ; Lu. 4:18.-->.
11:3	Il flairera la crainte de YHWH. Il ne jugera pas à vue d'œil et ne corrigera pas selon la rumeur de ses oreilles<!--Jé. 11:20 ; Mt. 22:16 ; Ap. 2:23.-->.
11:4	Mais il jugera les pauvres avec justice et corrigera les malheureux de la Terre avec droiture. Il frappera la Terre par la verge de sa bouche et fera mourir le méchant par l'Esprit de ses lèvres<!--Job 4:9, 15:30 ; 2 Th. 2:8.-->.
11:5	La justice sera la ceinture de ses reins, et la fidélité, la ceinture de ses flancs<!--Ep. 6:14.-->.
11:6	Le loup habitera avec l'agneau, et le léopard se couchera avec le chevreau. Le veau, le lionceau et le bétail qu'on engraisse seront ensemble, et un petit garçon les conduira.
11:7	La jeune vache paîtra avec l'ourse, leurs petits auront un même gîte, et le lion, comme le bœuf, mangera de la paille<!--Es. 65:25.-->.
11:8	Le nourrisson s'amusera<!--Ou encore « caresser ».--> sur le trou du cobra et l'enfant sevré mettra sa main dans l'antre de la vipère.
11:9	On ne fera plus de mal et on ne détruira plus sur toute ma montagne sainte, car la Terre sera remplie de la connaissance de YHWH, comme les eaux couvrent le fond de la mer.
11:10	Et il arrivera en ce jour-là que la racine<!--Yéhoshoua (Jésus) est la Racine de David. Voir Ro. 15:12 ; Ap. 5:5, 22:16.--> d'Isaï se tiendra debout comme une bannière<!--Voir le dictionnaire en annexe.--> pour les peuples ; les nations la chercheront, et son lieu de repos sera glorieux.

### Établissement du règne du Mashiah

11:11	Il arrivera en ce jour-là, qu'Adonaï étendra encore sa main une seconde fois pour acquérir le reste de son peuple dispersé en Assyrie, en Égypte, à Pathros, en Éthiopie, à Éylam, à Shinear, à Hamath et dans les îles de la mer.
11:12	Il élèvera une bannière parmi les nations, il rassemblera les exilés d'Israël qui auront été chassés, et il recueillera les dispersés de Yéhouda des quatre extrémités de la Terre.
11:13	La jalousie d'Éphraïm sera ôtée, et les oppresseurs de Yéhouda seront retranchés ; Éphraïm ne sera plus jaloux de Yéhouda, et Yéhouda n'opprimera plus Éphraïm.
11:14	Ils voleront sur l'épaule des Philistins vers la mer, ils pilleront ensemble les fils de l'orient. Ils étendront leur main sur Édom et Moab, et les fils d'Ammon leur obéiront.
11:15	YHWH frappera d'interdit la langue de la mer d'Égypte, et il lèvera sa main contre le fleuve par la force de son vent, et il le frappera en ses sept torrents, on y marchera en sandales.
11:16	Il y aura une voie pour le reste de son peuple qui restera en Assyrie, comme il en était d’Israël le jour où il monta de la terre d'Égypte.

## Chapitre 12

### Louange au sein du royaume

12:1	Tu diras en ce jour-là : Je te loue YHWH ! Car tu as été irrité contre moi, ta colère s'est apaisée, et tu m'as consolé.
12:2	Voici, El est mon salut<!--yeshuw`ah. Voir commentaire en Ge. 49:18.-->, j'aurai confiance et je ne tremblerai plus, car Yah, YHWH est ma force et ma musique<!--Ex. 15:2 ; Ps. 118:14.-->, il est devenu mon salut<!--Il est devenu mon yeshuw`ah. Ps. 35:3, 42:5-12, 43:5, 62:2-3. Voir Ac. 4:12.-->.
12:3	Vous puiserez de l'eau avec joie aux sources du salut<!--Jn. 4:10-14.-->,
12:4	et vous direz en ce jour-là : Louez YHWH, invoquez son Nom, publiez ses œuvres parmi les peuples, rappelez que son Nom est une haute retraite !
12:5	Chantez des louanges à YHWH car il a fait des choses magnifiques : cela est connu de toute la Terre !
12:6	Habitante de Sion, crie d'une manière aiguë et pousse des cris de joie ! Car le Saint d'Israël est grand au milieu de toi.

## Chapitre 13

### YHWH lève une armée

13:1	Fardeau de Babel, révélé à Yesha`yah, fils d'Amots.
13:2	Élevez la bannière sur la haute montagne, élevez la voix vers eux, faites des signes avec la main, et qu'on entre dans les portes des magnifiques !
13:3	C'est moi qui ai donné des ordres à ceux qui me sont consacrés, j'ai appelé mes hommes vaillants pour exécuter ma colère, ceux qui se réjouissent de ma grandeur.
13:4	Voix d'une multitude sur les montagnes, semblable à celle de peuples nombreux ! Voix d’un vacarme des royaumes des nations rassemblées ! YHWH Tsevaot passe en revue l'armée pour le combat.
13:5	D'une terre éloignée, de l'extrémité des cieux, YHWH vient avec les instruments de sa colère pour détruire toute la Terre.

### Jugement de YHWH sur Babel (Babylone)

13:6	Hurlez, car le jour de YHWH<!--Voir commentaire en Za. 14:1.--> est proche, il vient comme un ravage de Shaddaï.
13:7	C'est pourquoi toutes les mains deviennent lâches, et tout cœur du mortel se fond.
13:8	Ils sont terrifiés. Les détresses et les douleurs les saisissent, ils se tordent comme une femme qui accouche. L’homme à son compagnon, ils s’étonnent. Leurs faces sont des faces enflammées.
13:9	Voici, le jour de YHWH<!--Voir commentaire en Za. 14:1.--> arrive. Jour cruel, jour de colère et d'ardente fureur<!--Mal. 3:19 ; Ap. 19:15.--> qui transformera la Terre en désolation et en exterminera les pécheurs.
13:10	Car les étoiles des cieux et leurs constellations ne feront plus briller leur lumière, le soleil s'obscurcira dès son lever, et la lune ne fera plus resplendir sa lumière<!--Joë. 3:4 ; Mt. 24:29 ; Lu. 21:25-26 ; Mc. 13:24.-->.
13:11	Je punirai le monde à cause de sa malice, et les méchants à cause de leur iniquité. Je ferai cesser l'orgueil des hautains et j'abaisserai l'arrogance des terrifiants.
13:12	Je rendrai les mortels plus précieux que l'or fin, les humains plus que l'or d'Ophir.
13:13	C'est pourquoi j'ébranlerai les cieux, et la Terre sera secouée sur sa base<!--Ag. 2:6.-->, à cause de la fureur de YHWH Tsevaot, à cause du jour de son ardente colère.
13:14	Ils deviendront comme une gazelle bannie, comme un troupeau que nul ne rassemble, chaque homme se tournera vers son peuple, chaque homme s'enfuira vers sa terre.
13:15	Tous ceux que l'on trouvera seront transpercés et tous ceux que l'on attrapera tomberont par l'épée.
13:16	Leurs enfants seront écrasés sous leurs yeux<!--Na. 3:10.-->, leurs maisons seront pillées et leurs femmes violées.

### YHWH envoie les Mèdes contre Babel

13:17	Voici, je vais susciter contre eux les Mèdes, qui ne pensent pas à l'argent et ne désirent pas l'or.
13:18	Leurs arcs écraseront les jeunes hommes, ils seront sans pitié pour le fruit des entrailles, leur œil n'épargnera pas les fils.
13:19	Babel, l'ornement des royaumes, la parure et l'orgueil des Chaldéens, deviendra comme Sodome et Gomorrhe qu'Elohîm détruisit.
13:20	Elle ne sera plus jamais habitée, elle ne sera pas habitée d’âge en âge. Même les Arabes n'y dresseront pas leurs tentes, et les bergers n'y feront plus reposer leurs troupeaux.
13:21	Les bêtes sauvages des déserts y prendront leur gîte, les hiboux rempliront ses maisons, les filles de l'autruche y habiteront et les boucs y sauteront.
13:22	Les chacals hurleront dans ses palais, et les dragons dans ses maisons de plaisance. Son temps est près d'arriver et ses jours ne se prolongeront pas.

## Chapitre 14

### Chant d'Israël après la chute de Babel

14:1	Oui, YHWH aura pitié de Yaacov, il choisira encore Israël, et il les rétablira dans leur sol. Les étrangers se joindront à eux et s'attacheront à la maison de Yaacov.
14:2	Les peuples les prendront et les feront venir vers leur lieu. La maison d'Israël les possédera, sur le sol de YHWH, comme serviteurs et comme servantes. Ils emmèneront captifs ceux qui les emmenaient captifs, et ils domineront sur leurs oppresseurs.
14:3	Et il arrivera qu'au jour où YHWH te fera reposer de ta douleur, de ton trouble et du dur service auquel on t’a asservi,
14:4	tu prononceras ce proverbe contre le roi de Babel, et tu diras : Comment le tyran a cessé ! La cité en or a cessé !
14:5	YHWH a brisé le bâton des méchants et la verge des dominateurs,
14:6	celui qui, dans sa fureur, frappait de coups les peuples sans relâche, qui dominait sur les nations avec colère par une persécution sans retenue.
14:7	Toute la Terre jouit du repos et de la paix, on éclate en cris de joie.
14:8	Même les cyprès et les cèdres du Liban se réjouissent de toi : Depuis que tu es tombé, personne n'est monté pour nous abattre.
14:9	Le shéol dans ses parties inférieures s'agite à ton sujet pour venir à ta rencontre. Il réveille à cause de toi les fantômes<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».--> et il fait lever de leurs sièges tous les principaux de la Terre.
14:10	Tous répondent et te disent : Toi aussi, tu es sans force comme nous, tu es devenu semblable à nous !
14:11	Ta hauteur est descendue dans le shéol, avec le son de tes luths. Tu es couché sur une couche de vers et la vermine est ta couverture.

### Orgueil, rébellion et chute de Satan

14:12	Comment es-tu tombé des cieux, Heylel<!--« Porteur de lumière », « le brillant », « l'étoile du matin ». D'autres traduisent par « Lucifer ». L'hébreu « heylel » a pour racine « halal » qui signifie « briller », « louer ».-->, fils de l'aurore ? Tu es abattu jusqu'à terre, toi qui affaiblissais les nations ! 
14:13	Tu disais dans ton cœur : Je monterai aux cieux, je placerai mon trône au-dessus des étoiles de El et je m'assiérai sur la montagne de l'assemblée, du côté d'Aquilon<!--Aquilon est un dieu des vents septentrionaux, froids et violents, dans la mythologie romaine.--> ;
14:14	je monterai au-dessus des hauts lieux des nuées, je serai semblable à Élyon.
14:15	Mais on t'a fait descendre au shéol, aux parties extrêmes de la fosse<!--Voir commentaire en Ge. 1:1-2.-->.
14:16	Ceux qui te voient t'observent avec attention, ils te considèrent attentivement, en disant : N'est-ce pas celui qui faisait trembler la Terre, qui ébranlait les royaumes,
14:17	qui transformait le monde en désert, qui détruisait les villes et n'ouvrait pas la maison à ses prisonniers ?

### Babel anéantie

14:18	Tous les rois des nations, tous, reposent avec gloire, chaque homme dans sa maison.
14:19	Mais toi, tu as été jeté loin de ton sépulcre, comme un rejeton abominable, comme les vêtements de gens tués, transpercés avec l'épée, qui sont descendus parmi les pierres d'une fosse, comme un cadavre foulé aux pieds.
14:20	Tu ne seras pas rangé comme eux dans le sépulcre, car tu as ravagé ta terre, tu as tué ton peuple. La postérité des méchants ne sera plus jamais nommée.
14:21	Préparez le massacre des fils, à cause de l'iniquité de leurs pères. Qu'ils ne se lèvent pas pour prendre possession de la Terre et remplir de villes les faces du monde !
14:22	Je m'élèverai contre eux, – déclaration de YHWH Tsevaot –, je retrancherai de Babel le nom et le reste, sa descendance et sa progéniture<!--Ap. 14:8, 18:2.--> – déclaration de YHWH.
14:23	J'en ferai la possession du hérisson et une mare d’eau. Je la balayerai avec le balai de la destruction, – déclaration de YHWH Tsevaot.

### Jugement sur le roi d'Assyrie

14:24	YHWH Tsevaot l'a juré, en disant : Certainement, comme je l'ai imaginé, ainsi il arrivera, et comme je l'ai conseillé, ainsi il tiendra.
14:25	Je briserai l'Assyrie en ma terre, je la foulerai aux pieds sur mes montagnes. Son joug leur sera ôté, et son fardeau sera ôté de dessus leurs épaules.
14:26	C'est là le conseil qui est conseillé contre toute la Terre, c'est là la main étendue sur toutes les nations.
14:27	Car YHWH Tsevaot l'a conseillé : qui le ferait échouer ? Sa main est étendue : qui la détournerait<!--Ec. 7:13.--> ?

### Jugement sur la terre des Philistins

14:28	L'année de la mort du roi Achaz, ce fardeau fut prononcé :
14:29	Ne te réjouis pas, toi terre des Philistins, de ce que la verge de celui qui te frappait est brisée ! Car de la racine du serpent sortira une vipère, et son fruit sera un serpent brûlant qui vole.
14:30	Les premiers-nés des pauvres paîtront, et les pauvres se coucheront en sécurité. Mais je ferai mourir de faim ta racine, et ce qui restera de toi sera tué.
14:31	Porte, hurle ! Ville, crie ! Tremble, terre tout entière des Philistins ! Car d'Aquilon vient une fumée, et il ne restera pas un homme dans ses habitations.
14:32	Que répondra-t-on aux envoyés de cette nation ? Que YHWH a fondé Sion, et que les affligés de son peuple y trouvent un refuge.

## Chapitre 15

### Jugement sur Moab

15:1	Fardeau de Moab. Oui, dans la nuit, Ar a été ravagée, Moab est détruite ! Oui, dans la nuit, Kir a été ravagée, Moab est détruite  !
15:2	Il monte à Bayith et à Dibon, dans les hauts lieux, pour pleurer. Moab est en lamentations sur Nebo et sur Médeba : toutes les têtes sont rasées et toutes les barbes sont coupées.
15:3	Dans les rues ils se ceignent de sacs. Sur les toits et sur les places tous hurlent et descendent en larmes<!--Jé. 48:38.-->.
15:4	Hesbon et Élealé poussent des cris, et l'on entend leur voix jusqu'à Yahats. C'est pourquoi les guerriers de Moab se lamentent, ils ont l'effroi dans l'âme.
15:5	Mon cœur crie à cause de Moab, dont les fugitifs s'enfuient jusqu'à Tsoar, comme une génisse de 3 ans. En effet, ils montent par la montée de Louhith avec des pleurs, et ils jettent des cris de détresse sur le chemin de Choronaïm.
15:6	Car les eaux de Nimrim deviennent une horreur. Car l’herbe verte s’est desséchée, l'herbe est consumée, et il n'y a pas de verdure.
15:7	C'est pourquoi ils emmagasinent les richesses qu'ils ont produites, et ils les transportent au-delà du torrent des Saules.
15:8	Car les cris environnent les frontières de Moab, ses lamentations retentissent jusqu'à Églaïm, ses lamentations retentissent jusqu'à Beer-Élim.
15:9	Oui, les eaux de Dimon sont pleines de sang, oui, j’imposerai plus encore à Dimon : un lion contre les rescapés de Moab, et le reste du sol.

## Chapitre 16

### Lamentation sur Moab

16:1	Envoyez l'agneau au dominateur de la terre, envoyez-le du rocher du désert, à la montagne de la fille de Sion.
16:2	Il arrivera que les filles de Moab seront au passage de l'Arnon, comme un oiseau volant çà et là, comme une nichée chassée de son nid.
16:3	Fais venir le conseil, fais justice ! En plein midi, étends ton ombre comme celle de la nuit, cache les bannis, que les errants ne soient pas découverts !
16:4	Que les bannis de Moab demeurent chez toi ! Sois pour eux un refuge contre le dévastateur ! Car l’oppresseur ira vers sa fin, la dévastation cessera, celui qui piétine sera consumé de dessus la terre.
16:5	Le trône sera affermi par la bonté. Sur lui siégera avec fidélité, dans la tente de David, un juge cherchant le droit et prompt à faire justice<!--Mi. 4:7 ; Da. 7:14 ; Lu. 1:33 ; Ap. 11:15.-->.
16:6	Nous avons entendu l'orgueil de Moab, le très orgueilleux, sa fierté, son orgueil, son arrogance et ses vains discours.
16:7	C'est pourquoi Moab gémit sur Moab, il gémit tout entier. Vous soupirez pour les fondements de Kir-Haréseth, il n'y aura que des gens blessés à mort.
16:8	Car les champs de Hesbon et le vignoble de Sibma languissent. Les maîtres des nations ont foulé ses meilleurs ceps, qui s'étendaient jusqu'à Ya`azeyr, qui couraient çà et là par le désert. Ses rameaux s'étendaient et passaient au-delà de la mer.
16:9	C'est pourquoi je pleure sur la vigne de Sibma, comme sur Ya`azeyr, je vous arrose de mes larmes, Hesbon et Élealé ! Car l'ennemi avec des cris s'est jeté sur tes fruits d'été et sur ta moisson.
16:10	La joie et l'allégresse se sont retirées des vergers. On ne se réjouit plus et on ne s'égaye plus dans les vignes, le vendangeur ne foule plus dans les cuves, j'ai fait cesser la chanson de la vendange<!--Jé. 48:31-34.-->.
16:11	C'est pourquoi mes entrailles gémissent sur Moab comme une harpe, et mon intérieur sur Kir-Harès.
16:12	Il arrivera quand on verra que Moab est fatiguée sur le haut lieu, qu'il entrera dans son sanctuaire pour prier, mais il ne prévaudra pas.
16:13	Telle est la parole que YHWH a prononcée depuis longtemps sur Moab.
16:14	Et maintenant YHWH a parlé, en disant : Dans trois ans, comme les années d'un mercenaire, la gloire de Moab sera méprisée, avec toute cette grande multitude. Il en restera très peu, un petit nombre sans aucune puissance.

## Chapitre 17

### Prophétie sur la chute de Damas et de ses alliés

17:1	Fardeau de Damas. Voici Damas écartée des villes, elle devient un tas de ruines<!--Jé. 49:23-27.-->.
17:2	Les villes d'Aroër sont abandonnées, elles sont livrées aux troupeaux qui s'y reposent, et il n'y a personne qui les effraie.
17:3	Il n'y aura plus de forteresse en Éphraïm, ni de royaume à Damas et dans le reste de la Syrie. Ils seront comme la gloire des fils d'Israël, – déclaration de YHWH Tsevaot.
17:4	Il arrivera en ce jour-là que la gloire de Yaacov sera affaiblie et la graisse de sa chair sera fondue.
17:5	Ce sera comme quand le moissonneur recueille les blés, et qu'il moissonne les épis avec son bras<!--Joë. 4:13 ; Mt. 13:24-30.--> ; ce sera comme quand on ramasse les épis dans la vallée des géants.
17:6	Il en restera quelques grappillages, comme quand on secoue l'olivier, et qu'il reste 2 ou 3 olives en haut de la cime, et qu'il y en a 4 ou 5 que l'olivier a produites dans ses branches fruitières, – déclaration de YHWH, l'Elohîm d'Israël.
17:7	En ce jour-là, l'être humain regardera vers celui qui l'a fait, et ses yeux se tourneront vers le Saint d'Israël.
17:8	Il ne regardera plus vers les autels, qui sont l'ouvrage de ses mains, et il ne regardera plus ce que ses doigts ont fabriqué, ni les asherah<!--Voir commentaire en Ex. 34:13.-->, ni les statues du soleil.
17:9	En ce jour-là, ses villes fortes seront abandonnées à cause des fils d'Israël, ils seront comme un bois taillis et des rameaux abandonnés, et ce sera un désert.
17:10	Parce que tu as oublié l'Elohîm de ton salut, et que tu ne t'es pas souvenue du rocher<!--Voir commentaire Es. 8:13-14.--> de ton refuge, c'est pourquoi tu plantes des plantations d'agréments et tu sèmes des sarments étrangers.
17:11	Le jour où tu les plantes, tu les entoures d'une clôture, et le matin tu fais fleurir tes semences. Le jour de la moisson sera faible et un tas de douleur incurable.
17:12	Malheur à la multitude de peuples nombreux ! Ils murmurent comme murmurent les mers, le vacarme de nations qui font un vacarme comme le tapage des eaux puissantes !
17:13	Les nations font un vacarme comme une tempête éclatante de grosses eaux. Mais il les menace, et elles s'enfuient au loin, chassées comme la balle des montagnes face au vent, comme un tourbillon face au vent d'orage.
17:14	Au temps du soir, voici une terreur soudaine, mais avant le matin, ils ne sont plus ! C'est là le partage de ceux qui nous dépouillent, et le lot de ceux qui nous pillent.

## Chapitre 18

### Jugement sur l'Éthiopie

18:1	Malheur à la terre qui fait ombre avec des ailes, la région au delà des fleuves de l'Éthiopie<!--So. 3:10.-->,
18:2	qui envoie par mer des messagers, dans des navires de papyrus, sur les faces des eaux ! Allez messagers rapides, vers la nation robuste et vigoureuse, vers le peuple redoutable, depuis là où il est et par delà, la nation puissante qui foule aux pieds et dont les fleuves ravagent sa terre !
18:3	Vous tous, habitants du monde, et vous qui habitez sur la terre, quand la bannière sera élevée sur les montagnes, regardez ! Lorsque le shofar sonnera, écoutez !
18:4	Car ainsi m'a parlé YHWH : Je me tiens tranquillement, et je regarde de ma demeure, par la chaleur de la lumière, et par la vapeur de la rosée, au temps de la chaude moisson.
18:5	Car avant la moisson, quand le bourgeon est à son terme et que la fleur devient un raisin qui mûrit, il coupera les rameaux avec des serpes, il enlèvera et retranchera les branches.
18:6	Ils seront tous ensemble abandonnés aux oiseaux de proie des montagnes et aux bêtes de la Terre. Les oiseaux de proie seront sur eux tout le long de l'été, et toutes les bêtes de la Terre y passeront l'hiver.
18:7	En ce temps-là, un présent sera apporté à YHWH Tsevaot, par le peuple robuste et vigoureux, de la part du peuple redoutable depuis là où il est et au-delà, nation puissante et qui foule aux pieds, et dont la terre est ravagée par ses fleuves. Il sera apporté dans la demeure du Nom de YHWH Tsevaot, sur la montagne de Sion.

## Chapitre 19

### Chute de l'Égypte

19:1	Fardeau de l'Égypte. Voici, YHWH est monté sur une nuée<!--Ez. 30:3 ; Na. 1:3 ; Lu. 21:27 ; Ap. 1:7.--> rapide, il entre en Égypte. Les faux elohîm de l'Égypte tremblent face à lui, et le cœur de l’Égypte se fond au milieu d'elle<!--Jé. 43:12.-->.
19:2	J’armerai l’Égypte contre l’Égypte et ils se battront, - l’homme contre son frère, l’homme contre son compagnon, ville contre ville, royaume contre royaume.
19:3	L'esprit de l'Égypte disparaîtra du milieu d'elle, et j'engloutirai son conseil. Ils consulteront les faux elohîm et les enchanteurs, ceux qui évoquent les morts et ceux qui ont un esprit de divination.
19:4	Je livrerai l'Égypte entre les mains d'un seigneur cruel. Un roi féroce dominera sur eux, – déclaration du Seigneur, YHWH Tsevaot.
19:5	Les eaux de la mer tariront, le fleuve séchera et tarira<!--Jé. 51:36.-->.
19:6	Les rivières deviendront puantes, les fleuves d'Égypte baisseront et se dessécheront, les roseaux et les joncs se flétriront.
19:7	Ce sera la nudité le long du fleuve, à l'embouchure du fleuve ! Tout ce qui aura été semé près du fleuve se desséchera, sera emporté : il n'y aura plus rien.
19:8	Et les pêcheurs gémiront, tous ceux qui jettent l'hameçon dans le fleuve mèneront deuil, et ceux qui étendent des filets de pêche sur les eaux languiront.
19:9	Ceux qui travaillent le lin peigné et ceux qui tissent des étoffes blanches seront confus.
19:10	Les fondements de la terre seront écrasés, et tous les travailleurs salariés l’âme attristée.
19:11	En effet, les chefs de Tsoan ne sont que des fous, les sages d'entre les conseillers de pharaon forment un conseil stupide. Comment osez-vous dire à pharaon : Je suis fils des sages, fils des anciens rois ?
19:12	Où sont-ils maintenant ? Où sont tes sages ? Qu'ils t'annoncent, s'il te plaît, s'ils le savent, ce que YHWH Tsevaot a conseillé contre l'Égypte.
19:13	Les chefs de Tsoan sont devenus fous, les chefs de Noph se sont trompés, ils ont fait errer l’Égypte, l’angle de ses tribus.
19:14	YHWH a mêlé au milieu d’elle l’esprit qui pervertit<!--1 R. 22:18-22.-->, et ils ont fait errer l’Égypte dans toute son œuvre, comme un homme ivre qui erre dans ce qu’il a vomi.
19:15	Et l'Égypte n'aura pas d'œuvre que puisse faire la tête et la queue, la branche de palmier et le roseau.

### L'Égypte et l'Assyrie dans le royaume du Mashiah

19:16	En ce jour-là, l'Égypte deviendra comme des femmes : elle tremblera et sera dans la crainte à cause de la main de YHWH Tsevaot, quand il élèvera la main contre elle.
19:17	Le sol de Yéhouda deviendra pour l'Égypte une terreur. Tous ceux auxquels on en fait mention sont dans la crainte, à cause du conseil de YHWH Tsevaot qu’il conseille contre elle.
19:18	En ce jour-là, il y aura cinq villes en terre d'Égypte, qui parleront la langue de Kena'ân<!--Canaan.-->, et qui jureront par YHWH Tsevaot : l'une sera appelée ville de la destruction.
19:19	En ce jour-là, il y aura un autel pour YHWH au milieu de la terre d'Égypte, et un monument dressé pour YHWH sur la frontière.
19:20	Elle deviendra un signe, un témoignage pour YHWH Tsevaot en terre d'Égypte. Car ils crieront à YHWH à cause des oppresseurs, il leur enverra un sauveur, quelqu'un de grand, et il les délivrera<!--Es. 43:11.-->.
19:21	YHWH se fera connaître à l’Égypte, et l’Égypte connaîtra YHWH en ce jour-là. Ils serviront au sacrifice et à l’offrande, ils feront des vœux à YHWH et les accompliront.
19:22	YHWH frappera l’Égypte, il frappera et guérira. Ils retourneront jusqu’à YHWH. Il se laissera implorer par eux et les guérira.
19:23	En ce jour-là, il y aura une grande route de l'Égypte en Assyrie. L'Assyrie viendra en Égypte, et l'Égypte en Assyrie, et l'Égypte servira avec l'Assyrie.
19:24	En ce jour-là, Israël sera, lui troisième, uni à l'Égypte et à l'Assyrie, et la bénédiction sera au milieu de la Terre.
19:25	YHWH Tsevaot les bénira, en disant : Bénis soient l'Égypte mon peuple, et l'Assyrie œuvre de mes mains, et Israël mon héritage !

## Chapitre 20

### Conquête de l'Égypte et de l'Éthiopie

20:1	L'année où Tharthan vint à Asdod, envoyé par Sargon, roi d'Assyrie, fit la guerre contre Asdod et la prit.
20:2	En ce temps-là, YHWH parla par la main de Yesha`yah, fils d'Amots, et lui dit : Va, délie le sac de dessus tes reins et ôte tes sandales de tes pieds. Il fit ainsi, marchant nu et pieds nus.
20:3	YHWH dit : Comme mon serviteur Yesha`yah marche nu et pieds nus pendant trois ans, en signe et prodige contre l'Égypte et contre l'Éthiopie,
20:4	ainsi le roi d'Assyrie emmènera de l'Égypte et de l'Éthiopie prisonniers et captifs, les jeunes et les vieux, nus et pieds nus, ayant les hanches découvertes, ce qui sera l'opprobre de l'Égypte<!--2 S. 10:4 ; Es. 3:17 ; Jé. 13:22-26.-->.
20:5	Ils seront effrayés et honteux à cause de l'Éthiopie, objet de leur espoir, et de l'Égypte, leur gloire.
20:6	Et les habitants de cette côte diront en ce jour-là : Voilà ce qu’est devenu l’objet de notre espoir, vers qui nous avons couru pour avoir du secours, pour être délivrés face au roi d'Assyrie ! Comment pourrons-nous échapper ?

## Chapitre 21

### Annonce de la conquête de Babel

21:1	Fardeau du désert de la mer. Comme les vents d'orage passent dans le midi, cela vient du désert, de la terre redoutable.
21:2	Une vision cruelle m'a été révélée. Le traître demeure traître, celui qui saccage, saccage. Monte, Éylam ! Assiège, Médie ! Je fais cesser tous les soupirs.
21:3	C'est pourquoi mes reins sont remplis de douleur. Les angoisses me saisissent comme les angoisses de celle qui enfante. Ce que j'entends me déforme, ce que je vois me terrifie.
21:4	Mon cœur est agité de toutes parts, la terreur s'empare de moi. La nuit de mes plaisirs devient une nuit de crainte.
21:5	On prépare la table, on étend les tapis, on mange et on boit. Levez-vous chefs ! Oignez le bouclier !
21:6	Car ainsi m'a parlé Adonaï : Va, place la sentinelle, et qu'elle rapporte ce qu'elle verra<!--Ez. 33:1-19.-->.
21:7	Et elle vit un char, un couple de cavaliers, un char tiré par des ânes, un char tiré par des chameaux, qu'elle prête attention, avec grande attention.
21:8	Elle cria : « Un lion ! » Sur la tour de garde, Adonaï, je me tiens debout constamment le jour, et toutes les nuits je suis à mon poste,
21:9	et voici venir le char d'un homme, un couple de cavaliers ! Elle répondit et dit : Elle est tombée, elle est tombée, Babel<!--Prophétie sur la chute de Babel. Voir Jé. 50-51 ; Ap. 18.-->, et toutes les images gravées de ses elohîm sont brisées par terre.
21:10	Toi qui as été battu comme du grain, fils de mon aire ! Je vous ai annoncé ce que j'ai entendu de YHWH Tsevaot, de l'Elohîm d'Israël.

### Prophétie sur Doumah

21:11	Fardeau de Doumah. On me crie de Séir : Gardien ! Qu'en est-il de la nuit ? Gardien ! Qu'en est-il de la nuit ?
21:12	Le Gardien dit : Le matin vient et la nuit aussi. Si vous voulez interroger, interrogez ! Convertissez-vous et venez !

### Jugement sur l'Arabie

21:13	Fardeau de l'Arabie. Vous passerez la nuit dans la forêt, en Arabie, caravanes de Dedan !
21:14	Les habitants de la terre de Théma portent de l'eau à ceux qui ont soif ! Ils viennent au-devant du fugitif avec du pain pour lui.
21:15	Car ils fuient devant les épées, devant l'épée dégainée, devant l'arc tendu, devant l'acharnement de la bataille.
21:16	Car ainsi m'a parlé Adonaï : Encore une année, comme les années d'un mercenaire, et toute la gloire de Qedar prendra fin.
21:17	Et le nombre des vaillants archers, fils de Qedar, qui seront restés diminuera, car YHWH, l'Elohîm d'Israël, a parlé.

## Chapitre 22

### Malédiction sur la vallée des visions, Yeroushalaim (Jérusalem)

22:1	Fardeau de la vallée de la vision. Qu’as-tu donc que tu sois montée tout entière sur les toits ?
22:2	Toi ville bruyante, pleine de tumulte, ville joyeuse ! Tes blessés mortellement ne sont pas blessés mortellement par l'épée, ils n'ont pas été tués à la guerre.
22:3	Tous tes chefs prennent la fuite ensemble, ils sont faits prisonniers par les archers. Tes habitants, tous ensemble, sont faits prisonniers tandis qu'ils s'enfuient au loin.
22:4	C'est pourquoi je dis : Détournez de moi vos regards, que je pleure amèrement. Ne vous empressez pas pour me consoler du désastre de la fille de mon peuple.
22:5	Car c'est le jour de trouble, d'oppression et de confusion<!--La. 1:5, 2:2.-->, envoyé par Adonaï YHWH Tsevaot, dans la vallée des visions. Il démolit la muraille et les cris retentissent jusqu'à la montagne.
22:6	Éylam porte le carquois dans le char de l’humain, des cavaliers. Kir découvre le bouclier.
22:7	Et il arrivera que les meilleures de tes vallées se rempliront de chars, et les cavaliers se rangeront en bataille à la porte.
22:8	La couverture de Yéhouda sera enlevée. Tu regarderas en ce jour-là vers les armes de la maison de la forêt.
22:9	Vous voyez que les brèches de la cité de David sont nombreuses et vous faites provision d'eau dans le réservoir inférieur.
22:10	Vous faites le dénombrement des maisons de Yeroushalaim, et vous démolissez les maisons pour fortifier la muraille.
22:11	Et vous faites aussi un réservoir d'eau entre les deux murailles, pour les eaux de l'ancien étang. Mais vous ne regardez pas à celui qui a fait ces choses, qui les a formées il y a longtemps.
22:12	Adonaï YHWH Tsevaot appellera en ce jour-là aux pleurs et aux gémissements, à la calvitie et à se ceindre d'un sac<!--Ez. 7:18 ; Joë. 1:13.-->.
22:13	Et voici il y a de la joie et de l'allégresse ! On tue des bœufs et l'on abat des moutons, on mange la viande et l'on boit du vin : Mangeons et buvons, car demain nous mourrons<!--Es. 56:12 ; 1 Co. 15:32.--> !
22:14	YHWH Tsevaot s'est découvert à mes oreilles : Non, la propitiation pour cette iniquité ne sera jamais faite en votre faveur jusqu'à ce que vous mouriez, a dit Adonaï YHWH Tsevaot.

### Élyakim succède à Shebna

22:15	Ainsi parle Adonaï YHWH Tsevaot : Va, entre chez ce trésorier, chez Shebna, qui est au-dessus de la maison.
22:16	Quoi, toi ici ? Qui, toi ici ? Oui, tu te creuses ici un sépulcre, on se creuse un sépulcre sur la hauteur, on se taille dans le rocher un tabernacle !
22:17	Voici que YHWH te jettera en captivité, homme fort ! Il t'enveloppera, il t'enveloppera.
22:18	Il t’enroulera, boule qui roule et roule vers une terre aux mains larges. Là tu mourras, là seront les chars de ta gloire, la honte de la maison de ton Seigneur !
22:19	Je te chasserai de ton poste, et on t'arrachera de ton service.
22:20	Et il arrivera en ce jour-là que j'appellerai mon serviteur Élyakim, fils de Chilqiyah.
22:21	Je le revêtirai de ta tunique, je le ceindrai de ta ceinture, et je remettrai ton autorité entre ses mains, il sera un père pour les habitants de Yeroushalaim et pour la maison de Yéhouda.
22:22	Je mettrai la clef de la maison de David sur son épaule : il ouvrira et personne ne fermera, il fermera et personne n’ouvrira<!--La clé de David est le symbole de l'autorité du Mashiah (Es. 9:5 ; Mt. 28:18 ; Ap. 3:7-8).-->.
22:23	Je l'enfoncerai comme un clou dans un lieu sûr, et il sera un trône de gloire pour la maison de son père.
22:24	On suspendra sur lui toute la gloire de la maison de son père, les descendants et les rejetons, tous les ustensiles de petite taille, les ustensiles, les bassins, depuis les ustensiles jusqu'aux outres.
22:25	En ce jour-là – déclaration de YHWH Tsevaot, le clou enfoncé dans un lieu sûr sera enlevé, il sera abattu et tombera, et le fardeau qui était sur lui sera retranché, car YHWH a parlé.

## Chapitre 23

### Effondrement de Tyr

23:1	Fardeau de Tyr. Hurlez, navires de Tarsis ! Car elle est détruite, il n'y a plus de maisons, on n'y entre plus ! Ceci leur a été révélé de la terre de Kittim.
23:2	Habitants de l'île, taisez-vous ! Toi qui étais remplie de marchands de Sidon, et de ceux qui traversaient la mer !
23:3	À travers les grandes eaux, les grains de Shiychor, la moisson du Nil était pour elle son revenu ; elle était le marché des nations<!--Ez. 27.-->.
23:4	Sois honteuse, Sidon ! Car la mer, la forteresse de la mer, a parlé, en disant : Je n'ai pas eu de douleurs, je n'ai pas enfanté, je n'ai pas nourri de jeunes hommes ni élevé aucune vierge.
23:5	Tout comme à la nouvelle concernant l'Égypte, on sera de même saisi de douleurs à la nouvelle sur Tyr.
23:6	Passez à Tarsis, lamentez-vous, habitants de l'île !
23:7	Est-ce là votre exultation, qui avait son origine dès les jours d’autrefois ? Ses propres pieds la porteront au loin pour séjourner.
23:8	Qui a donné ce conseil contre Tyr qui donnait des couronnes ? Ses marchands étaient des princes, ses trafiquants des honorables de la Terre<!--Ap. 18:9-18.-->.
23:9	C'est YHWH Tsevaot lui-même qui a donné ce conseil, pour profaner l’orgueil de toute beauté, pour avilir tous les honorables de la Terre.
23:10	Traverse ta terre comme une rivière, fille de Tarsis ! Il n'y a plus de ceinture.
23:11	Il a étendu sa main sur la mer, il a fait trembler les royaumes. YHWH a ordonné la destruction des forteresses de Kena'ân.
23:12	Il a dit : Tu ne te livreras plus à la joie, vierge opprimée, fille de Sidon ! Lève-toi, passe à Kittim ! Même là, il n'y aura pas de repos pour toi.
23:13	Voici la terre des Chaldéens, ce peuple n'existait pas : Assour<!--Assour : le second fils de Shem (Ge. 10:22). L'ancêtre des Assyriens.--> l'a fondé pour les gens du désert. On a dressé ses forteresses, on a élevé ses palais, et il l'a mis en ruine.
23:14	Hurlez, navires de Tarsis ! Car votre force est détruite !
23:15	Il arrivera en ce jour-là que Tyr sera oubliée pendant 70 ans, comme les jours d'un roi. Au bout de 70 ans<!--Jé. 25:11-12.-->, il arrivera à Tyr selon la chanson de la prostituée :
23:16	Prends la harpe, fais-le tour de la ville, prostituée qu'on oublie ! Joue bien de ton instrument, multiplie tes chants afin qu'on se souvienne de toi !
23:17	Il arrivera à la fin de 70 ans que YHWH visitera Tyr, mais elle retournera à son salaire de prostituée, et elle se prostituera avec tous les royaumes de la Terre, sur les faces de la Terre.
23:18	Mais son gain et son salaire de prostituée seront consacrés à YHWH. Ils ne seront ni emmagasinés ni conservés, car son gain sera pour ceux qui habitent en présence de YHWH pour qu'ils mangent à satiété et pour la couverture splendide.

## Chapitre 24

### Désastre après l'invasion babylonienne 

24:1	Voici, YHWH va rendre la Terre vide et la dévaster, il en renversera les faces et dispersera ses habitants<!--Ge. 11:1-8.-->.
24:2	Il en sera du peuple comme du prêtre, du serviteur comme de son maître, de la servante comme de sa maîtresse, de l’acheteur comme du vendeur, du prêteur comme de l’emprunteur, du débiteur comme du créancier.
24:3	La Terre est dévastée, elle est dévastée et pillée, pillée, car YHWH a prononcé cet arrêt.
24:4	La Terre pleure, elle se fane. Le monde languit et se fane. Ceux qui sont élevés parmi le peuple de la Terre languissent.
24:5	La Terre a été profanée sous ceux qui l'habitent parce qu'ils ont transgressé la torah, changé les ordonnances et rompu l'alliance éternelle<!--Da. 7:25.-->.
24:6	C'est pourquoi la malédiction dévore la Terre, et ses habitants sont incriminés. C'est pourquoi les habitants de la Terre sont brûlés et il ne reste que peu de mortels.
24:7	Le vin nouveau pleure, la vigne languit, et tous ceux qui avaient le cœur joyeux soupirent.
24:8	La joie des tambours a cessé, le vacarme de ceux qui s'égayent a pris fin, la joie de la harpe a cessé.
24:9	On ne boit plus de vin en chantant, les boissons fortes sont amères à ceux qui les boivent.
24:10	La ville du tohu est brisée. Toutes les maisons sont fermées, on n'y entre plus.
24:11	On crie dans les rues pour du vin. Toute la joie est tournée en obscurité, l'allégresse de la Terre s'en est allée.
24:12	La désolation est restée dans la ville et la porte est frappée d'une ruine éclatante.
24:13	Car il en sera sur Terre parmi les peuples, comme quand on secoue l'olivier, comme quand on grappille après la vendange.

### Un reste de réchappés célèbre YHWH

24:14	Ils élèvent leur voix, ils se réjouissent avec chant de triomphe. Du côté de la mer, ils célèbrent la majesté de YHWH.
24:15	C'est pourquoi glorifiez YHWH dans les vallées, le Nom de YHWH, l'Elohîm d'Israël, dans les îles de la mer !
24:16	Depuis les extrémités de la Terre nous entendons des chants : Gloire au juste ! Mais moi je dis : Maigreur sur moi ! Maigreur sur moi ! Malheur à moi ! Les traîtres trahissent, les traîtres ont agi avec tromperie en trahissant.

### Manifestation des jugements de YHWH

24:17	La terreur, la fosse et le piège sont sur toi, habitant de la Terre !
24:18	Et il arrivera que celui qui fuit au bruit de la terreur tombera dans la fosse, et celui qui remonte du milieu de la fosse sera pris dans le piège, car les écluses d'en haut s'ouvrent et les fondements de la Terre tremblent.
24:19	La Terre se brise, elle se brise ! La Terre se rompt, elle se rompt ! La Terre chancelle, elle chancelle !
24:20	La Terre tremble, elle tremble comme un homme ivre, elle vacille comme une hutte. Sa transgression pèse sur elle, elle tombe et ne se relève plus.
24:21	Et il arrivera en ce jour-là, que YHWH punira dans le lieu élevé l'armée d'en haut, et sur le sol les rois du sol.
24:22	Ils seront rassemblés dans une fosse au rassemblement des prisonniers, ils seront enfermés dans une prison et, après un grand nombre de jours, ils seront visités.
24:23	La lune rougira et le soleil sera honteux quand YHWH Tsevaot régnera sur la montagne de Sion et à Yeroushalaim, resplendissant de gloire en présence de ses anciens<!--Mt. 24:29-30 ; 2 Pi. 3:10-12 ; Ap. 6:12.-->.

## Chapitre 25

### Le Royaume de YHWH

25:1	YHWH, tu es mon Elohîm, je t'exalterai, je célébrerai ton Nom, car tu as fait des choses merveilleuses. Tes conseils conçus d'avance se sont fidèlement accomplis.
25:2	Car tu as fait de la ville un monceau de pierres, et de la cité forte une ruine. Le palais des étrangers qui était dans la ville ne sera jamais rebâti.
25:3	C'est pourquoi des peuples puissants te glorifient, la ville des nations terrifiantes te craint.
25:4	Car tu es devenu la forteresse pour le faible, la forteresse pour le pauvre dans sa détresse, le refuge contre la tempête, l'ombrage contre la chaleur. Car le souffle des terrifiants est comme une tempête contre un mur.
25:5	Comme la chaleur sur un sol desséché, tu rabaisses le vacarme des étrangers. Comme la chaleur sous l'ombre d'un nuage, le chant des terrifiants a été apaisé.
25:6	YHWH Tsevaot prépare pour tous les peuples sur cette montagne un banquet de choses grasses, un banquet de vins vieux, de choses grasses et moelleuses, et de vins vieux bien purifiés.
25:7	Il détruira sur cette montagne la couverture qui enveloppe les faces de tous les peuples, et le voile qui est étendu sur toutes les nations.
25:8	Il engloutira la mort par sa victoire. Adonaï YHWH essuiera les larmes de tous les visages, et il ôtera l'insulte de son peuple de toute la Terre, car YHWH a parlé.
25:9	Et l'on dira en ce jour-là : Voici notre Elohîm ! Celui en qui nous avons espéré et qui nous sauve : c'est YHWH, en qui nous avons espéré. Soyons dans l'allégresse, et réjouissons-nous de son salut !
25:10	Car la main de YHWH reposera sur cette montagne et Moab sera foulé aux pieds sous lui, comme on foule la paille pour en faire du fumier.
25:11	Il étendra ses mains au milieu d'eux, comme le nageur étend ses mains pour nager. Mais il abaissera son orgueil, ainsi que l'artifice de ses mains.
25:12	Il abattra, il renversera les fortifications inaccessibles de tes murs, il les jettera à terre jusque dans la poussière.

## Chapitre 26

### Adoration à YHWH

26:1	En ce jour-là, ce cantique sera chanté en terre de Yéhouda : Nous avons une ville forte. Il y met le salut<!--Le mot salut vient du mot « Yeshuw'ah ». Cette même racine a donné le prénom Yéhoshoua (Jésus ou Josué) qui signifie YHWH sauve. Yéhoshoua (Jésus) est notre muraille et notre rempart. Dans Ex. 15:2, Moshé (Moïse) identifie YHWH à « Yeshuw'ah » c'est-à-dire à Yéhoshoua (Jésus). Dans 1 Ch. 16:23, il est dit que « Yeshuw'ah » doit être annoncé tous les jours. Dans Ps. 62:2, il est présenté comme Elohîm et le Rocher. Dans Es. 12:2, il est l'Elohîm qui sauve. Yaacov et David avaient mis en lui leur espoir (Ge. 49:18 ; Ps. 119:166). Dans Es. 49:6, il est dit que le salut (« Yeshuw'ah ») doit être annoncé aux extrémités de la terre, et cela est répété et confirmé en Mt. 28:18-20. Es. 56:1 nous apprend que celui qui vient s'appelle « Yeshuw'ah ». Es. 59:17 le présente comme notre casque, ce qui fait écho au casque du salut en Ep. 6:17. Les murs de la nouvelle Yeroushalaim (Jérusalem) portent son Nom (Es. 60:18). Ha. 3:8 nous dit que « Yeshuw'ah » montera sur ses chevaux, corroborant le récit de son retour en gloire dans Ap. 19:11-20. « Yeshuw'ah » est notre flambeau selon Es. 62:1 et Ap. 21:23.--> pour muraille et pour rempart.
26:2	Ouvrez les portes, et la nation juste, celle qui garde la fidélité, y entrera.
26:3	Tu gardes dans la paix, dans la paix<!--Voir commentaire en Ge. 2:16-17.-->, celui dont les desseins s'appuient sur toi, parce qu'il se confie en toi<!--Es. 57:19 ; Ph. 4:6-7.-->.
26:4	Confiez-vous en YHWH à perpétuité, car Yah, YHWH est le Rocher<!--Voir commentaire en Es. 8:13-14.--> éternel.
26:5	Car il a abattu ceux qui habitaient les hauteurs, il a abaissé la ville inaccessible, il l'a abaissée jusqu'à terre, il lui a fait toucher la poussière.
26:6	Le pied la piétinera, les pieds des pauvres, sous les pas des faibles.
26:7	Le sentier du juste, c'est la droiture. Toi qui es juste, tu aplanis la route du juste.
26:8	Aussi t'avons-nous attendu YHWH dans le sentier de tes jugements ! Ton Nom et ton souvenir sont le désir de notre âme.
26:9	Pendant la nuit, mon âme te désire, et dès le point du jour, mon esprit qui est en moi te cherche, car, lorsque tes jugements s'exercent sur la Terre, les habitants du monde apprennent la justice.
26:10	Si l'on fait grâce au méchant, il n'apprend pas la justice, mais il agira méchamment en terre de la droiture et il ne regardera pas à la majesté de YHWH.
26:11	YHWH, ta main est levée et ils ne le voient pas ! Ils verront ta jalousie pour le peuple et seront honteux, dévorés par le feu destiné à tes ennemis.
26:12	YHWH, tu mets en nous la paix, car toutes nos œuvres, c'est toi qui les accomplis pour nous.
26:13	YHWH, notre Elohîm, d'autres seigneurs, à part toi, ont dominé sur nous ; c'est par toi seul que nous pouvons faire mention de ton Nom.
26:14	Les morts ne vivront pas, les fantômes<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».--> ne se lèveront pas, car tu les as châtiés, exterminés, tu as fait périr tout souvenir d'eux<!--Ec. 9:5.-->.
26:15	Tu as augmenté la nation, YHWH ! Tu as augmenté la nation, tu t'es glorifié, tu as reculé toutes les limites de la terre.

### Un reste épargné de la colère de YHWH

26:16	YHWH, quand ils étaient dans la détresse, ils t'ont cherché, ils ont répandu leurs chuchotements quand ton châtiment était sur eux.
26:17	Comme une femme enceinte qui est près d'accoucher se tord et crie dans ses douleurs, ainsi avons-nous été en face de toi YHWH !
26:18	Nous avons conçu, nous avons été dans les douleurs de l'accouchement et, quand nous accouchons, ce n'est que du vent. Nous n'avons pas accompli le salut de la Terre, et les habitants du monde ne sont pas tombés.
26:19	Tes morts vivront ! Mes cadavres se lèveront ! Réveillez-vous et réjouissez-vous avec des chants de triomphe, vous, habitants de la poussière ! Car ta rosée est comme la rosée de lumière, et la terre jettera dehors les fantômes<!--Voir Es. 14:9. Os. 13:14 ; Da. 12:2 ; 1 Co. 15:52.-->.
26:20	Va, mon peuple, entre dans tes chambres et ferme ta porte derrière toi<!--Mt. 6:6.--> ! Cache-toi pour un petit moment, jusqu'à ce que la colère soit passée.
26:21	Car voici, YHWH sort de son lieu pour punir l'iniquité des habitants de la Terre<!--Voir Mt. 25:31-46.-->, et la Terre découvrira ses sangs et ne couvrira plus ses tués.

## Chapitre 27

### Israël rétabli

27:1	En ce jour-là, YHWH punira de sa dure, grande et puissante épée le Léviathan<!--Ps. 104:26 ; Job 40:25.-->, le serpent fuyard, le Léviathan, le serpent tortueux, et il tuera le monstre marin qui est dans la mer.
27:2	En ce jour-là, la vigne agréable<!--Yesha`yah annonce ici le rétablissement d'Israël. Voir également Ro. 11:1-24.-->, chantera pour Lui.
27:3	C'est moi YHWH qui la garde, je l'arrose à chaque instant. De peur qu’on ne la visite, je la garde nuit et jour.
27:4	Il n'y a pas de fureur en moi. Qu'on me donne des ronces, des épines pour les combattre ! Je marcherai contre elles, je les brûlerai toutes ensemble,
27:5	à moins qu'on n'ait recours à ma protection, qu'on fasse la paix avec moi, qu'on fasse la paix avec moi.
27:6	Dans l'avenir Yaacov prendra racine, Israël fleurira et s'épanouira et remplira les faces du monde de ses fruits.
27:7	L'a-t-il frappé comme il a frappé ceux qui le frappaient ? L'a-t-il tué comme il a tué ceux qui le tuaient ?
27:8	C'est avec mesure que tu l'as combattu en le rejetant, lorsqu'il fut emporté par ton vent sévère, un jour du vent d'orient.
27:9	C'est pourquoi la propitiation de l'iniquité de Yaacov sera faite par ce moyen, et le fruit de tout cela sera l'éloignement de son péché : quand il aura transformé toutes les pierres des autels comme des pierres de chaux réduites en poussière, lorsque les asherah et les statues consacrées au soleil ne seront plus debout.
27:10	Car la ville fortifiée est solitaire, la demeure agréable est abandonnée et délaissée comme un désert. Le veau en a fait sa pâture, il s'y couche et broute les branches.
27:11	Quand ses rameaux se dessèchent, on les brise et des femmes viennent y mettre le feu. Car c'est un peuple sans discernement<!--De. 32:28 ; Es. 1:3.-->, c'est pourquoi celui qui l'a fait n'a pas eu pitié de lui et celui qui l'a formé ne lui a pas fait grâce.
27:12	Il arrivera en ce jour-là que YHWH secouera, depuis le cours du fleuve jusqu'au torrent d'Égypte, mais vous serez glanés un à un, fils d'Israël !
27:13	Il arrivera, en ce jour-là, qu'il sonnera du grand shofar, alors viendront les perdus en terre d'Assyrie, les bannis en terre d'Égypte, ils se prosterneront devant YHWH, sur la sainte montagne, à Yeroushalaim.

## Chapitre 28

### Malheur et captivité d'Éphraïm en Assyrie

28:1	Malheur à la couronne orgueilleuse des ivrognes d'Éphraïm, dont la glorieuse beauté n'est qu'une fleur fanée sur le sommet de la vallée fertile, à ceux qui sont frappés par le vin !
28:2	Voici le fort et le puissant d'Adonaï, semblable à une tempête de grêle, à un orage de destruction, à une tempête d'eaux puissantes qui débordent : il le repose à terre à la main.
28:3	Elle sera foulée aux pieds, la couronne orgueilleuse des ivrognes d'Éphraïm.
28:4	Et la fleur fanée de sa glorieuse beauté sur le sommet de la vallée fertile deviendra comme un fruit précoce avant l’été : dès que celui qui l’aperçoit l’a vu, à peine il est dans sa paume, il l’avale.
28:5	En ce jour-là, YHWH Tsevaot deviendra la couronne de beauté et le diadème de gloire pour le reste de son peuple,
28:6	et l'Esprit de jugement pour celui qui siège au jugement, et la force de ceux qui dans le combat repousseront l'ennemi jusqu'à la porte.
28:7	Mais eux aussi se sont égarés à cause du vin, et ils chancellent à cause des boissons fortes. Le prêtre et le prophète se sont égarés à cause des boissons fortes. Ils sont engloutis par le vin, ils chancellent à cause des boissons fortes, ils s'égarent dans leur vision, ils vacillent dans le jugement.
28:8	Car toutes leurs tables sont couvertes de vomissements et d'excréments, si bien qu'il n'y a plus de place !
28:9	À qui enseigne-t-on la connaissance ? À qui fait-on comprendre l'enseignement ? Est-ce à ceux qu'on vient de sevrer du lait et de retirer de la mamelle ?
28:10	Car c'est commandement sur commandement, commandement sur commandement, règle sur règle, règle sur règle, un peu ici, un peu là<!--Hé. 5:12.-->.
28:11	C'est pourquoi, il parlera à ce peuple par des lèvres qui balbutient et par une langue étrangère<!--Voir Ac. 2:1-47.-->.
28:12	Il leur disait : Voici le repos ! Donnez du repos à celui qui est épuisé ! Voici le soulagement ! Mais ils n'ont pas voulu écouter.
28:13	Ainsi la parole de YHWH deviendra pour eux commandement sur commandement, commandement sur commandement, règle sur règle, règle sur règle, un peu ici, un peu là, afin qu’en marchant ils tombent à la renverse et se brisent, afin qu'ils tombent dans le piège et qu’ils soient capturés.

### YHWH rompt le pacte du shéol par une pierre angulaire

28:14	C'est pourquoi écoutez la parole de YHWH, vous hommes moqueurs, qui dominez sur ce peuple qui est à Yeroushalaim !
28:15	Car vous dites : Nous avons fait un pacte avec la mort et avec le shéol, nous avons réalisé une vision : quand le fléau qui déborde passera, il ne nous atteindra pas, car nous avons le mensonge pour refuge et nous nous sommes cachés sous la fausseté.
28:16	C'est pourquoi ainsi parle Adonaï YHWH : Voici, j'établirai en Sion une pierre<!--Voir commentaire en Es. 8:13-16.-->, une pierre éprouvée, angulaire, précieuse, une fondation établie. Celui qui croira ne se hâtera pas.
28:17	Je placerai le jugement comme une règle et la justice comme un fil à plomb. La grêle détruira le refuge du mensonge et les eaux submergeront votre abri.
28:18	Et votre pacte avec la mort sera détruit, votre vision avec le shéol ne tiendra pas. Quand le fléau qui déborde passera, vous serez piétinés par lui.
28:19	Chaque fois qu'il passera, il vous emportera, car matin après matin il passera, pendant le jour et pendant la nuit. Ce sera la terreur que d'en comprendre la rumeur.
28:20	Car le lit sera trop court pour s'y étendre, et la couverture trop étroite pour s'en envelopper.
28:21	Car YHWH se lèvera comme à la montagne de Peratsim, et il sera ému comme dans la vallée de Gabaon, pour faire son œuvre, son œuvre étrange, et pour faire son service, son service étranger.
28:22	Maintenant, ne vous moquez pas, de peur que vos liens ne soient renforcés ! Car j'ai entendu que la destruction est décretée par Adonaï YHWH Tsevaot, contre toute la Terre.
28:23	Prêtez l'oreille et écoutez ma voix ! Soyez attentifs et écoutez mon discours !
28:24	Est-ce tout le jour que le laboureur laboure pour semer, qu’il ouvre et herse son sol ?
28:25	Quand il en a aplani la surface, n'y répand-il pas le cumin noir<!--Plante à petites graines noires utilisées comme condiment.--> et n'y jette-t-il pas le cumin ? Ne met-il pas le blé par rangées, l'orge à une place marquée et l'épeautre<!--L'épeautre est une espèce de blé.--> sur les bords ?
28:26	Parce que son Elohîm l'a instruit, et lui a enseigné ce qu'il faut faire.
28:27	Car on ne foule pas le cumin noir avec la herse<!--La herse est un instrument agricole permettant de travailler la terre en surface.--> et on ne tourne pas la roue du chariot sur le cumin, mais on bat le cumin noir avec la verge et le cumin avec le bâton.
28:28	Écrase-t-on le pain ? Celui qui bat le blé ne le bat pas indéfiniment : il y pousse la roue et les chevaux, mais il ne l’écrase pas.
28:29	Cela aussi vient de YHWH Tsevaot qui est merveilleux en conseil et grand en sagesse.

## Chapitre 29

### Avertissement d'un châtiment imminent

29:1	Malheur à Ariel<!--Ariel : « Lion de El », nom appliqué à Yeroushalaim (Jérusalem).--> ! Ariel, ville où campa David ! Ajoutez année sur année, que les fêtes accomplissent leur cycle.
29:2	Je mettrai Ariel à l'étroit. Il n'y aura que tristesse et deuil, et elle deviendra pour moi comme Ariel.
29:3	Je camperai en cercle contre toi, je t'assiégerai avec des tours et je dresserai contre toi des retranchements.
29:4	Tu seras abaissée, tu parleras depuis la terre, ta parole sortira étouffée par la poussière. Ta voix sortira de terre comme l'esprit d'un mort, et ta parole sera comme un murmure sortant de la poussière.
29:5	La multitude de tes étrangers sera comme une fine poussière, la multitude des terrifiants sera comme la balle qui passe, et cela sera pour un petit moment.
29:6	Elle sera visitée par YHWH Tsevaot avec des tonnerres, des tremblements de terre, et un grand bruit<!--Za. 14:13-14 ; Ap. 16:18-19.-->, avec le vent d'orage, le tourbillon et avec la flamme d'un feu dévorant.
29:7	Et comme il en est d'un rêve, d'une vision de la nuit, ainsi en sera-t-il de la multitude des nations qui combattront Ariel, de tous ceux qui l'attaqueront, elle et sa forteresse, et qui la serreront de près.
29:8	Il en sera comme d'un homme qui a faim rêve qu'il mange, mais quand il se réveille son âme est vide, ou comme un homme qui a soif rêve qu'il boit, mais quand il se réveille, il est épuisé et son âme est altérée. Il en sera de même pour la multitude de toutes les nations qui combattront contre la montagne de Sion.

### YHWH donne les raisons du châtiment

29:9	Arrêtez-vous et soyez étonnés ! Bouchez les yeux et soyez aveugles ! Ils sont ivres, mais non de vin, ils chancellent, mais non pas à cause des boissons fortes.
29:10	Car YHWH a répandu sur vous un esprit de profond sommeil<!--Ro. 11:8.-->, il a fermé vos yeux, prophètes, et couvert vos têtes, voyants.
29:11	Toute vision devient pour vous comme les paroles d'un livre scellé que l'on donne à quelqu’un qui connaît le livre, en disant : S'il te plaît, lis cela ! Il dit : Je ne peux pas, car il est scellé.
29:12	On donne le livre à quelqu’un qui ne connaît pas le livre, en disant : S'il te plaît, lis cela ! Il dit : Je ne connais pas le livre.
29:13	C'est pourquoi Adonaï dit : Parce que ce peuple s'approche de moi de sa bouche et qu'il m'honore de ses lèvres, mais que son cœur est éloigné de moi et que la crainte qu'il a de moi n'est qu'un commandement enseigné par des humains<!--Mt. 15:8-9 ; Mc. 7:6-7.-->,
29:14	à cause de cela, voici, je continuerai à faire à l'égard de ce peuple des merveilles, des merveilles et des miracles, et la sagesse de ses sages périra et le discernement de ceux qui discernent se cachera<!--1 Co. 1:19.-->.
29:15	Malheur à ceux qui cachent profondément leurs desseins pour les dérober à YHWH, et dont les œuvres se font dans les ténèbres, et qui disent : Qui nous voit, et qui nous connaît<!--Es. 47:10 ; Ez. 8:12 ; Ps. 10:11, 94:7.--> ?
29:16	Quelle perversité ! Le potier doit-il être considéré comme l'argile, pour que l'œuvre dise de celui qui l'a faite : Il ne m'a pas faite, pour que le pot dise de son potier : Il n'y connaît rien<!--Ps. 100:3.--> ?

### YHWH rachète Yaacov

29:17	N’est-ce pas encore un peu, très peu de temps, et le Liban redeviendra un verger, et le verger sera compté comme forêt ?
29:18	En ce jour-là, les sourds entendront les paroles du livre et, de l’obscurité, des ténèbres, les yeux des aveugles verront<!--Mt. 11:5 ; Lu. 7:22.-->.
29:19	Les humbles auront joie sur joie en YHWH et les pauvres d'entre les humains se réjouiront dans le Saint d'Israël<!--Mt. 5:3-11.-->.
29:20	Car le terrifiant arrivera à sa fin, le moqueur sera consumé, et tous ceux qui veillaient pour commettre l'iniquité seront retranchés<!--Ap. 20:10.-->,
29:21	ceux qui font pécher les humains par la parole, qui tendent des pièges à celui qui les reprend à la porte, et qui font tomber le juste dans le tohu.
29:22	C'est pourquoi ainsi parle YHWH à la maison de Yaacov, lui qui a racheté Abraham : Maintenant Yaacov ne sera plus honteux, et maintenant ses faces ne blanchiront plus.
29:23	Oui, en voyant ses enfants, l’œuvre de mes mains, au milieu de lui, ils sanctifieront mon Nom, ils sanctifieront le Saint de Yaacov, et ils craindront l'Elohîm d'Israël.
29:24	Les esprits égarés connaîtront le discernement, et ceux qui murmuraient apprendront l'enseignement.

## Chapitre 30

### Mise en garde contre les alliances étrangères

30:1	Malheur aux fils rebelles ! – déclaration de YHWH. Ils prennent conseil, mais non de moi, ils se forgent<!--Qui versent des libations avec sacrifice d'alliance.--> des idoles de métal où mon Esprit n'est pas, afin d'ajouter péché sur péché.
30:2	Qui vont pour descendre en Égypte sans avoir interrogé ma bouche, pour se réfugier sous la protection de pharaon et se retirer sous l'ombre de l'Égypte<!--Jé. 42:19.-->.
30:3	La protection de pharaon deviendra pour vous une honte, et le refuge sous l'ombre de l'Égypte votre confusion.
30:4	Car ses princes sont à Tsoan, et ses messagers ont atteint Hanès.
30:5	Tous seront couverts de honte par un peuple qui ne leur apportera aucun profit, ils n'en recevront aucun secours ni aucun profit, il sera leur honte et leur insulte.
30:6	Les bêtes sont chargées pour aller au midi, ils portent leurs richesses sur le dos des ânons, et leurs trésors sur la bosse des chameaux, vers le peuple qui ne leur profitera pas en terre de détresse et d'angoisse, d'où viennent la lionne et le lion, la vipère et le serpent volant.
30:7	Le secours de l'Égypte n'est que vanité et néant, c’est pourquoi j’ai appelé cela : « L'arrogance qui reste tranquille ».
30:8	Va maintenant, écris-le sur une tablette avec eux, grave-le dans un livre, afin que ce soit, pour les jours à venir, un témoignage pour toujours et à jamais.
30:9	Car c'est un peuple rebelle, des enfants menteurs, des enfants qui ne veulent pas écouter la torah de YHWH<!--No. 20:3-5 ; De. 9:7 ; Ac. 7:51.-->,
30:10	qui disent aux voyants : Ne voyez pas ! Et aux prophètes : Ne nous prophétisez pas des vérités ! Dites-nous des choses flatteuses, prophétisez des illusions<!--2 Ti. 4:3-4 ; Mi. 2:6.--> !
30:11	Retirez-vous du chemin, détournez-vous du sentier, éloignez de notre présence le Saint d'Israël<!--Jn. 14:6.-->.
30:12	C'est pourquoi ainsi parle le Saint d'Israël : Parce que vous rejetez cette parole et que vous vous confiez dans l'oppression et dans les détours, et que vous vous êtes appuyés sur ces choses,
30:13	à cause de cela, cette iniquité deviendra pour vous comme une brèche qui tombe, un renflement dans un mur élevé, dont la ruine vient soudainement, en un instant.
30:14	Il la brise comme on brise un vase de terre que l'on n'épargne pas, et de ses pièces, il ne se trouve pas un tesson pour prendre du feu au foyer ou pour puiser de l'eau à la citerne.

### La confiance en YHWH, la vraie force

30:15	Car ainsi a parlé Adonaï YHWH, le Saint d'Israël : C'est dans le retrait et le repos que vous serez sauvés, c'est dans la tranquillité et la confiance que sera votre force. Mais vous ne l'avez pas voulu !
30:16	Vous avez dit : Non ! Mais nous nous enfuirons à cheval ! À cause de cela vous vous enfuirez ! Nous monterons sur des chevaux rapides ! À cause de cela ceux qui vous poursuivront seront rapides.
30:17	Vous serez un millier face à la menace d'un seul, et face à la menace de cinq vous prendrez la fuite, jusqu'à ce que vous ne soyez plus qu'un reste semblable à un mât de drapeau au sommet d'une montagne, comme une bannière sur la colline.
30:18	C’est pourquoi YHWH attend pour vous faire grâce, c'est pourquoi il se lèvera pour vous faire miséricorde. Car YHWH est l'Elohîm de jugement : heureux sont tous ceux qui se confient en lui !
30:19	Car le peuple demeurera dans Sion et dans Yeroushalaim. Tu ne pleureras plus ! Tu ne pleureras plus ! Il aura pitié de toi, il aura pitié de toi dès qu'il entendra ton cri ; dès qu'il aura entendu, il te répondra.
30:20	Adonaï vous donnera du pain de détresse et de l'eau d'angoisse. Ceux qui t'enseignent ne se cacheront plus et tes yeux verront ceux qui t'enseignent.
30:21	Et tes oreilles entendront la parole de celui qui sera derrière toi, disant : Voici le chemin, marchez-y ! Quand vous irez à droite ou quand vous irez à gauche.
30:22	Et vous tiendrez pour impurs les placages d'argent de vos images gravées et les éphods d'or de vos images de fonte. Tu les jetteras au loin comme une indisposition menstruelle et tu leur diras : Hors d'ici !
30:23	Il donnera la pluie pour ta semence avec laquelle tu ensemenceras le sol, et le pain que produira le sol sera gras et riche. En ce jour-là, ton bétail paîtra dans un pâturage spacieux<!--Jn. 14:6.-->.
30:24	Les bœufs et les ânes qui labourent le sol mangeront un fourrage assaisonné, vanné avec la pelle à vanner et le van.
30:25	Et il y aura des ruisseaux d'eau courante sur toute haute montagne, et sur toute colline haut élevée, au jour de la grande tuerie, quand les tours tomberont.
30:26	La lumière de la lune deviendra comme la lumière du soleil, et la lumière du soleil deviendra sept fois semblable à la lumière de sept jours, le jour où YHWH bandera la blessure de son peuple et guérira la blessure de sa plaie.

### Jugement de YHWH sur les Assyriens

30:27	Voici, le Nom de YHWH vient de loin. Sa colère brûle, elle s'élève avec véhémence. Ses lèvres sont pleines d'indignation, et sa langue est comme un feu dévorant.
30:28	Son Esprit est comme un torrent qui déborde et atteint jusqu'au cou, pour remuer çà et là les nations avec le crible du néant, et il y aura aux mâchoires des peuples une bride qui les fera s'égarer.
30:29	Le cantique sera pour vous comme dans la nuit où se consacre la fête, la joie du cœur comme celle du marcheur à la flûte, pour aller à la montagne de YHWH, vers le Rocher d'Israël.
30:30	YHWH fera entendre la majesté de sa voix, il fera voir la descente de son bras, dans la fureur de sa colère, au milieu de la flamme d'un feu dévorant, de l'orage qui emporte tout, de la tempête et des pierres de grêle.
30:31	Car l'Assyrien, qui frappait du bâton, sera effrayé par la voix de YHWH.
30:32	Chaque passage de ce bâton deviendra une fondation ; YHWH le fera reposer sur lui au son des tambourins et de harpes et, en agitant la main, il combattra contre lui.
30:33	Car Topheth<!--Topheth : lieu pour brûler. Un lieu à l'extrémité sud-est de la vallée de Hinnom au sud de Yeroushalaim (Jérusalem).--> est déjà préparé, il est même prêt pour le roi ! On a fait son bûcher profond et large. Son bûcher c'est du feu et du bois en abondance. Le souffle de YHWH l'allume comme un torrent de soufre.

## Chapitre 31

### Le secours de YHWH préférable à celui de l'Égypte

31:1	Malheur à ceux qui descendent en Égypte pour avoir de l'aide, qui s'appuient sur les chevaux et mettent leur confiance dans leurs chars parce qu'ils sont nombreux, et dans leurs cavaliers parce qu'ils sont très forts, mais qui ne regardent pas vers le Saint d'Israël, et ne cherchent pas YHWH.
31:2	Lui aussi, il est sage. Il fait venir le malheur et ne révoque pas sa parole. Il s'élève contre la maison des méchants et contre l'aide de ceux qui pratiquent la méchanceté.
31:3	Les Égyptiens sont des humains et non El, leurs chevaux sont chair et non esprit. YHWH étendra sa main, et celui qui donne du secours trébuchera, celui à qui le secours est donné tombera, et eux tous ensemble seront consumés.
31:4	Oui, ainsi m'a parlé YHWH : Quand le lion ou le lionceau rugit sur sa proie, quand on appelle contre lui une multitude des bergers, il n'a pas peur de leur voix, il ne s'occupe pas de leur tumulte. C'est ainsi que YHWH Tsevaot descendra pour combattre sur la montagne de Sion, sur sa colline<!--Za. 14:1-4.-->.
31:5	Comme des oiseaux qui volent, ainsi YHWH Tsevaot<!--YHWH des armées.--> défendra Yeroushalaim. Il la protégera et la sauvera, il passera au-dessus d'elle et la délivrera<!--De. 32:11 ; Ps. 91:4 ; Mt. 23:37.-->.
31:6	Retournez vers celui de qui les fils d'Israël se sont profondément détournés.
31:7	Car en ce jour-là ils rejetteront chacun ses faux elohîm d'argent et ses faux elohîm d'or, qu'ont fabriqué pour vous vos mains pécheresses.
31:8	L'Assyrien tombera par une épée qui n’est pas d’un homme, et une épée qui n’est pas d’un être humain le dévorera. Il s'enfuira face à l'épée, et ses jeunes hommes deviendront tributaires.
31:9	Son rocher s'enfuira, terrifié, et ses chefs seront effrayés à cause de la bannière, – déclaration de YHWH, qui a son feu dans Sion et son fourneau dans Yeroushalaim.

## Chapitre 32

### La venue de l'Esprit annonce la paix et la justice

32:1	Voici, un roi régnera selon la justice, et les princes gouverneront avec équité.
32:2	Un homme deviendra une cachette contre le vent, un asile contre la tempête, comme des ruisseaux d'eaux dans la sécheresse, comme l'ombre d'un grand rocher sur une terre épuisée.
32:3	Les yeux de ceux qui voient ne se détourneront plus et les oreilles de ceux qui entendent seront attentives.
32:4	Le cœur de ceux qui agissent avec précipitation comprendra la connaissance, et la langue de ceux qui balbutient parlera vite et clairement.
32:5	L'insensé ne sera plus appelé noble et l'on ne dira plus de l'avare qu'il est généreux.
32:6	Car l'insensé profère la folie, et son cœur s'adonne à la méchanceté, pour exécuter son hypocrisie et pour proférer des faussetés contre YHWH, pour rendre vide l'âme de celui qui a faim, et faire tarir la boisson de celui qui a soif<!--Jn. 10:10.-->.
32:7	Les armes de l'avare sont mauvaises. Il donne des conseils pleins de méchancetés pour détruire par des paroles mensongères les affligés, même quand la cause du pauvre est juste<!--2 Pi. 2:3.-->.
32:8	Mais celui qui est généreux ne donne que de généreux conseils et il se lève en faveur de choses généreuses.
32:9	Femmes insouciantes, levez-vous, écoutez ma voix ! Filles confiantes, prêtez l'oreille à ma parole !
32:10	Dans un an et quelques jours, vous tremblerez, vous qui êtes confiantes, car la vendange sera achevée, la récolte n'arrivera plus.
32:11	Vous qui êtes insouciantes, soyez terrifiées ! Vous qui êtes confiantes, tremblez ! Dépouillez-vous, mettez-vous à nu, une ceinture sur les reins !
32:12	On se frappe la poitrine à cause de la vigne abondante en fruits.
32:13	Les épines et les ronces montent sur le sol de mon peuple, même sur toutes les maisons où il y a de la joie et sur la ville joyeuse.
32:14	Car le palais est abandonné, la multitude de la cité est délaissée. Les lieux inaccessibles de la terre et les forteresses serviront de cavernes pour toujours. Les ânes sauvages y joueront et les troupeaux y paîtront,
32:15	jusqu'à ce que l'Esprit soit répandu d'en haut sur nous<!--Joë. 3:1 ; Za. 12:10 ; Ac. 2:17-18.--> et que le désert devienne un verger<!--Ou Carmel.-->, et que le verger soit compté comme une forêt.
32:16	Le jugement résidera dans le désert et la justice habitera dans le verger<!--Ou Carmel.-->.
32:17	L'œuvre de la justice sera le shalôm, et l'ouvrage de la justice, le repos et la sécurité pour toujours.
32:18	Mon peuple habitera dans un tabernacle de shalôm, dans des habitations sûres, dans des lieux de repos tranquilles.
32:19	Mais la grêle tombera sur la forêt, et la ville sera entièrement abaissée.
32:20	Heureux vous qui semez sur toutes les eaux, et qui laissez sans entraves le pied du bœuf et de l'âne !

## Chapitre 33

### YHWH se lève

33:1	Malheur à toi qui dépouilles et qui n'as pas été dépouillé, qui pilles et qu'on n'a pas encore pillé ! Quand tu auras fini de dépouiller, tu seras dépouillé. Quand tu auras achevé de piller, on te pillera.
33:2	YHWH, aie pitié de nous ! Nous nous attendons à toi ! Sois leur bras dès le matin et notre délivrance au temps de la détresse !
33:3	Au son du tumulte, les peuples s'enfuient. Quand tu te lèves, les nations se dispersent.
33:4	On recueille votre butin comme la collecte des sauterelles, on s’y rue comme se précipitent les sauterelles.
33:5	YHWH est élevé, car il habite dans les lieux élevés. Il remplit Sion de jugement et de justice<!--Ps. 97:9.-->.
33:6	La fidélité existera dans votre temps, la sagesse et la connaissance seront les richesses du salut. La crainte de YHWH sera votre trésor.
33:7	Voici, ceux d'Ariel poussent des cris au-dehors, les messagers de paix pleurent amèrement.
33:8	Les routes sont réduites en désolation, les passants n'y passent plus. Il a rompu l'alliance, il a rejeté les villes, il n’a pas tenu compte du mortel.
33:9	On mène le deuil, la terre languit. Le Liban est honteux et flétri. Le Sharôn est comme un désert. Le Bashân et le Carmel secouent leur feuillage.
33:10	Maintenant je me lèverai, dit YHWH, maintenant je serai exalté, maintenant je serai élevé.
33:11	Vous avez conçu du foin, et vous enfanterez de la paille. Votre souffle est le feu qui vous dévorera.
33:12	Et les peuples deviendront des combustions de chaux, ils seront brûlés au feu comme des épines coupées.
33:13	Vous qui êtes loin, écoutez ce que j'ai fait ! Et vous qui êtes près, connaissez ma force !

### YHWH assure la paix aux justes

33:14	Les pécheurs sont effrayés dans Sion, et le tremblement saisit les athées : Qui de nous pourra séjourner avec le feu dévorant<!--Hé. 12:29.--> ? Qui de nous pourra séjourner avec les flammes éternelles ?
33:15	Celui qui marche dans la justice et qui parle avec droiture, qui rejette le gain injuste acquis par extorsion, et qui secoue ses paumes pour ne pas accepter un pot-de-vin, qui bouche ses oreilles pour ne pas entendre des propos sanguinaires, et qui ferme ses yeux pour ne pas voir le mal.
33:16	Celui-là habitera dans des lieux élevés, des forteresses bâties sur le rocher seront sa haute retraite, son pain lui sera donné, et ses eaux ne lui manqueront pas<!--Jn. 4:14, 6:33-35 ; Ap. 21:6.-->.
33:17	Tes yeux contempleront le roi dans sa beauté, et verront la terre de loin.
33:18	Ton cœur méditera sur la terreur : Où est le scribe, où est le trésorier ? Où est celui qui tient le compte des tours ?
33:19	Tu ne verras plus le peuple barbare, le peuple au langage inintelligible, à la langue bégayante qu'on ne comprend pas.
33:20	Regarde Sion, la ville de nos fêtes solennelles ! Que tes yeux voient Yeroushalaim, séjour tranquille, tabernacle qui ne sera pas transporté, et dont les pieux ne seront jamais ôtés, et dont les cordages ne seront pas rompus<!--Ap. 21:2.-->.
33:21	Car c'est là que YHWH est majestueux pour nous : il nous tient lieu de fleuves et de rivières aux mains larges où n'ira pas de navire à rame, où aucun gros navire ne passera.
33:22	Parce que YHWH est notre Juge, YHWH est notre Législateur, YHWH est notre Roi<!--Yéhoshoua ha Mashiah (Jésus-Christ) exerce toutes les fonctions gouvernementales : législatives, exécutives et judiciaires.-->, c'est lui qui vous sauvera.
33:23	Tes cordages sont relâchés : ils ne maintiennent plus le mât sur sa base et ne tendent plus les voiles. Alors la dépouille d'un grand butin est partagée. Les boiteux pillent le butin.
33:24	Et celui qui fait sa demeure dans la maison ne dit pas : Je suis malade ! Le peuple qui habite en elle reçoit le pardon de son péché.

## Chapitre 34

### Le jugement des nations<!--Ap. 19:17-21.-->

34:1	Approchez-vous nations, pour écouter ! Et vous peuples, soyez attentifs ! Que la Terre et tout ce qui la remplit écoute, le monde et tout ce qui y est produit !
34:2	Car la colère de YHWH est sur toutes les nations, et sa fureur sur toute leur armée : il les voue à l'interdit, il les livre au massacre.
34:3	Leurs blessés mortellement sont jetés là, et la puanteur de leurs cadavres se répand et les montagnes se fondent à cause de leur sang.
34:4	Toute l'armée des cieux se décompose. Les cieux sont roulés comme un livre<!--Ap. 6:14.-->, toute leur armée tombe, comme tombe la feuille de la vigne, comme tombe celle du figuier<!--Mt. 24:28 ; Mc. 13:25.-->.
34:5	Parce que mon épée s'est enivrée dans les cieux, voici, elle va descendre en jugement contre Édom, contre le peuple que j'ai voué à l'interdit.
34:6	L'épée de YHWH est pleine de sang, engraissée de graisse, du sang des agneaux et des boucs, et de la graisse des reins de béliers. Car YHWH a un sacrifice à Botsrah et un grand massacre en terre d'Édom.
34:7	Les taureaux sauvages descendent avec eux et les jeunes taureaux avec les puissants. Leur terre est enivrée de sang et leur poussière engraissée de graisse.
34:8	Car c'est un jour de vengeance pour YHWH, une année de rétribution pour maintenir la cause de Sion<!--Jé. 46:10 ; Joë. 2:2 ; So. 1:15.-->.
34:9	Les torrents d'Édom seront changés en poix, sa poussière en soufre et sa terre deviendra de la poix ardente.
34:10	Elle ne sera éteinte ni le jour ni la nuit. Sa fumée montera éternellement. Elle sera desséchée d'âges en âges, plus jamais personne n'y passera.
34:11	Le pélican et le hérisson en prendront possession, la chouette et le corbeau y habiteront. On étendra sur elle le cordeau du tohu<!--Vient de l'hébreu « tohuw » qui signifie « informe », « confusion », « désert ». Voir Ge. 1:2.--> et les pierres du bohu<!--Vient de l'hébreu « bohuw » qui signifie « vide », « nul », « perdre », « détruire ». Voir Ge. 1:2.-->.
34:12	Ses nobles n'y proclameront plus de royaume, et tous ses princes seront réduits à néant.
34:13	Les épines croîtront dans ses palais, les chardons et les buissons dans ses forteresses. Elle sera la demeure des dragons et le parvis des filles de l'autruche.
34:14	Les bêtes sauvages y rencontreront les chacals, les satyres<!--Créatures sylvestres de la mythologie grecque, mi-homme mi-bête.--> appelleront leurs compagnons. C'est là que Lilith<!--Lilith est le nom d'une déesse de la nuit connue pour être un démon nocturne qui hantait les lieux déserts d'Édom.--> se tiendra tranquille et trouvera son lieu de repos.
34:15	C'est là que le serpent fera son nid, déposera ses œufs, les couvera et recueillera ses petits à son ombre. En effet, c'est là que se rassembleront les oiseaux de proie, chaque femelle avec sa voisine.
34:16	Consultez le livre de YHWH et lisez ! Il n'en manquera pas un, aucune femelle ne manquera de visiter sa voisine. Car c'est sa bouche qui l'a ordonné et c'est son Esprit qui les rassemblera.
34:17	Il leur a jeté le sort, et sa main leur a partagé cette terre au cordeau. Ils en prendront possession pour toujours, ils y demeureront d'âges en âges.

## Chapitre 35

### YHWH se révèle et sauve son peuple

35:1	Le désert et la sécheresse se réjouiront ! La région aride exultera et fleurira comme une rose.
35:2	Elle fleurira, elle fleurira et se réjouira avec allégresse et avec des cris de joie. La gloire du Liban lui est donnée, avec la magnificence de Carmel et de Sharôn. Ils verront la gloire de YHWH et la magnificence de notre Elohîm.
35:3	Fortifiez les mains faibles et affermissez les genoux qui trébuchent<!--Hé. 12:12.-->.
35:4	Dites à ceux qui ont le cœur troublé : Prenez courage, n'ayez pas peur<!--Jn. 14:1, 16:33.--> ! Voici votre Elohîm : la vengeance viendra, la rétribution d'Elohîm. Il viendra lui-même et vous délivrera.
35:5	Alors les yeux des aveugles seront ouverts et les oreilles des sourds seront débouchées.
35:6	Alors le boiteux sautera comme un cerf et la langue du muet poussera des cris de joie<!--Mt. 11:4-5.-->. Car des eaux jailliront dans le désert, et des torrents dans la région aride.
35:7	La terre desséchée deviendra un étang, le sol assoiffé, des sources d'eaux. Dans la demeure où les dragons avaient leur lieu de repos, l’herbe verte poussera comme les roseaux et le papyrus.
35:8	Il y aura là une grande route, un chemin qu'on appellera le chemin de sainteté. Celui qui est souillé n'y passera pas, mais il sera pour eux seuls. Ceux qui marcheront dans ce chemin et les fous ne s'y égareront pas.
35:9	Là il n'y aura pas de lion, aucune bête féroce n'y montera ni ne s'y trouvera. Mais les rachetés y marcheront.
35:10	Ceux pour qui YHWH aura payé la rançon<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH qui a payé notre rançon (2 S. 7:23 ; Es. 51:11 ; Ps. 130:8 ; Mc. 10:45 ; 1 Ti. 2:6).--> retourneront, ils viendront à Sion avec chant de triomphe et une joie éternelle sera sur leur tête. Ils obtiendront la joie et l'allégresse, la douleur et le gémissement s'enfuiront.

## Chapitre 36

### Invasion de Sanchérib, menaces de Rabshaké<!--2 R. 18:9-37 ; 2 Ch. 32:1-19.-->

36:1	La quatorzième année du roi Hizqiyah, Sanchérib, roi d'Assyrie, monta contre toutes les villes fortes de Yéhouda et les prit<!--2 R. 18:17.-->.
36:2	Le roi d'Assyrie envoya de Lakis à Yeroushalaim, vers le roi Hizqiyah, Rabshaké avec une puissante armée. Rabshaké s'arrêta à l'aqueduc de l'étang supérieur, sur le chemin du champ du foulon.
36:3	Élyakim, fils de Chilqiyah, chef de la maison du roi, Shebna, le scribe, et Yoach, fils d'Asaph, l'archiviste, sortirent vers lui.
36:4	Rabshaké leur dit : Dites maintenant à Hizqiyah : Ainsi parle le grand roi, le roi d'Assyrie : Quelle est cette confiance que tu as ?
36:5	Je dis : Ce ne sont que paroles des lèvres. Pour la guerre, il faut le conseil et la force ! Maintenant en qui t'es-tu confié pour t'être rebellé contre moi ?
36:6	Voici, tu t'es confié sur ce bâton qui n'est qu'un roseau cassé, sur l'Égypte, qui perce et traverse la paume de celui qui s'appuie dessus ! Tel est pharaon, roi d'Égypte, pour tous ceux qui se confient en lui.
36:7	Et si tu me dis : Nous nous confions en YHWH, notre Elohîm. Mais n'est-ce pas lui dont Hizqiyah a ôté les hauts lieux et les autels, en disant à Yéhouda et à Yeroushalaim : Vous vous prosternerez devant cet autel-ci ?
36:8	Maintenant, s'il te plaît, échange des garanties avec mon seigneur, le roi d'Assyrie, et je te donnerai 2 000 chevaux, si tu peux donner des cavaliers pour les monter.
36:9	Et comment ferais-tu détourner les faces à un seul gouverneur d'entre les moindres serviteurs de mon seigneur ? Mais tu te confies en l'Égypte pour les chars et pour les cavaliers.
36:10	Mais suis-je monté sans YHWH contre cette terre pour la détruire ? YHWH m'a dit : Monte vers cette terre et détruis-la !
36:11	Élyakim, Shebna et Yoach dirent à Rabshaké : S'il te plaît, parle à tes serviteurs en araméen, car nous écoutons, mais ne nous parle pas en hébreu, aux oreilles du peuple qui est sur la muraille.
36:12	Et Rabshaké dit : Est-ce vers ton seigneur et vers toi que mon seigneur m'a envoyé pour prononcer ces paroles ? N'est-ce pas vers les hommes qui se tiennent sur la muraille pour manger avec vous leurs excréments et boire leur urine ?
36:13	Et Rabshaké se tint debout et cria à grande voix en hébreu et dit : Écoutez les paroles du grand roi, du roi d'Assyrie !
36:14	Ainsi parle le roi : Qu'Hizqiyah ne vous séduise pas ! Car il ne pourra pas vous délivrer.
36:15	Qu'Hizqiyah ne vous amène pas à vous confier en YHWH, en disant : YHWH nous délivrera, il nous délivrera et cette ville ne sera pas livrée entre les mains du roi d'Assyrie.
36:16	N'écoutez pas Hizqiyah, car ainsi parle le roi d'Assyrie : Faites un traité de paix avec moi pour votre bien, et sortez vers moi ! Chacun de vous mangera de sa vigne et de son figuier, chacun boira de l'eau de sa citerne,
36:17	jusqu'à ce que je vienne, et que je vous emmène vers une terre qui est comme votre terre, une terre de blé et de vin nouveau, une terre de pain et de vignes.
36:18	Qu'Hizqiyah ne vous séduise pas, en disant : YHWH nous délivrera. Les elohîm des nations ont-ils délivré chacun leur terre de la main du roi d'Assyrie ?
36:19	Où sont les elohîm de Hamath et d'Arpad ? Où sont les elohîm de Sepharvaïm ? Ont-ils délivré Samarie de ma main ?
36:20	Qui sont ceux d'entre tous les elohîm de ces terres qui aient délivré leur terre de ma main, pour que YHWH délivre Yeroushalaim de ma main ?
36:21	Mais ils se turent et ne lui répondirent pas un mot, car le roi avait donné cet ordre, en disant : Vous ne lui répondrez pas.

### Hizqiyah (Ézéchias) informé des menaces

36:22	Élyakim fils de Chilqiyah, chef de la maison du roi, Shebna, le scribe, et Yoach, fils d'Asaph l'archiviste, vinrent auprès d'Hizqiyah, les vêtements déchirés, et lui rapportèrent les paroles de Rabshaké.

## Chapitre 37

### Hizqiyah (Ézéchias) recherche YHWH auprès de Yesha`yah<!--2 R. 19:1-7 ; 2 Ch. 32:20.-->

37:1	Il arriva que lorsque le roi Hizqiyah l’eut appris, il déchira ses vêtements, se couvrit d'un sac et entra dans la maison de YHWH<!--2 R. 19:1-7 ; 2 Ch. 32:20.-->.
37:2	Il envoya Élyakim, chef de la maison du roi, et Shebna, le scribe, et les plus anciens des prêtres couverts de sacs, vers Yesha`yah, le prophète, fils d'Amots.
37:3	Et ils lui dirent : Ainsi parle Hizqiyah : Ce jour est un jour d'angoisse, de répréhension et de blasphème, car les fils sont venus jusqu'au lieu de brèche, mais il n'y a pas de force pour enfanter.
37:4	Peut-être que YHWH, ton Elohîm, aura entendu les paroles de Rabshaké, que le roi d'Assyrie, son seigneur, a envoyé pour insulter l'Elohîm vivant, et qu'il le jugera pour les paroles que YHWH, ton Elohîm, a entendues. Fais monter une prière pour le reste qui se trouve encore !
37:5	Les serviteurs du roi Hizqiyah vinrent vers Yesha`yah.
37:6	Et Yesha`yah leur dit : Voici ce que vous direz à votre seigneur : Ainsi parle YHWH : N'aie pas peur face aux paroles que tu as entendues, par lesquelles les serviteurs du roi d'Assyrie m'ont blasphémé.
37:7	Voici, je vais mettre en lui un esprit tel que, sur une nouvelle qu'il entendra, il retournera vers sa terre, et je le ferai tomber par l'épée sur sa terre.

### Provocation et menace de Sanchérib<!--2 R. 19:8-13 ; 2 Ch. 32:17-19.-->

37:8	Rabshaké s'en retourna et trouva le roi d'Assyrie qui combattait contre Libnah, car il avait appris qu'il était parti de Lakis.
37:9	Alors il entendit dire sur Tirhaka, roi d'Éthiopie : Il est sorti pour te faire la guerre. Dès qu'il eut entendu cela, il envoya des messagers à Hizqiyah, en leur disant :
37:10	Vous parlerez ainsi à Hizqiyah, roi de Yéhouda : Que ton Elohîm, auquel tu te confies, ne te séduise pas, en disant : Yeroushalaim ne sera pas livrée entre les mains du roi d'Assyrie.
37:11	Voici, tu as appris ce que les rois d'Assyrie ont fait à toutes les terres, en les dévouant par interdit. Et toi, tu serais délivré ?
37:12	Les elohîm des nations que mes pères ont détruites les ont-ils délivrées ? Gozan, Charan, Retseph et les fils d'Éden qui sont à Telassar ?
37:13	Où sont le roi de Hamath, le roi d'Arpad, et le roi de la ville de Sepharvaïm, d'Héna et d'Ivva ?

### Prière d'Hizqiyah (Ézéchias) à YHWH<!--2 R. 19:14-19 ; 2 Ch. 32:20.-->

37:14	Hizqiyah prit la lettre de la main des messagers et la lut. Puis Hizqiyah monta à la maison de YHWH et la déploya devant YHWH.
37:15	Hizqiyah fit sa prière à YHWH et dit :
37:16	YHWH Tsevaot ! Elohîm d'Israël qui es assis entre les chérubins ! C'est toi qui es le seul Elohîm de tous les royaumes de la Terre, c'est toi qui as fait les cieux et la Terre.
37:17	YHWH ! incline ton oreille et écoute ! YHWH ! ouvre tes yeux et regarde ! Écoute les paroles de Sanchérib, qu'il m'a envoyé dire pour blasphémer l'Elohîm vivant.
37:18	Il est vrai, YHWH, que les rois d'Assyrie ont détruit toutes les terres et leurs propres terres,
37:19	et qu'ils ont jeté dans le feu leurs elohîm, mais ce n'étaient pas des elohîm : ils n'étaient que l'œuvre des mains humaines, du bois et de la pierre. Voilà pourquoi ils les ont détruits.
37:20	Maintenant, YHWH, notre Elohîm ! Sauve-nous de sa main, afin que tous les royaumes de la Terre sachent que toi seul es YHWH.

### Yesha`yah transmet la réponse de YHWH<!--2 R. 19:20-34.-->

37:21	Yesha`yah, fils d'Amots, envoya dire à Hizqiyah : Ainsi parle YHWH, l'Elohîm d'Israël : Quant à la prière que tu m’as faite au sujet de Sanchérib, roi d'Assyrie,
37:22	voici la parole que YHWH a déclarée contre lui : Elle te méprise, elle se moque de toi, la vierge, la fille de Sion. Elle hoche la tête après toi, la fille de Yeroushalaim.
37:23	Contre qui as-tu élevé ta voix, et levé tes yeux en haut ? C'est contre le Saint d'Israël !
37:24	Par la main de tes serviteurs, tu as insulté Adonaï et tu as dit : Je suis monté avec la multitude de mes chars au sommet des montagnes, aux côtés du Liban. Je couperai les plus élevés de ses cèdres, et ses plus beaux cyprès, et j'atteindrai sa dernière cime et jusqu'à la forêt de son Carmel.
37:25	J'ai creusé et j'ai bu de l'eau, je tarirai avec la plante de mes pieds tous les fleuves de l'Égypte.
37:26	N'as-tu pas entendu que de loin j'ai préparé cela ? Dès les temps anciens je l’ai façonné. Maintenant j'ai fait venir ceci pour que tu réduises les villes fortifiées en monceaux de ruines.
37:27	Leurs habitants ont la main courte, ils sont épouvantés et honteux. Ils sont devenus de l’herbe des champs, la végétation d'un jardin potager, l'herbe verte des toits et le champ avant que le blé ne soit sur pied.
37:28	Ta demeure, ta sortie et ton entrée, je les connais, ainsi que ta rage contre moi<!--Ps. 139:2.-->.
37:29	Parce que ta rage et ton arrogance sont montées à mes oreilles, je mettrai ma boucle à tes narines et mon mors à tes lèvres, et je te ferai retourner par le chemin par lequel tu es venu.
37:30	Ceci te servira de signe : on mangera cette année ce que les champs produisent d'eux-mêmes, et la deuxième année ce qui croîtra encore sans semer, mais la troisième année, vous sèmerez, vous moissonnerez, vous planterez des vignes, et vous en mangerez le fruit.
37:31	Ce qui aura été sauvé de la maison de Yéhouda, ce qui restera, poussera encore des racines vers le bas, et portera du fruit vers le haut.
37:32	Car il sortira de Yeroushalaim un reste, et de la montagne de Sion quelques rescapés, la jalousie de YHWH Tsevaot fera cela.
37:33	C'est pourquoi ainsi parle YHWH sur le roi d'Assyrie : Il n'entrera pas dans cette ville, il n'y jettera aucune flèche, il ne se présentera pas contre elle avec le bouclier, et il ne dressera pas de tertres contre elle.
37:34	Il s'en retournera par le chemin par lequel il est venu, et il n'entrera pas dans cette ville, – déclaration de YHWH.
37:35	Car je protégerai cette ville pour la délivrer à cause de moi, et à cause de David, mon serviteur.

### YHWH frappe Sanchérib<!--2 R. 19:35-37 ; 2 Ch. 32:21.-->

37:36	L'Ange de YHWH<!--Ge. 16:7.--> sortit et frappa 185 000 hommes dans le camp des Assyriens. Et quand on se leva le matin, voici, ils étaient tous morts.
37:37	Alors Sanchérib, roi d'Assyrie, se retira, s'en alla et retourna pour résider à Ninive.
37:38	Et il arriva qu'étant prosterné dans la maison de Nisrok<!--Le nom Nisrok signifie « le grand aigle ». C'était une idole de Ninive adorée par Sanchérib, symbolisée par un aigle à figure humaine.-->, son elohîm, Adrammélek et Sharetser, ses fils, le tuèrent avec l'épée et s'enfuirent en terre d'Ararat. Et Ésar-Haddon, son fils, régna à sa place.

## Chapitre 38

### Maladie et guérison d'Hizqiyah (Ézéchias)<!--2 R. 20:1-11 ; 2 Ch. 32:24-30.-->

38:1	En ces temps-là, Hizqiyah fut malade à la mort<!--2 R. 20:1-11 ; 2 Ch. 32:24-30.-->. Et Yesha`yah, le prophète, fils d'Amots, vint auprès de lui, et lui dit : Ainsi parle YHWH : Donne tes ordres à ta maison, car tu vas mourir et tu ne vivras plus.
38:2	Alors Hizqiyah tourna ses faces vers le mur et fit sa prière à YHWH,
38:3	et dit : Oh ! S'il te plaît, YHWH, souviens-toi maintenant que j'ai marché en face de toi en vérité, avec un cœur entier, et que j'ai fait ce qui est agréable à tes yeux ! Et Hizqiyah pleura, pleura beaucoup.
38:4	La parole de YHWH vint à Yesha`yah, en disant :
38:5	Va, et dis à Hizqiyah ainsi parle YHWH, l'Elohîm de David, ton père : J'ai entendu ta prière, j'ai vu tes larmes. Voici, j'ajouterai à tes jours quinze années.
38:6	Je te délivrerai de la paume du roi d'Assyrie, toi et cette ville, et je défendrai cette ville.
38:7	Ceci sera pour toi, de la part de YHWH, le signe que YHWH accomplira la parole qu'il a déclarée.
38:8	Voici, je ferai revenir de 10 degrés en arrière avec le soleil l'ombre des degrés qui est descendue sur les degrés d'Achaz<!--Voir commentaire en 2 R. 20:11.-->. Et le soleil retourna de 10 degrés par les degrés par lesquels il était descendu.
38:9	Voici l'écrit d'Hizqiyah, roi de Yéhouda, lorsqu'il fut malade et survécut à sa maladie.
38:10	Je me disais : Quand mes jours sont en repos : Je m'en irai aux portes du shéol. Je suis privé de ce qui restait de mes années.
38:11	Je disais : Je ne verrai plus Yah, Yah sur la terre des vivants. Je ne regarderai plus aucun être humain parmi les habitants du monde !
38:12	Ma génération s'en est allée, elle est transportée loin de moi comme une cabane de berger. Ma vie est coupée, je suis retranché comme la toile que le tisserand détache de sa trame. Du jour à la nuit tu en auras fini<!--Aux versets 12 et 13, le mot qui a été traduit par « fini » est « shalam » : « être dans une alliance de paix, être en paix ».--> avec moi !
38:13	Je me suis contenu jusqu'au matin, mais pareil à un lion, il brisait tous mes os. Du jour à la nuit tu en auras fini avec moi !
38:14	Je murmurais comme la grue et l'hirondelle, je gémissais comme la colombe. Mes yeux se sont lassés à regarder en haut : Adonaï, je suis opprimé, sois mon garant !
38:15	Que dirai-je ? Il m'a parlé et lui-même l'a fait. Je m'en irai tout doucement pendant toutes mes années dans l'amertume de mon âme.
38:16	Adonaï, c'est par ces choses qu'on a la vie, et c'est dans toutes ces choses que mon esprit trouve la vie. Ainsi tu me rétabliras et me feras revivre.
38:17	Voici, dans ma paix une amertume, une amertume m'était survenue. Mais tu t'es attaché à mon âme afin qu'elle ne tombe pas dans la fosse de la pourriture, car tu as jeté tous mes péchés derrière ton dos.
38:18	Car le shéol ne te loue pas, la mort ne te célèbre pas. Ceux qui sont descendus dans la fosse n'espèrent plus en ta fidélité<!--Ps. 115:17.-->.
38:19	Le vivant, le vivant est celui qui te célèbre, comme moi aujourd'hui, et c'est le père qui fait connaître aux enfants ta fidélité<!--Pr. 22:6 ; Ep. 6:4.-->.
38:20	YHWH m’a sauvé ! Nous jouerons mes musiques tous les jours de notre vie dans la maison de YHWH.
38:21	Or Yesha`yah avait dit : Qu'on prenne une masse de figues sèches et qu'on en fasse un emplâtre sur l'ulcère et Hizqiyah vivra.
38:22	Et Hizqiyah avait dit : Quel est le signe que je monterai à la maison de YHWH ?

## Chapitre 39

### Hizqiyah (Ézéchias) montre ses richesses aux Babyloniens<!--2 R. 20:12-19.-->

39:1	En ce temps-là<!--2 R. 20:12-19.-->, Merodak-Baladan, fils de Baladan, roi de Babel, envoya des lettres avec un présent à Hizqiyah, parce qu'il avait appris qu'il avait été malade, et qu'il était guéri.
39:2	Hizqiyah s'en réjouit et leur montra sa maison du trésor, l'argent, l'or, les aromates et l'huile précieuse, toute sa maison d'armes et tout ce qui se trouvait dans ses trésors. Il n'y eut rien que Hizqiyah ne leur montra dans sa maison et dans tout son royaume.
39:3	Le prophète Yesha`yah vint vers le roi Hizqiyah et lui dit : Qu'ont dit ces hommes-là, et d'où sont-ils venus vers toi ? Et Hizqiyah dit : Ils sont venus vers moi d'une terre éloignée, de Babel.
39:4	Il dit : Qu'ont-ils vu dans ta maison ? Hizqiyah répondit : Ils ont vu tout ce qui est dans ma maison : il n'y a rien dans mes trésors que je ne leur aie montré.
39:5	Yesha`yah dit à Hizqiyah : Écoute la parole de YHWH Tsevaot :
39:6	Voici, les jours viennent où l'on emportera à Babel tout ce qui est dans ta maison, et ce que tes pères ont amassé dans leurs trésors jusqu'à aujourd'hui ; il n'en restera rien, dit YHWH<!--2 R. 24:13, 25:13-15 ; Jé. 20:5.-->.
39:7	Même on prendra de tes fils qui sortiront de toi, et que tu auras engendrés afin qu'ils soient eunuques dans le palais du roi de Babel<!--Da. 1:3-4.-->.
39:8	Hizqiyah dit à Yesha`yah : La parole de YHWH que tu as déclarée est bonne. Car, dit-il, il y aura paix et sécurité pendant mes jours.

## Chapitre 40

### Un nouveau message pour Yesha`yah

40:1	Consolez, consolez mon peuple, dit votre Elohîm.
40:2	Parlez à Yeroushalaim selon son cœur et criez-lui que son combat est terminé, que son iniquité est expiée, qu'elle a reçu de la main de YHWH le double pour tous ses péchés.
40:3	Une voix crie : Dans le désert<!--L'accomplissement de cette prophétie se trouve en Mt. 3:3, où il est dit que la voix qui devait crier ces choses était celle de Yohanan le Baptiste (Jean-Baptiste) (voir aussi Mal. 3:1, 4:5-6 ; Mt. 17:10-13).-->, préparez le chemin de YHWH, aplanissez dans les régions arides un chemin pour notre Elohîm.
40:4	Toute vallée sera élevée, toute montagne et toute colline seront abaissées, et les lieux tortueux seront redressés, et les lieux raboteux seront aplanis.
40:5	Alors la gloire de YHWH sera révélée, et toute chair en même temps la verra, car la bouche de YHWH a parlé.

### La grandeur d'Elohîm échappe à l'être humain

40:6	La voix dit : Crie ! On a répondu : Que crierai-je ? Toute chair est de l'herbe et toute sa grâce est comme la fleur d'un champ<!--Ja. 1:10 ; 1 Pi. 1:24-25.-->.
40:7	L'herbe sèche et la fleur tombe, parce que le vent de YHWH souffle dessus. En vérité, le peuple, c'est de l'herbe !
40:8	L'herbe sèche et la fleur tombe, mais la parole de notre Elohîm demeure éternellement<!--1 Pi. 1:25.-->.
40:9	Monte sur une haute montagne, Sion, toi qui portes la bonne nouvelle ! Élève ta voix avec force, Yeroushalaim, toi qui portes la bonne nouvelle. Élève-la, n'aie pas peur ! Dis aux villes de Yéhouda : Voici votre Elohîm !
40:10	Voici, Adonaï YHWH vient<!--Yéhoshoua ha Mashiah est YHWH qui vient (Es. 35:4, 40:10-11, 60:1, 62:11-12, 66:15-16 ; Za. 14:1-7 ; Mt. 24 ; Jn. 14:1-3 ; Ac. 1:10-12 ; Ap. 3:11, 19:11-12, 22:7,12,20).--> avec force, son bras domine pour lui. Voici son salaire avec lui, sa récompense<!--Ge. 15:1 ; Es. 49:4, 62:11.--> en face de lui.
40:11	Il paîtra son troupeau comme un berger, il rassemblera les agneaux dans ses bras, il les placera dans son sein ; il conduira celles qui allaitent<!--Jn. 10.-->.
40:12	Qui a mesuré les eaux avec le creux de sa main, pris avec la paume les dimensions des cieux, fait tenir la poussière de la Terre dans un tiers de mesure, pesé au crochet les montagnes et les collines à la balance ?
40:13	Qui a mesuré l'Esprit de YHWH ou qui a été son conseiller pour l'enseigner<!--1 Co. 2:16 ; Ro. 11:34.--> ?
40:14	De qui a-t-il pris conseil pour avoir du discernement ? Qui lui a enseigné la voie du jugement ? Qui lui a enseigné la connaissance ? Qui lui a fait connaître le chemin de l'intelligence ?
40:15	Voici les nations, elles sont comme une goutte qui tombe d'un seau, elles sont considérées comme de la poussière sur une balance ! Voici les îles, elles sont comme une fine poussière qu'il soulève !
40:16	Le Liban ne suffirait pas pour faire le feu et les bêtes qui y sont ne seraient pas suffisantes pour l'holocauste.
40:17	Toutes les nations sont devant lui comme un rien, elles comptent pour lui comme un néant, un tohu<!--Vient de l'hébreu « tohuw » qui signifie « informe, confusion, chose irréelle, vide ». Voir Ge. 1:2.--> !
40:18	À qui ferez-vous ressembler El ? Et à quelle ressemblance l'égalerez-vous ?
40:19	L'ouvrier fond l'idole, et l'orfèvre la couvre d'or, et y soude des chaînettes d'argent.
40:20	Le pauvre qui renonce à faire une offrande choisit un bois qui ne pourrisse pas ; il se cherche un habile ouvrier pour faire une idole qui ne bouge pas<!--Es. 44:9-20.-->.
40:21	Ne le savez-vous pas ? Ne l'avez-vous pas entendu ? Cela ne vous a-t-il pas été déclaré dès le commencement ? N'avez-vous pas compris la fondation de la Terre ?
40:22	C'est lui qui habite au-dessus du cercle de la Terre, et ceux qui l'habitent sont comme des sauterelles. Il étend les cieux comme un voile, il les déploie comme une tente pour y habiter.
40:23	C'est lui qui réduit les princes à rien et qui fait des juges de la Terre un tohu.
40:24	Ils ne sont pas même plantés, pas même semés, même leur tronc n'a pas de racine en terre ; il souffle sur eux, et ils sèchent, et le tourbillon les emporte comme de la paille.
40:25	À qui me ferez-vous ressembler, et à qui serai-je égalé ? dit le Saint<!--Voir commentaire en Lu. 1:35 ; Ac. 3:14.-->.
40:26	Levez vos yeux en haut et regardez ! Qui a créé ces choses ? C'est lui qui fait sortir leur armée en bon ordre. Il les appelle toutes par leur nom<!--Voir Ps. 147:4.-->. Par la grandeur de son pouvoir et de sa force puissante, pas une ne manque.
40:27	Pourquoi dis-tu, Yaacov, pourquoi dis-tu, Israël : Ma voie est cachée à YHWH et ma cause<!--Vient d'un mot qui signifie aussi jugement, justice, droit, etc.--> passe inaperçue devant mon Elohîm ?
40:28	Ne le sais-tu pas ? Ne l'as-tu pas entendu ? C'est El-Olam<!--El d'éternité.-->, YHWH, qui a créé les extrémités de la Terre. Il ne se fatigue pas, il ne se lasse pas, et il n'y a pas moyen de sonder son intelligence.
40:29	C'est lui qui donne de la force à celui qui est fatigué, et il multiplie la force de celui qui n'a aucune vigueur.
40:30	Les garçons se lassent et se fatiguent, et les jeunes hommes trébuchent, ils trébuchent.
40:31	Mais ceux qui s'attendent à YHWH renouvellent leur force. Ils s'élèvent avec des ailes comme des aigles. Ils courent et ne se fatiguent pas, ils marchent et ne se lassent pas.

## Chapitre 41

### Dénonciation des idoles

41:1	Îles, faites-moi silence ! Que les peuples renouvellent leurs forces, qu'ils s'approchent et qu'ils parlent ! Allons ensemble en justice.
41:2	Qui a réveillé de l'est la justice ? Qui l'a appelée à ses pieds ? Qui a soumis à son commandement les nations ? Qui lui a donné la domination sur les rois ? Qui les a livrés à son épée comme de la poussière, et à son arc comme de la paille poussée par le vent ?
41:3	Il les a poursuivis, il est passé en paix par le chemin que son pied n'avait jamais foulé.
41:4	Qui a fait et exécuté cela ? Celui qui a appelé les âges dès le commencement. Moi, YHWH, le premier<!--Ap. 1:8, 21:6, 22:13.-->, avec les derniers, moi, lui.
41:5	Les îles le voient, elles sont dans la crainte, les extrémités de la Terre sont effrayées : elles s'approchent, elles viennent.
41:6	Chaque homme aide son compagnon et dit à son frère : Fortifie-toi !
41:7	L'artisan encourage le fondeur, le polisseur au marteau celui qui frappe sur l'enclume. Il dit de la soudure : Elle est bonne ! Puis il fixe l'idole avec des clous, afin qu'elle ne bouge pas.
41:8	Mais toi, Israël, mon serviteur, Yaacov, que j'ai choisi, postérité d'Abraham qui m'a aimé !
41:9	Toi, que j'ai pris des extrémités de la Terre et que j'ai appelé parmi ses nobles, à qui j'ai dit : Tu es mon serviteur, je t'ai choisi et je ne te rejette pas<!--De. 7:6 ; Ps. 77:8.--> !
41:10	N'aie pas peur, car je suis avec toi. Ne sois pas inquiet, car je suis ton Elohîm. Je te fortifie, je viens à ton secours et je te soutiens par la droite de ma justice.
41:11	Voici, tous ceux qui sont indignés contre toi seront honteux et confus ; ils seront réduits à néant, et les hommes qui ont querelle avec toi périront.
41:12	Tu les chercheras et tu ne les trouveras plus, ces hommes qui ont querelle avec toi. Ils deviendront comme un rien et comme néant, ces hommes qui te font la guerre.
41:13	Car je suis YHWH, ton Elohîm, qui soutiens ta main droite et qui te dis : Ne crains rien ! C'est moi qui te secours.
41:14	Ne crains rien, toi vermisseau de Yaacov, et vous hommes d'Israël ! Je viens à ton secours, – déclaration de YHWH, le Saint d'Israël est ton Rédempteur.
41:15	Voici, je fais de toi un traîneau aigu, tout neuf, ayant des dents, tu fouleras les montagnes et les broieras, et tu rendras les collines semblables à de la balle.
41:16	Tu les vanneras, et le vent les emportera, et le tourbillon les dispersera. Mais toi, tu te réjouiras en YHWH, tu te glorifieras dans le Saint d'Israël.
41:17	Les affligés et les pauvres cherchent de l'eau, mais il n'y en a pas, et leur langue est desséchée par la soif. Moi, YHWH, je répondrai à leurs prières. Moi, l'Elohîm d'Israël, je ne les abandonnerai pas<!--Ge. 28:15 ; Jos. 1:5 ; Hé. 13:5.-->.
41:18	Je ferai jaillir des fleuves sur les hauteurs et des fontaines au milieu des vallées. Je ferai du désert des étangs d'eaux et de la terre sèche des sources d'eaux.
41:19	Je ferai croître dans le désert le cèdre, l'acacia, le myrte et l'olivier. Je mettrai dans les régions arides le cyprès, l'orme et le buis ensemble,
41:20	afin qu'on voie, qu'on sache, qu'on pense, et qu'on comprenne que la main de YHWH a fait cela, et que le Saint d'Israël a créé cela.
41:21	Présentez votre cause, dit YHWH. Avancez vos arguments, dit le Roi de Yaacov.
41:22	Qu'ils s'approchent et qu'ils nous révèlent ce qui arrivera. Les premiers événements, qu'en était-il ? Révélez-le et nous y appliquerons notre cœur pour en connaître l'issue ! Ou bien faites-nous entendre les choses à venir !
41:23	Révélez les choses qui arriveront plus tard, et nous saurons que vous êtes des elohîm ! Oui, faites du bien et faites du mal, afin que nous le regardions et le voyions ensemble.
41:24	Voici, vous n'êtes rien et votre œuvre est sans valeur. C'est une abomination que de vous choisir.
41:25	Je l'ai suscité du nord et il est venu. Depuis le soleil levant, il proclame mon Nom. Il marche sur les gouverneurs comme sur la boue, et les foule comme le potier foule l'argile.
41:26	Qui l'a révélé dès le commencement pour que nous le sachions, longtemps à l'avance pour que nous disions : Il est juste ? Mais personne ne l'a révélé, personne ne l'a fait entendre, personne n'a entendu vos paroles.
41:27	Moi, le premier j'ai dit à Sion : Les voici ! Les voici ! Je donnerai à Yeroushalaim un porteur de bonne nouvelle<!--Es. 52:7 ; Ap. 14:6.-->.
41:28	Je regarde : pas un seul homme ! Parmi eux, pas un conseiller que je puisse interroger et qui réponde quelque chose !
41:29	Voici, quant à eux tous, leurs œuvres ne sont que vanité, leurs idoles en métal fondu ne sont que vent et tohu.

## Chapitre 42

### Le Mashiah, serviteur de YHWH

42:1	Voici mon serviteur que je soutiens, mon élu en qui mon âme prend plaisir. J'ai mis mon Esprit sur lui, il fera sortir le jugement pour les nations<!--Mt. 3:17, 17:5 ; Mc. 9:7.-->.
42:2	Il ne criera pas, il n'élèvera pas la voix, il ne la fera pas entendre dans les rues.
42:3	Il ne brisera pas le roseau cassé et il n'éteindra pas le lumignon qui fume<!--Mt. 12:18-20.-->, il fera sortir la justice selon la vérité.
42:4	Il ne sera pas écrasé et ne s'affaiblira pas jusqu'à ce qu'il ait établi la justice sur la Terre, et que les îles espèrent en sa torah.
42:5	Ainsi parle El, YHWH, qui a créé les cieux, et qui les a étendus, qui a aplani la Terre avec ce qu'elle produit, qui donne la respiration au peuple qui est sur elle, et l'esprit à ceux qui y marchent.
42:6	Moi YHWH, je t'ai appelé en justice, et je prendrai ta main et te garderai, et je t'établirai pour être l'alliance du peuple et la lumière des nations<!--Voir commentaire en Ge. 1:3-5.-->,
42:7	pour ouvrir les yeux des aveugles, pour faire sortir du cachot le prisonnier, et de la maison de détention ceux qui demeurent dans la ténèbre.

### Israël n'a pas été attentif à YHWH

42:8	Je suis YHWH, c'est là mon Nom, et je ne donnerai pas ma gloire à un autre, ni ma louange aux images gravées<!--Es. 48:11.-->.
42:9	Voici, les premières choses sont arrivées et je vous en annonce de nouvelles : avant qu'elles germent, je vous les fais entendre.
42:10	Chantez à YHWH un cantique nouveau, sa louange depuis l’extrémité de la Terre, vous qui descendez sur la mer et ce qui la remplit, vous les îles et vous qui les habitez ! 
42:11	Que le désert et ses villes élèvent la voix ! Que les villages où habite Qedar et ceux qui habitent dans les rochers poussent des cris de joie ! Qu'ils hurlent du sommet des montagnes !
42:12	Qu'on donne gloire à YHWH, et qu'on publie sa louange dans les îles !
42:13	YHWH sortira comme un homme vaillant, il réveillera sa jalousie comme un homme de guerre, il poussera des cris de joie, il poussera de grands cris, et il prévaudra sur ses ennemis.
42:14	Dès longtemps je suis resté tranquille, j'ai gardé le silence, je me suis contenu. Je crierai comme celle qui enfante, je soufflerai et je serai haletant à la fois.
42:15	Je réduirai les montagnes et les collines en ruines, et j'en dessécherai toute la verdure, je réduirai les fleuves en îles, et je ferai tarir les étangs.
42:16	Je conduirai les aveugles sur un chemin qu'ils ne connaissent pas, je les ferai marcher par des sentiers qu'ils ne connaissent pas. Je changerai devant eux les ténèbres en lumière et les endroits tortueux en terrain plat. Voilà ce que je ferai, et je ne les abandonnerai pas.
42:17	Ils reculeront et seront couverts de honte, ceux qui se confient aux idoles et qui disent aux images en métal fondu : Vous êtes nos elohîm !
42:18	Sourds, écoutez ! Et vous aveugles, regardez et voyez !
42:19	Qui est aveugle, sinon mon serviteur ? Et qui est sourd comme mon messager que j'envoie ? Qui est aveugle comme celui que j'ai comblé de grâces ? Qui est aveugle comme le serviteur de YHWH ?
42:20	Vous voyez beaucoup de choses, mais vous ne prenez garde à rien. Vous avez les oreilles ouvertes, mais vous n'entendez rien.
42:21	YHWH a pris plaisir, à cause de sa justice, à rendre la torah grande et majestueuse.
42:22	Mais c'est un peuple pillé et dépouillé ! On les a tous pris au piège dans des trous, cachés dans des maisons d'arrêt. Ils sont livrés au pillage et personne ne les délivre ! ils sont dépouillés et il n'y a personne pour dire : Restitue !
42:23	Qui parmi vous prêtera l'oreille à ces choses ? Qui s'y rendra attentif et l'écoutera à l'avenir ?
42:24	Qui a livré Yaacov au pillage et Israël aux pillards<!--Jg. 2:13-16.--> ? N'est-ce pas YHWH, contre lequel nous avons péché ? Car on n'a pas voulu marcher dans ses voies et on n'a pas obéi à sa torah.
42:25	C'est pourquoi il a déversé sur son peuple toute l'ardeur de sa colère et la férocité de la guerre. Elle l'a embrasé de tous côtés, mais il ne l'a pas reconnu. Et il l'a consumé, mais il ne l'a pas pris à cœur.

## Chapitre 43

### YHWH veut racheter Israël

43:1	Mais maintenant ainsi parle YHWH, qui t'a créé, Yaacov ! Celui qui t'a formé, Israël ! N'aie pas peur, car je te rachète. Je t'appelle par ton nom, tu es à moi !
43:2	Si tu passes par les eaux, je serai avec toi, et les fleuves, ils ne te submergeront pas. Si tu marches dans le feu, tu ne seras pas brûlé, et la flamme ne t'embrasera pas.
43:3	Car je suis YHWH, ton Elohîm, le Saint d'Israël, ton Sauveur. Je donne l'Égypte pour ta rançon, l'Éthiopie et Saba à ta place.
43:4	Parce que tu es précieux à mes yeux, tu es rendu honorable et je t'aime. Je donne des humains à ta place et des peuples pour ton âme.
43:5	N'aie pas peur, car je suis avec toi. Je ferai venir ta postérité de l'est et je te rassemblerai de l'occident.
43:6	Je dirai au septentrion : Donne ! Et au Théman : Ne retiens pas ! Fais venir mes fils de loin, et mes filles du bout de la Terre,
43:7	tous ceux qui s'appellent de mon Nom<!--Dans les Écritures, le Nom d'Elohîm le plus cité est YHWH. Yéhoshoua (Jésus), dont le nom signifie « YHWH est salut » correspond au nom et à l'identité qu'Elohîm a révélés à tous ceux qui l'ont rencontré quand il était sur Terre. Dans sa dernière prière à Gethsémané, Yéhoshoua dit : « J'ai manifesté ton Nom aux humains » (Jn. 17:6), et « Je leur ai fait connaître ton Nom » (Jn. 17:26). Ce Nom n'est autre que le sien puisque Yéhoshoua (YHWH est salut) était et est le Nom d'Elohîm. Moshé (Moïse) n'avait pas reçu la révélation de ce Nom (Ex. 3:13-14) car cette révélation était réservée à l'Assemblée (Église). En tant qu'épouse du Mashiah (Christ), l'Assemblée porte le Nom du Seigneur et bénéficie de l'autorité qu'il confère. Ainsi, Yéhoshoua est le seul Nom par lequel nous pouvons être sauvés (Ac. 4:12). C'est aussi en son Nom que nous devons être baptisés (Ac. 8:16, 19:5), que nous recevons l'exaucement de nos prières (Jn. 14:13-14, 16:24), que nous sommes délivrés de l'ennemi et obtenons la victoire sur le camp de l'ennemi (Mc. 16:17 ; Ph. 2:9-11).-->, que j'ai créés pour ma gloire, que j'ai formés, que j'ai faits.

### YHWH appelle ses témoins

43:8	Amène dehors le peuple aveugle qui a des yeux, et les sourds qui ont des oreilles.
43:9	Que toutes les nations soient réunies ensemble et que les peuples se rassemblent ! Lequel d'entre eux a proclamé cela et nous a fait connaître les choses anciennes ? Qu'ils produisent leurs témoins et qu'ils se justifient, qu'on les entende et qu'on dise : C'est vrai !
43:10	Vous êtes mes témoins<!--Ac. 1:8.-->, – déclaration de YHWH –, et mon serviteur que j'ai choisi, afin que vous connaissiez et que vous croyiez en moi. Comprenez, moi, lui ! Avant mes faces il n'a pas été formé de El, et il n'y en aura pas après moi.
43:11	Je suis, je suis YHWH, et en dehors<!--« Excepté », « sauf », « sans », « en outre ». Voir Es. 44:6,8, 45:6,21 ; Ps. 18:32.--> de moi il n'y a pas de Sauveur<!--YHWH dit qu'à part lui, il n'y a pas d'autres sauveurs. Or les écrits de la nouvelle alliance affirment que Yéhoshoua ha Mashiah est le seul Sauveur (Lu. 1:67-80, 19:10 ; Ac. 4:11-12).-->.
43:12	C'est moi qui ai proclamé ce qui devait arriver, qui vous ai sauvés, et qui vous ai fait entendre l'avenir, ce n'est pas un étranger parmi vous. Vous êtes mes témoins, – déclaration de YHWH –, que je suis El.
43:13	Et même avant que le jour fût, je suis, et il n'y a personne qui puisse délivrer de ma main. Je ferai l'œuvre, qui m'en empêchera ?

### YHWH fera une chose nouvelle, car Yaacov ne l'a pas honoré

43:14	Ainsi parle YHWH, votre Rédempteur<!--Es. 60:16 ; 1 Co. 1:30 ; Ro. 3:24 ; Ep. 1:7.-->, le Saint d'Israël : J'envoie par amour pour vous quelqu'un contre Babel, et je fais descendre tous les fugitifs, et on entendra le cri des Chaldéens sur les navires.
43:15	Je suis YHWH, votre Saint<!--Voir commentaire en Ac. 3:14.-->, le Créateur d'Israël, votre Roi.
43:16	Ainsi parle YHWH, qui donne un chemin dans la mer et un sentier dans les eaux puissantes,
43:17	qui fait sortir les chars et les chevaux, l'armée et ses vaillants guerriers. Ils se couchent ensemble pour ne plus se relever, ils sont anéantis, éteints comme une mèche.
43:18	Ne pensez plus aux choses passées, et ne considérez pas les choses anciennes.
43:19	Voici, je fais une chose nouvelle. Maintenant elle va germer. Ne la connaîtrez-vous pas ? Oui, je mettrai un chemin dans le désert et des fleuves dans le lieu de désolation.
43:20	Les bêtes des champs me glorifieront, les dragons et les filles de l'autruche, parce que j'aurai mis des eaux dans le désert, et des fleuves dans la solitude, pour abreuver mon peuple que j'ai élu.
43:21	Ce peuple que je me suis formé racontera mes louanges.
43:22	Mais tu ne m'as pas invoqué, Yaacov, car tu t'es lassé de moi, Israël !
43:23	Tu ne m'as pas apporté d'agneaux en holocauste, et tu ne m'as pas honoré par tes sacrifices. Je ne t'ai pas asservi pour me faire des offrandes, et je ne t'ai pas fatigué pour de l'encens.
43:24	Tu ne m'as pas acheté à prix d'argent du roseau aromatique, et tu ne m'as pas rassasié de la graisse de tes sacrifices. Mais tu m'as asservi par tes péchés, et tu m'as peiné par tes iniquités.
43:25	C'est moi, c'est moi qui efface tes transgressions pour l'amour de moi, et je ne me souviendrai plus de tes péchés.
43:26	Réveille ma mémoire, et plaidons ensemble, fais toi-même le compte afin que tu sois justifié !
43:27	Ton premier père a péché, et tes docteurs se sont rebellés contre moi.
43:28	C'est pourquoi j'ai profané les chefs du lieu saint, et j'ai livré Yaacov comme quelqu'un qui est voué à une entière destruction, et Israël à l'opprobre.

## Chapitre 44

### Promesse de l'Esprit, folie de l'idolâtrie

44:1	Maintenant écoute, Yaacov, mon serviteur, Israël, toi que j'ai choisi ! 
44:2	Ainsi parle YHWH, qui t'a fait et formé dès le ventre, celui qui te soutient : Ne crains rien, Yaacov, mon serviteur, Yeshouroun<!--Yeshouroun : « celui qui est droit ». Il s'agit du nom symbolique d'Israël décrivant son caractère idéal.-->, toi que j'ai choisi.
44:3	Car je répandrai des eaux sur celui qui est altéré, et des rivières sur la terre sèche. Je répandrai mon Esprit sur ta postérité, et ma bénédiction sur tes descendants.
44:4	Ils germeront parmi l’herbe, comme les saules auprès des courants d'eau.
44:5	Celui-ci dira : Je suis à YHWH ! Celui-là s'appellera du nom de Yaacov. Cet autre écrira de sa main : Je suis à YHWH ! Et il prendra pour surnom le nom d'Israël.
44:6	Ainsi parle YHWH, le Roi d'Israël et son Rédempteur, YHWH Tsevaot : Je suis le premier, et je suis le dernier. En dehors de moi, il n'y a pas d'Elohîm<!--Ap. 1:8, 21:6, 22:13.-->.
44:7	Et qui, comme moi, a appelé, déclaré et ordonné cela, depuis que j'ai établi le peuple ancien ? Qu'ils déclarent les choses à venir, les choses qui arriveront ci-après !
44:8	Ne tremblez pas et n'ayez pas de crainte ! Ne te l'ai-je pas fait entendre, et déclaré dès ce temps-là ? Vous êtes mes témoins : y a-t-il un autre Éloah en dehors de moi ? Non, il n'y a pas de Rocher<!--YHWH dit qu'il ne connaît pas d'autre rocher. Voir 2 S. 22:32, 23:3. Yéhoshoua ha Mashiah est ce Rocher qui suivait les Hébreux dans le désert (Ps. 18:32 ; Mt. 16:18 ; 1 Co. 10:1-4. Voir aussi commentaire en Es. 8:14).-->, je n'en connais pas !
44:9	Les ouvriers d'idoles ne sont tous que tohu, et leurs choses les plus désirables ne sont d'aucun profit. Elles le témoignent elles-mêmes, elles ne voient rien et ne connaissent rien, afin qu'ils soient honteux.
44:10	Mais qui est-ce qui fabrique un el, ou fond une idole, pour n'en avoir aucun profit ?
44:11	Voici, tous ses compagnons seront honteux, car tous ces ouvriers ne sont que des humains. Qu'ils se rassemblent tous, qu'ils se tiennent là ! Ils seront effrayés et rendus honteux tous ensemble.
44:12	Le forgeron fabrique une hache sur des charbons, il la façonne à coups de marteau, il la travaille à la force de son bras. Mais dès qu'il a faim, il n'a plus de force, et s'il ne boit pas d'eau il est fatigué.
44:13	L'artisan sur bois étend sa règle, il la dessine avec le stylet, il la façonne au ciseau, il la dessine au compas. Il la façonne en prenant pour modèle un homme, la beauté d'un être humain, afin qu'elle habite dans une maison.
44:14	Il se coupe des cèdres, il prend des cyprès et des chênes qu'il a laissés croître parmi les arbres de la forêt, il plante des pins et la pluie les fait croître.
44:15	L'être humain se sert de ces arbres pour les brûler car il en prend une partie pour se chauffer, il en fait aussi du feu pour cuire son pain. Avec ça il fabrique aussi un el et se prosterne, il en fait une idole et l'adore !
44:16	Il en brûle au feu la moitié afin de pouvoir manger de la viande, préparer un rôti et se rassasier. Il l'utilise aussi pour se chauffer en disant : Ah ! Je me chauffe, je vois la flamme !
44:17	Puis avec le reste il fait un el pour être son idole ! Il se prosterne devant elle et l'adore, il lui adresse des prières en disant : Délivre-moi, car tu es mon El !
44:18	Ils ne savent et ne discernent rien, car on leur a plâtré les yeux afin qu'ils ne voient pas, et les cœurs pour qu'ils ne comprennent pas.
44:19	Il ne prend rien à cœur<!--So. 2:1 ; 2 Co. 13:5.-->, il n'a ni connaissance ni intelligence pour dire : J'en ai brûlé une partie au feu, et même j'ai cuit du pain sur les charbons, j'ai rôti de la viande et je l'ai mangée, et ferai-je du reste une abomination ? Me prosternerai-je devant ce qui provient d'un arbre ?
44:20	Il se nourrit de cendres, son cœur trompé l'égare, il ne délivrera pas son âme et ne dira pas : N'est-ce pas du mensonge que j'ai dans ma main droite ?

### YHWH rachète son peuple

44:21	Souviens-toi de ces choses, Yaacov, Israël, car tu es mon serviteur. Je t'ai formé, tu es mon serviteur, Israël, je ne t'oublierai pas.
44:22	J'efface tes transgressions comme une nuée épaisse, et tes péchés comme une nuée. Reviens à moi, car je t'ai racheté.
44:23	Cieux, poussez des cris de joie, car YHWH a agi. Parties inférieures de la Terre, poussez des cris de triomphe ! Montagnes, éclatez en cris de joie ! Vous aussi, forêts, avec tous vos arbres, car YHWH a racheté Yaacov, il s'est glorifié en Israël.
44:24	Ainsi parle YHWH, ton Rédempteur, celui qui t'a formé dès le ventre : Moi, YHWH je fais toute chose. Seul, j'ai étendu les cieux, par moi-même j'ai aplani la Terre.
44:25	Je fais échouer les signes des menteurs, je rends insensés les devins, je fais retourner en arrière les sages et je change leur connaissance en folie.
44:26	Je confirme la parole de mon serviteur et j'accomplis le conseil de mes messagers. Je dis de Yeroushalaim : Tu seras habitée ! Et aux villes de Yéhouda : Vous serez rebâties ! Et je redresserai ses désolations.
44:27	Je dis aux profondeurs de l'océan : Asséchez-vous ! Je mettrai vos fleuves à sec.

### Prophétie sur le rétablissement d'Israël par Cyrus

44:28	Je dis de Cyrus<!--Yesha`yah prophétisa la destruction de Babel (Babylone) deux siècles avant la réalisation de cet événement qui eut lieu le 5 octobre 539 av. J.-C. Fait remarquable : il précisa même le nom du commandant Cyrus qui dompta le lion babylonien. L'historien Hérodote (480 – 425 av. J.-C.) donnera par ailleurs raison au prophète sur le déroulement de la prise de Babel.--> : Il est mon berger et il accomplira tous mes désirs. Il dira à Yeroushalaim : Tu seras rebâtie ! Et au temple : Tu seras fondé !

## Chapitre 45

### Cyrus suscité par YHWH

45:1	Ainsi parle YHWH à son mashiah, à Cyrus<!--Cyrus le Grand (580 – 530 av. J.-C.). Voir Esd. 1.-->
45:2	que je tiens par la main droite, pour terrasser les nations devant lui, pour délier les reins des rois, pour ouvrir devant lui les portes afin qu'elles ne soient plus fermées.
45:3	Je marcherai devant toi et j'aplanirai les lieux tortueux. Je briserai les portes en cuivre et je mettrai en pièces les barres en fer. Je te donnerai des trésors des ténèbres<!--Lieu caché.-->, des trésors des lieux secrets, afin que tu saches que je suis YHWH, l'Elohîm d'Israël, qui t'appelle par ton nom.
45:4	Pour l'amour de Yaacov, mon serviteur, et d'Israël mon élu, je t'ai appelé par ton nom. Je t'ai donné un titre flatteur alors que tu ne me connaissais pas.

### YHWH, le seul Elohîm

45:5	C'est moi YHWH, et il n'y en a pas d'autre. Moi excepté, il n'y a pas d'Elohîm<!--De. 4:35 ; Es. 45:22, 46:9.-->. Je t'ai ceint avant que tu me connaisses,
45:6	afin que l'on sache, du soleil levant au soleil couchant, qu'il n'y a rien en dehors de moi. C'est moi YHWH, et il n'y en a pas d'autre.
45:7	Je forme la lumière et je crée les ténèbres, je fais la paix et je crée le mal<!--« Mauvais », « mal », « malheur ».-->. Moi, YHWH, je fais toutes ces choses.
45:8	Que les cieux distillent d’en haut, que les nuages laissent couler la justice ! Que la terre s’ouvre et produise le salut et fasse germer la justice en même temps ! Moi, YHWH, je crée ces choses.
45:9	Malheur à celui qui conteste avec celui qui l'a façonné ! Vase parmi des vases de terre ! L'argile dira-t-elle à celui qui l'a façonnée : Que fais-tu ? ou : Ton œuvre n'a pas de mains<!--Jé. 18:6 ; Ro. 9:21.--> ?
45:10	Malheur à celui qui dit à son père : Pourquoi engendres-tu ? Et à sa mère : Pourquoi enfantes-tu ?
45:11	Ainsi parle YHWH, le Saint d'Israël, qui est son Créateur : M'interrogerez-vous sur les choses à venir ? Me donnerez-vous des ordres au sujet de mes fils et de l'œuvre de mes mains ?
45:12	C'est moi qui ai fait la Terre et qui ai créé l'être humain sur elle. C'est moi qui ai étendu les cieux de mes mains, et c'est moi qui donne des ordres à toute leur armée.
45:13	C'est moi qui l'ai suscité dans ma justice, qui aplanirai toutes ses voies. C'est lui qui rebâtira ma ville, qui renverra mes captifs<!--Cyrus le Grand libéra les Juifs après 70 ans de captivité (Esd. 1).--> sans prix ni pots-de-vin, dit YHWH Tsevaot.

### Les autres peuples reconnaîtront la main de YHWH sur Israël

45:14	Ainsi parle YHWH : Le produit du travail de l'Égypte, les gains de l'Éthiopie et des Sabéens, ces hommes de grande stature, passeront chez toi et seront à toi. Ils marcheront derrière toi, ils passeront enchaînés. Ils se prosterneront devant toi et te prieront en disant : Certainement, El est au milieu de toi et il n'y a pas d'autre Elohîm que lui.
45:15	En vérité, tu es le El qui te caches, l'Elohîm d'Israël, le Sauveur.
45:16	Ils sont tous honteux et confus, ils s'en vont tous avec ignominie, les fabricants d'idoles.
45:17	Israël a été sauvé par YHWH, d'un salut éternel<!--Hé. 5:9.-->. Vous ne serez ni honteux ni confus pour l'éternité et à jamais.
45:18	Car ainsi parle YHWH qui a créé les cieux, lui qui est l'Elohîm qui a formé la Terre, qui l'a faite et qui l'a affermie, qui ne l’a pas créée tohu<!--« Informe », « confusion », « chose irréelle », « vide ». Voir commentaire en Ge. 1:2.-->, qui l'a formée pour qu'elle soit habitée : Je suis YHWH, et il n'y en a pas d'autre.
45:19	Je n'ai pas parlé en secret ni dans un lieu ténébreux de la Terre, je n'ai pas dit à la postérité de Yaacov : Cherchez-moi dans le tohu ! Je suis YHWH, qui prononce ce qui est juste, qui déclare ce qui est droit.
45:20	Rassemblez-vous et venez, approchez-vous ensemble, vous les rescapés des nations ! Ceux qui portent le bois de leur idole n'ont aucune connaissance, ils invoquent un el qui ne sauve pas.
45:21	Déclarez-le et faites-les approcher ! Qu'ils prennent conseil ensemble ! Qui a fait entendre ces choses dès l'origine, et les a déclarées dès longtemps ? N'est-ce pas moi, YHWH ? Il n'y a pas d'autre Elohîm en dehors de moi. Le El-Tsaddiq<!--Le El Juste.--> et Sauveur, il n'y en a pas, excepté moi !
45:22	Vous tous, aux extrémités de la Terre, regardez vers moi et soyez sauvés ! Car je suis El, il n'y en a pas d'autre<!--De. 4:35 ; Es. 46:9.-->.
45:23	Je le jure par moi-même, la parole est sortie avec justice de ma bouche, et elle ne sera pas révoquée : Tout genou fléchira devant moi, toute langue jurera<!--Ph. 2:9-11.-->.
45:24	En effet, on dira à propos de moi : C'est en YHWH seul que se trouvent la justice et la force. À lui viendront, confondus, tous ceux qui s'irritaient contre lui.
45:25	Toute la postérité d'Israël sera justifiée, et elle se glorifiera en YHWH.

## Chapitre 46

### La puissance de YHWH, l'incapacité des idoles

46:1	Bel<!--elohîm protecteur de Babylone. Bel est la contraction de Baal.--> se plie, Nebo<!--Une divinité de Babylone qui veillait sur l'enseignement et les lettres; correspond au Grec Hermès, au Latin Mercure, à l'Egyptien Toth.--> s'accroupit. Leurs idoles sont mises sur des animaux et sur des bêtes, sont devenues des fardeaux portés, des charges sur du bétail lassé !
46:2	Elles se sont accroupies, elles se sont pliées ensemble sur leurs genoux, et ne peuvent échapper au fardeau, mais leur âme s’en ira en captivité.
46:3	Écoutez-moi, maison de Yaacov, et vous tous, reste de la maison d'Israël ! Je me suis chargé de vous dès le ventre, et je vous ai portés dès le sein maternel.
46:4	Jusqu'à votre vieillesse, moi, lui. Je vous soutiendrai jusqu'à vos cheveux gris. Je l'ai fait, et je vous porterai encore, je vous soutiendrai et vous sauverai.
46:5	À qui me ferez-vous ressembler pour le faire mon égal<!--Ph. 2:6.--> ? À qui me comparerez-vous pour que nous soyons semblables ?
46:6	Ils versent l'or de leur bourse, et pèsent l'argent à la balance, et ils engagent un orfèvre pour en faire un el, puis ils se prosternent et l'adorent !
46:7	Ils le portent, ils le chargent sur les épaules, ils le déposent à sa place et il se tient debout. Il ne bouge pas de son lieu ! Puis on crie vers lui, mais il ne répond pas, et il ne les sauve pas de leur détresse.
46:8	Souvenez-vous de cela et montrez votre virilité<!--« Soyez des hommes », « champion », « grand homme ».--> ! Retournez-le dans votre cœur, transgresseurs !
46:9	Souvenez-vous des premières choses d'autrefois ! Je suis El, et il n'y en a pas d'autre<!--De. 4:35 ; Es. 45:22.-->. Je suis Elohîm et il n'y en a pas comme moi.
46:10	Je déclare dès le commencement ce qui doit arriver à la fin, et longtemps auparavant, les choses qui n'ont pas encore été faites. Je dis : Mon conseil s'accomplira et je ferai tout ce que je désire.
46:11	J'appelle de l'est l'oiseau de proie, et d'une terre éloignée un homme pour exécuter mon conseil. Oui, j'ai parlé, et je le ferai arriver, je l'ai formé, je l'accomplirai aussi.
46:12	Écoutez-moi, vous qui avez le cœur endurci et qui êtes éloignés de la justice.
46:13	Je fais approcher ma justice, elle n'est pas loin, mon salut, il ne tardera pas. Je mettrai le salut en Sion pour Israël, qui est ma gloire.

## Chapitre 47

### Jugement sur Babel

47:1	Descends, et assieds-toi dans la poussière, vierge, fille de Babel ! Assieds-toi à terre, il n'y a plus de trône pour la fille des Chaldéens ! Car tu ne te feras plus appeler la douce et la délicate.
47:2	Prends les meules et mouds de la farine ! Découvre ton voile<!--Voir Ca. 4:1,3, 6:7.-->, retrousse la traîne de ta robe, découvre tes jambes et traverse les fleuves !
47:3	Ta nudité sera découverte et ta honte sera vue. Je prendrai vengeance, je ne t'affronterai pas comme un être humain.
47:4	Quant à notre Rédempteur, son Nom est YHWH Tsevaot, le Saint d'Israël.
47:5	Assieds-toi en silence et entre dans les ténèbres, fille des Chaldéens, car tu ne te feras plus appeler la maîtresse des royaumes.
47:6	J'ai été embrasé de colère contre mon peuple, j'ai profané mon héritage, c'est pourquoi je les ai livrés entre tes mains, tu n'as pas usé de miséricorde envers eux, tu as durement appesanti ton joug sur le vieillard.
47:7	Et tu as dit : Je serai maîtresse pour toujours ! Si bien que tu n'as pas pris ces choses à cœur, tu ne t'es pas souvenue que cela prendrait fin.
47:8	Maintenant écoute ceci, toi voluptueuse qui habites avec assurance, et qui dis en ton cœur : C'est moi, et il n'y en a pas d'autre que moi ! Je ne deviendrai pas veuve, et je ne saurai pas ce que c'est que d'être privée d'enfants !
47:9	Mais ces deux choses t'arriveront en un instant, en un jour : la privation d'enfants et le veuvage. Elles viendront sur toi dans leur perfection, malgré le grand nombre de tes sorcelleries, malgré toute la puissance de tes incantations<!--Ap. 18:7-8.-->.
47:10	Tu t'es confiée dans ta méchanceté, et tu disais : Personne ne me voit ! Ta sagesse et ta connaissance t'ont pervertie, et tu disais en ton cœur : C'est moi, et il n'y en a pas d'autre que moi !
47:11	C'est pourquoi le malheur viendra sur toi sans que tu en connaisses l'aurore, le désastre qui tombera sur toi sera tel, que tu ne pourras pas l'apaiser<!--Vient de l'hébreu « kaphar » qui signifie « couvrir », « purger », « faire une expiation », « réconciliation », « recouvrir de poix » ou encore « propitiation ».-->, la dévastation viendra sur toi soudainement, sans que tu t'en aperçoives.
47:12	Tiens-toi maintenant avec tes incantations, et avec le grand nombre de tes sorcelleries, dans lesquelles tu as travaillé dès ta jeunesse ! Peut-être pourras-tu en tirer quelque profit, peut-être sauras-tu faire trembler !
47:13	Tu t'es lassée de la multitude de tes conseils. Qu'ils se présentent maintenant et qu'ils te sauvent, ces astrologues qui observent les étoiles et les cieux, qui, aux nouvelles lunes, te font connaître ce qui doit t'arriver !
47:14	Voici, ils sont devenus comme de la paille, le feu les consume, ils ne délivreront pas leur âme du pouvoir de la flamme. Il n'y a ni charbon pour se chauffer, ni feu pour s'asseoir en face.
47:15	Ainsi sont-ils pour toi, ceux pour qui tu t'es fatiguée. Ceux avec lesquels tu as trafiqué dès ta jeunesse erreront chacun de son côté comme un vagabond : il n'y a personne pour te sauver.

## Chapitre 48

### YHWH rappelle ses promesses

48:1	Écoutez ceci, maison de Yaacov, vous qui vous appelez du nom d'Israël, vous qui êtes sortis des eaux de Yéhouda, vous qui jurez par le Nom de YHWH et qui faites mention de l'Elohîm d'Israël, mais non pas conformément à la vérité et à la justice<!--Jé. 5:2.--> !
48:2	Car ils prennent leur nom de la sainte cité, et ils s'appuient sur l'Elohîm d'Israël, dont le Nom est YHWH Tsevaot<!--Ex. 20:7.-->.
48:3	J'ai déclaré les premières choses dès le commencement, elles sont sorties de ma bouche et je les ai proclamées ; soudain je les ai faites, elles sont arrivées.
48:4	Parce que je savais que tu es obstiné, que ton cou est une barre de fer, et que ton front est en cuivre,
48:5	je t'ai déclaré ces choses dès lors, je les ai fait entendre avant qu'elles arrivent, de peur que tu ne dises : C'est mon idole qui a fait toutes ces choses, c'est mon idole ou mon image en métal fondu qui les a ordonnées.
48:6	Tu l'entends ! Vois tout cela ! Et vous, ne l'annoncerez-vous pas ? Je te fais entendre dès maintenant des choses nouvelles, des choses tenues secrètes, que tu ne connaissais pas.
48:7	Elles sont créées maintenant, et non pas dès cette époque-là, et avant ce jour tu ne les as jamais entendues, de peur que tu ne dises : Voici, je le savais !
48:8	Tu n'en avais pas entendu parler, tu n'en savais rien et ton oreille n'a pas été ouverte longtemps avant, car je savais que tu trahirais, que tu trahirais ; aussi tu as été appelé transgresseur dès le ventre.
48:9	Pour l'amour de mon Nom, je suspends ma colère, et pour l'amour de ma louange, je retiens mon courroux contre toi, afin de ne pas te retrancher.
48:10	Voici, je t'ai épuré, mais non comme l'argent, je t'ai choisi au creuset de l'affliction.
48:11	C'est pour l'amour de moi, pour l'amour de moi que j'agis en effet. Comment me laisserais-je profaner ! Je ne donnerai pas ma gloire à un autre.
48:12	Écoute-moi, Yaacov, Israël, mon appelé ! Moi, lui ; moi, le premier et le dernier<!--Ap. 1:17-18, 22:13.--> aussi.
48:13	Oui, c’est ma main qui a fondé la Terre, ma droite qui a étendu les cieux. Quand je les appelle, ils se tiennent là ensemble.
48:14	Vous tous, rassemblez-vous et écoutez ! Lequel parmi eux a déclaré ces choses ? Celui que YHWH aime exécutera ce qu'il désire contre Babel, et son bras sera contre les Chaldéens.
48:15	Moi, moi, j’ai parlé, je l’ai aussi appelé. Je l'ai fait venir et il réussit dans sa voie.
48:16	Approchez-vous de moi et écoutez ceci ! Depuis le commencement, je n'ai pas parlé en cachette, depuis le temps où ces choses sont arrivées, je suis là ! Maintenant, Adonaï YHWH m'a envoyé avec son Esprit.
48:17	Ainsi parle YHWH, ton Rédempteur, le Saint d'Israël : Je suis YHWH, ton Elohîm, qui t'enseigne pour ton profit, et qui te guide dans le chemin où tu dois marcher.
48:18	Si tu étais attentif à mes commandements, ta paix serait comme un fleuve, et ta justice comme les flots de la mer<!--Jos. 1:8 ; Ps. 1:2 ; Jn. 14:21 ; Ja. 1:22.-->.
48:19	Ta postérité deviendrait comme le sable, et les rejetons de tes entrailles comme les grains de sable<!--Ge. 15:5, 22:17, 32:12.-->. Son nom ne serait ni retranché ni détruit en face de moi.
48:20	Sortez de Babel<!--Voir Jé. 51:6 et Ap. 18:4.-->, fuyez loin des Chaldéens ! Publiez ceci avec une voix de chant de triomphe, annoncez-le, portez ceci jusqu'aux extrémités de la Terre, dites : YHWH a racheté son serviteur Yaacov !
48:21	Ils n’ont pas eu soif quand il les a fait marcher dans les lieux laissés en ruine : du rocher il a fait ruisseler pour eux de l'eau, il a fendu le rocher<!--1 Co. 10:4.--> et l'eau a coulé.
48:22	Il n'y a pas de paix pour les méchants, dit YHWH.

## Chapitre 49

### Le Mashiah, la lumière de tous les peuples

49:1	Îles, écoutez-moi ! Soyez attentifs, vous peuples éloignés ! YHWH m'a appelé dès le ventre, il a fait mention de mon nom dès les entrailles de ma mère<!--Jé. 1:5 ; Ps. 139:16.-->.
49:2	Il a rendu ma bouche semblable à une épée tranchante. Il m'a caché dans l'ombre de sa main, et m'a rendu semblable à une flèche bien polie, il m'a caché dans son carquois.
49:3	Il m'a dit : Tu es mon serviteur, Israël, en qui je me glorifierai.
49:4	Et moi j'ai dit : C'est en vain que j'ai travaillé, c'est pour le tohu, pour du vent que j'ai consumé ma force. En vérité, mon droit est auprès de YHWH et ma récompense<!--Ou le salaire. Voir Es. 40:10.--> auprès de mon Elohîm.
49:5	Et maintenant, dit YHWH, qui m’a formé dès le ventre pour être son serviteur, pour ramener à lui Yaacov et pour qu’Israël soit rassemblé près de lui. Je serai honoré aux yeux de YHWH, et mon Elohîm deviendra ma force.
49:6	Il dit : C'est peu de chose que tu sois mon serviteur pour relever les tribus de Yaacov et pour ramener les préservés d'Israël : c'est pourquoi je t'ai donné pour lumière aux nations, afin que tu sois mon salut jusqu'aux extrémités de la Terre.
49:7	Ainsi parle YHWH, le Rédempteur, le Saint d'Israël, à celui dont l'âme est méprisée, celui que la nation a en abomination, l’esclave des dominateurs : Les rois le verront et se lèveront, les princes se prosterneront à cause de YHWH, qui est fidèle, du Saint d'Israël, qui t'a choisi.
49:8	Ainsi parle YHWH : Je t'ai répondu au temps de la faveur, et je t'ai secouru au jour du salut<!--Yeshuw`ah.-->. Je te garderai, et je te donnerai pour être l'alliance du peuple, pour relever la terre, afin que tu possèdes les héritages dévastés,
49:9	en disant à ceux qui sont emprisonnés : Sortez ! Et à ceux qui sont dans les ténèbres : Découvrez-vous ! Ils paîtront sur les chemins, et leurs pâturages seront sur tous les lieux élevés.
49:10	Ils n'auront pas faim et ils n'auront pas soif, la terre desséchée et le soleil ne les frapperont plus. Car celui qui a pitié d'eux les guidera, et il les conduira vers des sources d'eaux<!--Ps. 121:6 ; Lu. 1:67-79.-->.
49:11	Je transformerai toutes mes montagnes en chemins, et mes grandes routes seront élevées.
49:12	Voici, ceux-ci viendront de loin, et voici, ceux-là, du nord et de l'ouest, et ceux-ci, de la terre de Sinim.
49:13	Cieux, poussez des cris de joie ! Terre, réjouis-toi ! Montagnes, éclatez en cris de joie ! Car YHWH console son peuple, il a compassion de ceux qu'il a affligés.
49:14	Sion disait : YHWH m'abandonne, Adonaï m'oublie !
49:15	Une femme oublie-t-elle son nourrisson ? N'a-t-elle pas pitié du fils de ses entrailles ? Même si elle l'oubliait, moi je ne t'oublierai jamais.
49:16	Voici, je t'ai gravé sur mes paumes ! Tes murs sont continuellement devant moi.
49:17	Tes fils se hâtent de revenir, mais ceux qui te détruisaient et ceux qui te réduisaient en désert, s'éloignent de toi.
49:18	Lève tes yeux et regarde tout autour : tous se rassemblent, ils viennent à toi. Je suis vivant, – déclaration de YHWH –, tu te revêtiras de tous comme d'une parure et tu t'en ceindras comme une épouse.
49:19	Car tes lieux laissés en ruine, dévastés, et ta terre détruite seront désormais trop étroits pour ses habitants, et ceux qui t'engloutissaient s'éloigneront.
49:20	Les fils que tu as eus quand tu étais privée d’enfants diront encore à tes oreilles : Le lieu est trop étroit pour moi, fais-moi de la place pour que je puisse y demeurer.
49:21	Tu diras en ton cœur : Qui m'a engendré ceux-ci ? Moi, j’étais privée d’enfants, stérile, en exil et emmenée au loin. Et ceux-ci, qui les a fait grandir ? Voici, moi j’étais laissée seule. Ceux-ci, où étaient-ils ?
49:22	Ainsi parle Adonaï YHWH : Voici, je lèverai ma main vers les nations et je dresserai ma bannière vers les peuples, et ils ramèneront tes fils entre leurs bras, et ils porteront tes filles sur les épaules.
49:23	Les rois deviendront tes nourriciers et leurs princesses tes nourrices. Ils se prosterneront devant toi le visage contre terre, et ils lécheront la poussière de tes pieds. Et tu sauras que je suis YHWH, et que ceux qui se confient en moi ne seront pas confus<!--Ps. 22:5-6, 69:7 ; Ro. 9:33 ; 1 Pi. 2:6.-->.
49:24	Le butin de l'homme vaillant lui sera-t-il enlevé ? Et les captifs du juste seront-ils délivrés ?
49:25	Car ainsi parle YHWH : Même les captifs pris par l'homme vaillant lui seront enlevés et le butin du terrifiant lui échappera, car je plaiderai moi-même avec ceux qui plaident contre toi et je délivrerai tes enfants.
49:26	Je ferai manger leur propre chair à ceux qui t'oppriment, ils s'enivreront de leur sang comme d'un vin doux et toute chair saura que je suis YHWH, ton Sauveur, ton Rédempteur, le Puissant de Yaacov.

## Chapitre 50

### Avertissements de YHWH par son serviteur

50:1	Ainsi parle YHWH : Où est la lettre de divorce par laquelle j'ai répudié votre mère<!--De. 24:1 ; Jé. 3:8 ; Mt. 5:31.--> ? Ou bien, auquel de mes créanciers vous ai-je vendus ? Voici, vous avez été vendus à cause de vos iniquités, et votre mère a été répudiée à cause de vos transgressions.
50:2	Pourquoi ? Je suis venu, et pas d’homme ! J'ai appelé, et personne n'a répondu. Ma main est-elle courte, courte pour racheter<!--No. 11:23 ; Es. 59:1.--> ? Ou n'y a-t-il plus de force en moi pour délivrer ? Voici, par ma menace, je dessèche la mer, je réduis les fleuves en désert. Leurs poissons sentent mauvais faute d'eau, et ils meurent de soif.
50:3	J'habille les cieux de ténèbres et je leur mets un sac pour couverture.
50:4	Adonaï YHWH m'a donné la langue de disciple<!--Vient d'un mot hébreu qui signifie « enseigné, appris, traité en disciple, exercé ».--> pour que je sache soutenir par la parole celui qui est fatigué<!--Job 6:14 ; 1 Th. 5:14.-->. Matin après matin, il réveille, il réveille mon oreille pour que j'écoute comme un disciple.
50:5	Adonaï YHWH m'a ouvert l'oreille et je n'ai pas été rebelle, et je ne me suis pas retiré en arrière.
50:6	J'ai exposé mon dos à ceux qui me frappaient et mes joues à ceux qui m'arrachaient la barbe, je n'ai pas caché mes faces aux insultes et aux crachats<!--Mt. 5:39, 26:67 ; Lu. 6:29, 18:32.-->.
50:7	Mais Adonaï YHWH m'a aidé, c'est pourquoi je n'ai pas été humilié. C’est pourquoi, j’ai rendu mes faces semblables au rocher<!--Ez. 3:8-9.-->, car je sais que je ne serai pas honteux.
50:8	Celui qui me justifie est proche : qui plaidera contre moi ? Comparaissons ensemble ! Qui est mon adversaire ? Qu'il s'approche de moi !
50:9	Voici, Adonaï YHWH m'aidera, qui me condamnera ? Voici, tous seront usés comme un vêtement, les mites les dévoreront.
50:10	Qui d’entre vous craint YHWH, qui obéit à la voix de son serviteur ? Que celui qui marche dans l'obscurité, et qui n’a pas d'éclat, se confie dans le Nom de YHWH, qu'il s'appuie sur son Elohîm.
50:11	Voici, vous tous qui allumez le feu, et qui vous ceignez d'étincelles, marchez à la lumière de votre feu et des étincelles que vous avez allumées ! Voici ce que vous aurez de ma main : vous vous coucherez dans la terreur.

## Chapitre 51

### Exhortation à ceux qui recherchent YHWH

51:1	Écoutez-moi, vous qui poursuivez la justice et qui cherchez YHWH ! Regardez au rocher d'où vous avez été taillés, au trou de la citerne d’où vous avez été tirés.
51:2	Regardez à Abraham, votre père, et à Sarah qui vous a enfantés ! Car lui seul je l'ai appelé, je l'ai béni et multiplié<!--Ro. 4:1-16 ; Hé. 11:8-12.-->.
51:3	Car YHWH consolera Sion, il consolera tous ses lieux laissés en ruine, il rendra son désert semblable à Éden, et sa région aride à un jardin de YHWH. En elle sera trouvée la joie et l'allégresse, les actions de grâces et le son de mélodie.
51:4	Écoutez-moi attentivement, mon peuple ! Prêtez-moi l'oreille, vous ma nation ! Car la torah sortira de moi, et j'établirai mon jugement pour être la lumière des peuples.
51:5	Ma justice est proche, mon salut va paraître, et mes bras jugeront les peuples. Les îles espéreront en moi, elles se confieront en mon bras.
51:6	Levez les yeux vers les cieux et regardez en bas sur la Terre ! Car les cieux s'évanouiront comme la fumée, et la Terre tombera en lambeaux comme un vêtement, et ses habitants périront pareillement. Mais mon salut demeurera éternellement, et ma justice ne sera pas anéantie.
51:7	Écoutez-moi, vous qui connaissez la justice, peuple dans le cœur duquel est ma torah ! N'ayez pas peur de l'insulte des mortels et ne soyez pas effrayés devant leurs outrages.
51:8	Car les mites les dévoreront comme un vêtement<!--Mt. 6:19 ; Lu. 12:33 ; Ja. 5:2.-->, et la gerce les dévorera comme de la laine. Mais ma justice demeurera toujours, et mon salut d'âges en âges.
51:9	Réveille-toi, réveille-toi, revêts-toi de force, bras de YHWH ! Réveille-toi comme aux jours anciens, aux siècles passés. N'es-tu pas celui qui tailla en pièce l'Égypte, et qui blessa mortellement le dragon ?
51:10	N'est-ce pas toi qui fis tarir la mer, les eaux du grand abîme ? Qui réduisis les lieux les plus profonds de la mer en un chemin afin que les rachetés y passent ?
51:11	Ainsi ceux pour qui YHWH aura payé la rançon retourneront, ils iront à Sion avec des cris de joie, une joie éternelle sera sur leurs têtes. La joie et l'allégresse les saisiront, la douleur et le gémissement s'enfuiront.
51:12	C'est moi, c'est moi qui vous console. Qui es-tu pour avoir peur du mortel qui mourra, du fils de l'être humain qui sera donné à l’herbe ?
51:13	Tu oublies YHWH qui t'a fait, qui a étendu les cieux et fondé la Terre ! Tout le jour, tu trembles continuellement, en face de la fureur de l'oppresseur, quand il se prépare à détruire ! Où est la fureur de l'oppresseur ?
51:14	Rapidement celui qui est courbé sera libéré, il ne mourra pas dans la fosse, il ne manquera plus de pain.
51:15	Moi, je suis YHWH, ton Elohîm, qui fend la mer et les flots rugissants. YHWH Tsevaot est son Nom.
51:16	Je mets mes paroles dans ta bouche et je te couvre de l'ombre de ma main pour établir les cieux et fonder la Terre, pour dire à Sion : Tu es mon peuple !
51:17	Réveille-toi ! Réveille-toi ! Lève-toi, Yeroushalaim, qui as bu de la main de YHWH la coupe de sa fureur, tu as bu, tu as sucé la lie de la coupe d'étourdissement<!--Ps. 60:5 ; Ap. 14:10.--> !
51:18	Il n'y en a aucun pour la conduire, de tous les fils qu'elle a enfantés, il n'y en a aucun qui la prenne par la main, de tous les fils qu'elle a élevés.
51:19	Ces deux choses te sont arrivées, qui te plaindra ? Le ravage et la ruine, la famine et l'épée. Par qui te consolerai-je ?
51:20	Tes fils se sont évanouis, ils sont couchés à la tête de toutes les rues comme une antilope dans un filet, pleins de la fureur de YHWH, de la menace de ton Elohîm.
51:21	C'est pourquoi, écoute maintenant ceci, affligée, ivre, mais non pas de vin.
51:22	Ainsi parle YHWH, ton Seigneur et ton Elohîm, qui plaide la cause de son peuple : Voici, je prends de ta main la coupe d'étourdissement, la lie de la coupe de ma fureur, tu n'en boiras plus désormais !
51:23	Car je la mettrai dans la main de ceux qui t'ont affligée, qui disaient à ton âme : Courbe-toi, et nous passerons ! C'est pourquoi tu as exposé ton corps comme la terre, comme une rue pour les passants.

## Chapitre 52

### Le réveil de Yeroushalaim (Jérusalem), la ville sainte

52:1	Réveille-toi, réveille-toi, Sion ! Revêts-toi de ta force ! Yeroushalaim, ville sainte ! Revêts-toi de tes vêtements magnifiques ! Car désormais ni l'incirconcis ni l'impur n'entreront plus chez toi.
52:2	Yeroushalaim, secoue ta poussière, lève-toi, et assieds-toi ! Détache les liens de ton cou, captive, fille de Sion !
52:3	Car ainsi parle YHWH : Vous avez été vendus gratuitement, et vous serez aussi rachetés sans argent.
52:4	Car ainsi parle Adonaï YHWH : Mon peuple descendit jadis en Égypte pour y séjourner, mais les Assyriens l'opprimèrent sans cause.
52:5	Et maintenant, qu'ai-je à faire ici, – déclaration de YHWH –, quand mon peuple a été enlevé gratuitement ? Ceux qui dominent sur lui le font hurler, – déclaration de YHWH. Tout le jour, mon Nom est continuellement méprisé<!--Blasphémé. Voir Ro. 2:24.-->.
52:6	C'est pourquoi mon peuple connaîtra mon Nom. C'est pourquoi il saura, en ce jour-là, que je suis parle : Voici je suis !
52:7	Combien sont beaux sur les montagnes les pieds de celui qui porte la bonne nouvelle, qui publie la paix<!--Na. 2:1 ; Ro. 10:15.-->, de celui qui porte une nouvelle de bonheur, qui publie le salut, qui dit à Sion : Ton Elohîm règne !
52:8	Tes sentinelles élèvent leurs voix, elles poussent ensemble des cris de joie car, les yeux dans les yeux, elles voient YHWH revenir à Sion.
52:9	Lieux laissés en ruine de Yeroushalaim, éclatez ensemble en cris de joie ! Car YHWH console son peuple, il rachète Yeroushalaim.
52:10	YHWH manifeste le bras de sa sainteté aux yeux de toutes les nations<!--Es. 53:1.-->, et toutes les extrémités de la Terre verront le salut<!--Toutes les extrémités de la Terre verront le salut de YHWH, c'est-à-dire Yéhoshoua (Jésus) (Mt. 28:18-20).--> de notre Elohîm.
52:11	Partez, partez, sortez de là ! Ne touchez à rien d'impur ! Sortez du milieu d'elle<!--Jé. 51:45 ; 2 Co. 6:17 ; Ap. 18:4.--> ! Purifiez-vous, vous qui portez les ustensiles de YHWH !
52:12	Car vous ne sortirez pas à la hâte, vous ne marcherez pas en fuyant, car YHWH ira en face de vous, et l'Elohîm d'Israël vous rassemblera.

### Le serviteur de YHWH

52:13	Voici, mon serviteur prospérera : il sera haut placé, élevé et exalté à l'extrême.
52:14	De même que beaucoup ont été stupéfaits à son sujet, tant son apparence d’homme a été défigurée, sa forme des fils d'humain,
52:15	de même il aspergera beaucoup de nations. Devant lui les rois fermeront la bouche, car ils verront ce qui ne leur avait pas été raconté et ils considéreront ce qu'ils n'avaient pas entendu.

## Chapitre 53

### Le sacrifice du Mashiah, serviteur de YHWH

53:1	Qui a cru à notre prédication ? Le bras de YHWH<!--Yéhoshoua ha Mashiah (Jésus-Christ) homme est le bras de YHWH. Ce bras est le symbole de la puissance divine qui s'est manifestée dans l'œuvre du Mashiah accomplissant le salut du monde. Le prophète Yesha`yah est transporté au moment où le peuple juif, après avoir rejeté son Mashiah, ouvrira enfin les yeux et acceptera celui qu'il a percé (Za. 12:10 ; Ap. 1:7). Voir aussi Jé. 27:4-5, 32:17.-->, pour qui s’est-il découvert ?
53:2	Il est monté en face de lui comme une jeune plante, comme une racine en terre aride. Il n’avait ni forme ni splendeur, quand nous le regardions, ni apparence qui nous le fasse désirer.
53:3	Le méprisé et le rejeté des hommes<!--Ps. 22:6-7 ; Mt. 27:27-31 ; Mc. 9:12 ; Jn. 16:32.-->, homme de douleur et sachant ce qu'est la maladie, tel celui devant qui l'on cache ses faces, il était méprisé, nous ne l'avons pas considéré.
53:4	En vérité, il a porté nos maladies et il s'est chargé de nos douleurs<!--Mt. 8:17 ; 1 Pi. 2:24.-->. Mais nous, nous avons estimé qu'il était frappé, battu par Elohîm et humilié.
53:5	Il a été profané<!--Lé. 18:21 ; 20:3 ; Ps. 74:7 ; Jé. 34:16 ; Ez. 36:23.--> à cause de nos transgressions, brisé à cause de nos iniquités : le châtiment de notre paix a été sur lui, et c'est par ses meurtrissures que nous sommes guéris.
53:6	Nous avons tous été errants comme des brebis<!--Petros (Pierre), apôtre de l'Agneau, confirme que le Mashiah est bel et bien le Bon Berger (1 Pi. 2:25).-->, nous nous sommes tournés chacun vers son propre chemin, et YHWH a fait venir sur lui l'iniquité de nous tous.
53:7	Opprimé et humilié, il n'a pas ouvert sa bouche<!--Mt. 26:62-63 ; Mc. 15:3-5 ; Jn. 19:9 ; Ac. 8:32-33.-->, semblable à un agneau qu'on mène au massacre, à une brebis muette face à ceux qui la tondent, il n'a pas ouvert sa bouche.
53:8	Enlevé par la contrainte et le jugement, et parmi sa génération, qui est-ce qui en a parlé ? Car il a été retranché de la terre des vivants, et la plaie lui a été faite à cause des transgressions de mon peuple.
53:9	On lui a donné son sépulcre avec les méchants, et dans sa mort<!--Littéralement « dans ses morts ».-->, il a été avec le riche, bien qu'il n'ait pas commis de violence et qu'il n'y ait pas eu de tromperie dans sa bouche<!--Mc. 15:28 ; Lu. 23:32-33.-->.
53:10	YHWH a pris plaisir à l’écraser. Il l'a rendu malade. S'il donne son âme en sacrifice de culpabilité, il verra une postérité et prolongera ses jours, et le désir de YHWH prospérera entre ses mains<!--Jé. 23:5.-->.
53:11	Du labeur de son âme il verra et se rassasiera. Mon serviteur, le juste, justifiera beaucoup de gens par la connaissance qu'ils auront de lui et lui-même portera leurs iniquités.
53:12	C'est pourquoi je lui donnerai une part avec les grands, il partagera le butin avec les puissants : parce qu'il a livré son âme à la mort et qu'il a été mis au rang des transgresseurs, parce qu'il a porté lui-même les péchés de beaucoup et qu'il a intercédé pour les transgresseurs.

## Chapitre 54

### YHWH réhabilite Israël la délaissée

54:1	Pousse des cris de joie, stérile, toi qui n'enfantes pas ! Fais éclater un cri retentissant et crie d'une manière aiguë, toi qui n'as pas connu les douleurs de l'accouchement ! Car les fils de la désolée seront plus nombreux que les fils de l'épousée, dit YHWH<!--Voir Ga. 4:27.-->.
54:2	Élargis l'espace de ta tente, et qu'on étende les couvertures de ton tabernacle : ne retiens rien ! Allonge tes cordages et affermis tes pieux !
54:3	Car tu te répandras à droite et à gauche, ta postérité possédera les nations et habitera les villes désertes.
54:4	N'aie pas peur, car tu ne seras pas couverte de honte. Ne sois pas confuse, car tu ne rougiras pas. Au contraire, tu oublieras la honte de ta jeunesse et tu ne te souviendras plus de l'insulte de ton veuvage.
54:5	Car ton créateur est ton époux : YHWH Tsevaot<!--YHWH des armées.--> est son Nom<!--YHWH, l'Époux de Yeroushalaim (Jérusalem) n'est autre que Yéhoshoua (Jésus) qui épousera la ville sainte à la fin des temps. Voir Ap. 21:1-10.-->. Ton Rédempteur est le Saint d'Israël, il s'appelle l'Elohîm de toute la Terre.
54:6	Car YHWH t'appelle comme une femme abandonnée et à l'esprit affligé, comme une épouse de la jeunesse qui a été répudiée, dit ton Elohîm.
54:7	Un petit moment je t'avais abandonnée, mais je te rassemblerai avec de grandes compassions.
54:8	Dans un débordement de colère, je t'avais un moment caché mes faces, mais j'aurai compassion de toi avec une bonté éternelle, dit YHWH, ton Rédempteur.
54:9	Car il en sera pour moi comme les eaux de Noah : j'avais juré que les eaux de Noah ne se répandraient plus sur la Terre<!--Ge. 8:21, 9:11.-->, je jure de même de ne plus m'irriter contre toi et de ne plus te menacer.
54:10	Quand les montagnes s'éloigneraient et que les collines chancelleraient, ma bonté ne s'éloignera pas de toi, et mon alliance de paix ne chancellera pas, dit YHWH, qui a compassion de toi.
54:11	Affligée, agitée par la tempête, toi que personne ne console ! Voici, je coucherai tes pierres dans l'antimoine et je te fonderai sur des saphirs.
54:12	Je ferai tes créneaux<!--Littéralement « tes soleils ».--> en agates, tes portes en pierres d'escarboucle et toutes tes frontières en pierres précieuses.
54:13	Tous tes fils seront disciples de YHWH, et grande sera la paix de tes fils.
54:14	Tu seras affermie par la justice. Tu seras loin de l'oppression et tu ne craindras rien ! Tu seras loin de la frayeur, car elle n'approchera pas de toi !
54:15	Voici, on complote, on complote, cela ne vient pas de moi ! Quiconque complotera contre toi, tombera à cause de toi<!--Ps. 91:7 ; Ge. 37.-->.
54:16	Voici, c'est moi qui ai créé l'artisan qui souffle sur le feu du charbon et qui en fait sortir une arme par son travail, j'ai créé aussi le destructeur pour détruire.
54:17	Toute arme façonnée contre toi ne réussira pas, et toute langue qui se lèvera en jugement contre toi, tu la condamneras<!--Ps. 23:4.-->. Tel est l'héritage des serviteurs de YHWH, et telle est la justice qui leur viendra de moi, – déclaration de YHWH.

## Chapitre 55

### Le salut par la grâce d'Elohîm

55:1	Ah ! vous tous qui avez soif, venez aux eaux, même vous qui n'avez pas d'argent ! Venez, achetez et mangez, venez, achetez sans argent et sans prix du vin et du lait !
55:2	Pourquoi pesez-vous de l'argent pour ce qui ne nourrit pas ? Pourquoi travaillez-vous pour ce qui ne rassasie pas<!--Ro. 14:17.--> ? Écoutez-moi, écoutez-moi et vous mangerez ce qui est bon, et votre âme se délectera de la graisse.
55:3	Inclinez l'oreille et venez à moi<!--Mt. 11:28.-->, écoutez et votre âme vivra ! Je traiterai avec vous une alliance éternelle, celle de la bonté fidèle envers David<!--Ac. 13:34.-->.
55:4	Voici, je l'ai donné comme témoin auprès des peuples, comme chef et dominateur des peuples.
55:5	Voici, tu appelleras des nations que tu ne connais pas, et les nations qui ne te connaissent pas accourront vers toi, à cause de YHWH, ton Elohîm, et du Saint d'Israël, qui t'aura glorifié.
55:6	Cherchez YHWH pendant qu'il se trouve, appelez-le pendant qu’il est proche.
55:7	Que le méchant abandonne sa voie, et l'homme injuste ses pensées ! Qu'il retourne à YHWH qui aura pitié de lui, à notre Elohîm qui pardonne abondamment<!--Jé. 18:11 ; Ez. 33:11 ; Jon. 3:10 ; 1 Ti. 2:1-4 ; 2 Pi. 3:9.-->.
55:8	Car mes pensées ne sont pas vos pensées, et mes voies ne sont pas vos voies, – déclaration de YHWH.
55:9	Car autant les cieux sont élevés au-dessus de la Terre, autant mes voies sont élevées au-dessus de vos voies, et mes pensées au-dessus de vos pensées.
55:10	Car comme la pluie et la neige descendent des cieux et n'y retournent pas sans avoir arrosé la Terre, sans l'avoir fécondée et avoir fait germer ses plantes, sans avoir donné de la semence au semeur et du pain à celui qui mange<!--2 Co. 9:10.-->,
55:11	ainsi en est-il de ma parole qui sort de ma bouche : elle ne retourne pas vers moi sans effet<!--Ou à vide. Voir De. 16:16.-->, sans avoir accompli ce que je désire et amené au succès la chose pour laquelle je l'ai envoyée.
55:12	Car vous sortirez avec joie et vous serez conduits dans la paix. Les montagnes et les collines éclateront en cris de joie devant vous, et tous les arbres des champs battront des paumes.
55:13	Au lieu de l'épine s'élèvera le cyprès, au lieu de la ronce croîtra le myrte, ce sera pour YHWH un nom, un signe éternel qui ne sera jamais retranché.

## Chapitre 56

### Exhortation à s'attacher à YHWH

56:1	Ainsi parle YHWH : Observez la justice, faites ce qui est juste, car mon salut ne tardera pas à venir, et ma justice à être révélée.
56:2	Heureux le mortel qui fait cela, le fils de l'être humain qui s'y tient, observant le shabbat pour ne pas le profaner, et gardant ses mains pour ne faire aucun mal.
56:3	Que le fils de l'étranger qui se joint à YHWH ne parle pas en disant : YHWH me séparera, il me séparera de son peuple ! Et que l'eunuque ne dise pas : Voici, je suis un arbre sec.
56:4	Car ainsi parle YHWH concernant les eunuques : Ceux qui garderont mes shabbats, et qui choisiront les choses auxquelles je prends plaisir, et qui s'attacheront à mon alliance,
56:5	je leur donnerai dans ma maison et dans mes murs la main et un nom meilleurs que des fils et des filles. Je leur donnerai un nom éternel qui ne sera jamais retranché<!--Ap. 2:17.-->.
56:6	Et les fils des étrangers qui se joindront à YHWH pour le servir, pour aimer le Nom de YHWH, pour être ses serviteurs, tous ceux qui garderont le shabbat pour ne pas le profaner et qui s'attacheront à mon alliance<!--Ex. 31:14.-->,
56:7	je les amènerai sur ma montagne sainte, et je les réjouirai dans ma maison de prière. Leurs holocaustes et leurs sacrifices seront agréés sur mon autel, car ma maison sera appelée : Maison de prière pour tous les peuples<!--Mt. 21:13 ; Mc. 11:17 ; Lu. 19:46.-->.
56:8	Déclaration d'Adonaï YHWH, de celui qui rassemble les exilés d'Israël : J'en rassemblerai encore d'autres autour de lui avec ceux qui sont déjà rassemblés.
56:9	Bêtes des champs, bêtes des forêts, venez toutes pour manger !
56:10	Toutes ses sentinelles sont aveugles, elles ne connaissent rien. Ce sont tous des chiens muets, incapables d'aboyer. Ils rêvent, restent couchés, aiment à sommeiller.
56:11	Ce sont des chiens ayant une âme féroce, ils ne connaissent pas la satiété. Ce sont des bergers qui ne comprennent rien. Tous se tournent vers leur propre voie, chacun vers son gain injuste, jusqu'au bout<!--Mt. 23:24 ; Tit. 1:7-11 ; 1 Pi. 5:2.--> :
56:12	Venez, je vais chercher du vin, et nous nous enivrerons de boissons fortes ! Demain sera comme aujourd'hui, il en reste encore beaucoup !

## Chapitre 57

### YHWH expose la fausseté et défend le juste

57:1	Le juste périt et personne ne le prend à cœur, les hommes de bonté sont enlevés sans que personne ne comprenne que face au mal<!--Mi. 7:2 ; Ec. 7:15.--> le juste a été enlevé.
57:2	Il entrera en paix, il reposera sur sa couche, celui qui aura marché dans la droiture<!--Mt. 25:23 ; Lu. 19:17.-->.
57:3	Mais vous, approchez ici, fils de l'enchanteresse, postérité de l'adultère et de la prostituée !
57:4	De qui vous êtes-vous moqués ? Contre qui avez-vous ouvert la bouche et tirez-vous la langue ? N'êtes-vous pas des enfants de la transgression, une postérité de mensonge,
57:5	s'échauffant près des faux elohîm, sous tout arbre vert, tuant les enfants dans les vallées, sous les fentes des rochers<!--Lé. 18:21 ; 1 R. 14:23 ; Jé. 2:20, 32:35.--> ?
57:6	C'est parmi les pierres polies des torrents qu'est ta portion. Ce sont elles, ce sont elles qui sont ton lot ! C'est à elles que tu verses des libations, que tu offres des offrandes. Me consolerais-je de ces choses ?
57:7	Tu dresses ta couche sur les montagnes hautes et élevées. C'est aussi là que tu montes pour sacrifier des sacrifices.
57:8	Tu mets ton souvenir derrière la porte et les poteaux. Car loin de moi tu te découvres et montes, tu élargis ta couche et traites alliance avec eux. Tu aimes leur couche et contemples la main.
57:9	Tu voyages vers le roi avec de l'huile et tu multiplies les parfumeries, tu envoies au loin tes messagers, tu t'abaisses jusqu'au shéol.
57:10	Tu te fatigues par la longueur du chemin, et tu ne dis pas : C'est sans espoir ! Tu trouves encore de la vigueur dans ta main, c'est pourquoi tu n'as pas été languissante.
57:11	Qui redoutais-tu, de qui avais-tu peur, pour que tu aies menti, et que tu ne te sois pas souvenue de moi et ne l'aies pas pris à cœur ? N'ai-je pas gardé le silence, et même depuis longtemps ? Et de moi, tu n’as pas peur.
57:12	Je vais révéler ta justice et tes œuvres, car elles ne te profiteront pas.
57:13	Quand tu cries, que ceux que tu as rassemblés te délivrent ! Mais le vent les emmènera tous, la vanité les enlèvera. Mais celui qui met sa confiance en moi héritera la terre et prendra possession de ma montagne sainte<!--Es. 2:3 ; Ps. 2:6 ; Hé. 12:22.-->.
57:14	On dira : Frayez, frayez, préparez le chemin, enlevez tout obstacle loin du chemin de mon peuple !

### YHWH aime l'homme contrit

57:15	Car ainsi parle celui qui est haut et élevé, qui habite l'éternité et dont le Nom est le Saint<!--Voir commentaire en Ac. 3:14.--> : J'habiterai dans les lieux hauts et saints, avec celui qui est contrit et humble d'esprit, pour donner la vie à l'esprit des humbles, pour donner la vie aux cœurs brisés<!--Ps. 34:19, 51:19.-->.
57:16	Car je ne contesterai pas sans fin, et je ne serai pas éternellement fâché, car devant moi défaillirait l'esprit, le souffle que j'ai fait<!--Mi. 7:18 ; Ps. 85:6, 103:9.-->.
57:17	À cause de l'iniquité de ses gains injustes, je me suis irrité et je l'ai frappé. Je me suis caché dans ma colère, et l'apostat a marché dans la voie de son cœur.
57:18	J'ai vu ses voies, mais je le guérirai. Je le conduirai et je le comblerai de consolations, lui et ceux qui mènent deuil avec lui.
57:19	Je crée le fruit des lèvres : Shalôm, shalôm à celui qui est loin et à celui qui est près ! dit YHWH, car je le guérirai.
57:20	Mais les méchants sont comme la mer agitée, quand elle ne peut se calmer, et dont les eaux rejettent la boue et le bourbier.
57:21	Il n'y a pas de shalôm pour les méchants, dit mon Elohîm.

## Chapitre 58

### Le vrai et le faux jeûne

58:1	Crie à plein gosier, ne te retiens pas ! Élève ta voix comme un shofar, et annonce à mon peuple ses transgressions et à la maison de Yaacov ses péchés !
58:2	Car ils me cherchent tous les jours, ils prennent plaisir à connaître mes voies. Comme une nation qui aurait pratiqué la justice, et qui n'aurait pas abandonné les ordonnances de son Elohîm, ils me demandent des jugements justes, ils prennent plaisir à s'approcher d'Elohîm.
58:3	Pourquoi jeûnons-nous ? Tu ne le vois pas ! Pourquoi affligeons-nous nos âmes ? Tu ne le sais pas ! Voici, le jour de votre jeûne, vous trouvez votre plaisir, et vous oppressez tous vos travailleurs.
58:4	Voici, vous jeûnez pour vous livrer aux querelles et aux disputes, et pour frapper du poing avec méchanceté. Vous ne jeûnez pas comme le veut ce jour, pour que votre voix soit entendue en haut.
58:5	Est-ce là le jeûne que j'ai choisi, que l'être humain afflige son âme un jour ? Courber sa tête comme le jonc et en étendant le sac et la cendre ? Appelleras-tu cela un jeûne et un jour agréable à YHWH ?
58:6	N'est-ce pas plutôt ceci le jeûne que j'ai choisi : que tu détaches les liens de la méchanceté, que tu délies les cordages du joug, que tu laisses aller libres les opprimés, et que l'on rompe toute espèce de joug ?
58:7	N'est-ce pas que tu partages ton pain avec celui qui a faim ? Et que tu fasses venir dans ta maison les affligés errants ? Quand tu vois un homme nu, que tu le couvres, et que tu ne te caches pas de ta propre chair ?

### Bénédiction pour ceux qui pratiquent le bien

58:8	Alors ta lumière éclatera comme l'aurore et ta guérison germera rapidement, ta justice marchera devant toi, la gloire de YHWH sera ton arrière-garde.
58:9	Alors tu appelleras et YHWH répondra, tu crieras au secours et il dira : Me voici ! Si tu ôtes du milieu de toi le joug, le doigt tendu et les paroles méchantes,
58:10	si tu ouvres ton âme à celui qui a faim, si tu rassasies l'âme affligée, ta lumière se lèvera sur les ténèbres et l'obscurité sera comme le midi.
58:11	Et YHWH te conduira continuellement, il rassasiera ton âme dans les grandes sécheresses, il fortifiera tes os, et tu seras comme un jardin arrosé, et comme une source d'eaux dont les eaux ne tarissent pas<!--Jn. 4:14 ; Ap. 21:6.-->.
58:12	Par toi seront rebâtis les lieux laissés en ruine depuis longtemps, tu rétabliras les fondements qui sont d’âge en âge. On t'appellera le réparateur des brèches, le restaurateur des chemins pour qu'on y habite.
58:13	Si tu détournes ton pied pendant le shabbat pour ne pas faire ton plaisir en mon saint jour, si tu appelles le shabbat tes délices, et honorable ce qui est saint à YHWH, et si tu l'honores au lieu d'agir selon tes voies, de trouver ce qui te fait plaisir et de proférer une parole,
58:14	alors tu prendras plaisir en YHWH. Je te ferai monter sur les hauteurs de la terre et te nourrirai de l'héritage de Yaacov, ton père. Oui, la bouche de YHWH a parlé.

## Chapitre 59

### Le péché sépare de YHWH

59:1	Voici, la main de YHWH n'est pas trop courte pour sauver, ni son oreille trop pesante pour entendre.
59:2	Mais ce sont vos iniquités qui sont une séparation entre vous et votre Elohîm, ce sont vos péchés qui lui font cacher ses faces de vous pour ne plus entendre<!--De. 31:17-18 ; Ez. 39:23-24.-->.
59:3	Car vos paumes sont souillées par le sang et vos doigts par l'iniquité. Vos lèvres profèrent le mensonge, votre langue déclare l'injustice.
59:4	Personne ne crie pour la justice, personne ne plaide pour la vérité. Ils mettent leur confiance dans le tohu et disent des faussetés, ils conçoivent le mal et enfantent l'iniquité.
59:5	Ils font éclore des œufs de vipère, et ils tissent des toiles d'araignée. Celui qui mange de leurs œufs meurt et, si l'on en écrase un, il en sort une vipère.
59:6	Leurs toiles ne servent pas à faire des vêtements, et on ne peut se couvrir de leurs œuvres ; leurs œuvres sont des œuvres de méchanceté, et des actes de violence sont dans leurs paumes.
59:7	Leurs pieds courent au mal et se hâtent pour répandre le sang innocent. Leurs pensées sont des pensées d'iniquité, le ravage et la ruine sont sur leurs voies.
59:8	Ils ne connaissent pas le chemin de la paix, et il n'y a pas de justice dans leurs voies, ils se sont pervertis dans leurs sentiers, tous ceux qui y marchent ignorent la paix<!--Pr. 1:16, 6:16-19.-->.
59:9	C'est pourquoi le jugement s'est éloigné de nous, et la justice ne parvient pas jusqu'à nous. Nous attendions la lumière et voici la ténèbre, la clarté et nous marchons dans l'obscurité.
59:10	Nous tâtonnons comme des aveugles le long du mur, nous tâtonnons comme ceux qui sont sans yeux. Nous chancelons en plein midi comme la nuit. Tout en étant pleins de vigueur, nous sommes semblables à des morts.
59:11	Nous rugissons tous comme des ours, nous gémissons, nous gémissons comme des colombes. Nous attendions le jugement, et il n'y en a pas,  le salut, et il s’est éloigné de nous.
59:12	Car nos transgressions se sont multipliées devant toi et nos péchés témoignent contre nous. Car nos transgressions sont avec nous et nous connaissons nos iniquités :
59:13	se rebeller et renier YHWH, se détourner de notre Elohîm, parler d'oppression et d'apostasie, concevoir et méditer dans le cœur des paroles mensongères.
59:14	C'est pourquoi le jugement s'est éloigné et la justice se tient éloignée. Car la vérité trébuche sur la place publique, et la droiture ne peut y entrer.
59:15	La vérité a disparu<!--Ps. 119:43 ; Jé. 7:28.--> et quiconque se détourne du mal se fait dépouiller. YHWH l'a vu, et cela a déplu à ses yeux parce qu'il n'y a plus de droiture.

### YHWH cherche un homme, il suscite le Mashiah

59:16	Il voit qu’il n’y a pas un homme, il est désolé de ce que personne n’intercède, c'est pourquoi son bras lui vient en aide, et sa propre justice lui sert d'appui<!--Es. 53:1, 63:5 ; Ps. 77:15-16 ; Ac. 13:17.-->.
59:17	Il se revêt de la justice comme d'une cuirasse, et le casque du salut est sur sa tête<!--Ep. 6:14-17.-->. Il se revêt de la vengeance comme d'un vêtement, et se couvre de la jalousie comme d'une robe.
59:18	Selon leurs actes, il rendra à chacun la pareille<!--Jé. 17:10 ; Job 34:11 ; Mt. 16:27 ; Ap. 2:23, 20:13.-->, la fureur à ses adversaires, la rétribution à ses ennemis ! Il rendra ainsi la rétribution aux îles.
59:19	On craindra le Nom de YHWH depuis l'occident et sa gloire depuis le soleil levant. Quand l'ennemi viendra comme un fleuve, l'Esprit de YHWH le mettra en fuite.
59:20	Le Rédempteur<!--Le Rédempteur qui viendra pour Sion est le Seigneur Yéhoshoua ha Mashiah (Ro. 11:26). Voir aussi Es. 60:16.--> viendra pour Sion, pour ceux de Yaacov qui reviendront de leur transgression, – déclaration de YHWH.
59:21	Quant à moi, voici mon alliance avec eux, dit YHWH : Mon Esprit qui est sur toi, et mes paroles que j'ai mises dans ta bouche, ne se retireront pas de ta bouche, ni de la bouche de ta postérité, ni de la bouche de la postérité de ta postérité, dit YHWH, dès maintenant et à jamais.

## Chapitre 60

### La gloire de YHWH se lèvera sur Sion

60:1	Lève-toi, sois la lumière, car ta lumière arrive, et la gloire de YHWH se lève sur toi.
60:2	Car voici, les ténèbres couvrent la Terre et la profonde obscurité couvre les peuples, mais sur toi YHWH se lève, sur toi sa gloire apparaît.
60:3	Des nations marchent à ta lumière, et des rois à l’éclat de ton aurore<!--Ap. 21:24.-->.
60:4	Lève tes yeux alentour et regarde : ils se rassemblent tous, ils viennent vers toi. Tes fils viennent de loin, et tes filles sont portées sur le côté.
60:5	Alors tu verras et tu seras rayonnante, ton cœur tressaillira et se dilatera, quand l'abondance de la mer se tournera vers toi et que les richesses des nations viendront chez toi.
60:6	Tu seras couverte d'une foule de chameaux, des dromadaires de Madian et d'Eyphah. Ils viendront tous de Séba, ils porteront de l'or et de l'encens, ils annonceront les louanges de YHWH.
60:7	Les brebis de Qedar se réuniront toutes chez toi, les béliers de Nebayoth seront à ton service, ils monteront sur mon autel, ils y seront en faveur et je glorifierai la maison de ma gloire.
60:8	Qui sont ceux-là qui volent comme des nuées, comme des colombes vers leur colombier ?
60:9	Car les îles s'attendent à moi, et les navires de Tarsis les premiers, afin d'amener de loin tes enfants, avec leur argent et leur or, à cause du Nom de YHWH, ton Elohîm, et du Saint d'Israël qui te glorifie.
60:10	Les fils des étrangers rebâtiront tes murailles, et leurs rois te serviront, car je t'ai frappée dans ma colère, mais dans ma faveur j’ai eu compassion de toi.
60:11	Tes portes seront continuellement ouvertes, elles ne seront fermées ni nuit ni jour, pour qu'on emmène chez toi les richesses des nations et qu'on introduise leurs rois<!--Ap. 21:25-26.-->.
60:12	Car la nation et le royaume qui ne te serviront pas périront, et ces nations-là seront en ruine, elles seront en ruine.
60:13	La gloire du Liban viendra vers toi, le cyprès, l'orme, et le buis, tous ensemble pour embellir le lieu de mon sanctuaire, et je rendrai glorieux le lieu de mes pieds.
60:14	Mais les enfants de tes oppresseurs viendront vers toi en se courbant, et tous ceux qui te méprisaient se prosterneront à la plante de tes pieds et t'appelleront la ville de YHWH, la Sion du Saint d'Israël.
60:15	Au lieu que tu sois délaissée et haïe, sans personne qui passe, je ferai de toi une majesté éternelle et une joie d'âges en âges.
60:16	Tu suceras le lait des nations, tu suceras la mamelle des rois et tu sauras que je suis YHWH, ton Sauveur, ton Rédempteur<!--Le verbe « ga'al » et le nom correspondant « go'el » ont été traduits respectivement en français par « racheter » et « rédempteur ». Selon la torah de Moshé (Moïse), si quelqu'un perdait son héritage à cause d'une dette ou s'il se vendait comme esclave, lui et ses biens pouvaient être rachetés par un proche parent qui devait payer le prix de la rédemption (Lé. 25:23-55). YHWH se présente comme le Rédempteur par excellence (Es. 49:26 ; Ps. 78:35, 130:7 ; Job 19:25).-->, le Puissant de Yaacov.
60:17	Au lieu du cuivre je ferai venir de l'or, au lieu du fer je ferai venir de l'argent, au lieu du bois, du cuivre, et au lieu des pierres, du fer. Je ferai régner la paix et dominer la justice.
60:18	On n'entendra plus parler de violence en ta terre ni de ravage et de ruine dans ton territoire. Mais tu appelleras tes murailles : Salut<!--yeshuw`ah.-->, et tes portes : Louange.
60:19	Tu n'auras plus le soleil pour la lumière du jour, et la lueur de la lune ne t'éclairera plus, mais YHWH sera pour toi la lumière<!--Voir Ex. 13:21, 14:20, 25:37.--> éternelle<!--Voir le commentaire en Ge. 1:3.-->, et ton Elohîm sera ta gloire.
60:20	Ton soleil ne se couchera plus, et ta lune ne se retirera plus, car YHWH sera pour toi la lumière éternelle, et les jours de ton deuil seront finis.
60:21	Quant à ton peuple, ils seront tous justes, ils prendront possession de la terre pour toujours, le germe de mes plantes, l'œuvre de mes mains pour y être glorifié<!--Es. 11:1 ; Ro. 15:12 ; Ap. 5:5, 22:16.-->.
60:22	Le petit deviendra un millier, et le moindre deviendra une nation puissante. Je suis YHWH, je hâterai ces choses en leur temps.

## Chapitre 61

### La mission du Mashiah

61:1	L'Esprit d'Adonaï YHWH est sur moi, car YHWH m'a oint pour porter la bonne nouvelle aux malheureux. Il m'a envoyé pour guérir ceux qui ont le cœur brisé, pour proclamer aux captifs la liberté, et aux prisonniers l'ouverture de la prison,
61:2	pour proclamer l’année de grâce de YHWH, et le jour de vengeance de notre Elohîm, pour consoler tous ceux qui mènent deuil<!--Lu. 4:14-19.-->,
61:3	pour mettre et donner à ceux de Sion qui mènent deuil une parure de la tête au lieu de la cendre, une huile de joie au lieu du deuil, un manteau de louange au lieu d'un esprit abattu<!--Job 29:14 ; Ja. 1:12 ; 1 Co. 9:25 ; 2 Ti. 4:8.-->, afin qu'on les appelle les térébinthes de la justice, la plantation de YHWH pour se glorifier.
61:4	Ils rebâtiront les lieux laissés en ruine depuis longtemps, ils relèveront les désolations du passé, ils renouvelleront les villes ravagées, les lieux dévastés d'âges en âges.
61:5	Et des étrangers s'y tiendront là et feront paître vos troupeaux, et les enfants de l'étranger seront vos laboureurs et vos vignerons.
61:6	Mais vous, vous serez appelés prêtres de YHWH, on vous dira serviteurs de notre Elohîm<!--Ap. 1:6, 5:10.-->. Vous mangerez les richesses des nations et vous vous glorifierez de leur gloire.
61:7	À la place de votre double honte et de la confusion, ils pousseront des cris de joie au sujet de leur portion. C'est ainsi qu'ils hériteront le double en leur terre et leur joie sera éternelle.
61:8	Car je suis YHWH qui aime la justice, qui hait le vol pour l’holocauste. J'établirai leur œuvre dans la vérité et je traiterai avec eux une alliance éternelle.
61:9	Leur postérité sera connue parmi les nations, ceux qui seront sortis d'eux, parmi les peuples. Tous ceux qui les verront reconnaîtront qu'ils sont la postérité que YHWH aura bénie.
61:10	Je me réjouirai, je me réjouirai en YHWH, et mon âme sera joyeuse en mon Elohîm, car il m'a revêtu des vêtements du salut, il m'a couvert de la robe de la justice, comme un époux se pare de sa parure de la tête, comme une épouse s’orne de ses joyaux<!--Os. 2:21-22 ; Ap. 19:7-8.-->.
61:11	Car comme la terre fait éclore son germe, et comme un jardin fait germer ses semences, ainsi Adonaï YHWH fera germer la justice, et la louange en présence de toutes les nations.

## Chapitre 62

### YHWH proclame la restauration d'Israël

62:1	Pour l'amour de Sion, je ne garderai pas le silence, pour l'amour de Yeroushalaim, je ne prendrai pas de repos, jusqu'à ce que sa justice sorte comme un éclat, et son salut comme une torche allumée.
62:2	Les nations verront ta justice, et tous les rois ta gloire. On t'appellera d'un nouveau nom<!--Ap. 2:17.--> que la bouche de YHWH désignera.
62:3	Tu deviendras une couronne de gloire dans la main de YHWH, un turban, un turban royal dans la paume de ton Elohîm.
62:4	On ne t'appellera plus : « Délaissée ! » On n'appellera plus ta terre : « Dévastation ! » Mais on t'appellera « Mon plaisir est en elle », et on appellera ta terre « Mariée », car YHWH prend plaisir en toi et ta terre aura un mari.
62:5	Car de même qu’un jeune homme se marie à une vierge, tes fils se marieront à toi et, de la joie que le fiancé a de sa fiancée, ton Elohîm se réjouira de toi.
62:6	Yeroushalaim, j'ai placé des gardes sur tes murailles, tout le jour et toute la nuit, continuellement, ils ne resteront pas silencieux. Vous qui faites mention de YHWH, pas de repos pour vous ! 
62:7	Ne lui donnez aucun repos, jusqu’à ce qu’il ait établi, jusqu’à ce qu’il ait mis Yeroushalaim en louange de la Terre.
62:8	YHWH l'a juré par sa droite et par son bras puissant : Je ne donnerai plus ton blé pour nourriture à tes ennemis, et les enfants des étrangers ne boiront plus ton vin nouveau pour lequel tu as travaillé.
62:9	Mais ceux qui l’auront récolté le mangeront et loueront YHWH, et ceux qui l’auront recueilli le boiront dans les parvis de mon lieu saint.
62:10	Passez, passez les portes ! Préparez le chemin du peuple ! Frayez, frayez la route, et ôtez-en les pierres ! Élevez une bannière vers les peuples.
62:11	Voici que YHWH se fait entendre jusqu'à l'extrémité de la Terre : Dites à la fille de Sion : Voici, ton salut vient<!--Yahweh est notre Salut (Ps. 27:1). De nombreux passages, notamment dans le livre de Yesha`yah (Ésaïe), présentent Elohîm comme le seul Sauveur (Es. 43:3,11 ; Os. 13:4) qui viendra pour délivrer son peuple (Es. 35:4, 60:1 ; Za. 14:1-7). Yéhoshoua ha Mashiah (Jésus-Christ) a accompli en tous points les prophéties relatives à la venue de YHWH. Elohîm est bel et bien venu sur Terre il y a plus de 2 000 ans et ce même Elohîm revient bientôt (Ac. 1:11 ; Ap. 1:7).-->, voici, son salaire est avec lui et sa récompense<!--Ge. 15:1 ; Es. 40:10, 49:4 ; Ap. 22:12.--> marche devant lui.
62:12	On les appellera « peuple saint », « rachetés de YHWH »<!--1 Pi. 2:9 ; Ap. 5:9.-->. Et toi, on t'appellera « cherchée », « ville non abandonnée ».

## Chapitre 63

### Le jour de vengeance du Mashiah<!--Es. 2:10-22 ; Ap. 19:11-21.-->

63:1	Qui est celui-ci qui vient d'Édom, de Botsrah, en vêtements rouges, magnifiquement paré dans ses habits, marchant selon la grandeur de sa force ? C'est moi qui parle avec justice, et qui suis Grand pour sauver.
63:2	Pourquoi tes vêtements sont-ils rouges, tes habits comme ceux d’un fouleur au pressoir ?
63:3	J'ai été seul à fouler au pressoir à vendange, et parmi les peuples, aucun homme n'était avec moi. Cependant, j'ai marché sur eux dans ma colère, et je les ai foulés dans ma fureur. Leur sang a jailli sur mes vêtements et j'ai souillé tous mes habits.
63:4	Car le jour de la vengeance était dans mon cœur et l'année de mes rachetés est venue.
63:5	Je regardais, mais il n'y avait personne pour m'aider. J'étais étonné, mais il n'y avait personne pour me soutenir. Alors mon bras m'a sauvé et ma fureur m'a soutenu.
63:6	J'ai foulé des peuples dans ma colère, je les ai enivrés dans ma fureur et j'ai abattu leur force par terre.

### Yesha`yah confesse les péchés du peuple

63:7	Je rappellerai les bontés de YHWH, les louanges de YHWH, pour tous les bienfaits<!--« Traiter », « récompenser », « sevrer un enfant », « être sevré », « mûrir », « amener à maturité ».--> que YHWH nous a faits et pour l'abondance des biens qu'il a faits à la maison d'Israël, dans ses compassions et dans la grandeur de ses bontés.
63:8	Il a dit : Certainement, ils sont mon peuple, des enfants qui ne tricheront pas ! Et il a été pour eux un Sauveur.
63:9	Et dans toutes leurs détresses, il a été en détresse, et l'Ange de ses faces les a sauvés<!--Ge. 16:7-10 ; Jg. 6:11-14 ; Za. 1:11.-->. C'est lui-même qui les a rachetés dans son amour et sa miséricorde, et il les a soutenus et portés, tous les jours d'autrefois.
63:10	Mais ils ont été rebelles, ils ont affligé son Esprit Saint<!--Ou l’Esprit de sa sainteté. Ep. 4:30.-->, c'est pourquoi il est devenu leur ennemi, il a lui-même combattu contre eux.
63:11	Son peuple se souvint des anciens jours de Moshé : Où est celui qui les fit monter de la mer avec le berger de son troupeau ? Où est celui qui mit au milieu d'eux son Esprit Saint ;
63:12	qui les dirigea par la droite de Moshé et par son bras glorieux, qui fendit les eaux devant eux pour se faire un nom éternel,
63:13	qui les dirigea à travers les abîmes, comme un cheval dans le désert, sans qu'ils ne bronchent ?
63:14	L'Esprit de YHWH les a menés au repos comme on mène une bête qui descend dans la vallée. C'est ainsi que tu as conduit ton peuple, afin de t'acquérir un nom glorieux.
63:15	Regarde des cieux et vois de ta demeure sainte et glorieuse : où sont ton zèle et ta puissance ? Le son de tes entrailles et tes compassions pour moi ont-ils été contenus ?
63:16	Car tu es notre Père ! Car Abraham ne nous connaît pas, et Israël ignore qui nous sommes. YHWH, c'est toi qui es notre Père, et ton Nom est notre Rédempteur de tout temps.
63:17	Pourquoi nous as-tu fait égarer loin de tes voies, YHWH, et endurcis-tu notre cœur pour ne pas te craindre ? Reviens, pour l'amour de tes serviteurs, des tribus de ton héritage !
63:18	Ton peuple saint n'a pris possession de la terre que peu de temps, nos ennemis ont foulé ton sanctuaire.
63:19	Nous sommes devenus, depuis longtemps, des gens sur qui tu ne règnes plus et sur qui ton Nom n'est pas proclamé. Si seulement tu fendais les cieux et descendais, en face de toi les montagnes trembleraient !

## Chapitre 64

### Prière du reste d'Israël à YHWH pour sa délivrance

64:1	Comme le feu allume les broussailles, comme le feu fait bouillir l'eau, tu ferais connaître ton Nom à tes ennemis de sorte que les nations tremblent en ta présence.
64:2	Lorsque tu fis les choses redoutables que nous n'attendions pas, tu descendis et les montagnes tremblèrent devant toi.
64:3	Jamais on n'a appris ni entendu dire, et jamais l'œil n'a vu un autre elohîm que toi agir de cette manière pour ceux qui s'attendent à lui<!--1 Co. 2:9.-->.
64:4	Tu as rencontré celui qui exulte et qui pratique la justice, ceux qui marchent dans tes voies et qui se souviennent de toi. Voici tu t'es fâché parce que nous avons péché. Mais par cela, pour toujours, serions-nous sauvés ? 
64:5	Or nous sommes tous devenus comme une chose souillée, et toute notre justice est comme le linge le plus souillé<!--Ap. 19:8.-->. Nous sommes tous flétris comme la feuille et nos iniquités nous emportent comme le vent.
64:6	Il n'y a personne qui invoque ton Nom, qui se réveille pour s'attacher fortement à toi. C'est pourquoi tu nous as caché tes faces, et tu nous fais fondre dans la main de nos iniquités.
64:7	Maintenant, YHWH, tu es notre Père. Nous sommes l'argile, et c'est toi qui nous as formés, et nous sommes tous l'ouvrage de ta main<!--Es. 29:16, 45:9 ; Jé. 18:6 ; Job 10:9 ; Ro. 9:20-21.-->.
64:8	Ne t'irrite pas à l'extrême, YHWH, et ne te souviens pas pour toujours de notre iniquité ! Voici, regarde, s'il te plaît, nous sommes tous ton peuple.
64:9	Tes villes saintes sont devenues un désert. Sion est devenue un désert, et Yeroushalaim une désolation.
64:10	Notre maison de sainteté et de gloire, où nos pères te louaient, est devenue une chose à brûler au feu, tout ce que nous avions de précieux est devenue une désolation.
64:11	À cause de cela, YHWH, te contiendras-tu ? Resteras-tu silencieux et nous affligeras-tu extrêmement ?

## Chapitre 65

### Réponse de YHWH

65:1	Je me suis laissé consulter par ceux qui ne me demandaient rien, et je me suis laissé trouver par ceux qui ne me cherchaient pas<!--Mt. 7:7 ; Lu. 11:9.-->. J'ai dit à la nation qui ne s'appelait pas de mon Nom : Me voici, me voici !
65:2	J'ai tendu mes mains tous les jours vers un peuple rebelle, qui marche dans une mauvaise voie, après ses propres pensées,
65:3	vers un peuple qui m'irrite continuellement en face, qui sacrifie dans les jardins, et qui brûle de l'encens sur des briques,
65:4	qui habite dans les sépulcres et passe la nuit dans les cabanes de gardes, qui mange la chair de porc, et ayant dans ses vases le jus des choses infâmes.
65:5	Qui dit : Retire-toi, ne m'approche pas, car je suis plus saint que toi ! Ceux-là sont une fumée dans mes narines, un feu ardent tout le jour.
65:6	Voici, cela est écrit devant moi, je ne me tairai pas, mais je leur ferai payer, je leur ferai payer dans leur sein,
65:7	leurs iniquités et les iniquités de leurs pères ensemble, dit YHWH, eux qui ont brûlé de l'encens sur les montagnes et qui m'ont blasphémé sur les collines. C'est pourquoi je mesurerai dans leur sein le salaire de leurs œuvres passées.
65:8	Ainsi parle YHWH : Quand on trouve du vin nouveau dans une grappe, on dit : Ne la détruis pas, car il y a là une bénédiction ! J'agirai de la même manière à cause de mes serviteurs, afin de ne pas tous les détruire.
65:9	Je ferai sortir de Yaacov une postérité, et de Yéhouda celui qui prendra possession de mes montagnes : mes élus en prendront possession, mes serviteurs y habiteront.
65:10	Le Sharôn deviendra un pâturage pour le petit bétail, et la vallée d'Acor, un lieu de repos pour le gros bétail, pour mon peuple qui m'aura recherché.
65:11	Mais vous, qui abandonnez YHWH et qui oubliez ma montagne sainte, qui dressez la table pour Gad<!--Gad : dieu de la fortune.-->, et qui remplissez une coupe pour Meni<!--Meni : divinité païenne assimilée à la lune et dont le nom signifie « destin, sort » ou « fortune ».-->,
65:12	je vous destine aussi à l'épée, et vous serez tous courbés pour le massacre. Car j'ai appelé et vous n'avez pas répondu, j'ai parlé et vous n'avez pas écouté. Mais vous avez fait ce qui est mal à mes yeux, et vous avez choisi les choses auxquelles je ne prends pas plaisir.
65:13	C'est pourquoi, ainsi parle Adonaï YHWH : Voici, mes serviteurs mangeront, et vous aurez faim. Voici, mes serviteurs boiront, et vous aurez soif. Voici, mes serviteurs se réjouiront, et vous serez honteux.
65:14	Voici, mes serviteurs pousseront des cris de joie à cause du bonheur de leur cœur, mais vous, vous crierez à cause de la douleur de votre cœur et vous hurlerez à cause du brisement de votre esprit.
65:15	Et vous laisserez votre nom à mes élus comme une malédiction. Adonaï YHWH vous fera mourir, mais il donnera à ses serviteurs un autre nom.
65:16	Celui qui se bénira sur la Terre, se bénira par l'Elohîm de l'Amen, et celui qui jurera sur la Terre jurera par l'Elohîm de l'Amen. Car les détresses du passé seront oubliées, et même elles seront cachées à mes yeux.

### De nouveaux cieux et une nouvelle terre

65:17	Car voici, je crée de nouveaux cieux et une nouvelle Terre<!--Es. 66:22 ; 2 Pi. 3:13 ; Ap. 21:1.-->. On ne se souviendra plus des premiers, ils ne monteront plus au cœur.
65:18	Oui, réjouissez-vous et soyez pour toujours dans l'allégresse, à cause de ce que je crée. Oui, voici je crée Yeroushalaim pour la joie et son peuple pour l'allégresse.
65:19	Je me réjouirai au sujet de Yeroushalaim, j’exulterai au sujet de mon peuple ! On n'y entendra plus le bruit des pleurs et le bruit des clameurs.
65:20	Il n’y aura plus là de nourrissons de quelques jours, ni de vieillard qui n’accomplisse ses jours. Car un jeune homme mourra fils de 100 ans, et le pécheur fils de 100 ans sera maudit.
65:21	Ils bâtiront des maisons et les habiteront. Ils planteront des vignes et ils en mangeront le fruit.
65:22	Ils ne bâtiront pas des maisons pour qu'un autre les habite, ils ne planteront pas pour qu’un autre mange. Oui, les jours de mon peuple seront comme les jours des arbres, mes élus jouiront de l'œuvre de leurs mains.
65:23	Ils ne travailleront plus en vain, et ils n'engendreront pas pour une fin soudaine. Car ils seront la postérité des bénis de YHWH, et leur descendance avec eux.
65:24	Et il arrivera qu'avant qu'ils n'appellent, je leur répondrai ; ils parleront encore que je les aurai entendus.
65:25	Le loup et l'agneau paîtront ensemble, le lion comme le bœuf mangeront de la paille, et la poussière sera la nourriture du serpent<!--Es. 2:4, 11:6-7.-->. On ne fera plus de mal et on ne détruira plus sur toute ma montagne sainte, dit YHWH.

## Chapitre 66

### YHWH réprouve l'hypocrisie et agrée ceux qui le craignent

66:1	Ainsi parle YHWH : Les cieux sont mon trône, et la Terre est le marchepied de mes pieds<!--Mt. 5:34-35 ; Ac. 7:49.-->. Quelle maison me bâtiriez-vous, et quel serait le lieu de mon repos ?
66:2	Toutes ces choses, ma main les a faites, et toutes ces choses existent, – déclaration de YHWH. Mais à qui regarderai-je ? À celui qui est affligé, qui a l'esprit abattu, et qui tremble à ma parole.
66:3	Celui qui tue un bœuf tue un homme, celui qui sacrifie une brebis brise la nuque à un chien, celui qui présente une offrande de grain offre du sang de porc, celui qui fait un mémorial d'encens bénit les idoles : tous ceux-là ont choisi leurs voies, et leur âme trouve du plaisir dans leurs abominations.
66:4	Moi aussi je choisirai leurs dévergondages, et je ferai venir sur eux les choses qu'ils craignent, parce que j'ai appelé, et personne n'a répondu, parce que j'ai parlé, et qu'ils n'ont pas écouté. Mais ils ont fait ce qui est mal à mes yeux et ils ont choisi les choses auxquelles je ne prends pas de plaisir.
66:5	Écoutez la parole de YHWH, vous qui tremblez à sa parole. Vos frères, qui vous haïssent et qui vous repoussent comme une chose abominable, à cause de mon Nom disent : Que YHWH montre sa gloire et que nous voyions votre joie ! Cependant, c'est eux qui seront couverts de honte.
66:6	Une voix, un vacarme depuis la ville, une voix, depuis le temple, c'est la voix de YHWH qui rend à ses ennemis selon leurs œuvres.

### Israël renaît en un jour

66:7	Elle a enfanté, avant d'éprouver les douleurs de l'accouchement, elle a donné naissance à un enfant mâle, avant que les douleurs de l'enfantement ne lui viennent.
66:8	Qui a déjà entendu une telle chose ? Qui a déjà vu quelque chose de semblable ? Une terre peut-elle naître en un jour ? Ou une nation peut-elle être enfantée d'un seul coup<!--Cette prophétie fait allusion à la création de l'État d'Israël le 14 mai 1948.--> ? Pourtant dès que Sion a été en travail, elle a enfanté ses enfants !
66:9	Moi qui ouvre le sein maternel, ne ferais-je pas enfanter ? dit YHWH. Moi qui fais enfanter, le fermerais-je ? dit ton Elohîm.

### Réjouissance à Yeroushalaim (Jérusalem) et consolation

66:10	Réjouissez-vous avec Yeroushalaim, exultez en elle, vous tous qui l'aimez ! Réjouissez-vous avec elle de sa joie, vous tous qui menez deuil sur elle !
66:11	Afin que vous soyez allaités et rassasiés par le sein de ses consolations, afin que vous suciez et que vous vous délectiez de la plénitude de sa gloire.
66:12	Car ainsi parle YHWH : Voici, je ferai couler vers elle la paix comme un fleuve, et la gloire des nations comme un torrent qui déborde, et vous serez allaités, vous serez portés sur les côtés et caressés sur les genoux.
66:13	Je vous consolerai pour vous apaiser, comme un homme que sa mère caresse pour l'apaiser, vous serez consolés dans Yeroushalaim.

66:14	Vous le verrez et votre cœur se réjouira, et vos os germeront comme l'herbe. La main de YHWH se fera connaître à ses serviteurs, mais il sera indigné contre ses ennemis.

### Jugement de YHWH<!--Voir Mt. 25:31-46 ; Ap. 19:11-21.-->

66:15	Car voici, YHWH viendra avec le feu<!--Voir 2 Th. 1:6-8 ; Hé. 12:29 ; 2 Pi. 3:7-12 ; Jud. 1:15 ; Ap. 1:14 et 19:12.-->, et ses chars<!--Voir Ap. 19:14.--> seront comme le vent d'orage, afin de tourner sa colère en fureur, et sa menace en flamme de feu.
66:16	Car c’est par le feu, avec son épée<!--Voir Ap. 19:15.-->, que YHWH juge toute chair, et les tués de YHWH seront en grand nombre.
66:17	Ceux qui se sanctifient et se purifient au milieu des jardins, l'un après l'autre, qui mangent de la chair de porc et des choses abominables, comme des souris, seront ensemble consumés, – déclaration de YHWH.
66:18	Moi, je viens rassembler<!--Voir Mt. 25:31-46.--> toutes les nations et toutes les langues, avec leurs œuvres et leurs pensées. Elles viendront et verront ma gloire.

### Toutes les nations adoreront YHWH

66:19	Je mettrai sur eux un signe et j'enverrai certains de leurs rescapés vers les nations, à Tarsis, à Poul, à Loud, gens tirant de l'arc, à Toubal et à Yavan, les îles lointaines, qui n'ont pas entendu de rumeur à mon sujet et qui n'ont pas vu ma gloire. Ils raconteront ma gloire parmi les nations.
66:20	Ils feront venir tous vos frères d'entre toutes les nations, sur des chevaux, sur des chars et dans des litières, sur des mulets et sur des dromadaires, en offrande à YHWH, à la montagne sainte, à Yeroushalaim, dit YHWH, comme lorsque les fils d'Israël apportent l'offrande dans un vase pur, à la maison de YHWH.
66:21	Je prendrai aussi parmi eux des prêtres, des Lévites, dit YHWH.
66:22	Car comme les nouveaux cieux et la nouvelle Terre que je ferai se tiendront en face de moi, – déclaration de YHWH –, ainsi votre postérité se tiendra avec votre nom.
66:23	Il arrivera que de nouvelle lune en nouvelle lune, et de shabbat en shabbat, toute chair viendra se prosterner en face de moi, dit YHWH.
66:24	Ils sortiront et verront les cadavres des hommes qui se sont rebellés contre moi. Car leur ver ne mourra pas, et leur feu ne s'éteindra pas<!--Mc. 9:48.-->, et ils deviendront une aversion pour toute chair.
