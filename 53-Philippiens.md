# Philippiens (Ph.)

Auteur : Paulos (Paul)

Thème : Expérience chrétienne

Date de rédaction : Env. 60 ap. J.-C.

Fondée par Philippos II (Philippe II) (382 – 336 av. J.-C.) en 356 av. J.-C., Philippes est une ville grecque de Macédoine orientale. Située sur une voie romaine qui traversait les Balkans (Via Egnatia), elle est restée de taille modeste en dépit de son fort taux de fréquentation.

La première mention de l'assemblée de Philippes se trouve dans Actes 16, lors de la rencontre de Paulos (Paul) avec des femmes réunies à l'extérieur de la ville pour la prière. À travers les paroles de Paulos, le Seigneur toucha particulièrement Ludia (Lydie) qui, après avoir été baptisée avec sa famille, le reçut avec ses compagnons d'œuvre dans sa maison.

C'est à Rome, sous le règne de Néron (37 – 68 ap. J.-C.), que Paulos, alors captif, rédigea cette lettre. Cet écrit de l'apôtre accusait réception d'un don monétaire que l'assemblée de Philippes lui avait fait parvenir par le biais d'Epaphroditos (Épaphrodite). Paulos y exprimait sa joie en dépit des souffrances et invitait les Philippiens à faire de même. Loin des erreurs doctrinales reprochées à d'autres, ces chrétiens de Philippes recevaient ainsi l'expression de l'affection de Paulos et ses encouragements à persévérer dans la foi en Mashiah en toutes circonstances.

## Chapitre 1

### Introduction

1:1	Paulos et Timotheos, esclaves de Yéhoshoua Mashiah, à tous les saints en Mashiah Yéhoshoua qui sont à Philippes, avec les surveillants et les diacres :
1:2	à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:3	Je rends grâce à mon Elohîm chaque fois que je fais mention de vous,
1:4	toujours, en chaque supplication pour vous tous, faisant ma supplication avec joie,
1:5	au sujet de votre contribution volontaire à l'Évangile, depuis le premier jour jusqu'à maintenant.
1:6	Étant persuadé de ceci : que celui qui a commencé la bonne œuvre en vous, la mènera à son terme jusqu'au jour de Yéhoshoua Mashiah.
1:7	Comme il est juste que je pense cela de vous tous, parce que je vous ai dans mon cœur, vous qui êtes tous, soit dans mes liens, soit dans la plaidoirie et la confirmation de l’Évangile, participants de la grâce avec moi.
1:8	Car Elohîm m'est témoin que je soupire après vous tous dans les entrailles de Yéhoshoua Mashiah.
1:9	Et ce que je demande en priant, c'est que votre amour abonde encore de plus en plus en connaissance précise et correcte et en toute perception,
1:10	pour éprouver<!--Voir 1 Ti. 3:10.--> ce qui est important, afin que vous soyez purs et irréprochables pour le jour du Mashiah,
1:11	étant remplis de fruits de justice, par le moyen de Yéhoshoua Mashiah à la gloire et à la louange d'Elohîm.

### Les chrétiens encouragés par les souffrances de Paulos (Paul)

1:12	Or je veux que vous sachiez, frères, que les choses qui me sont arrivées ont plutôt servi au progrès de l'Évangile.
1:13	De sorte que mes liens en Mashiah sont devenus manifestes dans tout le prétoire, et partout ailleurs.
1:14	Et la plupart des frères dans le Seigneur, ayant pris confiance par mes liens, ont beaucoup plus d'audace pour annoncer la parole sans aucune crainte.
1:15	En effet, certains prêchent le Mashiah par envie et dispute, mais d’autres aussi par bienveillance.
1:16	Les uns annoncent en effet le Mashiah par un esprit de parti<!--Voir Ph. 2:3.-->, et non pas avec sincérité, croyant ajouter de la tribulation à mes liens.
1:17	Mais ceux-ci par amour, sachant que je suis établi pour la plaidoirie de l'Évangile<!--Les versets 16 et 17 sont inversés dans les bibles basées sur les textes minoritaires. Le texte majoritaire (byzantin) présente les versets dans cet ordre.-->.
1:18	Car, de toute façon, que ce soit seulement par prétexte ou en vérité, Mashiah est annoncé. Et en cela je m'en réjouis et m'en réjouirai.
1:19	Car je sais que cela tournera à mon salut par le moyen de votre supplication et de l'assistance de l'Esprit de Yéhoshoua Mashiah,
1:20	selon ma ferme attente<!--Ferme espoir et attente dans l'anxiété.--> et mon espérance que je n'aurai honte de rien. Mais avec une entière assurance, maintenant comme toujours, Mashiah sera glorifié dans mon corps, soit par le moyen de la vie, soit par le moyen de la mort.
1:21	Car pour moi, vivre c'est le Mashiah, et mourir est un avantage.
1:22	Mais si vivre dans cette chair m’est un fruit pour l'œuvre, je ne sais même pas lequel je choisirai.
1:23	Car je suis pressé des deux côtés : j'ai le désir de quitter<!--« Quitter » est la traduction du grec « analuo » qui signifie « délier » ; « partir, quitter cette vie, interrompre, retourner » ; etc.--> cette vie et d'être avec Mashiah, c'est beaucoup plus avantageux,
1:24	mais demeurer dans la chair est plus nécessaire à cause de vous.
1:25	Et, persuadé de ceci, je sais que je resterai et que je demeurerai avec vous tous, pour votre progrès et la joie dans la foi,
1:26	afin que le sujet que vous avez de vous glorifier en moi abonde de nouveau en Mashiah Yéhoshoua, à travers ma parousie auprès de vous.
1:27	Seulement, conduisez-vous comme des citoyens<!--Vient du grec « politeuomai » qui signifie : « être un citoyen », « administrer les affaires civiles, gérer l'état », « faire ou créer un citoyen », « se conduire comme un citoyen », « se rendre capable de reconnaître les lois », « se conduire comme engagé à un certain mode de vie ». Voir Ac. 23:1.-->, d'une manière digne de l'Évangile du Mashiah, afin que, soit que je vienne vous voir, soit que je reste absent, j'entende quant à votre état, que vous persistez dans un même esprit, luttant ensemble d'une même âme pour la foi de l'Évangile,
1:28	et n'étant en rien effrayés par vos adversaires. En effet, pour eux c'est une preuve de perdition, mais pour vous de salut, et cela de la part d'Elohîm.
1:29	Parce qu'il vous a été gratuitement donné en ce qui concerne Mashiah, non seulement de croire en lui, mais aussi de souffrir pour lui,
1:30	ayant le même combat que vous avez vu en moi et que vous apprenez être maintenant en moi.

## Chapitre 2

### Exhortation à l'unité

2:1	Si donc il y a quelque consolation dans le Mashiah, s'il y a quelque parole persuasive dans l'amour, s'il y a quelque communion d'esprit, s'il y a quelque compassion et quelque miséricorde,
2:2	comblez-moi de joie afin que vous ayez la même pensée<!--Voir Ph. 2:5.-->, ayant le même amour, étant d'un même accord<!--Ce mot est composé de « sun » (ensemble, avec) et « psuchos » (l'âme, le moi, la vie intérieure, ou le siège des sentiments, des désirs, des affections). Ainsi le mot se réfère à une unité dans l'esprit, d'une façon harmonieuse. Paulos désirait que les Philippiens soient unis dans leurs affections, qu'ils soient en Mashiah dans tous leurs désirs ! Le mot ne se trouve qu'ici dans le Testament de Yéhoshoua.-->, d'une même pensée,
2:3	rien par esprit de parti<!--« Parti » est la traduction du grec « eritheia ». Avant la nouvelle alliance, ce mot ne se trouve que dans les écrits d'Aristote (philosophe grec, disciple de Platon, né en 384 et mort en 322 av. J.-C.) où il dénote une « recherche personnelle, la poursuite d'une fonction politique par des moyens injustes ». Ce mot signifie aussi « faire une campagne électorale » ou « intriguer pour une fonction dans un esprit partisan, querelleur ». On retrouve le mot grec dans Ja. 3:14,16 ; Ga. 5:20 ; Ro. 2:8 ; Ph. 1:16.-->, ou par vaine gloire, mais par humilité, estimez les autres supérieurs à vous-mêmes.
2:4	Ne regardez pas chacun à votre propre intérêt, mais aussi à celui des autres.

### L'humilité du Mashiah

2:5	Qu'il y ait donc en vous la même pensée que dans le Mashiah Yéhoshoua,
2:6	lequel étant en forme d'Elohîm, n’a pas considéré comme une proie à saisir d’être égal<!--Es. 46:5.--> à Elohîm.
2:7	Mais il s'est vidé de lui-même en prenant la forme d'esclave, en devenant semblable aux humains,
2:8	et, reconnu à son apparence comme un être humain, il s'est abaissé lui-même, en se rendant obéissant jusqu'à la mort, même jusqu'à la mort de la croix.
2:9	C'est pourquoi aussi Elohîm l'a élevé à la suprême majesté et lui a donné le Nom<!--Es. 45:23. Voir Jn. 17:6,11-12,26.--> qui est au-dessus de tout nom,
2:10	afin qu'au Nom de Yéhoshoua fléchisse tout genou des êtres célestes et terrestres, et de ceux qui demeurent dans le monde souterrain,
2:11	et que toute langue confesse que Yéhoshoua Mashiah est le Seigneur, à la gloire d'Elohîm le Père.
2:12	C'est pourquoi, mes bien-aimés, comme vous avez toujours obéi, accomplissez votre propre salut avec crainte et tremblement, non seulement comme en ma parousie, mais beaucoup plus maintenant que je suis absent.
2:13	Car c'est Elohîm qui opère en vous et le vouloir et le faire pour son plaisir.
2:14	Faites toutes choses sans murmures et sans raisonnements,
2:15	afin que vous deveniez sans reproche et sans mélange<!--Voir Mt. 10:16.-->, des enfants d'Elohîm innocents au milieu d'une génération tordue<!--Voir Ac. 2:40.--> et déformée, parmi laquelle vous brillez comme des luminaires<!--Voir Ap. 21:11.--> dans le monde,
2:16	portant la parole de vie, pour m’être un sujet de gloire au jour du Mashiah, puisque je n'aurai pas couru en vain ni travaillé en vain.
2:17	Mais même si je suis versé comme une libation<!--« Verser comme une boisson d'offrande », « faire une libation ». Le terme grec « spendo » fait allusion à quelqu'un dont le sang est versé dans une mort violente pour la cause d'Elohîm.--> sur le sacrifice et le service de votre foi, j'en suis heureux et je me réjouis avec vous tous.
2:18	Vous aussi, pareillement, soyez heureux et réjouissez-vous avec moi.

### Paulos (Paul) témoigne de Timotheos (Timothée) et d'Epaphroditos (Épaphrodite)

2:19	Et j'espère dans le Seigneur Yéhoshoua vous envoyer rapidement Timotheos pour être moi-même encouragé en apprenant ce qui vous concerne.
2:20	Car je n'ai personne d'un esprit égal qui s'occupera sincèrement de ce qui vous concerne,
2:21	car tous cherchent leurs propres intérêts, et non ceux de Mashiah Yéhoshoua.
2:22	Mais vous savez qu'il a été mis à l'épreuve, puisqu'il a servi avec moi dans l'Évangile, comme un enfant avec son père.
2:23	C'est donc lui en effet que j'espère envoyer dès que j'aurai vu clair ce qui me concerne.
2:24	Mais j'ai cette confiance dans le Seigneur que moi-même aussi je viendrai rapidement.
2:25	Mais j'ai estimé nécessaire d'envoyer vers vous Epaphroditos, mon frère, et mon compagnon d'œuvre et mon compagnon de combat, mais votre apôtre<!--Vient du grec « apostolos » qui signifie « apôtre », « envoyé », « un délégué », « un messager », « celui qui est envoyé avec des ordres ».--> et ministre pour mon besoin.
2:26	Puisqu'il soupirait après vous tous et qu'il était troublé parce que vous aviez appris qu'il avait été malade.
2:27	Car il a été malade, et tout près de la mort, mais Elohîm a eu pitié de lui, et non seulement de lui, mais aussi de moi, afin que je n'aie pas douleur sur douleur.
2:28	C’est pourquoi je me suis hâté de l'envoyer, afin qu'en le voyant de nouveau vous vous réjouissiez, et que je sois moi-même libéré de la peine.
2:29	Recevez-le donc dans le Seigneur avec une joie entière et ayez de l'estime pour ceux qui sont tels que lui.
2:30	Parce qu'il a été près de la mort à cause de l'œuvre du Mashiah, n'ayant pas regardé à son âme, afin de suppléer à l'insuffisance de votre service envers moi.

## Chapitre 3

### Le légalisme et la justice de la loi mosaïque

3:1	Au reste, mes frères, réjouissez-vous dans le Seigneur. En effet, vous écrire les mêmes choses n'est pas pénible<!--Littéralement « paresseux ».--> pour moi, et pour vous c'est une sécurité.
3:2	Prenez garde aux chiens, prenez garde aux mauvais ouvriers, prenez garde à la mutilation<!--Vient du grec « katatome » qui signifie aussi « couper ».-->.
3:3	Car c'est nous qui sommes la circoncision, nous qui servons Elohîm en Esprit, et qui nous glorifions en Mashiah Yéhoshoua et qui ne nous confions pas en la chair.
3:4	Bien que j'aie aussi confiance dans la chair. Si quelqu’un d’autre pense se confier dans la chair, à plus forte raison moi :
3:5	circoncis dès le huitième jour, de la race d'Israël, de la tribu de Benyamin, Hébreu né d'Hébreux, pharisien en ce qui concerne la torah.
3:6	Quant au zèle, persécutant l'Assemblée. Quant à la justice à l'égard de la torah, devenu sans reproche.

### L'excellence de la connaissance de Yéhoshoua ha Mashiah

3:7	Mais ces choses qui étaient pour moi un avantage, je les ai regardées comme une perte<!--Le mot grec signifie aussi « dommage ». Voir Ac. 27:10,21.--> à cause du Mashiah.
3:8	Mais au contraire, je considère même que toutes choses sont une perte à cause de l'excellence de la connaissance du Mashiah Yéhoshoua, le Seigneur, à cause duquel, j’ai perdu<!--« Affecter d'un dommage », « endommager », « soutenir un dommage », « subir un préjudice », « souffrir une perte ».--> toutes choses, et je les considère comme les excréments des animaux afin de gagner Mashiah,
3:9	et d'être trouvé en lui, ayant non pas ma propre justice, celle qui vient de la torah, mais celle qui est par la foi au Mashiah, la justice qui vient d'Elohîm par la foi,
3:10	pour le connaître, lui, et la puissance de sa résurrection et la communion de ses souffrances, en devenant conforme à lui dans sa mort,
3:11	pour arriver en effet, par n'importe quel moyen, à la résurrection d'entre les morts.
3:12	Non que j’aie déjà reçu cela ou que je sois déjà rendu parfait, mais je cours pour le saisir, et c'est pour cela aussi que j'ai été saisi par le Mashiah, Yéhoshoua.
3:13	Frères, pour moi, je n’estime pas moi-même l’avoir saisi, mais une seule chose compte : oubliant en effet les choses qui sont en arrière et me portant vers celles qui sont devant,
3:14	je cours vers le but<!--« La marque distante que l'on regarde, le but que quelqu'un a en vue. »--> pour le prix<!--« La récompense du vainqueur dans les jeux. » 1 Co. 9:24.--> de l'appel céleste d'Elohîm en Yéhoshoua Mashiah.
3:15	C'est pourquoi, comme nous sommes parfaits, ayons cette même pensée et, si en quelque chose vous pensez autrement, Elohîm vous révèlera ces choses aussi.
3:16	Cependant, marchons suivant une même règle<!--Du grec « kanon » signifiant « règle, modèle », lui-même emprunté à l'hébreu « qaneh » qui signifie « roseau, mesure, canne ». Voir Ga. 6:16.--> pour les choses auxquelles nous sommes parvenus, et ayons un même sentiment.
3:17	Devenez tous ensemble mes imitateurs, frères, et regardez à ceux qui marchent selon le modèle que vous avez en nous.
3:18	Car beaucoup dont je vous ai souvent parlé et dont je parle maintenant même en pleurant, marchent en ennemis de la croix du Mashiah.
3:19	Eux dont la fin est la destruction, qui ont pour elohîm leur ventre, qui mettent leur gloire dans leur honte, et qui ne pensent<!--Ro. 8:5.--> qu'aux choses de la Terre.
3:20	Car notre communauté des citoyens est dans les cieux, d'où nous attendons aussi assidûment et patiemment le Sauveur, le Seigneur Yéhoshoua Mashiah,
3:21	qui transformera le corps de notre humiliation pour le rendre conforme au corps de sa gloire selon l'efficacité<!--Ep. 3:7.--> par laquelle il peut même se soumettre toutes choses.

## Chapitre 4

### Encouragements

4:1	C'est pourquoi, mes frères bien-aimés et désirés, ma joie et ma couronne, demeurez ainsi fermes dans le Seigneur, bien-aimés.
4:2	J'exhorte Euodia et j'exhorte Syntyche à avoir une même pensée dans le Seigneur.
4:3	Et je te le demande aussi, mon vrai compagnon<!--Le mot « compagnon » est la traduction du grec « suzugos » qui signifie littéralement « ensemble sous le joug, compagnon de peine, de joug ». Cette expression renvoie à 2 Co. 6:14.--> de peine, aide-les, elles qui ont partagé mes luttes dans l'Évangile, avec Clément aussi et mes autres compagnons d'œuvre, dont les noms sont dans le livre de vie.
4:4	Réjouissez-vous toujours dans le Seigneur ! Je dirai encore : réjouissez-vous !
4:5	Que votre douceur soit connue de tous les humains. Le Seigneur est proche.
4:6	Ne vous inquiétez de rien, mais en toute chose faites connaître vos requêtes à Elohîm par la prière et la supplication avec action de grâce.
4:7	Et la paix d'Elohîm, qui surpasse toute intelligence, gardera vos cœurs et vos pensées en Mashiah Yéhoshoua.

### L'objet de nos pensées

4:8	Au reste, frères, que toutes les choses qui sont vraies, toutes les choses honorables<!--« Vénérable », « auguste », « révérend ».-->, toutes les choses justes, toutes les choses pures, toutes les choses acceptables, toutes les choses de bonne renommée, toutes les choses où il y a quelque vertu et quelque louange, ces choses, méditez-les !
4:9	Vous les avez aussi apprises, et reçues, et entendues et vues en moi : mettez-les en pratique, et l'Elohîm de paix sera avec vous.

### Le contentement dans l'abondance et dans la pauvreté

4:10	Or je me suis réjoui grandement dans le Seigneur de ce qu'enfin vous avez fait refleurir votre pensée pour moi. Vous y pensiez bien, mais vous manquiez d'occasion.
4:11	Je ne dis pas cela à cause de la pauvreté, car j'ai appris à être content de l'état où je me trouve.
4:12	Mais je sais être abaissé, je sais aussi être dans l'abondance. En tout et en tous je suis enseigné pleinement<!--« Initié dans les mystères », « instruire », « accoutumer quelqu'un à une chose », « donner à quelqu'un une connaissance intime d'une chose ».--> à être rassasié et à avoir faim, à être dans l'abondance et à être dans le besoin.
4:13	Je peux tout dans le Mashiah qui me fortifie.
4:14	Néanmoins, vous avez bien fait en participant à ma tribulation.
4:15	Or vous savez aussi, vous Philippiens, qu'au commencement de l'Évangile, quand j'ai quitté la Macédoine, aucune assemblée ne s'est jointe à moi comme associée pour un discours sur ce qui est donné et sur celui qui reçoit, excepté vous seuls.
4:16	Parce que, même à Thessalonique, vous m'avez envoyé une fois et même deux fois ce dont j'avais besoin.
4:17	Ce n'est pas que je cherche le don, mais je cherche le fruit qui se multiplie pour votre compte.
4:18	Mais j'ai tout et je suis dans l'abondance. J'ai été comblé de biens en recevant d'Epaphroditos ce qui vient de vous, un parfum de bonne odeur, un sacrifice accepté, agréable à Elohîm.
4:19	Mais mon Elohîm comblera<!--Vient du grec « pleroo » qui signifie « remplir jusqu'au bord », « amener à la réalisation, réaliser », « accomplir ». Voir Col. 2:10.--> tous vos besoins selon sa richesse, avec gloire en Yéhoshoua Mashiah.

### Salutations

4:20	Mais à notre Elohîm et Père soit la gloire pour les âges des âges ! Amen !
4:21	Saluez chaque saint en Mashiah Yéhoshoua. Les frères qui sont avec moi vous saluent.
4:22	Tous les saints, et surtout ceux de la maison de César, vous saluent.
4:23	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous tous, Amen !
