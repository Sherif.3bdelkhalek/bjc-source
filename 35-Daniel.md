# Daniye'l (Daniel) (Da.)

Signification : El est mon juge

Auteur : Daniye'l (Daniel)

Thème : Ascension et chute des royaumes

Date de rédaction : 6ème siècle av. J.-C.

Issu d'une famille princière de Yéhouda (Juda), Daniye'l fut déporté de Yeroushalaim (Jérusalem) à Babel (Babylone) pendant sa jeunesse, sous le règne de Neboukadnetsar. Lui et trois de ses amis – eux aussi de noble lignée – furent choisis pour être instruits selon la sagesse babylonienne en vue de servir le roi. Fervent dans sa foi en YHWH, Daniye'l – imité ensuite par ses compagnons – résolut de ne pas se souiller et obtint ainsi la faveur de son Elohîm. Son intégrité et sa crainte d'Elohîm lui valurent de miraculeuses victoires, de nombreuses distinctions et une grande sagesse. Daniye'l avait reçu du discernement pour expliquer rêves et visions et délivra plusieurs prophéties dont certaines se sont déjà accomplies, d'autres se réaliseront à la fin du temps des nations, au moment du retour du Mashiah.

Elohîm témoigna de la justice de Daniye'l au prophète Yehezkel (Ézéchiel) dont il fut contemporain.

## Chapitre 1

### Yéhouda livré à la captivité babylonienne

1:1	La troisième année du règne de Yehoyaqiym, roi de Yéhouda, Neboukadnetsar, roi de Babel<!--Babylone.-->, vint à Yeroushalaim et l'assiégea.
1:2	Adonaï livra entre ses mains Yehoyaqiym<!--Après avoir organisé un premier siège de la ville de Yeroushalaim (Jérusalem) et une première déportation en 605 av. J.-C., le puissant roi babylonien Neboukadnetsar revint mater la révolte de Yehoyaqiym qui complotait avec l'Égypte pour se défaire du protectorat auquel il avait été soumis. Le 16 mars 597 av. J.-C., le souverain babylonien assiégea de nouveau la cité, déporta le roi Yehoyakiyn, et une partie de ses dignitaires, et le remplaça par son oncle Tsidqiyah (Sédécias). C'est ainsi qu'en cette période de Pâque qui commémorait la délivrance du joug égyptien (Ex. 12), les Hébreux se retrouvèrent paradoxalement de nouveau esclaves.-->, roi de Yéhouda et une partie des vases de la maison d'Elohîm. Il les fit venir en terre de Shinear<!--Shinear : « le pays des deux fleuves ». C'est l'ancien nom du territoire qui est devenu Babylonie ou Chaldée. Il s'agit de la terre de Nimrod (Ge. 10:6-12). C'est à Shinear qu'on tenta de construire la tour de Babel et de mettre en place le premier gouvernement mondial.-->, dans la maison de son elohîm, il les mit dans la maison du trésor de son elohîm.
1:3	Le roi dit à Ashpenaz, chef de ses eunuques, de faire venir quelques-uns des fils d'Israël de la postérité royale<!--Daniye'l était de la race royale (2 R. 20:16-19 ; Es. 39:6-7).--> et des principaux seigneurs,
1:4	des enfants en qui il n'y avait aucun défaut physique, beaux de figure, ayant de la perspicacité en toute sagesse, du savoir, de la connaissance, du discernement, pleins d'intelligence, qui avaient aussi en eux la force de se tenir debout dans le palais du roi, à qui l'on enseignerait les lettres et la langue des Chaldéens.
1:5	Le roi leur assigna, parole du jour en son jour, des mets délicats du roi et du vin de ses festins, afin de les faire croître<!--« Devenir grand ou important », « promouvoir », « rendre puissant », « louer », « glorifier », « faire de grandes choses ».--> ainsi pendant trois ans au bout desquels ils se tiendraient debout en face du roi.
1:6	Il y avait parmi eux, d'entre les fils de Yéhouda, Daniye'l, Chananyah, Miyshael et Azaryah.
1:7	Le chef des eunuques leur imposa des noms : il imposa à Daniye'l celui de Beltshatsar, à Chananyah celui de Shadrac, à Miyshael celui de Méshac et à Azaryah celui d'Abed-Négo.

### La fermeté de Daniye'l à Babel

1:8	Daniye'l mit dans son cœur de ne pas se souiller par les mets délicats du roi et par le vin de ses festins. C'est pourquoi il supplia le chef des eunuques afin qu'il ne l'oblige pas à se souiller.
1:9	Elohîm accorda à Daniye'l bonté et compassion devant le chef des eunuques.
1:10	Le chef des eunuques dit à Daniye'l : Je crains le roi, mon seigneur, qui a dénombré votre viande et vos festins, car pourquoi verrait-il vos faces plus tristes que celles des enfants de votre âge ? Vous exposeriez alors ma tête auprès du roi !
1:11	Daniye'l dit au gardien que le chef des eunuques avait assigné à Daniye'l, Chananyah, Miyshael et Azaryah :
1:12	Éprouve, s'il te plaît, tes serviteurs pendant 10 jours, et qu'on nous donne des légumes à manger et de l'eau à boire.
1:13	On regardera, en ta présence, notre apparence et l'apparence des enfants qui mangent les mets délicats du roi, et tu agiras envers tes serviteurs selon ce que tu auras vu.
1:14	Il les écouta dans cette affaire et les éprouva pendant 10 jours.
1:15	Au bout des 10 jours, leur apparence paraissait meilleure et plus grasse en chair que tous les enfants qui mangeaient les mets délicats du roi.
1:16	Il arriva que le gardien emportait les mets délicats et le vin de leur festin et leur donnait des légumes.
1:17	Elohîm donna à ces quatre enfants de la connaissance et de l'intelligence dans toutes les lettres, et de la sagesse. Daniye'l comprenait toutes les visions et tous les rêves.
1:18	Et à la fin des jours fixés par le roi pour qu'on les lui amène, le chef des eunuques les présenta à Neboukadnetsar.
1:19	Le roi parla avec eux et, parmi eux tous, il ne s'en trouva aucun comme Daniye'l, Chananyah, Miyshael et Azaryah. C'est pourquoi ils se tinrent debout en face du roi.
1:20	Quant à toute parole de sagesse et de discernement sur laquelle le roi les interrogeait, il trouvait dix fois plus de force en eux que dans tous les magiciens et les astrologues qui étaient dans tout son royaume.
1:21	Et Daniye'l fut là jusqu’en l’an un du roi Cyrus.

## Chapitre 2

### Les sages de Babel condamnés à mort

2:1	La deuxième année du règne de Neboukadnetsar, Neboukadnetsar rêva des rêves. Son esprit fut agité et son sommeil finit.
2:2	Le roi dit d’appeler les magiciens, les astrologues, les sorciers et les Chaldéens, pour qu'ils révèlent au roi ses rêves. Ils vinrent et se tinrent debout en face du roi.
2:3	Le roi leur dit : J'ai rêvé un rêve. Mon esprit est agité en cherchant à connaître ce rêve<!--Ce rêve annonce la future mise en place d'un gouvernement mondial. Voir commentaire en Da. 7:3.-->.
2:4	Et les Chaldéens répondirent au roi en langue araméenne<!--Da. 2:4 à 7:28, le livre est écrit en araméen.--> : Roi, vis éternellement ! Dis le rêve à tes serviteurs et nous expliquerons son interprétation.
2:5	Le roi répondit et dit aux Chaldéens : La parole est émanée de moi : si vous ne me faites connaître le rêve et son interprétation, vous serez mis en pièces et vos maisons seront réduites en un tas de déchets.
2:6	Mais si vous m'expliquez le rêve et son interprétation, vous recevrez en face de moi, des dons, des récompenses et beaucoup d'honneurs. C'est pourquoi, expliquez-moi le rêve et son interprétation.
2:7	Ils répondirent pour la seconde fois et dirent : Que le roi dise le rêve à ses serviteurs et nous expliquerons son interprétation.
2:8	Le roi répondit et dit : Je sais maintenant que vous ne cherchez qu'à gagner du temps, parce que vous voyez que la parole est émanée de moi.
2:9	Mais si vous ne me faites pas connaître le rêve, il y aura un même décret contre vous tous. Car vous vous êtes mis d'accord d'avance pour dire devant moi une parole mensongère et corrompue en attendant que les temps changent. C'est pourquoi, dites-moi le rêve et je saurai que vous pouvez m'expliquer son interprétation.
2:10	Les Chaldéens répondirent au roi et dirent : Il n'y a aucun être humain sur la Terre qui puisse expliquer ce que le roi demande. Et aussi il n'y a ni roi, ni seigneur, ni gouverneur qui ait jamais demandé une telle chose à quelque magicien, astrologue ou Chaldéen que ce soit.
2:11	Car la chose que le roi demande est difficile<!--Rare.--> et il n'y a personne qui puisse l'expliquer au roi, excepté les élahhs dont la demeure n'est pas avec la chair<!--Jn. 1:14.-->.
2:12	À cause de cela, le roi se mit en colère et se fâcha extrêmement. Il commanda de détruire tous les sages de Babel.
2:13	Le décret fut publié. On mettait à mort les sages et l'on cherchait Daniye'l et ses compagnons pour les tuer.

### Daniye'l implore la miséricorde d'Élahh

2:14	Alors Daniye'l répondit avec conseil et jugement à Aryok, chef des gardes du roi, qui était sorti pour tuer les sages de Babel.
2:15	Il répondit et dit à Aryok, gouvernant du roi : Pourquoi le décret du roi est-il si rude ? Aryok exposa la chose à Daniye'l.
2:16	Et Daniye'l entra et pria le roi de lui accorder du temps pour expliquer l'interprétation au roi.
2:17	Alors Daniye'l alla dans sa maison et informa de cette affaire Chananyah, Miyshael et Azaryah, ses compagnons,
2:18	pour demander la compassion devant l'Élahh des cieux au sujet de ce secret, afin que Daniye'l et ses compagnons ne soient pas détruits avec le reste des sages de Babel.

### Le rêve de la grande statue révélé à Daniye'l

2:19	Et le secret fut révélé à Daniye'l dans une vision pendant la nuit. Et Daniye'l bénit l'Élahh des cieux.
2:20	Daniye'l répondit et dit : Béni soit le Nom d'Élahh, d'éternité en éternité, car c'est à lui qu'appartiennent la sagesse et la force<!--Job 12:13 ; Ap. 5:12, 7:12.--> !
2:21	C'est lui qui change les temps et les saisons, qui renverse les rois et qui établit les rois, qui donne la sagesse aux sages et la connaissance à ceux qui ont de l'intelligence.
2:22	C'est lui qui révèle les choses profondes et cachées, il connaît les choses qui sont dans les ténèbres et la lumière demeure avec lui<!--De. 29:28 ; Es. 48:6 ; Jé. 33:3 ; Lu. 12:2-3 ; Hé. 4:13.-->.
2:23	Élahh de nos pères ! Je rends grâces et je te loue de ce que tu m'as donné de la sagesse et de la force, et de ce que tu m'as maintenant fait connaître ce que nous t'avons demandé, en nous faisant connaître le secret du roi.
2:24	C'est pourquoi Daniye'l entra chez Aryok, à qui le roi avait ordonné de détruire les sages de Babel. Il alla et lui parla ainsi : Ne détruis pas les sages de Babel, fais-moi entrer devant le roi et j'expliquerai au roi l'interprétation.
2:25	Alors Aryok fit promptement entrer Daniye'l devant le roi et lui parla ainsi : J'ai trouvé parmi les fils captifs de Yéhouda un homme qui donnera au roi l'interprétation.
2:26	Le roi répondit et dit à Daniye'l, dont le nom était Beltshatsar : Es-tu capable de me faire connaître le rêve que j’ai vu et son interprétation ?
2:27	Daniye'l répondit en face du roi et dit : Ce que le roi demande est un secret que les sages, les astrologues, les magiciens et les devins ne sont pas capables d'expliquer au roi.
2:28	Mais il y a dans les cieux l'Élahh qui révèle les secrets et qui a fait connaître au roi Neboukadnetsar ce qui arrivera dans les derniers jours<!--« Les derniers jours » voir commentaires dans Ge. 49:1 ; Ac. 2:14-17.-->. Voici ton rêve et les visions de ta tête, sur ta couche :
2:29	Sur ta couche, roi, il t'est monté des pensées concernant ce qui arrivera ci-après. Et celui qui révèle les secrets t'a fait connaître ce qui arrivera.
2:30	Et moi, ce n’est pas à cause d’une sagesse qui, en moi, serait supérieure à celle de tous les vivants que ce secret m’a été révélé, mais c’est afin que l’interprétation en soit donnée au roi et que tu connaisses les pensées de ton cœur.
2:31	Toi, roi, tu regardais, et voici une grande statue<!--Voir annexe « La statue de Neboukadnetsar ».-->. Cette statue était grande et d'une splendeur extraordinaire. Elle était debout devant toi et son apparence faisait peur.
2:32	La tête de cette statue était en or très fin, sa poitrine et ses bras, en argent, son ventre et ses cuisses, en cuivre,
2:33	ses jambes étaient en fer et ses pieds étaient en partie en fer et en partie en argile.
2:34	Tu regardais cela, jusqu'à ce qu'une pierre se détacha sans l'aide d'une main, frappa les pieds en fer et en argile de la statue et les brisa.
2:35	Alors le fer, l'argile, le cuivre, l'argent et l'or, furent brisés ensemble et devinrent comme la paille de l'aire en été, que le vent transporte çà et là et nulle trace n'en fut retrouvée. Mais la pierre qui avait frappé la statue devint une grande montagne et remplit toute la terre.

### Premier empire universel : Babel (Babylone)<!--Cp. Da. 7:4.-->

2:36	C'est là le rêve. Nous en dirons l'interprétation en face du roi.
2:37	Roi, tu es le roi des rois, parce que l'Élahh des cieux t'a donné le royaume, le pouvoir, la force et la gloire.
2:38	Il a remis entre tes mains, en quelque lieu qu'ils habitent, les enfants des humains, les bêtes des champs et les oiseaux des cieux, et il t'a fait dominer sur eux tous. C'est toi qui es la tête en or<!--Jé. 27:6-7.-->.

### Deuxième et troisième empire : la Médie et la Perse, et la Grèce<!--La Médie et la Perse : cp. Da. 7:5, 8:20 ; La Grèce : cp. Da. 7:6, 8:21.-->

2:39	Mais après toi, il s'élèvera un autre royaume, moindre que le tien, puis un autre royaume, un troisième qui sera en cuivre et qui dominera sur toute la Terre.

### Quatrième empire : Rome<!--Cp. Da. 7:7, 9:26.-->

2:40	Il y aura un quatrième royaume, fort comme du fer. De même que le fer brise et écrase tout, ainsi il brisera et écrasera tout, comme le fer qui met tout en pièces.
2:41	Puisque tu as vu que les pieds et les orteils étaient en partie en argile de potier et en partie en fer, ce royaume sera divisé, mais il y aura en lui quelque chose de la force du fer, parce que tu as vu le fer mêlé à l'argile de potier.
2:42	Les doigts des pieds étaient en partie en fer et en partie en argile. Ce royaume sera en partie fort et en partie fragile.
2:43	Puisque tu as vu le fer mêlé à l'argile de potier, ils seront mêlés à la semence<!--Vient de l'araméen « zera » qui signifie « semence » et « descendant ». Il y a l'idée des mariages.--> humaine, mais ils ne seront pas unis l'un à l'autre de même que le fer ne se mêle pas à l'argile.

### Le Royaume du Mashiah

2:44	Dans les jours de ces rois, l'Élahh des cieux suscitera un Royaume qui ne sera jamais détruit, et ce Royaume ne passera pas à un autre peuple. Il brisera et anéantira tous ces royaumes-là, et lui-même sera établi éternellement.
2:45	C'est pourquoi, tu as vu que de la montagne, une pierre a été coupée sans l'aide d'une main et qu'elle a brisé le fer, le cuivre, l'argile, l'argent et l'or. Le grand Élahh a fait connaître au roi ce qui arrivera après cela. Or le rêve est véritable et son interprétation est certaine.

### YHWH, l'Élahh qui révèle les secrets

2:46	Alors le roi Neboukadnetsar tomba sur ses faces<!--1 Co. 14:25.--> et se prosterna devant Daniye'l. Il ordonna de faire pour lui des sacrifices et des offrandes apaisants<!--Esd. 6:10.-->.
2:47	Le roi répondit à Daniye'l et lui dit : En vérité, votre Élahh est l'Élahh des élahhs, et le Seigneur des rois, et il révèle les secrets, puisque tu as pu déclarer ce secret.
2:48	Alors le roi éleva Daniye'l en dignité et lui fit de nombreux et riches présents. Il l'établit gouverneur sur toute la province de Babel et grand préfet de tous les sages de Babel.
2:49	Daniye'l demanda au roi de mettre à la tête de l’administration de la province de Babel, Shadrac, Méshac et Abed-Négo. Et Daniye'l se tenait à la porte du roi.

## Chapitre 3

### La statue d'or de Neboukadnetsar

3:1	Le roi Neboukadnetsar fit une statue d'or<!--Neboukadnetsar est un type de l'Anti-Mashiah (Antichrist) qui s'oppose aux plans d'Elohîm. Contrairement à la statue composée de plusieurs métaux qu'il avait vue en rêve, et où il était représenté par la tête en or (Da. 2:38), il s'est fait construire une statue entièrement en or, se déclarant ainsi symboliquement invincible et immortel. En agissant de la sorte, Neboukadnetsar s'était fait elohîm et a exigé par conséquent d'être adoré (2 Th. 2:3-4). D'un point de vue prophétique, cette statue annonce la mise en place d'une religion mondiale, fruit d'un mélange entre la politique et la religion.-->, dont la hauteur était de 60 coudées<!--30 mètres de haut. Voir tableau de poids et de mesures.-->, et la largeur de 6 coudées<!--3 mètres.-->. Il la dressa dans la vallée de Doura, dans la province de Babel.
3:2	Le roi Neboukadnetsar envoya des messagers pour rassembler les satrapes<!--Gouverneur d'une province de Perse, haut fonctionnaire de l'Empire babylonien, et du royaume de Darius le Mède.-->, les préfets, les gouverneurs, les conseillers, les trésoriers, les jurisconsultes, les juges, et tous les magistrats des provinces, afin qu'ils se rendent à la dédicace de la statue que le roi Neboukadnetsar avait dressée.
3:3	Alors se rassemblèrent les satrapes, les préfets, les gouverneurs, les conseillers, les trésoriers, les jurisconsultes, les juges, et tous les magistrats des provinces, pour la dédicace de la statue que le roi Neboukadnetsar avait dressée. Ils se tenaient debout devant la statue que le roi Neboukadnetsar avait dressée.
3:4	Le héraut proclama avec force : Il vous est ordonné, peuples, nations, et hommes de toutes langues :
3:5	Au temps où vous entendrez le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, de la cornemuse, et de toutes sortes d'instruments de musique, vous vous jetterez à terre et vous adorerez la statue d'or que le roi Neboukadnetsar a dressée.
3:6	Quiconque ne se jettera pas à terre et n'adorera pas sera jeté à l'instant même au milieu de la fournaise de feu ardent.
3:7	C'est pourquoi, au moment même où tous les peuples entendirent le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, et de toutes sortes d'instruments de musique, tous les peuples, les nations, et les hommes de toutes les langues, se jetèrent à terre et adorèrent la statue d'or que le roi avait dressée.

### Le refus de l'idolâtrie

3:8	En conséquence, au moment même, des hommes chaldéens s'approchèrent et accusèrent<!--Littéralement : « pour manger un morceau des Juifs ».--> les Juifs.
3:9	Et ils répondirent et dirent au roi Neboukadnetsar : Roi, vis éternellement !
3:10	Toi-même, roi, tu as pris un décret d'après lequel tout être humain qui entendrait le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, de la cornemuse, et de toutes sortes d'instruments de musique, devrait se jeter à terre et adorer la statue d'or,
3:11	et que quiconque ne se jetterait pas à terre et ne l'adorerait pas, serait jeté au milieu d'une fournaise de feu ardent.
3:12	Il y a des hommes juifs, à qui tu as remis l’administration de la province de Babel, Shadrac, Méshac et Abed-Négo, ces hommes-là, roi, ne tiennent aucun compte de ton décret. Ils ne servent pas tes élahhs et n'adorent pas la statue d'or que tu as dressée.
3:13	Alors le roi Neboukadnetsar, saisi de colère et de rage, ordonna qu'on amène Shadrac, Méshac, et Abed-Négo. Alors ces hommes furent amenés devant le roi.
3:14	Et le roi Neboukadnetsar répondit et leur dit : Est-il vrai, Shadrac, Méshac, et Abed-Négo, que vous ne servez pas mes élahhs et que vous n'adorez pas la statue d'or que j'ai dressée ?
3:15	Maintenant si vous êtes prêts, au moment où vous entendrez le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, de la cornemuse, et de toutes sortes d'instruments de musique, vous vous jetterez à terre, et vous adorerez la statue que j'ai faite. Si vous ne l'adorez pas, vous serez jetés à l'instant au milieu de la fournaise de feu ardent. Et qui est l'élahh qui vous délivrera de mes mains ?
3:16	Shadrac, Méshac et Abed-Négo répondirent et dirent au roi Neboukadnetsar : Nous n'avons pas besoin de te répondre une parole sur cela.
3:17	Si cela doit être, notre Élahh que nous servons est capable de nous délivrer de la fournaise de feu ardent, et il nous délivrera de ta main, roi !
3:18	Sinon, sache, roi, que nous ne servirons pas tes élahhs, et que nous n'adorerons pas la statue d'or que tu as dressée.

### L'épreuve de la fournaise de feu ardent

3:19	Alors Neboukadnetsar fut rempli de rage, et l’apparence<!--Vient d'un mot qui signifie aussi idole.--> de son visage changea à l'égard de Shadrac, Méshac et Abed-Négo. Il répondit et ordonna de chauffer la fournaise sept fois plus qu'on avait coutume de la chauffer.
3:20	Il ordonna aux hommes les plus forts et les plus vaillants qui étaient dans son armée de lier Shadrac, Méshac, et Abed-Négo, pour les jeter dans la fournaise de feu ardent.
3:21	Et en même temps ces hommes furent liés avec leurs manteaux, leurs tuniques, leurs turbans et leurs vêtements, et ils furent jetés au milieu de la fournaise de feu ardent.
3:22	Parce que la parole du roi était rude et que la fournaise était excessivement chauffée, la flamme tua les hommes qui y avaient jeté Shadrac, Méshac, et Abed-Négo.
3:23	Et ces trois hommes, Shadrac, Méshac, et Abed-Négo, tombèrent tous liés au milieu de la fournaise de feu ardent.

### La grandeur de YHWH, l'Élahh qui délivre

3:24	Alors le roi Neboukadnetsar fut effrayé et se leva précipitamment. Il répondit et dit à ses conseillers : N'avons-nous pas jeté trois hommes liés au milieu du feu ? Ils répondirent et dirent au roi : Certainement, roi !
3:25	Il répondit et dit : Voici, je vois quatre hommes sans liens qui marchent au milieu du feu, et il n'y a en eux aucun dommage, et l'apparence du quatrième est semblable à celle d'un fils d'Élahh.
3:26	Alors Neboukadnetsar s'approcha vers la porte de la fournaise de feu ardent. Il répondit et dit : Shadrac, Méshac, et Abed-Négo, serviteurs de l'Élahh Très-Haut, sortez et venez ! Alors Shadrac, Méshac, et Abed-Négo sortirent du milieu du feu.
3:27	Les satrapes, les préfets, les gouverneurs, et les conseillers du roi se rassemblèrent pour contempler ces hommes-là, et le feu n'avait eu aucun pouvoir sur leurs corps, et aucun cheveu de leur tête n'était brûlé, et leurs caleçons n'étaient pas endommagés, et l'odeur du feu n'avait pas passé sur eux.
3:28	Neboukadnetsar répondit et dit : Béni soit l'Élahh de Shadrac, Méshac, et Abed-Négo, lequel a envoyé son ange et délivré ses serviteurs qui ont eu confiance en lui, et qui ont changé<!--Vient de l'araméen « shena » qui signifie « changer, être altéré, être changé ».--> le commandement du roi et livré leur corps afin de ne servir et de n'adorer aucun autre élahh que leur Élahh<!--Mt. 4:10 ; Ac. 4:19, 5:29.-->.

### Shadrac, Méshac et Abed-Nego élevés par le roi

3:29	Voici le décret que j'ai pris : Tout homme, à quelque nation ou langue qu'il appartienne, qui parlera avec négligence<!--Vient de l'araméen « shalah » qui signifie « négligence, relâchement ».--> de l'Élahh de Shadrac, Méshac, et Abed-Négo, sera mis en pièces, et sa maison sera réduite en un tas de déchets, parce qu'il n'y a aucun autre élahh qui puisse délivrer comme lui.
3:30	Alors le roi fit réussir Shadrac, Méshac, et Abed-Négo dans la province de Babel.

### La suprématie de YHWH déclarée aux nations

3:31	Le roi Neboukadnetsar, à tous les peuples, aux nations, aux hommes de toutes langues qui habitent sur toute la Terre : Que votre paix soit multipliée !
3:32	Les signes et les merveilles que l'Élahh Très-Haut a faits pour moi, il m’a paru bon de les expliquer.
3:33	Que ses signes sont grands, et ses merveilles pleines de force ! Son règne est un règne éternel, et sa domination subsiste de génération en génération<!--Ps. 102:13 ; La. 5:19 ; Lu. 1:33.-->.

## Chapitre 4

### La vision du grand arbre

4:1	Moi, Neboukadnetsar, j'étais tranquille dans ma maison et heureux dans mon palais,
4:2	j'ai vu un rêve qui m'a fait peur. Des images mentales sur ma couche et des visions de ma tête m'effrayaient.
4:3	J'ai pris alors un décret afin qu'on fasse entrer devant moi tous les sages de Babel afin de me faire connaître l'interprétation du rêve.
4:4	Alors entrèrent les magiciens, les astrologues, les Chaldéens et les devins. J'ai raconté le rêve devant eux, mais ils ont été incapables de m'en faire connaître l'interprétation.
4:5	À la fin, s'est présenté devant moi Daniye'l, dont le nom est Beltshatsar, selon le nom de mon élahh, et qui a en lui l'esprit des élahhs saints. Je lui ai raconté le rêve :
4:6	Beltshatsar, chef des magiciens, comme je sais que l'esprit des élahhs saints est en toi, et qu'aucun secret n'est contraignant pour toi, dis-moi l'interprétation des visions que j'ai vues en rêve !
4:7	Sur ma couche, je regardais les visions de ma tête : Voici un arbre au milieu de la Terre, d'une grande hauteur.
4:8	Cet arbre était devenu grand et fort, sa cime atteignait les cieux, et on le voyait des extrémités de toute la Terre.
4:9	Son feuillage était beau, et son fruit abondant, et il portait de la nourriture pour tous. Les bêtes des champs s'abritaient sous son ombre, les oiseaux des cieux habitaient dans ses branches, et toute chair se nourrissait de lui.
4:10	Je regardais, dans les visions de ma tête, sur mon lit, et voici, un éveillé<!--Veilleur, vigilant, celui qui ne dort pas.-->, un saint descendit des cieux.
4:11	Il cria avec force et parla ainsi : Coupez l'arbre, et ébranchez-le ! Secouez son feuillage et dispersez son fruit ! Que les bêtes s'enfuient de dessous et les oiseaux du milieu de ses branches !
4:12	Toutefois le tronc avec ses racines, laissez-le en terre, avec des liens de fer et de cuivre, parmi l’herbe des champs. Qu'il soit trempé de la rosée des cieux, et qu'il ait, comme les bêtes, l'herbe de la terre pour partage.
4:13	Que son cœur d'homme soit changé, et qu'un cœur de bête lui soit donné, et que sept temps passent sur lui !
4:14	Cette parole est un décret des éveillés, cette affaire est un ordre des saints, afin que les vivants sachent que le Très-Haut domine sur le royaume des humains, qu'il le donne à qui il lui plaît, et qu'il y établit le plus vil des humains<!--Cette vérité est confirmée par Paulos (Paul) dans Ro. 13:1. C'est Elohîm qui choisit souverainement qui il établit à la tête d'un territoire. Selon les Écritures, toute autorité vient d'Elohîm.-->.
4:15	Voilà le rêve que j'ai vu, moi, le roi Neboukadnetsar. Toi Beltshatsar, dis-moi l'interprétation, puisque tous les sages de mon royaume ne peuvent me faire connaître l’interprétation. Mais toi, tu le peux, parce que l'esprit des élahhs saints est en toi.

### Interprétation de la vision

4:16	Alors Daniye'l, dont le nom est Beltshatsar, fut un moment épouvanté et ses pensées le troublaient. Le roi répondit et dit : Beltshatsar, que le rêve et son interprétation ne te troublent pas ! Beltshatsar répondit et dit : Mon seigneur, que le rêve soit pour ceux qui te haïssent, et son interprétation pour tes ennemis !
4:17	L'arbre que tu as vu, qui était devenu grand et fort, dont la cime s'élevait jusqu'aux cieux, et qu'on voyait de tous les points de la terre, 
4:18	cet arbre, dont le feuillage était beau, et les fruits abondants, qui portait de la nourriture pour tous, sous lequel s'abritaient les bêtes des champs, et parmi les branches duquel les oiseaux des cieux habitaient,
4:19	c'est toi, roi, qui es devenu grand et fort, et ta grandeur s'est accrue et s'est élevée jusqu'aux cieux, et dont la domination s'étend jusqu'aux extrémités de la terre.
4:20	Le roi a vu un éveillé, un saint, descendre des cieux et dire : Coupez l'arbre et ébranchez-le ! Toutefois le tronc avec ses racines, laissez-le en terre, mais avec des liens de fer et de cuivre, parmi l’herbe des champs ! Qu'il soit arrosé de la rosée des cieux et que son partage soit avec les bêtes des champs, jusqu'à ce que sept temps soient passés sur lui.
4:21	Telle est l'interprétation, roi ! et c’est un décret du Très-Haut qui est parvenu jusqu'à mon seigneur le roi.
4:22	On te chassera du milieu des humains, tu auras ta demeure avec les bêtes des champs, et l'on te donnera de l'herbe comme aux bœufs, et tu seras arrosé de la rosée des cieux, et sept temps passeront sur toi, jusqu'à ce que tu reconnaisses que le Très-Haut domine sur le royaume des humains, et qu'il le donne à qui il lui plaît.
4:23	Parce qu’on a commandé de laisser le tronc avec les racines de l’arbre, ton royaume sera durable pour toi quand tu auras connu que les cieux dominent.
4:24	C'est pourquoi, roi, que mon conseil te soit agréable : rachète tes péchés par la justice et tes iniquités en faisant miséricorde aux pauvres, voici, ce sera une prolongation pour ta prospérité.

### Le roi Neboukadnetsar déchu à cause de son orgueil

4:25	Toutes ces choses arrivèrent au roi Neboukadnetsar.
4:26	Au bout de douze mois, comme il se promenait dans le palais royal de Babel,
4:27	le roi répondit et dit : N'est-ce pas ici Babel la grande, que j'ai bâtie pour être la maison royale, par la puissance de mon pouvoir et pour la gloire de ma majesté ?
4:28	La parole était encore dans la bouche du roi, qu'une voix descendit des cieux, disant : Roi Neboukadnetsar, on t'annonce que ton royaume va t'être ôté.
4:29	On te chassera du milieu des humains, tu auras ta demeure avec les bêtes des champs. On te donnera de l'herbe à manger comme aux bœufs, et sept temps passeront sur toi, jusqu'à ce que tu saches que le Très-Haut domine sur le royaume des humains, et qu'il le donne à qui il lui plaît.
4:30	Au même instant, la parole s'accomplit sur Neboukadnetsar. Il fut chassé du milieu des humains, il mangea de l'herbe comme les bœufs, et son corps fut arrosé de la rosée des cieux jusqu'à ce que son poil croisse comme les plumes des aigles, et ses ongles comme ceux des oiseaux.

### Le roi Neboukadnetsar est rétabli ; il loue l'Elohîm Très-Haut

4:31	Mais à la fin de ces jours-là, moi Neboukadnetsar, je levai mes yeux vers les cieux, et la connaissance me revint. J'ai béni le Très-Haut, j'ai loué et glorifié celui qui vit éternellement, celui dont la domination est une domination éternelle, et dont le règne subsiste de génération en génération.
4:32	Tous les habitants de la Terre ne comptent pour rien devant lui. Il agit comme il lui plaît avec l'armée des cieux et avec les habitants de la Terre, et il n'y a personne qui empêche sa main, et qui lui dise : Que fais-tu<!--Es. 45:9 ; Jé. 23:18-22 ; Ps. 115:3 ; Job 9:12.--> ?
4:33	À l'instant même, la connaissance me revint, la gloire de mon royaume, ma majesté et ma splendeur me furent rendues : mes conseillers et mes nobles me cherchèrent, je fus rétabli dans mon royaume et ma grandeur fut extraordinairement augmentée.
4:34	Maintenant, moi, Neboukadnetsar, je loue, j'exalte, et je glorifie le Roi des cieux, dont toutes les œuvres sont véritables et les voies justes, et qui peut abaisser ceux qui marchent avec orgueil<!--De. 32:4 ; Es. 13:11 ; Ez. 17:24 ; Ps. 145:17.-->.

## Chapitre 5

### Les vases du temple souillés

5:1	Le roi Belshatsar donna un grand festin à ses nobles au nombre de 1 000, et en présence de ces 1 000 il buvait du vin.
5:2	Sous le goût du vin, Belshatsar ordonna qu'on apporte les vases d'or et d'argent que Neboukadnetsar, son père, avait enlevés du temple de Yeroushalaim<!--2 Ch. 36:10.-->, afin que le roi et ses nobles, ses femmes et ses concubines, s'en servent pour boire.
5:3	Alors furent apportés les vases d'or qui avaient été enlevés du temple, de la maison d'Élahh qui était à Yeroushalaim, et le roi, ses nobles, ses femmes et ses concubines s'en servirent pour boire.
5:4	Ils burent du vin et ils louèrent leurs élahhs d'or, d'argent, de cuivre, de fer, de bois et de pierre.

### L'écriture sur la muraille

5:5	À ce moment-là, sortirent les doigts d'une main humaine et ils écrivirent, en face du chandelier, sur le plâtre du mur du palais royal, et le roi voyait la paume d’une main qui écrivait.
5:6	Alors le roi changea de couleur, et ses pensées l'effrayèrent, si bien que les jointures de ses reins se desserrèrent, et ses genoux se cognèrent l'un contre l'autre.
5:7	Le roi cria avec force qu'on fasse entrer les astrologues, les Chaldéens et les devins. Le roi répondit et dit aux sages de Babel : Quiconque lira cette écriture et m'expliquera son interprétation sera revêtu de pourpre, il aura un collier d'or à son cou, et sera le troisième dans le gouvernement du royaume.
5:8	Alors tous les sages du roi entrèrent, mais ils ne furent pas capables de lire l’écriture ni de faire connaître l’interprétation au roi.
5:9	Alors le roi Belshatsar fut très effrayé, il changea de couleur, et ses nobles furent perplexes.

### Interprétation de l'écriture : division de l'Empire babylonien

5:10	La reine entra dans la maison du festin, à cause des paroles du roi et de ses nobles. La reine répondit et dit : Roi, vis éternellement ! Que tes pensées ne te troublent pas, et que ton visage ne change pas de couleur !
5:11	Il y a dans ton royaume un homme qui a en lui l'esprit des élahhs saints. Aux jours de ton père, il se trouva en lui une lumière, une intelligence et une sagesse comme la sagesse des élahhs. Le roi Neboukadnetsar, ton père, l’avait établi chef des magiciens, des astrologues, des Chaldéens et des devins – c'était le roi, ton père<!--Belshatsar était le petit-fils de Neboukadnetsar qui avait régné conjointement avec son père, Nabonide, à partir de 552 av. J.-C.--> – 
5:12	parce qu'on trouva en lui, en Daniye'l, à qui le roi lui-même donna le nom de Beltshatsar, un esprit extraordinaire, de la connaissance et de l'intelligence, pour interpréter les rêves, pour expliquer les questions obscures et résoudre les choses difficiles. Que Daniye'l soit appelé et il expliquera l'interprétation.
5:13	Alors on fit entrer Daniye'l devant le roi. Le roi répondit et dit à Daniye'l : Es-tu ce Daniye'l, l'un des fils des captifs de Yéhouda, que le roi, mon père, a amenés de Yéhouda ?
5:14	J’ai entendu dire que l'esprit des élahhs est en toi, et qu'on trouve en toi une lumière, une intelligence et une sagesse extraordinaire.
5:15	On vient de faire entrer devant moi les sages et les astrologues, afin qu'ils lisent cette écriture et m'en donnent l'interprétation, mais ils ne sont pas capables d'expliquer l'interprétation de la chose.
5:16	J’ai entendu dire aussi que tu es capable d'interpréter et de résoudre les choses difficiles. Maintenant, si tu es capable de lire cette écriture, et de m'en faire connaître l'interprétation, tu seras revêtu de pourpre et tu auras à ton cou un collier d'or, et tu seras le troisième dans le gouvernement du royaume.
5:17	Alors Daniye'l répondit et dit en présence du roi : Que tes dons soient à toi et donne tes récompenses à un autre ! Toutefois je lirai au roi l'écriture et je lui en ferai connaître l'interprétation.
5:18	Roi ! L'Élahh Très-Haut avait donné à Neboukadnetsar, ton père, le royaume, la majesté, la gloire et l'honneur.
5:19	Et à cause de la grandeur qu'il lui avait donnée, tous les peuples, les nations, et les hommes de toutes langues tremblaient de peur devant lui. Car il faisait mourir ceux qu'il voulait, et il laissait la vie à ceux qu'il voulait. Il élevait ceux qu'il voulait, et il abaissait ceux qu'il voulait.
5:20	Mais lorsque son cœur s'éleva et que son esprit s'endurcit jusqu'à l'arrogance, il fut renversé de son trône royal et dépouillé de sa gloire.
5:21	Il fut chassé du milieu des enfants d'humains, son cœur fut rendu semblable à celui des bêtes, et sa demeure fut avec les ânes sauvages. On lui donna comme aux bœufs de l'herbe à manger, et son corps fut arrosé de la rosée des cieux, jusqu'à ce qu'il reconnaisse que l'Élahh Très-Haut domine sur les royaumes des humains, et qu'il y établit ceux qu'il lui plaît.
5:22	Et toi aussi, Belshatsar, son fils, tu n'as pas humilié ton cœur, quoique tu saches toutes ces choses.
5:23	Mais tu t'es élevé contre le Seigneur des élahhs. Les vases de sa maison ont été apportés devant toi et vous vous en êtes servis pour boire du vin, toi et tes nobles, tes femmes et tes concubines. Tu as loué les élahhs d'argent, d'or, de cuivre, de fer, de bois et de pierre, qui ne voient pas, qui n'entendent pas et qui ne savent rien, et tu n'as pas glorifié l'Élahh dans la main duquel est ton souffle et toutes tes voies<!--Job 12:10, 33:4.-->.
5:24	Alors de sa part a été envoyée cette partie de main, et cette écriture a été gravée.
5:25	Voici l'écriture qui a été gravée : Mené, mené, teqel, parsîn.
5:26	Voici l'interprétation de ces paroles. Mené (Compté) : Élahh a fait les comptes de ton règne et y a mis fin.
5:27	Teqel (Pesé) : tu as été pesé dans la balance et tu as été trouvé léger.
5:28	Parsîn (Divisé) : ton royaume a été divisé et donné aux Mèdes et aux Perses.
5:29	Alors Belshatsar ordonna qu'on revête Daniye'l de pourpre, qu'on lui mette au cou un collier d'or et qu'on proclame qu'il deviendrait le troisième gouvernant dans le royaume.
5:30	Cette même nuit, Belshatsar, roi des Chaldéens, fut tué.

## Chapitre 6

### Règne de Darius, le Mède

6:1	Darius, le Mède, reçut le royaume, étant fils de 62 ans<!--Es. 13:17, 21:2 ; Jé. 51:11.-->.
6:2	Or Darius trouva bon d'établir sur le royaume 120 satrapes, qui devaient être répartis dans tout le royaume.
6:3	Il mit à leur tête trois chefs, au nombre desquels était Daniye'l, afin que ces satrapes leur rendent compte, et que le roi ne souffre aucun préjudice.
6:4	Mais Daniye'l surpassait les autres chefs et satrapes, parce qu'il y avait en lui un esprit extraordinaire et le roi pensait à l'établir sur tout le royaume.

### Daniye'l refuse l'idolâtrie et persévère dans la prière

6:5	Alors les chefs et les satrapes cherchèrent à trouver une occasion contre Daniye'l au sujet des affaires du royaume. Mais ils ne purent trouver en lui aucune occasion, ni corruption, parce qu'il était digne de confiance, et qu'on ne trouvait en lui ni négligence, ni corruption.
6:6	Alors ces hommes dirent : Puisque nous n’avons trouvé aucune occasion contre ce Daniye'l, nous en trouverons donc contre lui dans la loi de son Élahh.
6:7	Alors ces chefs et les satrapes se rendirent tumultueusement auprès du roi et lui parlèrent ainsi : Roi Darius, vis éternellement !
6:8	Tous les chefs de ton royaume, les préfets, les satrapes, les conseillers et les gouverneurs ont tenu conseil ensemble pour établir un statut royal et un décret de restriction ferme : tout être humain qui, dans l'espace de 30 jours, adressera une requête à quelque élahh ou à quelque être humain, excepté à toi, roi, sera jeté dans la fosse aux lions.
6:9	Maintenant, roi, établis le décret de restriction et signe l'écrit afin qu'il soit irrévocable, selon le décret des Mèdes et des Perses qui est immuable.
6:10	À cause de cela, le roi Darius signa l'écrit et le décret de restriction.
6:11	Lorsque Daniye'l sut que l'écrit était signé, il entra dans sa maison, où les fenêtres de sa chambre haute étaient ouvertes dans la direction de Yeroushalaim. Trois fois par jour il se mettait à genoux, priait et rendait grâce devant son Élahh, comme il le faisait auparavant<!--1 R. 8:44 ; Ps. 55:17-18.-->.
6:12	Alors ces hommes entrèrent tumultueusement, et ils trouvèrent Daniye'l qui priait et implorait une faveur devant son Élahh.
6:13	Ils s'approchèrent et dirent en face du roi, au sujet du décret de restriction du roi : N'as-tu pas signé un décret de restriction portant que tout être humain qui, dans l'espace de 30 jours, adresserait une requête à quelque élahh ou à quelque être humain, excepté à toi, roi, serait jeté dans la fosse aux lions ? Le roi répondit et dit : La chose est certaine, selon la loi des Mèdes et des Perses, qui est irrévocable.
6:14	Alors ils répondirent et dirent en face du roi : Daniye'l, qui est d’entre les fils des captifs de Yéhouda, n'a tenu aucun compte de toi, roi, ni du décret de restriction que tu as signé, et il fait sa prière trois fois par jour.
6:15	Alors le roi, quand il entendit cette parole, fut extrêmement affligé. Il prit à cœur de délivrer Daniye'l, et jusqu'au coucher du soleil il s'efforça de le sauver.
6:16	Alors ces hommes entrèrent tumultueusement auprès du roi, et lui dirent : Sache roi que la loi des Mèdes et des Perses exige que tout décret de restriction ou tout statut établi par le roi soit irrévocable.

### Daniye'l demeure fidèle à Élahh face à la mort

6:17	Alors le roi ordonna qu'on amène Daniye'l, et qu'on le jette dans la fosse aux lions. Le roi répondit et dit à Daniye'l : Ton Élahh que tu sers constamment sera celui qui te délivrera.
6:18	On apporta une pierre, et on la mit sur l'ouverture de la fosse. Le roi la scella de son anneau et de l'anneau de ses nobles afin que rien ne soit changé à l'égard de Daniye'l.

### YHWH fait justice à Daniye'l

6:19	Alors le roi s’en alla dans son palais et passa la nuit en jeûnant. Il ne fit pas entrer des danseuses<!--Le mot « danseuse » vient de l'araméen « dachavah » qui signifie « divertissement », « instrument de musique », « danseuse », « concubine », « musique ».--> devant lui et le sommeil s’enfuit loin de lui.
6:20	Alors le roi se leva au petit matin, dès la lumière du jour, et il alla en hâte à la fosse aux lions.
6:21	En s'approchant de la fosse, il cria d'une voix triste : Daniye'l ! Le roi répondit et dit à Daniye'l : Daniye'l, serviteur de l'Élahh vivant, ton Élahh, que tu sers constamment, a-t-il pu te délivrer des lions ?
6:22	Alors Daniye'l parla avec le roi : Roi, vis éternellement !
6:23	Mon Élahh a envoyé son ange et a fermé la gueule des lions, et ils ne m’ont pas détruit, parce que devant lui l’innocence s’est trouvée en moi. Devant toi non plus, roi, je n'ai commis aucun crime.
6:24	Alors le roi fut extrêmement heureux pour lui et il ordonna qu'on fasse remonter Daniye'l de la fosse. Ainsi Daniye'l fut remonté de la fosse, et on ne trouva sur lui aucune blessure, parce qu'il avait cru en son Élahh.
6:25	Le roi ordonna que ces hommes qui avaient accusé Daniye'l, soient amenés et jetés dans la fosse aux lions, eux, leurs enfants et leurs femmes, et avant qu'ils soient parvenus au fond de la fosse, les lions se saisirent d'eux, et leur brisèrent tous les os.

### Les merveilles de YHWH proclamées aux nations

6:26	Après cela, le roi Darius écrivit à tous les peuples, à toutes les nations, aux hommes de toutes les langues, qui habitent sur toute la Terre : Que votre paix soit multipliée !
6:27	Voici le décret que j'établis : Dans toute l'étendue de mon royaume, on doit trembler et craindre devant l'Élahh de Daniye'l. Car il est l'Élahh vivant et il subsiste éternellement. Son Royaume ne sera jamais détruit, et sa domination durera jusqu'à la fin<!--Lu. 1:33.-->.
6:28	Il sauve et délivre, il fait des prodiges et des merveilles dans les cieux et sur la Terre, et il a délivré Daniye'l de la main des lions.
6:29	Ainsi Daniye'l prospéra sous le règne de Darius, et sous le règne de Cyrus, roi de Perse.

## Chapitre 7

### Rêve des quatre bêtes

7:1	En l'an un de Belshatsar, roi de Babel, Daniye'l eut un rêve et des visions de sa tête, étant sur sa couche. Alors il écrivit le rêve et il relata les principales choses.
7:2	Daniye'l répondit et dit : Je regardais dans ma vision nocturne, et voici, les quatre vents des cieux se levèrent avec impétuosité sur la grande mer.
7:3	Puis quatre grandes bêtes<!--Le rêve de Daniye'l fait écho au rêve de la statue qu'a eu Neboukadnetsar en Da.2 (voir annexe « la statue de Neboukadnetsar »). Chaque bête représente un empire historique :\\- Le lion ailé pour l'Empire néo-babylonien (625 – 539 av. J.-C.) qui s'est démarqué par la rapidité de ses conquêtes (Jé. 4:13 ; Ha. 1:6-8).\\- L'ours pour l'Empire Médo-Perse (539 – 331 av. J.-C.). Les trois côtes dans la gueule de l'animal représentent respectivement la conquête de la Lydie (546 av. J.-C.), de la Babylonie (539 av. J.-C.) et de l'Égypte (524 av. J.-C.).\\- Le léopard ailé pour l'Empire Gréco-Macédonien (331 – 146 av. J.-C.), qui illustre bien la rapidité des conquêtes de cet Empire à l'ascension fulgurante.\\- La quatrième bête pour l'Empire romain (146 av. J.-C. – 476 ap. J.-C.) qui s'est illustré par l'immensité de son territoire, sa longévité et son caractère violent. Aujourd'hui, l'Empire romain n'existe plus officiellement. Il a toutefois été remplacé par l'Union européenne, son héritière géographique, culturelle et spirituelle qui, selon les Écritures, établira un gouvernement mondial. En effet, ce quatrième animal est l'équivalent de la bête qu'a vue Yohanan en Ap. 13. La particularité de cette créature c'est qu'elle a eu une blessure mortelle (la fin de l'Empire Romain en 476 ap. J.-C.) qui a été guérie (restauration de l'Empire romain sous une autre forme).--> montèrent de la mer, différentes les unes des autres.

### Premier empire : Babel (Babylone)<!--Cp. Da. 2:38.-->

7:4	La première était semblable à un lion, et avait des ailes d'aigle. Je la regardai jusqu'à ce que ses ailes furent arrachées. Elle fut soulevée de terre et mise debout sur ses pieds comme un être humain, et un cœur d'être humain lui fut donné.

### Deuxième empire : la Médie et la Perse<!--Cp. Da. 2:39, 8:20.-->

7:5	Et voici une autre bête, une seconde, semblable à un ours. Elle se tenait sur un côté, ayant trois côtes dans la gueule entre ses dents. On lui disait ainsi : Lève-toi, mange beaucoup de chair !

### Troisième empire : la Grèce<!--Cp. Da. 2:39, 8:21-22, 11:2-4.-->

7:6	Après cela je regardais, et voici une autre bête, semblable à un léopard. Elle avait sur son dos quatre ailes d'oiseau, et cette bête avait quatre têtes<!--À la mort d'Alexandre le Grand (356 – 323 av. J.-C.) son royaume fut partagé par quatre de ses principaux généraux : l'Asie Mineure pour Lysimaque (361 – 281 av. J.-C.), la Grèce et la Macédoine pour Cassandre (358 – 297 av. J.-C.), la Syrie et la Babylonie pour Séleucos Ier Nicator (358 – 281 av. J.-C.), l'Égypte, la Judée et une partie de la Syrie pour Ptolémée Ier Sôter (368 – 283 av. J.-C.). Voir Da. 8:8 et 11:4.-->, et la domination lui fut donnée.

### Quatrième empire : Rome<!--Cp. Da. 2:40-43, 7:23-24, 9:26.-->

7:7	Après cela, je regardais dans mes visions nocturnes, et voici, il y avait une quatrième bête, effrayante, terrible et extraordinairement forte. Elle avait de grandes dents en fer. Elle mangeait, brisait et foulait aux pieds ce qui restait. Elle était différente de toutes les bêtes précédentes et avait dix cornes.

### Les dix cornes et la petite corne<!--Da. 7:24-27.-->

7:8	Comme je considérais les cornes, voici, une autre petite corne sortit du milieu d'elles, et trois des premières cornes furent arrachées par elle. Et sur cette corne, il y avait des yeux comme des yeux d'être humain, et une bouche qui disait de grandes choses.

### Le règne de YHWH, l'Ancien des jours<!--Cp. Mt. 24:27-30 ; Ap. 19:11-21.-->

7:9	Je regardais jusqu'à ce que les trônes soient placés. Et l'Ancien des jours s'assit. Son vêtement était blanc comme la neige, et les cheveux de sa tête étaient comme de la laine pure<!--Voir Ap. 1:14.-->. Son trône était des flammes de feu, et ses roues un feu ardent.
7:10	Un fleuve de feu coulait et sortait de devant ses faces. Mille milliers le servaient et des myriades de myriades se tenaient debout en face de lui. Le jugement se tint, et les livres furent ouverts<!--1 R. 22:19 ; Ps. 68:18 ; Ap. 5:11.-->.
7:11	Je regardais alors, à cause du son des grandes choses que disait la corne. Pendant que je regardais, la bête fut tuée, et son corps fut détruit et livré pour être brûlé au feu.
7:12	Les autres bêtes furent dépouillées de leur domination, mais une prolongation de vie leur fut accordée jusqu'à un temps fixé et un temps.

### La domination du Fils d'humain est éternelle<!--Cp. Ap. 5:1-14.-->

7:13	Je regardais dans ces visions de la nuit, et voici qu'avec les nuées des cieux venait comme un Fils d'humain. Il arriva jusqu'à l'Ancien des jours, et on le fit approcher de lui<!--Jud. 1:15 ; Ap. 19:14.-->.
7:14	Et il lui donna la domination, la gloire et le règne. Tous les peuples, les nations et les langues le serviront. Sa domination est une domination éternelle qui ne passera pas, et son règne ne sera jamais détruit.

### Interprétation de la vision du quatrième animal

7:15	Mon esprit, à moi, Daniye'l, fut affligé dans l’intérieur de mon corps et les visions de ma tête m'effrayèrent.
7:16	Je m'approchai d'un de ceux qui se tenaient là, et je lui demandai la vérité au sujet de tout cela. Il me parla et me fit connaître l'interprétation de ces choses :
7:17	Ces quatre grandes bêtes sont quatre rois, qui s'élèveront de la Terre.
7:18	Mais les saints d'Élyown<!--Terme araméen, qui signifie « le Très-Haut, le Plus Haut », correspondant à l'hébreu « 'elyown » (Élyon) ; cp. Ps. 7:18.--> recevront le Royaume, et ils posséderont le Royaume éternellement, d'éternité en éternité.
7:19	Alors je désirai connaître la vérité sur la quatrième bête, qui était différente de toutes les autres, extraordinairement effrayante, qui avait des dents en fer et des ongles en cuivre, qui mangeait, brisait et foulait aux pieds ce qui restait,
7:20	et sur les dix cornes qui étaient sur sa tête, et sur l’autre qui était sortie et devant laquelle trois étaient tombées, sur cette corne qui avait des yeux, une bouche qui disait de grandes choses et une plus grande apparence que ses associées.
7:21	Je regardais comment cette corne faisait la guerre aux saints et l'emportait sur eux<!--Ap. 13:2-7.-->,
7:22	jusqu’à ce que vienne l'Ancien des jours et que le jugement soit donné aux saints d'Élyown, que le temps arrive et que les saints possèdent le Royaume.
7:23	Il parla ainsi : La quatrième bête est un quatrième royaume qui sera sur la Terre, différent de tous les royaumes, et qui dévorera toute la Terre, la foulera et la brisera.

### Règne de l'homme impie et jugement d'Elohîm

7:24	Mais les dix cornes sont dix rois qui s'élèveront de ce royaume. Un autre s'élèvera après eux, il sera différent des premiers, et il abattra trois rois.
7:25	Il dira des paroles contre le Très-Haut, il harcèlera les saints d'Élyown, il aura l'intention de changer les temps et la loi, et ils seront livrés entre ses mains pendant un temps, des temps, et la moitié d'un temps<!--Ap. 11:2, 12:13-17.-->.
7:26	Mais le jugement s’assiéra, et on ôtera sa domination, pour la détruire et la faire périr jusqu’à la fin.
7:27	Le règne, la domination et la grandeur de tous les royaumes qui sont sous les cieux seront donnés au peuple des saints d'Élyown. Son Royaume est un royaume éternel, et toutes les dominations les serviront et leur obéiront.
7:28	Jusqu'ici est la fin de la parole. Moi, Daniye'l, mes pensées m'effrayèrent beaucoup, c'est pourquoi je changeai de couleur, mais je gardai la parole dans mon cœur.

## Chapitre 8

### Vision du bélier et du bouc

8:1	La troisième année du règne du roi Belshatsar, moi, Daniye'l, j'ai vu une vision, après celle que j'avais vue auparavant.
8:2	Je vis cette vision, et il arriva, quand je la vis, que j’étais à Suse, la capitale, dans la province d'Éylam. Je vis dans cette vision que j'étais près du fleuve Oulaï.
8:3	Je levai mes yeux, je regardai, et voici, un bélier se tenait debout en face du fleuve et il avait deux cornes. Les deux cornes étaient hautes, mais l'une était plus haute que l'autre, et la plus haute s'éleva la dernière.
8:4	Je vis ce bélier heurtant vers l'ouest, vers le nord, et vers le midi. Aucune bête n'était capable de tenir debout en face de lui, et il n'y avait personne qui puisse délivrer de sa main. Il agissait selon sa volonté et devint grand.
8:5	Moi, j’étais en train de regarder, et voici, un bouc d'entre les chèvres venait de l'occident sur les faces de toute la terre, sans toucher la terre. Ce bouc avait entre les yeux une corne considérable.
8:6	Il arriva jusqu'au bélier qui avait deux cornes et que j'avais vu se tenant debout en face du fleuve, et il courut sur lui dans la fureur de sa force.
8:7	Je le vis arriver à proximité du bélier et manifester son amertume contre lui : il frappa le bélier et lui brisa les deux cornes, et le bélier n'avait aucune force pour se tenir debout face à lui. Il le jeta par terre et le piétina, et il n'y eut personne pour délivrer le bélier de sa main.
8:8	Alors le bouc d'entre les chèvres grandit extrêmement, mais lorsqu'il fut puissant, sa grande corne se brisa. Quatre cornes<!--À la mort d'Alexandre le Grand (356 – 323 av. J.-C.) son royaume fut partagé par quatre de ses principaux généraux : l'Asie Mineure pour Lysimaque (361 – 281 av. J.-C.), la Grèce et la Macédoine pour Cassandre (358 – 297 av. J.-C.), la Syrie et la Babylonie pour Séleucos Ier Nicator (358 – 281 av. J.-C.), l'Égypte, la Judée et une partie de la Syrie pour Ptolémée Ier Sôter (368 – 283 av. J.-C.). Voir Da. 7:6 et 11:4.--> considérables s'élevèrent pour la remplacer, aux quatre vents des cieux.

### La petite corne renverse la vérité

8:9	De l'une d'elles sortit une petite corne<!--Antiochos IV Épiphane est le fils d'Antiochos III le Grand, né vers 215 av. J.-C. Il gouverna le royaume séleucide de 175 à 164 av. J.-C., date de sa mort. Ce dernier avait profané le temple de Yeroushalaim (Jérusalem) en sacrifiant des porcs sur l'autel (Voir commentaire en Mt. 24:15). Cette petite corne, qui fait tomber par terre une partie de l'armée des étoiles, agit de même que Satan au ciel qui avait fait chuter un tiers des étoiles, soit des anges (Ap. 12:3-4).-->, qui s'agrandit beaucoup vers le midi, vers l'est et vers la noblesse<!--Vient d'un mot qui signifie aussi « gloire, honneur, chevreuil, gazelle ».-->.
8:10	Elle s'éleva même jusqu'à l'armée des cieux, elle fit tomber à terre une partie de l'armée et des étoiles, et elle les piétina<!--Es. 14:12-15 ; Ez. 28:12-19.-->.
8:11	Et elle s'éleva même jusqu'au chef de l'armée, lui enleva le sacrifice perpétuel, et renversa la demeure de son sanctuaire.
8:12	L'armée fut livrée avec le sacrifice perpétuel, à cause de la transgression. La corne jeta la vérité par terre, elle agit et réussit.
8:13	J'entendis un saint qui parlait, et un autre saint dit à celui qui parlait : Jusqu'à quand cette vision du sacrifice perpétuel, de la transgression qui cause la désolation, du sanctuaire et de l'armée foulés aux pieds ?
8:14	Il me dit : Jusqu’à 2 300 soirs et matins, et le sanctuaire sera purifié.

### La vision du bélier et du bouc interprétée

8:15	Et il arriva que, lorsque moi, Daniye'l, j’eus vu la vision, j’en cherchai le discernement, et voici, quelqu'un qui avait l'apparence d'un homme fort se tenait debout devant moi.
8:16	J'entendis la voix d'un être humain au milieu de l'Oulaï qui appelait et disait : Gabriy'el, discerne pour celui-ci la vision !
8:17	Il vint près du lieu où je me tenais, et quand il vint, je fus terrifié et je tombai sur mes faces. Il me dit : Comprends, fils d'humain, car la vision est pour le temps de la fin.
8:18	Comme il parlait avec moi, je m'endormis profondément, les faces contre terre. Il me toucha, et me fit tenir debout à la place où je me tenais.
8:19	Il dit : Voici, je te fais connaître ce qui arrivera à la fin de la colère, car il y a un temps fixé<!--Ge. 1:14.--> pour la fin.
8:20	Le bélier que tu as vu qui avait deux cornes, ce sont les rois des Mèdes et des Perses.
8:21	Le bouc velu, c'est le roi de Yavan<!--Yavan ou Grèce.-->. La grande corne entre ses yeux, c'est le premier roi.
8:22	La brisée, à la place de laquelle les quatre se sont élevées, ce sont quatre royaumes. Elles s'élèveront de cette nation, mais qui n'auront pas autant de force.

### Le roi impie, l'adversaire d'Elohîm ; la vision tenue secrète

8:23	À la fin de leur règne, quand les rebelles seront au complet, il s’élèvera un roi aux faces féroces et qui comprendra les énigmes.
8:24	Sa puissance s'accroîtra, mais non par sa propre force. Il accomplira d'extraordinaires destructions, il prospérera en agissant, il détruira les puissants et le peuple des saints.
8:25	Par son habileté, il fera prospérer la tromperie dans sa main. Il s'enorgueillira dans son cœur et, dans la tranquillité, il détruira une multitude. Il s'élèvera contre le Prince des princes, mais il sera brisé sans main.
8:26	Quant à la vision des soirs et des matins telle qu'elle a été dite, c'est la vérité. Mais toi, tiens secrète la vision, car elle s'accomplira dans beaucoup de jours.
8:27	Moi, Daniye'l, je fus affaibli et malade pendant des jours. Puis je me levai, et je m'occupai des affaires du roi. J'étais étonné de la vision, et personne n'en eut connaissance.

## Chapitre 9

### Supplications de Daniye'l à YHWH

9:1	En l’an un de Darius, fils d'Assuérus, de la postérité des Mèdes, lequel était devenu roi du royaume des Chaldéens,
9:2	en l'an un de son règne, moi, Daniye'l, je discernai par les livres le nombre d'années qui devait s'accomplir sur les désolations de Yeroushalaim, d'après la parole de YHWH qui était venue à Yirmeyah<!--Jé. 25:11.-->, le prophète : 70 ans.
9:3	Je tournai mes faces vers Adonaï Elohîm pour le chercher par la prière et la supplication, avec le jeûne, le sac et la cendre.
9:4	Je priai YHWH, mon Elohîm, et je fis ma confession : Oh ! Je te prie, Adonaï, El Gadowl<!--El Grand.--> et redoutable, toi qui gardes ton alliance et qui fais miséricorde à ceux qui t'aiment et qui gardent tes commandements !
9:5	Nous avons péché, nous avons commis l'iniquité, nous avons agi méchamment, nous avons été rebelles, et nous nous sommes détournés de tes commandements et de tes ordonnances.
9:6	Nous n'avons pas écouté tes serviteurs, les prophètes, qui ont parlé en ton Nom à nos rois, à nos chefs, à nos pères, et à tout le peuple de la terre.
9:7	Adonaï ! À toi est la justice, et à nous la confusion de face, en ce jour, aux hommes de Yéhouda, aux habitants de Yeroushalaim, et à tout Israël, à ceux qui sont près et à ceux qui sont loin, en toutes les terres où tu les as bannis, à cause des délits, des transgressions qu'ils ont commises contre toi<!--Ps. 106:6 ; La. 3:42 ; Né. 9:30.-->.
9:8	Adonaï, à nous est la confusion de face, à nos rois, à nos chefs, et à nos pères, parce que nous avons péché contre toi.
9:9	Auprès d'Adonaï, notre Elohîm, la miséricorde et le pardon, car nous avons été rebelles envers lui.
9:10	Nous n'avons pas écouté la voix de YHWH, notre Elohîm, pour marcher dans sa torah, qu'il avait mise devant nous par la main de ses serviteurs, les prophètes.
9:11	Tout Israël a transgressé ta torah et s'est détourné pour ne pas écouter ta voix. Alors se sont répandus sur nous la malédiction et le serment qui sont écrits dans la torah de Moshé, serviteur d'Elohîm, parce que nous avons péché contre lui<!--Lé. 26:14-39 ; Né. 1:6.-->.
9:12	Il a accompli les paroles qu'il avait prononcées contre nous, et contre nos chefs qui nous ont gouvernés, et il a fait venir sur nous un grand mal, et il n'en est jamais arrivé sous tous les cieux un semblable à celui qui est arrivé à Yeroushalaim.
9:13	Comme cela est écrit dans la torah de Moshé, ce mal est venu sur nous, et nous n'avons pas supplié les faces de YHWH, notre Elohîm, pour nous détourner de nos iniquités, et pour nous rendre attentifs à ta vérité.
9:14	YHWH a veillé sur le mal, et l'a fait venir sur nous, car YHWH, notre Elohîm, est juste dans toutes les œuvres qu'il a faites, mais nous n'avons pas obéi à sa voix.
9:15	Or maintenant, Adonaï, notre Elohîm ! Toi qui as fait sortir ton peuple de la terre d'Égypte par ta main puissante, et qui t'es acquis un Nom comme il l'est aujourd'hui, nous avons péché, nous avons été méchants.
9:16	Adonaï, s'il te plaît, selon ta justice, que ta colère et ton indignation se détournent de ta ville de Yeroushalaim, de la montagne de ta sainteté ! Car à cause de nos péchés et des iniquités de nos pères, Yeroushalaim et ton peuple sont en opprobre à tous ceux qui nous entourent.
9:17	Maintenant, notre Elohîm, écoute la prière et les supplications de ton serviteur, et pour l'amour d'Adonaï, fais briller tes faces sur ton sanctuaire dévasté !
9:18	Mon Elohîm, prête l'oreille et écoute ! Ouvre tes yeux et regarde nos ruines, et la ville sur laquelle ton Nom est invoqué ! Car ce n'est pas à cause de nos justices que nous présentons nos supplications devant tes faces, mais à cause de tes grandes compassions.
9:19	Adonaï entends ! Adonaï pardonne ! Adonaï sois attentif et agis ! Ne tarde pas, par amour pour toi, mon Elohîm ! Car ton Nom est invoqué sur ta ville, et sur ton peuple.

### Les 70 semaines

9:20	Je parlais encore, je priais, je confessais mon péché, et le péché de mon peuple d'Israël, et je présentais ma supplication à YHWH, mon Elohîm, en faveur de la sainte montagne de mon Elohîm.
9:21	Je parlais encore en prière, quand l'homme Gabriy'el, que j'avais vu auparavant dans une vision, étant épuisé de fatigue, s'approcha de moi au temps de l'offrande du soir.
9:22	Il me fit comprendre, et parla avec moi et dit : Daniye'l, je suis venu maintenant pour que tu prospères en discernement.
9:23	La parole est sortie dès le commencement de tes supplications, et je suis venu pour te la déclarer, car tu es précieux. Sois attentif à la parole, et comprends la vision.
9:24	70 semaines<!--Il convient tout d'abord de préciser que cette prophétie concerne en premier lieu Israël en tant que nation. Daniye'l annonce une sorte de compte à rebours sur les événements qui interviendront depuis son époque jusqu'au jugement dernier. Une semaine représente sept années prophétiques. Ainsi, les 70 semaines correspondent à 490 ans. L'ange Gabriy'el a indiqué à Daniye'l que le départ de la 70ème semaine aura lieu lorsque Yeroushalaim (Jérusalem) sera rebâtie (Da. 9:25). Or les Écritures ne citent qu'un seul décret relatif à la reconstruction de Yeroushalaim, à savoir celui du roi Artaxerxès qui, en 445 av. J.-C., au mois de Nisan, permit à Ezra (Esdras) de retourner à Yeroushalaim pour terminer sa reconstruction (Esd. 7:6-10, 9:9 ; Né. 2:5). Cette date est considérée comme le point de départ des 69 semaines qui vont jusqu'au Mashiah (Christ). Daniye'l annonce deux événements principaux qui doivent survenir. D'une part, le retranchement du Mashiah, c'est-à-dire la mort de Yéhoshoua ha Mashiah (Jésus-Christ) à la croix pour expier nos péchés (Da. 9:26) ; et d'autre part, une nouvelle destruction de la ville et de son sanctuaire (Da. 9:26). Cette prophétie s'est accomplie en l'an 70 avec l'entrée des troupes romaines menées par Titus dans Yeroushalaim. Il reste donc une semaine, c'est-à-dire sept années prophétiques que l'ange appelle « un temps, des temps », c'est-à-dire sept ans ; « et la moitié d'un temps », c'est-à-dire trois ans et demi (Da. 7:25). La dernière partie de ces sept années, à savoir trois ans et demi, 42 mois ou encore 1 260 jours, correspond à la grande tribulation (Mt. 24:21-29 ; Mc. 13:24 ; Ap. 7:9-14, 11:1-3, 12:14, 13:5). Voir commentaire sur la grande tribulation en Ap. 7:14.--> ont été déterminées sur ton peuple et sur la ville de ton sanctuaire, pour abolir la transgression et mettre fin aux péchés, pour faire la propitiation pour l'iniquité, pour faire venir la justice éternelle, pour sceller la vision et le prophète et pour oindre le Saint des saints.
9:25	Tu sauras et comprendras que depuis la sortie de la parole pour restaurer et rebâtir Yeroushalaim jusqu'au Mashiah, le Chef, il y a 7 semaines et 62 semaines. Les places et les brèches seront rebâties, mais en des temps d'angoisse.
9:26	Après ces 62 semaines, le Mashiah sera retranché et n'aura rien. Le peuple du chef qui viendra détruira la ville et le sanctuaire, et sa fin arrivera comme par une inondation. Il est déterminé que les dévastations dureront jusqu'à la fin de la guerre.
9:27	Il confirmera l'alliance avec beaucoup pour une semaine, et à la moitié de cette semaine il fera cesser le sacrifice et l'offrande. Sous l'aile<!--Le mot hébreu signifie aussi « extrémité, bord, bordure, coin ». Il est peut-être question d'une extrémité du temple.--> sera l'abomination de la désolation<!--Voir Mt. 24:15 ; Mc. 13:14.--> jusqu’à ce que, l’achèvement décrété, la désolation se déverse.

## Chapitre 10

### Daniye'l voit la gloire du Mashiah

10:1	La troisième année de Cyrus, roi de Perse, une parole fut révélée à Daniye'l qui est appelé du nom de Beltshatsar. Cette parole est véritable et annonce une grande guerre. Il fut attentif à cette parole, et il eut le discernement de la vision.
10:2	En ce temps-là, moi Daniye'l, je fus dans les pleurs pendant trois semaines de jours.
10:3	Je ne mangeai aucune nourriture désirable, il n'entra ni viande ni vin dans ma bouche, et je ne m'oignis pas, je ne m'oignis pas jusqu'à ce que ces trois semaines de jours soient accomplies.
10:4	Le vingt-quatrième jour du premier mois, j'étais au bord du grand fleuve qui est Hiddéqel.
10:5	Je levai les yeux, et je regardai, et voici, il y avait un homme vêtu de lin, et ayant sur les reins une ceinture d'or fin d'Ouphaz.
10:6	Son corps était comme de chrysolithe, et son visage brillait comme l'éclair, ses yeux étaient comme des torches de feu, ses bras et ses pieds ressemblaient à du cuivre poli, et le son de sa voix était comme le bruit d'une multitude<!--Ap. 1:12-15.-->.
10:7	Moi, Daniye'l, je vis seul la vision, et les hommes qui étaient avec moi ne virent pas la vision, mais une grande terreur tomba sur eux et ils s'enfuirent pour se cacher. 
10:8	Je restai seul et je vis cette grande vision. Il ne me resta aucune force, ma splendeur se transforma en ruine et je ne conservai aucune force.
10:9	J'entendis le son de ses paroles et, en entendant le son de ses paroles, je m'endormis profondément sur mes faces, ayant mes faces contre terre.

### Le combat du monde spirituel

10:10	Et voici, une main me toucha, et secoua mes genoux et les paumes de mes mains.
10:11	Il me dit : Daniye'l, homme précieux d'Elohîm, sois attentif aux paroles que je vais te dire, et tiens-toi debout à la place où tu es ! Car je suis maintenant envoyé vers toi. Lorsqu'il m'eut ainsi parlé, je me tins debout en tremblant.
10:12	Il me dit : Ne crains rien, Daniye'l, car dès le premier jour où tu as appliqué ton cœur à comprendre, et à t'humilier devant ton Elohîm, tes paroles ont été entendues, et c'est à cause de tes paroles que je viens.
10:13	Mais le chef du royaume de Perse s’est tenu debout en face de moi 21 jours. Mais voici, Miyka'el, l’un des premiers chefs, est venu à mon secours et je suis resté là, près des rois de Perse.
10:14	Je viens maintenant pour te faire comprendre ce qui arrivera à ton peuple dans les derniers jours, car la vision s'étend jusqu'à ces jours-là.
10:15	Pendant qu'il parlait avec moi selon ces paroles, je mis mes faces contre terre, et je devins muet.
10:16	Et voici, comme la ressemblance des fils des humains toucha mes lèvres. J'ouvris la bouche, je parlai, et je dis à celui qui se tenait debout en face de moi : Mon seigneur, à cause de la vision, des angoisses m'ont renversé et je ne détiens plus de force.
10:17	Comment le serviteur de mon seigneur, que voici, pourrait-il parler avec mon seigneur, que voici ? Maintenant, il ne subsiste en moi aucune force et il ne reste plus de souffle en moi.
10:18	De nouveau l’apparence humaine me toucha et me fortifia.
10:19	Il dit : Ne crains rien, homme précieux, shalôm à toi ! Fortifie-toi, fortifie-toi ! Comme il parlait avec moi, je repris des forces, et je dis : Que mon seigneur parle, car tu m'as fortifié.
10:20	Il dit : Ne sais-tu pas pourquoi je suis venu vers toi ? Maintenant, je retourne combattre le chef de Perse et, quand je partirai, voici, le chef de Yavan viendra.
10:21	Mais je vais te faire connaître ce qui est écrit dans le livre de vérité. Car il n'y en a pas un qui tient ferme avec moi dans ces choses, excepté Miyka'el, votre chef.

## Chapitre 11

### Succession des monarques jusqu'à l'homme impie<!--Da. 11-12.-->

11:1	Et moi, en l'an un de Darius, le Mède, je me tenais auprès de lui pour lui donner force et protection.
11:2	Maintenant, je vais te faire connaître la vérité : voici, il y aura encore trois rois en Perse. Le quatrième s’enrichira d’une richesse plus grande que tous et, quand il sera devenu fort par sa richesse, il excitera tout contre le royaume de Yavan.
11:3	Mais il s'élèvera un vaillant roi<!--Ce vaillant roi est Alexandre le Grand qui régna de 336 à 323 av. J.-C.-->, qui dominera avec une grande domination et fera ce qu'il voudra.
11:4	Dès qu'il se sera élevé, son royaume sera brisé et divisé<!--Le prophète fait ici allusion à la mort d'Alexandre le Grand (356 – 323 av. J.-C.) et au partage de son royaume entre quatre de ses principaux généraux : l'Asie Mineure pour Lysimaque (361 – 281 av. J.-C.), la Grèce et la Macédoine pour Cassandre (358 – 297 av. J.-C.), la Syrie et la Babylonie pour Séleucos Ier Nicator (358 – 281 av. J.-C.), l'Égypte, la Judée et une partie de la Syrie pour Ptolémée Ier Sôter (368 – 283 av. J.-C.). Voir Da. 7:6 et 8:8.--> aux quatre vents des cieux, mais non pas pour sa postérité et non pas selon sa domination avec laquelle il avait gouverné, car son royaume sera déraciné et il passera à d'autres qu'à eux.
11:5	Le roi du midi<!--Le roi du midi est Ptolémée Ier Sôter (règne : 305 – 283 av. J.-C.), le chef qui deviendra plus fort que lui est Séleucos Ier Nicator (règne : 305 – 281 av. J.-C.).--> deviendra fort, mais l'un de ses chefs<!--Ce chef est Séleucos Ier Nicator (règne : 305 – 281 av. J.-C.).--> deviendra plus fort que lui et dominera. Sa domination sera une grande domination.
11:6	À la fin des années, ils s'uniront. La fille du roi du midi viendra vers le roi du nord pour agir avec équité. Mais elle ne détiendra pas de force en son bras, et son bras ne tiendra pas. Elle sera livrée, elle et ceux qui l’avaient fait venir, celui qui l’a engendrée et celui qui la fortifiait en ce temps-là.
11:7	Mais un rejeton de ses racines se tiendra à sa place<!--Ptolémée III Évergète (règne : 246 – 222 av. J.-C.).-->. Il viendra vers l'armée et entrera dans les forteresses du roi du nord. Il agira contre eux et deviendra fort.
11:8	Même leurs elohîm, avec leurs images en métal fondu et leurs objets précieux en argent et en or, il les emmènera en captivité en Égypte. Puis il se tiendra debout quelques années de plus que le roi du nord.
11:9	Celui-ci viendra dans le royaume du roi du midi et il retournera vers son sol.
11:10	Ses fils<!--Ses fils sont les deux rois de Syrie : Séleucos III Sôter Keraunos (Ceraunus) (règne : 226 – 223 av. J.-C.) et Antiochos III le Grand (règne : 223 – 187 av. J.-C.).--> entreront en guerre et rassembleront une multitude de nombreuses armées. Il viendra, il viendra, il submergera et passera outre. Il reviendra et fera la guerre jusqu'à la forteresse.
11:11	Le roi du midi, amer, sortira et combattra contre lui, contre le roi du nord. Celui-ci mettra sur pied une grande multitude, mais cette multitude sera livrée entre ses mains.
11:12	Cette multitude étant levée, son cœur s'élèvera. Il fera tomber des milliers, mais il n'en sera pas plus fort.
11:13	Car le roi du nord reviendra et rassemblera une plus grande multitude que la première. À la fin des temps, de quelques années, il viendra, il viendra avec une grande armée et beaucoup de biens.
11:14	En ce temps-là, beaucoup se tiendront debout contre le roi du midi. Les fils de ton peuple, des violents, se lèveront pour faire tenir debout la vision, mais ils trébucheront.
11:15	Le roi du nord<!--Le roi du nord est Séleucos IV Philopator (règne : 187 – 175 av. J.-C.).--> viendra, il élèvera des tertres et prendra les villes fortes. Les forces du midi ne tiendront pas, et les meilleurs de son peuple seront sans force pour tenir.
11:16	Celui qui viendra contre lui agira selon sa volonté et personne ne tiendra debout devant lui. Il se tiendra sur la terre de noblesse, ayant en main la destruction.
11:17	Il tournera ses faces pour venir avec la force de tout son royaume et agira avec droiture avec lui<!--Le roi du midi.-->. Il lui donnera sa fille pour femme afin de le détruire, mais elle ne tiendra pas et elle ne sera pas pour lui.
11:18	Il tournera ses faces vers les îles, et il en prendra beaucoup. Mais un chef lui fera cesser son insulte sans qu'il lui retourne l'insulte.
11:19	Il tournera sa face vers les forteresses de sa terre où il chancellera et tombera, et on ne le trouvera plus.
11:20	Quelqu’un se tiendra à sa place, qui fera passer un exacteur dans l'ornement du royaume. Mais en peu de jours il sera brisé, et ce ne sera ni par la colère ni par la guerre.
11:21	À sa place se tiendra un homme méprisé, à qui on ne donnera pas la majesté du royaume. Mais il viendra en paix, et il s'emparera du royaume par des flatteries.
11:22	Les forces de l'inondation seront submergées devant lui et brisées, de même qu'un chef de l'alliance.
11:23	Depuis l’alliance qu’on aura faite avec lui, il usera de tromperie, il montera et deviendra fort par le moyen d’une petite nation.
11:24	En toute tranquillité il entrera dans les lieux les plus fertiles de la province. Il fera ce que n'avaient pas fait ses pères, ni les pères de ses pères. Il distribuera butin, pillage et biens, il formera des plans contre les places fortes, et cela jusqu'à un certain temps.
11:25	Il excitera sa force et son cœur contre le roi du midi avec une grande armée. Et le roi du midi s'avancera en bataille avec une très grande et très forte armée, mais il ne tiendra pas, car on formera des plans contre lui.
11:26	Ceux qui mangent ses mets délicats le briseront, son armée sera submergée et beaucoup tomberont blessés mortellement.
11:27	Les deux rois auront à cœur de faire du mal et, à une même table, ils se parleront avec fausseté. Mais cela ne réussira pas, car la fin ne viendra qu'au temps fixé.
11:28	Il retournera sur sa terre avec beaucoup de biens. Son cœur sera contre la sainte alliance, il agira contre elle, puis retournera sur sa terre.
11:29	Au temps fixé, il retournera et viendra contre le midi, mais ce ne sera ni comme la première fois, ni comme la dernière.
11:30	Les navires de Kittim viendront contre lui et il sera découragé. Il reviendra et sera indigné contre la sainte alliance, il agira contre elle. Il reviendra et considèrera ceux qui abandonnent la sainte alliance.
11:31	Des forces se tiendront là de sa part et profaneront le sanctuaire, la forteresse. Elles aboliront le sacrifice perpétuel et dresseront l'abomination qui cause la désolation<!--Mt. 24:15 ; 2 Thes 2:1-7.-->.
11:32	Il corrompra par des flatteries ceux qui agissent méchamment à l'égard de l'alliance. Mais ceux du peuple qui connaîtront leur Elohîm agiront avec courage.
11:33	Les plus intelligents parmi le peuple feront discerner un grand nombre. Il en est qui trébucheront pour un temps à l'épée et à la flamme, à la captivité et au pillage.
11:34	Dans le temps où ils trébucheront, ils seront secourus avec un peu de secours, et beaucoup se joindront à eux par hypocrisie.
11:35	Quelques-uns parmi ceux qui seront prudents trébucheront, afin qu'ils soient épurés, purifiés et blanchis, jusqu'au temps de la fin, car elle n'arrivera qu'au temps fixé.
11:36	Le roi agira selon sa volonté, il s'élèvera, il se glorifiera au-dessus de tout el. Il déclarera des choses étonnantes contre le El<!--Es. 9:5.--> des elohîm, il prospérera jusqu'à ce que la colère soit accomplie, car ce qui est décrété sera fait.
11:37	Il ne discernera pas les elohîm de ses pères, le désir des femmes et tout Éloah. Il ne les discernera pas, car il s'élèvera au-dessus de tout.
11:38	À la place, il honorera l'éloah des forteresses<!--Vient de l'hébreu « mâouzzim » généralement traduit par « Mahuzzim » qui signifie « lieu ou moyens de sécurité, protection, refuge, forteresse ».-->, un éloah que n'avaient pas connu ses pères. Il l'honorera avec de l'or, de l'argent, des pierres précieuses et des objets désirables.
11:39	C'est avec l'éloah étranger qu'il agira contre les endroits fortifiés, et ceux qui le reconnaîtront, ceux qui le reconnaîtront il multipliera leur gloire, il les fera dominer sur beaucoup et leur distribuera des terrains en récompense.
11:40	Au temps de la fin, le roi du midi se heurtera contre lui avec ses cornes. Et le roi du nord fondra sur lui comme une tempête, avec des chars et des cavaliers, et avec de nombreux navires. Il viendra vers les terres, il les submergera sur son passage.
11:41	Il entrera sur la terre de gloire, et beaucoup trébucheront, mais Édom, Moab et les principaux des fils d'Ammon échapperont de sa main.
11:42	Il étendra sa main sur ces terres-là, et la terre d'Égypte n'échappera pas.
11:43	Il dominera sur les trésors cachés de l’or et de l’argent, et sur toutes les choses précieuses de l'Égypte. Les Libyens et les Éthiopiens seront sur ses pas.
11:44	Des nouvelles de l'est et du nord le terrifieront, et il sortira avec une grande fureur pour détruire et exterminer un grand nombre.
11:45	Il plantera les tentes de son palais entre la mer et la montagne de sainte beauté et il arrivera à sa fin sans que personne lui vienne en aide.

## Chapitre 12

### La résurrection pour le jugement éternel

12:1	En ce temps-là se tiendra debout Miyka'el, le grand chef, qui se tient debout pour les fils de ton peuple. Ce sera un temps de détresse tel qu'il n'y en a pas eu depuis qu'il existe une nation jusqu'à ce temps-là. En ce temps-là, ton peuple échappera – tous ceux qui seront trouvés inscrits dans le livre.

### Les deux résurrections

12:2	Beaucoup de ceux qui dorment dans la poussière du sol se réveilleront<!--Il est question ici de la résurrection. Tout d'abord, il y aura la résurrection des morts en Mashiah (Christ), lors du retour de Yéhoshoua ha Mashiah (Jésus-Christ) pour enlever l'Assemblée (Église) (1 Th. 4:13-17). Ensuite, il y aura celle de tous les saints ayant vécu avant Yéhoshoua ha Mashiah, et de toutes les personnes converties au Mashiah et mises à mort pendant la grande tribulation (Ap. 19-20). Enfin, la dernière résurrection interviendra à l'issue du millénium. Il s'agit de la résurrection des impies de tous les temps (Ap. 20:11-15). Voir également Jn. 5:24-29, 11:25.-->, ceux-ci pour la vie éternelle, et ceux-là pour la honte, pour l'aversion éternelle.
12:3	Ceux qui auront été prudents brilleront comme la splendeur du firmament, et ceux qui auront rendu justes beaucoup de gens, comme les étoiles, pour toujours et à perpétuité<!--Mt. 13:43.-->.

### Dernières paroles de YHWH à Daniye'l ; le livre scellé jusqu'au temps de la fin

12:4	Mais toi, Daniye'l, tiens secrètes ces paroles et scelle le livre jusqu'au temps de la fin. Beaucoup courront çà et là, et la connaissance augmentera<!--Ap. 5:2, 10:4.-->.
12:5	Et moi, Daniye'l, je regardai, et voici, deux autres hommes se tenaient debout, l'un en deçà du bord du fleuve, et l'autre au-delà du bord du fleuve.
12:6	L'un d'eux dit à l'homme habillé de lin, qui se tenait au-dessus des eaux du fleuve : À quand la fin des merveilles ?
12:7	J'entendis l'homme habillé de lin, qui se tenait au-dessus des eaux du fleuve. Il leva sa main droite et sa main gauche vers les cieux, et il jura par le Vivant<!--Ap. 1:8 ; 4:9 ; 5:14 ; 10:6.--> éternel que ce sera pour un temps fixé, des temps fixés et une moitié, et que toutes ces choses s'accompliront quand la force du peuple saint sera entièrement brisée.
12:8	Et moi, j’entendis, mais je ne compris pas, et je dis : Mon seigneur, quelle sera la fin de ces choses ?
12:9	Il dit : Va, Daniye'l, car ces paroles sont tenues secrètes et scellées jusqu'au temps de la fin.
12:10	Beaucoup seront purifiés, blanchis et éprouvés. Mais les méchants agiront avec méchanceté, et aucun des méchants ne comprendra, mais ceux qui seront prudents comprendront.
12:11	Depuis le temps où cessera le sacrifice perpétuel et où sera placée l'abomination de la désolation, il y aura 1 290 jours<!--Mt. 24:15 ; Mc. 13:14 ; Lu. 21:20.-->.
12:12	Heureux celui qui attendra et qui atteindra 1 335 jours !
12:13	Et toi, marche jusqu'à la fin ! Tu te reposeras et tu te tiendras debout pour ton lot à la fin des jours.
