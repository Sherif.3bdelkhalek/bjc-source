# 2 Petros (2 Pierre) (2 Pi.)

Signification : Roc, pierre

Auteur : Petros (Pierre)

Thème : Appel à la sainteté et prophétie sur l'apparition de moqueurs et de maîtres corrompus

Date de rédaction : Env. 66 ap. J.-C.

Sans doute écrite à Rome, cette lettre, tout comme la première, semble avoir été destinée aux assemblées d'Asie Mineure. Petros (Pierre) y exhorte les chrétiens à être vigilants quant aux faux docteurs et aux nombreuses hérésies. Il insiste sur l'appel et l'élection des chrétiens dont le comportement doit être exemplaire. Il leur rappelle la véracité des écrits prophétiques et les invite à persévérer dans la vie chrétienne.

## Chapitre 1

### Introduction

1:1	Shim’ôn Petros<!--Simon Pierre.-->, esclave et apôtre de Yéhoshoua Mashiah, à ceux qui ont obtenu par tirage au sort une foi du même prix que la nôtre, par la justice de notre Elohîm et Sauveur Yéhoshoua Mashiah<!--Petros (Pierre) affirme ici avec force la divinité de Yéhoshoua ha Mashiah (Jésus-Christ).-->.
1:2	Grâce et shalôm à vous, multipliés dans la connaissance précise et correcte d'Elohîm et de Yéhoshoua notre Seigneur !

### Affermir sa vocation et son élection

1:3	Comme sa divine puissance nous a donné tout ce qui regarde la vie et la piété, au moyen de la connaissance précise et correcte de celui qui nous a appelés au moyen de la gloire et de la vertu,
1:4	au moyen desquelles nous sont données les plus grandes et précieuses promesses, afin que par leur moyen vous deveniez participants de la nature divine, ayant échappé à la corruption qui est dans le monde par la convoitise.
1:5	Or, pour cette même raison aussi, y apportant en outre tout empressement, fournissez à votre foi la vertu, et à la vertu la connaissance,
1:6	et à la connaissance le contrôle de soi, et au contrôle de soi la patience, et à la patience la piété,
1:7	et à la piété l'amour fraternel, et à l'amour fraternel l'amour.
1:8	Car si ces choses sont en vous et se multiplient, elles ne vous rendent pas paresseux ni stériles pour la connaissance précise et correcte de notre Seigneur Yéhoshoua Mashiah.
1:9	Car celui en qui ces choses ne sont pas présentes est aveugle, et ne voit pas de loin, ayant oublié la purification de ses anciens péchés.
1:10	C'est pourquoi, frères, efforcez-vous de plus en plus à affermir votre vocation et votre élection, car en faisant ces choses, vous ne trébucherez jamais.
1:11	Car c'est ainsi que l'entrée dans le Royaume éternel de notre Seigneur et Sauveur Yéhoshoua Mashiah vous sera richement fournie.

### Autorité du témoignage de Petros (Pierre)

1:12	C'est pourquoi je ne négligerai pas de vous rappeler sans cesse ces choses, quoique vous les sachiez et que vous soyez affermis dans la présente vérité.
1:13	Mais j'estime qu'il est juste de vous réveiller par des avertissements, aussi longtemps que je suis dans cette tente<!--Tente ou tabernacle. Ces termes sont utilisés comme une belle métaphore du corps humain qui est la demeure de l'esprit humain. Voir Za. 12:1.-->,
1:14	sachant que l'abandon<!--« Rejeter », « quitter », « évacuer ».--> de ma tente est imminent, comme aussi notre Seigneur Yéhoshoua Mashiah me l'a fait connaître<!--Voir Jn. 21:19.-->.

### Souvenir de la transfiguration

1:15	Mais je m’efforcerai de faire en sorte qu’après mon départ vous ayez toujours de quoi vous rappeler ces choses.
1:16	Car ce n'est pas en suivant des fables<!--Voir 1 Ti. 1:4 ; 2 Ti. 4:4 ; Tit. 1:14.--> inventées, que nous vous avons fait connaître la puissance et la parousie<!--La parousie du Seigneur. Voir Mt. 24:3.--> de notre Seigneur Yéhoshoua Mashiah, mais c’est en étant devenus témoins oculaires<!--Vient du grec « epoptes ». C'était le nom donné à ceux qui avait atteint le troisième, donc le plus haut niveau, dans l'initiation aux mystères d'Éleusis, ville proche d'Athènes, dans un culte rendu à la déesse de la Végétation, Déméter.--> de sa grandeur.
1:17	Car il a reçu d'Elohîm le Père honneur et gloire, lorsque la voix que voici a été portée jusqu'à lui par la gloire magnifique : Celui-ci est mon Fils bien-aimé, en qui j'ai pris plaisir<!--Mt. 3:17.-->.
1:18	Et nous avons entendu cette voix portée depuis le ciel, étant avec lui sur la sainte montagne.

### La parole prophétique

1:19	Nous avons aussi la ferme parole prophétique, à laquelle vous faites bien d'être attentifs, comme à une lampe qui brille dans un lieu obscur, jusqu'à ce que le jour ait brillé à travers l'obscurité de la nuit<!--Apparaître.--> et que l'Étoile du matin<!--Yéhoshoua est l'Étoile du matin. C'est cette Étoile qui indiqua aux mages le chemin de la maison où était l'enfant Yéhoshoua (Mt. 2). Balaam a également parlé de cette Étoile (No. 24:17) et Yéhoshoua lui-même s'est présenté comme l'Étoile brillante du matin (Ap. 22:16).--> se lève dans vos cœurs.
1:20	Sachez d'abord ceci : qu'aucune prophétie de l'Écriture ne vient d'une interprétation particulière,
1:21	car la prophétie n'a jamais été autrefois apportée par la volonté humaine, mais c'est portés par le Saint-Esprit que les saints hommes d'Elohîm ont parlé.

## Chapitre 2

### Avertissements contre les faux docteurs

2:1	Mais il y a eu aussi de faux prophètes parmi le peuple, comme il y aura aussi parmi vous de faux docteurs, qui introduiront secrètement des sectes de perdition, reniant le Seigneur qui les a achetés, et amenant sur eux-mêmes une perdition soudaine.
2:2	Et beaucoup suivront leurs perditions<!--Le mot grec traduit par perdition signifie aussi destruction.-->, et à cause d'eux, la voie de la vérité sera blasphémée.
2:3	Et, par cupidité, ils trafiqueront de vous au moyen de paroles fabriquées<!--Vient du grec « plastos » qui signifie « moulé, modelé, formé comme dans l'argile, ce qui est feint ou trompeur ».-->, eux dont le jugement depuis longtemps n'est pas inactif et dont la destruction ne sommeille pas.
2:4	Car si Elohîm n'a pas épargné les anges qui avaient péché, mais s'il les a livrés aux chaînes d’obscurité<!--Voir Jud 1:6.--> dans le Tartare<!--Tartare. Du grec « tartaroo », ce terme est le nom de la région souterraine, lugubre et sombre, considérée par les Grecs anciens comme la demeure du méchant à sa mort, où il souffre le châtiment pour ses mauvaises actions. « Tartaroo » vient de « tartaros » qui signifie « les plus profonds abîmes du Hadès ».--> où ils sont gardés pour le jugement,
2:5	et s'il n'a pas épargné l'ancien monde, mais s'il a gardé Noah<!--Voir Ge. 7.-->, le huitième homme, prédicateur de la justice, lorsqu'il a fait venir le déluge sur un monde d'impies,
2:6	et s'il a condamné à la destruction totale et réduit en cendres les villes de Sodome et Gomorrhe, les donnant en exemple à ceux qui par la suite vivraient dans l'impiété,
2:7	et s'il a délivré Lot<!--Voir Ge. 18-19.--> le juste, qui était profondément affligé par la luxure sans bride de ces hors-la-loi,
2:8	– car ce juste, habitant parmi eux, torturait de jour en jour son âme juste à cause de ce qu'il voyait et entendait dire de leurs œuvres violeuses de la torah<!--Voir Mc. 15:28.--> –
2:9	le Seigneur sait délivrer de la tentation les pieux et garder les injustes pour être punis au jour du jugement.
2:10	Mais principalement ceux qui vont après la chair dans un désir d’impureté et qui méprisent la seigneurie. Audacieux, arrogants, ils ne craignent pas de blasphémer les gloires,
2:11	alors que les anges, qui sont plus grands en force et en puissance, ne prononcent pas contre elles de jugement blasphématoire devant le Seigneur.
2:12	Mais eux, semblables à des bêtes dépourvues de raison, gouvernées par les instincts naturels, nées pour la capture et la corruption, parlant d'une manière blasphématoire de ce qu'ils ignorent, ils seront corrompus par leur propre corruption.
2:13	Ils recevront la récompense de l'injustice. Ils estiment le plaisir d'une vie de luxe en plein jour. Ce sont des taches et des souillures. Ils vivent dans le luxe et dans leurs propres tromperies quand ils font des festins somptueux avec vous.
2:14	Ayant les yeux pleins d'adultère<!--Ou femme adultère.--> et, incapables d'arrêter de pécher, ils attrapent avec un appât les âmes instables<!--Le mot grec signifie aussi « chancelant, inconstant ». Voir 2 Pi. 3:16.-->. Ils ont le cœur exercé à la cupidité : ce sont des enfants de malédiction.
2:15	Après avoir abandonné la voie droite, ils se sont égarés en suivant la voie de Balaam<!--Balaam. Voir No. 22.--> de Beor<!--Le père de Balaam. No. 24:3.-->, qui a aimé le salaire de l'injustice, 
2:16	mais il a reçu une réprimande pour sa transgression : une ânesse sans la faculté de la parole, émettant un son avec une voix humaine, a empêché la folie du prophète.
2:17	Ce sont des sources sans eau, des nuées agitées par un tourbillon de vent : l'obscurité de la ténèbre leur est réservée pour l'éternité.
2:18	Car en émettant des sons enflés et dépourvus de vérité et de convenance<!--Enflés de perversité.-->, ils attrapent avec un appât, par les désirs de la chair<!--Voir Ga. 5:16-21.--> et par la luxure sans bride, ceux qui ont vraiment échappé aux gens qui tournent en rond dans l'égarement.
2:19	Ils leur promettent la liberté, étant eux-mêmes esclaves de la corruption, car on devient esclave de celui par qui on est vaincu.
2:20	Car, si après avoir échappé aux souillures du monde par la connaissance précise et correcte du Seigneur et Sauveur Yéhoshoua Mashiah, ils s'y empêtrent<!--2 Ti. 2:4.--> de nouveau et sont vaincus par elles, leur dernière condition est pire que la première<!--Mt. 12:43-45.-->.
2:21	Car mieux valait pour eux n'avoir pas connu la voie de la justice, que de l'avoir connue et se détourner du saint commandement qui leur avait été donné.
2:22	Mais ce qu'on dit par un proverbe véritable leur est arrivé : Le chien est retourné à son propre vomissement<!--Pr. 26:11.-->, et la truie lavée s'est vautrée dans le bourbier.

## Chapitre 3

3:1	Bien-aimés, voici déjà la seconde lettre que je vous écris. Dans l'une et dans l'autre je réveille par un rappel la pureté de vos pensées,
3:2	afin que vous vous souveniez des paroles qui ont été dites auparavant par les saints prophètes, et du commandement de vos apôtres, qui est celui du Seigneur et Sauveur.

### La promesse de la parousie du Seigneur

3:3	Sachant avant tout ceci : que dans les derniers<!--Ou la fin des jours. Yaacov est le premier homme à avoir utilisé l'expression « derniers jours ». Ge. 49:1-2 ; Da. 2:28 ; Ac. 2:17 ; 2 Ti. 3:1 ; Jud. 1:18. Voir la résurrection des saints. Jn. 6:39-40 et 44 et 54.--> jours, des moqueurs viendront, se conduisant selon leurs propres désirs,
3:4	et disant : Où est la promesse de sa parousie ? Car depuis que les pères sont morts, toutes choses restent permanentes comme depuis le commencement de la création.
3:5	Car ils oublient volontairement que des cieux existèrent autrefois par la parole d'Elohîm, ainsi qu'une terre tirée de l'eau et qui subsistait au moyen de l'eau,
3:6	et que, par ces mêmes causes, le monde d'alors fut détruit, étant submergé par l'eau<!--Voir Ge. 6.-->.
3:7	Mais les cieux et la Terre d'à présent sont gardés par la même parole, étant réservés pour le feu au jour du jugement et de la destruction des humains impies.
3:8	Mais n’ignorez pas cette chose, bien-aimés, c’est qu’un jour est devant le Seigneur comme 1 000 ans, et 1 000 ans comme un jour<!--Ps. 90:4.-->.
3:9	Le Seigneur ne tarde pas en ce qui concerne la promesse, comme quelques-uns estiment qu'il y a de la lenteur. Mais il est patient envers nous, ne voulant qu'aucun ne périsse, mais que tous laissent un espace<!--« Laisser un espace (qui peut être rempli ou occupé par un autre), faire une place, donner une chambre », « céder ».--> pour la repentance.
3:10	Or le jour du Seigneur<!--Le jour du Seigneur. Voir Za. 14:1.--> viendra comme un voleur dans la nuit. Et en ce jour-là, les cieux passeront avec un fort bruit, et les éléments embrasés seront dissous, et la Terre avec les œuvres qui sont en elle sera brûlée entièrement.
3:11	Puisque toutes ces choses se dissolvent ainsi, quels gens devriez-vous être en sainte conduite et en piété,
3:12	attendant et hâtant la parousie du jour d'Elohîm, à cause duquel les cieux en feu seront dissous et les éléments embrasés se fondront !
3:13	Mais nous attendons, selon sa promesse, de nouveaux cieux et une nouvelle Terre<!--Es. 66:22.--> dans lesquels la justice habite.

### Petros (Pierre) témoigne de Paulos (Paul)

3:14	C'est pourquoi, bien-aimés, en attendant ces choses, efforcez-vous d’être trouvés par lui sans tache et innocents dans la paix.
3:15	Et considérez comme salut la patience du Seigneur, comme Paulos, notre frère bien-aimé vous l'a aussi écrit, selon la sagesse qui lui a été donnée.
3:16	Comme il le fait aussi dans toutes ses lettres, où il parle de ces choses, dans lesquelles il y a des points difficiles à comprendre, que les personnes ignorantes et instables<!--Voir 2 Pi. 2:14.--> tordent<!--Jé. 23:36.-->, ainsi que les autres Écritures, pour leur propre destruction.

### Conclusion

3:17	Vous donc, bien-aimés, puisque vous le savez d'avance, prenez garde, de peur qu'entraînés par l'égarement des hors-la-loi, vous ne perdiez votre ferme condition.
3:18	Mais croissez dans la grâce et dans la connaissance de notre Seigneur et Sauveur Yéhoshoua Mashiah. À lui la gloire<!--Mt. 6:13 ; 16:27 ; Lu. 2:14 ; Ro. 16:27.-->, et maintenant et pour le jour de l'éternité ! Amen !
