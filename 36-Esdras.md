# Ezra (Esdras) (Esd.)

Signification : Secours

Auteur : Ezra (Esdras)

Thème : Édit de Cyrus et reconstruction du temple

Date de rédaction : 5ème siècle av. J.-C.

Conformément aux prophéties reçues par Yesha`yah (Ésaïe) et Yirmeyah (Jérémie), YHWH réveilla le cœur du roi Cyrus afin de renvoyer les fils d'Israël sur leur terre avec la mission de reconstruire le temple détruit quelques décennies auparavant. Ce livre montre comment Elohîm ramena glorieusement son peuple à Yeroushalaim (Jérusalem) et retrace la reconstruction du temple ainsi que les épreuves ayant accompagné ce projet. Il traite également des réformes sociales et religieuses mises en place dans le cadre d'un retour total à YHWH.

## Chapitre 1

### Publication de Cyrus

1:1	En l’an un de Cyrus<!--538 av. J.-C.-->, roi de Perse, afin que la parole de YHWH prononcée par la bouche de Yirmeyah<!--Jé. 25:12, 29:10, 33:7-10.--> soit accomplie, YHWH réveilla l'esprit de Cyrus, roi de Perse, qui fit publier dans tout son royaume cet édit, et même par écrit disant :
1:2	Ainsi parle Cyrus, roi de Perse : Tous les royaumes de la Terre, YHWH, l'Elohîm des cieux, me les a donnés, et lui-même m’a désigné pour lui bâtir une maison à Yeroushalaim, en Yéhouda.
1:3	Qui d’entre vous, quel qu’il soit, est de son peuple, que son Elohîm soit avec lui, et qu'il monte à Yeroushalaim, en Yéhouda, bâtir la maison de YHWH, l'Elohîm d'Israël – c'est l'Elohîm qui est à Yeroushalaim !
1:4	Et quant à tous ceux qui restent, en tous lieux où ils séjournent, que les hommes du lieu où ils demeurent, leur viennent en aide avec de l'argent, de l'or, des biens et du bétail, outre ce qu'on offrira volontairement pour la maison d'Elohîm qui est à Yeroushalaim.

### Les ustensiles du temple restitués ; offrandes volontaires

1:5	Les têtes des pères de Yéhouda, de Benyamin, des prêtres et des Lévites, se levèrent pour conduire tous ceux dont Elohîm réveilla l'esprit, afin de monter pour bâtir la maison de YHWH, qui habite à Yeroushalaim.
1:6	Tous ceux qui étaient autour d'eux les fortifièrent de leurs mains avec des objets en argent et en or, des biens, du bétail, et des choses précieuses, outre tout ce qu'on offrit volontairement.
1:7	Le roi Cyrus fit sortir les ustensiles de la maison de YHWH, que Neboukadnetsar avait fait sortir<!--2 R. 24:13 ; Da. 1:1-2 ; 2 Ch. 36:7.--> de Yeroushalaim et qu'il avait mis dans la maison de son elohîm.
1:8	Et Cyrus, roi de Perse, les fit sortir par la main de Mithredath, le trésorier, qui en fit le compte pour Sheshbatsar, le prince de Yéhouda.
1:9	Et voici leur nombre : 30 bassins d'or, 1 000 bassins d'argent, 29 couteaux,
1:10	30 bols d'or, 410 bols d'argent de second ordre, et d'autres ustensiles par milliers.
1:11	Tous les ustensiles en or et en argent étaient de 5 400. Sheshbatsar fit monter le tout avec la montée des exilés de Babel<!--Babylone.--> à Yeroushalaim.

## Chapitre 2

### Dénombrement des Israélites revenus de captivité

2:1	Voici les fils de la province qui montèrent de la captivité et de l'exil, ceux que Neboukadnetsar, roi de Babel, avait emmenés captifs à Babel, et qui retournèrent à Yeroushalaim et en Yéhouda. Chaque homme dans sa ville<!--Né. 7:6.-->.
2:2	Ils vinrent avec Zerubbabel<!--Zorobabel.-->, Yéshoua, Nehemyah, Serayah, Re’élayah, Mordekay<!--Mardochée.-->, Bilshan, Mispar, Bigvaï, Rehoum, Ba`anah. Nombre des hommes du peuple d'Israël :
2:3	Les fils de Pareosh : 2 172.
2:4	Les fils de Shephatyah : 372.
2:5	Les fils d'Arach : 775.
2:6	Les fils de Pachath-Moab, des fils de Yéshoua, de Yoab : 2 812.
2:7	Les fils d'Éylam : 1 254.
2:8	Les fils de Zatthou : 945.
2:9	Les fils de Zakkay : 760.
2:10	Les fils de Bani : 642.
2:11	Les fils de Bébaï : 623.
2:12	Les fils d'Azgad : 1 222.
2:13	Les fils d'Adonikam : 666.
2:14	Les fils de Bigvaï : 2 056.
2:15	Les fils d'Adin : 454.
2:16	Les fils d'Ather, de Yehizqiyah : 98.
2:17	Les fils de Betsaï : 323.
2:18	Les fils de Yorah : 112.
2:19	Les fils de Hashoum : 223.
2:20	Les fils de Guibbar : 95.
2:21	Les fils de Bethléhem : 123.
2:22	Les hommes de Netophah : 56.
2:23	Les hommes d'Anathoth : 128.
2:24	Les fils d'Azmaveth : 42.
2:25	Les fils de Qiryath-Yéarim, de Kephiyrah et de Beéroth : 743.
2:26	Les fils de Ramah et de Guéba : 621.
2:27	Les hommes de Micmas : 122.
2:28	Les hommes de Béth-El et de Aï : 223.
2:29	Les fils de Nebo : 52.
2:30	Les fils de Magbish : 156.
2:31	Les fils d'un autre Éylam : 1 254.
2:32	Les fils de Harim : 320.
2:33	Les fils de Lod, de Hadid et d'Ono : 725.
2:34	Les fils de Yeriycho : 345.
2:35	Les fils de Senaa : 3 630.

### Dénombrement des prêtres revenus de captivité

2:36	Des prêtres : Les fils de Yekda`yah, de la maison de Yéshoua : 973.
2:37	Les fils d'Immer : 1 052.
2:38	Les fils de Pashhour : 1 247.
2:39	Les fils de Harim : 1 017.

### Dénombrement des Lévites revenus de captivité

2:40	Des Lévites : les fils de Yéshoua et de Qadmiy'el, d'entre les fils d'Hodavyah : 74.
2:41	Des chanteurs : les fils d'Asaph : 128.
2:42	Des fils des portiers : les fils de Shalloum, les fils d'Ather, les fils de Thalmon, les fils d'Aqqoub, les fils de Hathitha, les fils de Shobaï, en tout 139.
2:43	Des Néthiniens : les fils de Tsicha, les fils de Hasoupha, les fils de Tabbahoth,
2:44	les fils de Kéros, les fils de Siaha, les fils de Padon,
2:45	les fils de Lebana, les fils de Hagaba, les fils d'Aqqoub,
2:46	les fils de Hagab, les fils de Shamlaï, les fils de Hanan,
2:47	les fils de Guiddel, les fils de Gachar, les fils de Reayah,
2:48	les fils de Retsin, les fils de Nekoda, les fils de Gazzam,
2:49	les fils d'Ouzza, les fils de Paséach, les fils de Bésaï,
2:50	les fils d'Asna, les fils de Mehounim, les fils de Nephousim,
2:51	les fils de Baqbouq, les fils de Haqoupha, les fils de Har-hour,
2:52	les fils de Batslouth, les fils de Mehida, les fils de Harsha,
2:53	les fils de Barkos, les fils de Sisera, les fils de Thamach,
2:54	les fils de Netsiach, les fils de Hathipha.

### Dénombrement des serviteurs de Shelomoh revenus de captivité

2:55	Des fils des serviteurs de Shelomoh : les fils de Sothaï, les fils de Sophéreth, les fils de Perouda,
2:56	les fils de Ya`ala, les fils de Darkon, les fils de Guiddel,
2:57	les fils de Shephatyah, les fils de Hatthil, les fils de Pokéreth-Hatsebaïm, les fils d'Ami.
2:58	Total des Néthiniens et des fils des serviteurs de Shelomoh : 392.
2:59	Voici ceux qui montèrent de Thel-Mélach, de Thel-Harsha, de Keroub-Addân, d'Immer et qui ne purent pas faire connaître si la maison de leurs pères et leur semence étaient d’Israël : 
2:60	Les fils de Delayah, les fils de Tobiyah, les fils de Nekoda : 652.

### Des prêtres sont privés de la prêtrise

2:61	Parmi les fils des prêtres : les fils de Chabayah, les fils d'Hakkots, les fils de Barzillaï, qui avait pris une femme parmi les filles de Barzillaï<!--2 S. 17:27 ; Né. 7:63.-->, le Galaadite et fut appelé de leur nom.
2:62	Ceux-ci cherchèrent leurs registres généalogiques, mais ils ne les trouvèrent pas. C'est pourquoi on les déclara souillés pour la prêtrise.
2:63	Le gouverneur leur dit de ne pas manger du saint des saints, en attendant qu'un prêtre ait consulté l'ourim et le thoummim.

### Ensemble des Israélites revenus de captivité

2:64	L'assemblée tout entière était seulement de 42 360,
2:65	outre leurs serviteurs et leurs servantes ; ceux-ci : 7 337. Leurs chanteurs et leurs chanteuses : 200.
2:66	Ils avaient 736 chevaux, et 245 mulets,
2:67	435 chameaux et 6 720 ânes.
2:68	Des têtes des pères, quand elles vinrent à la maison de YHWH à Yeroushalaim, firent des offrandes volontaires pour la maison d’Elohîm, afin qu'on la rétablisse sur ses fondations.
2:69	Elles donnèrent au trésor de l'ouvrage, selon leurs moyens, 61 000 drachmes d'or, et 5 000 mines d'argent, et 100 tuniques de prêtres.
2:70	Les prêtres et les Lévites, ainsi que certains du peuple, les chanteurs, les portiers et les Néthiniens habitèrent dans leurs villes, et tout Israël dans ses villes.

## Chapitre 3

### Rétablissement de l'autel et reprise des sacrifices

3:1	Or le septième mois approcha et les fils d'Israël étaient dans leurs villes. Le peuple se rassembla comme un seul homme à Yeroushalaim.
3:2	Yéshoua, fils de Yotsadaq, avec ses frères les prêtres, et Zerubbabel, fils de Shealthiel, avec ses frères, se levèrent et bâtirent l'autel de l'Elohîm d'Israël, pour y faire monter des holocaustes, comme il est écrit dans la torah de Moshé, homme d'Elohîm.
3:3	Ils rétablirent l'autel d'Elohîm sur sa base, car la terreur des peuples de ces terres était sur eux, et ils y firent monter des holocaustes à YHWH, les holocaustes du matin et du soir<!--No. 28:3.-->.
3:4	Ils célébrèrent aussi la fête des cabanes, comme il est écrit, et ils firent monter jour par jour des holocaustes, selon leur nombre, selon l’ordonnance, parole du jour en son jour.
3:5	Ensuite il y eut l’holocauste perpétuel et celui pour les nouvelles lunes, et pour toutes les fêtes solennelles consacrées à YHWH, ainsi que pour tous ceux qui faisaient des offrandes volontaires à YHWH.
3:6	Dès le premier jour du septième mois, ils commencèrent à faire monter des holocaustes à YHWH, bien que le temple de YHWH ne fût pas encore fondé.
3:7	Ils donnèrent de l'argent aux tailleurs de pierres et aux charpentiers, de la nourriture, de la boisson, de l’huile aux Sidoniens et aux Tyriens, afin qu'ils amènent du bois de cèdre du Liban par la mer de Yapho, selon la permission que Cyrus, roi de Perse, leur en avait donnée.

### Pose des fondements de la maison de YHWH

3:8	La deuxième année de leur arrivée à la maison d’Elohîm à Yeroushalaim, au deuxième mois, Zerubbabel, fils de Shealthiel, Yéshoua, fils de Yotsadaq, et le reste de leurs frères, les prêtres et les Lévites, et tous ceux qui étaient revenus de la captivité à Yeroushalaim, commencèrent le travail. Ils établirent des Lévites, des fils de 20 ans et au-dessus, afin de surveiller les travaux de la maison de YHWH.
3:9	Yéshoua, ses fils et ses frères, Qadmiy'el avec ses fils, fils de Yéhouda, se tenaient debout comme un seul homme pour surveiller ceux qui faisaient le travail de la maison d’Elohîm, de même que les fils de Hénadad, avec leurs fils et leurs frères, les Lévites.
3:10	Lorsque ceux qui bâtissaient posèrent les fondements du temple de YHWH, on y fit assister les prêtres revêtus de leurs habits avec leurs trompettes, et les Lévites, fils d'Asaph, avec les cymbales, pour qu'ils célèbrent YHWH, par la main de David, roi d'Israël.
3:11	Et ils se répondaient, en louant et célébrant YHWH : Car il est bon ! Car sa bonté pour Israël est éternelle ! Et tout le peuple poussait de grands cris de joie en louant YHWH, parce qu'on posait les fondements de la maison de YHWH.
3:12	Beaucoup de prêtres, de Lévites et de têtes de pères parmi les plus âgés, ceux qui avaient vu la maison d'autrefois pleuraient à grand bruit, tandis qu'on posait sous leurs yeux les fondations de cette maison. Beaucoup poussaient des cris de joie, en élevant leur voix.
3:13	Et le peuple ne pouvait discerner le bruit des cris de joie de celui des pleurs du peuple, car le peuple poussait de grands cris dont le son s'entendait au loin.

## Chapitre 4

### Tentatives de découragement des ennemis de Yéhouda et Benyamin

4:1	Or les ennemis de Yéhouda et de Benyamin apprirent que les fils de la captivité bâtissaient un temple à YHWH, l'Elohîm d'Israël.
4:2	Ils s'approchèrent de Zerubbabel et des têtes des pères et leur dirent : Nous bâtirons<!--On ne doit jamais s'associer avec les impies pour bâtir l'œuvre du Seigneur. Satan essaie toujours de s'infiltrer dans les assemblées afin de nous éloigner de la vérité, c'est pour cela que nous devons faire preuve de discernement (2 Co. 6:14-16).--> avec vous ! Car nous cherchons votre Elohîm comme vous, et c'est à lui que nous sacrifions depuis le temps d'Ésar-Haddon, roi d'Assyrie, qui nous a fait monter ici.
4:3	Mais Zerubbabel, Yéshoua, et le reste des têtes des pères d'Israël, leur dirent : Il n'est pas convenable que nous bâtissions ensemble la maison de notre Elohîm, mais nous la bâtirons nous seuls pour YHWH, l'Elohîm d'Israël, comme nous l'a ordonné le roi Cyrus, roi de Perse<!--Esd. 1:1-5.-->.
4:4	Il arriva que le peuple de la terre rendit paresseuses les mains du peuple de Yéhouda, en lui faisant peur de bâtir.
4:5	Ils avaient même engagé des conseillers pour annuler leur conseil, tous les jours de Cyrus, roi de Perse, jusqu'au règne de Darius, roi de Perse.
4:6	Sous le règne d'Assuérus, au commencement de son règne, ils écrivirent une accusation calomnieuse contre les habitants de Yéhouda et de Yeroushalaim.

### Lettre des opposants à Artaxerxès

4:7	Aux jours d'Artaxerxès, Bishlam, Mithredath, Thabeel et le reste de leurs collègues, écrivirent à Artaxerxès, roi de Perse. La lettre était écrite en caractères araméens et elle était traduite en araméen.
4:8	Rehoum, le seigneur des décrets et Shimshaï, le scribe, écrivirent au roi Artaxerxès une lettre contre Yeroushalaim, comme suit :
4:9	Rehoum, le seigneur des décrets, Shimshaï, le scribe et le reste de leurs collègues, ceux de Diynay, d'Apharcekay, de Tharpel, d'Apharas, d'Érec, de Babel, de Suse, de Déha, d'Éylam,
4:10	et le reste des peuples que le grand et glorieux Osnappar a emmenés en exil et fait habiter dans la ville de Samarie, et le reste au-delà du fleuve, et maintenant,
4:11	voici ici la copie de la lettre qu'ils envoyèrent au roi Artaxerxès : Tes serviteurs, les hommes d'au-delà du fleuve. Et maintenant :
4:12	Que le roi sache que les Juifs qui sont montés d’auprès de toi vers nous, sont venus à Yeroushalaim, qu'ils bâtissent la ville rebelle et méchante, et qu’ils posent les fondements des murailles et les relèvent.
4:13	Maintenant le roi doit être informé que si cette ville est rebâtie et si ses murs sont achevés, ils ne paieront plus de tribut, ni d'impôt, ni de droit de passage, et elle causera une grande nuisance aux revenus du roi.
4:14	Maintenant, puisque nous mangeons le sel du palais, il ne nous paraît pas convenable de voir le roi déshonoré. C'est pourquoi nous avons envoyé au roi et nous lui faisons savoir :
4:15	qu'il cherche dans le livre des mémoires de ses pères, et il trouvera et il apprendra dans ce livre des mémoires que cette ville est une ville rebelle, nuisible aux rois et aux provinces, et qu'on s'y est livré à la révolte dès les jours anciens. Donc cette ville a été détruite à cause de cela.
4:16	Nous faisons savoir au roi que si cette ville est rebâtie et si ses murailles sont relevées, il n'aura plus de possession au-delà du fleuve.

### Réponse du roi Artaxerxès ; suspension des travaux

4:17	Le roi envoya ce rapport : À Rehoum, le seigneur des décrets, à Shimshaï, le scribe, et au reste de leurs collègues demeurant à Samarie, et au reste au-delà du fleuve : Paix ! Et maintenant,
4:18	la lettre que vous nous avez envoyée a été exactement lue devant moi.
4:19	Et j'ai pris un décret afin qu'on fasse des recherches et l'on a trouvé que dès les jours anciens, cette ville s'est soulevée contre les rois et qu'on s'y est livré à la sédition et à la révolte.
4:20	Il y eut à Yeroushalaim des rois puissants, qui dominèrent sur tout ce qui est au-delà du fleuve et auxquels on payait tribut, impôt et droit de passage<!--2 S. 8:2,6 ; 2 Ch. 17:11.-->.
4:21	Maintenant, donnez l'ordre<!--Le mot araméen signifie littéralement « décret ».--> de faire cesser le travail de ces gens, afin que cette ville ne soit pas rebâtie jusqu'à ce que je l'ordonne par décret.
4:22	Gardez-vous de mettre en cela de la négligence, de peur que le mal n'augmente au préjudice des rois.
4:23	Et dès que la copie de la lettre du roi Artaxerxès eut été lue devant Rehoum, Shimshaï, le scribe, et leurs collègues, ils allèrent en hâte à Yeroushalaim vers les Juifs, et ils les firent cesser par force et par violence.
4:24	Alors l'ouvrage de la maison d'Élahh, à Yeroushalaim, cessa, et elle demeura dans cet état, jusqu'à la deuxième année du règne de Darius, roi de Perse.

## Chapitre 5

### Chaggay (Aggée) et Zekaryah (Zacharie) prophétisent ; reprise des travaux

5:1	Chaggay, le prophète, et Zekaryah, fils d'Iddo, le prophète, prophétisèrent aux Juifs qui étaient en Yéhouda et à Yeroushalaim, au nom de l'Élahh d'Israël qui était au-dessus d’eux.
5:2	Et Zerubbabel, fils de Shealthiel, et Yeshuwa<!--Vient de l'araméen et signifie « il est sauvé ».-->, fils de Yotsadaq, se levèrent et commencèrent à rebâtir la maison d’Élahh à Yeroushalaim. Et ils avaient avec eux les prophètes d'Élahh qui les soutenaient.
5:3	En ce temps-là, Thathnaï, gouverneur d’au-delà du fleuve, Shethar-Boznaï et leurs collègues vinrent vers eux et leur parlèrent ainsi : Qui vous a donné l'ordre<!--Littéralement « décret ».--> de rebâtir cette maison et de relever ces murailles ?<!--Esd. 5:9.-->
5:4	Ils leur dirent alors : Quels sont les noms des hommes qui construisent cet édifice ?
5:5	Mais parce que l'œil de leur Élahh était sur les anciens des Juifs, on ne les fit pas cesser, jusqu'à ce que l'affaire parvienne à Darius, et jusqu'à la réception d'une lettre sur cet objet.

### Lettre des opposants à Darius

5:6	Copie de la lettre envoyée au roi Darius par Thathnaï, gouverneur d’au-delà du fleuve, Shethar-Boznaï et leurs collègues d'Apharcekay, d’au-delà du fleuve.
5:7	Ils lui envoyèrent un rapport ainsi écrit : Paix parfaite soit au roi Darius !
5:8	Le roi doit être informé que nous sommes allés dans la province de Yéhouda, vers la maison du grand Élahh. Elle se bâtit avec des pierres de taille, et le bois se pose dans les murs ; ce travail se réalise complètement et prospère entre leurs mains.
5:9	Nous avons interrogé ces anciens et nous leur avons parlé ainsi : Qui vous a donné l'autorisation de rebâtir cette maison et de finir ces murs ?<!--Esd. 5:3.-->
5:10	Nous leur avons aussi demandé leurs noms pour te les faire connaître, et nous avons mis par écrit les noms des hommes à leur tête.
5:11	Voici le rapport qu'ils ont fait en disant : Nous sommes les serviteurs de l'Élahh des cieux et de la Terre, et nous rebâtissons la maison qui avait été bâtie autrefois. Il y a de nombreuses années un grand roi d'Israël l'avait bâtie et achevée.
5:12	Mais après que nos pères eurent provoqué la colère de l'Élahh des cieux, il les livra entre les mains de Neboukadnetsar<!--Voir 2 R. 24 et 25.-->, roi de Babel, le Chaldéen, qui détruisit cette maison et qui emmena le peuple en exil à Babel.
5:13	Mais l'an un de Cyrus, roi de Babel, le roi Cyrus prit un décret pour rebâtir cette maison d’Élahh<!--Esd. 1:1-2.-->.
5:14	Et même le roi Cyrus tira hors du temple de Babel, les ustensiles en or et en argent de la maison d’Élahh, que Neboukadnetsar avait emportés du temple qui était à Yeroushalaim et qu'il avait apportés dans le temple de Babel. Ils furent remis à un nommé Sheshbatsar, lequel il avait établi gouverneur.
5:15	Et il lui dit : Prends ces ustensiles et va-t'en, et fais-les porter dans le temple qui était à Yeroushalaim, et que la maison d’Élahh soit rebâtie sur sa place.
5:16	Alors ce Sheshbatsar est venu, et il a posé les fondements de la maison d’Élahh à Yeroushalaim. Depuis lors jusqu’à maintenant, on la bâtit, et elle n'est pas encore achevée.
5:17	Maintenant, s'il semble bon au roi, que l'on fasse des recherches dans la maison des trésors du roi qui est à Babel, pour voir s'il est vrai qu'il y a eu un décret pris par Cyrus pour rebâtir cette maison d’Élahh à Yeroushalaim. Puis, que le roi nous transmette sa volonté sur cet objet.

## Chapitre 6

### Recherche, découverte et confirmation de l'édit de Cyrus

6:1	Alors le roi Darius prit un décret et l'on fit des recherches dans la maison des livres où l'on déposait les trésors à Babel.
6:2	Et l'on trouva dans un coffre, au palais royal, qui était dans la province de Médie, un rouleau à l'intérieur duquel était écrit le mémoire suivant :
6:3	L'an un du roi Cyrus, le roi Cyrus prit un décret au sujet de la maison d’Élahh à Yeroushalaim : Que cette maison soit rebâtie, afin d'être un lieu où l'on offre des sacrifices, et que ses fondements soient solides pour porter sa charge. La hauteur sera de 60 coudées, et la longueur de 60 coudées,
6:4	trois rangées de pierres de taille et une rangée de bois neuf. Les frais seront payés par la maison du roi.
6:5	Et quant aux ustensiles d'or et d'argent de la maison d’Élahh, que Neboukadnetsar avait enlevés du temple qui est à Yeroushalaim et apportés à Babel, qu'on les rende et qu'ils soient remis au temple qui est à Yeroushalaim, chacun à sa place et qu'on le fasse conduire à la maison d’Élahh.
6:6	Maintenant, vous Thathnaï, gouverneur d’au-delà du fleuve, Shethar-Boznaï et vos collègues d'Apharcekay qui sont au-delà du fleuve, retirez-vous de là.
6:7	Laissez faire l'ouvrage de cette maison d’Élahh. Que le gouverneur des Juifs et leurs anciens rebâtissent cette maison d’Élahh à sa place.
6:8	Voici le décret que je prends concernant ce que vous aurez à faire avec les anciens de ces Juifs pour rebâtir cette maison d’Élahh : C'est sur la richesse du roi provenant des tributs d'au-delà du fleuve, les frais seront complètement payés à ces hommes, pour ne rien arrêter.
6:9	Et quant à ce qui sera nécessaire pour les holocaustes de l'Élahh des cieux, veaux, béliers et agneaux, blé, sel, vin et huile, ils seront livrés, sur leur demande, aux prêtres de Yeroushalaim, jour après jour, sans négligence,
6:10	afin qu'ils offrent des sacrifices apaisants<!--Da. 2:46.--> pour l'Élahh des cieux et qu'ils prient pour la vie du roi et de ses fils.
6:11	Je décrète encore que tout homme qui changera ce commandement, on arrachera de sa maison une pièce de bois, on la dressera, afin qu'il y soit exterminé, et l'on fera de sa maison un tas de déchets<!--Da. 3:29.-->.
6:12	Et que l'Élahh, qui fait résider en ce lieu son Nom, détruise tout roi et tout peuple qui étendrait sa main pour changer et détruire cette maison d’Élahh qui est à Yeroushalaim ! Moi, Darius, j'ai pris ce décret. Qu'il soit exécuté complètement.

### Reprise des travaux encouragée par Darius ; achèvement et dédicace du temple

6:13	Alors Thathnaï, gouverneur d'au-delà du fleuve, Shethar-Boznaï et leurs collègues, firent exécuter ainsi complètement ce que le roi Darius leur envoya.
6:14	Et les anciens des Juifs bâtirent avec succès, selon les prophéties de Chaggay, le prophète et de Zekaryah, fils d'Iddo. Ils bâtirent et finirent, d'après l'ordre de l'Élahh d'Israël, et d'après le décret de Cyrus, de Darius, et d'Artaxerxès, roi de Perse.
6:15	Cette maison fut achevée le troisième jour du mois d'Adar, dans la sixième année du règne du roi Darius.
6:16	Et les fils d'Israël, les prêtres, les Lévites, et le reste des fils de la captivité, firent la dédicace de cette maison d’Élahh avec joie.
6:17	Et ils offrirent pour la dédicace de cette maison d’Élahh, 100 taureaux, 200 béliers, 400 agneaux, et 12 boucs en offrandes pour le péché de tout Israël, selon le nombre des tribus d'Israël.
6:18	Ils établirent les prêtres selon leurs classes, les Lévites selon leurs divisions, pour le service d'Élahh à Yeroushalaim, selon ce qui est écrit dans le livre de Moshé<!--No. 3:6,32, 8:11.-->.

### Célébration de la Pâque et de la fête des pains sans levain

6:19	Les fils de la captivité firent la Pâque le quatorzième jour du premier mois<!--Lé. 23:5 ; No. 28:16 ; De. 16:2.-->.
6:20	Car les prêtres et les Lévites s'étaient purifiés comme un seul homme, tous étaient purs. C'est pourquoi ils immolèrent la Pâque pour tous les fils de la captivité, pour leurs frères les prêtres, et pour eux-mêmes<!--No. 9:11.-->.
6:21	Elle fut mangée par les fils d'Israël qui étaient revenus de la captivité, et par tous ceux qui s'étaient séparés de l'impureté des nations de la terre pour chercher YHWH, l'Elohîm d'Israël.
6:22	Et ils firent avec joie la fête des pains sans levain pendant sept jours, car YHWH les avait réjouis en disposant le cœur du roi d'Assyrie à fortifier leurs mains dans l'œuvre de la maison d'Elohîm, de l'Elohîm d'Israël.

## Chapitre 7

### Ezra se rend à Yeroushalaim (Jérusalem)

7:1	Après ces choses, sous le règne d'Artaxerxès, roi de Perse, Ezra, fils de Serayah, fils d'Azaryah, fils de Chilqiyah,
7:2	fils de Shalloum, fils de Tsadok, fils d'Ahitoub,
7:3	fils d'Amaryah, fils d'Azaryah, fils de Merayoth,
7:4	fils de Zerachyah, fils d'Ouzzi, fils de Bouqqi,
7:5	fils d'Abishoua, fils de Phinées, fils d'Èl’azar, fils d'Aaron, prêtre en tête,
7:6	lui, Ezra, qui était un scribe habile dans la torah de Moshé que YHWH l'Elohîm d'Israël avait donnée, monta de Babel et le roi lui accorda toute sa requête, selon que la main de YHWH son Elohîm était sur lui.
7:7	Quelques-uns aussi des fils d'Israël, des prêtres, des Lévites, des chanteurs, des portiers et des Néthiniens, montèrent à Yeroushalaim, la septième année du roi Artaxerxès.
7:8	Il vint à Yeroushalaim le cinquième mois de la septième année du roi,
7:9	car au premier jour du premier mois, on commença à partir de Babel et, au premier jour du cinquième mois, il vint à Yeroushalaim selon que la main de son Elohîm était bonne sur lui.
7:10	Car Ezra avait préparé son cœur à consulter la torah de YHWH, à l'observer et à enseigner les lois et les ordonnances parmi le peuple d'Israël.

### Ezra mandaté par Artaxerxès pour faire la volonté de YHWH

7:11	Voici la copie de la lettre que le roi Artaxerxès donna à Ezra, prêtre et scribe, enseignant les paroles des commandements de YHWH et ses ordonnances concernant Israël :
7:12	Artaxerxès, roi des rois, à Ezra, prêtre et scribe de la loi de l'Élahh des cieux, etc. Maintenant,
7:13	j'ai pris un décret afin que tous ceux de mon royaume qui sont du peuple d'Israël, de ses prêtres et Lévites, qui se présenteront volontairement pour aller à Yeroushalaim, aillent avec toi.
7:14	Parce que tu es envoyé de la part du roi et de ses sept conseillers, pour inspecter Yéhouda et Yeroushalaim concernant la loi de ton Élahh, laquelle est entre tes mains,
7:15	et pour porter l'argent et l'or que le roi et ses conseillers ont offert volontairement à l'Élahh d'Israël, dont la demeure est à Yeroushalaim,
7:16	et tout l'argent et l'or que tu trouveras dans toute la province de Babel, avec les offrandes volontaires du peuple et des prêtres, qu'ils feront volontairement à la maison de leur Élahh à Yeroushalaim.
7:17	Tu achèteras donc promptement, avec cet argent, des taureaux, des béliers, des agneaux, avec leurs offrandes et leurs libations, et tu les offriras sur l'autel de la maison de votre Élahh qui habite à Yeroushalaim.
7:18	Vous ferez, selon la volonté de votre Élahh, ce qu'il te semblera bon à toi et à tes frères de faire avec le reste de l'argent et de l'or.
7:19	Les ustensiles qui te sont remis pour le service de la maison de ton Élahh, dépose-les en face de l'Élahh de Yeroushalaim.
7:20	Quant au reste de ce qui sera nécessaire pour la maison de ton Élahh, autant que tu auras à en donner, tu le donneras de la maison des trésors du roi.
7:21	Moi, le roi Artaxerxès, je donne l'ordre<!--Le mot araméen signifie littéralement décret.--> à tous les trésoriers qui sont d'au-delà du fleuve, que tout ce qu'Ezra, prêtre et scribe de la loi de l'Élahh des cieux vous demandera soit fait avec zèle,
7:22	jusqu'à 100 talents d'argent, 100 cors de froment, 100 baths de vin, 100 baths d'huile, et du sel sans nombre.
7:23	Que tout ce qui est ordonné par l'Élahh des cieux soit promptement fait pour la maison de l'Élahh des cieux, de peur qu'il n'y ait de la colère contre le royaume et contre le roi et ses enfants.
7:24	De plus, nous vous faisons savoir qu'on ne pourra imposer ni tribut, ni impôt, ni droit de passage sur aucun des prêtres, des Lévites, des chanteurs, des portiers, des Néthiniens et des serviteurs de cette maison d’Élahh.
7:25	Quant à toi, Ezra, établis des magistrats et des juges selon la sagesse de ton Élahh que tu possèdes, afin qu'ils rendent justice à tout ce peuple d'au-delà du fleuve, à tous ceux qui connaissent les lois de ton Élahh, afin que vous enseigniez celui qui ne les connaît pas.
7:26	Et quant à tous ceux qui n'observeront pas la loi de ton Élahh et la loi du roi, que le jugement soit aussitôt exécuté sur eux, soit pour la mort, soit pour le bannissement, soit pour une amende sur sa richesse, ou pour l'emprisonnement.

### Ezra bénit YHWH

7:27	Béni soit YHWH, l'Elohîm de nos pères, qui a mis cela au cœur du roi, pour honorer la maison de YHWH, qui habite à Yeroushalaim,
7:28	et qui a fait que j'ai trouvé grâce devant le roi, devant ses conseillers et devant tous les puissants chefs ! Fortifié par la main de YHWH, mon Elohîm, qui était sur moi, j'ai rassemblé les têtes d'Israël, afin qu'elles montent avec moi.

## Chapitre 8

### Dénombrement des Juifs montés de Babel avec Ezra

8:1	Voici les têtes des pères, avec le dénombrement fait selon les généalogies de ceux qui montèrent avec moi de Babel, pendant le règne du roi Artaxerxès.
8:2	Des fils de Phinées, Guershom ; des fils d'Ithamar, Daniye'l ; des fils de David, Hattoush ;
8:3	des fils de Shekanyah ; des fils de Pareosh, Zekaryah, et avec lui, en faisant le dénombrement par leur généalogie selon les hommes, 150 hommes ;
8:4	des fils de Pachath-Moab, Élyehow`eynay, fils de Zerachyah, et avec lui 200 hommes ;
8:5	des fils de Shekanyah, le fils de Yachaziy'el, et avec lui 300 hommes ;
8:6	des fils d'Adin, Ébed, fils de Yonathan, et avec lui 50 hommes ;
8:7	des fils d'Éylam, Yesha`yah, fils d'Athalyah, et avec lui 70 hommes ;
8:8	des fils de Shephatyah, Zebadyah, fils de Miyka'el, et avec lui 80 hommes ;
8:9	des fils de Yoab, Obadyah, fils de Yechiy'el, et avec lui 218 hommes ;
8:10	des fils de Shelomiyth, le fils de Yosiphyah, et avec lui 160 hommes ;
8:11	des fils de Bébaï, Zekaryah, fils de Bébaï, et avec lui 28 hommes ;
8:12	des fils d'Azgad, Yohanan, fils d'Hakkathan, et avec lui 110 hommes ;
8:13	des fils d'Adonikam, les derniers, dont voici les noms : Éliyphelet, Yéiël et Shema’yah, et avec eux, 60 hommes ;
8:14	des fils de Bigvaï, Outaï, Zabboud, et avec eux 70 hommes.
8:15	Et je les rassemblai près du fleuve qui coule vers Ahava, et nous campâmes là trois jours. Puis je portai mon attention sur le peuple et les prêtres, et je n'y trouvai aucun des fils de Lévi.
8:16	J'envoyai donc Éliy`ezer, Ariel, Shema’yah, Elnathan, Yarib, Elnathan, Nathan, Zekaryah, Meshoullam, les têtes, avec les docteurs Yoyariyb et Elnathan.
8:17	Je leur donnai des ordres pour Iddo, la tête, demeurant à Kaciphya, et je mis dans leur bouche les paroles qu'ils devaient dire à Iddo et à ses frères les Néthiniens, qui étaient à Kaciphya, afin qu'ils nous amènent des serviteurs pour la maison de notre Elohîm<!--Esd. 2:43.-->.
8:18	Et ils nous amenèrent, selon que la main de notre Elohîm était bonne sur nous, un homme intelligent, d'entre les fils de Machli, fils de Lévi, fils d'Israël : Sherebyah, avec ses fils et ses frères, au nombre de 18<!--Esd. 7:6,9,28.--> ;
8:19	Chashabyah, et avec lui Yesha`yah, d'entre les fils de Merari, ses frères, et leurs fils, au nombre de 20 ;
8:20	et des Néthiniens, que David et les chefs du peuple avaient assignés pour le service des Lévites, 220 Néthiniens, tous désignés par leur nom<!--Esd. 2:58.-->.

### Ezra et le peuple s'humilient et prient Elohîm

8:21	Et là, près du fleuve d'Ahava, je proclamai un jeûne pour que nous nous humiliions devant notre Elohîm, afin de lui demander la route droite pour nous, pour nos enfants et pour tous nos biens.
8:22	Car j'aurais eu honte de demander au roi une armée et des cavaliers pour nous soutenir contre des ennemis pendant le chemin, car nous avions dit au roi : La main de notre Elohîm est favorable sur tous ceux qui le cherchent ; mais sa force et sa colère sont contre ceux qui l'abandonnent.
8:23	Nous avons jeûné et recherché notre Elohîm à cause de cela. Et il s’est laissé implorer par nous.

### Ezra établit douze prêtres et leur confie les trésors

8:24	Je mis à part douze chefs des prêtres, Sherebyah, Chashabyah, et dix de leurs frères.
8:25	Je pesai l'argent, l'or et les ustensiles donnés en offrandes pour la maison de notre Elohîm par le roi, ses conseillers, ses chefs et tous ceux d'Israël qu'on avait trouvés<!--Esd. 7:14-15.-->.
8:26	Je pesai en leurs mains 650 talents d’argent, des ustensiles d’argent de 100 talents, 100 talents d’or, 
8:27	20 bols d'or : 1 000 drachmes, et deux ustensiles d'un beau cuivre poli, aussi précieux que de l'or.
8:28	Je leur dis : Vous êtes consacrés à YHWH, et les ustensiles sont consacrés, et cet argent et cet or sont une offrande volontaire faite à YHWH, l'Elohîm de vos pères.
8:29	Soyez vigilants et gardez-les, jusqu'à ce que vous les pesiez devant les chefs des prêtres et les Lévites, et devant les chefs des pères d'Israël, à Yeroushalaim, dans les chambres de la maison de YHWH.
8:30	Les prêtres et les Lévites reçurent le poids de l'argent, de l'or et des ustensiles, pour les apporter à Yeroushalaim, dans la maison de notre Elohîm.

### Ezra et ses compagnons arrivent à Yeroushalaim (Jérusalem)

8:31	Nous avons quitté le fleuve d'Ahava pour aller à Yeroushalaim, le douzième jour du premier mois. La main de notre Elohîm fut sur nous et nous délivra de la paume des ennemis et des embûches sur le chemin.
8:32	Arrivés à Yeroushalaim, nous y sommes restés trois jours.
8:33	Le quatrième jour, nous avons pesé l'argent, l'or et les ustensiles dans la maison de notre Elohîm, entre les mains de Merémoth, fils d'Ouriyah, le prêtre ; il était avec Èl’azar, fils de Phinées, et avec eux, les Lévites, Yozabad, fils de Yéshoua et No`adyah, fils de Binnouï.
8:34	Selon tout le nombre et le poids de toutes ces choses, et tout le poids fut mis alors par écrit.
8:35	Et les fils de la captivité revenus de l'exil, présentèrent en holocauste à l'Elohîm d'Israël 12 taureaux, 96 béliers, 77 agneaux et 12 boucs pour le péché de tout Israël, le tout en holocauste à YHWH.
8:36	Ils donnèrent les décrets du roi aux satrapes du roi et aux gouverneurs de l’autre côté du fleuve, et ceux-ci soutinrent le peuple et la maison d’Elohîm.

## Chapitre 9

### Infidélité du peuple envers YHWH

9:1	Dès que ces choses furent achevées, les chefs du peuple s'approchèrent de moi, en disant : Le peuple d'Israël, les prêtres et les Lévites, ne se sont pas séparés des peuples de ces terres quant à leurs abominations, celles des Kena'ânéens<!--Cananéens.-->, des Héthiens, des Phéréziens, des Yebousiens, des Ammonites, des Moabites, des Égyptiens et des Amoréens.
9:2	Car ils ont pris de leurs filles pour eux et pour leurs fils, et ont mêlé la semence sainte avec les peuples de ces terres. Et la main des chefs et des dirigeants a été la première à commettre ce péché<!--Né. 13:23.-->.
9:3	Aussitôt que j'entendis cela, je déchirai mes vêtements et ma robe, j'arrachai les cheveux de ma tête et ma barbe, et je m'assis tout épouvanté.
9:4	Et tous ceux qui tremblaient aux paroles de l'Elohîm d'Israël, se rassemblèrent auprès de moi, à cause de l'infidélité de ceux de la captivité. Je restai assis tout épouvanté jusqu'à l'offrande du soir.

### Prière et confession d'Ezra

9:5	Et au temps de l'offrande du soir, je me levai du sein de mon affliction, et ayant mes vêtements et ma robe déchirés, je me mis à genoux, et j'étendis mes paumes vers YHWH, mon Elohîm,
9:6	et je dis : Mon Elohîm ! J'ai honte, et je suis trop confus, mon Elohîm, pour lever ma face vers toi, car nos iniquités se sont multipliées au-dessus de nos têtes, et notre péché s'est élevé jusqu'aux cieux.
9:7	Depuis les jours de nos pères jusqu'à ce jour, nous sommes grandement coupables, et c'est à cause de nos iniquités que nous avons été livrés, nous, nos rois et nos prêtres entre les mains des rois des terres, à l'épée, à la captivité, au pillage, et à la honte, comme il paraît aujourd'hui.
9:8	Et maintenant YHWH, notre Elohîm, nous a fait grâce depuis peu de temps en nous laissant quelques rescapés et en nous donnant un piquet dans son saint lieu, afin que notre Elohîm soit la lumière de nos yeux et nous rende un peu de vie dans notre servitude.
9:9	Car nous sommes esclaves, mais notre Elohîm ne nous a pas abandonnés dans notre esclavage. Il nous a accordé de la faveur devant les rois de Perse, pour nous rendre la vie, afin que nous relevions la maison de notre Elohîm et que nous redressions ses désolations, et pour nous donner une clôture en Yéhouda et à Yeroushalaim.
9:10	Mais maintenant, notre Elohîm ! Que dirons-nous après ces choses ? Car nous avons abandonné tes commandements,
9:11	que tu as ordonnés par la main de tes serviteurs les prophètes, en disant : La terre dans laquelle vous entrez pour en prendre possession est une terre souillée par les impuretés des peuples de ces terres, à cause des abominations dont ils l'ont remplie d'un bout à l'autre par leurs impuretés<!--Lé. 18:25-27.-->.
9:12	Maintenant, ne donnez pas vos filles à leurs fils et ne prenez pas leurs filles pour vos fils, ne cherchez jamais ni leur bonheur, ni leur paix, ainsi vous deviendrez forts, vous mangerez les meilleures productions de la terre, et vous la laisserez hériter à vos fils pour toujours<!--De. 7:3.-->.
9:13	Or après toutes les choses qui nous sont arrivées à cause de nos mauvaises œuvres et de notre grande culpabilité, mais toi, notre Elohîm, tu nous as épargnés au-dessous de nos iniquités et tu nous as donné une telle délivrance.
9:14	Recommencerions-nous à violer tes commandements et à faire alliance avec ces peuples abominables ? Ta colère n'éclaterait-elle pas contre nous jusqu'à nous exterminer, sans aucun reste ni aucun rescapé ?
9:15	YHWH, Elohîm d'Israël ! Tu es juste, car nous sommes aujourd'hui un reste de rescapés. Nous voici en face de toi dans notre culpabilité, car, à cause de cela, on ne peut se tenir en face de toi.

## Chapitre 10

### Le peuple appelé à se confesser et se séparer

10:1	Tandis qu'Ezra priait et faisait la confession en pleurant et en tombant en face de la maison d’Elohîm, une grande multitude d'hommes, de femmes et de fils d'Israël s'assembla auprès de lui, et le peuple se lamenta abondamment par des pleurs.
10:2	Shekanyah, fils de Yechiy'el, d'entre les fils d'Éylam, répondit et dit à Ezra : Nous avons commis un délit contre notre Elohîm, en faisant habiter chez nous des femmes étrangères d'entre les peuples de cette terre. Mais Israël ne reste pas pour cela sans espérance<!--De. 7:22-23.-->.
10:3	Maintenant, traitons une alliance avec notre Elohîm, en faisant sortir toutes ces femmes et ceux qui sont nés d’elles, selon le conseil du seigneur et de ceux qui tremblent au commandement de notre Elohîm. Que l'on agisse selon la torah<!--Esd. 9:4 ; Mal. 3:16.--> !
10:4	Lève-toi, car cette affaire te regarde et nous serons avec toi. Fortifie-toi et agis !
10:5	Ezra se leva et fit jurer aux chefs des prêtres des Lévites et de tout Israël d'agir selon cette parole. Et ils le jurèrent.
10:6	Ezra se leva en face de la maison d’Elohîm et s'en alla dans la chambre de Yehohanan, fils d'Élyashiyb. Et quand il y fut entré, il ne mangea pas de pain, ne but pas d'eau, parce qu'il se lamentait à cause du péché de ceux de la captivité.
10:7	On fit passer une voix dans Yéhouda et dans Yeroushalaim, pour que tous les fils de la captivité se rassemblent à Yeroushalaim,
10:8	et que quiconque ne s'y rendrait pas dans trois jours, selon l'avis des chefs et des anciens, aurait tous ses biens complètement détruits, et que lui-même serait séparé de l'assemblée de ceux de la captivité.
10:9	Ainsi tous ceux de Yéhouda et de Benyamin se rassemblèrent à Yeroushalaim dans les trois jours. C'était le vingtième jour du neuvième mois. Tout le peuple se tenait sur la place de la maison d’Elohîm, tremblant au sujet de cette affaire et à cause des pluies<!--1 S. 12:18.-->.
10:10	Ezra, le prêtre, se leva et leur dit : Vous avez commis un délit en faisant habiter chez vous des femmes étrangères, pour ajouter à la culpabilité d’Israël<!--De. 7:3.-->.
10:11	Mais maintenant faites confession à YHWH, l'Elohîm de vos pères et faites sa volonté ! Séparez-vous des peuples de la terre et des femmes étrangères.
10:12	Et toute l'assemblée répondit, et dit à grande voix : Oui, c’est à nous de faire selon ta parole !

### Les anciens et les chefs établis pour juger les affaires

10:13	Mais le peuple est nombreux et c'est le temps des pluies, et nous n'avons pas la force de rester dehors. D'ailleurs, ce n'est pas une affaire d'un jour, ni de deux, car nous sommes nombreux à avoir péché dans cette affaire.
10:14	S’il te plaît, que nos chefs se tiennent là pour toute l’assemblée et que tous ceux qui, dans nos villes, ont pris des femmes étrangères viennent aux temps fixés avec les anciens de chaque ville et ses juges, jusqu'à ce que nous détournions de nous l'ardeur de la colère de notre Elohîm, au sujet de cette affaire.
10:15	Yonathan, fils d'Asaël et Yahzeyah, fils de Tiqvah se dressèrent contre cette affaire, et Meshoullam et Shabthaï, Lévites, les aidèrent.
10:16	Mais les fils de la captivité firent comme on avait dit. On choisit Ezra, le prêtre, et les têtes des pères selon les maisons de leurs pères, et tous par leur nom. Ils s’assirent le premier jour du dixième mois pour étudier cette affaire.
10:17	Et le premier jour du premier mois, ils en finirent avec tous ceux qui avaient pris chez eux des femmes étrangères.
10:18	Or quant aux fils des prêtres qui avaient pris chez eux des femmes étrangères, il se trouva d'entre les fils de Yéshoua, fils de Yotsadaq et de ses frères, Ma`aseyah, Éliy`ezer, Yarib et Gedalyah,
10:19	qui, en donnant leurs mains, firent sortir leurs femmes et, comme coupables, ils offrirent pour leur délit un bélier du troupeau.
10:20	Des fils d'Immer, Chananiy et Zebadyah ;
10:21	des fils de Harim, Ma`aseyah, Éliyah, Shema’yah, Yechiy'el et Ouzyah ;
10:22	des fils de Pashhour, Élyehow`eynay, Ma`aseyah, Yishmael, Netanél, Yozabad et El`asah.
10:23	Parmi les Lévites : Yozabad, Shimeï, Qelayah (ou Qeliyta), Pethachyah, Yéhouda et Éliy`ezer.
10:24	Parmi les chanteurs : Élyashiyb. Et des portiers : Shalloum, Thélem et Ouri.
10:25	Parmi ceux d'Israël : des fils de Pareosh, Ramyah, Yizziyah, Malkiyah, Miyamin, Èl’azar, Malkiyah et Benayah ;
10:26	des fils d'Éylam, Mattanyah, Zekaryah, Yechiy'el, Abdi, Yeriymoth et Éliyah ;
10:27	des fils de Zatthou, Élyehow`eynay, Élyashiyb, Mattanyah, Yeriymoth, Zabad et Aziyza ;
10:28	des fils de Bébaï, Yehohanan, Chananyah, Zabbay et Athlaï ;
10:29	des fils de Bani, Meshoullam, Mallouk, Adayah, Yashoub, Sheal et Ramoth ;
10:30	des fils de Pachath-Moab, Adna, Kelal, Benayah, Ma`aseyah, Mattanyah, Betsaleel, Binnouï et Menashè ;
10:31	des fils de Harim, Éliy`ezer, Yishshiyah, Malkiyah, Shema’yah, Shim’ôn,
10:32	Benyamin, Mallouk, Shemaryah ;
10:33	des fils de Hashoum, Matthnaï, Mattattah, Zabad, Éliyphelet, Yerémaï, Menashè et Shimeï ;
10:34	des fils de Bani, Maadaï, Amram, Ouël,
10:35	Benayah, Bedeyeah, Kelouhou,
10:36	Vanyah, Merémoth, Élyashiyb,
10:37	Mattanyah, Matthnaï, Yaasaï,
10:38	Bani, Binnouï, Shimeï,
10:39	Shelemyah, Nathan, Adayah,
10:40	Macnadbaï, Shashaï, Sharaï,
10:41	Azareel, Shelemyah, Shemaryah,
10:42	Shalloum, Amaryah et Yossef ;
10:43	des fils de Nebo, Yéiël, Mattithyah, Zabad, Zebiyna, Yaddaï, Yoel et Benayah.
10:44	Tous ceux-là avaient pris des femmes étrangères, et il y avait quelques-uns d'entre eux qui avaient eu des enfants de ces femmes-là.
