# Hébreux (Hé.)

Auteur : Inconnu

Thème : La prêtrise du Mashiah

Date de rédaction : Env. 68 ap. J.-C.

Cette lettre fut rédigée avant la destruction de Yeroushalaim (Jérusalem), car le temple y subsistait encore. Elle s'adressait à des Juifs convertis connaissant bien l'auteur. Parmi eux, certains étaient tentés de retourner au judaïsme à cause des persécutions. L'auteur désire affermir ces chrétiens en leur montrant que l'objectif de la loi avait été réalisé par le Mashiah (Christ), qui est supérieur aux anges, aux prophètes et à Moshé (Moïse). Il leur montre combien son œuvre rédemptrice est parfaite et les invite à suivre le Seigneur avec une foi indéfectible, en persévérant dans l'amour fraternel.

## Chapitre 1

### Elohîm parle par le Fils

1:1	Elohîm ayant autrefois parlé aux pères, à plusieurs reprises et de plusieurs manières par les prophètes,
1:2	nous a parlé en ces derniers jours<!--Les derniers jours ont commencé avec la naissance de l'Assemblée. Voir Joë. 3:1 ; Ac. 2:14-17.--> par le Fils, qu'il a établi héritier de toutes choses, par le moyen duquel aussi il a fait les âges.
1:3	Lequel, étant la splendeur de sa gloire et l'empreinte de son être, et soutenant toutes choses par la parole de sa puissance, ayant fait par lui-même la purification de nos péchés, s'est assis à la droite de la Majesté divine dans les hauteurs.

### Le Fils, supérieur aux anges

1:4	Étant devenu d'autant plus excellent que les anges, que le nom qu'il a reçu en héritage est supérieur au leur.
1:5	Car auquel des anges a-t-il jamais dit : Tu es mon Fils, je t'ai engendré aujourd'hui<!--Ps. 2:7.--> ? Et encore : Je serai pour lui un Père et il sera pour moi un Fils<!--2 S. 7:14.--> ?
1:6	Et de nouveau, quand il introduit le premier-né<!--Voir commentaire en Col. 1:15.--> dans la terre habitée, il dit : Que tous les anges d'Elohîm l'adorent<!--Ps. 97:7.--> !
1:7	Et quant aux anges, il dit en effet : Il fait de ses anges des vents, et de ses serviteurs une flamme de feu<!--Ps. 104:4.-->.
1:8	Mais envers le Fils : Ton trône, Elohîm, subsiste d'âge en âge<!--Voir Ps. 45:7.--> ! Le sceptre de ton règne est le sceptre de justice.
1:9	Tu as aimé la justice et tu as haï la violation de la torah. C'est pourquoi Elohîm, ton Elohîm t'a oint d'une huile d'allégresse au-dessus de tes associés<!--Ps. 45:7-8.--> !
1:10	Et : Toi, dans les commencements, Seigneur, tu as fondé la Terre, et les cieux sont les œuvres de tes mains.
1:11	Ils disparaîtront, mais toi, tu restes d'une façon permanente. Et ils vieilliront tous comme un vêtement.
1:12	Et tu les plieras en rouleau comme un habit, et ils seront changés. Mais toi, tu es le même et tes années ne cesseront pas<!--Es. 50:9, 51:6 ; Ps. 102:27-28.-->.
1:13	Et auquel des anges a-t-il jamais dit : Assieds-toi à ma droite, jusqu'à ce que j'aie mis tes ennemis pour le marchepied de tes pieds<!--Ps. 110:1.--> ?
1:14	Ne sont-ils pas tous des esprits relatifs à la réalisation d'un service, envoyés pour exercer un service en faveur de ceux qui doivent hériter du salut ?

## Chapitre 2

2:1	C'est pourquoi, il faut nous attacher plus sérieusement aux choses que nous avons entendues, de peur que nous ne soyons emportés au loin<!--Vient du grec 'pararrhueo' qui signifie : "glisser", "de peur que nous ne soyons emportés", "passer par", "de peur que le salut que ces choses entendues nous montrent comment l'obtenir soit emporté loin de nous", "une chose m'échappe", "échappe à mon esprit".-->.
2:2	Car, si la parole prononcée par le moyen des anges était devenue ferme, et si toute transgression et désobéissance a reçu une juste rémunération,
2:3	comment échapperons-nous si nous négligeons un si grand salut qui, annoncé au commencement par le moyen du Seigneur, nous a été confirmé par ceux qui l’avaient entendu ?
2:4	Elohîm confirmant leur témoignage par des signes, et des prodiges, et divers miracles et distributions de l'Esprit Saint, selon sa propre volonté.
2:5	Car, ce n'est pas aux anges qu'il a soumis la terre habitée à venir dont nous parlons.
2:6	Et quelqu'un a rendu ce témoignage quelque part en disant : Qu'est-ce que l'humain pour que tu te souviennes de lui, ou le fils d'humain pour que tu le visites ?
2:7	Tu l'as rendu pour un peu de temps inférieur aux anges, tu l'as couronné de gloire et d'honneur, et tu l'as établi sur les œuvres de tes mains.
2:8	Tu as soumis toutes choses sous ses pieds<!--Ps. 8:5-7.-->. Car, en lui soumettant toutes choses, il n'a rien laissé qui ne lui soit assujetti. Or maintenant nous ne voyons pas encore que toutes choses lui soient soumises.
2:9	Mais celui qui a été rendu pour un peu de temps inférieur aux anges, Yéhoshoua, nous le voyons couronné de gloire et d'honneur, à cause de la mort qu'il a soufferte, afin que, par la grâce d'Elohîm, il goûtât la mort pour tout le monde.
2:10	Car il était convenable que celui pour qui sont toutes choses et par le moyen duquel sont toutes choses, conduisant beaucoup de fils à la gloire, rendît parfait le Prince de leur salut par le moyen des souffrances.
2:11	Car, et celui qui sanctifie, et ceux qui sont sanctifiés, viennent tous d'un seul. C'est pourquoi il n'a pas honte de les appeler ses frères,
2:12	en disant : Je déclarerai ton Nom à mes frères et je te chanterai des hymnes pascals au milieu de l'assemblée<!--Ps. 22:23. Voir Jn. 17:6,26.-->.
2:13	Et encore : Je mettrai ma confiance en lui. Et encore : Me voici, moi et les enfants qu'Elohîm m'a donnés<!--Es. 8:17-18.-->.
2:14	Ainsi donc, puisque les enfants participent à la chair et au sang, lui aussi, pareillement, a participé aux mêmes choses, afin que, par le moyen de la mort, il privât de pouvoir<!--« Rendre vain », « inemployé », « inactif », « inopérant », « faire cesser », « amener à une fin », « annuler », « abolir ».--> celui qui a la force souveraine de la mort, c'est-à-dire le diable,
2:15	et qu'il délivrât tous ceux qui, par peur de la mort, étaient tenus en esclavage toute leur vie.
2:16	Car ce ne sont pas les anges qu'il prend véritablement en charge, mais il prend en charge la postérité d'Abraham.
2:17	C'est pourquoi il a fallu qu'il soit semblable en toutes choses aux frères, afin de devenir le Grand-Prêtre miséricordieux et fidèle près d'Elohîm, pour faire la propitiation pour les péchés du peuple.
2:18	Car du fait qu’il a souffert lui-même lorsqu'il a été tenté, il peut secourir ceux qui sont tentés.

## Chapitre 3

### Le Mashiah (Christ), supérieur à Moshé (Moïse)

3:1	C'est pourquoi, frères saints, participants de la vocation céleste, considérez attentivement l'apôtre et le grand-prêtre de notre profession, Yéhoshoua Mashiah.
3:2	Il est fidèle à celui qui l’a fait, comme aussi Moshé dans toute sa maison.
3:3	Car celui-là a été jugé digne d'une gloire supérieure à celle de Moshé, dans la mesure où celui qui a construit une maison a plus d'honneur que la maison elle-même.
3:4	Car chaque maison est construite par quelqu'un, mais celui qui a construit toutes choses, c'est Elohîm.
3:5	Et Moshé a vraiment été fidèle dans toute sa maison comme serviteur d’Elohîm, en témoignage des choses qui seront annoncées.
3:6	Mais Mashiah l’est comme Fils sur sa maison. Et nous sommes sa maison<!--L'Assemblée véritable est la maison d'Elohîm. Voir Es. 66:1 ; 1 Co. 3:16, 6:19 ; Ep. 2:21-22. Les bâtiments ne sont pas la maison d'Elohîm. Le premier bâtiment d'assemblée avait été édifié par des fidèles sous le règne d'Alexandre Sévère en 222-235. L'Assemblée véritable est composée de pierres vivantes qui ont pour fondement le Roc (Yéhoshoua), parce qu'elle est bâtie par Yéhoshoua Mashiah (Jésus-Christ) lui-même et qu'elle est sa propriété. Les démons ne peuvent pas la détruire. L'Assemblée véritable ne peut donc être confondue avec un bâtiment ou une maison physique.-->, pourvu que nous retenions jusqu'à la fin la ferme assurance et l'espérance qui est notre sujet de gloire.
3:7	C'est pourquoi, comme dit le Saint-Esprit : Aujourd'hui, si vous entendez sa voix,
3:8	n'endurcissez pas vos cœurs comme lors de la provocation, le jour de la tentation dans le désert,
3:9	où vos pères me tentèrent, m'éprouvèrent et virent mes œuvres pendant 40 ans<!--Ps. 95:8-11.-->.
3:10	C'est pourquoi je fus irrité contre cette génération, et je dis : Ils s’égarent sans cesse dans leur cœur et ils n’ont pas connu mes voies.
3:11	Aussi, je jurai dans ma colère : On verra bien s’ils entreront dans mon repos !
3:12	Prenez garde, frères, qu'il n'y ait peut-être en quelqu'un de vous un mauvais cœur d'incrédulité qui s'éloigne de l'Elohîm vivant,
3:13	mais exhortez-vous les uns les autres chaque jour, aussi longtemps qu'il est dit : « Aujourd'hui » de peur que quelqu'un d'entre vous ne s'endurcisse par la séduction du péché.
3:14	Car nous sommes devenus participants du Mashiah, pourvu que nous retenions ferme jusqu'à la fin notre première assurance,
3:15	en ce qui a été dit : Aujourd'hui, si vous entendez sa voix, n'endurcissez pas vos cœurs, comme lors de la provocation.
3:16	Car qui sont ceux qui, après avoir entendu, le provoquèrent ? Mais n’étaient-ce tous ceux qui sont sortis d'Égypte par le moyen de Moshé ?
3:17	Mais contre qui s’irrita-t-il pendant 40 ans ? N’est-ce pas contre ceux qui péchèrent et dont les cadavres tombèrent dans le désert ?
3:18	Mais à qui jura-t-il qu'ils n’entreront pas dans son repos, sinon à ceux qui furent rebelles ?
3:19	Et nous voyons qu'ils ne purent y entrer à cause de l'incrédulité.

## Chapitre 4

### Le repos

4:1	Craignons donc, tandis que la promesse d'entrer dans son repos nous est laissée, que l'un de vous ne semble être resté en arrière.
4:2	Car nous aussi, nous avons été évangélisés de même que ceux-là. Mais la parole entendue ne leur a servi à rien, parce qu'ils n'étaient pas mêlés par la foi avec ceux qui l'entendirent.
4:3	Car pour nous qui avons cru, nous entrons dans le repos, selon qu'il dit : Comme j'ai juré dans ma colère : On verra bien s’ils entreront dans mon repos<!--Hé. 3:11.--> ! Il dit cela quoique ses œuvres aient été achevées depuis la fondation du monde.
4:4	Car il a parlé quelque part ainsi du septième jour : Et Elohîm se reposa de toutes ses œuvres le septième jour<!--Ge. 2:2.-->.
4:5	Et de nouveau dans ce passage : On verra bien s’ils entreront dans mon repos !
4:6	Donc, puisqu'il reste à quelques-uns d'y entrer, et que ceux qui premièrement avaient été évangélisés n'y sont pas entrés à cause de leur obstination,
4:7	il détermine de nouveau un certain jour : « Aujourd'hui », en disant bien longtemps après, par David, comme il a été dit plus haut : Aujourd'hui, si vous entendez sa voix, n'endurcissez pas vos cœurs<!--Ps. 95:8-11.-->.
4:8	Car, si Yéhoshoua<!--Josué.--> les avait introduits dans le repos, jamais après cela il n'aurait parlé d'un autre jour.
4:9	Il reste donc un repos shabbatique pour le peuple d'Elohîm.
4:10	Car celui qui est entré dans son repos, se repose aussi lui-même de ses œuvres<!--Ap. 14:12.-->, comme Elohîm des siennes.
4:11	Efforçons-nous donc d'entrer dans ce repos-là, de peur que quelqu'un ne tombe dans le même exemple d'obstination.
4:12	Car la parole d'Elohîm est vivante et efficace, et plus pénétrante qu'aucune épée à deux tranchants, et perçant jusqu'à la division de l'âme et de l'esprit, et des jointures et des moelles. Et elle juge les pensées et les intentions du cœur.
4:13	Et aucune créature n'est cachée devant lui, mais toutes choses sont nues et entièrement découvertes aux yeux de celui à qui nous avons affaire.
4:14	Ayant donc un souverain<!--Souverain prêtre, chef prêtre Celui qui était honoré du titre de souverain prêtre. Il devait remplir les charges de sa fonction, mais son rôle principal était, une fois par an, le jour de l'expiation, d'entrer dans le Saint des Saints, et d'y offrir des sacrifices pour ses propres péchés, et pour ceux du peuple. Il présidait le Sanhédrin, conseil suprême. Selon la Torah de Moshé, nul ne pouvait aspirer à ce poste s'il n'était de la tribu d'Aaron et s'il ne descendait d'une famille de souverains prêtres. La fonction était conservée jusqu'à la mort. Au temps d'Antiochus Epiphane, les princes Hérodiens et les Romains s'arrogèrent le pouvoir des souverains prêtres, et la fonction ne fut plus héréditaire ni à vie. Pendant les 107 ans qui séparent Hérode le Grand et la destruction de la ville sainte, 28 personnes ont reçu cette dignité.--> grand-prêtre qui a traversé les cieux, Yéhoshoua, le Fils d'Elohîm, retenons notre profession.
4:15	Car nous n'avons pas un grand-prêtre incapable de compatir à nos faiblesses, lui qui a été tenté en toutes choses d'une manière semblable, mais sans pécher.
4:16	Approchons-nous donc avec assurance du trône de la grâce, afin d'obtenir miséricorde et de trouver grâce, pour un secours opportun.

## Chapitre 5

### Le service du grand-prêtre

5:1	Car tout grand-prêtre pris parmi les humains est établi pour les humains près d'Elohîm, afin de présenter des dons et des sacrifices pour les péchés,
5:2	il peut être indulgent pour les ignorants et les égarés, parce qu'il est lui-même entouré de faiblesse.
5:3	Et c'est à cause de cela qu’il doit apporter, pour lui-même aussi bien que pour le peuple, un présent pour les péchés.
5:4	Et nul ne s'approprie de lui-même cet honneur, excepté celui qui est appelé d'Elohîm comme le fut Aaron.

### Le Mashiah (Christ), grand-prêtre selon l'ordre de Malkiy-Tsédeq (Melchisédek)

5:5	De même aussi, le Mashiah ne s'est pas glorifié lui-même d'être fait grand-prêtre, mais par celui qui a dit : C'est toi qui es mon Fils, je t'ai engendré aujourd'hui<!--Ps. 2:7.--> !
5:6	Comme il lui dit aussi dans un autre endroit : Tu es prêtre pour l’éternité, selon l'ordre de Malkiy-Tsédeq<!--Ps. 110:4.-->.
5:7	C'est lui qui, pendant les jours de sa chair, a offert avec de grands cris et avec larmes, des prières et des supplications à celui qui pouvait le sauver de la mort, et il a été exaucé à cause de sa piété.
5:8	Bien qu'étant Fils, il a pourtant appris l'obéissance par les choses qu'il a souffertes.
5:9	Et ayant été rendu parfait, il est devenu l'auteur du salut éternel<!--Es. 45:17.--> pour tous ceux qui lui obéissent,
5:10	étant appelé d'Elohîm à être grand-prêtre selon l'ordre de Malkiy-Tsédeq.
5:11	Sur ce sujet, nous avons un grand discours et difficile à expliquer, parce que vous êtes devenus paresseux d'oreilles.

### Les enfants et les hommes parfaits<!--Jusqu'à Hé. 6:12.-->

5:12	Car vous qui devriez aussi être des docteurs, en raison du temps, vous avez encore besoin qu'on vous enseigne les premiers rudiments des oracles d'Elohîm, et vous êtes devenus tels, que vous avez encore besoin de lait et non de nourriture solide.
5:13	Car quiconque participe au lait est inexpérimenté dans la parole de la justice, car il est un enfant<!--Le mot enfant dans ce passage vient du grec « nepios » qui signifie « ignorant ».-->.
5:14	Mais la nourriture solide est pour les hommes parfaits<!--Vient du grec « teleois » qui signifie « amené à ses fins », « accompli », « ce qui est parfait », « fait », « adulte », « âge mûr ». Voir Mt. 5:48 ; Ep. 4:13.-->, pour ceux qui, par l'habitude, ont les facultés de perception exercées à distinguer le bien et le mal.

## Chapitre 6

6:1	C'est pourquoi, laissant la parole du commencement du Mashiah, avançons vers la perfection, ne posant pas de nouveau le fondement de la repentance des œuvres mortes, et de la foi en Elohîm,
6:2	de la doctrine des lavages<!--Vient du grec « baptismos » qui signifie : « lavage ou purification effectuée au moyen de l'eau ».--> effectués au moyen de l'eau, et de l'imposition des mains, et de la résurrection des morts, et du jugement éternel.
6:3	Et c'est ce que nous ferons, si Elohîm le permet.
6:4	Car il est impossible, quant à ceux qui ont été une fois illuminés<!--Ep. 1:18.-->, et qui ont goûté le don céleste, et qui sont devenus participants de l'Esprit Saint,
6:5	et qui ont goûté la bonne parole d'Elohîm et les puissances de l'âge à venir,
6:6	et qui sont tombés<!--Vient d'un mot grec qui signifie « tomber à côté d'une personne ou d'une chose » ; « glisser de côté », « dévier du droit chemin, se détourner, errer ».-->, de les renouveler de nouveau pour la repentance, eux qui, pour eux-mêmes, crucifient de nouveau le Fils d'Elohîm et l'exposent à une disgrâce publique.
6:7	Car la terre qui boit souvent la pluie qui vient sur elle et qui produit<!--Le mot grec signifie aussi « enfanter ». Voir Mt. 1:21.--> des herbes utiles à ceux par qui elle est cultivée, participe à la bénédiction d'Elohîm.
6:8	Mais si elle porte des épines et des chardons, elle est réprouvée et proche de malédiction, et sa fin est d'être brûlée.
6:9	Mais nous sommes persuadés, en ce qui vous concerne, bien-aimés, de choses meilleures et qui tiennent au salut, quoique nous parlions ainsi.
6:10	Car Elohîm n'est pas injuste pour oublier votre œuvre et le travail d'amour que vous avez démontré pour son Nom, ayant servi et servant les saints.
6:11	Or, nous souhaitons que chacun de vous montre jusqu'à la fin, le même empressement pour la pleine certitude de l'espérance,
6:12	afin que vous ne deveniez pas paresseux, mais des imitateurs de ceux qui, par le moyen de la foi et de la patience, héritent des promesses.
6:13	Car, lorsqu'Elohîm fit la promesse à Abraham, ne pouvant jurer par un plus grand que lui, il jura par lui-même,
6:14	en disant : Oui, vraiment, en bénissant je te bénirai, et en multipliant je te multiplierai<!--Ge. 22:16-17.-->.
6:15	Et, ayant attendu ainsi avec patience, il obtint la promesse.
6:16	Car les humains jurent en effet par celui qui est plus grand et, pour eux, la confirmation d’un serment est fin de toute opposition.
6:17	C'est pourquoi, Elohîm voulant montrer plus abondamment aux héritiers de la promesse que son dessein est inaltérable, il y a fait intervenir le serment,
6:18	afin que, par le moyen de deux choses inaltérables dans lesquelles il est impossible qu'Elohîm mente, nous qui nous sommes enfuis vers le refuge, nous ayons un puissant encouragement à saisir l’espérance qui est placée devant nous :
6:19	laquelle nous avons comme une ancre pour l’âme, à la fois sûre et ferme, et qui pénètre dans l’intérieur du voile,
6:20	où Yéhoshoua<!--Jésus.--> est entré comme notre précurseur, devenu grand-prêtre pour l’éternité, selon l'ordre de Malkiy-Tsédeq<!--Voir Ge. 14.-->.

## Chapitre 7

### Malkiy-Tsédeq (Melchisédek), type du Mashiah (Christ)<!--Ge. 14.-->

7:1	Car c'est ce Malkiy-Tsédeq, roi de Shalem, prêtre de l'Elohîm Très-Haut<!--Ge. 14:18.-->, qui alla à la rencontre d'Abraham lorsqu'il revenait de la défaite des rois et qui le bénit.
7:2	À qui aussi Abraham donna en effet pour sa part la dîme de tout<!--Ge. 14:20. Pour en savoir plus sur la dîme, voir les commentaires en De. 14:22 ; No. 18:21 et Mal. 3:10.-->. Or, en interprétant son nom, il est d'abord roi de justice, et ensuite, il est aussi roi de Shalem, c'est-à-dire roi de paix<!--Roi de shalom.-->.
7:3	Il est sans père, sans mère, sans généalogie, n'ayant ni commencement de jours ni fin de vie, mais il est rendu semblable au Fils d'Elohîm. Il demeure prêtre pour toujours.

### La prêtrise de Malkiy-Tsédeq, supérieure à celle d'Aaron

7:4	Mais considérez combien est grand celui à qui même Abraham, le patriarche, donna la dîme du butin.
7:5	Et ceux des fils de Lévi qui reçoivent la prêtrise, ont en effet l'ordre selon la torah de prélever la dîme sur le peuple, c'est-à-dire sur leurs frères, bien qu'ils soient sortis des reins d'Abraham.
7:6	Mais celui qui n'est pas d'une même famille qu'eux par sa généalogie, a reçu la dîme d’Abraham, et a béni celui qui avait les promesses.
7:7	Et sans aucune opposition, c'est le moindre qui est béni par celui qui est plus excellent.
7:8	Et ici, ce sont en effet des humains mortels qui prennent les dîmes, mais là, c'est celui de qui il est rendu témoignage qu'il est vivant.
7:9	Et pour ainsi dire, Lévi même qui prend les dîmes, a payé les dîmes à travers Abraham.
7:10	Car il était encore dans le rein<!--Endroit où les Hébreux pensaient que résidait la puissance de procréation (le sperme, la semence).--> de son père, quand Malkiy-Tsédeq alla à la rencontre de celui-ci.

### La prêtrise selon l'ordre d'Aaron n'a rien amené à la perfection

7:11	En effet, si donc la perfection avait été par le moyen de la prêtrise lévitique – car c'est sur elle que repose la torah donnée au peuple – quel besoin y aurait-il eu encore que se lève un autre prêtre selon l'ordre de Malkiy-Tsédeq, et qui ne soit pas nommé selon l'ordre d'Aaron ?
7:12	Car la prêtrise étant changée<!--Transposer (deux choses, une étant mise à la place de l'autre).-->, le changement de la torah aussi devient une nécessité.
7:13	Car, celui à l'égard duquel ces choses sont dites, a fait partie d'une autre tribu, dont personne n'a été attaché à l'autel.
7:14	Car il est connu de tous que notre Seigneur s'est levé de Yéhouda<!--Mt. 1:2.-->, tribu à l'égard de laquelle Moshé n'a rien dit concernant la prêtrise.
7:15	Et cela est encore plus évident s'il s'élève selon la similitude de Malkiy-Tsédeq un autre prêtre,
7:16	qui l'est devenu, non selon la torah d'un commandement charnel, mais selon la puissance de la vie impérissable.
7:17	Car il rend ce témoignage : Tu es prêtre pour l’éternité, selon l'ordre de Malkiy-Tsédeq<!--Ps. 110:4.-->.
7:18	Car il y a en effet abolition du commandement précédent à cause de sa faiblesse et de son inutilité.
7:19	Car la torah n'a rien amené à la perfection mais, à la place, est introduite une espérance plus excellente par le moyen de laquelle nous nous approchons d'Elohîm.
7:20	Et cela n'a pas eu lieu sans serment,
7:21	car tandis que ceux-là sont en effet devenus prêtres sans serment, mais celui-ci l'est devenu avec serment par celui qui lui a dit : Le Seigneur l'a juré et il ne s'en repentira pas<!--Voir Ps. 110:4.--> : Tu es prêtre pour l’éternité, selon l'ordre de Malkiy-Tsédeq,
7:22	par cela même, Yéhoshoua est devenu le garant d'une alliance plus excellente.

### Les prêtres sont mortels, seul le Mashiah est éternel

7:23	Et ceux-là, sont en effet devenus prêtres en très grand nombre, parce que la mort les empêchait de rester vivants,
7:24	mais lui, parce qu'il demeure pour l’éternité, possède une prêtrise qui n'est pas transmissible.
7:25	C'est pourquoi aussi il peut sauver parfaitement ceux qui s'approchent d'Elohîm par son moyen, étant toujours vivant pour intercéder<!--Le Seigneur Yéhoshoua Mashiah (Jésus-Christ) est le modèle parfait en ce qui concerne la prière d'intercession. Il se tient devant le Père pour nous. En tant qu'homme (1 Ti. 2:5) et grand-prêtre, il se tient entre le Père et l'homme pécheur, comme le faisaient les prêtres sous la loi mosaïque. Voir Lu. 22:31-32 ; Ro. 8:34 ; 1 Jn. 2:1-2.--> pour eux.
7:26	Car tel est le grand-prêtre qui nous convenait : saint, inoffensif, sans tache, séparé des pécheurs et élevé au-dessus des cieux.
7:27	Il n'a pas besoin, comme les grands-prêtres, d'offrir tous les jours des sacrifices, premièrement pour ses péchés, et ensuite pour ceux du peuple, vu qu'il a fait cela une fois en s'offrant lui-même.
7:28	Car la torah établit grands-prêtres, des humains ayant de la faiblesse, mais la parole du serment qui a été fait après la torah établit un Fils, qui est parfait pour toujours.

## Chapitre 8

### La nouvelle alliance

8:1	Or la chose principale de notre discours, c'est que nous avons un tel grand-prêtre, qui est assis à la droite du trône de la Majesté dans les cieux,
8:2	le Ministre du sanctuaire et du véritable tabernacle, celui qui a été dressé par le Seigneur et non pas par un être humain.
8:3	Car tout grand-prêtre est établi pour offrir des offrandes et des sacrifices. D'où il est nécessaire que celui-ci ait aussi quelque chose à présenter.
8:4	Car s'il était sur la Terre, il ne serait même pas prêtre, puisqu'il y a des prêtres qui offrent en effet les offrandes selon la torah.
8:5	Lesquels servent l'exemple et l'ombre des choses célestes, selon l'avertissement divin que reçut Moshé lorsqu'il devait achever le tabernacle : Prends garde, lui dit-il, de faire toutes choses selon le modèle qui t'a été montré sur la montagne<!--Ex. 25:40.-->.
8:6	Mais maintenant, il a obtenu un service d'autant plus excellent qu'il est le Médiateur d'une alliance plus excellente, qui a été établie sur de meilleures promesses.
8:7	Car si la première avait été irréprochable, il n'y aurait pas eu lieu d'en chercher une seconde.
8:8	Car, c'est en les blâmant qu'il a dit : Voici, les jours viennent, dit le Seigneur, où je traiterai avec la maison d'Israël et avec la maison de Yéhouda une alliance nouvelle,
8:9	non selon l'alliance que j’ai faite avec leurs pères, le jour où je les ai pris par la main pour les conduire hors de la terre d’Égypte. Car ils n'ont pas persévéré dans mon alliance, moi aussi je les ai négligés, dit le Seigneur.
8:10	Car voici l'alliance, le testament<!--Ac. 3:25 ; Hé. 9:16-17.--> que je ferai, après ces jours-là, avec la maison d'Israël, dit le Seigneur : Je mettrai mes torahs dans leur esprit et je les écrirai dans leur cœur, je serai leur Elohîm et ils seront mon peuple.
8:11	Et ils n'enseigneront jamais chacun leur prochain, ni chacun leur frère, en disant : Connais le Seigneur ! parce que tous me connaîtront, du petit jusqu'au grand d'entre eux.
8:12	Parce que je serai miséricordieux à l’égard de leurs injustices et que je ne me souviendrai plus jamais de leurs péchés, ni de leurs violations de la torah<!--Jé. 31:31-34.-->.
8:13	En l'appelant nouvelle, il a déclaré vieille la première. Or ce qui devient vieux et ancien est proche de la disparition.

## Chapitre 9

### Les ordonnances et le sanctuaire de la première alliance

9:1	En effet, la première alliance avait donc aussi des ordonnances du service sacré et le sanctuaire terrestre<!--Ex. 25:1-9.-->.
9:2	Car on avait construit un premier tabernacle appelé le Lieu saint, dans lequel se trouvaient le chandelier ainsi que la table et l'exposition des pains<!--Ex. 25:30.-->.
9:3	Et derrière le second voile<!--Ex. 26:31-35.--> était le tabernacle appelé le Saint des saints,
9:4	ayant un encensoir d'or<!--Ustensile pour faire fumer ou brûler des encens. Encensoir ou autel d'or pour les parfums : Lé. 16:12 ; Ex. 30:1-10. Le jour de Yom Kippour, le Grand Prêtre devait brûler de l'encens dans le lieu très saint en présence du trône de YHWH. Lé. 16:12-13 ; Ex. 30:34-38.-->, et l'arche de l'alliance<!--Ex. 25:10.--> recouverte<!--Couvrir tout autour.--> d'or de tous côtés, dans laquelle il y avait le vase d'or<!--Ex. 16:33.--> contenant la manne, et la verge d'Aaron<!--No. 17:16-25.--> qui avait fleuri, et les tablettes de l'alliance<!--Les tablettes de l'alliance ou du témoignage : Ex. 34:29 ; De. 10:2-5.-->.
9:5	Et au-dessus de l'arche, des chérubins de la gloire couvrant de leur ombre le propitiatoire<!--Propitiatoire ou couvercle de l'arche de l'alliance : Lé. 9:7, 16:15-17.-->. Ce n'est pas le moment de parler sur ce sujet en détail.
9:6	Or ces choses étant ainsi préparées, les prêtres qui accomplissent le service sacré entrent en effet en tout temps dans le premier tabernacle<!--No. 28:3.-->.
9:7	Mais seul le grand-prêtre entre dans le second une fois par an, non sans du sang qu’il offre pour lui-même et pour les péchés du peuple<!--Lé. 16:34.--> commis dans l'ignorance.
9:8	Le Saint-Esprit faisant connaître ceci, que le chemin des lieux saints n'était pas encore manifesté pendant que le premier tabernacle était encore debout.
9:9	C'est une parabole pour le temps présent, pendant lequel on offre des dons et des sacrifices qui ne peuvent rendre parfait sur le plan de la conscience, celui qui fait le service.
9:10	Ce sont seulement des ordonnances charnelles portant sur des aliments, des boissons et divers lavages effectués au moyen de l'eau<!--Vient du grec « baptismos » qui signifie « lavage ou purification effectuée au moyen de l'eau ». Il est question du lavage prescrit par la loi mosaïque (Mc. 7:4), qui semble indiquer la différence avec le baptême chrétien (Hé. 6:1-2).-->, et imposées jusqu'au temps de la réformation<!--Sens physique : remettre droit, restaurer aux conditions naturelles et normales quelque chose qui a été défait, cassé, ou qui est difforme. Pour les actions et institutions : réformation.-->.

### Le sang du Mashiah (Christ)

9:11	Mais Mashiah est survenu, grand-prêtre des bonnes choses à venir, à travers le tabernacle plus grand et plus parfait, qui n'est pas fait par la main de l'homme, c'est-à-dire, qui n'est pas de cette création.
9:12	Et il est entré une fois pour toutes dans les lieux saints, non à travers le sang des veaux ou des boucs, mais à travers son propre sang, ayant trouvé la rédemption éternelle.
9:13	Car si le sang des taureaux et des boucs, et la cendre d'une génisse<!--No. 19:1-12.--> dont on purifie par aspersion ceux qui sont impurs, sanctifie quant à la pureté de la chair,
9:14	combien plus le sang du Mashiah, qui, par le moyen de l'Esprit éternel, s'est offert lui-même à Elohîm sans défaut, purifiera-t-il votre conscience des œuvres mortes, pour servir l'Elohîm vivant ?
9:15	Et c'est pour cela qu'il est Médiateur d'une alliance nouvelle, afin que, sa mort étant venue pour la rédemption des transgressions de la première alliance, ceux qui ont été appelés, reçoivent la promesse de l'héritage éternel.
9:16	Car là où il y a un testament, il est nécessaire que la mort de celui qui a fait le testament<!--Vient du grec « diatithemai » qui signifie « arranger », « disposer », « faire ses affaires », « faire un testament ». Voir Lu. 22:29.--> survienne.
9:17	Car un testament n'est ferme qu'en cas de mort, puisqu'il n'a aucune force tant que celui qui a fait le testament est en vie.
9:18	C'est pourquoi la première alliance elle-même n'a pas été inaugurée sans le sang.
9:19	Car Moshé, après avoir prononcé devant tout le peuple chaque commandement selon la torah, prit le sang des veaux et des boucs, avec de l'eau, et de la laine écarlate et de l'hysope, et il purifia par aspersion le livre et tout le peuple, 
9:20	en disant : Ceci est le sang de l'alliance qu'Elohîm a ordonnée pour vous<!--Ex. 24:3-8.-->.
9:21	Et il purifia par aspersion de la même façon avec du sang le tabernacle, mais aussi tous les ustensiles du service<!--Ex. 29:12,36.-->.
9:22	Et presque toutes choses, selon la torah, sont purifiées par le sang, et sans effusion de sang, il n'y a pas de pardon des péchés.

### Un sacrifice plus excellent<!--Lé. 16:33.-->

9:23	Il était donc nécessaire que les exemples<!--« Un signe suggestif d'une chose », « le dessin d'une chose », « la représentation », « une figure ».--> des choses qui sont dans les cieux soient en effet purifiés de cette manière, mais que les choses célestes elles-mêmes le soient par des sacrifices plus excellents que ceux-là.
9:24	Car ce n'est pas dans les lieux saints faits par la main de l'homme, imitations des véritables, que le Mashiah est entré, mais dans le ciel même, afin de paraître maintenant pour nous devant la face d'Elohîm.
9:25	Et ce n'est pas pour s'offrir lui-même plusieurs fois qu'il y est entré, comme le grand-prêtre entre dans les lieux saints chaque année avec le sang des autres,
9:26	puisqu'il aurait fallu qu'il eût souffert plusieurs fois depuis la fondation du monde. Mais maintenant, à l'achèvement des âges, il a été manifesté une seule fois pour l'abolition du péché par le moyen de son sacrifice.
9:27	Et comme il est réservé aux humains de mourir une seule fois<!--Ce passage réfute la doctrine de la réincarnation.-->, mais après cela un jugement,
9:28	de même aussi, le Mashiah qui était offert une seule fois pour porter les péchés de beaucoup, apparaîtra une seconde fois, hors du péché, à ceux qui l'attendent assidûment et patiemment pour le salut.

## Chapitre 10

### Le sacrifice unique du Mashiah est supérieur à tous les sacrifices

10:1	Car la torah qui possède l'ombre des bonnes choses à venir et non l'image exacte des choses, ne peut jamais, par les mêmes sacrifices que l'on offre continuellement chaque année, rendre parfaits ceux qui s'en approchent.
10:2	Autrement, n'auraient-ils pas cessé d'être offerts ? Car ceux qui font ce service, une fois purifiés, n'auraient plus eu conscience des péchés.
10:3	Mais il y a chaque année dans ces sacrifices le souvenir des péchés.
10:4	Car il est impossible que le sang des taureaux et des boucs ôte les péchés.
10:5	C'est pourquoi, en entrant dans le monde, il dit : Tu n'as pas voulu de sacrifice, ni d'offrande, mais tu m'as équipé<!--« Réparer», « ajuster ». Voir 1 Th. 3:10.--> d'un corps.
10:6	Tu n'as pas pris plaisir aux holocaustes, ni aux sacrifices pour le péché<!--Ps. 40:7-9.-->.
10:7	Alors j'ai dit : Me voici, je viens – il est écrit à mon sujet entête<!--« Une petite tête » du grec « kephalis » qui signifie également « la partie la plus haute, l'extrémité d'une chose ». Les écrivains d'Alexandrie transférèrent le nom au rouleau lui-même.--> du livre, pour faire, Elohîm, ta volonté !
10:8	Ayant dit plus haut : Tu n'as pas voulu de sacrifice, ni d'offrande, ni d'holocauste, ni d'offrande pour le péché et tu n'y as pas pris plaisir, choses qui sont offertes selon la torah. 
10:9	Alors il dit : Me voici, je viens afin de faire, Elohîm, ta volonté ! Il abolit le premier afin d'établir le second.
10:10	C'est par cette volonté que nous sommes sanctifiés au moyen de l'offrande du corps de Yéhoshoua Mashiah, une fois pour toutes.
10:11	Et tout prêtre en effet se tient debout<!--2 Ch. 29:11 et 30:16 ; Né. 13:30.--> chaque jour en exerçant son service et en offrant plusieurs fois les mêmes sacrifices, qui ne peuvent jamais ôter les péchés,
10:12	mais lui, après avoir offert un seul sacrifice pour les péchés, s'est assis pour toujours à la droite d'Elohîm,
10:13	attendant désormais que ses ennemis soient mis pour le marchepied de ses pieds.
10:14	Car, par une seule offrande, il a rendu parfaits pour toujours ceux qui sont sanctifiés.
10:15	Et le Saint-Esprit aussi nous rend témoignage, car après avoir dit premièrement :
10:16	Voici l'alliance que je ferai avec eux, après ces jours-là, dit le Seigneur<!--Voir Jé. 31:31-34.--> : Je mettrai mes torahs dans leur cœur et je les écrirai dans leur pensée,
10:17	et je ne me souviendrai plus jamais de leurs péchés, ni de leurs violations de la torah.
10:18	Or, là où il y a eu pardon, il n'y a plus d'offrande au sujet du péché.

### Exhortation à s'approcher d'Elohîm avec foi

10:19	Ayant donc, frères, la liberté pour entrer dans les lieux saints par le sang de Yéhoshoua,
10:20	chemin<!--Yéhoshoua (Jésus) est le chemin qui conduit au Saint des saints, à la vie (Voir Jn. 14:6). Ce chemin n'avait pas été manifesté avant sa naissance. Hé. 9:8.--> nouveau et vivant qu'il nous a inauguré au travers du voile, c'est-à-dire de sa propre chair,
10:21	et ayant un grand-prêtre établi sur la maison d'Elohîm,
10:22	approchons-nous de lui avec un cœur sincère et une foi inébranlable, les cœurs, par aspersion, purifiés d'une mauvaise conscience et le corps lavé d'une eau pure.
10:23	Retenons fermement la profession de l'espérance, car celui qui a fait la promesse est fidèle.
10:24	Et observons-nous les uns les autres pour nous inciter à l'amour et aux bonnes œuvres.
10:25	N'abandonnons pas notre rassemblement<!--Rassemblement : du grec « episunagoge » qui veut dire « être assemblé en un lieu ». Ce passage fait premièrement allusion au rassemblement de l'Assemblée auprès du Seigneur lors de son retour. On peut y voir aussi une forme d'encouragement pour demeurer dans la communion fraternelle et non une interdiction à quitter une assemblée locale. Ce même mot est aussi utilisé dans 2 Th. 2:1.-->, comme c'est la coutume de quelques-uns, mais exhortons-nous les uns les autres, et cela d'autant plus que vous voyez approcher le jour.

### Ne pas mépriser le sacrifice du Mashiah

10:26	Car, si nous péchons volontairement après avoir reçu la connaissance précise et correcte de la vérité, il ne reste plus de sacrifice pour les péchés,
10:27	mais une attente terrible du jugement et l'ardeur d'un feu qui doit dévorer les adversaires.
10:28	Quelqu'un a-t-il rejeté la torah de Moshé ? Il meurt sans miséricorde sur la déposition de deux ou de trois témoins<!--De. 17:6.-->.
10:29	Combien pire pensez-vous que sera le châtiment dont sera jugé digne celui qui aura foulé aux pieds le Fils d'Elohîm, et qui aura considéré comme une chose profane le sang de l'alliance par lequel il a été sanctifié, et qui aura insulté l'Esprit de grâce ?
10:30	Car nous connaissons celui qui a dit : À moi la vengeance ! Moi, je rendrai la pareille, dit le Seigneur. Et encore : Le Seigneur jugera son peuple<!--De. 32:35-36.-->.
10:31	C'est une chose terrible que de tomber entre les mains de l'Elohîm vivant !
10:32	Mais rappelez-vous des premiers jours, où, après avoir été illuminés<!--Ep. 1:18.-->, vous avez enduré les souffrances d'un grand combat :
10:33	En effet, d'un côté vous avez été exposés publiquement aux opprobres et aux tribulations, et de l'autre, vous êtes devenus solidaires de ceux dont telle était la vie.
10:34	Car vous avez aussi pris part à la souffrance de mes liens et vous avez accepté avec joie, le pillage de vos biens, sachant en vous-mêmes que vous avez dans les cieux, des possessions meilleures et permanentes.
10:35	N'abandonnez donc pas votre assurance qui a une grande rémunération.
10:36	Car vous avez besoin de persévérance, afin qu'après avoir fait la volonté d'Elohîm, vous obteniez la promesse.
10:37	Car encore un peu, bien peu de temps, celui qui vient arrivera, et il ne tardera pas.
10:38	Or le juste vivra par la foi. Mais si quelqu'un se retire, mon âme ne prend pas plaisir en lui<!--Ha. 2:4.-->.
10:39	Mais nous ne sommes pas de ceux qui se retirent pour la perdition, mais de ceux qui ont la foi pour l'acquisition de leur âme.

## Chapitre 11

11:1	Or la foi est la substance<!--Vient du grec « hupostasis » : ce qui est fixé ou placé au dessous. Chose posée au-dessous, sous-structure, fondation, fondement.--> de choses qu'on espère, une conviction<!--2 Ti. 3:16.--> de celles qu'on ne voit pas.
11:2	Car c'est par elle que les anciens ont obtenu le témoignage.
11:3	Par la foi, nous comprenons que les âges ont été mis en ordre par la parole d'Elohîm, de sorte que les choses qui se voient proviennent de celles qui ne sont pas exposées à la vue.
11:4	Par la foi, Abel<!--Ge. 4:3-5.--> offrit à Elohîm un sacrifice plus excellent que Qayin, et par elle il obtint le témoignage d'être juste, parce qu'Elohîm rendait témoignage de ses offrandes. Et c'est par elle qu'il parle encore, quoique mort.
11:5	Par la foi, Hanowk<!--Hénoc. Ge. 5:22-24.--> fut enlevé pour ne pas voir la mort, et il ne parut plus parce qu'Elohîm l'avait enlevé. Car, avant son transfert, il avait obtenu le témoignage d'avoir été agréable à Elohîm.
11:6	Or il est impossible de lui être agréable sans la foi, car il faut que celui qui s'approche d'Elohîm croie que celui-ci est et qu'il est le rémunérateur de ceux qui le cherchent.
11:7	Par la foi, Noah<!--Ge. 6:14-22.-->, ayant été divinement averti au sujet des choses qui ne se voyaient pas encore, craignit<!--« Agir avec précaution », « avec circonspection », « prendre garde ».--> et construisit l'arche pour le salut<!--« Délivrance », « conservation », « sûreté », « sécurité ».--> de sa famille. Et c'est par elle qu'il condamna le monde et devint héritier de la justice qui est selon la foi.
11:8	Par la foi, Abraham<!--Ge. 12:1-4.--> étant appelé, obéit, pour aller sur la terre qu'il devait recevoir en héritage, et il partit sans savoir où il allait.
11:9	Par la foi, il séjourna comme un étranger sur la terre de la promesse, habitant sous des tentes<!--Ou « tabernacles ».--> avec Yitzhak et Yaacov<!--Isaac et Jacob.-->, héritiers avec lui de la même promesse.
11:10	Car il attendait la ville qui a des fondements, celle dont Elohîm est l'architecte<!--« Un artisan », « un ouvrier », « un technicien ».--> et le constructeur.
11:11	Par la foi aussi, Sarah<!--Ge. 21:1-2.--> elle-même reçut la force pour la conception d’une postérité, et elle enfanta hors<!--Vient du grec « kairos » qui signifie « mesure, mesure ou portion de temps, temps favorable ».--> d'âge, parce qu’elle jugea fidèle celui qui avait promis. 
11:12	C'est pourquoi aussi d'un seul, et qui était déjà atteint par la mort<!--Voir Ro. 4:19.-->, sont nés des gens comme les étoiles du ciel, en multitude, et comme le sable qui est sur le rivage de la mer qu'on ne peut compter<!--Ge. 22:17.-->.
11:13	Tous ceux-là sont morts dans la foi, sans avoir obtenu les promesses, mais ils les ont vues de loin, et ont été persuadés et les ont saluées, et ils ont confessé qu'ils étaient étrangers et voyageurs sur la Terre<!--1 Pi. 2:11.-->.
11:14	Car ceux qui parlent ainsi montrent qu’ils cherchent leur pays natal.
11:15	Et s'ils avaient en effet gardé à l'esprit celui d'où ils étaient sortis, ils auraient eu le temps d'y retourner.
11:16	Mais maintenant, ils en désirent un meilleur, c'est-à-dire un céleste. C'est pourquoi Elohîm n'a pas honte d'être appelé leur Elohîm, parce qu'il leur a préparé une ville<!--Jn. 14:2 ; Ap. 21:2.-->.
11:17	Par la foi, Abraham étant éprouvé, offrit Yitzhak. Celui qui avait reçu les promesses, offrit même son fils unique<!--Ge. 22:1-2.-->,
11:18	lui à qui il avait été dit : C’est en Yitzhak que ta postérité<!--Ge. 21:12.--> sera appelée.
11:19	Il estimait qu'Elohîm pouvait même ressusciter d'entre les morts. C'est pourquoi il le retrouva aussi en parabole.
11:20	Par la foi, Yitzhak bénit Yaacov et Ésav, en vue des choses à venir<!--Ge. 27:26-40.-->.
11:21	Par la foi, Yaacov mourant, bénit chacun des fils de Yossef<!--Ge. 48:1-22.--> et adora, appuyé sur l'extrémité de son bâton<!--Ge. 47:31.-->.
11:22	Par la foi, Yossef mourant, fit mention de la sortie des fils d'Israël et il donna des ordres au sujet de ses os<!--Ge. 50:24-25.-->.
11:23	Par la foi, Moshé<!--Ex. 2:1-3.-->, à sa naissance, fut caché pendant trois mois par ses parents, parce qu'ils virent que l'enfant était beau, et ils ne craignirent pas l'ordre du roi.
11:24	Par la foi, Moshé devenu grand, refusa d'être appelé fils de la fille de pharaon,
11:25	choisissant d'être maltraité avec le peuple d'Elohîm, plutôt que d'avoir pour un temps la jouissance du péché.
11:26	Estimant l'opprobre du Mashiah comme une richesse plus grande que les trésors de l'Égypte, car il regardait plus loin, vers la rémunération.
11:27	Par la foi, il quitta l'Égypte, sans craindre la fureur du roi, car il demeura ferme<!--Être constant.-->, comme voyant celui qui est invisible.
11:28	Par la foi, il fit la Pâque et l'aspersion du sang, afin que le destructeur des premiers-nés, ne les touche pas<!--Ex. 12:1-14,29-30.-->.
11:29	Par la foi, ils traversèrent la Mer Rouge comme au travers d'un lieu sec, tandis que les Égyptiens qui en firent la tentative furent engloutis<!--Ex. 14:13-31.-->.
11:30	Par la foi, les murs de Yeriycho<!--Jéricho.--> tombèrent, après qu'on en eut fait le tour pendant sept jours<!--Jos. 6:1-20.-->.
11:31	Par la foi, Rahab la prostituée, ne périt pas avec les rebelles, parce qu'elle avait reçu les espions en paix<!--Jos. 2:1-21, 6:23.-->.
11:32	Et que dirai-je encore ? Car le temps me manquerait pour parler de Guid'ôn<!--Gédéon. Jg. 6:11.-->, et de Barak<!--Jg. 4:6.-->, et de Shimshôn<!--Jg. 13:24.-->, et de Yiphtah<!--Jg. 11:1.-->, et de David<!--1 S. 16-17.-->, et de Shemouél<!--1 S. 1:19 à 25:1.-->, et des prophètes,
11:33	qui, par le moyen de la foi, vainquirent des royaumes, exercèrent la justice, obtinrent des promesses, fermèrent les gueules des lions,
11:34	éteignirent la force du feu, échappèrent aux bouches de l’épée, furent fortifiés à partir de la faiblesse, devinrent forts dans la bataille et mirent en fuite des armées étrangères.
11:35	Des femmes recouvrèrent leurs morts par le moyen de la résurrection. Mais d'autres furent torturés<!--Torturer avec le « tympanum », un instrument de châtiment, le « tympanum » semble avoir été un instrument en forme de roue, sur lequel les criminels étaient étendus, comme on étend une peau, puis horriblement battus avec des bâtons ou des lanières (être brisé sur la roue).-->, n'acceptant pas la rédemption afin d'obtenir une meilleure résurrection.
11:36	Mais d'autres ont reçu leur épreuve par des moqueries et des fouets, et même par des liens et de la prison.
11:37	Ils furent lapidés, sciés<!--Scier, couper en deux avec une scie, être « scié en deux » était un des châtiments en cours chez les Hébreux et la tradition s'accorde à dire que ce fut infligé au prophète Yesha`yah.-->, mis à l’épreuve. Ils moururent par meurtre de l’épée. Ils étaient vagabonds, vêtus de peaux de brebis, de peaux de chèvres, ils étaient dans le besoin, oppressés, maltraités,
11:38	eux dont le monde n'était pas digne, égarés dans les régions inhabitées et dans les montagnes, et dans les cavernes et dans les trous de la Terre.
11:39	Et tous ceux-là, bien qu’ayant reçu un bon témoignage à cause de leur foi, n'ont pourtant pas obtenu la promesse.
11:40	Elohîm ayant pourvu quelque chose de meilleur pour nous, afin qu’ils ne soient pas rendus parfaits sans nous.

## Chapitre 12

### Fixer les regards sur Yéhoshoua 

12:1	Nous donc aussi, puisque nous sommes entourés d'une si grande nuée de témoins<!--Témoin : du grec « martus », terme qui dans un sens légal et historique signifie « celui qui est spectateur d'une chose ». Dans un sens éthique, il est question de « ceux qui ont prouvé la force et l'authenticité de leur foi en Yéhoshoua en supportant une mort violente ». « Martus » a donné le mot « martyr » en français.-->, mettons de côté<!--Poser de côté ou au loin. Ro. 13:12 ; Ep. 4:22 et 25 ; Col. 3:8 ; Ja. 1:21 ; 1 P. 2:1.--> tout fardeau, et le péché qui nous entoure avec habileté<!--C'est-à-dire : assiéger.-->, et courons avec persévérance dans l'arène<!--Vient du grec « Agon » qui signifie : « une assemblée, un lieu d'assemblée : spécialement pour voir des jeux », « un lieu de la manifestation : arène ou stade », « l'assemblée des Grecs à leurs jeux nationaux », « la lutte pour le prix », « un combat », « une action en justice », « un procès ». Voir Ph. 1:30 ; Col. 2:1 ; 1 Th. 2:2 ; 1 Ti. 6:12 ; 2 Ti. 4:7.--> qui est placée devant nous,
12:2	fixant les yeux sur Yéhoshoua, l'auteur de la foi et qui la mène à la perfection. En échange de la joie qui lui était réservée, il a souffert la croix, ayant méprisé la honte, et il s'est assis à la droite du trône d'Elohîm.
12:3	Considérez en effet celui qui a supporté<!--Supporter bravement et calmement les mauvais traitements.--> contre lui-même une telle opposition de la part des pécheurs, afin que vous ne vous lassiez pas, étant découragés dans vos âmes.
12:4	Vous n'avez pas encore résisté jusqu'au sang en combattant contre le péché.

### Le châtiment du Père

12:5	Et vous avez oublié l'exhortation qui s'adresse à vous comme à des fils : Mon fils, ne méprise<!--« Aimer peu », « avoir de faibles égards », « prendre peu en compte ».--> pas le châtiment du Seigneur et ne perds pas courage, lorsqu'il te reprend.
12:6	Car le Seigneur châtie celui qu'il aime, et il châtie avec un fouet tout fils qu'il reçoit<!--Pr. 3:11-12 ; Job. 5:17 ; 1 Co. 11:32 ; Ap. 3:19.-->.
12:7	Si vous supportez<!--Supporter bravement et calmement les mauvais traitements.--> le châtiment, Elohîm se comporte envers vous comme envers des fils. Car quel est le fils que le père ne châtie pas ?
12:8	Mais si vous êtes sans châtiment auquel tous participent, vous êtes donc des enfants bâtards<!--« Enfant illégitime », « enfant naturel : celui qui est né, non de l'épouse légitime mais d'une concubine ou d'une esclave ».--> et non des fils !
12:9	D'ailleurs, nous avions en effet pour professeurs<!--« Instructeur », « précepteur », « celui qui châtie ».--> nos pères selon la chair, et nous les respections. À bien plus forte raison nous nous soumettrons au Père des esprits et nous vivrons !
12:10	Car eux nous châtiaient en effet pour peu de jours, comme ils le jugeaient bon, mais lui nous châtie pour notre bien, afin que nous soyons participants de sa sainteté.
12:11	Or tout châtiment, en effet, ne semble pas être sur le moment une joie, mais une douleur. Mais plus tard, il rend un fruit paisible de justice à ceux qui ont été exercés par ce moyen.
12:12	C'est pourquoi, redressez les mains languissantes et les genoux affaiblis<!--Paralysés.-->,
12:13	et faites des pistes<!--Une piste faite par une roue, une ornière.--> droites pour vos pieds, afin que ce qui est boiteux ne dévie pas, mais plutôt soit guéri.
12:14	Poursuivez<!--« Courir rapidement pour attraper une personne ou une chose », « harceler », « troubler », « molester », « persécuter », « courir après », « suivre ».--> la paix avec tous, et la sanctification, sans laquelle personne ne verra le Seigneur.

### Que nul ne se prive de la grâce d'Elohîm !

12:15	Veillez à ce que personne ne se prive de la grâce d'Elohîm, à ce qu'aucune racine d'amertume, poussant en haut, ne vous trouble, et que beaucoup ne soient souillés par elle.
12:16	Qu'il n'y ait parmi vous ni fornicateur, ni profane, comme Ésav, qui pour un aliment vendit son droit d'aînesse<!--Ge. 25:33.-->.
12:17	Car vous savez que plus tard, désirant hériter la bénédiction, il fut rejeté, car il ne trouva pas de lieu à la repentance, quoiqu'il l'ait recherché avec larmes.

### L'Assemblée véritable s'est approchée de Sion

12:18	Car vous ne vous êtes pas approchés d'une montagne qu'on touche avec la main<!--Ex. 19:12.-->, ni du feu brûlant, ni de la nuée épaisse, ni de la ténèbre, ni de la tempête,
12:19	ni du retentissement de la trompette, ni du son des paroles au sujet duquel ceux qui l'entendirent prièrent que la parole ne leur soit plus adressée<!--Ex. 20:18-21.-->,
12:20	car ils ne pouvaient pas supporter ce qui était ordonné : Que si même une bête touche la montagne, elle sera lapidée ou percée d'un dard<!--Ex. 19:12-13.-->.
12:21	Et ce spectacle était si terrible que Moshé dit : Je suis extrêmement effrayé et tout tremblant !
12:22	Mais vous vous êtes approchés de la montagne de Sion, de la cité de l'Elohîm vivant, la Yeroushalaim céleste, d'une multitude innombrable d'anges,
12:23	du rassemblement de fête publique et de l'Assemblée des premiers-nés qui sont inscrits<!--Ce mot vient du grec « apographo » qui signifie aussi « enregistrer ». Voir Lu. 2:1-5.--> dans les cieux, d'Elohîm qui est le juge de tous, et des esprits des justes qui ont été rendus parfaits,
12:24	de Yéhoshoua qui est le Médiateur de la nouvelle alliance, et du sang de l'aspersion qui parle des choses plus excellentes que celui d'Abel.

### Exhortation à la crainte d'Elohîm

12:25	Prenez garde de ne pas refuser celui qui parle. Car s'ils ne se sont pas échappés, ceux qui refusèrent sur Terre celui qui avertissait, combien plus nous-mêmes, si nous nous détournons de celui des cieux ?
12:26	Lui dont la voix secoua alors la Terre, mais qui maintenant a fait une promesse en disant : Une fois encore je secouerai non seulement la Terre, mais aussi le ciel<!--Ag. 2:6.-->.
12:27	Or ce une fois encore indique le changement des choses secouées, comme choses qui ont été faites, afin que celles qui ne sont pas secouées demeurent.
12:28	C'est pourquoi, recevant un royaume inébranlable, ayons la grâce, à travers elle, servons Elohîm d'une manière qui lui soit agréable, avec respect et crainte,
12:29	car notre Elohîm est aussi le feu dévorant<!--De. 4:24.-->.

## Chapitre 13

13:1	Que l’amour fraternel demeure.
13:2	N'oubliez pas l'hospitalité, car par elle, quelques-uns ont logé des anges sans le savoir.
13:3	Souvenez-vous des prisonniers comme si vous étiez emprisonnés avec eux, de ceux qui sont maltraités, comme étant aussi vous-mêmes dans un corps.
13:4	Que le mariage soit honorable<!--Vient du grec « timios » qui signifie « d'un grand prix, précieux, tenu en honneur, estimé, particulièrement cher ».--> chez tous, et que le lit nuptial soit sans souillure, mais Elohîm jugera les fornicateurs<!--Vient du grec « pornos ». Pornos fait allusion à un homme qui prostitue son corps et le loue à la convoitise d'un autre homme. Voir 1 Co. 5:11 et 6:9 ; Ep. 5:5 ; 1 Ti. 1:10 ; Hé. 12:16.--> et les adultères.
13:5	Que votre façon de vivre ne soit pas attirée par l'argent, soyez contents de ce que vous avez présentement. Car lui-même a dit : Je ne te délaisserai jamais et je ne t'abandonnerai jamais<!--De. 31:6.-->.
13:6	C'est pourquoi nous disons avec hardiesse : Le Seigneur est mon aide et je n'aurai peur de rien. Que me fera un être humain<!--Ps. 118:6.--> ?
13:7	Souvenez-vous de vos conducteurs qui vous ont annoncé la parole d'Elohîm. Regardez attentivement l’issue<!--« Une sortie », « manière de sortir », « d'une manière figurée le moyen de s'échapper de la tentation. » Ce verset se réfère à la fois à la fin de la vie physique et à la manière dont se termine une vie bien remplie.--> de leur conduite et imitez leur foi.
13:8	Yéhoshoua Mashiah est le même, hier, et aujourd'hui et pour les âges<!--Du grec « aion » qui signifie « période », « temps », « éternité », etc. Le grec utilise ici un pluriel. Ce passage confirme que Yéhoshoua est vraiment YHWH. Voir Ps. 102:27-28 ; Mal. 3:6.-->.
13:9	Ne soyez pas portés çà et là par des doctrines diverses et étrangères, car il est bon que le cœur soit affermi par la grâce, et non pas par les aliments qui n'ont pas profité à ceux qui y ont marché.

### Porter ses regards sur la ville céleste

13:10	Nous avons un autel dont ceux qui servent le tabernacle n’ont pas le droit de manger.
13:11	Car les corps des animaux dont le sang est apporté dans les lieux saints par le moyen du grand-prêtre au sujet du péché, sont brûlés hors du camp.
13:12	C'est pourquoi aussi Yéhoshoua, afin de sanctifier le peuple par le moyen de son propre sang, a souffert hors de la porte<!--Ex. 29:14. Yéhoshoua a souffert hors de Yeroushalaim (Jn. 19:17-18).-->.
13:13	Sortons donc vers lui, hors du camp<!--Le mot « camp » dans ce passage vient du grec « parambole », terme faisant référence au judaïsme antique dans lequel s'étaient embourbés des chrétiens d'origine hébraïque. Aujourd'hui, il représente plutôt le christianisme paganisé, essentiellement basé sur la torah de Moshé (loi de Moïse) et constituant une prison qui empêche certains enfants d'Elohîm de vivre pleinement leur liberté en Yéhoshoua.-->, en portant son opprobre.
13:14	Car nous n'avons pas ici-bas de ville qui demeure, mais nous cherchons celle qui est à venir.

### Le sacrifice de louange

13:15	Par son moyen donc, offrons continuellement à Elohîm un sacrifice de louange, c'est-à-dire le fruit de lèvres qui confessent son Nom.
13:16	Et n'oubliez pas la bienfaisance et de faire part de vos biens, car Elohîm prend plaisir à de tels sacrifices.

### L'obéissance aux conducteurs

13:17	Laissez-vous persuader<!--Le verbe « persuader », en grec « peitho », veut dire « se laisser persuader par des mots ». Il signifie aussi « donner avec persuasion l'envie à quelqu'un de faire quelque chose en le rassurant ». Par conséquent, les conducteurs doivent comprendre que la soumission et l'obéissance des chrétiens n'a rien à voir avec la dictature et l'autoritarisme. Ils doivent les rassurer et les convaincre – car tout ce qui n'est pas fait avec foi est péché (Ro. 14:23) – et ne pas tyranniser leurs frères en les obligeant à leur obéir (Mt. 20:25 ; 1 Pi. 5:2-3).--> par vos conducteurs et soyez-leur soumis, car ils veillent pour vos âmes dont ils auront à rendre compte, afin qu'ils le fassent avec joie et non en gémissant, car cela ne vous serait pas profitable.
13:18	Priez pour nous, car nous sommes persuadés que nous avons une bonne conscience, désirant en toutes choses bien nous conduire.
13:19	Et je vous exhorte spécialement à le faire, afin que je vous sois rendu plus vite.

### Salutations

13:20	Mais que l'Elohîm de paix, qui a emmené ailleurs, hors des morts, le grand Berger des brebis, par le sang de l'alliance éternelle, notre Seigneur Yéhoshoua,
13:21	vous équipe de toute bonne œuvre pour faire sa volonté. Qu'il fasse en vous ce qui lui est agréable par le moyen de Yéhoshoua Mashiah, auquel soit la gloire pour les âges des âges ! Amen !
13:22	Mais je vous prie, frères, supportez la parole d'exhortation ! Car je vous ai écrit en peu de mots en effet.
13:23	Sachez que notre frère Timotheos a été relâché. S'il vient plus vite, je vous verrai avec lui.
13:24	Saluez tous vos conducteurs et tous les saints. Ceux d'Italie vous saluent.
13:25	Que la grâce soit avec vous tous ! Amen !
