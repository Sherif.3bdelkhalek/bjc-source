# Matthaios (Matthieu) (Mt.)

Signification : Don d'Elohîm

Auteur : Matthaios (Matthieu)

Thème : Yéhoshoua, le Roi

Date de rédaction : Env. 50 ap. J.-C.

Matthaios (Matthieu), également connu sous le nom de Lévi, était un Juif percepteur d'impôts au service des Romains. Appelé par Yéhoshoua ha Mashiah (Jésus-Christ) à Capernaüm et choisi pour être l'un des douze disciples, il offrit un repas en l'honneur de Yéhoshoua dans sa maison, ce qui lui valut l'hostilité des pharisiens. Rédigé à Antioche de Syrie, son évangile était destiné à des Juifs convertis, comme en témoignent ses nombreuses allusions à l'ancienne alliance.

Matthaios, démontre l'hégémonie de Yéhoshoua, fils de David, fils d'Abraham, roi d'Israël. Il évoque son règne qui se manifestera un jour physiquement, lorsque le Roi jugera tous les humains du « trône de sa gloire ». Il fait concorder les paroles et les événements de la vie de Yéhoshoua avec les prophéties de l'ancienne alliance.

Son récit exalte la royauté de Yéhoshoua et expose l'Évangile du Royaume.

## Chapitre 1

### Genèse de Yéhoshoua (Jésus)

1:1	Livre de la genèse<!--Vient du grec « genesis » qui signifie « source », « origine ».--> de Yéhoshoua Mashiah, fils de David, fils d'Abraham.
1:2	Abraham engendra Yitzhak<!--Isaac.-->, et Yitzhak engendra Yaacov<!--Jacob.-->, et Yaacov engendra Yéhouda<!--Juda.--> et ses frères,
1:3	et Yéhouda engendra Pérets et Zérach, de Tamar, et Pérets engendra Hetsron, et Hetsron engendra Aram,
1:4	et Aram engendra Amminadab, et Amminadab engendra Nahshôn, et Nahshôn engendra Salmon,
1:5	et Salmon engendra Boaz, de Rahab<!--Rahab était une prostituée kena'ânéenne (cananéenne) qui est devenue l'ancêtre du Mashiah (Jos. 6).-->, et Boaz engendra Obed, de Routh<!--Routh était une Moabite, son peuple était issu de la relation incestueuse de Lot et sa fille aînée (Ge. 19:36-37). Elle est devenue l'ancêtre du Mashiah (Ru. 4:17).-->, et Obed engendra Isaï,
1:6	et Isaï engendra le roi David. Le roi David engendra Shelomoh<!--Salomon.-->, de la femme d'Ouriyah<!--Urie.-->.
1:7	Et Shelomoh engendra Rehabam, et Rehabam engendra Abiyah, et Abiyah engendra Asa,
1:8	et Asa engendra Yehoshaphat<!--Josaphat.-->, et Yehoshaphat engendra Yehoram, et Yehoram engendra Ouzyah,
1:9	et Ouzyah engendra Yotham, et Yotham engendra Achaz, et Achaz engendra Hizqiyah,
1:10	et Hizqiyah engendra Menashè<!--Manassé.-->, et Menashè engendra Amon, et Amon engendra Yoshiyah<!--Josias.-->,
1:11	et Yoshiyah engendra Yekonyah<!--Jéconias.--> et ses frères, au temps de la déportation à Babel.
1:12	Et après la déportation à Babel, Yekonyah engendra Shealthiel, et Shealthiel engendra Zerubbabel<!--Zorobabel.-->,
1:13	et Zerubbabel engendra Abioud, et Abioud engendra Élyakim, et Élyakim engendra Azor,
1:14	et Azor engendra Sadok, et Sadok engendra Achim, et Achim engendra Élioud,
1:15	et Élioud engendra Èl’azar, et Èl’azar engendra Matthan, et Matthan engendra Yaacov,
1:16	et Yaacov engendra Yossef<!--Joseph.-->, l'époux de Myriam<!--Marie.-->, de laquelle est né Yéhoshoua, qui est appelé le Mashiah<!--Vient de l'hébreu « mashiyach », traduit par « Messie » ou « Christ » en français. C'est le titre officiel du Seigneur Yéhoshoua.-->.
1:17	Ainsi toutes les générations depuis Abraham jusqu'à David sont en tout 14 générations et de David jusqu'à la déportation à Babel, 14 générations, et depuis la déportation à Babel jusqu'au Mashiah, 14 générations.

### La naissance miraculeuse de Yéhoshoua ha Mashiah (Jésus-Christ)<!--Lu. 1:26-38, 2:1-7 ; Jn. 1:1-2,14.-->

1:18	Or la naissance de Yéhoshoua Mashiah arriva de cette manière. Myriam sa mère, ayant été fiancée à Yossef, se trouva l’ayant dans le ventre par l'Esprit Saint avant qu’ils n’aient vécu ensemble.
1:19	Mais Yossef, son époux, étant un homme juste, et ne voulant pas l'exposer à une disgrâce publique, résolut de la répudier<!--Répudier vient du grec « apoluo » qui signifie « libérer », « congédier » ou « divorcer ». Sous la torah de Moshé (loi de Moïse), une femme pouvait être répudiée par décision unilatérale de son mari.--> secrètement.
1:20	Mais comme il y pensait, voici, un ange du Seigneur lui apparut en rêve et lui dit : Yossef, fils de David, n'aie pas peur de prendre avec toi Myriam, ta femme, car ce qui a été engendré en elle est de l'Esprit Saint.
1:21	Et elle enfantera un fils et tu l'appelleras du Nom de Yéhoshoua, car c'est lui qui sauvera son peuple de ses péchés.
1:22	Mais tout cela arriva afin que fût accompli ce que le Seigneur avait annoncé par le prophète, disant :
1:23	Voici, la vierge aura dans son ventre et enfantera un fils, et ils l'appelleront du Nom d'Immanou-El<!--Es. 7:14.-->, ce qui, interprété, est : El<!--Jos. 22:22 ; 2 S. 22:32 ; Es. 9:5, 10:21, 43:12.--> avec nous.
1:24	Et Yossef s'étant donc réveillé de son sommeil, fit ce que l'ange du Seigneur lui avait ordonné et il prit sa femme.
1:25	Mais il ne la connut pas jusqu'à ce qu'elle ait enfanté son fils premier-né, et il l'appela du Nom de Yéhoshoua.

## Chapitre 2

### Les mages adorent Yéhoshoua

2:1	Or Yéhoshoua étant né à Bethléhem en Judée, aux jours du roi Hérode, voici que des mages d'orient arrivèrent à Yeroushalaim,
2:2	en disant : Où est le Roi des Juifs qui est né ? Car nous avons vu son étoile en orient, et nous sommes venus l'adorer.
2:3	Mais le roi Hérode, ayant entendu cela, fut troublé, et tout Yeroushalaim avec lui.
2:4	Et il rassembla tous les principaux prêtres et les scribes du peuple, et s'enquit auprès d'eux du lieu où le Mashiah devait naître.
2:5	Et ils lui dirent : À Bethléhem en Judée, car voici ce qui a été écrit par le moyen du prophète :
2:6	Et toi, Bethléhem, terre de Yéhouda, tu n'es nullement la plus petite parmi les gouverneurs de Yéhouda, car de toi sortira le chef qui paîtra mon peuple d'Israël<!--Mi. 5:1.-->.
2:7	Alors Hérode ayant appelé secrètement les mages, s’enquit soigneusement auprès d'eux du temps où l'étoile était apparue.
2:8	Et il les envoya à Bethléhem, en disant : Allez, et informez-vous précisément sur l'enfant. Et quand vous l'aurez trouvé, faites-le-moi savoir, afin que j'aille aussi moi-même l'adorer.
2:9	Et ayant entendu le roi, ils partirent. Et voici, l'étoile<!--Le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) s'est révélé à Yohanan comme l'étoile brillante du matin (Ap. 22:16).--> qu'ils avaient vue en orient, allait devant eux jusqu'à ce qu'elle vînt s'arrêter au-dessus de l'endroit où était l'enfant.
2:10	Et en voyant l’étoile, ils se réjouirent d'une très grande joie.
2:11	Et étant entrés dans la maison, ils virent l'enfant avec Myriam, sa mère et, s'étant prosternés, ils l'adorèrent. Et ayant ouvert leurs trésors, ils lui présentèrent des dons : de l'or, de l'arbre à encens et de la myrrhe.
2:12	Et ayant été divinement avertis en rêve de ne pas retourner vers Hérode, ils se retirèrent dans leur pays par un autre chemin.

### Fuite en Égypte

2:13	Mais après leur départ, voici qu’un ange du Seigneur apparaît en rêve à Yossef et lui dit : Réveille-toi, prends l'enfant et sa mère, fuis en Égypte, et demeure là jusqu'à ce que je te le dise car Hérode est sur le point de chercher le petit enfant pour le détruire.
2:14	Et s'étant réveillé, il prit de nuit l'enfant et sa mère, et il se retira en Égypte.
2:15	Et il fut là jusqu’au décès d'Hérode, afin que fût accompli ce que le Seigneur avait annoncé par le moyen du prophète, disant : J'ai appelé mon Fils hors d'Égypte<!--Os. 11:1.-->.

### Les enfants de Bethléhem massacrés

2:16	Alors Hérode, voyant que les mages s'étaient moqués de lui, se mit dans une grande colère, et il envoya tuer tous les enfants qui étaient à Bethléhem et dans tout son territoire, depuis l'âge de deux ans et au-dessous, selon le temps dont il s'était exactement enquis auprès des mages.
2:17	Alors fut accompli ce qui avait été annoncé par Yirmeyah<!--Jérémie.--> le prophète, en disant :
2:18	Une voix a été entendue à Ramah, des lamentations, des plaintes et des grands gémissements : Rachel pleure ses enfants et n'a pas voulu être consolée parce qu'ils ne sont plus<!--Jé. 31:15.-->.

### Yossef, Myriam et Yéhoshoua reviennent en Israël<!--Lu. 2:39-52.-->

2:19	Mais après la mort d'Hérode, voici, un ange du Seigneur apparaît dans un rêve à Yossef, en Égypte,
2:20	disant : Réveille-toi, prends l'enfant et sa mère, et va en terre d'Israël car ceux qui cherchaient l’âme de l'enfant sont morts.
2:21	Et s'étant réveillé, il prit l'enfant et sa mère et alla en terre d'Israël.
2:22	Mais, entendant qu'Archélaüs règne en Judée à la place d'Hérode, son père, il craignit d'y aller. Et étant divinement averti dans un rêve, il se retira dans le territoire de la Galilée,
2:23	et vint habiter dans la ville appelée Nazareth, afin que fût accompli ce qui avait été dit par le moyen des prophètes : Il sera appelé Nazaréen<!--Ge. 49:26 ; De. 33:16 ; Am. 2:11-12.-->.

## Chapitre 3

### Yohanan le Baptiste, l'envoyé d'Elohîm<!--Mc. 1:1-8 ; Lu. 3:1-20 ; Jn. 1:6-8,15-37.-->

3:1	Or en ces jours-là arrive Yohanan le Baptiste, prêchant dans le désert de la Judée,
3:2	et disant : Repentez-vous, car le Royaume des cieux s'est approché.
3:3	Car c'est celui dont Yesha`yah, le prophète, a parlé en disant : C'est ici la voix de celui qui crie : Dans le désert, préparez le chemin du Seigneur, aplanissez ses sentiers.
3:4	Or Yohanan lui-même avait son vêtement de poil de chameau et une ceinture de cuir autour de ses reins. Et il se nourrissait de sauterelles et de miel sauvage.
3:5	Alors Yeroushalaim, toute la Judée et toute la région environnante du Yarden se rendaient auprès de lui.
3:6	Et, confessant leurs péchés, ils se faisaient baptiser par lui dans le Yarden.
3:7	Mais, voyant venir à son baptême beaucoup de pharisiens et de sadducéens, il leur dit : Progénitures de vipères, qui vous a appris à fuir la colère<!--Voir Ap. 6:16-17.--> qui vient ?
3:8	Produisez donc des fruits convenables à la repentance
3:9	et ne pensez pas à dire en vous-mêmes : Nous avons Abraham pour père ! Car je vous dis qu'Elohîm peut réveiller de ces pierres mêmes des enfants à Abraham.
3:10	Et déjà la hache est mise à la racine des arbres : tout arbre donc qui ne produit pas de beau fruit est coupé et jeté au feu.
3:11	Moi, en effet je vous baptise dans l'eau en vue de la repentance, mais celui qui vient après moi est plus puissant que moi et je ne suis pas digne de porter ses sandales. C'est lui qui vous baptisera dans le Saint-Esprit et le feu<!--Le baptême du Saint-Esprit ne doit pas être confondu avec la plénitude du Saint-Esprit. Le baptême est un acte définitif qui nous greffe au corps du Mashiah lors de la conversion (1 Co. 12:13). La plénitude consiste quant à elle en un constant renouvellement que nous devons impérativement rechercher (Ep. 5:18). Certains courants chrétiens charismatiques enseignent que le parler en langues est le signe distinctif du baptême du Saint-Esprit. Cette doctrine est basée sur au moins trois passages : Ac. 2:4, 10:44-46 et 19:1-7. Si cela était vraiment le cas, plusieurs chrétiens seraient encore dans leurs péchés et n'appartiendraient pas au Seigneur Yéhoshoua ha Mashiah. En effet, Ro. 8:9 déclare ceci : « Si quelqu'un n'a pas l'Esprit du Mashiah, il ne lui appartient pas. » Or il est manifeste que bon nombre de chrétiens nés d'en haut ne parlent pas en langues, ce qui est d'ailleurs attesté par Paulos (Paul) (1 Co. 12:30). Il n'y a aucun verset dans les Écritures qui nous ordonne de chercher le baptême du Saint-Esprit pour la bonne et simple raison que nous le recevons à la conversion.-->.
3:12	Il a la pelle à vanner dans sa main et il nettoiera complètement son aire, et il assemblera son blé dans le grenier, mais il brûlera la paille dans un feu qui ne s'éteint pas.

### Yohanan baptise Yéhoshoua ha Mashiah (Jésus-Christ)<!--Mc. 1:9-11 ; Lu. 3:21-22 ; Jn. 1:31-34.-->

3:13	Alors, Yéhoshoua arrive de Galilée au Yarden vers Yohanan, pour être baptisé par lui.
3:14	Mais Yohanan l'en empêchait en disant : C'est moi qui ai besoin d'être baptisé par toi, et tu viens à moi !
3:15	Mais répondant, Yéhoshoua lui dit : Laisse faire maintenant, car c'est ainsi qu'il nous convient d'accomplir toute justice. Et alors il le laisse faire.
3:16	Et dès que Yéhoshoua eut été baptisé, il sortit directement de l'eau. Et voici, les cieux lui furent ouverts, et il vit l'Esprit d'Elohîm descendant comme une colombe et venant sur lui.
3:17	Et voici qu'une voix venue des cieux dit : Celui-ci est mon Fils bien-aimé, en qui j'ai pris plaisir.

## Chapitre 4

### La tentation de Yéhoshoua ha Mashiah (Jésus-Christ)<!--Ge. 3:6 ; Mc. 1:12-13 ; Lu. 4:1-13 ; 1 Jn. 2:16.-->

4:1	Alors Yéhoshoua fut emmené par l'Esprit dans le désert pour être tenté par le diable<!--Vient du grec « diabolos » qui signifie « calomniateur », « prompt à la calomnie », « accusant faussement ». C'est le serpent ancien et le père du mensonge.-->.
4:2	Et après avoir jeûné 40 jours et 40 nuits, finalement, il eut faim.
4:3	Et le tentateur, s'étant approché, dit : Si tu es le Fils d'Elohîm, ordonne que ces pierres deviennent des pains.
4:4	Mais il répondit et dit : Il est écrit : L'être humain<!--Il existe plusieurs termes grecs qui ont été traduits en français par « homme ». Le plus fréquemment utilisé est [ánthrōpos]. Équivalent du mot hébreu [’âdâm], il désigne l'espèce humaine, incluant le genre masculin et féminin (voir Ge. 1:26).--> ne vivra pas de pain seulement, mais de toute parole qui sort de la bouche d'Elohîm<!--Yéhoshoua se réfère ici à De. 8:3 : « [...] l'être humain vivra de tout ce qui sort de la bouche de YHWH ».-->.
4:5	Alors le diable le prend avec lui dans la sainte ville et le met au sommet du temple,
4:6	et il lui dit : Si tu es le Fils d'Elohîm, jette-toi en bas ! Car il est écrit qu'il donnera pour toi des ordres à ses anges, et sur leurs mains ils te porteront, de peur que tu ne heurtes du pied une pierre<!--Ps. 91:12-13.-->.
4:7	Yéhoshoua lui dit : Il est aussi écrit : Tu ne tenteras pas le Seigneur, ton Elohîm<!--De. 6:16.-->.
4:8	De nouveau le diable le prend avec lui dans une montagne très élevée et lui montre tous les royaumes du monde et leur gloire.
4:9	Et il lui dit : Je te donnerai toutes ces choses, si tu te prosternes et m'adores.
4:10	Alors Yéhoshoua lui dit : Retire-toi, Satan ! Car il est écrit : Tu adoreras le Seigneur ton Elohîm, et tu le serviras lui seul<!--De. 6:13, 10:20.-->.
4:11	Alors le diable le laisse. Et voici, des anges s'approchèrent et ils le servaient.

### Yéhoshoua s'établit à Capernaüm<!--Mc. 1:14-15 ; Lu. 4:14-15.-->

4:12	Mais Yéhoshoua ayant appris que Yohanan avait été mis en prison, se retira dans la Galilée.
4:13	Et ayant quitté Nazareth, il alla demeurer à Capernaüm, ville maritime, sur les confins de Zebouloun et de Nephthali,
4:14	afin que fût accompli ce qui avait été annoncé par Yesha`yah le prophète, en disant :
4:15	La terre de Zebouloun et la terre de Nephthali, de la contrée voisine de la mer, au-delà du Yarden, et la Galilée des nations,
4:16	ce peuple, assis dans la ténèbre, a vu une grande lumière, et pour ceux qui étaient assis dans la région et dans l'ombre de la mort, la lumière elle-même s'est levée<!--Es. 9:1.-->.
4:17	Dès lors, Yéhoshoua commença à prêcher et à dire : Repentez-vous, car le Royaume des cieux s'est approché.

### Appel de Petros (Pierre), Andreas (André), Yaacov (Jacques) et Yohanan (Jean)<!--Mc. 1:16-20 ; Lu. 5:1-11 ; Jn. 1:35-51.-->

4:18	Et comme Yéhoshoua marchait le long de la Mer de Galilée, il vit deux frères, Shim’ôn, appelé Petros, et Andreas, son frère, qui jetaient un filet dans la mer, car ils étaient pêcheurs.
4:19	Et il leur dit : Venez après moi et je ferai de vous des pêcheurs d'êtres humains.
4:20	Et laissant immédiatement leurs filets, ils le suivirent.
4:21	Et de là, étant allé plus en avant, il vit deux autres frères, Yaacov, fils de Zabdi<!--Généralement traduit par Zébédée.-->, et Yohanan, son frère, dans un bateau, avec Zabdi, leur père, qui réparaient leurs filets, et il les appela.
4:22	Et laissant immédiatement leur bateau et leur père, ils le suivirent.
4:23	Et Yéhoshoua allait par toute la Galilée, enseignant dans leurs synagogues, prêchant l'Évangile du Royaume, et guérissant toutes sortes de maladies et toutes sortes d'infirmités parmi le peuple.
4:24	Et sa renommée se répandit dans toute la Syrie. Et on lui amena tous les malades, oppressés par des maladies et des tourments divers, des démoniaques, des lunatiques, des paralytiques et il les guérissait.
4:25	Et des grandes foules le suivirent, de Galilée, de la Décapole<!--Ligue des 10 villes. Une zone de territoire ainsi appelée à cause des 10 villes qui s'y trouvaient, à l'est du Yarden (Jourdain). Selon l'écrivain et naturaliste romain Pline l'Ancien (23-79), ces villes étaient : Damas, Hippos, Philadelphie, Raphana, Scythopolis, Gadara, Dion, Pella, Gerasa et Kanatha.-->, de Yeroushalaim, de Judée et au-delà du Yarden.

## Chapitre 5

### L'enseignement de Yéhoshoua sur la montagne<!--Lu. 6:20-49.-->

5:1	Et voyant les foules, il monta sur la montagne. Et s'étant assis, ses disciples s'approchèrent de lui.
5:2	Et ayant ouvert la bouche, il les enseigna, en disant :
5:3	Bénis sont les pauvres en esprit, parce que le Royaume des cieux est à eux !
5:4	Bénis sont ceux qui sont en deuil, parce qu'ils seront consolés !
5:5	Bénis sont ceux qui sont doux, parce qu'ils hériteront la Terre !
5:6	Bénis sont ceux qui ont faim et soif de la justice, parce qu'ils seront rassasiés !
5:7	Bénis sont les miséricordieux, parce qu'ils obtiendront miséricorde !
5:8	Bénis sont ceux qui sont purs de cœur, parce qu'ils verront Elohîm !
5:9	Bénis sont les pacificateurs, parce qu'ils seront appelés fils d'Elohîm !
5:10	Bénis sont ceux qui sont persécutés à cause de la justice, parce que le Royaume des cieux est à eux !
5:11	Bénis serez-vous quand ils vous insulteront<!--1 Pi. 4:14.--> et qu'ils vous persécuteront, et qu'ils diront faussement toutes sortes de mauvaises choses contre vous, à cause de moi.
5:12	Réjouissez-vous et exultez, parce que votre récompense est grande dans les cieux. Car c'est ainsi qu'ils ont persécuté les prophètes qui ont été avant vous.

### Le sel de la Terre et la lumière du monde<!--Mc. 4:21-23 ; Lu. 8:16-18, 11:33-36.-->

5:13	Vous, vous êtes le sel de la Terre. Mais si le sel a perdu sa force et sa saveur<!--Le mot grec signifie littéralement « être fou », « agir follement ». Voir Ro. 1:22 ; 1 Co. 1:20.-->, avec quoi sera-t-il salé ? Car il n'a plus sa force, mais doit être jeté dehors et foulé aux pieds par les humains.
5:14	Vous, vous êtes la lumière du monde. Une ville située sur une montagne ne peut être cachée.
5:15	Et on n'allume<!--« Mettre en feu », « allumer », « brûlant », « brûler », « consumer par le feu ». Lu. 12:35, 24:32.--> pas une lampe pour la mettre sous le boisseau<!--Mesure de volume pour matière sèche d'environ 9 litres.-->, mais sur le chandelier et elle brille pour tous ceux qui sont dans la maison.
5:16	Que votre lumière brille ainsi devant les humains, afin qu'ils voient vos bonnes œuvres et qu'ils glorifient votre Père qui est dans les cieux.

### Le Mashiah et la torah

5:17	Ne croyez pas que je sois venu pour détruire la torah ou les prophètes. Je ne suis pas venu pour détruire, mais pour accomplir.
5:18	Amen<!--Signifie « ferme ». Au début d'un discours – sûrement, vraiment, en vérité. À la fin d'un discours – ainsi soit-il, que ceci s'accomplisse. C'était une coutume, passée des synagogues aux assemblées chrétiennes, que lorsque quelqu'un avait lu ou parlé, ou avait offert une prière solennelle, les autres répondent « Amen », montrant ainsi qu'ils s'associaient à ce qui avait été dit.-->, car je vous le dis : jusqu'à ce que le ciel et la Terre aient passé, il ne passera jamais de la torah un yod ou un point jusqu'à ce que tout soit arrivé.
5:19	Celui donc qui aura renversé l'un de ces plus petits commandements et qui aura ainsi enseigné les gens, sera appelé le plus petit dans le Royaume des cieux. Mais celui qui les observera et qui enseignera à les observer, celui-là sera appelé grand dans le Royaume des cieux.
5:20	Car, je vous dis que si votre justice n'est pas supérieure à celle des scribes et des pharisiens, vous n'entrerez jamais dans le Royaume des cieux.

### Le meurtre et la colère sans cause

5:21	Vous avez entendu qu'il a été dit aux anciens : Tu n'assassineras pas. Et celui qui assassinera sera passible du jugement.
5:22	Mais moi, je vous dis que quiconque se met en colère sans cause contre son frère, sera passible du jugement. Et celui qui dira à son frère : Raca<!--Expression de mépris utilisée parmi les Juifs au temps de Yéhoshoua (Jésus) signifiant « vide, indigne » ou encore « vaurien ».--> ! sera passible du sanhédrin. Et celui qui lui dira : Fou ! sera passible de la géhenne de feu<!--La géhenne ou le lac de feu. Voir commentaire en Ap. 20:14.-->.
5:23	Si donc tu apportes ton offrande à l'autel, et que là tu te souviennes que ton frère a quelque chose contre toi,
5:24	laisse là ton offrande devant l'autel et va te réconcilier d'abord avec ton frère, et alors viens et présente ton offrande.
5:25	Accorde-toi rapidement avec ta partie adverse, tandis que tu es en chemin avec elle, de peur que ta partie adverse ne te livre au juge, et que le juge ne te livre à l'officier de justice, et que tu ne sois mis en prison.
5:26	Amen, je te le dis, tu ne sortiras jamais de là, jusqu'à ce que tu aies payé le dernier quart de sou.

### L'adultère, la convoitise et le divorce<!--Mt. 19:3-11 ; Mc. 10:2-12 ; 1 Co. 7:1-16.-->

5:27	Vous avez entendu qu'il a été dit aux anciens : Tu ne commettras pas d'adultère.
5:28	Mais moi, je vous dis que quiconque regarde une femme pour la convoiter a déjà commis dans son cœur l’adultère avec elle.
5:29	Mais si ton œil droit est pour toi une occasion de chute, arrache-le et jette-le loin de toi, car il est avantageux pour toi qu'un seul de tes membres périsse et que ton corps entier ne soit pas jeté dans la géhenne.
5:30	Et si ta main droite est pour toi une occasion de chute, coupe-la et jette-la loin de toi, car il est avantageux pour toi qu'un seul de tes membres périsse, et que ton corps entier ne soit pas jeté dans la géhenne.
5:31	Mais il a été dit encore : Si quelqu'un répudie sa femme, qu'il lui donne une lettre de répudiation<!--De. 24:1.-->.
5:32	Mais moi, je vous dis que celui qui répudie sa femme, excepté pour cause de relation sexuelle illicite<!--Vient du grec « porneia » et signifie « adultère, fornication, homosexualité, mœurs d'une lesbienne, relation avec des animaux, etc. ; relation sexuelle avec un proche parent (Lé. 18) ; relation sexuelle avec un ou une divorcée ».-->, lui fait commettre un adultère, et que celui qui épouse une femme répudiée, commet un adultère.

### Le parjure

5:33	Vous avez aussi entendu qu'il a été dit aux anciens : Tu ne te parjureras pas, mais tu rendras au Seigneur ce que tu auras promis par serment.
5:34	Mais moi, je vous dis de ne jurer aucunement, ni par le ciel, parce que c'est le trône d'Elohîm,
5:35	ni par la Terre, parce que c'est le marchepied de ses pieds, ni par Yeroushalaim, parce que c'est la ville du grand Roi.
5:36	Ne jure pas non plus par ta tête, car tu ne peux pas rendre blanc ou noir un seul cheveu.
5:37	Mais que votre parole soit : oui, oui, non, non car ce qui est de plus vient du Mauvais.

### Le comportement du disciple face au mal

5:38	Vous avez entendu qu'il a été dit : Œil pour œil et dent pour dent.
5:39	Mais moi je vous dis de ne pas résister au méchant. Mais quiconque te frappera avec un bâton sur ta joue droite, présente-lui aussi l'autre.
5:40	Et à celui qui veut plaider contre toi et prendre ta tunique, laisse-lui aussi le manteau.
5:41	Et quiconque te forcera à faire un mille, fais-en deux avec lui<!--Une loi autorisait les soldats romains à forcer les résidents des terres occupées à les accompagner sur la distance d'un mille (soit 1 480 m), pour les aider par exemple à porter leurs bagages. Cet éclairage historique nous permet non seulement d'avoir une bonne compréhension des propos du Seigneur, mais aussi de réaliser que la première venue du Mashiah n'avait pas pour but de délivrer les Hébreux de l'occupation romaine, mais de leurs péchés.-->.
5:42	Donne à celui qui te demande et ne te détourne pas de celui qui veut emprunter de toi.

### Aimer ses ennemis<!--Lu. 6:27-36.-->

5:43	Vous avez entendu qu'il a été dit : Tu aimeras ton prochain et tu haïras ton ennemi.
5:44	Mais moi je vous dis : Aimez vos ennemis, bénissez ceux qui vous maudissent, faites du bien à ceux qui vous haïssent, et priez pour ceux qui vous accusent faussement et vous persécutent,
5:45	afin que vous soyez les fils de votre Père qui est dans les cieux. Car il fait lever son soleil sur les méchants et sur les bons, et il envoie sa pluie sur les justes et sur les injustes.
5:46	Car si vous aimez seulement ceux qui vous aiment, quelle récompense en avez-vous ? Les publicains<!--Un publicain était un collecteur de taxes et d'impôts, employé par un fermier général comme péager. Ces péagers étaient d'une classe détestée par les Juifs, mais aussi par les autres, à cause de leur manière de faire leur travail et de leur rudesse.--> eux-mêmes n'en font-ils pas autant ?
5:47	Et si vous faites accueil seulement à vos frères, que faites-vous d'extraordinaire ? Les publicains eux-mêmes n'en font-ils pas autant ?
5:48	Vous serez donc parfaits, comme votre Père qui est dans les cieux est parfait.

## Chapitre 6

### Yéhoshoua condamne l'hypocrisie

6:1	Gardez-vous de pratiquer votre justice devant les gens, pour être vus par eux ; autrement, vous ne recevrez pas la récompense de votre Père qui est dans les cieux.
6:2	Donc, lorsque tu fais ton aumône, ne fais pas sonner la trompette devant toi, comme font les hypocrites dans les synagogues et dans les rues, afin d'être glorifiés par les gens. Amen, je vous le dis, ils reçoivent leur récompense entièrement.
6:3	Mais quand tu fais ton aumône, que ta main gauche ne sache pas ce que fait ta droite,
6:4	afin que ton aumône se fasse en secret ; et ton Père qui voit dans le secret, te récompensera publiquement<!--« Ils reçoivent », dans Mt. 6:2 est modifié par un préfixe qui change son sens en « avoir en totalité », qui était communément utilisé sur les reçus et du sens de « entièrement payé ». Nul paiement ni service n'était nécessaire pour clore la transaction.-->.
6:5	Et quand tu pries, ne sois pas comme les hypocrites, car ils aiment faire leurs prières en se tenant debout dans les synagogues et aux coins des rues, pour être vus des gens. Amen, je vous le dis, ils reçoivent leur récompense.
6:6	Mais toi, quand tu pries, entre dans ta chambre, et ayant fermé ta porte, prie ton Père qui est là dans le secret. Et ton Père, qui voit dans le secret, te récompensera publiquement.
6:7	Mais quand vous priez, ne multipliez pas de vaines paroles comme font les païens<!--Voir Mt. 18:17.-->, car ils pensent qu’en parlant beaucoup, ils seront exaucés.
6:8	Ne leur ressemblez donc pas, car votre Père sait de quoi vous avez besoin avant que vous le lui demandiez.

### Enseignement de Yéhoshoua sur la prière

6:9	Vous donc, priez ainsi : Notre Père qui es aux cieux ! Que ton Nom soit sanctifié,
6:10	que ton royaume vienne, que ta volonté soit faite, comme dans le ciel, aussi sur la Terre.
6:11	Donne-nous aujourd'hui notre pain qui nous suffit chaque jour<!--Le mot a comme sens premier : la survie physique demandée à YHWH, jour après jour. Soit « le pain qui nous est nécessaire » ou « le pain qui nous suffit chaque jour ». Voir également Ex. 16:16-22 ; Mt. 6:34 ; Lu. 11:3.-->,
6:12	et remets-nous nos dettes<!--Du grec « opheilema » : « ce qui est légalement dû, une dette ». Les Écritures considèrent le péché comme une dette. Voir Mt. 18:21-35.-->, comme nous aussi nous les avons remises à nos débiteurs,
6:13	ne nous amène pas en tentation, mais délivre-nous du mal. Car c'est à toi qu'appartiennent, dans tous les âges, le règne, la puissance et la gloire. Amen !
6:14	Car si vous remettez aux gens leurs fautes, votre Père céleste vous remettra aussi.
6:15	Mais si vous ne remettez pas aux gens leurs fautes, votre Père ne vous remettra pas non plus vos fautes.

### Le comportement du disciple pendant le jeûne

6:16	Mais quand vous jeûnez<!--S'abstenir sur un ou plusieurs jours, à titre religieux, de nourriture et de boisson. Le jeûne peut être total ou partiel, par de la nourriture simple et frugale.-->, ne prenez pas un aspect sombre et triste comme les hypocrites. Car ils se défigurent le visage pour faire paraître aux gens qu'ils jeûnent. Amen, je vous le dis, ils reçoivent leur récompense.
6:17	Mais toi, quand tu jeûnes, oins ta tête et lave ton visage,
6:18	afin qu'il ne paraisse pas aux gens que tu jeûnes, mais à ton Père qui est présent dans le secret, et ton Père qui voit dans le secret te récompensera publiquement.

### Le trésor selon Elohîm

6:19	Ne vous accumulez pas des richesses et des trésors sur la Terre, où la teigne et la rouille détruisent, où les voleurs percent et volent,
6:20	mais accumulez-vous des richesses et des trésors dans le ciel, où la teigne et la rouille ne détruisent pas, où les voleurs ne percent ni ne volent.
6:21	Car là où est votre trésor, là aussi sera votre cœur.
6:22	L'œil est la lampe du corps. Si donc ton œil est simple, tout ton corps sera lumineux.
6:23	Mais si ton œil est méchant, tout ton corps sera dans l'obscurité. Si donc la lumière qui est en toi n'est que ténèbre, qu'elle est grande, la ténèbre !
6:24	Nul ne peut être esclave de deux seigneurs, car, ou il haïra l'un et aimera l'autre, ou il s'attachera à l'un et méprisera l'autre. Vous ne pouvez être esclaves d'Elohîm et de Mamon<!--Mamon est un nom d'origine araméenne qui signifie richesse.-->.

### Rechercher le Royaume

6:25	C'est pourquoi je vous dis : Ne vous inquiétez pas pour votre âme, de ce que vous mangerez et de ce que vous boirez ; ni pour votre corps, de quoi vous serez vêtus. L’âme n'est-elle pas plus que la nourriture, et le corps plus que le vêtement ?
6:26	Regardez les oiseaux du ciel, car ils ne sèment ni ne moissonnent, ni ne recueillent dans des greniers, et cependant votre Père céleste les nourrit. N'êtes-vous pas beaucoup plus excellents qu'eux ?
6:27	Et qui de vous, par ses inquiétudes, peut ajouter une coudée à sa stature ?
6:28	Et pourquoi vous inquiéter au sujet du vêtement ? Examinez soigneusement comment croissent les lis des champs : ils ne travaillent ni ne filent,
6:29	cependant je vous dis que Shelomoh même, dans toute sa gloire, n'a pas été vêtu comme l'un d'eux.
6:30	Mais si Elohîm habille ainsi l'herbe des champs, qui est aujourd'hui et qui demain est jetée au four<!--Un « clibanus », four familial privé, était une grande jarre en terre et transportable. Après l'avoir chauffée, on appliquait contre les parois de la jarre : les galettes, qui cuites de cette manière, étaient très minces.-->, ne le fera-t-il pas à bien plus forte raison pour vous, gens de petite foi ?
6:31	Ne vous inquiétez donc pas, en disant : Que mangerons-nous ? Ou : Que boirons-nous ? Ou : De quoi serons-nous vêtus ?
6:32	Car ce sont les nations qui cherchent sérieusement toutes ces choses. Mais votre Père céleste sait que vous avez besoin de toutes ces choses.
6:33	Mais cherchez premièrement le Royaume d'Elohîm et sa justice, et toutes ces choses vous seront ajoutées.
6:34	Ne vous inquiétez donc pas pour le lendemain, car le lendemain s'inquiétera de lui-même. À chaque jour suffit son mal<!--Ou le mal de chaque jour est suffisant ou assez.-->.

## Chapitre 7

### Le jugement hypocrite<!--Lu. 6:37-42.-->

7:1	Ne jugez pas, afin que vous ne soyez pas jugés.
7:2	Car on vous jugera du jugement dont vous jugez, et l'on vous mesurera avec la mesure dont vous mesurez.
7:3	Et pourquoi vois-tu la paille qui est dans l'œil de ton frère, et n'aperçois-tu pas la poutre qui est dans ton œil ?
7:4	Ou comment diras-tu à ton frère : Permets que j'ôte de ton œil cette paille, et voici que la poutre est dans ton œil ?
7:5	Hypocrite, ôte premièrement la poutre de ton œil ! Alors tu verras clair pour ôter la paille de l'œil de ton frère.
7:6	Ne donnez pas les choses saintes aux chiens et ne jetez pas vos perles devant les pourceaux, de peur qu'ils ne les foulent aux pieds, ne se retournent et ne vous déchirent.

### Exhortation à la prière

7:7	Demandez et il vous sera donné, cherchez et vous trouverez, frappez et l'on vous ouvrira.
7:8	Car quiconque demande reçoit, et celui qui cherche trouve, et à celui qui frappe on ouvrira.
7:9	Et quel est l'être humain parmi vous qui, si son fils lui demande du pain, lui donnera une pierre ?
7:10	Et, s'il lui demande un poisson, lui donnera-t-il un serpent ?
7:11	Si donc, mauvais comme vous l'êtes, vous savez donner des dons qui sont bons à vos enfants, à combien plus forte raison votre Père qui est dans les cieux, donnera-t-il de bonnes choses à ceux qui les lui demandent ?

### La loi et les prophètes<!--Lu. 6:31 ; Ep. 4:32.-->

7:12	Toutes les choses donc que vous voulez que les gens fassent pour vous, faites-les de même pour eux, car c'est la torah et les prophètes.

### Le chemin de la perdition et le chemin de la vie<!--Ps. 1.-->

7:13	Entrez par la porte étroite, parce que large est la porte, et spacieux est le chemin qui mène à la perdition<!--Le mot grec « apoleia » traduit par « perdition » signifie aussi « destruction (d'un navire) », « perte », « ruine », etc. Voir Ro. 9:22 ; Ph. 1:28, 3:19 ; 2 Pi. 2:1-3, 3:7,16 ; Ap. 17:8,11.-->, et il y en a beaucoup qui entrent par elle ;
7:14	parce qu’étroite est la porte, et rétréci<!--Presser (comme des grappes), oppresser.--> le chemin qui mène à la vie, et il y en a peu qui le trouvent.

### Les faux prophètes, reconnaissables à leurs fruits<!--Lu. 6:43-45.-->

7:15	Mais gardez-vous des faux prophètes qui viennent à vous en habits de brebis, mais au-dedans, ce sont des loups ravisseurs.
7:16	Vous les reconnaîtrez à leurs fruits. Des raisins se cueillent-ils sur des épines ? Ou des figues sur des chardons ?
7:17	Ainsi tout bon arbre produit de bons fruits, mais l'arbre pourri produit de mauvais fruits.
7:18	Un bon arbre ne peut produire de mauvais fruits, ni un arbre pourri produire de bons fruits.
7:19	Tout arbre qui ne produit pas de bons fruits est coupé et jeté au feu.
7:20	Vous les reconnaîtrez donc à leurs fruits.

### Faire la volonté du Père<!--Lu. 6:46.-->

7:21	Ce n’est pas quiconque me dit : Seigneur ! Seigneur, qui entrera dans le royaume des cieux, mais celui qui fait la volonté de mon Père qui est dans les cieux !
7:22	Beaucoup me diront en ce jour-là : Seigneur, Seigneur, n'est-ce pas en ton Nom que nous avons prophétisé ? En ton Nom que nous avons chassé les démons ? En ton Nom que nous avons fait beaucoup de miracles ?
7:23	Et alors je leur dirai ouvertement : Je ne vous ai jamais connus. Retirez-vous de moi, vous qui travaillez pour la violation de la torah !

### Parabole des deux bâtisseurs<!--Lu. 6:47-49.-->

7:24	C'est pourquoi quiconque entend de moi ces paroles et les met en pratique, je le comparerai à un homme prudent qui a bâti sa maison sur le rocher.
7:25	Et la pluie est tombée, et les fleuves sont venus, et les vents ont soufflé et se sont précipités sur cette maison : et elle n'est pas tombée, parce qu'elle était fondée sur le rocher<!--Yéhoshoua ha Mashiah (Jésus-Christ) le Rocher. Voir Es. 8:13-17.-->.
7:26	Mais quiconque entend de moi ces paroles, et ne les met pas en pratique sera semblable à un homme insensé qui a bâti sa maison sur le sable.
7:27	Et la pluie est tombée, et les fleuves sont venus et les vents ont soufflé et se sont abattus contre cette maison. Et elle est tombée et sa ruine a été grande.
7:28	Or il arriva que, quand Yéhoshoua eut achevé ces discours, les foules furent choquées par sa doctrine,
7:29	car il les enseignait comme ayant de l'autorité, et non comme les scribes.

## Chapitre 8

### Guérison d'un lépreux<!--Mc. 1:40-45.-->

8:1	Et quand il fut descendu de la montagne, de grandes foules le suivirent.
8:2	Et voici qu'un lépreux étant venu, l'adorait en disant : Seigneur<!--Le mot grec pour « seigneur » est « kurios ». C'est la première fois que ce terme est appliqué à Yéhoshoua. Notez que c'est un lépreux qui a eu la révélation que Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH. Voir Lé. 14:4-32 ; De. 24:8.-->, si tu le veux, tu peux me rendre pur.
8:3	Et Yéhoshoua, étendant la main le toucha, et dit : Je le veux, sois pur. Et à l'instant même, sa lèpre fut purifiée.
8:4	Et Yéhoshoua lui dit : Attention ! Ne le dis à personne, mais va, montre-toi au prêtre et offre l'offrande<!--Lé. 14:4-32 ; De. 24:8.--> que Moshé a prescrite, en témoignage pour eux.

### Guérison du serviteur d'un officier de l'armée romaine<!--Lu. 7:1-10.-->

8:5	Et quand Yéhoshoua fut entré dans Capernaüm, un officier de l'armée romaine s'approcha de lui, le suppliant
8:6	et disant : Seigneur, mon serviteur est couché à la maison, atteint de paralysie et violemment tourmenté.
8:7	Et Yéhoshoua lui dit : Moi, je viendrai et je le guérirai.
8:8	Et l'officier de l'armée romaine répondit en disant : Seigneur, je ne suis pas digne que tu entres sous mon toit, mais dis seulement une parole et mon serviteur sera guéri.
8:9	Car moi-même qui suis un homme soumis à l'autorité, ayant sous moi des soldats, et je dis à l'un : Va ! et il va, et à un autre : Viens ! et il vient, et à mon esclave : Fais cela ! et il le fait.
8:10	Or, en l’entendant, Yéhoshoua fut étonné, et il dit à ceux qui le suivaient : Amen, je vous le dis, même en Israël, je n'ai pas trouvé une aussi grande foi.
8:11	Mais je vous dis que beaucoup viendront de l'orient et de l'occident, et seront à table dans le Royaume des cieux avec Abraham, et Yitzhak et Yaacov.
8:12	Et les fils du Royaume seront jetés dans la ténèbre du dehors : là seront les pleurs et le grincement des dents.
8:13	Et Yéhoshoua dit à l'officier de l'armée romaine : Va, et qu'il te soit fait selon que tu as cru. Et à l'heure même son serviteur fut guéri.

### Guérison de la belle-mère de Petros (Pierre)<!--Mc. 1:29-34 ; Lu. 4:38-41.-->

8:14	Et Yéhoshoua étant venu dans la maison de Petros, vit sa belle-mère qui était couchée et qui avait la fièvre.
8:15	Et il toucha sa main et la fièvre la quitta. Et elle se leva et le servit.
8:16	Et le soir étant venu, on lui amena beaucoup de démoniaques. Et il chassa les esprits par sa parole et guérit tous ceux qui étaient malades,
8:17	afin que fût accompli ce qui avait été annoncé par Yesha`yah le prophète, en disant : Lui-même a pris nos faiblesses, et a porté nos maladies<!--Es. 53:4.-->.

### Le prix de la consécration du disciple<!--Lu. 9:57-62.-->

8:18	Or Yéhoshoua, voyant autour de lui de grandes foules, ordonna de s’en aller de l’autre côté.
8:19	Et un scribe s'approchant, lui dit : Docteur, je te suivrai partout où tu iras.
8:20	Et Yéhoshoua lui dit : Les renards ont des tanières et les oiseaux du ciel ont des nids, mais le Fils d'humain n'a pas de place pour reposer sa tête.
8:21	Mais un autre de ses disciples lui dit : Seigneur, permets-moi d'aller d'abord ensevelir mon père.
8:22	Et Yéhoshoua lui dit : Suis-moi et laisse les morts ensevelir leurs morts.

### L'autorité de Yéhoshoua sur les vents et la mer<!--Mc. 4:35-41 ; Lu. 8:22-25.-->

8:23	Et quand il fut monté dans le bateau, ses disciples le suivirent.
8:24	Et voici, il s'éleva sur la mer une si grande tempête que le bateau était couvert par les grosses vagues. Et lui, il dormait.
8:25	Et ses disciples s'étant approchés le réveillèrent, et dirent : Seigneur, sauve-nous, nous périssons !
8:26	Et il leur dit : Pourquoi avez-vous peur, gens de peu de foi ? Alors s'étant réveillé, il réprimanda d'une manière tranchante les vents et la mer, et il se fit un grand calme.
8:27	Et les hommes furent dans l'admiration et dirent : Qui est celui-ci à qui obéissent même les vents et la mer ?

### Guérison de deux démoniaques<!--Mc. 5:1-20 ; Lu. 8:26-40.-->

8:28	Et quand il fut passé de l'autre côté, dans le pays des Guirgasiens, deux démoniaques sortant des sépulcres vinrent le rencontrer. Ils étaient si dangereux que personne ne pouvait passer par ce chemin-là.
8:29	Et voici, ils s'écrièrent en disant : Qu'y a-t-il entre nous et toi, Yéhoshoua, Fils d'Elohîm ? Es-tu venu ici nous tourmenter avant le temps ?
8:30	Or il y avait loin d'eux un grand troupeau de pourceaux qui paissait.
8:31	Et les démons le suppliaient, en disant : Si tu nous chasses dehors, permets-nous d'entrer dans ce troupeau de pourceaux.
8:32	Et il leur dit : Allez ! Et étant sortis, ils allèrent dans le troupeau de pourceaux. Et voici, tout le troupeau de pourceaux se précipita des pentes escarpées dans la mer, et ils périrent dans les eaux.
8:33	Mais ceux qui les faisaient paître s'enfuirent et allèrent raconter dans la ville toutes ces choses et ce qui était arrivé aux démoniaques.
8:34	Et voilà que toute la ville sortit à la rencontre de Yéhoshoua et, l'ayant vu, ils le prièrent de quitter leur territoire.

## Chapitre 9

### Yéhoshoua pardonne et guérit un paralytique<!--Mc. 2:3-12 ; Lu. 5:18-26.-->

9:1	Et étant monté dans un bateau, il traversa la mer et vint dans sa propre ville.
9:2	Et voici, on lui apporta un paralytique couché sur un lit. Et Yéhoshoua, voyant leur foi, dit au paralytique : Prends courage mon enfant, tes péchés sont remis.
9:3	Et voici, quelques-uns des scribes disaient au dedans d'eux : Cet homme blasphème.
9:4	Mais Yéhoshoua, connaissant leurs pensées, dit : Pourquoi avez-vous de mauvaises pensées dans vos cœurs ?
9:5	Car qu'est-ce qui est plus facile ? Dire : Tes péchés sont remis, ou dire : Lève-toi et marche ?
9:6	Or, afin que vous sachiez que le Fils d'humain a l'autorité sur la Terre de remettre les péchés – il dit au paralytique : Lève-toi, prends ton lit et va dans ta maison.
9:7	Et s'étant levé, il s'en alla dans sa maison.
9:8	Et les foules voyant cela, furent dans l'admiration, et elles glorifièrent Elohîm qui a donné aux humains une telle autorité.

### L'appel de Matthaios (Matthieu)<!--Mc. 2:14 ; Lu. 5:27-28.-->

9:9	Et de là, étant allé plus loin, Yéhoshoua vit un homme nommé Matthaios, assis au bureau du péage, et il lui dit : Suis-moi ! Et il se leva et le suivit.

### Yéhoshoua appelle les pécheurs à la repentance<!--Mc. 2:15-20 ; Lu. 5:29-35.-->

9:10	Et il arriva comme il était à table dans la maison, que voici, beaucoup de publicains et de pécheurs vinrent et se mirent à table avec Yéhoshoua et ses disciples.
9:11	Et les pharisiens voyant cela, dirent à ses disciples : Pourquoi votre Docteur mange-t-il avec les publicains et les pécheurs ?
9:12	Et Yéhoshoua les ayant entendus, leur dit : Ce ne sont pas ceux qui sont en bonne santé qui ont besoin de médecin, mais les malades.
9:13	Mais allez et apprenez ce que signifie : Je veux la miséricorde et non pas le sacrifice<!--Os. 6:6.-->. Car je ne suis pas venu appeler à la repentance les justes, mais les pécheurs.
9:14	Alors les disciples de Yohanan s’approchent de lui en disant : Pourquoi nous et les pharisiens jeûnons-nous souvent, tandis que tes disciples ne jeûnent pas ?
9:15	Et Yéhoshoua leur dit : Les fils de la chambre nuptiale peuvent-ils être en deuil pendant que l'Époux est avec eux ? Mais les jours viendront où l'Époux leur sera enlevé, et alors ils jeûneront.

### Parabole du drap neuf et des outres neuves<!--Mc. 2:21-22 ; Lu. 5:36-39.-->

9:16	Mais personne ne met morceau de tissu neuf à un vieux vêtement, car ce qui est mis pour remplir emporte l'habit et la déchirure en devient pire.
9:17	On ne met pas non plus du vin nouveau dans de vieilles outres, autrement, les outres se rompent, et le vin se répand, et les outres sont perdues. Mais on met le vin nouveau dans des outres neuves<!--Mc. 2:21-22 ; Lu. 5:36-39.Voir Job. 32:19.-->, et l'un et l'autre se conservent.

### Résurrection de la fille de Yaïr (Jaïrus) et guérison de la femme à la perte de sang<!--Mc. 5:21-43 ; Lu. 8:41-56.-->

9:18	Comme il leur disait ces choses, voici qu’un chef s’approche, et il se prosternait devant lui en disant : Ma fille est morte il y a un instant ; mais viens et impose-lui ta main et elle vivra.
9:19	Et, se levant, Yéhoshoua le suivit ainsi que ses disciples.
9:20	Et voici, une femme atteinte d'une perte de sang depuis 12 ans s'approcha par-derrière et toucha le bord de son vêtement.
9:21	Car elle se disait en elle-même : Si seulement je touche son vêtement, je serai sauvée !
9:22	Mais Yéhoshoua, se retournant et la voyant, dit : Aie confiance ma fille, ta foi t'a sauvée. Et la femme fut sauvée dès cette heure-là.
9:23	Et lorsque Yéhoshoua fut arrivé à la maison du chef, et qu'il vit les joueurs de flûte et une foule bruyante,
9:24	il leur dit : Retirez-vous, car la jeune fille n'est pas morte, mais elle dort, et ils le tournaient en dérision.
9:25	Mais quand la foule eut été mise dehors, il entra, prit la main de la jeune fille et elle se leva.
9:26	Et le bruit s'en répandit dans toute cette contrée.

### Deux aveugles et un démoniaque guéris

9:27	Et étant parti de là, Yéhoshoua fut suivi par deux aveugles, qui criaient en disant : Fils de David, aie pitié de nous !
9:28	Et quand il fut arrivé dans la maison, les aveugles s'approchèrent de lui, et Yéhoshoua leur dit : Croyez-vous que je puisse faire cela ? Ils lui disent : Oui, Seigneur !
9:29	Alors il toucha leurs yeux, en disant : Qu'il vous soit fait selon votre foi,
9:30	et leurs yeux s'ouvrirent. Alors Yéhoshoua leur donna un sérieux avertissement, en disant : Attention ! Que personne ne le sache !
9:31	Mais eux, étant sortis, répandirent sa renommée dans toute cette terre-là.
9:32	Mais comme ils s'en allaient, voici, on présenta à Yéhoshoua un homme muet et démoniaque.
9:33	Et le démon ayant été chassé, le muet parla. Et les foules furent dans l'admiration et dirent : Jamais pareille chose ne s'est vue en Israël !
9:34	Mais les pharisiens disaient : Il chasse les démons par le chef des démons.
9:35	Et Yéhoshoua allait dans toutes les villes et les villages, enseignant dans leurs synagogues, et prêchant l'Évangile du Royaume, et guérissant toutes sortes de maladies et toutes sortes d'infirmités parmi le peuple.

### Yéhoshoua ému de compassion pour la foule<!--Mc. 6:34.-->

9:36	Et voyant les foules, il fut ému de compassion pour elles, parce qu'elles étaient perdues et jetées à terre comme des brebis qui n'ont pas de berger.
9:37	Alors il dit à ses disciples : En effet la moisson est grande, mais il y a peu d'ouvriers.
9:38	Priez donc le Maître de la moisson d'envoyer des ouvriers dans sa moisson.

## Chapitre 10

### Mission des douze apôtres<!--Mc. 6:7-13 ; Lu. 9:1-6.-->

10:1	Et ayant appelé à lui ses douze disciples, il leur donna autorité sur les esprits impurs, pour les chasser et pour guérir toutes sortes de maladies et toutes sortes d'infirmités.
10:2	Et voici les noms des douze apôtres : Le premier est Shim’ôn, nommé Petros, et Andreas, son frère ; Yaacov, fils de Zabdi, et Yohanan, son frère ;
10:3	Philippos et Bar-Talmaï ; Thomas et Matthaios, le péager ; Yaacov, fils d'Alphaios et Lebbaios, surnommé Thaddaios ;
10:4	Shim’ôn le Qananite et Yéhouda Iscariot, celui qui le livra.
10:5	Tels sont les douze que Yéhoshoua envoya. Il leur donna ses ordres, en disant : N'allez pas sur la route des nations et n'entrez dans aucune ville des Samaritains.
10:6	Mais allez plutôt vers les brebis perdues de la maison d'Israël.
10:7	Et en allant, prêchez en disant : Le Royaume des cieux s'est approché !
10:8	Guérissez les malades, rendez purs les lépreux, ressuscitez les morts, chassez les démons. Vous avez reçu gratuitement, donnez gratuitement<!--Vous avez reçu gratuitement. Aucun chrétien, quel que soit son appel ou son don, ne peut prétendre qu'il a payé pour avoir les talents qu'il a reçus du Seigneur. Dans 1 Co. 4:7, Paulos (Paul) nous pose cette question : « Qu'as-tu que tu n'aies reçu ? Et si tu l'as reçu, pourquoi te glorifies-tu comme si tu ne l'avais pas reçu ? » Elohîm interroge également Iyov (Job) : « Qui m'a donné le premier ? Je lui rendrai » (Job 41:3). Vendre quelque chose qu'on a reçu gratuitement n'est rien d'autre que du vol. Donnez gratuitement : c'est la suite logique des choses, on reçoit gratuitement et on donne gratuitement. Si nous aimons Elohîm, nous devons garder sa parole et marcher comme lui a marché (Jn. 14:15 ; 1 Jn. 2:6). Il a donné ses enseignements et nourri les gens gratuitement. Dans Ap. 21:6 et 22:17, le Seigneur invite toutes les personnes qui ont soif à venir s'abreuver gratuitement. Alors pourquoi vendre la parole qu'on a reçue gratuitement ? Le Seigneur a envoyé les douze en mission et leur a demandé d'apporter l'Évangile du Royaume, de guérir les malades et de délivrer les possédés gratuitement (Ac. 8:18-24, 20:33-35 ; Ap. 21:6, 22:17).-->.
10:9	Ne vous procurez ni or, ni argent, ni monnaie dans vos ceintures,
10:10	ni de sac pour le voyage, ni deux tuniques, ni sandales, ni bâton, car l'ouvrier est digne de sa nourriture.
10:11	Et dans quelque ville ou village où vous entrerez, informez-vous qui y est digne et demeurez chez lui jusqu'à ce que vous partiez de là.
10:12	Et en entrant dans la maison, saluez-la :
10:13	et, si cette maison en est digne, en effet que votre shalôm vienne sur elle, mais si elle n'en est pas digne, que votre shalôm retourne à vous.
10:14	Mais lorsque quelqu'un ne vous recevra pas et n'écoutera pas vos paroles, secouez en partant de cette maison ou de cette ville la poussière de vos pieds.
10:15	Amen, je vous le dis : le sort de la terre de Sodome et de Gomorrhe sera plus supportable au jour du jugement, que celui de cette ville-là.

### Yéhoshoua annonce la persécution de ses disciples

10:16	Voici que moi, je vous envoie comme des brebis au milieu des loups. Soyez donc prudents comme des serpents, et sans mélange<!--Le mot grec « akeraios » signifie aussi « non mélangé », « pur » (tel du vin ou des métaux), « esprit sans mélange de mal », « libre d'artifice », « innocent », « simple ». Voir Ro. 16:19 et Ph. 2:15.--> comme des colombes.
10:17	Mais gardez-vous des gens, car ils vous livreront aux sanhédrins et vous châtieront avec un fouet dans leurs synagogues.
10:18	Et vous serez menés, à cause de moi, devant des gouverneurs et devant des rois, en témoignage pour eux et pour les nations.
10:19	Mais, quand ils vous livreront, ne vous inquiétez pas de ce que vous direz, ni comment vous parlerez, car cela vous sera donné à l'heure même.
10:20	Car ce n'est pas vous qui parlerez, mais c'est l'Esprit de votre Père qui parlera en vous.
10:21	Mais le frère livrera son frère à la mort et le père son enfant. Et les enfants s'élèveront contre leurs parents et les feront mourir.
10:22	Et vous serez haïs de tous, à cause de mon Nom, mais celui qui supportera bravement et calmement les mauvais traitements jusqu'à la fin, celui-là sera sauvé.
10:23	Mais quand ils vous persécuteront dans une ville, fuyez dans une autre. Amen, je vous le dis, en effet, vous n'en aurez jamais fini avec les villes d'Israël, que le Fils d'humain sera venu.
10:24	Le disciple n'est pas au-dessus du docteur, ni l'esclave au-dessus de son seigneur.
10:25	Il suffit au disciple de devenir comme son docteur, et à l'esclave comme son seigneur. S'ils ont appelé le Maître de la maison Béelzéboul, à combien plus forte raison appelleront-ils ainsi ses domestiques ?
10:26	N'ayez donc pas peur d'eux, car il n'y a rien de caché qui ne doive être découvert, ni rien de secret qui ne doive être connu.
10:27	Ce que je vous dis dans la ténèbre, dites-le dans la lumière. Et ce que vous entendez à l'oreille, prêchez-le sur les toits.
10:28	Et ne craignez pas ceux qui tuent le corps et qui ne peuvent tuer l'âme. Mais craignez plutôt celui qui peut faire périr et l'âme et le corps dans la géhenne.
10:29	Ne vend-on pas deux passereaux pour un as<!--Du grec « assarion » : pièce de monnaie romaine, en cuivre ou bronze, égale au dixième d'une drachme ; un sou.--> ? Cependant, il n'en tombe pas un à terre sans la volonté de votre Père.
10:30	Et même les cheveux de votre tête sont tous comptés.
10:31	N'ayez donc pas peur : Vous valez plus que beaucoup de passereaux.
10:32	Quiconque donc me confessera devant les gens, je le confesserai aussi devant mon Père qui est dans les cieux.
10:33	Mais quiconque me reniera devant les gens, je le renierai aussi devant mon Père qui est dans les cieux.
10:34	Ne croyez pas que je sois venu jeter la paix sur la Terre ! Je ne suis pas venu jeter la paix, mais l'épée.
10:35	Car je suis venu séparer l'homme d'avec son père, et la fille d'avec sa mère, et la belle-fille d'avec sa belle-mère,
10:36	et l'homme aura pour ennemis les gens de sa propre maison<!--Mi. 7:6.-->.
10:37	Celui qui aime son père ou sa mère plus que moi, n'est pas digne de moi, et celui qui aime son fils ou sa fille plus que moi, n'est pas digne de moi.
10:38	Et quiconque ne prend pas sa croix et ne vient pas après moi, n'est pas digne de moi.
10:39	Celui qui aura trouvé son âme la perdra, mais celui qui aura perdu son âme à cause de moi la trouvera.
10:40	Celui qui vous reçoit, me reçoit, et celui qui me reçoit, reçoit celui qui m'a envoyé.
10:41	Celui qui reçoit un prophète au nom de prophète, recevra la récompense d'un prophète, et celui qui reçoit un juste au nom de juste, recevra la récompense d'un juste.
10:42	Et quiconque aura donné à boire seulement une coupe d'eau froide à l'un de ces petits, au nom de disciple, amen, je vous le dis, il ne perdra jamais sa récompense.

## Chapitre 11

### Yohanan le Baptiste, le messager envoyé pour préparer la voie du Seigneur<!--Lu. 7:19-35.-->

11:1	Et il arriva que, quand Yéhoshoua eut achevé de donner ses ordres à ses douze disciples, il partit de là, pour aller enseigner et prêcher dans leurs villes.
11:2	Or, Yohanan ayant entendu parler dans sa prison des œuvres du Mashiah, envoya deux de ses disciples
11:3	pour lui dire : Es-tu celui qui vient, ou devons-nous en attendre un autre ?
11:4	Et Yéhoshoua répondant, leur dit : Allez et rapportez à Yohanan ce que vous entendez et ce que vous voyez :
11:5	Les aveugles recouvrent la vue, et les boiteux marchent, les lépreux sont purifiés, et les sourds entendent, les morts ressuscitent, et l'Évangile est annoncé aux pauvres<!--Yéhoshoua ha Mashiah (Jésus-Christ) est l'Elohîm véritable dont la venue a été annoncée par Yesha`yah (Ésaïe) (Es. 35:4-6).-->.
11:6	Mais, béni est celui qui n’aura pas été scandalisé en moi !
11:7	Et comme ils s'en allaient, Yéhoshoua se mit à dire aux foules au sujet de Yohanan : Mais qu'êtes-vous allés voir dans le désert ? Un roseau agité par le vent ?
11:8	Mais qu'êtes-vous allés voir ? Un homme vêtu de vêtements précieux<!--« Précieux », « doux au toucher » ; « efféminé », « d'un garçon ayant des relations homosexuelles avec un homme », « d'un homme qui soumet son corps à de l'impudicité hors nature », « d'un prostitué mâle ».--> ? Voici, ceux qui portent des vêtements précieux sont dans les maisons des rois.
11:9	Mais qu'êtes-vous allés voir ? Un prophète ? Oui, vous dis-je, et plus qu'un prophète.
11:10	Car c'est celui dont il est écrit : Voici que moi j'envoie mon messager<!--Mal. 3:1.--> devant ta face, pour préparer ton chemin devant toi.
11:11	Amen, je vous le dis, parmi ceux qui sont nés de femmes, il n'en a été suscité aucun de plus grand que Yohanan le Baptiste. Toutefois, le plus petit dans le Royaume des cieux est plus grand que lui.
11:12	Or depuis le temps de Yohanan le Baptiste jusqu'à maintenant, le Royaume des cieux est forcé, et ce sont les violents qui l’arrachent.
11:13	Car tous les prophètes et la torah ont prophétisé jusqu'à Yohanan.
11:14	Et si vous voulez le recevoir, c'est lui qui est l'Éliyah<!--Mal. 3:23-24.--> qui doit venir.
11:15	Que celui qui a des oreilles pour entendre entende !
11:16	Mais à qui comparerai-je cette génération ? Elle est semblable aux petits-enfants assis sur les places de marché et qui s'adressent à leurs compagnons,
11:17	et leur disent : Nous vous avons joué de la flûte et vous n'avez pas dansé. Nous avons chanté des chants funèbres mais vous ne vous êtes pas frappés la poitrine de chagrin.
11:18	Car Yohanan est venu, ne mangeant ni ne buvant, et ils disent : Il a un démon.
11:19	Le Fils d'humain est venu, mangeant et buvant, et ils disent : Voici un homme vorace et un buveur de vin, un ami des publicains et des pécheurs. Mais la sagesse a été justifiée par ses enfants.

### Yéhoshoua dénonce les incrédules

11:20	Alors il se mit à faire des reproches aux villes dans lesquelles avaient eu lieu la plupart de ses miracles, parce qu'elles ne s'étaient pas repenties.
11:21	Malheur à toi Chorazin ! Malheur à toi Bethsaïda ! Car si les miracles qui ont été faits au milieu de vous avaient été faits dans Tyr et dans Sidon, il y a longtemps qu'elles se seraient repenties, en prenant le sac et la cendre.
11:22	C'est pourquoi je vous dis que, le sort de Tyr et de Sidon sera plus supportable au jour du jugement que le vôtre.
11:23	Et toi, Capernaüm, qui as été élevée jusqu'au ciel, on te fera descendre jusqu'à l'Hadès<!--Le séjour des morts ou le Hadès se trouve en bas (Es. 14:15 ; Pr. 15:24 ; 1 S. 28). Voir commentaire en Mt. 16:18.--> ! Car si les miracles qui ont été faits au milieu de toi, avaient été faits dans Sodome, elle demeurerait encore aujourd'hui !
11:24	C'est pourquoi je vous dis que le sort de la terre de Sodome sera plus supportable au jour du jugement, que le tien.
11:25	En ce temps-là Yéhoshoua répondit en disant : Je te célèbre, Père ! Seigneur du ciel et de la Terre, de ce que tu as caché ces choses aux sages et aux intelligents, et que tu les as révélées aux petits enfants.
11:26	Oui, Père, car telle a été ta bonne volonté devant toi.
11:27	Toutes choses m'ont été livrées par mon Père, et personne ne connaît le Fils, excepté le Père, et personne ne connaît le Père, excepté le Fils, et celui à qui le Fils veut le révéler.
11:28	Venez à moi, vous tous qui êtes fatigués et chargés, et je vous donnerai du repos<!--Ap. 6:11, 14:13.-->.
11:29	Prenez mon joug sur vous et apprenez de moi, car je suis doux et humble de cœur, et vous trouverez le repos pour vos âmes.
11:30	Car mon joug est doux et mon fardeau est léger.

## Chapitre 12

### Yéhoshoua, le Seigneur du shabbat<!--Mc. 2:23-28 ; Lu. 6:1-5.-->

12:1	En ce temps-là, Yéhoshoua passa à travers les champs ensemencés un jour de shabbat. Et ses disciples, qui avaient faim, se mirent à arracher des épis et à les manger.
12:2	Mais les pharisiens voyant cela, lui dirent : Voici, tes disciples font ce qui n'est pas légal de faire le jour du shabbat.
12:3	Mais il leur dit : N'avez-vous pas lu ce que fit David quand il eut faim, lui et ceux qui étaient avec lui ?
12:4	Comment il entra dans la maison d'Elohîm et mangea les pains des faces<!--La disposition d'une chose placée en vue, le pain de proposition. Douze pains de froment, correspondant au nombre des tribus d'Israël, lesquels pains étaient offerts à chaque shabbat, et séparés en deux rangées, disposés pour sept jours sur une table placée dans le sanctuaire ou devant le tabernacle, et ensuite dans le temple.-->, ce qui n'était pas légal de manger ni pour lui, ni pour ceux qui étaient avec lui, mais pour les prêtres seulement ?
12:5	Ou n'avez-vous pas lu dans la torah que, les jours de shabbat, les prêtres profanent le shabbat dans le temple sans se rendre coupables ?
12:6	Mais je vous dis qu’il y a ici quelque chose de plus grand que le temple.
12:7	Mais si vous saviez ce que signifie : Je veux la miséricorde et non pas le sacrifice<!--1 S. 15:22 ; Os. 6:6.-->, vous n'auriez pas condamné des innocents.
12:8	Car le Fils d'humain est Seigneur même du shabbat.

### Guérison de l'homme à la main sèche<!--Mc. 3:1-5 ; Lu. 6:6-11.-->

12:9	Et étant parti de là, il entra dans leur synagogue.
12:10	Et voici, il s'y trouvait un homme qui avait la main sèche. Et pour pouvoir l'accuser, ils l'interrogèrent, en disant : Est-il légal de guérir les jours du shabbat ?
12:11	Mais il leur dit : Quel sera parmi vous l'être humain, qui, n'ayant qu'une brebis, et qu'elle vienne à tomber dans une fosse le jour du shabbat, ne la saisira-t-il pas pour l'en retirer ?
12:12	Combien un être humain ne vaut-il pas plus qu'une brebis ! Il est donc légal de faire du bien les jours du shabbat.
12:13	Alors il dit à cet homme : Étends ta main ! Il l'étendit et elle fut restaurée à l'état initial, aussi saine que l'autre.
12:14	Or les pharisiens sortirent et ils se consultèrent sur les moyens de le faire périr.

### Yéhoshoua (Jésus), le Mashiah (Christ) annoncé par Yesha`yah (Ésaïe)

12:15	Mais Yéhoshoua, l'ayant su, partit de là, et de grandes foules de gens le suivirent. Il les guérit tous.
12:16	Et il les réprimanda d'une manière tranchante afin qu'ils ne le fassent pas connaître<!--« Apparent », « manifeste », « évident », « connu », « manifesté, c'est-à-dire être clairement reconnu ou connu ».-->,
12:17	pour que fût accompli ce qui avait été annoncé par Yesha`yah le prophète, en disant :
12:18	Voici mon serviteur que j'ai choisi, mon bien-aimé, en qui mon âme a pris plaisir. Je mettrai mon Esprit en lui et il proclamera le jugement aux nations.
12:19	Il ne se querellera<!--S'engager dans une lutte. Ce verbe est utilisé pour décrire l'humeur calme de Yéhoshoua en contraste avec la véhémence des docteurs Juifs se disputant à propos de principes et de pratiques.--> pas, il ne criera pas et personne n'entendra sa voix dans les rues.
12:20	Il ne brisera pas le roseau cassé et n'éteindra pas le lumignon qui fume, jusqu'à ce qu'il ait fait triompher la justice.
12:21	Et les nations espéreront en son Nom<!--Es. 42:1-4.-->.

### Le blasphème contre le Saint-Esprit<!--Mc. 3:22-30 ; Lu. 11:15-23.-->

12:22	Alors, on lui amena un démoniaque aveugle et muet, et il le guérit, de sorte que l'aveugle et muet parlait et voyait.
12:23	Et toutes les foules en furent étonnées et elles disaient : Celui-ci n'est-il pas le Fils de David ?
12:24	Mais les pharisiens ayant entendu cela, disaient : Celui-ci ne chasse les démons que par Béelzéboul, le chef des démons.
12:25	Mais Yéhoshoua connaissant leurs pensées, leur dit : Tout royaume divisé contre lui-même, sera réduit en désert, et toute ville ou maison divisée contre elle-même, ne subsistera pas.
12:26	Et si Satan chasse Satan, il est divisé contre lui-même. Comment donc son royaume subsistera-t-il ?
12:27	Et si je chasse les démons par Béelzéboul, par qui vos fils les chassent-ils ? C'est pourquoi ils seront eux-mêmes vos juges.
12:28	Mais, si c’est par l’Esprit d'Elohîm que moi, je chasse les démons, le Royaume d'Elohîm est donc venu jusqu'à vous.
12:29	Ou, comment quelqu'un peut-il entrer dans la maison d'un homme fort et piller ses biens s’il n’a d’abord lié cet homme fort ? Et alors il pillera sa maison.
12:30	Celui qui n'est pas avec moi est contre moi, et celui qui n'assemble pas avec moi, disperse.
12:31	C'est pourquoi je vous dis que tout péché et tout blasphème sera remis aux humains, mais le blasphème contre l'Esprit, ne sera pas remis aux humains.
12:32	Et quiconque aura parlé contre le Fils d'humain, cela lui sera remis ; mais quiconque aura parlé contre le Saint-Esprit, cela ne lui sera remis ni dans cet âge ni dans celui qui est à venir.
12:33	Ou produisez<!--Mt. 3:8,10, 7:15-20.--> l'arbre beau et son fruit beau, ou produisez l'arbre pourri et son fruit pourri, car on connaît l'arbre par le fruit.
12:34	Progénitures de vipères, comment pourriez-vous dire de bonnes choses, méchants comme vous l'êtes ? Car c'est de l'abondance du cœur que la bouche parle.
12:35	L'être humain bon<!--Le mot « bon » dans ce passage vient du grec « agathos » qui signifie « de bonne constitution ou nature », « utile », « salutaire », « bon », « agréable », « plaisant », « joyeux », « heureux », « excellent », « distingué », « droit », « honorable ».--> tire de bonnes choses du bon trésor de son cœur et l'être humain méchant tire de mauvaises choses de son mauvais trésor.
12:36	Mais je vous dis que le jour du jugement, les humains rendront compte de toute parole vaine qu'ils auront prononcée.
12:37	Car par tes paroles tu seras justifié, et par tes paroles tu seras condamné.

### Le miracle de Yonah (Jonas), le prophète<!--Jon. 2:1 ; Lu. 11:29-32.-->

12:38	Alors quelques-uns des scribes et des pharisiens répondirent en disant : Docteur, nous voulons voir un signe de ta part.
12:39	Mais répondant, il leur dit : Une génération méchante et adultère<!--Littéralement : « une femme adultère ».--> demande un signe, mais il ne lui sera pas donné d'autre signe que celui de Yonah, le prophète.
12:40	Car, de même que Yonah fut trois jours et trois nuits dans le ventre d'un grand poisson, de même le Fils d'humain sera trois jours et trois nuits dans le cœur de la Terre.
12:41	Les hommes de Ninive se relèveront au jugement contre cette génération et la condamneront, parce qu'ils se repentirent à la prédication de Yonah, et voici, il y a ici plus que Yonah.
12:42	La reine du midi se réveillera au jugement contre cette génération et la condamnera, parce qu'elle vint des extrémités de la Terre pour entendre la sagesse de Shelomoh, et voici, il y a ici plus que Shelomoh.

### Le retour de l'esprit impur<!--Lu. 11:24-26.-->

12:43	Mais lorsque l’esprit impur est sorti de l’être humain, il passe par des lieux sans eau<!--Voir 2 Pi. 2:17 ; Jud. 1:12.-->, cherchant du repos, mais il n'en trouve pas.
12:44	Alors il dit : Je retournerai dans ma maison, d'où je suis sorti. Et quand il arrive, il la trouve vide, balayée et ornée.
12:45	Alors, il s'en va et prend avec lui sept autres esprits plus méchants que lui-même, et ils y entrent et s'y établissent, et la dernière condition de cet être humain est pire que la première. Il en sera de même pour cette génération méchante !

### La famille spirituelle<!--Mc. 3:31-35 ; Lu. 8:19-21.-->

12:46	Et comme il parlait encore aux foules, voici que sa mère et ses frères se tenaient dehors, cherchant à lui parler.
12:47	Et quelqu'un lui dit : Voici, ta mère et tes frères se tiennent dehors, cherchant à te parler.
12:48	Mais répondant, il dit à celui qui le lui avait dit : Qui est ma mère et qui sont mes frères ?
12:49	Et étendant sa main sur ses disciples, il dit : Voici ma mère et mes frères !
12:50	Car, quiconque aura fait la volonté de mon Père qui est dans les cieux, celui-là est mon frère, et ma sœur, et ma mère.

## Chapitre 13

### Parabole du semeur et des quatre terrains<!--Mc. 4:1-20 ; Lu. 8:4-15.-->

13:1	Or, en ce jour-là, Yéhoshoua étant sorti de la maison, s'était assis près de la mer.
13:2	Et de grandes foules se rassemblèrent auprès de lui, de sorte que, montant dans le bateau, il s'y assit. Et toute la foule se tenait sur le rivage.
13:3	Et il leur parla en paraboles sur beaucoup de choses, et il dit : Voici que celui qui sème sortit pour semer.
13:4	Il sème et il tomba de la semence en effet à côté du chemin et les oiseaux vinrent et la mangèrent.
13:5	Mais une autre tomba dans les endroits pierreux où elle n'avait pas beaucoup de terre, et elle leva immédiatement parce que la terre n'a pas de profondeur<!--Voir Ep. 3:18.-->.
13:6	Mais, quand le soleil parut, elle fut brûlée par la chaleur et sécha parce qu'elle n'a pas de racines.
13:7	Mais une autre tomba parmi les épines, et les épines montèrent et l'étouffèrent.
13:8	Et une autre partie tomba dans la bonne terre et elle donna en effet du fruit, l'un 100, l'autre 60 et un autre 30.
13:9	Que celui qui a des oreilles pour entendre, qu'il entende !

### Explication de la parabole du semeur

13:10	Et les disciples s'étant approchés, lui dirent : Pourquoi leur parles-tu en paraboles ?
13:11	Et répondant, il leur dit : Parce qu'il vous a été donné de connaître les mystères du Royaume des cieux, mais qu'à eux, cela n'a pas été donné.
13:12	Car on donnera à celui qui a, et il sera dans l'abondance, mais à celui qui n'a pas, on ôtera même ce qu'il a.
13:13	C'est pourquoi je leur parle en paraboles, parce qu'en voyant, ils ne voient pas et qu'en entendant, ils n'entendent pas et ne comprennent pas.
13:14	Et ainsi s'accomplit pour eux la prophétie de Yesha`yah<!--Es. 6:9.-->, qui dit : Vous entendrez de vos oreilles et vous ne comprendrez jamais ; et en regardant, vous regarderez et vous ne verrez jamais !
13:15	Car le cœur de ce peuple s'est engraissé, et de leurs oreilles ils ont entendu avec difficulté, et ils ont fermé leurs yeux de peur qu'ils ne voient de leurs yeux, qu'ils n'entendent de leurs oreilles, qu'ils ne comprennent de leur cœur, qu'ils ne se convertissent et que je ne les guérisse<!--Es. 6:9-10.-->.
13:16	Mais bénis sont vos yeux, parce qu’ils voient, et vos oreilles, parce qu'elles entendent !
13:17	Amen, car je vous dis que beaucoup de prophètes et de justes ont désiré voir ce que vous voyez et ne l'ont pas vu, et entendre ce que vous entendez et ne l'ont pas entendu.
13:18	Vous donc, entendez la parabole de celui qui sème.
13:19	Lorsque quelqu’un entend la parole du Royaume et ne la comprend pas, le Mauvais vient et arrache ce qui est semé dans son cœur : c’est celui qui a reçu la semence le long du chemin.
13:20	Et celui qui a été semé sur les endroits pierreux, c'est celui qui entend la parole et la reçoit immédiatement avec joie ;
13:21	mais il n'a pas de racine en lui-même et il ne tient qu'un temps et, dès que survient une tribulation ou une persécution à cause de la parole, immédiatement il trébuche.
13:22	Et celui qui a reçu la semence parmi les épines, c'est celui qui entend la parole d'Elohîm, mais en qui les soucis de cet âge et la séduction des richesses étouffent la parole et la rendent stérile.
13:23	Mais celui qui a reçu la semence dans la bonne terre, c'est celui qui entend la parole et la comprend. Et il produit en effet du fruit, l'un 100, l'autre 60, l'autre 30.

### Parabole du blé et de l'ivraie

13:24	Il leur proposa une autre parabole, en disant<!--La parabole du blé et de l'ivraie. En méditant cette parabole, nous remarquons que lorsque le blé eut poussé et donné du fruit, l'ivraie parut aussi. Il est vrai que lorsqu'il y a un réveil spirituel divin dans une assemblée ou dans un territoire, l'ennemi suscite aussi un faux réveil avec des faux ouvriers et des fausses manifestations spirituelles. Voilà pourquoi l'ivraie côtoiera le blé jusqu'à la fin du monde. Le mot « ivraie » se dit « ebriacus » en latin, ce qui donne « ébriété » en français. Nous comprenons donc que l'un des rôles de l'ivraie est d'enivrer le blé (les enfants d'Elohîm). Dans les Écritures, l'ivresse est synonyme de la débauche spirituelle ou physique. En grec l'ivraie se dit « zizanion » qui donne en français « zizanie ». Voir Mt. 12:25.--> : Le Royaume des cieux est semblable à un humain qui a semé de la bonne semence dans son champ.
13:25	Mais, pendant que les humains dormaient, son ennemi vint et sema de l'ivraie parmi le blé et s'en alla.
13:26	Mais lorsque l'herbe eut poussé et produit du fruit, alors l'ivraie apparut aussi.
13:27	Et les esclaves du maître de la maison vinrent à lui et lui dirent : Seigneur, n'as-tu pas semé de la bonne semence dans ton champ ? D'où vient donc qu'il a cette ivraie ?
13:28	Mais il leur répondit : C'est un ennemi, un humain qui a fait cela. Et les esclaves lui dirent : Veux-tu donc que nous allions la cueillir ?
13:29	Et il leur dit : Non, de peur qu'en cueillant l'ivraie, vous ne déraciniez le blé en même temps.
13:30	Laissez-les croître ensemble tous les deux jusqu'à la moisson et, au temps de la moisson, je dirai aux moissonneurs : Arrachez premièrement l'ivraie et liez-la en gerbes pour la brûler, mais amassez le blé dans mon grenier.

### Parabole du grain de sénevé<!--Mc. 4:30-32 ; Lu. 13:18-19.-->

13:31	Il leur proposa une autre parabole et il dit : Le Royaume des cieux est semblable au grain de sénevé<!--Le nom d'une plante des régions orientales, qui provenait d'une toute petite graine, puis atteignait la hauteur d'un arbre, 3 mètres et plus. De là une chose insignifiante est comme un grain de sénevé, mais elle peut grandir d'une façon considérable.--> qu'un être humain a pris et semé dans son champ.
13:32	En effet, c'est la plus petite de toutes les semences, mais quand il a poussé, il est plus grand que les légumes et devient un arbre, de sorte que les oiseaux du ciel viennent habiter et font leurs nids dans ses branches.

### Parabole du levain<!--Lu. 13:20-21.-->

13:33	Il leur dit une autre parabole : Le Royaume des cieux est semblable à du levain qu'une femme prend et cache dans trois mesures de farine, jusqu'à ce que le tout soit levé.
13:34	Yéhoshoua dit aux foules toutes ces choses en paraboles, et il ne leur parlait pas sans parabole,
13:35	afin que fût accompli ce qui avait été annoncé par le moyen du prophète, disant : J'ouvrirai ma bouche en paraboles, je déclarerai les choses qui ont été cachées depuis la fondation du monde<!--Ps. 78:2.-->.

### Explication de la parabole du blé et de l'ivraie

13:36	Alors Yéhoshoua renvoya les foules et entra dans la maison. Et ses disciples s'approchèrent de lui et lui dirent : Explique-nous la parabole de l'ivraie du champ.
13:37	Et répondant, il leur dit : Celui qui sème la bonne semence, c'est le Fils d'humain ;
13:38	et le champ, c'est le monde, la bonne semence ce sont les fils du Royaume, et l'ivraie ce sont les fils du Mauvais ;
13:39	et l'ennemi qui l'a semée, c'est le diable ; et la moisson<!--Joë. 4:13.-->, c'est l'achèvement de l'âge, et les moissonneurs sont les anges.
13:40	De même donc que l'on cueille l'ivraie et qu'on la brûle dans le feu, de même en sera-t-il lors de l'achèvement de l'âge.
13:41	Le Fils d'humain enverra ses anges qui arracheront de son Royaume tous les scandales et ceux qui travaillent pour la violation de la torah,
13:42	et les jetteront dans la fournaise de feu où il y aura le pleur et le grincement de dents.
13:43	Alors les justes resplendiront comme le soleil dans le Royaume de leur Père. Que celui qui a des oreilles pour entendre, qu'il entende !

### Parabole du trésor caché

13:44	Le Royaume des cieux est encore semblable à un trésor caché dans un champ. L'être humain qui l'a trouvé, le cache et, dans sa joie, il s'en va et vend tout ce qu'il a, et il achète ce champ.

### Parabole de la perle

13:45	Le Royaume des cieux est encore semblable à un être humain, un marchand qui cherche de bonnes perles,
13:46	et qui, ayant trouvé une perle de grand prix, s'en est allé et a vendu tout ce qu'il avait, et l'a achetée.

### Parabole du filet

13:47	Le Royaume des cieux est encore semblable à un filet jeté dans la mer et qui ramasse des choses de toute race.
13:48	Quand il est rempli, on le tire en haut sur le rivage, et on s'assied, on recueille dans des vases ce qui est bon et l'on jette ce qui est pourri.
13:49	Il en sera de même à l'achèvement de l'âge, les anges viendront séparer les méchants d'avec les justes,
13:50	et les jetteront dans la fournaise de feu où il y aura le pleur et le grincement de dents.
13:51	Yéhoshoua leur dit : Avez-vous compris toutes ces choses ? Ils lui répondirent : Oui, Seigneur.

### Le scribe instruit

13:52	Et il leur dit : C'est pourquoi, tout scribe instruit de ce qui regarde le Royaume des cieux est semblable à un être humain, un maître de maison qui tire de son trésor des choses nouvelles et des choses anciennes.
13:53	Et il arriva que quand Yéhoshoua eut achevé ces paraboles, il partit de là.

### Yéhoshoua à Nazareth<!--Mc. 6:1-6.-->

13:54	Et s'étant rendu dans son pays natal, il enseignait dans leur synagogue, de telle sorte qu'ils étaient choqués et disaient : D'où lui viennent cette sagesse et ces miracles ?
13:55	Celui-ci n'est-il pas le fils du charpentier ? Sa mère ne s'appelle-t-elle pas Myriam, et ses frères, Yaacov, Yossef, Shim’ôn et Yéhouda ?
13:56	Et ses sœurs ne sont-elles pas toutes parmi nous ? D'où lui viennent donc toutes ces choses ?
13:57	Et il était pour eux une occasion de chute. Mais Yéhoshoua leur dit : Un prophète n'est déshonoré que dans son pays natal et dans sa maison.
13:58	Et il ne fit là que peu de miracles, à cause de leur incrédulité.

## Chapitre 14

### Mort de Yohanan le Baptiste<!--Mc. 6:14-29 ; Lu. 9:7-9.-->

14:1	En ce temps-là, Hérode le tétrarque, entendit parler de la renommée de Yéhoshoua,
14:2	et il dit à ses serviteurs : C'est Yohanan le Baptiste ! Il est ressuscité des morts, c'est pourquoi les pouvoirs d'accomplir des miracles sont à l'œuvre à travers lui.
14:3	Car Hérode, ayant saisi Yohanan, l'avait lié et mis en prison, à cause d'Hérodias, femme de Philippos, son frère.
14:4	Car Yohanan lui disait : Il n'est pas légal pour toi de l'avoir.
14:5	Et il voulait le faire mourir, mais il craignait la foule, parce qu'ils le considéraient comme un prophète.
14:6	Or le jour où l'on célébra la fête d'anniversaire de la naissance d'Hérode, la fille d'Hérodias dansa au milieu d'eux et plut à Hérode,
14:7	c'est pourquoi il lui promit avec serment de lui donner tout ce qu'elle demanderait.
14:8	Or, ayant été traînée par sa mère, elle dit : Donne-moi ici, sur un plat, la tête de Yohanan le Baptiste.
14:9	Et le roi fut attristé, mais à cause de ses serments et de ceux qui étaient à table avec lui, il donna l'ordre qu'on la lui donnât.
14:10	Et il envoya décapiter Yohanan dans la prison.
14:11	Et sa tête fut apportée sur un plat et donnée à la fille qui la présenta à sa mère.
14:12	Et ses disciples vinrent et emportèrent son corps, et l'ensevelirent. Et ils allèrent l'annoncer à Yéhoshoua.
14:13	Et l'ayant appris, Yéhoshoua se retira en bateau dans un lieu désert, à l'écart. Et les foules l'ayant appris, sortirent des villes et le suivirent à pied.
14:14	Et Yéhoshoua étant sorti, vit une grande foule et il fut ému de compassion pour elle, et guérit leurs malades.

### Première multiplication des pains pour les 5 000 hommes<!--Mc. 6:32-44 ; Lu. 9:12-17 ; Jn. 6:1-14.-->

14:15	Et comme il se faisait tard, ses disciples vinrent à lui et lui dirent : Ce lieu est désert et l'heure est déjà avancée, renvoie les foules afin qu'elles aillent dans les villages, pour s'acheter des vivres.
14:16	Mais Yéhoshoua leur dit : Elles n'ont pas besoin de s'en aller. Donnez-leur vous-mêmes à manger !
14:17	Mais ils lui disent : Nous n'avons ici que cinq pains et deux poissons.
14:18	Et il dit : Apportez-les-moi ici.
14:19	Et après avoir ordonné aux foules de s'asseoir sur l'herbe, il prit les cinq pains et les deux poissons, et levant les yeux au ciel, il prononça la prière de bénédiction. Et ayant rompu les pains, il les donna aux disciples, et les disciples aux foules.
14:20	Et tous en mangèrent et furent rassasiés, et l'on emporta douze paniers pleins des morceaux qui restaient.
14:21	Or ceux qui avaient mangé étaient environ 5 000 hommes, sans compter les femmes et les petits enfants.

### Yéhoshoua marche sur les eaux<!--Mc. 6:45-56 ; Jn. 6:15-21.-->

14:22	Et immédiatement après, Yéhoshoua força ses disciples à monter dans le bateau et à passer avant lui de l'autre côté, pendant qu'il renverrait les foules.
14:23	Et, après avoir renvoyé les foules, il monta sur la montagne pour prier à l'écart et, le soir venu, il était là seul.
14:24	Mais le bateau, déjà au milieu de la mer, était battu par les grosses vagues, car le vent était contraire.
14:25	Et vers la quatrième veille de la nuit, Yéhoshoua alla vers eux, marchant sur la mer.
14:26	Et ses disciples le voyant marcher sur la mer, furent troublés et dirent : C'est un fantôme ! Et, dans leur frayeur, ils poussèrent des cris.
14:27	Mais Yéhoshoua leur dit immédiatement : Ayez confiance, c'est moi, n'ayez pas peur !
14:28	Mais Petros lui répondit en disant : Seigneur, si c'est toi, ordonne que j'aille vers toi sur les eaux.
14:29	Et il lui dit : Viens ! Et Petros sortit du bateau, marcha sur les eaux pour aller vers Yéhoshoua.
14:30	Mais voyant que le vent était fort, il eut peur et, comme il commençait à s'enfoncer, il s'écria en disant : Seigneur ! Sauve-moi !
14:31	Et immédiatement Yéhoshoua étendit sa main et le prit, en lui disant : Homme de peu de foi, pourquoi as-tu douté ?

### Yéhoshoua est adoré par ses disciples

14:32	Et quand ils furent montés dans le bateau, le vent s'apaisa.
14:33	Et ceux qui étaient dans le bateau vinrent et l'adorèrent en disant : Tu es vraiment le Fils d'Elohîm !

### Yéhoshoua guérit les malades à Génésareth<!--Mc. 6:53-56.-->

14:34	Et après la traversée, ils vinrent dans la terre de Génésareth.
14:35	Et les hommes de cet endroit l'ayant reconnu, envoyèrent des messagers dans toute la région environnante et on lui amena tous les malades.
14:36	Et ils le suppliaient de les laisser seulement toucher le bord de son vêtement. Et tous ceux qui le touchèrent furent préservés du danger.

## Chapitre 15

### Yéhoshoua condamne les traditions<!--Mc. 7:1-13.-->

15:1	Alors des scribes et des pharisiens viennent de Yeroushalaim auprès de Yéhoshoua, en disant :
15:2	Pourquoi tes disciples transgressent-ils la tradition des anciens ? Car ils ne se lavent pas les mains quand ils prennent du pain.
15:3	Mais il leur répondit et dit : Et vous, pourquoi transgressez-vous le commandement d'Elohîm à cause de votre tradition ?
15:4	Car Elohîm a commandé, disant : Honore ton père et ta mère. Et il a dit aussi : Celui qui maudit son père ou sa mère finit à la mort.
15:5	Mais vous, vous dites : Si quelqu'un a dit à son père ou à sa mère : Tout ce dont tu pourrais être assisté par moi est une offrande à Elohîm,
15:6	et il n’a pas du tout à honorer son père. Et vous avez annulé le commandement d'Elohîm à cause de votre tradition.
15:7	Hypocrites ! Yesha`yah a bien prophétisé sur vous, en disant :
15:8	Ce peuple s'approche de moi de sa bouche et m'honore des lèvres, mais son cœur est très éloigné de moi.
15:9	Mais ils m'adorent en vain, en enseignant des doctrines qui ne sont que des commandements d'humains<!--Es. 29:13.-->.

### Le cœur de l'être humain<!--Mc. 7:14-23.-->

15:10	Et ayant appelé à lui la foule, il lui dit : Écoutez, et comprenez ceci :
15:11	Ce n'est pas ce qui entre dans la bouche qui rend l'être humain impur, mais ce qui sort de la bouche. Voilà ce qui rend l'être humain impur.
15:12	Alors ses disciples, s'approchant, lui dirent : Sais-tu que les pharisiens ont été scandalisés quand ils ont entendu cette parole ?
15:13	Mais répondant, il dit : Toute plante que mon Père céleste n'a pas plantée sera déracinée.
15:14	Laissez-les : ce sont des aveugles qui guident des aveugles. Si un aveugle guide un autre aveugle, ils tomberont tous deux dans la fosse.
15:15	Mais Petros prenant la parole, lui dit : Explique-nous cette parabole.
15:16	Et Yéhoshoua dit : Vous aussi, êtes-vous encore sans intelligence ?
15:17	Ne comprenez-vous pas encore que tout ce qui entre dans la bouche va dans le ventre et est jeté dans les toilettes ?
15:18	Mais les choses qui sortent de la bouche viennent du cœur, et ces choses-là rendent l'être humain impur.
15:19	Car c'est du cœur que sortent les mauvaises pensées, les meurtres, les adultères, les relations sexuelles illicites, les vols, les faux témoignages, les blasphèmes.
15:20	Ce sont ces choses-là qui rendent l'être humain impur. Mais manger sans avoir les mains lavées, cela ne rend pas l'être humain impur.

### La femme kena'ânéenne (cananéenne) adore Yéhoshoua<!--Mc. 7:24-30.-->

15:21	Et Yéhoshoua étant parti de là, se retira dans le territoire de Tyr et de Sidon.
15:22	Et voici, une femme kena'ânéenne, sortant de ces contrées, s'écria en lui disant : Seigneur, Fils de David, aie pitié de moi ! Ma fille est misérablement tourmentée par un démon.
15:23	Mais il ne lui répondit pas un mot. Et les disciples, s'étant approchés, le priaient en disant : Renvoie-la, car elle crie derrière nous.
15:24	Et il répondit en disant : Je n'ai été envoyé qu'aux brebis perdues de la maison d'Israël.
15:25	Mais s'étant approchée, elle l'adorait en disant : Seigneur, aide-moi !
15:26	Et il lui répondit, en disant : Il ne convient pas de prendre le pain des enfants et de le jeter aux petits chiens.
15:27	Mais elle dit : Cela est vrai Seigneur ! Cependant les petits chiens mangent des miettes qui tombent de la table de leurs maîtres.
15:28	Alors Yéhoshoua répondant, lui dit : Ô femme ! Ta foi est grande. Qu'il t'arrive comme tu le veux ! Et à l'heure même, sa fille fut guérie.

### Yéhoshoua guérit beaucoup de malades

15:29	Et Yéhoshoua ayant quitté ce lieu, vint près de la Mer de Galilée. Et étant monté sur une montagne, il s’y assit.
15:30	Et de grandes foules s'approchèrent de lui, ayant avec elles des boiteux, des aveugles, des muets, des estropiés et beaucoup d'autres. Et ils les jetèrent aux pieds de Yéhoshoua et il les guérit.
15:31	De sorte que les foules étaient dans l'admiration de voir que les muets parlaient, que les estropiés étaient guéris, que les boiteux marchaient, que les aveugles voyaient, et elles glorifiaient l'Elohîm d'Israël.

### Seconde multiplication des pains<!--Mc. 8:1-9.-->

15:32	Or, Yéhoshoua ayant appelé ses disciples, dit : Je suis ému de compassion envers cette foule, car voilà trois jours qu'ils restent avec moi et ils n'ont rien à manger. Et je ne veux pas les renvoyer à jeun, de peur que les forces ne leur manquent en chemin.
15:33	Et ses disciples lui disent : D'où pourrions-nous tirer dans cette région inhabitée assez de pains pour rassasier une si grande foule ?
15:34	Et Yéhoshoua leur dit : Combien avez-vous de pains ? Et ils lui dirent : 7 et quelques petits poissons.
15:35	Alors il commanda aux foules de s'asseoir sur la terre.
15:36	Et ayant pris les 7 pains et les poissons, et après avoir rendu grâce, il les rompit et les donna à ses disciples, et les disciples à la foule.
15:37	Et tous mangèrent et furent rassasiés, et l'on emporta 7 corbeilles pleines des morceaux qui restaient.
15:38	Or ceux qui avaient mangé étaient 4 000 hommes, sans compter les femmes et les petits enfants.
15:39	Et ayant renvoyé les foules, il monta sur un bateau et se rendit dans le territoire de Magdala.

## Chapitre 16

### Les pharisiens et les sadducéens : Une génération méchante et adultère<!--Mc. 8:10-14.-->

16:1	Et les pharisiens et les sadducéens vinrent à lui et pour l'éprouver, lui demandèrent de leur faire voir un signe venant du ciel.
16:2	Mais il répondit en leur disant : Quand le soir est venu, vous dites : Il fera beau temps, car le ciel est rouge.
16:3	Et le matin : Il y aura de l'orage aujourd'hui, car le ciel est d'un rouge sombre. Hypocrites ! En effet, vous savez discerner<!--« Séparer », « faire une distinction », « distinguer », « préférer », « apprendre par discernement ».--> l'aspect du ciel, mais quant aux signes des temps vous ne le pouvez pas !
16:4	Une génération méchante et adultère<!--Littéralement : « une femme adultère ».--> demande un signe, mais il ne lui sera pas donné d'autre signe que le signe de Yonah, le prophète. Et les laissant, il s'en alla.

### Le levain des pharisiens et des sadducéens<!--Mc. 8:15-21 ; Lu. 12:1-15.-->

16:5	Et ses disciples, en passant sur l'autre bord, avaient oublié de prendre des pains.
16:6	Et Yéhoshoua leur dit : Attention ! Gardez-vous du levain des pharisiens et des sadducéens.
16:7	Et ils raisonnaient en eux-mêmes et disaient : C'est parce que nous n'avons pas pris de pains.
16:8	Et Yéhoshoua, le sachant, leur dit : Gens de peu de foi, pourquoi raisonnez-vous en vous-mêmes sur le fait que vous n'avez pas pris de pains ?
16:9	Vous n'avez donc pas encore compris ? Ne vous souvenez-vous pas des cinq pains pour les 5 000 et combien de paniers vous avez emportés ?
16:10	Ni des 7 pains pour les 4 000 et combien de corbeilles vous avez emportées ?
16:11	Comment ne comprenez-vous pas que ce n'est pas au sujet du pain que je vous ai dit de vous garder du levain des pharisiens et des sadducéens ?
16:12	Alors ils comprirent que ce n'était pas du levain du pain qu'il leur avait dit de se garder, mais de la doctrine des pharisiens et des sadducéens.

### Petros (Pierre) reconnaît Yéhoshoua comme étant le Mashiah (Christ)<!--Mc. 8:27-30 ; Lu. 9:18-21.-->

16:13	Mais Yéhoshoua, étant arrivé dans le territoire de Césarée de Philippos, interrogea ses disciples en disant : Qui suis-je aux dires des gens, moi, le Fils d'humain ?
16:14	Et ils dirent : Sûrement Yohanan le Baptiste ; les autres, Éliyah ; et les autres, Yirmeyah ou l'un des prophètes.
16:15	Il leur dit : Et vous, qui dites-vous que je suis ?
16:16	Shim’ôn Petros<!--Simon Pierre.--> répondit et dit : Tu es le Mashiah, le Fils d'Elohîm, le Vivant<!--Apo. 1:18.-->.
16:17	Et Yéhoshoua lui répondit et dit : Tu es béni, Shim’ôn, fils de Yonah, car ce ne sont pas la chair et le sang qui t'ont révélé cela, mais mon Père qui est dans les cieux.

### Yéhoshoua bâtit son Assemblée

16:18	Et moi, je te dis que tu es Petros<!--Pierre.-->, et que sur ce Rocher<!--Ce passage a été mal traduit par beaucoup comme suit : « Et moi, je te dis que tu es Pierre, et que sur cette pierre je bâtirai mon Église... » Or, pour une bonne compréhension des propos de Yéhoshoua, il est important d'insister sur la distinction que le grec fait entre « petros » (pierre, caillou, Pierre) et « petra » (roc, rocher), qui n'est autre que Yéhoshoua ha Mashiah (Jésus-Christ), le Rocher des âges (Es. 17:10, 26:4, 44:8 ; Ps. 18:32 ; 1 Co. 10:4). De là en découle un enseignement fondamental : l'Assemblée n'est bâtie ni par un homme ni sur l'homme, en l'occurrence Petros (Pierre) et ses supposés successeurs (papes), mais par Yéhoshoua ha Mashiah lui-même qui en est la pierre angulaire et le fondement inébranlable (Ac. 4:11 ; Ep. 2:20).-->, je bâtirai mon Assemblée, et les portes de l'Hadès<!--Usage ultérieur du mot : « tombe », « mort », « enfer ». Hadès chez les Grecs ou Pluton chez les Romains, était considéré comme le dieu des profondeurs souterraines et le maître des enfers. Ce terme est parfois traduit par « séjour des morts », équivalent hébreu de « shéol ». Les Grecs utilisaient l'euphémisme « pylartes », signifiant « aux portes solidement closes », pour parler du très craint Hadès. En effet, Juifs, Grecs et Romains avaient conscience que les portes closes de l'enfer ne laissaient personne sortir du royaume de la mort. Tous les impies, et même les croyants d'avant Yéhoshoua ha Mashiah, étaient retenus par les portes de l'enfer. Toutefois, les croyants allaient dans une partie de l'enfer que les Juifs appelaient « sein d'Abraham » (1 S. 28:7-19 ; Lu. 16:22-25, 23:43 – Voir commentaire en Lu. 16:22) où ils ne subissaient pas les tourments infligés aux impies. Lorsque le Seigneur est mort, il est descendu « dans les parties inférieures de la Terre » pour prendre les clés de l'Hadès, c'est-à-dire les clés du séjour des morts (Col. 2:15 ; Ap. 1:17-18) et libérer les captifs pieux. Yéhoshoua affirme que les portes de l'enfer ne prévaudront jamais contre son Assemblée puisque c'est lui qui l'a bâtie. Malgré tout, Hadès (l'enfer), bien que vaincu par le Seigneur, essaie d'attirer l'Assemblée que le Seigneur a établie dans les lieux célestes (Ep. 2:4-9 ; Col. 3:1) vers le royaume de la ténèbre, par les fausses doctrines et le péché. Au jour du jugement dernier, Hadès (l'enfer) et la mort, qui sont deux démons, seront jetés dans le lac de feu et de soufre (Ap. 20:11-15).--> ne prévaudront pas contre elle.
16:19	Je te donnerai les clés du Royaume des cieux et tout ce que tu lieras sur la Terre sera lié dans les cieux, et tout ce que tu délieras sur la Terre sera délié dans les cieux<!--Une mauvaise compréhension de ce verset a contribué à propager l'idée erronée selon laquelle Petros (Pierre) serait le médiateur entre Elohîm et les humains, puisque c'est lui qui détiendrait les clés du Royaume des cieux. Toutefois, Es. 22:22 affirme que seul Yéhoshoua ha Mashiah (Jésus-Christ) détient ces clés qui symbolisent l'autorité et la domination. Or, dans le cadre de l'héritage que le Seigneur nous a laissé, cette autorité est désormais exercée en son Nom par tous les membres du corps du Mashiah (Mt. 18:18).-->.
16:20	Alors il ordonna à ses disciples de ne dire à personne qu'il est Yéhoshoua ha Mashiah.

### Yéhoshoua annonce sa mort et sa résurrection<!--Mc. 8:31-33 ; Lu. 9:22.-->

16:21	Dès lors, Yéhoshoua commença à déclarer à ses disciples qu'il fallait qu'il aille à Yeroushalaim, qu'il souffre beaucoup de la part des anciens, des principaux prêtres et des scribes, qu'il soit mis à mort et qu'il soit ressuscité le troisième jour.
16:22	Et Petros, l'ayant pris à part, se mit à le réprimander d'une manière tranchante, en disant : Seigneur, sois miséricordieux avec toi, cela ne t'arrivera jamais !
16:23	Mais lui, s'étant retourné, dit à Petros : Va-t'en derrière moi Satan<!--Vient du grec « satanas » qui veut dire « adversaire » ou « ennemi ». Voir 1 R. 11:14,23-25 ; 1 S. 29:4.--> ! Tu m'es en scandale, parce que tu ne penses pas aux choses d'Elohîm, mais à celles des humains.

### Le renoncement à soi-même<!--Mc. 8:34-38 ; Lu. 9:23-26.-->

16:24	Alors Yéhoshoua dit à ses disciples : Si quelqu'un veut venir après moi, qu'il renonce à lui-même et qu'il se charge de sa croix, et qu'il me suive.
16:25	Car celui qui veut sauver son âme la perdra, mais quiconque perdra son âme à cause de moi, la trouvera.
16:26	Car que sert à un être humain de gagner tout le monde, s'il perdait son âme ? Ou, que donnerait un être humain en échange de son âme ?
16:27	Car le Fils d'humain doit venir dans la gloire de son Père, avec ses anges, et alors il rendra à chacun selon ses œuvres.
16:28	Amen, je vous le dis, quelques-uns de ceux qui sont ici présents, ne goûteront jamais la mort, non, jusqu'à ce qu'ils aient vu le Fils d'humain venant dans son règne<!--Ce passage doit être lu de concert avec Mt. 24:32-34. Yéhoshoua utilise un langage prophétique pour expliquer deux réalités. La première réalité est spirituelle et concerne ses contemporains qui allaient vivre l'effusion de l'Esprit pour rétablir le Royaume d'Elohîm dans le cœur des gens. En effet, le Seigneur ne les a pas laissés orphelins, mais il est revenu sous la forme de l'Esprit (Jn. 14:17-18 ; Ac. 2 ; Ac. 16:7). Aussi, les apôtres ont pu proclamer ce Royaume partout où ils allaient (Ac. 20:25). La deuxième réalité est matérielle et concerne le fleurissement du figuier, c'est-à-dire Israël. L'histoire atteste le fleurissement de ce figuier tant sur le plan géographique que sur le plan numérique. Depuis le 14 mai 1948, date de la naissance officielle de l'État hébreu, Israël ne cesse de s'étendre. Cette nation est l'horloge des temps car le Mashiah gouvernera le monde entier depuis Yeroushalaim (Jérusalem) (Mi. 4 ; Za. 14).-->.

## Chapitre 17

### La transfiguration de Yéhoshoua ha Mashiah (Jésus-Christ)<!--Mc. 9:1-8 ; Lu. 9:27-36.-->

17:1	Et six jours après, Yéhoshoua prend Petros et Yaacov, et son frère Yohanan, et les mène à l'écart sur une haute montagne.
17:2	Et il fut transfiguré en leur présence, son visage resplendit comme le soleil et ses vêtements devinrent blancs comme la lumière.
17:3	Et voici, Moshé et Éliyah leur apparurent, s'entretenant avec lui.
17:4	Mais Petros prenant la parole, dit à Yéhoshoua : Seigneur, il est bon que nous soyons ici. Faisons-y, si tu le veux, trois tabernacles, un pour toi, un pour Moshé et un pour Éliyah.
17:5	Comme il parlait encore, voici une nuée resplendissante les couvrit de son ombre. Et voici, une voix fit entendre de la nuée ces paroles : Celui-ci est mon Fils bien-aimé, en qui j'ai pris mon bon plaisir : Écoutez-le !
17:6	Et lorsque les disciples entendirent cette voix, ils tombèrent le visage contre terre et furent saisis d'une très grande frayeur.
17:7	Et Yéhoshoua s'approchant, les toucha et dit : Levez-vous et n'ayez pas peur.
17:8	Et ils levèrent les yeux et ne virent personne, excepté Yéhoshoua tout seul.
17:9	Et comme ils descendaient de la montagne, Yéhoshoua leur donna cet ordre, en disant : Ne parlez à personne de cette vision, jusqu'à ce que le Fils d'humain soit ressuscité des morts.
17:10	Et ses disciples l'interrogèrent, en disant : Pourquoi donc les scribes disent-ils qu'il faut qu'Éliyah vienne premièrement<!--Mal. 3:1.--> ?
17:11	Et Yéhoshoua répondit et leur dit : En effet, Éliyah vient premièrement et restaurera à l'état initial toutes choses.
17:12	Mais je vous dis qu'Éliyah est déjà venu et ils ne l'ont pas reconnu, mais ils l'ont traité comme ils ont voulu. De même, le Fils d'humain doit souffrir aussi de leur part.
17:13	Alors les disciples comprirent que c'était de Yohanan le Baptiste<!--Lu. 7:27.--> qu'il leur parlait.

### L'incrédulité des disciples<!--Mc. 9:14-29 ; Lu. 9:37-43.-->

17:14	Et quand ils furent arrivés près de la foule, un homme s'approcha de lui et, tombant à genoux devant lui,
17:15	dit : Seigneur, aie pitié de mon fils qui est lunatique et misérablement affligé, car il tombe souvent dans le feu et souvent dans l'eau.
17:16	Et je l'ai présenté à tes disciples, mais ils n'ont pas pu le guérir.
17:17	Et Yéhoshoua répondit et dit : Ô génération<!--Voir Mc. 9:19 ; Lu. 9:41 ; Ac. 2:40 ; Ph. 2:15.--> incrédule et déformée ! Jusqu'à quand serai-je avec vous ? Jusqu'à quand vous supporterai-je ? Amenez-le-moi ici.
17:18	Et Yéhoshoua réprimanda d'une manière tranchante le démon, qui sortit de lui, et à l'heure même l'enfant fut guéri.
17:19	Alors les disciples s'approchèrent de Yéhoshoua et lui dirent en particulier : Pourquoi n'avons-nous pas pu le chasser ?
17:20	Et Yéhoshoua leur dit : C'est à cause de votre incrédulité. Amen, je vous le dis, si vous avez de la foi comme un grain de sénevé, vous direz à cette montagne : Transporte-toi d'ici là et elle se transporterait ; et rien ne vous serait impossible.
17:21	Mais cette race ne sort que par la prière et par le jeûne<!--Le jeûne public fut prescrit par la loi mosaïque, observé annuellement le grand jour de l'expiation, le dixième jour du mois de Tishri (Septembre-Octobre). C'était un jeûne d'automne, quand la navigation était en général dangereuse à cause des tempêtes. Ac. 27:9.-->.

### Yéhoshoua annonce à nouveau sa mort et sa résurrection à ses disciples<!--Mc. 9:30-32 ; Lu. 9:44-45.-->

17:22	Et comme ils se trouvaient en Galilée, Yéhoshoua leur dit : Il arrivera que le Fils d'humain sera livré entre les mains des hommes,
17:23	et qu'ils le feront mourir, mais le troisième jour, il sera ressuscité. Et ils furent extrêmement attristés.

### Un statère dans la bouche d'un poisson<!--Mc. 12:13-17.-->

17:24	Et lorsqu'ils arrivèrent à Capernaüm, ceux qui percevaient les didrachmes<!--Une « didrachmon » ou double drachme, pièce d'argent égale à deux drachmes ou un Alexandrin, ou un demi-sicle.--> s'adressèrent à Petros et lui dirent : Votre Docteur ne paye-t-il pas les didrachmes ?
17:25	Oui, dit-il. Et quand il fut entré dans la maison, Yéhoshoua, prenant les devants, lui dit : Shim’ôn, qu'en penses-tu ? Les rois de la Terre, de qui perçoivent-ils les tributs ou les impôts ? Est-ce de leurs fils ou des étrangers ?
17:26	Petros lui dit : Des étrangers. Yéhoshoua lui dit : Les fils en sont donc exemptés.
17:27	Mais pour que nous ne les scandalisions pas, va à la mer et jette l'hameçon, et prends le premier poisson qui montera et, en lui ouvrant la bouche tu trouveras un statère<!--Un statère, une pièce de monnaie. Un statère d'argent équivalait à 4 drachmes grecques ou 4 deniers romains, un sicle juif.-->. Prends-le et donne-le-leur pour moi et pour toi.

## Chapitre 18

### L'humilité, le secret de la vraie grandeur<!--Mc. 9:33-37 ; Lu. 9:46-48.-->

18:1	En cette même heure-là, les disciples s'approchèrent de Yéhoshoua, en disant : Qui est le plus grand dans le Royaume des cieux ?
18:2	Et Yéhoshoua, ayant appelé un enfant, le mit au milieu d'eux
18:3	et leur dit : Amen, je vous le dis, si vous ne vous convertissez pas et si vous ne devenez pas comme les enfants, vous n'entrerez jamais dans le Royaume des cieux.
18:4	C'est pourquoi, quiconque s'abaissera<!--Mt. 23:12.--> comme cet enfant, celui-là est le plus grand dans le Royaume des cieux.
18:5	Et quiconque reçoit en mon Nom un enfant comme celui-ci, il me reçoit.

### Les scandales et les occasions de chute

18:6	Mais si quelqu'un était une occasion de chute pour un de ces petits qui croient en moi, il vaudrait mieux pour lui qu’on suspendit à son cou une meule à âne, et qu’on le plongeât au fond de la mer.
18:7	Malheur au monde à cause des scandales ! Car il est nécessaire qu'il arrive des scandales, mais malheur à l'être humain par qui le scandale arrive !
18:8	Et si ta main ou ton pied sont pour toi une occasion de chute, coupe-les et jette-les loin de toi. Car il est convenable pour toi d'entrer boiteux ou estropié dans la vie que d'avoir deux pieds ou deux mains et d'être jeté dans le feu éternel.
18:9	Et si ton œil est pour toi une occasion de chute, arrache-le et jette-le loin de toi. Car il est convenable pour toi d'entrer dans la vie n'ayant qu'un œil que d'avoir deux yeux et d'être jeté dans la géhenne de feu.
18:10	Veillez à ne pas mépriser un seul de ces petits, car je vous dis que dans les cieux, leurs anges voient continuellement la face de mon Père qui est dans les cieux.
18:11	Car le Fils d'humain est venu pour sauver ce qui était perdu.

### Parabole de la brebis égarée<!--Lu. 15:3-7.-->

18:12	Qu'en pensez-vous ? Si un certain homme a 100 brebis et que l'une d'elles s'égare, ne laissera-t-il pas les 99 sur les montagnes pour aller à la recherche de l'égarée ?
18:13	Et s'il arrive qu'il la trouve, amen, je vous le dis, il s'en réjouit plus que pour les 99 qui ne se sont pas égarées.
18:14	Ainsi ce n'est pas la volonté de votre Père qui est aux cieux qu'un seul de ces petits périsse.

### La discipline dans les assemblées

18:15	Mais si ton frère a péché contre toi, va et reprends-le entre toi et lui seul. S'il t'écoute, tu as gagné ton frère.
18:16	Mais, s'il ne t'écoute pas, prends encore avec toi une ou deux personnes, afin que par la bouche de deux ou trois témoins, toute parole soit ferme<!--De. 19:15.-->.
18:17	Mais s'il refuse de les écouter, dis-le à l'assemblée, et s'il refuse aussi d'écouter l'assemblée, qu'il soit pour toi comme un païen<!--Vient du grec « ethnikos » qui signifie « adapté au génie ou aux coutumes d'un peuple, ce qui est national », « manières ou langage des étrangers, ce qui est étranger », « dans la nouvelle alliance il s'agit de la nature des païens, par rapport au culte du vrai Elohîm, le paganisme ».--> et comme un publicain.
18:18	Amen, je vous le dis, tout ce que vous lierez sur la Terre sera lié dans le ciel et tout ce que vous délierez sur la Terre sera délié dans le ciel<!--Voir commentaire en Mt. 16:19.-->.
18:19	Je vous dis encore que si deux d'entre vous se mettent d'accord sur la Terre, tout ce qu'ils demanderont leur sera donné par mon Père qui est dans les cieux.
18:20	Car là où deux ou trois sont rassemblés en mon Nom, je suis là au milieu d'eux.

### Le pardon

18:21	Alors Petros s'étant approché, lui dit : Seigneur, combien de fois pardonnerai-je à mon frère, lorsqu'il péchera contre moi ? Jusqu'à 7 fois ?
18:22	Yéhoshoua lui dit : Je ne te dis pas jusqu'à 7 fois, mais jusqu'à 70 fois 7 fois.

### Parabole du roi et du méchant esclave

18:23	C'est pourquoi le Royaume des cieux est semblable à un être humain, un roi qui voulut faire rendre compte à ses esclaves.
18:24	Et quand il se mit à compter, on lui en présenta un qui lui devait 10 000 talents.
18:25	Et parce qu'il n'avait pas de quoi payer, son maître ordonna qu'il soit vendu, lui, sa femme, ses enfants et tout ce qu'il avait, et que la dette soit payée.
18:26	Mais cet esclave, se jetant à ses pieds, le suppliait, en disant : Seigneur, aie patience envers moi et je te rendrai le tout.
18:27	Mais le seigneur de cet esclave, ému de compassion, le relâcha, et lui remit la dette.
18:28	Mais cet esclave, étant sorti, rencontra un de ses compagnons de service, qui lui devait 100 deniers. Et l'ayant pris, il l'étranglait, en lui disant : Paye-moi ce que tu me dois.
18:29	Mais son compagnon de service se jetant à ses pieds, le suppliait, en disant : Sois patient avec moi et je te payerai tout.
18:30	Mais lui ne voulait pas, et il alla le jeter en prison, jusqu'à ce qu'il ait payé la dette.
18:31	Or ses autres compagnons de service, voyant ce qui était arrivé, en furent extrêmement attristés et ils allèrent raconter à leur seigneur tout ce qui s'était passé.
18:32	Alors, le faisant venir, son seigneur lui dit : Méchant esclave ! Je t'avais remis en entier ta dette, parce que tu m'en avais supplié.
18:33	Ne te fallait-il pas aussi avoir pitié de ton compagnon de service, comme moi-même j'avais eu pitié de toi ?
18:34	Et son seigneur étant en colère, le livra aux bourreaux, jusqu'à ce qu'il lui ait payé tout ce qu'il devait.
18:35	C'est ainsi que vous fera mon Père céleste, si vous ne pardonnez de tout votre cœur, chacun à son frère, ses fautes.

## Chapitre 19

### Yéhoshoua enseigne sur le mariage et le divorce<!--Mt. 5:31-32 ; Mc. 10:2-12 ; Lu. 16:18 ; Ro. 7:1-3 ; 1 Co. 7:10-16.-->

19:1	Et il arriva que, quand Yéhoshoua eut achevé ces discours, il quitta la Galilée et alla dans le territoire de la Judée, au-delà du Yarden.
19:2	Et de grandes foules le suivirent, et là, il les guérit.
19:3	Et les pharisiens vinrent à lui pour l'éprouver et ils lui dirent : Est-il légal pour un homme de répudier sa femme pour quelque cause que ce soit ?
19:4	Et il répondit et leur dit : N'avez-vous pas lu que Celui qui les a faits dès le commencement, les a faits mâle et femelle,
19:5	et qu'il a dit : À cause de cela l'homme quittera son père et sa mère et se joindra à sa femme, et les deux seront une seule chair ?
19:6	Ainsi ils ne sont plus deux, mais une seule chair<!--Ge. 2:24.-->. Que l'homme donc ne sépare pas ce qu'Elohîm a mis ensemble sous un joug<!--La majorité des traducteurs traduisent ce verset par « Que l'homme donc ne sépare pas ce que Dieu a joint ». Or le terme grec « suzeugnumi » qu'ils ont traduit par « joint » signifie plutôt « attacher un joug à quelqu'un, mettre ensemble sous un joug ». Un joug est une pièce de bois servant à atteler une paire d'animaux. De ce fait, les animaux sont contraints d'avancer dans la même direction, côte à côte (Ge. 2:22). En De. 22:10, Elohîm interdit d'atteler un âne avec un bœuf ensemble. L'adverbe « ensemble » vient de l'hébreu « yachad » et signifie « union d'une façon unitaire ». Ce verset fait référence symboliquement aux paroles de Paulos (Paul) en 2 Co. 6:14-16 qui nous mettent en garde contre le mariage avec des infidèles. Le mariage est donc semblable à un joug qui nous contraint à marcher à l'unisson dans la même direction. Ainsi, si on se lie à un inconverti, ce dernier risque de nous entraîner sur la voie de la perdition. En Mt. 11:29, le Mashiah nous invite à nous mettre sous son joug, qui est doux et léger. Quelle belle demande en mariage !-->.
19:7	Ils lui disent : Pourquoi donc, Moshé a-t-il commandé de donner une lettre de répudiation et de la répudier<!--De. 24:1.--> ?
19:8	Il leur dit : C'est à cause de la dureté<!--De. 29:18 ; Jé. 3:17, 7:24, 9:14, 11:8, 13:10, 16:12, 18:12, 23:17 ; Ps. 81:13.--> de votre cœur que Moshé vous a permis de répudier vos femmes, mais dès le commencement, il n'en était pas ainsi.
19:9	Mais moi je vous dis que celui qui aura répudié sa femme, si ce n'est pour relation sexuelle illicite<!--Vient du grec « porneia », c'est-à-dire « relation sexuelle illicite, impudicité ».-->, et en épouse une autre commet un adultère, et celui qui aura épousé une femme répudiée commet un adultère.
19:10	Ses disciples lui disent : Si telle est la condition de l'homme à l'égard de sa femme, il ne convient pas de se marier.
19:11	Mais il leur dit : Tous ne laissent pas un espace à cette parole, mais ceux-là à qui c'est donné.
19:12	Car il y a des eunuques qui sont nés ainsi du ventre de leur mère, et il y a des eunuques qui ont été castrés par les humains, et il y a des eunuques qui se sont eux-mêmes castrés à cause du Royaume des cieux. Que celui qui est capable de laisser un espace, laisse un espace !

### Yéhoshoua et les enfants<!--Mc. 10:13-16 ; Lu. 18:15-17.-->

19:13	Alors on lui apporta des enfants, afin qu'il leur imposât les mains et qu'il priât. Mais les disciples les réprimandaient d'une manière tranchante.
19:14	Et Yéhoshoua leur dit : Laissez les enfants, ne les empêchez pas de venir à moi, car le Royaume des cieux est pour ceux qui leur ressemblent.
19:15	Et leur ayant imposé les mains, il partit de là.

### Le jeune homme riche<!--Mc. 10:17-31 ; Lu. 10:25-37, 18:18-27.-->

19:16	Et voici, quelqu'un s'approchant, lui dit : Bon Docteur, que dois-je faire de bon pour avoir la vie éternelle ?
19:17	Mais il lui dit : Pourquoi m'appelles-tu bon ? Personne n'est bon, excepté un seul : Elohîm. Mais si tu veux entrer dans la vie, garde les commandements.
19:18	Il lui dit : Lesquels ? Et Yéhoshoua lui dit : Tu n'assassineras pas. Tu ne commettras pas d'adultère. Tu ne voleras pas. Tu ne diras pas de faux témoignage.
19:19	Honore ton père et ta mère. Et tu aimeras ton prochain comme toi-même<!--Ex. 20:12-16 ; Lé. 19:18.-->.
19:20	Le jeune homme lui dit : J'ai gardé toutes ces choses dès ma jeunesse. Que me manque-t-il encore ?
19:21	Yéhoshoua lui dit : Si tu veux être parfait, va, vends ce que tu as et donne-le aux pauvres, et tu auras un trésor dans le ciel. Et viens et suis-moi.
19:22	Mais quand ce jeune homme eut entendu cette parole, il s'en alla tout triste, car il avait beaucoup de propriétés.
19:23	Mais Yéhoshoua dit à ses disciples : Amen, je vous le dis, un riche entrera difficilement dans le Royaume des cieux.
19:24	Mais je vous le dis encore : Il est plus aisé à un chameau de passer par le trou d'une aiguille<!--Le trou d'une aiguille : il est peut-être question d'une entrée de la ville de Yeroushalaim (Jérusalem) qui était trop basse pour que les chameaux puissent y passer avec leurs chargements.-->, qu'il ne l'est à un riche d'entrer dans le Royaume d'Elohîm.
19:25	Mais ses disciples, ayant entendu ces choses, furent extrêmement choqués et disaient : Qui peut donc être sauvé ?
19:26	Mais Yéhoshoua les regardant, leur dit : Pour les humains, c'est impossible, mais pour Elohîm tout est possible.

### La récompense des disciples du Mashiah<!--Mc. 10:28-31 ; Lu. 18:28-30.-->

19:27	Alors Petros prenant la parole, lui dit : Voici, nous avons tout quitté et nous t'avons suivi. Qu'en sera-t-il donc pour nous ?
19:28	Mais Yéhoshoua leur dit : Amen, je vous le dis, à vous qui m'avez suivi : à la nouvelle naissance, lorsque le Fils d'humain s'assiéra sur son trône de gloire, vous aussi vous serez assis sur douze trônes, jugeant les douze tribus d'Israël.
19:29	Et quiconque aura quitté ou maisons, ou frères, ou sœurs, ou père, ou mère, ou femme, ou enfants, ou champs, à cause de mon Nom, il en recevra cent fois autant et héritera la vie éternelle.
19:30	Mais beaucoup des premiers seront les derniers, et beaucoup des derniers, les premiers.

## Chapitre 20

### Parabole des ouvriers

20:1	Car le Royaume des cieux est semblable à un être humain, un maître de maison qui sortit dès le point du jour afin de louer des ouvriers pour sa vigne.
20:2	Et s'étant mis d'accord avec les ouvriers pour un denier par jour, il les envoya à sa vigne.
20:3	Et sortant vers la troisième heure, il en vit d'autres qui se tenaient sur la place du marché, sans travail.
20:4	Il leur dit : Allez aussi à ma vigne et je vous donnerai ce qui sera juste.
20:5	Et ils y allèrent. Et il sortit de nouveau vers la sixième heure et vers la neuvième, et il fit de même.
20:6	Et étant sorti vers la onzième heure, il en trouva d'autres qui se tenaient là sans travail et leur dit : Pourquoi vous tenez-vous ici toute la journée sans travail ?
20:7	Ils lui disent : Parce que personne ne nous a loués. Et il leur dit : Allez vous aussi à ma vigne et vous recevrez ce qui sera juste.
20:8	Et le soir étant venu, le maître de la vigne dit à son intendant : Appelle les ouvriers et paye-leur le salaire, en commençant depuis les derniers jusqu'aux premiers.
20:9	Et ceux qui avaient été loués vers la onzième heure vinrent et reçurent chacun un denier.
20:10	Mais quand les premiers furent venus, ils pensèrent qu'ils recevraient davantage, mais ils reçurent aussi chacun un denier.
20:11	Et l'ayant reçu, ils murmuraient contre le maître de maison,
20:12	en disant : Ces derniers n'ont travaillé qu'une heure, et tu les as faits égaux à nous qui avons porté le fardeau du jour et de la chaleur.
20:13	Mais il répondit à l'un d'eux et lui dit : Compagnon, je ne te fais pas de tort. N'es-tu pas tombé d'accord avec moi pour un denier ?
20:14	Prends ce qui est à toi et va-t'en. Mais si je veux donner à ce dernier autant qu'à toi,
20:15	n'est-il pas légal pour moi de faire ce que je veux de mes biens ? Ton œil est-il mauvais parce que moi, je suis bon ?
20:16	Ainsi les derniers seront les premiers, et les premiers, les derniers. Car il y a beaucoup d'appelés, mais peu d'élus.

### Yéhoshoua annonce à nouveau sa mort et sa résurrection à ses disciples<!--Mt. 12:38-42, 16:21-28, 17:22-23 ; Mc. 10:32-34 ; Lu. 18:31-34.-->

20:17	Et comme Yéhoshoua montait à Yeroushalaim, il prit à part ses douze disciples et il leur dit en chemin :
20:18	Voici, nous montons à Yeroushalaim, et le Fils d'humain sera livré aux principaux prêtres et aux scribes. Et ils le condamneront à mort.
20:19	Et ils le livreront aux nations pour qu'elles se moquent de lui, et le châtient avec un fouet et le crucifient, et le troisième jour il ressuscitera.

### Yéhoshoua répond à la question de la mère de Yaacov (Jacques) et de Yohanan (Jean)<!--Mc. 10:35-45.-->

20:20	Alors la mère des fils de Zabdi s'approcha de lui avec ses fils, l'adorant et lui demandant quelque chose.
20:21	Et il lui dit : Que veux-tu ? Elle lui dit : Ordonne que mes deux fils, qui sont ici, soient assis l'un à ta droite et l'autre à ta gauche, dans ton Royaume.
20:22	Et Yéhoshoua répondit et dit : Vous ne savez pas ce que vous demandez. Pouvez-vous boire la coupe que moi, je suis sur le point de boire ou être baptisés du baptême dont je dois être baptisé ? Ils lui disent : Nous le pouvons.
20:23	Et il leur dit : En effet, vous boirez ma coupe et vous serez baptisés du baptême dont je serai baptisé. Mais pour ce qui est d'être assis à ma droite ou à ma gauche, il ne m'appartient de le donner qu'à ceux pour qui cela a été préparé<!--Voir Jn. 14:2-3.--> par mon Père.
20:24	Et les dix, ayant entendu cela, furent indignés contre les deux frères.
20:25	Mais Yéhoshoua les appela et leur dit : Vous savez que les chefs des nations dominent sur elles en maîtres et que les grands leur font sentir leur autorité.
20:26	Mais il n'en sera pas ainsi parmi vous. Au contraire, si quelqu'un veut devenir grand parmi vous, qu'il soit votre serviteur,
20:27	et si quelqu'un veut être le premier parmi vous, qu'il soit votre esclave.
20:28	De même que le Fils d'humain n'est pas venu pour être servi, mais pour servir et donner son âme en rançon pour beaucoup.

### Yéhoshoua guérit deux aveugles<!--Mc. 10:46-52 ; Lu. 18:35-43.-->

20:29	Et comme ils partaient de Yeriycho<!--Jéricho.-->, une grande foule le suivit.
20:30	Et voici que deux aveugles assis au bord du chemin, entendant que Yéhoshoua passait, crièrent, en disant : Seigneur, Fils de David ! Aie pitié de nous !
20:31	Et la foule les réprimandait d'une manière tranchante pour les faire taire, mais ils criaient encore plus fort : Seigneur, Fils de David ! Aie pitié de nous !
20:32	Et s'arrêtant, Yéhoshoua les appela et leur dit : Que voulez-vous que je fasse pour vous ?
20:33	Ils lui disent : Seigneur, que nos yeux soient ouverts !
20:34	Et Yéhoshoua, ému de compassion, toucha leurs yeux, et immédiatement ils recouvrèrent la vue et le suivirent.

## Chapitre 21

### Yéhoshoua ha Mashiah (Jésus-Christ) entre dans Yeroushalaim (Jérusalem)<!--Za. 9:9 ; Mc. 11:1-11 ; Lu. 19:28-40 ; Jn. 12:12-19.-->

21:1	Et quand ils approchèrent de Yeroushalaim et qu'ils furent arrivés à Bethphagé, vers le Mont des Oliviers, Yéhoshoua envoya alors deux disciples,
21:2	en leur disant : Allez au village qui est devant vous, et immédiatement vous trouverez une ânesse attachée et son ânon avec elle. Détachez-les et amenez-les-moi.
21:3	Et si quelqu'un vous dit quelque chose, vous direz que le Seigneur en a besoin. Et immédiatement il les laissera aller.
21:4	Or tout cela arriva afin que s'accomplît ce qui a été annoncé par le moyen du prophète, disant :
21:5	Dites à la fille de Sion : Voici, ton Roi vient à toi, plein de douceur et monté sur un âne, sur un ânon, le fils d'une ânesse<!--Za. 9:9.-->.
21:6	Et les disciples allèrent donc et firent ce que Yéhoshoua leur avait ordonné.
21:7	Ils amenèrent l'ânesse et l'ânon et mirent leurs vêtements sur eux, et il s'assit dessus.
21:8	Et la plus grande partie de la foule étendit ses vêtements sur le chemin, tandis que d'autres coupaient des rameaux des arbres et les étendaient sur le chemin.
21:9	Et les foules qui allaient devant et celles qui suivaient, criaient en disant : Hosanna au Fils de David ! Béni soit celui qui vient au Nom du Seigneur ! Hosanna dans les lieux très hauts !
21:10	Et lorsqu'il entra dans Yeroushalaim, toute la ville fut secouée et l’on disait : Qui est celui-ci ?
21:11	Et les foules disaient : C'est Yéhoshoua, le prophète de Nazareth, en Galilée.

### Yéhoshoua chasse les marchands du temple<!--Mc. 11:15-18 ; Lu. 19:45-46 ; Jn. 2:13-16.-->

21:12	Et Yéhoshoua entra dans le temple d'Elohîm. Et il chassa dehors tous ceux qui vendaient et qui achetaient dans le temple, et il renversa les tables des changeurs et les sièges des vendeurs des pigeons.
21:13	Et il leur dit : Il est écrit : Ma maison sera appelée une maison de prière. Mais vous, vous en avez fait une caverne de brigands<!--Es. 56:7 ; Jé. 7:11.-->.
21:14	Et des aveugles et des boiteux s'approchèrent de lui dans le temple. Et il les guérit.
21:15	Mais les principaux prêtres et les scribes furent indignés à la vue des choses merveilleuses qu'il faisait, et des enfants qui criaient dans le temple et qui disaient : Hosanna au Fils de David !
21:16	Et ils lui dirent : Entends-tu ce qu'ils disent ? Oui, leur dit Yéhoshoua. N'avez-vous jamais lu : Tu as tiré des louanges de la bouche des enfants et de ceux qui sont à la mamelle<!--Ps. 8:3.--> ?
21:17	Et, les ayant laissés, il sortit de la ville, pour aller à Béthanie, et il y passa la nuit.

### Le figuier stérile<!--Mc. 11:12-14, 20-26.-->

21:18	Et le matin, comme il retournait à la ville, il eut faim.
21:19	Et voyant un figuier qui était sur le chemin, il s'en approcha, mais il n'y trouva que des feuilles, et il lui dit : Qu'aucun fruit ne naisse plus de toi, à jamais ! Et immédiatement le figuier sécha.
21:20	Les disciples qui virent cela, furent étonnés et dirent : Comment ce figuier est-il devenu sec immédiatement ?
21:21	Mais Yéhoshoua leur répondit : Amen, je vous le dis, si vous avez de la foi et si vous n'hésitez pas, non seulement vous ferez ce qui a été fait à ce figuier, mais quand vous direz à cette montagne : Ôte-toi de là et jette-toi dans la mer, cela arrivera.
21:22	Et tout ce que vous demanderez par la prière, si vous croyez, vous le recevrez.

### L'autorité de Yéhoshoua et celle de Yohanan le Baptiste<!--Mc. 11:27-33 ; Lu. 20:1-8.-->

21:23	Et s'étant rendu dans le temple, les principaux prêtres et les anciens du peuple vinrent auprès de lui, pendant qu'il enseignait, et lui dirent : Par quelle autorité fais-tu ces choses et qui t'a donné cette autorité ?
21:24	Mais Yéhoshoua répondant, leur dit : Moi aussi je vous interrogerai : une seule parole ! Si vous me répondez, je vous dirai par quelle autorité je fais ces choses.
21:25	Le baptême de Yohanan, d'où venait-il ? Du ciel ou des humains ? Mais ils raisonnèrent entre eux, disant : Si nous disons : Du ciel, il nous dira : Pourquoi donc ne l'avez-vous pas cru ?
21:26	Et si nous disons : Des humains, nous avons à craindre la foule, car tous tiennent Yohanan pour un prophète.
21:27	Et ils répondirent à Yéhoshoua en disant : Nous ne savons pas ! Et il leur dit : Moi non plus, je ne vous dirai pas par quelle autorité je fais ces choses.

### Parabole des deux fils

21:28	Mais que pensez-vous de ceci ? Un homme avait deux enfants. Et s'adressant au premier, il lui dit : Mon enfant, va travailler aujourd'hui dans ma vigne.
21:29	Il répondit et dit : Je ne veux pas y aller. Ensuite, il se repentit et y alla.
21:30	Et s’approchant du second, il dit la même chose. Celui-ci répondit et dit : Moi, seigneur. Mais il n'alla pas.
21:31	Lequel des deux a fait la volonté du père ? Ils lui disent : Le premier. Yéhoshoua leur dit : Amen, je vous le dis, les publicains et les prostituées vous devanceront dans le Royaume d'Elohîm.
21:32	Car Yohanan est venu à vous dans la voie de la justice et vous ne l'avez pas cru. Mais les publicains et les femmes débauchées ont cru en lui. Et vous, qui avez vu cela, vous ne vous êtes pas ensuite repentis pour croire en lui.

### Parabole des vignerons<!--Es. 5:1-7 ; Mc. 12:1-12 ; Lu. 20:9-18.-->

21:33	Écoutez une autre parabole : Il y avait un homme, un maître de maison, qui planta une vigne, et mit autour une clôture, et y creusa un pressoir et bâtit une tour. Et il la laissa en location à des vignerons et partit pour un pays lointain.
21:34	Mais quand le temps des fruits approcha, il envoya ses esclaves vers les vignerons pour recevoir ses fruits.
21:35	Mais les vignerons s'étant saisis de ses esclaves, fouettèrent l'un, et tuèrent l'autre, et lapidèrent le troisième.
21:36	Il envoya encore d'autres esclaves, en plus grand nombre que les premiers, et ils leur firent de même.
21:37	Et enfin, il envoya vers eux son propre fils, en se disant : Ils respecteront mon fils.
21:38	Mais, quand les vignerons virent le fils, ils se dirent entre eux : Voici l'héritier. Venez, tuons-le et emparons-nous de son héritage !
21:39	Et s'étant saisis de lui, ils le jetèrent hors de la vigne et le tuèrent.
21:40	Quand donc le maître de la vigne viendra, que fera-t-il à ces vignerons ?
21:41	Ils lui disent : Il fera périr misérablement ces méchants et laissera en location sa vigne à d'autres vignerons qui lui en rendront les fruits en leur temps.
21:42	Yéhoshoua leur dit : N'avez-vous jamais lu dans les Écritures : La pierre qu'ont rejetée ceux qui bâtissaient est devenue la tête de l'angle. C'est par le Seigneur qu'elle l'est devenue, et elle est merveilleuse à nos yeux<!--Jg. 13:8 ; Es. 8:13-17, 28:16.--> ?
21:43	C'est pourquoi je vous dis que le Royaume d'Elohîm vous sera enlevé, et il sera donné<!--Voir Da. 4:14 ; 1 Ch. 29:25.--> à une nation qui en rendra les fruits.
21:44	Celui qui tombera sur cette pierre, s'y brisera, et celui sur qui elle tombera, sera écrasé.
21:45	Et après avoir entendu ses paraboles, les principaux prêtres et les pharisiens comprirent qu'il parlait d'eux.
21:46	Et ils cherchaient à se saisir de lui, mais ils craignaient les foules, parce qu'elles le tenaient pour un prophète.

## Chapitre 22

### Parabole des noces<!--Lu. 14:16-24.-->

22:1	Et Yéhoshoua, répondant, leur parla de nouveau en paraboles, disant :
22:2	Le Royaume des cieux est semblable à un être humain, un roi qui fit des noces pour son fils.
22:3	Et il envoya ses esclaves pour appeler ceux qui avaient été conviés aux noces, mais ils ne voulurent pas venir.
22:4	De nouveau il envoya d’autres esclaves, en disant : Dites aux conviés : Voici, j'ai préparé mon festin, mes bœufs et mes bêtes grasses sont tués, et tout est prêt. Venez aux noces.
22:5	Mais, sans tenir compte de l'invitation, ils s'en allèrent l'un à son champ et l'autre à son trafic.
22:6	Et les autres se saisirent de ses esclaves, les outragèrent et les tuèrent.
22:7	Mais quand le roi l'entendit, il se mit en colère. Il envoya ses bandes de soldats, fit périr ces meurtriers et brûla leur ville.
22:8	Alors il dit à ses esclaves : En effet les noces sont prêtes, mais les conviés n'en étaient pas dignes.
22:9	Allez donc dans les carrefours des chemins, et autant de gens que vous trouverez, appelez-les aux noces.
22:10	Et ces esclaves allèrent dans les chemins et rassemblèrent tous ceux qu'ils trouvèrent, méchants et bons, et les noces se remplirent de gens qui étaient à table.
22:11	Et le roi, étant entré pour voir ceux qui étaient à table, aperçut là un homme qui n'avait pas revêtu un habit de noces<!--Ap. 19:7-8.-->.
22:12	Et il lui dit : Compagnon, comment es-tu entré ici sans avoir un habit de noces ? Cet homme eut la bouche muselée.
22:13	Alors le roi dit aux serviteurs : Liez-lui les pieds et les mains, emportez-le et jetez-le dans la ténèbre de dehors : là seront les pleurs et le grincement des dents.
22:14	Car il y a beaucoup d'appelés, mais peu d'élus.

### Le tribut à César<!--Mc. 12:13-17 ; Lu. 20:19-26.-->

22:15	Alors les pharisiens allèrent tenir conseil afin de le prendre au piège en parole.
22:16	Et ils envoyèrent auprès de lui leurs disciples avec des hérodiens, en disant : Docteur, nous savons que tu es véritable et que tu enseignes la voie d'Elohîm selon la vérité, et que tu ne considères personne, car tu ne regardes pas à l'apparence des gens.
22:17	Dis-nous donc ce que tu en penses : Est-il légal de payer le tribut à César, ou non ?
22:18	Mais Yéhoshoua connaissant leur malice, dit : Hypocrites ! Pourquoi me tentez-vous ?
22:19	Montrez-moi la monnaie avec laquelle on paie le tribut. Et ils lui présentèrent un denier.
22:20	Et il leur dit : De qui est cette image et cette inscription ?
22:21	De César, lui disent-ils. Alors il leur dit : Rendez donc à César ce qui est à César, et à Elohîm, ce qui est à Elohîm.
22:22	Et ayant entendu cela, ils furent dans l'admiration, et l'ayant laissé, ils s'en allèrent.

### Enseignement sur la résurrection<!--Mc. 12:18-27 ; Lu. 20:27-38.-->

22:23	Le même jour, les sadducéens qui disent qu'il n'y a pas de résurrection, vinrent auprès de lui et lui posèrent cette question,
22:24	en disant : Docteur, Moshé a dit : Si quelqu'un meurt sans enfants, son frère épousera sa femme et suscitera une postérité à son frère.
22:25	Or il y avait parmi nous sept frères. Le premier se maria et mourut et, n'ayant pas eu d'enfants, il laissa sa femme à son frère.
22:26	Il en fut de même du deuxième, et du troisième, jusqu'au septième.
22:27	Mais après eux tous, la femme mourut aussi.
22:28	À la résurrection, duquel des sept sera-t-elle la femme ? Car tous l'ont eue.
22:29	Mais Yéhoshoua répondant, leur dit : Vous vous êtes égarés, parce que vous ne connaissez ni les Écritures, ni la puissance d'Elohîm.
22:30	Car à la résurrection, ils ne se marient pas et ils ne donnent pas en mariage, mais ils sont comme des anges d'Elohîm dans le ciel.
22:31	Et quant à la résurrection des morts, n'avez-vous pas lu ce qu'Elohîm vous a déclaré, en disant :
22:32	Moi, je suis l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov<!--Ge. 17:7, 26:24, 28:21.--> ? Or Elohîm n'est pas l'Elohîm des morts, mais des vivants.
22:33	Et les foules qui écoutaient étaient choquées par sa doctrine.

### Le grand commandement<!--Mc. 12:28-34 ; Lu. 10:25-28.-->

22:34	Mais quand les pharisiens apprirent qu'il avait muselé la bouche aux sadducéens, ils se rassemblèrent dans un même lieu,
22:35	et l'un d'eux, qui était docteur de la torah, l'interrogea pour l'éprouver, en disant :
22:36	Docteur, quel est le grand commandement dans la torah ?
22:37	Mais Yéhoshoua lui dit : Tu aimeras le Seigneur ton Elohîm, de tout ton cœur, de toute ton âme et de toute ta pensée<!--De. 6:4-5.-->.
22:38	C'est là le premier et le grand commandement.
22:39	Et voici le deuxième qui lui est semblable : Tu aimeras ton prochain comme toi-même<!--Lé. 19:18.-->.
22:40	À ces deux commandements sont suspendus toute la torah et les prophètes.

### Yéhoshoua interroge les pharisiens au sujet du Mashiah<!--Mc. 12:35-37 ; Lu. 20:39-44.-->

22:41	Mais comme les pharisiens étaient rassemblés, Yéhoshoua les interrogea,
22:42	en disant : Que pensez-vous du Mashiah ? De qui est-il Fils ? Ils lui disent : De David.
22:43	Et il leur dit : Comment donc David, par l'Esprit, l'appelle-t-il Seigneur, en disant :
22:44	Le Seigneur a dit à mon Seigneur, assieds-toi à ma droite, jusqu'à ce que j'aie mis tes ennemis pour le marchepied de tes pieds<!--Ps. 110:1.--> ?
22:45	Si donc David l'appelle Seigneur, comment est-il son Fils ?
22:46	Et personne ne pouvait lui répondre un seul mot. Et, depuis ce jour-là, personne n’osa plus l’interroger.

## Chapitre 23

### Yéhoshoua dénonce les scribes et les pharisiens<!--Mc. 12:38-40 ; Lu. 11:39-54, 20:45-47.-->

23:1	Alors Yéhoshoua parla aux foules et à ses disciples,
23:2	en disant : Les scribes et les pharisiens sont assis dans la chaire de Moshé.
23:3	Faites donc et observez toutes les choses qu'ils vous diront d'observer, mais ne faites pas selon leurs œuvres, parce qu'ils disent et ne font pas.
23:4	Car ils lient ensemble des fardeaux et des lourdes charges<!--Voir Mt. 11:28 ; 1 Jn. 5:3.--> et les mettent sur les épaules des gens, mais ils ne veulent pas les remuer de leur doigt.
23:5	Et ils font toutes leurs œuvres pour être vus des humains. Ainsi, ils portent de larges phylactères<!--Un moyen de protection, amulette, talisman. Bande de parchemin sur laquelle étaient inscrits de brefs passages de la torah de Moshé (Moïse) : Ex. 13:1-10,11-16 ; De. 6:4-9, 11:13-21, placés dans de petites boîtes, sur le front ou au bras gauche.--> et de longues franges<!--Un petit accessoire fait de laine tissée, qui pendait au bord du manteau ou du vêtement. Les Juifs avaient de tels accessoires fixés à leur manteau pour leur rappeler la torah.--> à leurs vêtements.
23:6	Ils aiment le premier siège dans les soupers et les premiers sièges dans les synagogues,
23:7	et les salutations sur les places du marché et à être appelés par les gens : Rabbi ! Rabbi !
23:8	Mais vous, ne vous faites pas appeler Rabbi, car un seul est votre Guide, le Mashiah, et vous êtes tous frères.
23:9	Et n'appelez personne sur la Terre votre Père, car un seul est votre Père, celui qui est dans les cieux.
23:10	Ne vous faites pas non plus appeler guides, car un seul est votre Guide, le Mashiah.
23:11	Mais le plus grand parmi vous sera votre serviteur<!--Domestique.-->.
23:12	Car quiconque s'élèvera sera abaissé et quiconque s'abaissera sera élevé.
23:13	Mais malheur à vous, scribes et pharisiens hypocrites ! Parce que vous fermez le Royaume des cieux aux gens ! Vous n'y entrez pas vous-mêmes et vous ne laissez pas entrer ceux qui veulent y entrer.
23:14	Mais malheur à vous, scribes et pharisiens hypocrites ! Parce que vous dévorez les maisons des veuves, tout en prétextant de faire de longues prières, c'est pourquoi vous en recevrez une plus grande condamnation.
23:15	Malheur à vous, scribes et pharisiens hypocrites ! Parce que vous parcourez la mer et la terre pour faire un prosélyte, et quand il l'est devenu, vous en faites un fils de la géhenne deux fois pire que vous.
23:16	Malheur à vous guides aveugles, qui dites : Si quelqu'un a juré par le temple, ce n'est rien, mais si quelqu'un a juré par l'or du temple, il est engagé.
23:17	Insensés et aveugles ! Car lequel est le plus grand, l'or, ou le temple qui sanctifie l'or ?
23:18	Et si quelqu'un a juré par l'autel, ce n'est rien, mais celui qui a juré par l'offrande qui est dessus, il est redevable.
23:19	Insensés et aveugles ! Car lequel est le plus grand, l'offrande, ou l'autel qui sanctifie l'offrande ?
23:20	Celui donc qui jure par l'autel, jure par l'autel et par toutes les choses qui sont dessus,
23:21	et celui qui jure par le temple, jure par le temple et par celui qui y habite,
23:22	et celui qui jure par le ciel, jure par le trône d'Elohîm et par celui qui y est assis.
23:23	Malheur à vous, scribes et pharisiens hypocrites, parce que vous payez la dîme<!--Voir commentaire en Mal. 3:10.--> de la menthe, de l'aneth et du cumin et que vous laissez de côté les charges les plus lourdes de la torah : la justice, la miséricorde et la fidélité. Il fallait pratiquer ces choses-là, sans négliger les autres choses.
23:24	Guides aveugles ! Vous coulez le moucheron et vous engloutissez le chameau<!--Les pharisiens filtraient leur eau par crainte d'avaler un moucheron.-->.
23:25	Malheur à vous, scribes et pharisiens hypocrites, parce que vous nettoyez le dehors de la coupe et du plat, alors qu'au-dedans, ils sont pleins de pillage et de manque d'auto-contrôle.
23:26	Pharisien aveugle ! Nettoie premièrement l'intérieur de la coupe et du plat, afin que l'extérieur aussi devienne pur.
23:27	Malheur à vous, scribes et pharisiens hypocrites, parce que vous ressemblez à des tombes blanchies, qui en effet, paraissent belles au-dehors, et qui au-dedans sont pleines d'ossements de morts et de toute sorte d'impureté.
23:28	Ainsi, vous aussi, au dehors vous paraissez justes aux gens en effet, mais au-dedans, vous êtes pleins d'hypocrisie et de violation de la torah.
23:29	Malheur à vous, scribes et pharisiens hypocrites, parce que vous bâtissez les tombes des prophètes et vous ornez les sépulcres des justes,
23:30	et vous dites : Si nous avions vécu du temps de nos pères, nous ne nous serions pas associés à eux pour verser le sang des prophètes.
23:31	Ainsi vous témoignez contre vous-mêmes que vous êtes les fils de ceux qui ont assassiné les prophètes.
23:32	Et vous achevez de remplir la mesure de vos pères.
23:33	Serpents, progénitures de vipères ! Comment éviterez-vous le supplice de la géhenne ?
23:34	C’est pourquoi, voici que moi j’envoie vers vous des prophètes, et des sages et des scribes. Et vous tuerez et crucifierez les uns, vous châtierez avec un fouet les autres dans vos synagogues et vous les persécuterez de ville en ville,
23:35	afin que vienne sur vous tout le sang innocent qui a été répandu sur la Terre, depuis le sang d'Abel le juste, jusqu'au sang de Zekaryah, fils de Berekyah, que vous avez assassiné entre le temple et l'autel.
23:36	Amen, je vous le dis, toutes ces choses viendront sur cette génération.

### Lamentations de Yéhoshoua sur Yeroushalaim (Jérusalem)<!--Jé. 22:5 ; Lu. 13:34-35, 19:41-44.-->

23:37	Yeroushalaim ! Yeroushalaim ! Qui tues les prophètes et qui lapides ceux qui te sont envoyés, combien de fois ai-je voulu rassembler tes enfants, comme la poule rassemble ses poussins sous ses ailes<!--Ps. 17:8.-->, et vous ne l'avez pas voulu !
23:38	Voici que votre maison vous est laissée déserte.
23:39	Car, je vous le dis, vous ne me verrez plus désormais, jusqu'à ce que vous disiez : Béni soit celui qui vient au Nom du Seigneur<!--Ps. 118:26.--> !

## Chapitre 24

### Prophétie sur la destruction du temple de Yeroushalaim (Jérusalem)<!--Mc. 13:1-2 ; Lu. 21:5-6.-->

24:1	Et comme Yéhoshoua sortait et s'en allait du temple, ses disciples s'approchèrent de lui pour lui faire remarquer les constructions du temple.
24:2	Mais Yéhoshoua leur dit : Voyez-vous bien toutes ces choses ? Amen, je vous le dis, il ne sera jamais laissé ici pierre sur pierre qui ne soit démolie.

### Les disciples interrogent Yéhoshoua<!--Mc. 13:3-4 ; Lu. 21:7.-->

24:3	Mais s'étant assis sur la Montagne des Oliviers, ses disciples vinrent à lui en particulier et lui dirent : Dis-nous quand ces choses arriveront-elles, et quel sera le signe de ta parousie<!--Du grec « parousia » qui signifie « présence », « la venue », « l'arrivée », « l'avènement ».--> et de l'achèvement de l'âge<!--Voir Mt. 13:36-43 et 49.--> ?

### Les temps de la fin<!--Da. 9:27 ; Mc. 13:5-13 ; Lu. 21:8-11.-->

24:4	Et Yéhoshoua répondant, leur dit : Prenez garde que personne ne vous égare.
24:5	Car beaucoup viendront sous mon Nom, en disant : Je suis, moi, le Mashiah. Et ils en égareront beaucoup.
24:6	Mais vous entendrez parler de guerres et de bruits de guerres : veillez à ne pas être troublés, car il faut que toutes ces choses arrivent. Mais ce ne sera pas encore la fin.
24:7	Car une nation s'élèvera contre une autre nation, et un royaume contre un autre royaume, et il y aura des famines, des pestes et des tremblements de terre en divers lieux.
24:8	Mais toutes ces choses ne seront que le commencement des douleurs.
24:9	Alors ils vous livreront à la tribulation et vous tueront. Et vous serez haïs de toutes les nations à cause de mon Nom.
24:10	Et alors beaucoup seront scandalisés, et ils se livreront les uns les autres et se haïront les uns les autres.
24:11	Et beaucoup de faux prophètes s'élèveront et en égareront beaucoup.
24:12	Et, parce que la violation de la torah sera multipliée, l'amour de beaucoup se refroidira.
24:13	Mais celui qui aura supporté bravement et calmement les mauvais traitements jusqu'à la fin sera sauvé.
24:14	Et cet Évangile du Royaume sera prêché dans toute la terre habitée, pour servir de témoignage à toutes les nations et alors viendra la fin.

### L'abomination qui causera la désolation<!--Da. 9:27, 11:32-35 ; Mc. 13:14-18 ; Lu. 21:20-23.-->

24:15	Quand donc vous verrez l'abomination<!--Une chose folle, détestable, des idoles et choses appartenant à l'idolâtrie.--> de la désolation annoncée par le moyen de Daniye'l le prophète<!--Daniye'l (Daniel) fut le premier à prophétiser au sujet de l'abomination de la désolation (Da. 9:24-27). Cette prophétie trouva un premier accomplissement lorsqu'en 168 av. J.-C., le roi de Syrie, Antiochos Épiphane (règne : 175 – 164 av. J.-C.), consacra le temple de Yeroushalaim (Jérusalem) aux dieux grecs et dédia l'autel des holocaustes à Zeus, l'Olympien. En décembre 167 av. J.-C., il sacrifia dessus des porcs, animaux éminemment abominables aux yeux des Juifs, fit interdire la circoncision, la lecture de la torah et l'observance des fêtes de YHWH. Par cet acte, ce type d'anti-mashiah (antichrist) espérait changer les temps (le calendrier juif) et la torah (la loi selon Da. 7:25) en Israël. En l'an 70 de notre ère, la prophétie de Daniye'l s'accomplit une seconde fois lorsque le temple fut de nouveau profané en étant détruit par Titus (39 – 81 ap. J.-C.). Actuellement, nous vivons la réalisation finale de cette parole qui se manifeste par le progrès de l'apostasie parmi les chrétiens et la diffusion d'un évangile erroné, mondain, expurgé de son caractère christocentrique dans un nombre croissant d'assemblées. Ainsi, les chrétiens séduits par les fausses doctrines, ayant abandonné leur premier amour (Ap. 2:5), sont désormais des temples d'Elohîm profanés par l'action néfaste des faux prophètes (1 Co. 6:19). Cette situation ira en s'empirant et conduira à l'accomplissement parfait de la prophétie de Daniye'l en la personne de l'homme impie, qui s'introduira dans le futur temple de Yeroushalaim pour usurper l'adoration qui revient à Elohîm (2 Th. 2:4).--> être établie dans le lieu saint, que celui qui lit comprenne !
24:16	Alors, que ceux qui seront en Judée fuient dans les montagnes,
24:17	que celui qui sera sur le toit ne descende pas pour emporter quoi que ce soit de sa maison,
24:18	et que celui qui sera dans les champs, ne retourne pas en arrière pour prendre ses habits.
24:19	Mais malheur aux femmes enceintes et à celles qui allaiteront en ces jours-là !
24:20	Priez pour que votre fuite n'arrive pas en hiver, ni un jour de shabbat<!--Sous la loi mosaïque, il était interdit aux Juifs de parcourir plus de 2 000 coudées (1 100 mètres) du lieu où ils se trouvaient pendant le shabbat (Ex. 16:29). Voir le tableau « Mesures de longueurs ».-->.

### La grande tribulation<!--Ps. 2:5 ; Jé. 30:5-8 ; Da. 12:1 ; Mc. 13:19-23 ; Lu. 21:23-24 ; Ap. 7:9-17.-->

24:21	Car alors il y aura une grande tribulation<!--Voir Ap. 7:9-17.-->, telle qu'il n'y en a pas eu depuis le commencement du monde jusqu'à présent et qu'il n'y en aura jamais.
24:22	Et si ces jours n'étaient abrégés, aucune chair ne serait sauvée, mais à cause des élus, ces jours seront abrégés.
24:23	Alors si quelqu'un vous dit : Voyez ! Le Mashiah est ici, ou bien : Là ! Ne le croyez pas.
24:24	Car il s'élèvera de faux mashiah et de faux prophètes, et ils donneront<!--Voir Mc. 14:44.--> de grands signes et des miracles pour égarer, s'il était possible, même les élus.
24:25	Voici, je vous l'ai dit d'avance.
24:26	Si donc ils vous disent : « Voici, il est dans le désert ! » N'y allez pas ! « Voici, il est dans les chambres ! » Ne le croyez pas.
24:27	Car, comme l'éclair sort de l'orient et brille jusqu'en occident, il en sera de même de la parousie du Fils d'humain.
24:28	Car là où est le cadavre, là se rassembleront les aigles<!--Voir Job 39:30 ; Lu. 17:37 ; Ap. 19:17-21.-->.

### Le retour du Roi sur la Terre<!--Mc. 13:24-27 ; Lu. 21:25-28.-->

24:29	Mais immédiatement après ces jours de tribulation, le soleil sera obscurci, la lune ne donnera plus sa lumière<!--Es. 13:10 ; Lu. 21:26.-->. Et les étoiles tomberont du ciel et les puissances des cieux seront ébranlées.
24:30	Et alors le signe du Fils d'humain apparaîtra dans le ciel. Et alors toutes les tribus de la Terre se frapperont la poitrine de chagrin et verront le Fils d'humain venant sur les nuées du ciel, avec puissance et une grande gloire.
24:31	Et il enverra ses anges avec un grand son de trompette, et ils rassembleront ses élus, des quatre vents, des extrémités des cieux à leurs extrémités.

### Parabole du figuier<!--Mc. 13:28-31 ; Lu. 21:29-33.-->

24:32	Mais apprenez du figuier cette parabole. Dès que sa jeune branche devient tendre et qu'elle pousse des feuilles, vous savez que l'été est proche.
24:33	De même vous aussi, quand vous verrez toutes ces choses, sachez qu'il est proche, à la porte.
24:34	Amen, je vous le dis, cette génération ne passera jamais, jusqu'à ce que tout cela ne soit arrivé.
24:35	Le ciel et la Terre passeront, mais mes paroles ne passeront jamais.

### Exhortation à la vigilance<!--Mc. 13:32-37 ; Lu. 21:34-38.-->

24:36	Mais, quant à ce jour-là et à l’heure, personne ne les connaît, ni les anges des cieux, mais mon Père seul.
24:37	Mais comme aux jours de Noah, ainsi sera la parousie du Fils d'humain.
24:38	Car, comme ils étaient, aux jours d'avant le déluge, mangeant et buvant, se mariant et donnant en mariage, jusqu'au jour où Noah entra dans l'arche,
24:39	et qu'ils ne surent rien jusqu'à ce que le déluge vint et les emporta tous, ainsi sera la parousie du Fils d'humain.
24:40	Alors, de deux hommes qui seront dans un champ, l'un sera pris et l'autre laissé.
24:41	Elles seront deux en train de moudre au moulin : l'une sera prise et l'autre laissée.
24:42	Veillez donc, parce que vous ne savez à quelle heure votre Seigneur vient.
24:43	Mais sachez que, si le maître de la maison savait à quelle veille le voleur vient, il veillerait et ne laisserait pas percer sa maison.
24:44	C'est pourquoi, vous aussi tenez-vous prêts, parce que le Fils d'humain vient à l'heure que vous ne pensez pas.
24:45	Quel est donc l'esclave fidèle et prudent, que son seigneur a établi sur tous ses serviteurs, pour leur donner la nourriture au temps convenable ?
24:46	Béni est cet esclave, celui que son seigneur, à son arrivée, trouvera agissant de cette manière !
24:47	Amen, je vous le dis, il l'établira sur tous ses biens.
24:48	Mais si c'est un méchant esclave, qui se dit en son cœur : Mon seigneur tarde à venir.
24:49	Et s'il se met à battre ses compagnons de service, s'il mange et boit avec ceux qui sont ivres,
24:50	le seigneur de cet esclave viendra le jour où il ne s'y attend pas et à l'heure qu'il ne connaît pas.
24:51	Et il le coupera en deux parts<!--Méthode cruelle de châtiment utilisée par les Hébreux et d'autres, en coupant quelqu'un en deux. Lu. 12:46.--> et lui donnera sa part avec les hypocrites : là seront les pleurs et le grincement des dents.

## Chapitre 25

### Parabole des dix vierges

25:1	Alors le Royaume des cieux sera semblable à dix vierges qui, ayant pris leurs lampes, allèrent à la rencontre de l'époux.
25:2	Or, cinq d'entre elles étaient sages, et cinq folles.
25:3	Les folles, en prenant leurs lampes, ne prirent pas d'huile avec elles ;
25:4	mais les sages prirent de l'huile dans leurs vases avec leurs lampes.
25:5	Et comme l'époux tardait à venir, elles s'assoupirent et s'endormirent toutes.
25:6	Or au milieu de la nuit il se fit un cri : Voici, l'époux vient, allez à sa rencontre !
25:7	Alors toutes ces vierges se réveillèrent<!--Réveiller : du grec « egeiro ». Ce terme signifie également ressusciter. Les saints qui attendent le retour du Seigneur connaîtront un réveil après un temps de sommeil spirituel (Ro. 13:11).--> et préparèrent leurs lampes.
25:8	Et les folles dirent aux sages : Donnez-nous de votre huile, car nos lampes s'éteignent.
25:9	Mais les sages répondirent, en disant : Non, de peur que nous n'en ayons pas assez pour nous et pour vous. Mais allez plutôt chez ceux qui en vendent et achetez-en pour vous-mêmes.
25:10	Or pendant qu'elles allaient en acheter, l'époux arriva. Celles qui étaient prêtes entrèrent avec lui dans la salle des noces, et la porte fut fermée.
25:11	Mais plus tard viennent aussi les autres vierges disant : Seigneur, Seigneur, ouvre-nous !
25:12	Mais répondant, il dit : Amen, je vous le dis, je ne vous connais pas.
25:13	Veillez donc, car vous ne savez ni le jour ni l'heure où le Fils d'humain vient.

### Parabole des talents

25:14	Car il en sera comme d'un homme qui, partant pour un voyage, appela ses esclaves et leur remit ses biens.
25:15	Il donna à l'un cinq talents, à l'autre deux, et au troisième un, à chacun selon sa propre capacité, et immédiatement après il partit.
25:16	Mais celui qui avait reçu les cinq talents s'en alla, les fit valoir et gagna cinq autres talents.
25:17	De même aussi celui qui en avait deux, en gagna deux autres.
25:18	Mais celui qui n'en avait reçu qu'un, alla et creusa dans la terre, et y cacha l'argent de son maître.
25:19	Et longtemps après, le maître de ces esclaves vient et il règle ses comptes avec eux.
25:20	Et celui qui avait reçu les cinq talents vint et présenta cinq autres talents, en disant : Seigneur, tu m'as confié cinq talents. Voici, j'en ai gagné cinq autres par-dessus.
25:21	Et son Seigneur lui dit : C'est bien ! Bon et fidèle esclave ! Tu as été fidèle en peu de choses, je t'établirai sur beaucoup. Viens participer à la joie de ton Seigneur.
25:22	Et celui qui avait reçu les deux talents vint aussi et dit : Seigneur, tu m'as confié deux talents. Voici, j'en ai gagné deux autres par-dessus.
25:23	Et son Seigneur lui dit : C'est bien ! Bon et fidèle esclave ! Tu as été fidèle en peu de choses, je t'établirai sur beaucoup. Viens prendre part à la joie de ton Seigneur.
25:24	Mais celui qui n'avait reçu qu'un talent, vint et dit : Seigneur, je savais que tu es un homme dur, qui moissonnes où tu n'as pas semé, et qui amasses où tu n'as pas vanné.
25:25	C'est pourquoi, ayant peur de toi, je suis allé cacher ton talent dans la terre. Voici, tu as ici ce qui t'appartient.
25:26	Et son Seigneur répondant, lui dit : Méchant et paresseux esclave ! Tu savais que je moissonnais où je n'ai pas semé, et que j'amassais où je n'ai pas vanné,
25:27	il te fallait donc remettre mon argent aux banquiers et à mon retour, j'aurais retiré ce qui est à moi avec un intérêt.
25:28	Ôtez-lui donc le talent et donnez-le à celui qui a les dix talents !
25:29	Car, à celui qui a, on donnera encore et il sera dans l'abondance, mais à celui qui n'a rien, on ôtera même ce qu'il a.
25:30	Et jetez donc l'esclave inutile dans la ténèbre de dehors : là seront les pleurs et le grincement des dents.

### Yéhoshoua, le juste Juge

25:31	Et quand le Fils d'humain viendra dans sa gloire et accompagné de tous les saints anges, alors il s'assiéra sur le trône de sa gloire.
25:32	Et toutes les nations seront rassemblées<!--Es. 66:18.--> devant lui. Et il séparera<!--Ez. 34:16-17.--> les uns d'avec les autres, comme le berger sépare les brebis d'avec les boucs.
25:33	Et il mettra en effet, les brebis à sa droite, et les boucs à sa gauche.
25:34	Alors le Roi dira à ceux qui seront à sa droite : Venez, vous qui êtes bénis de mon Père, recevez en héritage le Royaume qui vous a été préparé dès la fondation du monde.
25:35	Car j'ai eu faim et vous m'avez donné à manger. J'ai eu soif et vous m'avez donné à boire. J'étais étranger et vous m'avez recueilli.
25:36	J'étais nu et vous m'avez vêtu. J'étais malade et vous m'avez visité. J'étais en prison et vous êtes venus vers moi.
25:37	Alors les justes lui répondront : Seigneur, quand est-ce que nous t'avons vu avoir faim, et que nous t'avons nourri, ou avoir soif, et que nous t'avons donné à boire ?
25:38	Et quand est-ce que nous t'avons vu étranger, et que nous t'avons recueilli, ou nu et t'avons-nous vêtu ?
25:39	Et quand est-ce que nous t'avons vu malade, ou en prison, et que nous sommes venus vers toi ?
25:40	Et le Roi répondant, leur dira : Amen, je vous le dis, toutes les fois que vous avez fait ces choses à l'un de ces plus petits de mes frères, c'est à moi que vous les avez faites.
25:41	Alors il dira aussi à ceux qui seront à sa gauche : Maudits, retirez-vous de moi et allez dans le feu éternel, qui a été préparé pour le diable et pour ses anges.
25:42	Car j'ai eu faim et vous ne m'avez pas donné à manger. J'ai eu soif et vous ne m'avez pas donné à boire.
25:43	J'étais un étranger et vous ne m'avez pas recueilli, nu et vous ne m'avez pas vêtu, malade et en prison, et vous ne m'avez pas visité.
25:44	Alors ils répondront aussi, en disant : Seigneur, quand est-ce que nous t'avons vu avoir faim, ou avoir soif, ou être étranger, ou nu, ou malade, ou en prison, et que nous ne t'avons pas servi ?
25:45	Alors il leur répondra, en disant : Amen, je vous le dis, toutes les fois que vous n'avez pas fait ces choses à l'un de ces plus petits, c'est à moi que vous ne les avez pas faites.
25:46	Et ceux-ci iront au châtiment éternel, mais les justes à la vie éternelle.

## Chapitre 26

### Les prêtres, les anciens et les scribes complotent contre Yéhoshoua<!--Mc. 14:1-2 ; Lu. 22:1-2.-->

26:1	Et il arriva que, quand Yéhoshoua eut achevé tous ces discours, il dit à ses disciples :
26:2	Vous savez que la fête de Pâque a lieu dans deux jours, et que le Fils d'humain sera livré pour être crucifié.
26:3	Alors les principaux prêtres, les scribes et les anciens du peuple se réunirent dans la cour du grand-prêtre, appelé Kaïaphas<!--Un grand-prêtre des Juifs nommé à cette fonction par Valerius Gratus, gouverneur de Judée, en remplacement de Shim’ôn, fils de Camith, en 18 ap. J.-C. et remplacé en 36 ap. J.-C. par Yonathan, fils de Ananus, par une décision de Vitellius, gouverneur de Syrie.-->,
26:4	et ils tinrent conseil ensemble pour se saisir de Yéhoshoua par ruse, afin de le faire mourir.
26:5	Mais ils dirent : Que ce ne soit pas pendant la fête, de peur qu'il ne se fasse quelque tumulte parmi le peuple.

### Myriam de Béthanie répand du baume sur Yéhoshoua<!--Mc. 14:3-9 ; Jn. 12:1-8.-->

26:6	Mais comme Yéhoshoua était à Béthanie, dans la maison de Shim’ôn le lépreux,
26:7	une femme s'approcha de lui tenant un vase d'albâtre, plein d'un baume<!--Vient du grec « muron » traduit en français par « onguent », « pommade », « crème ». C'est une pommade à la consistance crémeuse souvent employée pour une utilisation médicale afin d'apaiser une quelconque douleur. Voir Mt. 26:9, 12 ; Mc. 14:3-4 ; Lu. 7:37-38,46, 23:56 ; Jn. 11:2, 12:3,5 ; Ap. 18:13.--> d'une grande valeur, et pendant qu'il était à table, elle le répandit sur sa tête.
26:8	Mais ses disciples voyant cela, en furent indignés et dirent : À quoi sert cette perte ?
26:9	Car ce baume pouvait être vendu bien cher et donné aux pauvres.
26:10	Mais Yéhoshoua en ayant eu connaissance leur dit : Pourquoi faites-vous de la peine à cette femme ? Car elle a fait une bonne action envers moi.
26:11	En effet, vous aurez toujours des pauvres avec vous, mais vous ne m'aurez pas toujours.
26:12	Car en répandant ce baume sur mon corps, elle l'a fait pour ma sépulture.
26:13	Amen, je vous le dis, partout où cet Évangile sera prêché dans le monde entier, on racontera aussi, en mémoire d'elle, ce qu'elle a fait.

### La trahison de Yéhouda Iscariot (Judas)<!--Mc. 14:10-11 ; Lu. 22:3-6.-->

26:14	Alors l'un des douze, appelé Yéhouda Iscariot, alla vers les principaux prêtres
26:15	et leur dit : Que voulez-vous me donner et je vous le livrerai ? Et ils lui comptèrent 30 pièces d'argent<!--Za. 11:12-13.-->.
26:16	Et dès lors, il cherchait une occasion favorable pour le livrer.

### La dernière Pâque<!--Mc. 14:12-21 ; Lu. 22:7-20 ; Jn. 13:1-12.-->

26:17	Or le premier jour des pains sans levain, les disciples s'approchèrent de Yéhoshoua pour lui dire : Où veux-tu que nous te préparions le repas de la Pâque ?
26:18	Et il dit : Allez à la ville chez un tel et dites-lui : Le Docteur dit : Mon temps est proche. Je ferai la Pâque chez toi avec mes disciples.
26:19	Et les disciples firent comme Yéhoshoua leur avait ordonné et préparèrent la Pâque.
26:20	Et quand le soir fut venu, il se mit à table avec les douze.
26:21	Et comme ils mangeaient, il dit : Amen, je vous le dis, l'un de vous me livrera.
26:22	Ils furent profondément attristés, et chacun d'eux commença à lui dire : Seigneur, est-ce moi ?
26:23	Mais il leur répondit et dit : Celui qui trempe sa main dans le plat avec moi, celui-là me livrera.
26:24	Le Fils d'humain s'en va, selon qu'il est écrit de lui, mais malheur à cet humain par le moyen de qui le Fils d'humain est livré ! Mieux vaudrait pour cet humain qu'il ne soit pas né.
26:25	Et Yéhouda qui le livrait, répondit et dit : Rabbi, est-ce moi ? Il lui dit : Tu l'as dit.

### Le repas de la Pâque<!--Mc. 14:22-25 ; Lu. 22:17-20 ; Jn. 13:12-30 ; 1 Co. 11:23-26.-->

26:26	Mais pendant qu'ils mangeaient, Yéhoshoua prit du pain et, ayant prononcé la bénédiction, il le rompit et le donna aux disciples en disant : Prenez, mangez, ceci est mon corps.
26:27	Et ayant pris la coupe, il rendit grâce, il la leur donna, en disant : Buvez-en tous,
26:28	car ceci est mon sang, celui de la nouvelle alliance, qui est répandu pour beaucoup, pour le pardon des péchés.
26:29	Or je vous dis que désormais je ne boirai plus de ce produit de la vigne, jusqu'à ce jour-là, quand je le boirai nouveau avec vous dans le royaume de mon Père.

### Yéhoshoua annonce à Petros (Pierre) son triple reniement<!--Mc. 14:26-31 ; Lu. 22:31-34 ; Jn. 13:36-38.-->

26:30	Et ayant chanté des hymnes pascals<!--Les cantiques évoqués dans ce passage sont des hymnes pascals composés des psaumes 113 à 118 et du psaume 136. Ce recueil de chants, appelé grand Hallel, est entonné à haute voix le premier soir de Pâque, à la pentecôte, à la fête des cabanes (ou des tabernacles), pour Hanoukka et Rosh Hodesh.-->, ils se rendirent à la Montagne des Oliviers.
26:31	Alors Yéhoshoua leur dit : Vous serez tous cette nuit scandalisés en moi, car il est écrit : Je frapperai le berger et les brebis du troupeau seront dispersées<!--Za. 13:7.-->.
26:32	Mais, après que je serai ressuscité, je vous précéderai en Galilée.
26:33	Mais Petros répondant, lui dit : Même si tous seront scandalisés en toi, moi, je ne serai jamais scandalisé.
26:34	Yéhoshoua lui dit : Amen, je te le dis, cette nuit même, avant que le coq ait chanté, tu me renieras trois fois.
26:35	Petros lui dit : Même s'il me faut mourir avec toi, je ne te renierai jamais. Et tous les disciples dirent la même chose.

### Gethsémané<!--Mc. 14:32-42 ; Lu. 22:39-46 ; Jn. 18:1.-->

26:36	Alors Yéhoshoua arrive avec eux dans un lieu appelé Gethsémané, et il dit à ses disciples : Asseyez-vous ici, jusqu'à ce que m'en étant allé là, j'aie prié.
26:37	Et il prit avec lui Petros et les deux fils de Zabdi, et il commença à être saisi de tristesse et d'angoisse<!--Vient du grec « ademoneo » qui signifie « être troublé, grande détresse ou angoisse, dépression ». C'est le mot grec le plus fort pour dépression. Voir Mc. 14:33 ; Ph. 2:26.-->.
26:38	Alors il leur dit : Mon âme est très triste jusqu'à la mort. Restez ici et veillez avec moi !

### Première prière de Yéhoshoua<!--Mc. 14:35-38 ; Lu. 22:41-42.-->

26:39	Et étant allé un peu plus avant, il tomba sur sa face, priant et disant : Mon Père, s'il est possible, fais que cette coupe passe loin de moi. Toutefois, non pas comme moi je veux, mais comme toi.
26:40	Et il vint vers ses disciples, qu'il trouva endormis, et il dit à Petros : Vous n’avez donc pas été capables de veiller une heure avec moi !
26:41	Veillez et priez, afin que vous n'entriez pas en tentation. En effet, l'esprit est bien disposé, mais la chair est faible.

### Deuxième prière de Yéhoshoua<!--Mc. 14:39 ; Lu. 22:44.-->

26:42	Il s'éloigna encore pour la seconde fois et il pria, en disant : Mon Père, s'il n'est pas possible que cette coupe passe loin de moi sans que je la boive, que ta volonté soit faite !
26:43	Il revint ensuite et les trouva encore endormis, car leurs yeux étaient appesantis.

### Troisième prière de Yéhoshoua<!--Mc. 14:41.-->

26:44	Et les ayant laissés, il s'en alla encore et pria pour la troisième fois, disant la même parole.
26:45	Et il alla vers ses disciples et leur dit : Dormez maintenant et reposez-vous ! Voici, l'heure est proche, et le Fils d'humain va être livré entre les mains des pécheurs.
26:46	Levez-vous, allons ! Voici, celui qui me livre s'approche.

### Yéhoshoua, trahi, abandonné et arrêté<!--Mc. 14:43-50 ; Lu. 22:47-53 ; Jn. 18:2-11.-->

26:47	Et comme il parlait encore, voici, Yéhouda, l'un des douze, vint, et avec lui, une grande foule avec des épées et des bâtons, de la part des principaux prêtres et des anciens du peuple.
26:48	Or celui qui le livrait leur avait donné un signe, en disant : Celui à qui je donnerai un baiser, c'est lui, saisissez-le !
26:49	Et immédiatement, s'approchant de Yéhoshoua, il lui dit : Rabbi, je te salue ! Et il l'embrassa tendrement.
26:50	Et Yéhoshoua lui dit : Compagnon, pour quel sujet es-tu ici ? Alors s'étant approchés, ils mirent les mains sur Yéhoshoua et le saisirent.
26:51	Et voici l'un de ceux qui étaient avec Yéhoshoua, ayant étendu la main, tira son épée et, ayant frappé l'esclave du grand-prêtre, lui emporta l'oreille.
26:52	Alors Yéhoshoua lui dit : Remets ton épée à sa place, car tous ceux qui auront pris l'épée périront par l'épée<!--Ap. 13:9.-->.
26:53	Penses-tu que je ne puisse pas maintenant appeler mon Père, et il m'offrira à l'instant plus de douze légions d'anges ?
26:54	Comment donc s'accompliraient les Écritures, puisqu'il faut que cela arrive ainsi ?
26:55	En cette heure-là, Yéhoshoua dit aux foules : Vous êtes venus avec des épées et des bâtons, comme après un brigand, pour me prendre. J'étais tous les jours assis parmi vous, enseignant dans le temple, et vous ne m'avez pas saisi.
26:56	Mais tout ceci est arrivé afin que les Écritures des prophètes soient accomplies. Alors tous les disciples l'abandonnèrent et s'enfuirent.

### Yéhoshoua comparaît devant Kaïaphas (Caïphe) et le sanhédrin<!--Mc. 14:53-65 ; Jn. 18:12-14,19-24.-->

26:57	Et ceux qui avaient saisi Yéhoshoua l'amenèrent chez Kaïaphas, le grand-prêtre, où les scribes et les anciens étaient rassemblés.
26:58	Et Petros le suivit de loin jusqu'à la cour du grand-prêtre, y entra et s'assit avec les officiers pour voir comment cela finirait.
26:59	Mais les principaux prêtres, les anciens et tout le sanhédrin cherchaient un faux témoignage contre Yéhoshoua pour le faire mourir.
26:60	Mais ils n'en trouvèrent pas, et bien que beaucoup de faux témoins se soient présentés, ils n'en trouvèrent pas. Mais à la fin, deux faux témoins s'approchèrent,
26:61	et dirent : Celui-ci a dit : Je peux détruire le temple d'Elohîm et le rebâtir en trois jours.
26:62	Et le grand-prêtre se leva et lui dit : Ne réponds-tu rien ? Qu'est-ce que ceux-ci témoignent contre toi ?
26:63	Mais Yéhoshoua garda le silence. Et le grand-prêtre prenant la parole, lui dit : Je t'adjure par l'Elohîm, le Vivant<!--Apo. 1:18.-->, de nous dire si tu es le Mashiah, le Fils d'Elohîm.
26:64	Yéhoshoua lui dit : Tu l'as dit. De plus, je vous le dis, dès maintenant vous verrez le Fils d'humain assis à la droite de la Puissance et venant sur les nuées du ciel.
26:65	Alors le grand-prêtre déchira ses vêtements<!--Lé. 10:6 et 21:10. La torah de Moshé (Moïse) interdisait aux grands-prêtres de déchirer leurs vêtements.-->, en disant : Il a blasphémé ! Qu'avons-nous encore besoin de témoins ? Voici, vous avez entendu maintenant son blasphème.
26:66	Qu'en pensez-vous ? Ils répondirent : Il est digne de mort.

### Yéhoshoua, maltraité par les Juifs

26:67	Alors ils lui crachèrent au visage et le frappèrent à coups de poing. Et d'autres le frappèrent avec leurs bâtons,
26:68	en disant : Mashiah, prophétise-nous qui est celui qui t'a frappé.

### Le triple reniement de Petros (Pierre)<!--Mc. 14:66-72 ; Lu. 22:55-62 ; Jn. 18:15-18,25-27.-->

26:69	Or Petros était assis dehors dans la cour. Et une servante s'approcha de lui et lui dit : Toi aussi, tu étais avec Yéhoshoua le Galiléen.
26:70	Mais il le nia devant tous, en disant : Je ne sais pas ce que tu dis.
26:71	Et comme il était sorti dans le vestibule, une autre le vit et elle dit à ceux qui étaient là : Celui-ci aussi était avec Yéhoshoua, le Nazaréen.
26:72	Et il le nia encore avec serment : Je ne connais pas cet homme.
26:73	Et peu après, ceux qui se trouvaient là s'approchèrent et dirent à Petros : Vraiment, toi aussi tu es de ces gens-là, car ton langage<!--Au 1er siècle, la langue parlée par la majorité de la population juive était un dialecte de l’araméen. C'est seulement au sud de la Judée que l'on parlait hébreu à cette époque.--> te fait connaître.
26:74	Alors il commença à se maudire et à jurer : Je ne connais pas cet homme. Et immédiatement le coq chanta.
26:75	Et Petros se souvint de la parole de Yéhoshoua, qui lui avait dit : Avant que le coq chante, tu me renieras trois fois. Et étant sorti dehors, il pleura amèrement.

## Chapitre 27

### Suicide de Yéhouda Iscariot (Judas)<!--Ac. 1:16-19.-->

27:1	Or, le matin venu, tous les principaux prêtres et les anciens du peuple tinrent conseil contre Yéhoshoua pour le faire mourir.
27:2	Et l'ayant lié, ils l'amenèrent et le livrèrent à Ponce Pilate, le gouverneur.
27:3	Alors Yéhouda qui l'avait livré, voyant qu'il était condamné, se repentit et rapporta les 30 pièces d'argent aux principaux prêtres et aux anciens,
27:4	en leur disant : J'ai péché en livrant le sang innocent. Mais ils dirent : Que nous importe ? Cela te regarde.
27:5	Et jetant les pièces d'argent dans le temple, il se retira et alla se suicider par pendaison.
27:6	Mais les principaux prêtres prirent les pièces d'argent, et dirent : Il n'est pas légal de les mettre dans le trésor, car c'est le prix du sang.
27:7	Et ayant tenu conseil, ils achetèrent le champ du potier, pour la sépulture des étrangers.
27:8	C'est pourquoi ce champ-là a été appelé jusqu'à aujourd'hui, le champ du sang.
27:9	Alors fut accompli ce qui avait été annoncé par Yirmeyah le prophète, en disant : Ils ont pris les 30 pièces d'argent, le prix de celui qui a été estimé, et qu’on a estimé de la part des fils d’Israël,
27:10	et ils les donnèrent pour le champ d'un potier, selon ce que le Seigneur m'avait ordonné<!--Ce verset se réfère certainement à Za. 11:12-13, avec une allusion à Jé. 18:1-4.-->.

### Yéhoshoua comparaît devant Pilate

27:11	Or Yéhoshoua comparut devant le gouverneur. Et le gouverneur l'interrogea : Es-tu le Roi des Juifs ? Mais Yéhoshoua lui déclara : Tu le dis.
27:12	Mais il ne répondit rien aux accusations des principaux prêtres et des anciens.
27:13	Alors Pilate lui dit : N'entends-tu pas combien de choses ils témoignent contre toi ?
27:14	Mais il ne lui donna de réponse sur aucune parole, ce qui étonna beaucoup le gouverneur.

### Yéhoshoua ou Barabbas ?<!--Mc. 15:6-15 ; Lu. 23:17-25 ; Jn. 18:39-40.-->

27:15	Or le gouverneur avait coutume de relâcher un prisonnier à chaque fête, celui que demandait la foule.
27:16	Et il y avait alors un prisonnier infâme, appelé Barabbas.
27:17	Comme ils étaient rassemblés, Pilate leur dit : Lequel voulez-vous que je vous relâche ? Barabbas ou Yéhoshoua, qu'on appelle Mashiah ?
27:18	Car il savait que c’était par envie qu’ils l’avaient livré.
27:19	Et pendant qu'il siégeait au tribunal, sa femme envoya quelqu'un pour lui dire : Qu'il n'y ait rien entre toi et ce juste, car j'ai beaucoup souffert aujourd'hui en rêve à cause de lui.
27:20	Mais les principaux prêtres et les anciens persuadèrent la multitude du peuple de demander Barabbas et de faire périr Yéhoshoua.
27:21	Et le gouverneur prenant la parole, leur dit : Lequel des deux voulez-vous que je vous relâche ? Et ils dirent : Barabbas.
27:22	Pilate leur dit : Que ferai-je donc de Yéhoshoua qu'on appelle Mashiah ? Ils lui disent tous : Qu'il soit crucifié !
27:23	Et le gouverneur leur dit : Mais quel mal a-t-il fait ? Et ils crièrent encore plus fort, en disant : Qu'il soit crucifié !
27:24	Alors Pilate voyant qu'il ne gagnait rien, mais que le tumulte s'augmentait, prit de l'eau et lava ses mains devant le peuple, en disant : Je suis innocent du sang de ce juste. Cela vous regarde.
27:25	Et tout le peuple répondit en disant : Que son sang retombe sur nous et sur nos enfants !
27:26	Alors il leur relâcha Barabbas et, après avoir fait fouetter Yéhoshoua, il le leur livra pour qu'il soit crucifié.

### Le Roi couronné d'épines<!--Mc. 15:16-23 ; Lu. 23:26-32 ; Jn. 19:16-17.-->

27:27	Alors les soldats du gouverneur amenèrent Yéhoshoua dans le prétoire et rassemblèrent devant lui toute la cohorte.
27:28	Et après l'avoir dépouillé, ils le revêtirent d'un manteau d'écarlate.
27:29	Et ayant fait une couronne d'épines entrelacées, ils la mirent sur sa tête et ils lui mirent un roseau dans sa main droite et, s'agenouillant devant lui, ils se moquaient de lui, en disant : Nous te saluons, Roi des Juifs !

### Yéhoshoua, maltraité par les soldats

27:30	Et ils crachaient sur lui, prenaient le roseau et frappaient sur sa tête.
27:31	Et quand ils se furent moqués de lui, ils lui ôtèrent le manteau et lui remirent ses propres vêtements, et l'amenèrent pour le crucifier.

### La crucifixion de Yéhoshoua<!--Mc. 15:24-41 ; Lu. 23:33-49 ; Jn. 19:17-37 ; Hé. 9:3-8, 10:19-20.-->

27:32	Or comme ils sortaient, ils rencontrèrent un homme de Cyrène, du nom de Shim’ôn, et ils le forcèrent à porter sa croix.
27:33	Et étant arrivés au lieu appelé Golgotha, c'est-à-dire, le lieu du Crâne,
27:34	ils lui donnèrent à boire du vinaigre mêlé avec du fiel<!--Le vinaigre mêlé au fiel (Ps. 69:22) : Ce breuvage, appelé « posca », était un vin amer qui se transformait en vinaigre à cause des mauvaises conditions de conservation. Allongée avec de l'eau et parfois adoucie avec de l'œuf, cette boisson bon marché et très rafraîchissante était consommée principalement par les légionnaires et les esclaves. Connue pour ses vertus antiseptiques, les soldats de l'Antiquité avaient coutume d'y ajouter des drogues comme la myrrhe et le fiel (opium) pour atténuer les souffrances. En refusant de le boire, le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) a réellement pris sur lui la plénitude du châtiment que nous méritons à cause de nos péchés.--> ; mais quand il l'eut goûté, il ne voulut pas boire.
27:35	Et après l'avoir crucifié, ils partagèrent ses vêtements, en tirant au sort, afin que fût accompli ce qui avait été annoncé par le prophète : Ils se sont partagé mes vêtements, et ils ont tiré mon habit au sort<!--Ps. 22:19.-->.
27:36	Et s'étant assis, ils le gardaient là.
27:37	Ils mirent aussi au-dessus de sa tête un écriteau, où la cause de sa condamnation était marquée en ces mots : CELUI-CI EST YÉHOSHOUA, LE ROI DES JUIFS.
27:38	Alors deux brigands sont crucifiés avec lui, l'un à droite, l'autre à gauche.
27:39	Et ceux qui passaient par là blasphémaient contre lui et secouaient la tête,
27:40	et disant : Toi qui détruis le temple et qui le rebâtis en trois jours, sauve-toi toi-même ! Si tu es le Fils d'Elohîm, descends de la croix !
27:41	Mais pareillement aussi, les principaux prêtres, avec les scribes et les anciens, se moquant, disaient :
27:42	Il a sauvé les autres et il ne peut pas se sauver lui-même ! S'il est le Roi d'Israël, qu'il descende maintenant de la croix et nous croirons en lui.
27:43	Il a mis en Elohîm sa confiance, qu'il le délivre maintenant, s'il l'aime ! Car il a dit : Je suis le Fils d'Elohîm.
27:44	Les brigands aussi qui étaient crucifiés avec lui, lui reprochaient la même chose.
27:45	Mais, depuis la sixième heure jusqu'à la neuvième la ténèbre<!--Le mot est au singulier.--> survint sur toute la Terre.
27:46	Mais vers la neuvième heure, Yéhoshoua cria d'une grande voix, en disant : Éli, Éli, lama sabachthani ? C'est-à-dire : Mon El<!--Voir le dictionnaire en annexe.-->, mon El, pourquoi m'as-tu abandonné<!--Voir Ps. 22:2.--> ?
27:47	Quelques-uns de ceux qui étaient là présents, ayant entendu cela, disaient : Celui-ci appelle Éliyah !
27:48	Et immédiatement, l'un d'entre eux courut prendre une éponge, et l'ayant remplie de vinaigre et fixée au bout d'un roseau, il lui donna à boire.
27:49	Mais les autres disaient : Laisse, voyons si Éliyah viendra le sauver.
27:50	Mais Yéhoshoua poussa de nouveau un grand cri et rendit l'esprit.

### FIN DE LA LOI MOSAÏQUE OU DE LA PREMIÈRE ALLIANCE


### Le voile du temple déchiré

27:51	Et voici, le voile du temple se déchira en deux, depuis le haut jusqu'en bas<!--C'est ici que s'achève la première alliance. Cette dernière était relative à la torah de Moshé (Moïse), c'est-à-dire, aux ordonnances liées au culte, qui reposait sur la prêtrise lévitique et les sacrifices d'animaux et au sanctuaire terrestre, à savoir le temple de Yeroushalaim (Jérusalem) (Hé. 9:1). Le Seigneur ayant offert une fois pour toutes le sacrifice parfait, les exigences de la justice divine ont été pleinement satisfaites (Hé. 9:11-12,25-26). Désormais, la première alliance n'a plus de raison d'être et peut donc disparaître (Hé. 8:13). Non seulement la déchirure du voile séparant le lieu saint du Saint des saints atteste la fin de la première alliance, mais invite aussi tout être humain à s'approcher d'Elohîm en esprit, sans intermédiaires (Lévites, prêtres, pasteurs, prophètes...) ni nécessité de se rendre dans un temple (Jn. 4:23). La nouvelle alliance est aussi un testament puisque Yéhoshoua ha Mashiah (Jésus-Christ), notre légataire, est passé par la mort (Hé. 9:16-18). Voir aussi commentaire en Ex. 19:5.--> ; et la terre trembla et les pierres se fendirent.
27:52	Et les sépulcres s'ouvrirent, et beaucoup de corps des saints qui s’étaient endormis furent ressuscités.
27:53	Et étant sortis des sépulcres après sa résurrection, ils entrèrent dans la ville sainte et se montrèrent à beaucoup.
27:54	L'officier de l'armée romaine et ceux qui étaient avec lui pour garder Yéhoshoua, ayant vu le tremblement de terre et tout ce qui venait d'arriver, furent extrêmement effrayés et dirent : Celui-ci était vraiment le Fils d'Elohîm.
27:55	Il y avait là aussi beaucoup de femmes qui regardaient de loin, et qui avaient suivi Yéhoshoua depuis la Galilée, pour le servir ;
27:56	parmi lesquelles étaient Myriam-Magdeleine, Myriam, mère de Yaacov et de Yossef, et la mère des fils de Zabdi.

### Yossef d'Arimathée se rend vers Pilate et demande le corps de Yéhoshoua<!--Mc. 15:42-47 ; Lu. 23:50-56 ; Jn. 19:38-42.-->

27:57	Et le soir étant venu, un homme riche d'Arimathée, appelé Yossef, qui était aussi disciple de Yéhoshoua,
27:58	se rendit vers Pilate et demanda le corps de Yéhoshoua. En même temps Pilate ordonna que le corps soit rendu.
27:59	Et Yossef, ayant pris le corps, l'enveloppa dans un linceul pur,
27:60	et le mit dans son sépulcre neuf qu'il avait taillé dans le rocher. Et il roula une grande pierre à l'entrée du sépulcre et il s'en alla.
27:61	Myriam-Magdeleine et l'autre Myriam étaient là, assises vis-à-vis de la tombe.

### Le sépulcre scellé et gardé

27:62	Or le lendemain, qui est après la préparation du shabbat, les principaux prêtres et les pharisiens allèrent ensemble auprès de Pilate,
27:63	et dirent : Seigneur, nous nous sommes souvenus que ce trompeur disait, quand il était encore en vie : Après trois jours je ressusciterai.
27:64	Ordonne donc que la tombe soit gardée sûrement jusqu'au troisième jour, de peur que ses disciples ne viennent de nuit et ne volent son corps, et qu'ils ne disent au peuple : Il est ressuscité des morts. Ce dernier égarement serait pire que le premier.
27:65	Et Pilate leur dit : Vous avez une garde, allez et faites-le garder, comme vous l'entendez.
27:66	Ils s'en allèrent donc et s'assurèrent de la tombe, au moyen d'une garde, après avoir scellé la pierre.

## Chapitre 28

### La résurrection de Yéhoshoua ha Mashiah (Jésus-Christ) annoncée par un ange<!--Mc. 16:1-14 ; Lu. 24:1-49 ; Jn. 20:1-23.-->

28:1	Mais après les shabbats<!--Il est question ici du shabbat annuel, c'est-à-dire la fête des pains sans levain. Ce shabbat n'a rien à voir avec le shabbat hebdomadaire ou le samedi.-->, à l'aube d'un des shabbats<!--Il est question ici du shabbat hebdomadaire, c'est-à-dire le septième jour ou le samedi.-->, Myriam-Magdeleine et l'autre Myriam, allèrent voir le sépulcre.
28:2	Et voilà qu'il se fit un grand tremblement de terre, car un ange du Seigneur, étant descendu du ciel, vint et roula la pierre à côté de l'entrée, et s'assit dessus.
28:3	Or son visage était comme un éclair et son vêtement blanc comme de la neige.
28:4	Et les gardes tremblèrent de peur et devinrent comme morts.
28:5	Mais l'ange prit la parole et dit aux femmes : Pour vous, ne craignez pas, car je sais que vous cherchez Yéhoshoua, qui a été crucifié.
28:6	Il n'est pas ici car il est ressuscité comme il l'avait dit. Venez et voyez le lieu où le Seigneur était couché,
28:7	et allez-vous-en promptement, et dites à ses disciples qu'il est ressuscité des morts. Et voici, il vous précède en Galilée. C'est là que vous le verrez. Voici, je vous l'ai dit.

### La résurrection de Yéhoshoua ha Mashiah (Jésus-Christ) annoncée par les femmes

28:8	Et étant sorties promptement du sépulcre avec crainte et grande joie, elles coururent porter la nouvelle à ses disciples.
28:9	Mais comme elles allaient porter la nouvelle à ses disciples, voici, Yéhoshoua se présenta devant elles et leur dit : Réjouissez-vous ! Et s'approchant, elles embrassèrent ses pieds et l'adorèrent.
28:10	Alors Yéhoshoua leur dit : N'ayez pas peur ! Allez et dites à mes frères d'aller en Galilée, c'est là qu'ils me verront.

### Les soldats soudoyés par les prêtres

28:11	Tandis qu'elles s'en allaient, voici que quelques-uns de la garde étant allés dans la ville, portèrent aux principaux prêtres la nouvelle de toutes les choses qui étaient arrivées.
28:12	Alors s'étant rassemblés avec les anciens et ayant tenu conseil, ils donnèrent une forte somme d'argent aux soldats,
28:13	en disant : Dites : ses disciples sont venus de nuit et l'ont volé pendant que nous dormions.
28:14	Et si le gouverneur l'apprend, nous le persuaderons et nous vous épargnerons tout souci.
28:15	Et eux, ayant pris l'argent, firent comme ils avaient été instruits. Et cette parole s'est répandue parmi les Juifs jusqu’à aujourd’hui.

### La mission des disciples<!--Mc. 16:15-18 ; Lu. 24:46-48 ; Jn. 17:18, 20:21 ; Ac. 1:8 ; 1 Co. 15:6.-->

28:16	Mais les onze disciples allèrent en Galilée, sur la montagne que Yéhoshoua leur avait désignée.
28:17	Et quand ils le virent, ils l'adorèrent, mais quelques-uns doutèrent.
28:18	Et Yéhoshoua s'étant approché, leur parla, en disant : Toute autorité m'a été donnée dans le ciel et sur la Terre.
28:19	Allez donc et faites de toutes les nations des disciples, les baptisant dans le Nom du Père et du Fils et du Saint-Esprit,
28:20	leur enseignant à garder tout ce que je vous ai ordonné. Et voici moi, je suis avec vous tous les jours jusqu'à l'achèvement de l'âge. Amen !
