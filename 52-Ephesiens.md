# Éphésiens (Ep.)

Auteur : Paulos (Paul)

Thème : L'Assemblée, corps du Mashiah (Christ)

Date de rédaction : Env. 60 ap. J.-C.

Éphèse figurait parmi les principales villes de l'Empire romain sous le règne de l'empereur Claude Ier (10 av. J.-C. – 54 ap. J.-C.). Bien que Pergame était considérée comme la capitale de l'Asie Mineure, en raison de sa position géographique et grâce à ses affluents, Éphèse possédait le plus grand port de la région, ce qui lui a valu le contrôle du trafic commercial. Richissime et prospère, elle était renommée pour son faste et sa liberté de parole, et constituait donc un endroit privilégié pour les philosophes. C'était une ville où l'activité culturelle tenait une grande place (jeux olympiques, théâtres, cirques, etc.) et où chacun pouvait y pratiquer la religion de son choix (croyances gréco-romaines, égyptiennes, judaïques, etc.).

Éphèse, dont le nom signifie « désirable », était la gardienne de l'Artémision, temple dédié à la déesse grecque Artémis, la Diane des Éphésiens.

L'assemblée d'Éphèse vit le jour lors du second voyage missionnaire de Paulos (50 – 52 ap. J.-C.). Quand il repartit, il laissa à Akulas et Priscilla la charge de la toute jeune assemblée. Lors de son troisième voyage (53-57), Paulos s'installa à Éphèse et y demeura presque trois ans. Il discourut pendant trois mois dans la synagogue sur le Royaume d'Elohîm, mais se retrouva confronté à l'endurcissement de certains. C'est alors qu'il se retira pour enseigner dans l'école d'un certain Turannos durant deux ans, de sorte que tous ceux qui habitaient l'Asie, Juifs et Grecs, entendirent parler de Yéhoshoua ha Mashiah.

Plusieurs de ceux qui avaient cru confessèrent leurs péchés et un certain nombre de ceux qui avaient pratiqué la magie allèrent même jusqu'à brûler leurs livres publiquement. C'est ainsi que l'assemblée d'Éphèse croissait en puissance et en force. La prédication de Paulos vint troubler le marché fructueux des fabricants d'idoles, au point que Démétrios (orfèvre tirant un grand profit de cette industrie) entraîna une émeute contre lui. Paulos était cependant soutenu par des amis influents : les Asiarques.

Rédigée en prison, cette lettre a pour vocation d'enseigner les chrétiens d'Éphèse sur la manière dont il convient de vivre les uns avec les autres au sein de l'Assemblée, corps du Mashiah (Christ).

## Chapitre 1

### Introduction

1:1	Paulos, apôtre de Yéhoshoua Mashiah par la volonté d'Elohîm, aux saints et fidèles en Yéhoshoua Mashiah qui sont à Éphèse :
1:2	à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !

### La position des élus dans le Royaume d'Elohîm

1:3	Béni soit l'Elohîm et Père de notre Seigneur Yéhoshoua ha Mashiah, qui nous a bénis de toute bénédiction spirituelle dans les lieux célestes en Mashiah !
1:4	Selon qu'il nous a élus en lui avant la fondation du monde, afin que nous soyons saints et sans défaut devant lui dans l'amour,
1:5	nous ayant prédestinés à l'adoption pour lui-même, par le moyen de Yéhoshoua Mashiah, selon le bon plaisir de sa volonté,
1:6	à la louange de la gloire de sa grâce, par laquelle il nous a rendus gracieux en son bien-aimé.
1:7	En lui nous avons la rédemption par le moyen de son sang, le pardon des fautes selon la richesse de sa grâce,
1:8	qu'il a fait abonder envers nous en toute sagesse et intelligence.
1:9	Il nous a fait connaître le mystère de sa volonté, selon son bon plaisir qu'il avait disposé en lui-même,
1:10	pour une administration lors de l'accomplissement des temps : réunir toutes choses en Mashiah, aussi bien ce qui est dans les cieux que ce qui est sur la Terre.
1:11	En lui, en qui aussi nous sommes devenus héritiers, ayant été prédestinés selon le dessein de celui qui opère toutes choses selon le conseil de sa volonté,
1:12	afin que nous soyons à la louange de sa gloire, nous qui d'avance avons espéré dans le Mashiah.
1:13	En qui vous êtes aussi, après avoir entendu la parole de la vérité, l'Évangile de votre salut, en lui vous avez cru et vous avez été marqués du sceau<!--Voir 2 Co. 1:22.--> du Saint-Esprit de la promesse,
1:14	lequel est le gage<!--Vient du grec « arrhabon » qui se traduit en français par « arrhes ». Ce terme fait allusions aux monnaies données en gage d'un futur paiement, et en attendant que le solde soit payé. Voir 2 Co. 1:22, 5:5.--> de notre héritage pour la rédemption de ceux qu'il s'est acquis pour la louange de sa gloire.
1:15	C'est pourquoi, ayant aussi entendu parler de la foi que vous avez en notre Seigneur Yéhoshoua, et de l'amour que vous avez envers tous les saints,
1:16	je ne cesse de rendre grâce pour vous, en faisant mention de vous dans mes prières,
1:17	afin que l'Elohîm de notre Seigneur Yéhoshoua Mashiah, le Père de gloire, vous donne l'Esprit de sagesse et de révélation dans sa connaissance précise et correcte.
1:18	Qu'il illumine les yeux de votre intelligence<!--L'esprit comme faculté de compréhension, sentiment. Voir 1 Jn. 5:20.-->, afin que vous sachiez quelle est l'espérance de sa vocation, et quelle est la richesse de la gloire de son héritage dans les saints,
1:19	et quelle est l'excellente grandeur de sa puissance envers nous qui croyons selon l'efficacité de la puissance de sa force souveraine,
1:20	qu'il a déployée avec efficacité en Mashiah, quand il l'a ressuscité des morts et qu'il l'a fait asseoir à sa droite dans les lieux célestes,
1:21	au-dessus de toute principauté, et autorité, et puissance, et seigneurie, et au-dessus de tout nom qui se nomme, non seulement dans l'âge présent, mais aussi dans celui qui est à venir.

### Le Mashiah est le chef (tête) suprême de l'Assemblée

1:22	Il a aussi soumis toutes choses sous ses pieds, et il l'a donné pour tête au-dessus de toutes choses, à l'Assemblée,
1:23	qui est son corps, et la plénitude de celui qui remplit tout en tous.

## Chapitre 2

### Le salut par la grâce

2:1	Et vous, étant morts par les fautes et les péchés
2:2	dans lesquels vous marchiez autrefois, selon l'âge<!--Le mot traduit par « âge » vient du grec « aion » qui signifie aussi « temps », « période », etc.--> de ce monde, selon le chef<!--Voir Jn. 12:31.--> de l'autorité de l'air, de l'esprit qui opère maintenant dans les fils de l'obstination,
2:3	parmi lesquels nous vivions tous autrefois, selon les désirs<!--Ga. 5:16,24 ; Ro. 13:14 ; 1 Pi. 2:11.--> de notre chair, accomplissant les désirs de la chair et de nos pensées. Et nous étions par nature des enfants de colère comme les autres.
2:4	Mais Elohîm, qui est riche en miséricorde, à cause de son grand amour dont il nous a aimés,
2:5	nous aussi, étant morts par les fautes, il nous a vivifiés ensemble avec le Mashiah. C'est par grâce que vous êtes sauvés.
2:6	Et il nous a ressuscités ensemble, et nous a fait asseoir ensemble dans les lieux célestes en Mashiah Yéhoshoua,
2:7	afin qu’il montre dans les âges qui viennent l'immense richesse de sa grâce par sa bénignité envers nous, en Mashiah Yéhoshoua.
2:8	Car vous êtes sauvés par la grâce, par le moyen de la foi. Et cela ne vient pas de vous, c'est le don d'Elohîm.
2:9	Cela ne vient pas des œuvres, afin que personne ne se glorifie.
2:10	Car nous sommes son ouvrage, ayant été créés en Yéhoshoua Mashiah pour les bonnes œuvres qu'Elohîm a préparées d'avance, afin que nous marchions en elles.
2:11	C'est pourquoi souvenez-vous que vous, autrefois les nations dans la chair, appelés incirconcision par ce qui est appelé la circoncision, faite dans la chair par la main de l'homme,
2:12	vous étiez en ce temps-là sans Mashiah, privés de citoyenneté en Israël et étrangers aux alliances de la promesse, n'ayant pas d'espérance et sans Elohîm dans le monde.
2:13	Mais maintenant, par Mashiah Yéhoshoua, vous qui étiez autrefois éloignés, vous avez été rapprochés par le sang du Mashiah.

### Juifs et nations forment un seul corps

2:14	Car lui-même est notre paix, lui qui des deux n'en a fait qu'un en détruisant la clôture, le mur de séparation,
2:15	ayant aboli dans sa chair l'inimitié, la torah des commandements avec ses dogmes, afin que des deux il créât en lui-même un seul homme nouveau, en faisant la paix,
2:16	et qu'il réconciliât les uns et les autres en un seul corps avec Elohîm par le moyen de la croix, ayant détruit par elle l'inimitié.
2:17	Et il est venu prêcher la paix à vous qui étiez loin, et à ceux qui étaient près,
2:18	parce que nous avons par son moyen les uns et les autres accès auprès du Père dans un même Esprit.

### L'Assemblée véritable

2:19	Ainsi donc, vous n'êtes plus des étrangers ni des gens sans citoyenneté<!--Un étranger, celui qui vit dans un lieu sans avoir le droit de cité, de citoyenneté. Voir 1 Pi. 2:11.-->, mais concitoyens des saints et membres de la famille d'Elohîm.
2:20	Ayant été édifiés sur le fondement<!--Le fondement a été posé une fois pour toutes par les apôtres et les prophètes. Et ce fondement est notre Seigneur Yéhoshoua ha Mashiah (1 Co. 3:11).--> des apôtres et des prophètes, et Yéhoshoua Mashiah lui-même étant la pierre angulaire.
2:21	C'est en lui que toute la construction, ayant ses parties bien ajustées<!--Ex. 26:11.-->, s'élève pour être un temple saint dans le Seigneur.
2:22	C'est en lui que, vous aussi, vous êtes construits ensemble pour être une habitation d'Elohîm en Esprit.

## Chapitre 3

### Le mystère caché de tout temps<!--Col. 1:24-27.-->

3:1	C'est pour cela que moi, Paulos, le prisonnier du Mashiah Yéhoshoua pour vous, les nations...
3:2	Si toutefois vous avez appris quelle est l'administration<!--1 Co. 9:17.--> de la grâce d'Elohîm qui m'a été donnée pour vous.
3:3	C'est par révélation qu'il m'a fait connaître ce mystère, ainsi que je l'ai écrit ci-dessus en peu de mots.
3:4	Par où, en lisant, vous pouvez comprendre mon intelligence du mystère du Mashiah,
3:5	lequel n'a pas été porté à la connaissance des fils des humains dans les autres générations, comme il a été révélé maintenant par l'Esprit à ses saints apôtres et prophètes :
3:6	que les nations sont cohéritières et membres du même corps, et coparticipantes de sa promesse dans le Mashiah par le moyen de l'Évangile,
3:7	dont j'ai été fait serviteur, selon le don de la grâce d'Elohîm qui m'a été donnée selon l'efficacité de sa puissance.
3:8	Cette grâce m'a été donnée à moi, qui suis plus bas que le plus inférieur de tous les saints, pour annoncer parmi les nations la richesse incompréhensible du Mashiah,
3:9	et pour mettre en évidence devant tous quelle est la communion du mystère qui était caché de tout âge en Elohîm, qui a créé toutes choses par le moyen de Yéhoshoua Mashiah,
3:10	afin que les principautés et les autorités dans les lieux célestes connaissent aujourd'hui, par le moyen de l'Assemblée, la sagesse infiniment variée d'Elohîm,
3:11	selon le dessein des âges qu'il a accompli dans le Mashiah Yéhoshoua notre Seigneur,
3:12	par lequel nous avons la liberté et l'accès avec confiance, par le moyen de la foi en lui.
3:13	C'est pourquoi je vous prie de ne pas vous relâcher à cause de mes tribulations que j'endure par amour pour vous, elles sont votre gloire.
3:14	À cause de cela, je fléchis les genoux devant le Père de notre Seigneur Yéhoshoua ha Mashiah,
3:15	de qui toute la famille dans les cieux et sur la Terre tire son nom,
3:16	afin que selon la richesse de sa gloire, il vous donne d'être puissamment fortifiés par le moyen de son Esprit dans l'homme intérieur,
3:17	en sorte que le Mashiah habite dans vos cœurs par le moyen de la foi ; pour que vous soyez enracinés et fondés dans l'amour,
3:18	afin d’être éminemment capables de saisir avec tous les saints, quelle est la largeur et la longueur et la profondeur et la hauteur,
3:19	et de connaître l'amour du Mashiah qui surpasse toute connaissance, afin que vous soyez remplis jusqu'à toute la plénitude d'Elohîm.
3:20	Or à celui qui par la puissance qui agit en nous avec efficacité, peut faire infiniment au-delà de tout ce que nous demandons et pensons,
3:21	à lui soit la gloire dans l'Assemblée, en Mashiah Yéhoshoua, pour toutes les générations, pour les âges des âges ! Amen !

## Chapitre 4

### L'unité

4:1	Je vous prie donc, moi, le prisonnier dans le Seigneur, de marcher d'une manière digne de la vocation à laquelle vous avez été appelés,
4:2	avec toute humilité et douceur, avec patience, vous supportant les uns les autres dans l'amour,
4:3	vous efforçant de garder l'unité de l'Esprit dans le lien de la paix :
4:4	Un seul corps et un seul Esprit, comme aussi vous êtes appelés à une seule espérance par votre vocation.
4:5	Un seul Seigneur, une seule foi, un seul baptême,
4:6	un seul Elohîm et Père de tous, qui est au-dessus de tous, parmi tous et en vous tous.

### Les dons du Mashiah pour le perfectionnement et l'édification de son corps<!--1 Co. 12:4-11.-->

4:7	Mais la grâce est donnée à chacun de nous selon la mesure du don du Mashiah.
4:8	C'est pourquoi il dit : Étant monté dans la hauteur, il a capturé<!--« Rendre captif », « garder captif ».--> la captivité et il a donné des dons aux humains<!--Ps. 68:19.-->.
4:9	Or que signifie : Il est monté, sinon qu'il est premièrement descendu dans les parties inférieures de la Terre ?
4:10	Celui qui est descendu, c'est le même qui est monté au-dessus de tous les cieux, afin de remplir toutes choses.
4:11	Et lui-même a donné en effet les apôtres, et les prophètes, et les évangélistes, et les bergers et docteurs,
4:12	pour l'équipement<!--Le mot « équipement » vient du grec « katartismos » qui tire son origine du terme « katartizo » : « redresser, ajuster, compléter, raccommoder (ce qui a été abîmé), réparer, équiper ». Ainsi, les divers services ont pour vocation, d'une part, à réparer les dégâts causés par le péché dans les âmes, et d'autre part, à équiper les disciples afin de rentrer à leur tour dans leur propre service.--> des saints, pour l'œuvre du service<!--Le mot « service » vient du grec « diakonos » qui signifie « service, ministère de ceux qui répondent aux besoins des autres ». Yéhoshoua ha Mashiah lui-même a pris la forme d'un serviteur pour nous servir et non pour être servi (Mt. 20:28).-->, pour la construction<!--2 Co. 13:10.--> du corps du Mashiah,
4:13	jusqu'à ce que nous soyons tous parvenus à l'unité de la foi et de la connaissance précise et correcte du Fils d'Elohîm, à l'état de l'homme parfait, à la mesure de la stature de la plénitude du Mashiah,
4:14	afin que nous ne soyons plus des enfants ballottés par les vagues et emportés çà et là à tout vent de doctrine, par le jeu de dés<!--Vient du grec « kubos » un (cube), c'est-à-dire un dé pour jouer. Métaphorique : la déception des hommes, car les joueurs de dés trichaient quelquefois et trompaient leurs partenaires.--> des humains, par leur habilité dans l'art de l'égarement.
4:15	Mais en professant la vérité dans l'amour, nous grandirons en toutes choses en celui qui est la tête, le Mashiah.
4:16	C'est de lui que tout le corps, ayant ses parties bien ajustées et liées ensemble, par le moyen de chaque jointure de son assistance, tire son accroissement selon l'efficacité qu'il distribue à chaque membre, pour se construire lui-même dans l'amour.

### Se dépouiller du vieil être humain

4:17	Voici donc ce que je dis et ce que j'atteste dans le Seigneur : c'est que vous ne marchiez plus comme le reste des nations qui marchent dans la perversité de leur pensée.
4:18	Ayant leur pensée couverte par les ténèbres<!--Ro. 1:21.-->, étant étrangers à la vie d'Elohîm à cause de l'ignorance qui est en eux, à cause de l'endurcissement de leur cœur.
4:19	Devenus insensibles, ils se sont livrés à la luxure sans bride pour commettre toute sorte d'impureté avec cupidité.
4:20	Mais vous, ce n'est pas ainsi que vous avez appris le Mashiah,
4:21	si toutefois vous l'avez entendu et avez été instruits par lui, conformément à la vérité qui est en Yéhoshoua :
4:22	à vous dépouiller<!--Mettre de côté. Voir Hé. 12:1.--> pour ce qui est de votre conduite précédente, du vieil être humain qui se corrompt selon les désirs de la séduction,
4:23	à vous laisser renouveler par l'Esprit dans votre pensée,
4:24	et à vous revêtir du nouvel être humain, créé selon Elohîm, dans la justice et la sainteté de la vérité.
4:25	C'est pourquoi, ayant mis de côté<!--Voir Hé. 12:1.--> le mensonge, dites la vérité chacun à son prochain, parce que nous sommes membres les uns des autres.
4:26	Soyez en colère, et ne péchez pas. Que le soleil ne se couche pas sur votre indignation.
4:27	Ne donnez pas une occasion d'agir<!--Le mot grec « topos » signifie aussi « lieu », « espace manqué » comme s'il s'agissait d'un espace entouré.--> au diable.
4:28	Que celui qui volait ne vole plus, mais plutôt qu'il travaille en faisant de ses mains ce qui est bon, pour avoir de quoi donner à celui qui est dans le besoin.
4:29	Qu'aucun discours pourri ne sorte de votre bouche, mais ce qui est bon pour la construction selon le besoin, afin qu'il communique une grâce à ceux qui l'écoutent.
4:30	Et n'attristez pas le Saint-Esprit d'Elohîm, par lequel vous avez été marqués d’un sceau<!--Voir 2 Co. 1:22.--> pour le jour de la rédemption.
4:31	Que toute espèce d'amertume, et de fureur, et de colère, et de clameur, et de blasphème, avec toute espèce de malice soient bannis du milieu de vous.
4:32	Mais devenez doux les uns envers les autres, pleins de compassion, et vous pardonnant les uns aux autres, ainsi qu'Elohîm vous a pardonné par Mashiah.

## Chapitre 5

5:1	Devenez donc imitateurs d'Elohîm, comme des enfants bien-aimés,
5:2	et marchez dans l'amour, de même que le Mashiah nous a aimés et s'est livré lui-même à Elohîm pour nous en offrande et en sacrifice, comme un parfum de bonne odeur.
5:3	Mais que la relation sexuelle illicite, l'impureté sous toutes ses formes ou la cupidité ne soient pas même nommées parmi vous, comme il convient à des saints.
5:4	Ni obscénité, ni propos insensé, ni plaisanterie, choses qui ne sont pas convenables, mais plutôt l’action de grâce.
5:5	Car vous savez ceci, qu'aucun fornicateur, ni impur, ni cupide, qui est un idolâtre, n'a d'héritage dans le Royaume du Mashiah et Elohîm<!--Une construction identique se trouve au verset 20 du même chapitre : Elohîm et Père. Il s'agit d'un même être.-->.
5:6	Que personne ne vous séduise par de vains discours, car à cause de ces choses la colère d'Elohîm vient sur les fils de l'obstination.
5:7	Ne soyez donc pas leurs associés.
5:8	Car vous étiez autrefois ténèbre, mais maintenant vous êtes lumière dans le Seigneur. Conduisez-vous donc comme des enfants de la lumière<!--Les disciples de Yéhoshoua sont les enfants de la lumière (Jn. 12:36 ; 1 Th. 5:5). Yéhoshoua est la lumière du monde (Jn. 8:12).--> !
5:9	Car le fruit de l'Esprit est en toute bonté et justice et vérité.
5:10	Examinant ce qui est agréable au Seigneur.
5:11	Et ne participez<!--Devenir un participant parmi les autres, ou être associé à une chose. Ap. 18:4.--> pas aux œuvres stériles de la ténèbre, mais plutôt condamnez-les en effet !
5:12	Car il est honteux de dire les choses qu'ils font en secret.
5:13	Mais toutes les choses qui sont condamnées sont manifestées par la lumière, car la lumière est celle qui manifeste tout.
5:14	C'est pourquoi il dit : Réveille-toi, toi qui dors, et lève-toi d'entre les morts, et le Mashiah brillera<!--Es. 60:1.--> sur toi.
5:15	Prenez donc garde de vous conduire soigneusement, non comme des insensés, mais comme des sages,
5:16	rachetant le temps, parce que les jours sont mauvais.
5:17	C'est pourquoi ne devenez pas insensés, mais comprenez quelle est la volonté du Seigneur.
5:18	Et ne vous enivrez pas du vin dans lequel il y a le libertinage, mais soyez remplis par l'Esprit.
5:19	Vous parlant entre vous par des psaumes, et des hymnes et des cantiques spirituels, chantant et célébrant dans votre cœur les louanges du Seigneur ;
5:20	rendant toujours grâces pour toutes choses au Nom de notre Seigneur Yéhoshoua Mashiah, l'Elohîm et Père,
5:21	vous soumettant les uns aux autres<!--1 Pi. 5:5.--> dans la crainte d'Elohîm.

### Le mariage selon Elohîm

5:22	Femmes, soyez soumises à vos maris comme au Seigneur,
5:23	parce que le mari est la tête<!--Ou « chef ».--> de la femme, comme le Mashiah aussi est la tête de l'Assemblée qui est son corps et dont il est le Sauveur.
5:24	Mais comme l'Assemblée est soumise au Mashiah, de même que les femmes aussi le soient à leurs maris en tout.
5:25	Et vous maris, aimez vos femmes, comme le Mashiah a aimé l'Assemblée, et s'est livré lui-même pour elle,
5:26	afin de la sanctifier en la purifiant et en la lavant par le bain<!--Le mot grec « loutron » traduit par « bain » signifie également « baignade », « l'action de baigner ». Voir Tit. 3:5.--> d'eau de la parole,
5:27	afin qu'il se présente l'Assemblée à lui-même, glorieuse, n’ayant ni tache, ni ride, ni rien de semblable, mais sainte et sans défaut.
5:28	C'est ainsi que les maris doivent aimer leurs femmes comme leurs propres corps. Celui qui aime sa femme s'aime lui-même,
5:29	car personne n'a jamais haï sa propre chair, mais il la nourrit et la chérit d'un tendre amour, comme le Seigneur le fait pour l'Assemblée,
5:30	parce que nous sommes membres de son corps, de sa chair et de ses os.
5:31	C'est pourquoi l'homme quittera son père et sa mère et se joindra à sa femme, et les deux seront une seule chair.
5:32	Ce mystère est grand, or je parle du Mashiah et de l'Assemblée.
5:33	Que chacun de vous aussi aime sa femme comme lui-même, et que la femme craigne son mari.

## Chapitre 6

### La famille selon Elohîm

6:1	Enfants, obéissez à vos parents dans le Seigneur, car cela est juste.
6:2	Honore ton père et ta mère, c'est le premier commandement avec une promesse,
6:3	afin que tout aille bien pour toi et que tu vives longtemps sur la Terre.
6:4	Et vous, pères, n'irritez pas vos enfants, mais nourrissez-les jusqu'à maturité<!--Le verbe « nourrir » vient du grec « ektrepho » qui signifie également « élever », « entretenir », « apporter ».--> par l'éducation<!--Toute l'éducation des enfants (qui est relative à la culture de l'esprit et de la moralité, et qui utilise dans ce but tantôt des ordres et des avertissements, tantôt des réprimandes et des punitions). Elle inclut également l'exercice et le soin du corps. Châtiment, correction. Hé. 12:7.--> et l'avertissement du Seigneur.

### Les rapports entre les maîtres et les serviteurs selon Elohîm

6:5	Esclaves, obéissez à vos seigneurs selon la chair avec crainte et tremblement, dans la simplicité de votre cœur, comme au Mashiah,
6:6	ne les servant pas seulement sous leurs yeux, comme cherchant à plaire aux humains, mais comme esclaves du Mashiah, faisant de toute votre âme la volonté d'Elohîm,
6:7	servant avec bonté le Seigneur et non pas les humains,
6:8	sachant que ce que chacun aura fait de bon, il le recevra du Seigneur, soit esclave, soit libre.
6:9	Et vous, seigneurs, faites de même envers eux. Renoncez à la menace, sachant que votre Seigneur<!--De. 10:17 ; 1 Ti. 6:15 ; Ap. 17:14, 19:16.--> à vous aussi est dans les cieux, et qu'auprès de lui il n'y a pas d'égard à l'apparence des personnes<!--Ro. 2:11.-->.

### Le combat spirituel

6:10	Au reste, mes frères, soyez fortifiés dans le Seigneur et dans la puissance de sa force souveraine.
6:11	Revêtez-vous de l'armure<!--Vient du grec « panoplia » qui signifie « l'armement complet avec bouclier, épée, lance, casque, jambières et pectoral ». Voir le verset 13 et Lu. 11:22.--> complète d'Elohîm, afin de pouvoir résister aux ruses du diable.
6:12	Parce que notre lutte<!--Le mot grec utilisé ici est « pale ». Il était employé pour parler de la lutte entre deux combattants où chacun essaye de renverser l'autre ; la victoire étant acquise par le maintien de l'adversaire au sol, et en lui mettant la main sur la nuque.--> n'est pas contre le sang et la chair, mais contre les principautés<!--Vient du grec « arche » qui signifie « commencement, origine, la personne ou chose qui commence, la première personne ou chose d'une série, le premier, le chef ».-->, contre les autorités, contre les seigneurs du monde de la ténèbre de cet âge, contre les esprits de méchanceté qui sont dans les lieux célestes.
6:13	C'est pourquoi prenez l'armure complète d'Elohîm<!--Voir en annexe « L'armure du chrétien ».-->, afin que vous puissiez résister dans le mauvais jour et, après avoir tout accompli, tenir ferme.
6:14	Tenez donc ferme, ayant à vos reins la vérité pour ceinture, ayant revêtu la cuirasse de la justice,
6:15	et ayant vos pieds chaussés, prêts pour l'Évangile de paix.
6:16	Par-dessus tout, prenez le bouclier de la foi, avec lequel vous pourrez éteindre tous les dards enflammés du Mauvais.
6:17	Prenez aussi le casque du salut et l'épée de l'Esprit, qui est la parole d'Elohîm.
6:18	Priez en tout temps dans l'Esprit, à travers toutes sortes de prières et de supplications, et veillez à cela avec une entière persévérance et priez au sujet de tous les saints,
6:19	et aussi pour moi afin que, quand j’ouvre la bouche, la parole me soit donnée pour faire connaître avec assurance le mystère de l’Évangile,
6:20	pour lequel je suis ambassadeur dans les chaînes, afin qu’en lui je parle avec assurance comme il faut que je parle.

### Salutations

6:21	Mais afin que vous sachiez aussi ce qui me concerne et ce que je fais, Tuchikos, notre frère bien-aimé et fidèle serviteur du Seigneur, vous fera tout savoir.
6:22	Je l'ai envoyé vers vous pour cela même, afin que vous connaissiez ce qui nous concerne et qu'il console vos cœurs.
6:23	Shalôm aux frères, et amour, avec la foi, de la part d'Elohîm le Père et Seigneur, Yéhoshoua Mashiah !
6:24	Que la grâce soit avec tous ceux qui aiment notre Seigneur Yéhoshoua Mashiah d'une manière incorruptible ! Amen !
