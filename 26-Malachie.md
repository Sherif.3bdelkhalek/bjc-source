# Malakhi (Malachie) (Mal.)

Signification : Mon messager

Auteur : Malakhi (Malachie)

Thème : Message final de la première alliance envers Israël, une nation désobéissante

Date de rédaction : 5ème siècle av. J.-C.

Malakhi exerça son service en Yéhouda (Juda) après la reconstruction du temple et la reprise des cultes. Il annonça la venue du Mashiah et du messager qui devait le précéder, le nouvel Éliyah (Élie) que Yéhoshoua ha Mashiah (Jésus-Christ) reconnut en Yohanan le Baptiste (Jean-Baptiste). Ses écrits mettent en évidence l'importance de l'obéissance à la torah de YHWH.

## Chapitre 1

### Israël, le peuple aimé de YHWH

1:1	Fardeau, parole de YHWH à Israël, par la main de Malakhi.
1:2	Je vous ai aimés, dit YHWH. Et vous dites : En quoi nous as-tu aimés ? Ésav n'était-il pas le frère de Yaacov ? – déclaration de YHWH. Or j'ai aimé Yaacov,
1:3	et j'ai haï Ésav : j'ai mis ses montagnes en désolation, et son héritage aux chacals du désert.
1:4	Si Édom dit : Nous sommes détruits, nous rebâtirons les lieux ruinés ! Ainsi parle YHWH Tsevaot : Ils rebâtiront, et moi, je détruirai ! On les appellera territoire de la méchanceté, peuple contre lequel YHWH est irrité pour toujours.
1:5	Vos yeux le verront, et vous direz : Que YHWH soit glorifié sur les frontières d'Israël !

### Transgressions des prêtres après le retour d'exil

1:6	Un fils honore son père, et un serviteur son seigneur. Si je suis Père, où est l'honneur qui m'appartient ? Et si je suis Seigneur, où est la crainte qu'on a de moi ? dit YHWH Tsevaot, à vous prêtres, qui méprisez mon Nom, et qui dites : En quoi avons-nous méprisé ton Nom ?
1:7	Vous présentez sur mon autel du pain souillé, et vous dites : En quoi t'avons-nous profané ? C'est en disant : La table de YHWH est méprisable !
1:8	Quand vous présentez une bête aveugle pour la sacrifier, n'est-ce pas mal ? Quand vous en présentez une boiteuse ou malade, n'est-ce pas mal ? Présente-la à ton gouverneur, s’il te plaît. Prendra-t-il plaisir en toi ou te recevra-t-il favorablement ? dit YHWH Tsevaot.
1:9	Maintenant, s’il vous plaît, suppliez les faces de El, pour qu'il ait pitié de nous ! C'est par vos mains que cela a eu lieu, vous recevra-t-il favorablement ? dit YHWH Tsevaot.
1:10	Qui d'entre vous fermera les portes, afin que vous ne fassiez plus briller en vain mon autel ? Je ne prends aucun plaisir en vous, dit YHWH Tsevaot, et je n'accepte pas l'offrande de vos mains.
1:11	Car du soleil levant jusqu'au soleil couchant, mon Nom est grand parmi les nations. En tous lieux on brûle de l'encens en mon Nom et l'on présente des offrandes pures, car mon Nom est grand parmi les nations, dit YHWH Tsevaot.
1:12	Mais vous, vous le profanez, en disant : La table d'Adonaï est souillée, et son fruit est une nourriture à mépriser.
1:13	Vous dites aussi : Quelle fatigue ! Et vous soufflez dessus, dit YHWH Tsevaot. Vous faites venir ce qui est volé, boiteux ou malade. Vous le faites venir en offrande ! Accepterai-je cela de vos mains ? dit YHWH.
1:14	C'est pourquoi, maudit soit le trompeur, qui a dans son troupeau un mâle, et qui voue et sacrifie à Adonaï ce qui est corrompu ! Car je suis le Grand Roi, dit YHWH Tsevaot, et mon Nom est redoutable parmi les nations.

## Chapitre 2

### Reproches aux prêtres

2:1	Maintenant, à vous ce commandement, prêtres ! 
2:2	Si vous n'écoutez pas, si vous ne prenez pas à cœur de donner gloire à mon Nom, dit YHWH Tsevaot, j'enverrai sur vous la malédiction et je maudirai vos bénédictions. Oui, je les maudirai, parce que vous ne prenez pas cela à cœur.
2:3	Voici, je détruirai vos semences et je répandrai les excréments de vos victimes sur vos visages, les excréments de vos fêtes, et on vous emportera avec eux.
2:4	Alors vous saurez que je vous ai envoyé ce commandement, afin que mon alliance avec Lévi subsiste, dit YHWH Tsevaot.
2:5	Mon alliance avec lui était la vie et la paix, c'est ce que je lui accordai pour qu'il me craigne. Il a eu pour moi de la crainte, et il a tremblé devant mon Nom.
2:6	La torah de la vérité était dans sa bouche, et l'injustice n'a pas été trouvée sur ses lèvres. Il a marché avec moi dans la paix et dans la droiture, et il en a détourné beaucoup de l'iniquité.
2:7	Car les lèvres du prêtre gardent la connaissance, et c'est à sa bouche qu'on demande la torah, car il est un messager de YHWH Tsevaot.
2:8	Mais vous, vous vous êtes détournés de ce chemin, vous en avez fait trébucher beaucoup par le moyen de la torah, et vous avez corrompu l'alliance de Lévi, a dit YHWH Tsevaot.

### Infidélités des prêtres

2:9	C'est pourquoi, moi aussi, je vous ai rendus méprisables et vils pour tout le peuple, parce que vous n'avez pas gardé mes voies, et que vous avez eu égard aux personnes<!--Littéralement : « portez des faces ».--> à propos de la torah.
2:10	N'avons-nous pas tous un seul Père ? N'est-ce pas un seul El qui nous a créés ? Pourquoi trahissons-nous chacun son frère, en profanant l'alliance de nos pères ?
2:11	Yéhouda s'est montré infidèle, et une abomination a été commise en Israël et à Yeroushalaim. Car Yéhouda a profané ce qui est consacré à YHWH, ce qu'il aime, il s'est marié à la fille d'un el étranger.
2:12	YHWH retranchera l'homme qui fait cela, celui qui veille et qui répond, il le retranchera des tentes de Yaacov, et il retranchera celui qui présente une offrande à YHWH Tsevaot.
2:13	Voici une seconde chose que vous faites : Vous couvrez l'autel de YHWH de larmes, de plaintes et de gémissements, de sorte qu'il ne regarde plus aux offrandes et qu'il ne prend plus plaisir à ce qui vient de vos mains.
2:14	Et vous dites : Pourquoi ? C'est parce que YHWH a été témoin entre toi et la femme de ta jeunesse, que tu as trahie, elle qui est ta compagne, la femme de ton alliance.
2:15	Or il y en a eu un seul qui ne l'a pas fait, parce qu'il avait le reste de l'Esprit. Un seul, et pourquoi ? Parce qu'il cherchait la postérité d'Elohîm. Prenez garde à votre esprit. Que personne ne trahisse la femme de sa jeunesse !
2:16	Car YHWH, l'Elohîm d'Israël a dit qu'il hait la répudiation. Mais on couvre la violence sous sa robe, a dit YHWH Tsevaot. Prenez garde à votre esprit ! Ne trahissez pas !

### Fausse profession religieuse

2:17	Vous fatiguez YHWH par vos paroles et vous dites : En quoi l'avons-nous fatigué ? C'est quand vous dites : Quiconque fait le mal est bon aux yeux de YHWH, et c’est en eux qu’il prend plaisir ! Ou bien : Où est l'Elohîm du jugement ?

## Chapitre 3

### Prophétie sur la venue de Yohanan le Baptiste

3:1	Voici, j'enverrai mon messager<!--Ce messager, ou Éliyah le prophète, est Yohanan le Baptiste (Es. 40:1-3 ; Mt. 3:1-15, 11:14, 17:10-13 ; Mc. 1:1-8, 9:11-13 ; Lu. 1:17, 3:1-5).-->. Il préparera le chemin devant moi. Et soudain entrera dans son temple le Seigneur que vous cherchez, le messager de l'alliance que vous désirez. Voici, il vient, dit YHWH Tsevaot.
3:2	Qui contiendra le jour de sa venue ? Qui se tiendra debout quand il paraîtra ? Car il sera comme le feu du fondeur et comme la potasse des foulons.
3:3	Il s’assiéra, il raffinera et purifiera l’argent. Il purifiera les fils de Lévi, il les épurera comme l'or et l'argent, et ils présenteront à YHWH des offrandes avec justice.
3:4	L'offrande de Yéhouda et de Yeroushalaim sera agréable à YHWH, comme aux anciens jours, comme aux années d'autrefois.
3:5	Je m'approcherai de vous pour le jugement, et je me hâterai de témoigner contre les sorciers et les adultères, contre ceux qui jurent faussement, et contre ceux qui extorquent le salaire du mercenaire, de la veuve et de l'orphelin, qui pervertissent l'étranger et qui ne me craignent pas, dit YHWH Tsevaot.
3:6	Car moi, YHWH, je ne change pas. À cause de cela, fils de Yaacov, vous n'avez pas été consumés.

### Hypocrisie d'Israël

3:7	Depuis le temps de vos pères, vous vous êtes détournés de mes statuts, et vous ne les avez pas gardés. Revenez à moi, et je reviendrai à vous, a dit YHWH Tsevaot. Et vous dites : En quoi reviendrons-nous ?
3:8	Un être humain volera-t-il Elohîm ? Car vous me volez, et vous dites : En quoi t'avons-nous volé ? Dans les dîmes et les offrandes.
3:9	Vous êtes maudits, la malédiction est sur vous parce que vous me volez, toute la nation !
3:10	Apportez toutes les dîmes<!--Il est question ici de la dîme de la dîme que les Lévites donnaient aux prêtres. Cette dîme était rapportée aux magasins ou greniers (Né. 10:36-40), là aussi étaient stockées toutes sortes de trésors. Pour les autres dîmes, voir le commentaire en De. 14:22-29.--> aux magasins, afin qu'il y ait de la provision dans ma maison. S'il vous plaît, éprouvez-moi à ce sujet, dit YHWH Tsevaot, si je n'ouvre pas pour vous les écluses des cieux, si je ne répands pas sur vous la bénédiction jusqu'à ce qu'il n'y ait plus assez de place.
3:11	Je réprimanderai par amour pour vous celui qui dévore, et il ne vous détruira pas les fruits du sol, et vos vignes ne seront pas stériles dans la campagne, a dit YHWH Tsevaot.
3:12	Toutes les nations vous diront heureux, car vous serez une terre de délices, dit YHWH Tsevaot.
3:13	Vos paroles sont rudes contre moi, a dit YHWH. Et vous dites : Qu'avons-nous dit contre toi ?
3:14	Vous avez dit : C'est en vain que l'on sert Elohîm. Qu'avons-nous gagné à garder ses injonctions et à marcher dans le deuil devant YHWH Tsevaot ?
3:15	Maintenant nous tenons pour heureux les orgueilleux. Oui, ceux qui pratiquent la méchanceté sont établis. Oui, ils mettent Elohîm à l'épreuve et ils échappent !

### Un reste demeure fidèle à YHWH

3:16	Alors ceux qui craignent YHWH se parlèrent, chaque homme avec son compagnon. YHWH fut attentif et il écouta. Et un livre de souvenir<!--Voir Est. 6:1.--> fut écrit devant lui pour ceux qui craignent YHWH et qui pensent à son Nom.
3:17	Ils seront ma propriété, dit YHWH Tsevaot, au jour que je prépare. Je les épargnerai comme un homme épargne<!--Épargner, pitié, avoir compassion de.--> son fils qui le sert.
3:18	Revenez et distinguez le juste du méchant, celui qui sert Elohîm de celui qui ne le sert pas.

### La parousie du jour de YHWH

3:19	Car voici, le jour vient, brûlant comme une fournaise. Tous les orgueilleux et tous ceux qui pratiquent la méchanceté deviendront du chaume. Ce jour qui vient les embrasera, dit YHWH Tsevaot, il ne leur laissera ni racine ni rameau.
3:20	Mais pour vous qui craignez mon Nom se lèvera le Soleil de justice<!--Lu. 1:78-79.-->, et la guérison sera sous ses ailes. Vous sortirez et vous sauterez comme les veaux d'une étable.
3:21	Vous foulerez les méchants, car ils deviendront de la cendre sous les plantes de vos pieds, au jour où je ferai mon œuvre, dit YHWH Tsevaot.
3:22	Souvenez-vous de la torah de Moshé, mon serviteur, auquel j'ai prescrit en Horeb, pour tout Israël, des statuts et des ordonnances.

### La venue d'Éliyah avant le jour de YHWH

3:23	Voici, je vous enverrai Éliyah, le prophète<!--Voir commentaire en Mal. 3:1.-->, avant que le jour grand et redoutable de YHWH vienne.
3:24	Il ramènera le cœur des pères à leurs fils, et le cœur des fils à leurs pères, de peur que je ne vienne et que je ne frappe la terre, la vouant à une entière destruction.
