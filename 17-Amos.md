# Amowc (Amos) (Am.)

Signification : Fardeau, porteur de fardeau

Auteur : Amowc

Thème : Jugement sur le péché

Date de rédaction : 8ème siècle av. J.-C.

Originaire de Tekoa, Amowc exerça son service dans le royaume du nord, au temps d'Ouzyah (Ozias), roi de Yéhouda (Juda), et de Yarobam II (Jéroboam II), roi d'Israël. Il fut aussi le contemporain des prophètes Hoshea (Osée), Mikayah (Michée), Yonah (Jonas) et Yesha`yah (Ésaïe).

Alors que le peuple juif jouissait d'une certaine prospérité, l'immoralité et les sacrilèges prirent place dans le royaume. Amowc avertit le peuple de son péché et du jugement qu'il encourait. Il lui rappela la bonté d'Elohîm et l'invita à revenir à YHWH et à lui rester fidèle.

## Chapitre 1

1:1	Les paroles d'Amowc qui était parmi les bergers de Tekoa, visions qu'il eut concernant Israël, aux temps d'Ouzyah, roi de Yéhouda, et de Yarobam, fils de Yoash, roi d'Israël, deux ans avant le tremblement de terre<!--Za. 14:5.-->.
1:2	Il dit : De Sion YHWH rugit, de Yeroushalaim il fait entendre sa voix. Les pâturages des bergers se lamentent, et le sommet du Carmel est desséché<!--Jé. 25:30 ; Joë. 4:16.-->.

### YHWH annonce ses jugements sur les villes et les terres d'alentour

1:3	Ainsi parle YHWH : À cause de trois transgressions de Damas, et même de quatre, je ne le ferai pas revenir<!--Revenir à Elohîm, se repentir. Voir Am. 9:14.-->, parce qu'ils ont foulé Galaad avec des herses de fer<!--Es. 17:1.-->.
1:4	Et j'enverrai le feu dans la maison de Hazaël, et il dévorera le palais de Ben-Hadad.
1:5	Je briserai aussi les verrous de Damas, j'exterminerai de Bikath-Aven ses habitants, et de Beth-Éden celui qui tient le sceptre, et le peuple de Syrie sera emmené en exil à Kir, dit YHWH.
1:6	Ainsi parle YHWH : À cause de trois transgressions de Gaza, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'ils ont emmené des captifs en grand nombre pour les livrer à Édom<!--Ez. 25:13-17.-->.
1:7	J'enverrai le feu dans les murailles de Gaza, et il dévorera ses palais.
1:8	J'exterminerai d'Asdod les habitants et d'Askalon celui qui tient le sceptre, je tournerai ma main contre Ékron, et le reste des Philistins périra, dit Adonaï YHWH.
1:9	Ainsi parle YHWH : À cause de trois transgressions de Tyr, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'ils ont livré à Édom beaucoup d'exilés et ne se sont pas souvenus de l'alliance fraternelle<!--Ez. 26:2.-->.
1:10	J'enverrai le feu dans les murailles de Tyr, et il dévorera ses palais.
1:11	Ainsi parle YHWH : À cause de trois transgressions d'Édom, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'il a poursuivi son frère avec l'épée et qu'il a altéré ses compassions, parce que sa colère déchire continuellement et qu'il garde sa fureur éternellement.
1:12	J'enverrai le feu dans Théman, et il dévorera les palais de Botsrah<!--Jé. 49:7 ; Ab. 1:9.-->.
1:13	Ainsi parle YHWH : À cause de trois transgressions des enfants d'Ammon, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'ils ont fendu le ventre des femmes enceintes de Galaad pour élargir leurs frontières<!--Ez. 21:33 ; So. 2:8.-->.
1:14	J'allumerai le feu dans les murs de Rabba et il en dévorera les palais au milieu d'alarme de guerre le jour du combat, dans le tourbillon au jour du vent d'orage.
1:15	Leur roi ira en captivité, lui et ses chefs ensemble, dit YHWH.

## Chapitre 2

### Suite des jugements prononcés sur les villes et les terres d'alentour

2:1	Ainsi parle YHWH : À cause de trois transgressions de Moab, et même de quatre, je ne le ferai pas revenir, parce qu'il a brûlé les os du roi d'Édom jusqu'à les calciner.
2:2	J'enverrai le feu dans Moab et il dévorera les palais de Qeriyoth, et Moab mourra dans le vacarme, au milieu d'alarme de guerre, au son du shofar<!--Ez. 25:8-11.-->.
2:3	Je supprimerai le juge au milieu de lui et je tuerai ensemble avec lui tous les chefs, dit YHWH.

### Yéhouda et Israël jugés à cause de leurs iniquités

2:4	Ainsi parle YHWH : À cause de trois transgressions de Yéhouda, et même de quatre, je ne le ferai pas revenir, parce qu'ils ont rejeté la torah de YHWH et n'ont pas gardé ses ordonnances, et que leurs mensonges, derrière lesquels leurs pères ont marché, les ont fait égarer.
2:5	J'enverrai le feu dans Yéhouda et il dévorera les palais de Yeroushalaim.
2:6	Ainsi parle YHWH : À cause de trois transgressions d'Israël, et même de quatre, je ne le ferai pas revenir, parce qu'ils ont vendu le juste pour de l'argent et le pauvre pour une paire de sandales.
2:7	Ils soupirent après la poussière de la terre pour la jeter sur la tête des faibles, et ils pervertissent la voie des pauvres. Le fils et le père vont vers la même jeune fille pour profaner mon saint Nom.
2:8	Et ils se couchent près de chaque autel, sur les vêtements qu'ils ont pris en gage, et boivent dans la maison de leurs elohîm le vin des condamnés.
2:9	J'ai pourtant détruit devant eux les Amoréens qui étaient hauts comme les cèdres et forts comme les chênes, j'ai détruit son fruit au-dessus et leurs racines en dessous<!--No. 21:24-25 ; Jos. 24:8.-->.
2:10	Je vous ai fait monter de la terre d'Égypte et je vous ai conduits dans le désert 40 ans pour que vous preniez possession de la terre des Amoréens.
2:11	J'ai suscité des prophètes parmi vos fils et des nazaréens<!--Le mot « nazaréen » vient de l'hébreu « nâzîr », de la racine « nâzar » qui signifie « séparer ». Il y avait deux types de nazaréens. Premièrement, ceux qui étaient appelés par Elohîm. Par exemple : Shimshôn (Samson) (Jg. 13:2-7), Shemouél (Samuel) (1 S. 1:11) et Yohanan le Baptiste (Jean-Baptiste) (Lu. 1:15). Deuxièmement, les personnes qui voulaient se consacrer à Elohîm (No. 6:1-3).--> parmi vos jeunes hommes. N'en est-il pas ainsi, fils d'Israël ? – déclaration de YHWH.
2:12	Mais vous avez fait boire du vin aux nazaréens, et vous avez donné cet ordre aux prophètes leur disant : Ne prophétisez plus<!--Es. 30:10 ; Jé. 11:21 ; Mi. 2:6.--> !
2:13	Voici que moi je fais vaciller ce qui est sous vous, comme vacille un chariot plein de gerbes.
2:14	La fuite sera perdue pour celui qui est rapide, la force du fort ne s'affermira pas, et l'homme vaillant ne sauvera pas son âme<!--Jé. 46:6.-->.
2:15	Celui qui manie l'arc ne pourra pas tenir ferme, celui qui a les pieds légers n'échappera pas et celui qui monte le cheval ne sauvera pas son âme.
2:16	Celui qui a le cœur plein de courage parmi les hommes vaillants s'enfuira nu en ce jour-là, – déclaration de YHWH.

## Chapitre 3

### La maison de Yaacov coupable devant YHWH

3:1	Écoutez cette parole, que YHWH a déclarée contre vous, enfants d’Israël, contre toutes les familles que j'ai fait monter de la terre d'Égypte, en disant :
3:2	Je vous ai connus, vous seuls parmi toutes les familles du sol, c'est pourquoi je vous châtierai pour toutes vos iniquités<!--Ex. 19:5-6 ; Ps. 147:19-20.-->.
3:3	Deux hommes marchent-ils ensemble s'ils ne se sont pas rencontrés ?
3:4	Le lion rugit-il dans la forêt sans avoir une proie ? Le jeune lion fait-il entendre sa voix de sa tanière s'il n'a rien capturé ?
3:5	L'oiseau tombe-t-il dans le filet qui est à terre sans qu'il y ait de piège ? Le filet s'élève-t-il du sol sans avoir pris quelque chose ?
3:6	Sonne-t-on du shofar dans une ville sans que le peuple ait peur ? Arrive-t-il un mal dans une ville sans que YHWH ne l'ait ordonné<!--Es. 45:7 ; La. 3:37-38.--> ?
3:7	Car Adonaï YHWH ne fait rien<!--Vient de l'hébreu « dabar » et signifie « parole », « discours », « chose ». On peut traduire ce verset par : « Car Adonaï YHWH ne fait aucune chose sans avoir révélé son secret à ses serviteurs les prophètes » ou par « Car Adonaï YHWH n'accomplit aucune parole sans avoir révélé son secret à ses serviteurs les prophètes ».--> sans avoir révélé son secret à ses serviteurs les prophètes.
3:8	Le lion rugit, qui ne craindrait ? Adonaï YHWH parle, qui ne prophétiserait<!--Lorsque YHWH parle, des prophètes sont suscités (Jé. 20:7-9 ; Mi. 3:8 ; Ac. 4:20).--> ?
3:9	Faites entendre ceci dans les palais d'Asdod et dans les palais de la terre d'Égypte, et dites : Rassemblez-vous sur les montagnes de Samarie et regardez les grands désordres au milieu d'elle, et ceux auxquels on a fait tort dans son sein !
3:10	Car ils ne savent pas faire ce qui est juste, – déclaration de YHWH –, ils amassent la violence et la rapine dans leurs palais.
3:11	C'est pourquoi ainsi parle Adonaï YHWH : L'ennemi autour de ta terre fera descendre loin de toi ta force, et tes palais seront pillés.
3:12	Ainsi parle YHWH : Comme un berger sauve de la gueule d'un lion les deux jambes ou le bout d'une oreille, ainsi les fils d'Israël qui habitent dans Samarie seront arrachés de l'angle d'un lit et de l'asile de Damas.
3:13	Écoutez et protestez contre la maison de Yaacov, – déclaration d'Adonaï YHWH, Elohîm Tsevaot :
3:14	Le jour où je punirai Israël pour ses transgressions, j'exercerai aussi mon châtiment sur les autels de Béth-El ; les cornes de l'autel seront retranchées et tomberont à terre.
3:15	Je frapperai la maison d'hiver et la maison d'été, les maisons d'ivoire seront détruites et les grandes maisons prendront fin, – déclaration de YHWH.

## Chapitre 4

### YHWH condamne les sacrifices du peuple

4:1	Écoutez cette parole, vaches de Bashân qui vous tenez sur la montagne de Samarie, vous qui opprimez les faibles, qui écrasez les pauvres et qui dites à leurs seigneurs : Apportez afin que nous buvions !
4:2	Adonaï YHWH l'a juré par sa sainteté : Voici, les jours viennent sur vous où l'on vous enlèvera avec des crochets, et votre postérité avec des hameçons de pêche<!--Jé. 16:16 ; Ha. 1:14-16.-->.
4:3	Les femmes sortiront par des brèches, chacune devant soi, et elles seront jetées hors de la haute forteresse, – déclaration de YHWH.
4:4	Allez à Béth-El et transgressez ! À Guilgal, et transgressez davantage ! Allez avec vos sacrifices dès le matin et vos dîmes tous les trois ans<!--Voir commentaires en No. 18:21 et Mal. 3:10.--> !
4:5	Faites fumer vos offrandes d'action de grâces avec du levain ! Proclamez vos offrandes volontaires, faites-les connaître ! Car c'est là ce que vous aimez, fils d'Israël, – déclaration d'Adonaï YHWH<!--Lé. 2:1.-->.

### Endurcissement du peuple malgré les châtiments de YHWH

4:6	C'est pourquoi, je vous ai donné la propreté des dents dans toutes vos villes, le manque de pain dans toutes vos demeures. Mais malgré cela, vous n'êtes pas revenus vers moi, – déclaration de YHWH.
4:7	Je vous ai même refusé la pluie quand il restait encore trois mois jusqu'à la moisson, j'ai fait pleuvoir sur une ville et je n'ai pas fait pleuvoir sur une autre ville. Une parcelle a été arrosée par la pluie, et l'autre parcelle sur laquelle il n'a pas plu s’est desséchée<!--1 R. 8:35, 17:1 ; Es. 5:6 ; Ag. 1:10-11.-->.
4:8	Deux, trois villes allaient en chancelant vers une ville pour boire de l’eau, sans être rassasiées, mais vous n'êtes pas revenus vers moi, – déclaration de YHWH.
4:9	Je vous ai frappés par la flétrissure et par la rouille. Vos nombreux jardins, vos vignes, vos figuiers et vos oliviers ont été dévorés par les sauterelles, mais vous n'êtes pas revenus vers moi, – déclaration de YHWH<!--De. 28:22-39 ; 1 R. 8:37 ; Ag. 2:17 ; 2 Ch. 6:28.-->.
4:10	J’ai envoyé contre vous la peste par le chemin de l’Égypte, j'ai fait mourir par l'épée vos jeunes hommes et vos chevaux en captivité. J'ai fait monter jusqu’à vos narines la puanteur de vos camps, mais vous n'êtes pas revenus vers moi, – déclaration de YHWH<!--Ez. 14:19.-->.
4:11	Je vous ai renversés de la même manière qu'Elohîm renversa Sodome et Gomorrhe, et vous êtes devenus comme un tison arraché du feu, mais vous n'êtes pas revenus vers moi, – déclaration de YHWH<!--Ge. 19:24 ; Jé. 49:18 ; Za. 3:2.-->.
4:12	C'est pourquoi je te traiterai de la même manière, Israël ! Et parce que je te traiterai ainsi, prépare-toi à la rencontre de ton Elohîm, Israël !
4:13	Car voici celui qui a formé les montagnes et créé le vent, qui déclare à l'être humain quelle est sa pensée, qui fait l'aube et l'obscurité, et qui marche sur les hauteurs de la Terre. Son Nom est YHWH, Elohîm Tsevaot.

## Chapitre 5

### La maison d'Israël invitée à revenir entièrement à YHWH

5:1	Écoutez cette parole, le chant funèbre que j'élève sur vous, maison d'Israël :
5:2	Elle est tombée, elle ne se relèvera plus, la vierge d'Israël. Elle est jetée au sol et il n'y a personne pour la relever.
5:3	Car ainsi parle Adonaï YHWH à la maison d'Israël : La ville d'où sortait un millier d'hommes n'en aura plus que 100, et celle d'où sortait une centaine n'en aura plus que 10.
5:4	Car ainsi parle YHWH à la maison d'Israël : Cherchez-moi et vous vivrez !
5:5	Ne cherchez pas Béth-El, n'allez pas à Guilgal, et ne passez pas à Beer-Shéba. Car Guilgal ira en exil, elle ira en exil et Béth-El sera détruite<!--Os. 4:15.-->.
5:6	Cherchez YHWH et vous vivrez, de peur qu'il ne saisisse comme un feu la maison de Yossef, et que ce feu ne la consume, sans qu'il y ait personne à Béth-El pour l'éteindre.
5:7	Ils changent le jugement en absinthe et ils renversent la justice<!--Es. 5:26-28 ; Ha. 1:1-3.-->.
5:8	Il a fait les Pléiades et l'Orion<!--Job 9:9.-->, il change en matin les profondes ténèbres, il obscurcit le jour en nuit, il appelle les eaux de la mer et les répand sur les faces de la Terre. YHWH est son Nom<!--Es. 58:8-10 ; Job 9:9, 38:31.-->.
5:9	Il fait éclater la ruine sur les puissants et la ruine vient sur les villes fortes.
5:10	Ils haïssent celui qui les corrige à la porte et ils ont en abomination celui qui parle avec intégrité.
5:11	C'est pourquoi, parce que vous opprimez le pauvre et que vous prélevez sur lui un tribut de blé, vous avez bâti des maisons en pierres de taille, mais vous n'y habiterez pas ; vous avez planté des vignes délicieuses, mais vous n'en boirez pas le vin.
5:12	Car je connais vos transgressions, elles sont nombreuses, et vos péchés se sont multipliés. Vous êtes des oppresseurs du juste, et des preneurs de rançon, et vous pervertissez le droit des pauvres à la porte.
5:13	C'est pourquoi le perspicace garde le silence en ce temps-ci, car les temps sont mauvais.
5:14	Recherchez ce qui est bon et non ce qui est mauvais, afin que vous viviez, et qu'ainsi YHWH, Elohîm Tsevaot, soit avec vous, comme vous l'avez dit.
5:15	Haïssez ce qui est mauvais, aimez ce qui est bon et établissez la justice à la porte. Peut-être YHWH, Elohîm Tsevaot, aura pitié des restes de Yossef.

### Le jour de YHWH

5:16	C'est pourquoi, ainsi parle YHWH, Elohîm Tsevaot, Adonaï : Sur toutes les places il y aura des lamentations, dans toutes les rues on dira : Hélas ! Hélas ! On appellera au deuil le laboureur, et aux lamentations ceux qui s'y connaissent en gémissements.
5:17	Et il y aura des lamentations dans les vignes, car je passerai au milieu de toi, dit YHWH.
5:18	Malheur à vous qui désirez le jour de YHWH<!--L'expression « le jour du Seigneur » ou « le jour de YHWH » est une période durant laquelle Yéhoshoua ha Mashiah interviendra ouvertement dans les affaires des êtres humains. Elle est utilisée 19 fois dans le Tanakh (Es. 2:12, 13:6-9 ; Ez. 13:5, 30:3 ; Joë. 1:15, 2:1,11,31, 3:14 ; Am. 5:18, 20 ; Ab. 1:15 ; So. 1:7,14 ; Za. 14:1 ; Mal. 3:23) et 4 fois dans le Testament de Yéhoshoua (Jésus) (Ac. 2:20 ; 2 Th. 2:2 ; 2 Pi. 3:10). On y fait également allusion dans d'autres passages (Ap. 6:17, 16:14).-->. Que sera-t-il pour vous, le jour de YHWH ? Il sera ténèbres et non lumière.
5:19	C'est comme si un homme s'enfuit devant un lion et qu'un ours le rencontre : ou qui entre dans sa maison, appuie sa main sur le mur, et un serpent le mord.

### Mépris du droit et de la justice

5:20	Le jour de YHWH n'est-il pas ténèbre et non lumière ? N'est-il pas obscur, sans éclat ?
5:21	Je hais, je rejette vos fêtes, et je ne veux plus sentir l'odeur de vos assemblées solennelles.
5:22	Car si vous faites monter pour moi des holocaustes et des offrandes de grain, je ne les accepterai pas favorablement, et les bêtes grasses de vos offrandes de paix<!--Voir commentaire en Lé. 3:1.-->, je ne les regarderai pas.
5:23	Ôtez de devant moi le bruit de vos chansons ! Je n'écouterai pas la mélodie de vos luths.
5:24	Mais que le jugement coule comme de l'eau, et la justice comme un torrent intarissable.
5:25	Est-ce à moi, maison d'Israël, que vous avez offert des sacrifices et des offrandes dans le désert pendant 40 ans ?
5:26	Au contraire, vous avez porté Sikkouth<!--« Une divinité babylonienne », « une tente ».-->, votre roi, Kiyoun<!--Kiyoun est une divinité de l'antiquité ainsi que l'un des nombreux noms donnés au soleil par les civilisations antiques.-->, votre idole, l'étoile de votre elohîm que vous vous êtes fabriquée.
5:27	Je vous transporterai au-delà de Damas, dit YHWH, dont le Nom est Elohîm Tsevaot.

## Chapitre 6

### Ceux qui prospèrent seront emmenés captifs

6:1	Malheur à ceux qui vivent tranquilles dans Sion, à ceux qui mettent leur confiance dans la montagne de Samarie, surnommés : La première des nations ! Vers qui va la maison d'Israël !
6:2	Passez à Kalneh et regardez, allez de là à Hamath-Rabbah, puis descendez à Gath chez les Philistins. Ces villes sont-elles plus prospères que vos deux royaumes, ou leur territoire n'est-il pas plus étendu que votre territoire ?
6:3	Vous qui éloignez le jour du malheur et qui approchez le règne de la violence.
6:4	Vous qui vous couchez sur des lits d'ivoire et qui êtes étendus sur vos coussins, vous mangez les agneaux du troupeau et les veaux choisis dans les étables.
6:5	Vous bégayez au son du luth, vous inventez des instruments de musique comme David.
6:6	Vous buvez le vin dans de grandes cuvettes, vous vous oignez avec les meilleures huiles, mais vous n'êtes pas affligés de la ruine de Yossef.
6:7	C'est pourquoi maintenant ils vont être exilés en tête des exilés, et l'orgie des voluptueux disparaîtra.
6:8	Adonaï YHWH l'a juré en son âme, – déclaration de YHWH, Elohîm Tsevaot : Je déteste l'orgueil de Yaacov, et je hais ses palais. C'est pourquoi je livrerai la ville et tout ce qui est en elle.
6:9	Et s'il arrive qu'il reste dix hommes dans une maison, ils mourront.
6:10	Et l’oncle de l’un d’eux, et celui qui doit le brûler, le prendra pour sortir de la maison les os, et il dira à celui qui est au fond de la maison : Y a-t-il encore quelqu'un avec toi ? Et il répondra : C'est fini ! Et il dira : Tais-toi ! Car nous ne pouvons faire mention du Nom de YHWH !
6:11	Car voici, YHWH donne ses ordres : il frappera la grande maison par des gouttes de rosée, et la petite maison par des fissures.
6:12	Les chevaux courent-ils sur les rochers ? Y laboure-t-on avec des bœufs, pour que vous ayez changé la droiture en poison, et le fruit de la justice en absinthe ?
6:13	Vous vous réjouissez de choses qui ne sont que néant, vous dites : N'est-ce pas par notre force que nous avons acquis de la puissance ?
6:14	Car voici, j'élèverai contre vous, maison d'Israël, – déclaration de YHWH, Elohîm Tsevaot –, une nation qui vous opprimera depuis l'entrée de Hamath jusqu'au torrent de la région aride.

## Chapitre 7

### Avertissement<!--Am. 8:1-3, 9:10.-->

7:1	Ainsi me fit voir Adonaï YHWH, et voici, il formait un essaim de criquets au temps où la dernière récolte commençait à pousser. Et voici, c'était la dernière récolte après l'herbe fauchée du roi.
7:2	Et il arriva, lorsqu’ils eurent achevé de dévorer l'herbe de la terre, que je dis : Adonaï YHWH, pardonne, s'il te plaît ! Comment Yaacov subsistera-t-il ? Car il est faible.
7:3	YHWH se repentit de cela. Cela n'arrivera pas, dit YHWH.
7:4	Ainsi me fit voir Adonaï YHWH, et voici, Adonaï YHWH appelait le feu pour combattre. Celui-ci dévora le grand abîme, puis il dévora le territoire.
7:5	Je dis : Adonaï YHWH ! Arrête, s'il te plaît ! Comment Yaacov se relèvera-t-il ? Car il est petit.
7:6	YHWH se repentit de cela. Cela non plus n'arrivera pas, dit Adonaï YHWH.
7:7	Ainsi il me fit voir, et voici, Adonaï se tenait debout sur un mur de fil à plomb, et dans sa main il y avait un fil à plomb.
7:8	YHWH me dit : Que vois-tu, Amowc ? Je dis : Un fil à plomb. Adonaï me dit : Voici, je mettrai un fil à plomb au milieu de mon peuple d'Israël : Je ne continuerai plus à passer outre pour lui.
7:9	Les hauts lieux de Yitzhak seront ravagés, les sanctuaires d'Israël seront détruits et je me lèverai contre la maison de Yarobam avec l'épée.

### Amatsyah accuse Amowc (Amos) devant Yarobam (Jéroboam)

7:10	Amatsyah, prêtre de Béth-El, envoya dire à Yarobam roi d'Israël : Amowc conspire contre toi au milieu de la maison d'Israël. La terre ne pourrait supporter toutes ses paroles.
7:11	Car ainsi a dit Amowc : Yarobam mourra par l'épée et Israël ira en exil, il ira en exil loin de son sol.
7:12	Amatsyah dit à Amowc : Voyant<!--Voyant ou prophète.--> ! Va, fuis pour toi vers la terre de Yéhouda, mange là le pain, et là tu prophétiseras.
7:13	Mais ne continue pas à prophétiser à Béth-El<!--Béth-El, qui signifie « maison d'Elohîm », était devenue le sanctuaire du roi Yarobam.-->, car c'est le sanctuaire du roi, et c'est une maison royale.

### Amowc (Amos) répond

7:14	Amowc répondit et dit à Amatsyah : Je n'étais ni prophète ni fils de prophète, j'étais un berger et je recueillais des figues de sycomores.
7:15	YHWH m'a pris derrière le troupeau, et YHWH m'a dit : Va, prophétise à mon peuple d'Israël.
7:16	Écoute maintenant la parole de YHWH : Tu me dis : Ne prophétise plus contre Israël et ne bave<!--« Tomber (en gouttes), dégoutter », « distiller », « prophétiser », « prêcher », « discourir ».--> pas sur la maison de Yitzhak.
7:17	C'est pourquoi ainsi parle YHWH : Ta femme se prostituera dans la ville, tes fils et tes filles tomberont par l'épée, ton sol sera partagé au cordeau, et toi, tu mourras sur un sol souillé, et Israël ira en exil, il ira en exil loin de son sol.

## Chapitre 8

### Vision du panier de fruit, la fin pour le peuple d'Israël

8:1	Ainsi me fit voir Adonaï YHWH, et voici, je vis une corbeille de fruits d'été.
8:2	Il dit : Que vois-tu, Amowc ? Je dis : Une corbeille de fruits d'été. Et YHWH me dit : La fin est venue pour mon peuple d'Israël, je ne passerai plus par-dessus lui.
8:3	Les cantiques du temple seront des hurlements en ce jour-là, – déclaration d'Adonaï YHWH. On jettera partout en silence une multitude de cadavres.
8:4	Écoutez ceci vous qui dévorez le pauvre, même jusqu'à exterminer les misérables de la terre,
8:5	et qui dites : Quand la nouvelle lune sera-t-elle passée, pour que nous vendions le blé, et le shabbat, pour que nous ouvrions les greniers, en faisant l'épha plus petit, en augmentant le sicle, et falsifiant la balance pour tromper ?
8:6	Afin d’acheter les faibles pour de l’argent et le pauvre pour une paire de sandales, et que nous vendions le déchet du blé ?
8:7	YHWH l'a juré par la gloire de Yaacov : Jamais je n'oublierai toutes leurs actions !
8:8	La terre ne tremblera-t-elle pas à cause de cela ? Et tous ses habitants ne se lamenteront-ils pas ? Elle montera tout entière comme un fleuve, elle soulèvera ses eaux et s’affaissera comme le fleuve d'Égypte.
8:9	Il arrivera en ce jour-là, – déclaration d'Adonaï YHWH –, que je ferai coucher le soleil à midi et que j'obscurcirai la terre en plein jour.
8:10	Je changerai vos fêtes en deuil et tous vos chants en un chant funèbre. Je couvrirai de sacs tous les reins, et je rendrai chauves toutes les têtes. Je mettrai la terre dans le deuil comme pour un fils unique, et sa fin sera un jour d'amertume.
8:11	Voici, les jours viennent, – déclaration d'Adonaï YHWH –, où j'enverrai la famine sur la terre, non une famine de pain, ni une soif d'eau, mais d'entendre les paroles de YHWH.
8:12	Ils erreront de la mer à la mer, du nord à l'est, ils iront çà et là pour chercher la parole de YHWH, et ils ne la trouveront pas.
8:13	En ce jour-là, les belles vierges et les jeunes hommes s'évanouiront de soif.
8:14	Ceux qui jurent par le péché de Samarie disent : Dan ! Ton elohîm est vivant, et vive la voie de Beer-Shéba ! Ils tomberont et ne se relèveront plus.

## Chapitre 9

### Prophétie annonçant la destruction<!--De. 28:63-68.-->

9:1	Je vis Adonaï se tenant debout sur l'autel. Il dit : Frappe le chapiteau et que les seuils en tremblent ! Brise-leur la tête à tous ! Et la dernière partie d’eux, je la tuerai par l’épée : il ne s'enfuira pas parmi eux de fuyard, il ne se sauvera pas parmi eux de fugitif.
9:2	S’ils creusent jusqu'au shéol, de là ma main les prendra, s’ils montent jusqu'aux cieux, je les en ferai descendre.
9:3	S’ils se cachent au sommet du Carmel, je les y chercherai et je les enlèverai de là. S’ils se dissimulent de devant mes yeux au fond de la mer, j’ordonnerai au serpent de les y mordre.
9:4	S’ils vont en captivité devant leurs ennemis, de là j'ordonnerai à l'épée, et elle les tuera. Je fixerai mes yeux sur eux pour le mal et non pour le bien.
9:5	Adonaï YHWH Tsevaot touche la Terre et elle se fond. Tous ceux qui l'habitent mènent deuil, et elle monte tout entière comme un fleuve, et elle s'affaisse comme le fleuve d'Égypte.
9:6	C'est lui qui a bâti ses étages dans les cieux et qui a établi sa voûte sur la Terre ; qui appelle les eaux de la mer et qui les répand sur le dessus de la terre. Son Nom est YHWH.
9:7	N'êtes-vous pas pour moi comme les enfants des Éthiopiens, fils d'Israël ? – déclaration de YHWH. N'ai-je pas fait monter Israël de la terre d'Égypte, les Philistins de Kaphtor et les Syriens de Kir ?
9:8	Voici, les yeux d'Adonaï YHWH sont sur ce royaume pécheur. Je le détruirai de dessus les faces du sol. Enfin, je ne détruirai pas, je ne détruirai pas la maison de Yaacov, – déclaration de YHWH.
9:9	Car voici, je donnerai mes ordres et je secouerai la maison d'Israël parmi toutes les nations, comme on secoue dans le crible, sans qu'il en tombe un grain à terre.
9:10	Tous les pécheurs de mon peuple mourront par l'épée, ceux qui disent : Le mal n'approchera pas, il ne nous atteindra pas.

### YHWH relève la maison de David

9:11	En ce jour-là, je relèverai le tabernacle de David qui est tombé, j'en réparerai les brèches, j'en redresserai les ruines, et je le rebâtirai comme aux jours d'autrefois<!--Voir Ac. 15:16.-->,
9:12	afin qu'ils prennent possession du reste d'Édom et de toutes les nations sur lesquelles mon Nom a été invoqué, – déclaration de YHWH –, qui accomplira cela.

### Restauration d'Israël

9:13	Voici, les jours viennent, – déclaration de YHWH –, où le laboureur suivra de près le moissonneur, et celui qui foule les raisins atteindra celui qui répand la semence, et le vin doux ruissellera des montagnes et découlera de toutes les collines.
9:14	Je ferai revenir les captifs de mon peuple Israël, et ils rebâtiront les villes dévastées et les habiteront. Ils planteront des vignes et en boiront le vin, ils feront aussi des jardins et en mangeront les fruits.
9:15	Je les planterai sur leur sol et ils ne seront plus arrachés du sol que je leur ai donné<!--Cette prophétie annonce la restauration de la maison de David. Personne ne chassera Israël de sa terre, aucune nation n'a le pouvoir de le déloger, car c'est le Seigneur qui l'a établi.-->, dit YHWH, ton Elohîm.
