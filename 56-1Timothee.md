# 1 Timotheos (1 Timothée) (1 Ti.)

Signification : Qui adore ou honore Elohîm

Auteur : Paulos (Paul)

Thème : Comment se conduire dans l'assemblée

Date de rédaction : Env. 64 ap. J.-C.

Cette lettre s'adresse à Timotheos (Timothée) dont le père était grec et la mère juive. Le jeune homme se convertit au Mashiah (Christ) avec sa mère et sa grand-mère, dès le premier voyage missionnaire de Paulos (Paul), au cours duquel ce dernier passa à Lystre.

Cette lettre fut rédigée après la première captivité de Paulos à Rome. Alors que les assemblées connaissaient une certaine expansion, Paulos s'adresse à Timotheos, jeune et fidèle compagnon d'œuvre qu'il a lui-même formé, sur des questions d'ordre disciplinaire et sur la pureté de la foi. Dans cette lettre, dite pastorale, Paulos donne des instructions précises à Timotheos pour enseigner, exhorter, diriger le culte public et choisir ses collaborateurs.

## Chapitre 1

### Introduction

1:1	Paulos, apôtre de Yéhoshoua Mashiah selon le mandat<!--Une injonction, un ordre.--> d'Elohîm, notre Sauveur et Seigneur, Yéhoshoua Mashiah notre espérance<!--Je. 50:7 ; Ps. 39:8, 71:5 ; Pr. 3:26.-->,
1:2	à Timotheos mon véritable fils dans la foi : grâce, miséricorde, shalôm, de la part d'Elohîm notre Père et du Mashiah Yéhoshoua notre Seigneur !

### Mise en garde contre les erreurs doctrinales ; le but de la Loi

1:3	Je te prie, comme je l'ai fait en partant pour la Macédoine, de rester à Éphèse, afin d'ordonner à certaines personnes de ne pas enseigner d'autres doctrines
1:4	et de ne pas s'attacher à des fables et à des généalogies sans fin, qui produisent des débats plutôt que l'action de construire en Elohîm par la foi.
1:5	Or la fin du commandement c'est l'amour qui vient d'un cœur pur, d'une bonne conscience et d'une foi sincère,
1:6	desquelles choses quelques-uns s'étant écartés, ils se sont égarés dans une conversation vaine,
1:7	voulant être docteurs de la torah, mais ne comprenant ni ce qu'ils disent ni ce qu'ils affirment fortement.
1:8	Or nous savons que la torah est bonne, si quelqu'un en use légalement,
1:9	sachant ceci, que ce n'est pas pour le juste que la torah est établie, mais pour les violeurs de la torah<!--Ou « impies », « violeurs de la loi ». Voir Mc. 15:28 et 1 Co. 9:21.--> et les réfractaires, pour les impies et les pécheurs, pour les sans religion et les profanes, pour les parricides et les meurtriers,
1:10	pour les fornicateurs, pour les homosexuels, pour les marchands d'esclaves<!--Le mot grec signifie aussi « kidnappeur », « celui qui réduit injustement l'homme libre à l'esclavage », « celui qui vole les esclaves des autres pour les vendre ».-->, pour les menteurs, pour les parjures et pour tout ce qui s'oppose à la saine doctrine,
1:11	selon l'Évangile de la gloire d'Elohîm béni, qui m'a été confié.

### Témoignage de Paulos (Paul)

1:12	Et je rends grâce à celui qui m'a fortifié, à Yéhoshoua Mashiah, notre Seigneur, de ce qu'il m'a jugé fidèle en m'établissant dans le service,
1:13	moi qui auparavant étais un blasphémateur, un persécuteur et un homme insolent. Mais j'ai obtenu miséricorde parce que j'agissais par ignorance, étant dans l'incrédulité.
1:14	Mais la grâce de notre Seigneur a débordé avec la foi et l'amour qui est en Mashiah Yéhoshoua.
1:15	La parole est sûre et entièrement digne d'être reçue, que Yéhoshoua Mashiah est venu dans le monde pour sauver des pécheurs, dont je suis le premier.
1:16	Mais à cause de ceci miséricorde m'a été faite, afin que Yéhoshoua Mashiah fasse voir en moi le premier, toute sa clémence, pour que je serve de modèle à ceux qui ont l'intention de croire en lui pour la vie éternelle.
1:17	Or, au Roi des âges, incorruptible<!--Le mot grec signifie aussi « immortel ».-->, invisible, à Elohîm seul sage, soient honneur et gloire pour les âges des âges ! Amen !

### Recommandations à Timotheos (Timothée)

1:18	Mon fils Timotheos, voici le commandement que je te confie, conformément aux prophéties faites précédemment sur toi, afin que par elles tu combattes le bon combat,
1:19	en ayant la foi et une bonne conscience. Quelques-uns ayant rejeté celle-ci ont fait naufrage quant à la foi.
1:20	De ce nombre sont Hymenaïos et Alexandros que j'ai livrés à Satan afin qu'ils apprennent à ne pas blasphémer.

## Chapitre 2

### Instructions sur la prière

2:1	J'exhorte donc, avant toutes choses, à faire des requêtes, des prières, des supplications et des actions de grâces pour tous les humains,
2:2	pour les rois et pour tous ceux qui sont constitués en dignité, afin que nous menions une vie paisible et tranquille, en toute piété et dignité.
2:3	Car cela est bon et agréable devant Elohîm, notre Sauveur,
2:4	qui veut que tous les humains soient sauvés et qu'ils viennent<!--Jn. 6:44-45 et 65 ; 2 Ti. 3:7.--> à la connaissance précise et correcte de la vérité.

### Yéhoshoua ha Mashiah (Jésus-Christ), le seul médiateur

2:5	Car Elohîm est un<!--Za. 14:9 ; Ga. 3:20 ; Ja. 2:19 ; Mc. 12:29.-->, et le médiateur entre Elohîm et les humains est un, l'humain Mashiah Yéhoshoua,
2:6	qui s'est donné lui-même en rançon<!--Es. 35:10.--> pour tous. C'est le témoignage en ses propres temps,
2:7	pour lequel j'ai été établi prédicateur, apôtre (je dis la vérité en Mashiah, je ne mens pas) et docteur des nations dans la foi et dans la vérité.
2:8	Je veux donc que les hommes prient en tout lieu en élevant des mains pures, sans colère ni raisonnement.

### La tenue de la femme

2:9	De même aussi, que les femmes, dans un vêtement<!--Vient du grec « katastole » et signifie « un vêtement », « une robe », « un costume », « la manière de s'habiller ».--> modeste<!--« Bien arrangé », « bienséant ».-->, avec égard pour les autres<!--« Sens de honte », « d'honneur », « de modestie », « timidité », « révérence », « respect ».--> et auto-contrôle<!--« Solidité de l'esprit », « sobriété ».-->, se parent<!--« Mettre en ordre », « arranger », « rendre prêt », « préparer », « orner ».--> non de tresses et d'or ou de perles ou d'habits très coûteux,
2:10	mais au moyen de bonnes œuvres, ce qui convient à des femmes qui font profession de révérence envers la bonté d'Elohîm.

### Le comportement de la femme envers son mari

2:11	Que la femme apprenne dans le calme, en toute soumission.
2:12	Mais je ne permets pas à la femme d'enseigner ni d'user d'autorité<!--Le mot « autorité » vient du grec « authenteo » et signifie « celui qui tue de ses propres mains un autre ou lui-même ; celui qui agit de sa propre autorité, autocrate ; un maître absolu ; gouverneur, exercer une domination ».--> sur le mari<!--Dans plusieurs traductions, ce verset est traduit par : « Je ne permets pas à la femme d'enseigner, ni de prendre de l'autorité sur l'homme ». Le terme grec qui a été traduit par « homme » est aner : « fiancé, époux, mari ». Il ne désigne pas tous les mâles, mais il se rapporte exclusivement au mari, à l'époux. Voir Mt. 1:16,19 ; 1 Co. 14:35 ; 2 Co. 11:2 ; 1 Ti. 3:2,12, 5:9 ; Tit. 1:6 ; Ap. 21:2.-->, mais qu'elle soit dans le calme<!--Le mot grec traduit par « calme » est « hesuchia » qui signifie aussi « paisiblement », « silence ». La racine de ce terme est « hesuchios » : « tranquille, paisible ».-->.
2:13	Car Adam a été modelé le premier, Chavvah<!--Ève.--> ensuite.
2:14	Et Adam n'a pas été séduit, mais la femme, ayant été séduite, a été la cause de la transgression.
2:15	Elle sera néanmoins sauvée par le moyen de la maternité<!--Ou par l'enfant qu'elle portera. Il est évident que le salut ne dépend pas du fait d'enfanter puisque nous sommes sauvés par grâce et non par les œuvres. Ce verset fait référence à Chavvah (Ève), la mère de tous les vivants. Par elle, le péché et la mort sont entrés dans le monde (Ro. 5:12) mais c'est aussi par sa postérité, à savoir le Mashiah (Ge. 3:15), que le salut a été apporté.-->, pourvu qu'elle persévère dans la foi, dans l'amour, et dans la sanctification, avec modestie.

## Chapitre 3

### Les surveillants et les diacres doivent manifester le caractère du Mashiah (Christ)

3:1	La parole est sûre, si quelqu'un désire la fonction de surveillant<!--Vient du grec « episkope » qui signifie « investigation, inspection, visite d'inspection ». C'est un acte par lequel Elohîm visite les êtres humains, observe leurs voies, leurs caractères, pour leur accorder en partage joie ou tristesse. Ce terme signifie également « surveillance, charge, contrôle, fonction, la fonction d'un ancien ». Voir Ac. 1:20.-->, il désire une œuvre excellente.
3:2	Mais il faut que le surveillant soit irréprochable, mari<!--Paulos (Paul) ne dit pas que les surveillants ne peuvent pas être célibataires. Il y a en effet une différence entre mari et marié. L'apôtre met l'accent sur la monogamie. Un homme célibataire peut en effet être surveillant s'il remplit les caractéristiques décrites dans ce passage.--> d'une seule femme, fléchissant ses désirs et impulsions, modéré, décent, hospitalier, capable d'enseigner.
3:3	Qu'il ne soit pas un ivrogne, ni violent, mais doux, non agressif, non attiré par l'argent,
3:4	dirigeant convenablement sa propre maison, tenant ses enfants dans la soumission, en toute dignité.
3:5	Mais si quelqu'un ne sait pas diriger sa propre maison, comment prendra-t-il soin de l'assemblée d'Elohîm ?
3:6	Que ce ne soit pas un nouveau converti<!--Vient du grec « neophutos » traduit en français par « néophyte ». Néophyte vient de « néo » : « nouveau », et de « phyton » : « plante ». Nouvellement planté.-->, de peur qu'enflé d'orgueil, il ne tombe sous le jugement du diable.
3:7	Mais il lui faut aussi avoir un bon témoignage de ceux du dehors, de peur qu'il ne tombe dans l'opprobre et dans les pièges du diable.
3:8	De même, que les diacres soient honorables, n'étant ni doubles en paroles<!--Double langage, double en parole, disant une chose à une personne et autre chose à une autre avec l'intention de tromper.-->, ni adonnés à beaucoup de vin, ni ardents aux gains,
3:9	ayant le mystère de la foi dans une conscience pure.
3:10	Mais qu'eux aussi soient d'abord mis à l'épreuve<!--« Éprouver », « examiner », « reconnaître comme véritable après examen », « juger digne ». Voir 1 Co. 16:3 ; 2 Co. 8:8,22 ; 1 Th. 2:4, 5:21 ; 1 Jn. 4:1.-->, et qu'ensuite ils servent en étant irréprochables.
3:11	Les femmes, de même, doivent être honorables, non calomniatrices<!--Vient du grec « diabolos » qui signifie « diable ». Voir Mt. 4:1.-->, sobres, fidèles en toutes choses.
3:12	Les diacres doivent être maris d'une seule femme, dirigeant honnêtement leurs enfants et leurs propres maisons.
3:13	Car ceux qui ont bien servi s'acquièrent un rang honorable et une grande liberté dans la foi qui est en Mashiah Yéhoshoua.
3:14	Je t'écris ces choses espérant que j'irai plus vite chez toi,
3:15	mais, si je tarde, tu sauras comment il faut se conduire dans la maison d'Elohîm, qui est l'assemblée de l'Elohîm vivant, la colonne et l'appui de la vérité.

### Le mystère de la piété

3:16	Et sans contredit, le mystère de la piété<!--Il s'agit de la connaissance d'Elohîm manifestée en chair dans la personne de Yéhoshoua ha Mashiah, 100% humain et 100% Elohîm. C'est l'incarnation de l'Elohîm Tout-Puissant dans le seul but de sauver les humains et de produire dans leur cœur la véritable piété.--> est grand : Elohîm a été manifesté en chair, justifié par l'Esprit, vu des anges, prêché dans les nations, cru dans le monde et élevé dans la gloire.

## Chapitre 4

### L'apostasie et la séduction : signes des derniers temps

4:1	Mais l'Esprit dit expressément que dans les derniers temps, certains s'éloigneront de la foi, s'attachant à des esprits trompeurs et à des doctrines de démons,
4:2	par l'hypocrisie de faux docteurs, ayant leur propre conscience marquée au fer,
4:3	empêchant de se marier, ordonnant de s'abstenir d'aliments qu'Elohîm a créés pour être pris avec action de grâce par les fidèles et par ceux qui ont connu précisément la vérité.
4:4	Parce que tout ce qu'Elohîm a créé est bon, et l'on ne doit rien rejeter de ce qui se prend avec action de grâce,
4:5	car cela est sanctifié au moyen de la parole d'Elohîm et de la prière.
4:6	En exposant ces choses aux frères, tu seras un bon serviteur de Yéhoshoua Mashiah, nourri des paroles de la foi et de la bonne doctrine que tu as exactement suivie.
4:7	Mais refuse les fables<!--Voir 1 Ti. 1:4 ; 2 Ti. 4:4 ; Tit. 1:14 ; 2 Pi. 1:16.--> profanes et absurdes comme les contes de vieilles femmes. Exerce-toi à la piété. 

### S'exercer à la piété

4:8	Car l'exercice corporel est utile à peu de chose, mais la piété est utile à toutes choses, ayant la promesse de la vie présente et de celle qui est à venir.
4:9	C'est là une parole certaine et digne d'être entièrement reçue.
4:10	Car c'est pour cela que nous travaillons dur et que nous sommes insultés, parce que nous espérons dans l'Elohîm vivant, qui est le Sauveur<!--Vient du grec « soter ». Ce nom était donné par les anciens aux déités, spécialement les déités de tutelle, aux princes, aux rois et en général aux hommes qui avaient apportés de grands bienfaits à leur pays ; ce mot fut ensuite employé, en des occasions moins nobles, pour la flatterie de personnages influents. Voir Lu. 1:47, 2:11 ; Jn. 4:42 ; Ac. 5:31, 13:23 ; Ep. 5:23 ; 1 Jn. 4:14.--> de tous les humains, mais principalement des fidèles.
4:11	Déclare ces choses et enseigne-les.
4:12	Que personne ne méprise ta jeunesse, mais deviens un exemple pour les fidèles, en parole, en conduite, en amour, en esprit, en foi, en pureté.
4:13	Jusqu'à ce que je vienne, applique-toi à la lecture, à l'exhortation, à l'enseignement.
4:14	Ne néglige pas le don qui est en toi et qui t'a été donné au moyen d'une prophétie, avec l'imposition des mains du corps des anciens.
4:15	Pratique ces choses et donne-toi tout entier à elles, afin que tes progrès soient évidents pour tous.
4:16	Veille sur toi-même et sur la doctrine. Persévère dans ces choses, car en faisant cela tu te sauveras toi-même et ceux qui t'écoutent.

## Chapitre 5

### Recommandations concernant les veuves

5:1	Ne gronde pas un ancien, mais exhorte-le comme un père, les jeunes gens comme des frères,
5:2	les anciennes comme des mères, les jeunes comme des sœurs, en toute pureté.
5:3	Honore les veuves qui sont véritablement veuves.
5:4	Mais si une veuve a des enfants ou des petits enfants, qu'ils apprennent avant tout à exercer la piété envers leur propre famille et à rendre à leurs parents ce qu'ils ont reçu d'eux, car cela est bon et agréable à Elohîm.
5:5	Mais celle qui est vraiment veuve et abandonnée, a mis son espérance en Elohîm et persévère nuit et jour dans les supplications et les prières.
5:6	Mais celle qui vit dans un grand luxe est morte en vivant.
5:7	Ordonne aussi ces choses, afin qu'elles soient irréprochables.
5:8	Mais si quelqu'un ne prend pas soin des siens, et principalement de ceux de sa famille, il a renié la foi et il est pire qu'un incrédule.
5:9	Qu'on inscrive<!--Expression qui s'apparente à l'enrôlement des soldats. Il est question des veuves ayant une certaine responsabilité sur le reste des femmes, des veuves et des orphelins.--> une veuve qui n'ait pas moins de 60 ans, femme d'un seul mari,
5:10	ayant le témoignage de ses bonnes œuvres : si elle a bien élevé ses enfants, si elle a exercé l'hospitalité, si elle a lavé les pieds des saints, si elle a porté assistance aux oppressés, si elle a accompagné toutes sortes de bonnes œuvres.
5:11	Mais refuse les veuves qui sont plus jeunes, car dès qu'elles ressentent les pulsions du désir sexuel qui les dressent contre le Mashiah, elles veulent se marier,
5:12	étant sous un jugement parce qu'elles ont annulé leur première foi.
5:13	Et en même temps, étant paresseuses, elles apprennent à parcourir les maisons. Et non seulement elles sont paresseuses, mais encore bavardes et s'occupent des affaires des gens, parlant de choses qui ne sont ni justes ni correctes.
5:14	Je veux donc que les jeunes se marient, qu'elles aient des enfants, qu'elles dirigent leur maison et qu'elles ne donnent à l'adversaire aucune occasion d'insulte.
5:15	Car quelques-unes se sont déjà détournées pour suivre Satan.
5:16	Si un fidèle ou une fidèle a des veuves, qu’il les assiste et que l'assemblée n'en soit pas chargée, afin qu'elle assiste celles qui sont vraiment veuves.

### Recommandations concernant les anciens

5:17	Que les anciens qui dirigent<!--Du grec « proistemi » : « disposer », « placer devant », « présider » (1 Th. 5:12 ; Ro. 12:8 ; 1 Ti. 3:4-12).--> convenablement soient jugés dignes d'un double honneur, spécialement ceux qui travaillent à la prédication et à l'enseignement.
5:18	Car l'Écriture dit : Tu ne muselleras pas le bœuf qui foule le grain<!--De. 25:4.-->. Et l'ouvrier est digne de son salaire<!--Lu. 10:7.-->.
5:19	Ne reçois pas d'accusation contre un ancien, excepté sur la déposition de deux ou de trois témoins<!--De. 19:15 ; Mt. 18:16 ; 2 Co. 13:1.-->.
5:20	Reprends, en présence de tous, ceux qui pèchent, afin que les autres aussi en aient de la crainte.
5:21	Je témoigne devant l'Elohîm et Seigneur, Yéhoshoua Mashiah, et les anges élus, que tu gardes ces choses sans préjugé, ne faisant rien avec partialité.
5:22	N'impose les mains à personne avec précipitation<!--Vient du grec « tacheos » qui signifie « rapidement », « au plus tôt ». Voir Lu. 14:21 et 16:6.-->, et ne participe pas aux péchés d'autrui. Toi-même, garde-toi pur.
5:23	Ne bois plus de l’eau seulement, mais use d'un peu de vin à cause de ton estomac et de tes fréquentes maladies.
5:24	Les péchés de certains humains sont connus de tous et les précèdent dans le jugement, mais ceux d’autres les suivent après.
5:25	De même aussi, les bonnes œuvres sont connues de tous, et celles qui sont autrement ne peuvent être cachées<!--Mt. 10:26 ; Mc. 4:22 ; Lu. 8:17, 12:2.-->.

## Chapitre 6

### L'attitude du serviteur envers son maître

6:1	Que tous ceux qui sont sous le joug de l'esclavage estiment leur maître digne de tout honneur, afin que le Nom d'Elohîm et la doctrine ne soient pas blasphémés.
6:2	Et que ceux qui ont des maîtres fidèles ne les méprisent pas parce que ce sont des frères. Au contraire, qu’ils soient d’autant plus volontiers des esclaves, parce qu’ils sont fidèles et bien-aimés, ayant leur part du même bienfait. Enseigne ces choses et exhorte.
6:3	Si quelqu'un enseigne une autre doctrine<!--Voir 1 Ti. 1:3.--> et ne vient pas aux saines paroles de notre Seigneur Yéhoshoua Mashiah, et à la doctrine qui est selon la piété,
6:4	il est enflé d'orgueil, il ne sait rien, mais c'est un malade qui s'occupe de débats et de querelles au sujet de choses vides et insignifiantes, d'où naissent l'envie, les querelles, les médisances et les mauvais soupçons,
6:5	les vaines disputes de gens à la pensée corrompue et privés<!--« Frauder », « voler », « spolier ».--> de la vérité, qui pensent que la piété est une source de gain. Sépare-toi de ces sortes de gens.

### L'amour de l'argent : la racine de tous les maux

6:6	Mais la piété est une grande source de gain quand on sait être content avec ce que l'on a.
6:7	Car nous n'avons rien apporté dans le monde, et il est évident que nous ne pouvons rien en emporter.
6:8	Si nous avons la nourriture et le vêtement, cela nous suffira.
6:9	Mais ceux qui veulent devenir riches<!--Pr. 22:4.--> tombent dans la tentation<!--La tentation se rapporte à l'envie de toujours posséder, de s'enrichir et de gagner plus d'argent. Elle pousse notamment l'homme à l'orgueil, au mensonge, à la duplicité, la fornication, l'adultère. En effet, une personne cupide, qui a l'envie de toujours posséder, finit en général par tromper son conjoint.-->, dans le piège<!--Le mot « piège » vient du grec « pagis » qui donne aussi en français « trappe », « filet ». « Car il surprendra comme un piège tous ceux qui habitent sur la surface de toute la Terre. » Lu. 21:35. Ce mot suggère l'inattendu, l'improviste, la surprise, car les oiseaux et autres animaux pris dans le filet sont attrapés par surprise. Les conséquences de la cupidité sont nombreuses, notamment le mensonge et l'adultère.--> et dans beaucoup de désirs insensés<!--« Inintelligents ». Lu. 24:25 ; Ga. 3:1.--> et pernicieux<!--Les désirs dénués d'intelligence et pernicieux sont multiples : l'envie de toujours posséder plus que les autres, la convoitise, les rivalités, la concurrence, la folie des grandeurs. Ces choses sortent les gens de la vision du Seigneur (Mc. 4:19).-->, qui plongent les humains dans la destruction et la perdition<!--Une personne cupide se perd en s'éloignant du Seigneur (2 Pi. 2). Selon Shelomoh (Salomon), « celui qui aime l'argent n'est pas rassasié par l'argent » (Ec. 5:9). L'enfant d'Elohîm ne doit pas être cupide et s'appuyer sur l'argent, car le système bancaire mondial s'écroulera dans les prochaines années (Ap. 18).-->.
6:10	Car l'amour de l'argent est la racine de tous les maux<!--L'amour de l'argent est la racine de tous les maux. Ceux qui espèrent en une sécurité divine doivent renoncer à la sécurité matérielle et financière que la chair désire.-->. Pour l'avoir désiré, certains se sont détournés de la foi et se sont transpercés eux-mêmes de beaucoup de peines.
6:11	Mais toi, ô homme d'Elohîm, fuis ces choses et recherche la justice, la piété, la foi, l'amour, la persévérance, la douceur.
6:12	Combats le bon combat de la foi, saisis la vie éternelle, à laquelle aussi tu as été appelé et pour laquelle tu as fait une belle profession en présence de beaucoup de témoins.
6:13	Je t'ordonne devant l'Elohîm qui donne la vie à toutes choses, et le Mashiah Yéhoshoua qui, témoignant devant Ponce Pilate, fit cette belle profession,
6:14	de garder ce commandement, sans tache et irréprochable, jusqu'à l'apparition de notre Seigneur Yéhoshoua Mashiah,
6:15	que montrera en ses propres temps le Béni et seul Prince, le Roi de ceux qui règnent et le Seigneur de ceux qui dominent en seigneurs<!--Lu. 22:25.-->,
6:16	le seul qui possède l'immortalité, qui habite une lumière inaccessible, lequel aucun des humains n'a vu ni ne peut voir. À lui, honneur et force souveraine éternelle ! Amen.
6:17	Ordonne aux riches de l'âge présent de ne pas s'enorgueillir et de ne pas mettre leur espérance dans la richesse incertaine, mais dans l'Elohîm vivant, qui nous donne toutes choses abondamment pour en jouir.
6:18	De faire du bien, d'être riches en bonnes œuvres, d’être généreux, prêts à partager,
6:19	qu'ils emmagasinent comme trésor un bon fondement pour l'avenir, afin qu'ils saisissent la vie éternelle.

### Conclusion

6:20	Ô Timotheos, garde le dépôt, en fuyant les discussions sur des sujets vains, inutiles et profanes, et les oppositions de la connaissance faussement nommée.
6:21	Pour l'avoir professée, certains se sont écartés de la foi. Que la grâce soit avec toi ! Amen !
