# Meguila Esther (Esther) (Est.)

Signification : Rouleau d'Esther

Auteur : Inconnu

Thème : Délivrance des Juifs de l'extermination

Date de rédaction : 5ème siècle av. J.-C.

Esther vient du persan « Ecter » et signifie « étoile ». Son nom hébreu était Hadassah (Est. 2:7) qui signifie « myrte ».

Dernier livre à caractère historique du Tanakh, l'histoire d'Esther se déroula à Suse, capitale du royaume de Perse. En ce temps, le peuple d'Israël était dispersé et le roi Assuérus régnait sur un large territoire allant de l'Inde à l'Éthiopie.

Ce livre raconte la vie d'Esther, son ascension au trône royal où elle succéda à la reine Vasthi et la manière dont elle fut utilisée pour éviter le génocide du peuple juif.

Bien que ne comportant pas le Nom d'Elohîm ni d'allusion à une œuvre spirituelle, hormis le jeûne, ce récit met en évidence le secours divin.

## Chapitre 1

### Un festin de 7 jours au palais de Suse

1:1	Il arriva au temps d'Assuérus, de cet Assuérus qui régnait depuis l'Inde jusqu'en Éthiopie sur 127 provinces ;
1:2	il arriva en ce temps-là, que le roi Assuérus était assis sur le trône royal à Suse, dans la capitale.
1:3	La troisième année de son règne, il fit un festin à tous les principaux princes : à ses serviteurs, à l'armée des Perses et des Mèdes, aux nobles et aux chefs des provinces qui furent réunis en face de lui.
1:4	Il montra la glorieuse richesse de son royaume et la splendeur de sa grande magnificence pendant de nombreux jours, pendant 180 jours.
1:5	Quand ces jours furent achevés, le roi fit un festin pendant 7 jours, dans la cour du jardin du palais royal, pour tout le peuple qui se trouvait à Suse la capitale, du plus grand au plus petit.
1:6	Des étoffes blanches, du coton fin et des étoffes violettes, étaient attachés par des cordons de byssus et de pourpre rouge à des anneaux d'argent et à des colonnes de marbre. Les lits étaient d'or et d'argent sur un pavé de porphyre, de marbre blanc, de perle et de marbre noir.
1:7	On donnait à boire dans des vases d'or, des vases, des vases divers, et il y avait du vin royal en abondance, selon la force du roi.
1:8	Selon le décret, on ne forçait personne à boire, car le roi avait ordonné à tous les grands de sa maison d'agir selon la volonté de chacun.
1:9	La reine Vasthi fit aussi un festin aux femmes dans la maison royale du roi Assuérus.

### Destitution de la reine Vasthi

1:10	Or le septième jour, comme le cœur du roi était réjoui par le vin, il ordonna à Mehoumân, Biztha, Harbona, Bigtha, Abagtha, Zéthar et Carcas, les sept eunuques qui servaient en face du roi Assuérus,
1:11	de faire venir en face du roi la reine Vasthi, avec la couronne royale, pour faire voir sa beauté aux peuples et aux princes, car elle était belle de figure.
1:12	Mais la reine Vasthi refusa de venir selon la parole du roi par la main des eunuques. Le roi en fut très fâché et sa colère s'enflamma au-dedans de lui.
1:13	Le roi dit aux sages qui avaient la connaissance des temps : (car c'est ainsi que le roi traitait les affaires en face de tous ceux qui connaissaient les décrets et le jugement,
1:14	et les plus proches de lui étaient Karshena, Shéthar, Admatha, Tarsis, Mérès, Marsena, Memoukân, sept princes de Perse et de Médie, qui voyaient les faces du roi et qui occupaient le premier rang dans le royaume) :
1:15	D’après les décrets, que faire à la reine Vasthi pour n’avoir pas exécuté l’ordre du roi Assuérus par la main des eunuques ?
1:16	Memoukân dit en face du roi et des princes : La reine Vasthi n'a pas seulement mal agi contre le roi, mais aussi contre tous les princes et tous les peuples qui sont dans toutes les provinces du roi Assuérus.
1:17	Car cette affaire de la reine sortira vers toutes les femmes, aux yeux desquelles leurs maris seront méprisés. Elles diront : Le roi Assuérus avait ordonné qu'on lui amène la reine et elle n'est pas venue.
1:18	Et dès ce jour, les princesses de Perse et de Médie qui ont entendu parler de l'action de la reine, en parleront à tous les princes du roi et il y aura assez de mépris et de colère !
1:19	Si le roi le trouve bon, qu'on publie de sa part et qu'on écrive parmi les décrets des Perses et des Mèdes, avec défense de la transgresser, une parole royale d'après laquelle Vasthi n'entrera plus en face du roi Assuérus et le roi donnera sa royauté à l'une de ses compagnes, meilleure qu'elle.
1:20	L'édit du roi sera présenté et connu dans tout son royaume, car il est grand, et toutes les femmes rendront honneur à leurs maris<!--Respect ou soumission de la femme à l'égard de son mari : Ep. 5:22 ; Col. 3:18 ; 1 Pi. 3:1-5.-->, depuis le plus grand jusqu'au plus petit.
1:21	Cette parole fut bonne aux yeux du roi et des princes, et le roi agit selon la parole de Memoukân.
1:22	Il envoya des lettres à toutes les provinces du roi, à chaque province selon sa manière d'écrire, et à chaque peuple selon sa langue, afin que chaque homme soit maître dans sa propre maison<!--L'homme est le chef de la femme et le maître de la maison : 1 Co. 11:3 ; Ep. 5:23.-->, et que cela soit publié selon la langue de chaque peuple.

## Chapitre 2

### Le roi choisit une autre reine

2:1	Après ces choses, quand la colère du roi Assuérus fut calmée, il se souvint de Vasthi, de ce qu'elle avait fait, et de ce qui avait été décrété contre elle.
2:2	Et les jeunes hommes qui servaient le roi dirent : Qu'on cherche pour le roi des jeunes filles, vierges et belles de figure.
2:3	Que le roi établisse des commissaires dans toutes les provinces de son royaume chargés de rassembler toutes les jeunes filles vierges et belles de figure, dans Suse, la capitale, dans la maison des femmes sous la main d'Hégaï, eunuque du roi et gardien des femmes, qu'on leur donne les onguents,
2:4	et la jeune fille qui plaira aux yeux du roi régnera à la place de Vasthi. Ce discours plut aux yeux du roi et il fit ainsi.
2:5	Il y avait à Suse, la capitale, un homme juif nommé Mordekay<!--Mardochée.-->, fils de Yaïr, fils de Shimeï, fils de Kis, Benyamite,
2:6	qui avait été exilé de Yeroushalaim parmi les déportés emmenés en exil avec Yekonyah, le roi de Yéhouda, que Neboukadnetsar, le roi de Babel<!--Babylone.--> avait emmené en exil.
2:7	Il élevait Hadassah qui est Esther, fille de son oncle, car elle n'avait ni père ni mère. La jeune fille était belle de taille et très belle de figure. Après la mort de son père et de sa mère, Mordekay l'avait prise pour sa fille.
2:8	Et il arriva que, lorsque la parole du roi et son décret furent connus et que beaucoup de jeunes filles furent rassemblées à Suse, la capitale, sous la main d'Hégaï, Esther fut aussi amenée dans la maison du roi, sous la main d'Hégaï, gardien des femmes.
2:9	La jeune fille plut à ses yeux et porta de la bonté en face de lui. Il s'empressa de lui donner ses onguents et ses portions. Il lui donna aussi les sept filles les plus remarquables de la maison du roi, puis il la plaça avec ses jeunes filles dans l'un des plus beaux appartements de la maison des femmes.
2:10	Esther n'avait fait connaître ni son peuple ni sa parenté, car Mordekay lui avait ordonné de ne rien raconter.
2:11	Tous les jours, Mordekay allait et venait devant la cour de la maison des femmes, pour savoir si Esther était en paix et ce qu’on faisait à son égard.
2:12	Le tour de chacune des jeunes filles d'aller chez le roi Assuérus arrivait à la fin de 12 mois qui lui étaient assignés suivant le décret des femmes<!--Esther se soumit à une toilette particulière avant de rencontrer le roi. Le mot « toilette » vient de l'hébreu « tam-rook », qui signifie « grattement ». La racine de ce mot signifie « nettoyer », « purifier », « polir » (voir Lé. 6:21 ; Jé. 46:4). Ce grattage symbolise le dépouillement du vieil homme et le renoncement aux œuvres de la chair (Ep. 4:22). Douze mois étaient nécessaires pour préparer Esther aux noces : 6 mois avec de l'huile de myrrhe et 6 mois avec des aromates et des parfums. La myrrhe était l'une des composantes de l'huile pour l'onction sainte dont on s'est servi pour oindre notamment la tente d'assignation, l'arche du témoignage ainsi qu'Aaron et ses fils (Ex. 30:23-30). La toilette d'Esther symbolise la sanctification nécessaire à notre préparation pour les noces (Hé. 12:14). La myrrhe est par ailleurs citée à sept reprises dans le livre du Cantique des cantiques, véritable hymne de l'amour parfait qui lie le Mashiah (Christ) à son Assemblée (Église). Le parfum quant à lui symbolise les prières que nous devons faire en tout temps afin de maintenir notre communion avec Yéhoshoua (Jésus), notre Époux (Ep. 6:18 ; 1 Th. 5:17 ; Ap. 5:8, 8:4). Ainsi, à l'instar d'Esther qui se préparait à rencontrer le roi, l'Assemblée se prépare depuis plus de 2 000 ans pour les noces de l'Agneau (Ap. 19:7-9).-->, car les jours de leurs toilettes du corps étaient ainsi remplis : 6 mois avec de l'huile de myrrhe, et 6 mois avec des aromates et des onguents des femmes.
2:13	Lorsque la jeune fille allait chez le roi, tout ce qu'elle demandait lui était donné pour aller avec elle de la maison des femmes à la maison du roi.
2:14	Elle y allait le soir, et le matin, elle retournait dans la seconde maison des femmes sous la main de Shaashgaz, eunuque du roi et gardien des concubines. Elle ne retournait plus vers le roi, à moins que le roi n'en ait le désir et qu'elle ne soit appelée par son nom.

### Esther, reine de Suse

2:15	Quand son tour d'aller vers le roi fut arrivé, Esther, fille d'Abichaïl, oncle de Mordekay qui l'avait prise pour sa fille, ne demanda rien sinon ce qui fut ordonné par Hégaï, eunuque du roi et gardien des femmes. Esther portait grâce aux yeux de tous ceux qui la voyaient.
2:16	Esther fut prise pour le roi Assuérus, dans sa maison royale, le dixième mois, qui est le mois de Tébeth, la septième année de son règne.
2:17	Le roi aima Esther plus que toutes les femmes, et elle porta grâce et bonté en face de lui plus que toutes les vierges. Il mit la couronne royale sur sa tête et l'établit reine à la place de Vasthi.
2:18	Le roi fit un grand festin à tous les princes et à ses serviteurs, un festin en l'honneur d'Esther. Il donna du repos aux provinces et fit des présents selon le pouvoir du roi.
2:19	Or pendant qu'on rassemblait les vierges pour la seconde fois, Mordekay s'assit à la porte du roi.
2:20	Esther n'avait fait connaître ni sa parenté ni son peuple, selon l’ordre de Mordekay, car elle exécutait ce que lui disait Mordekay comme quand elle était élevée chez lui.

### Le roi sauvé par Mordekay

2:21	En ces jours-là, Mordekay s'étant assis à la porte du roi, Bigthan et Théresh, deux eunuques du roi, gardes du seuil, se fâchèrent et cherchèrent à mettre la main sur le roi Assuérus.
2:22	L'affaire fut connue de Mordekay qui informa la reine Esther, et Esther le dit au roi au nom de Mordekay.
2:23	L’affaire fut recherchée et découverte, et tous les deux furent pendus à un bois. Et cela fut écrit dans le livre des discours du jour devant le roi<!--Voir Mal. 3:16.-->.

## Chapitre 3

### Conspiration d'Haman contre les Juifs

3:1	Après ces choses, le roi Assuérus fit de grands honneurs à Haman, fils d'Hammedatha, l'Agaguite. Il l'éleva et plaça son trône au-dessus de tous les princes qui étaient auprès de lui.
3:2	Tous les serviteurs du roi qui étaient à la porte du roi s'inclinaient et se prosternaient devant Haman, car le roi l'avait ainsi ordonné. Mais Mordekay ne s'inclinait pas et ne se prosternait pas.
3:3	Les serviteurs du roi, qui étaient à la porte du roi disaient à Mordekay : Pourquoi transgresses-tu l'ordre du roi ?
3:4	Et il arriva que, comme ils lui parlaient jour après jour et qu'il ne les écoutait pas, ils informèrent Haman, pour voir si les paroles de Mordekay seraient fermes, parce qu'il leur avait déclaré qu'il était Juif.
3:5	Haman vit que Mordekay ne s'agenouillait pas et ne se prosternait pas devant lui et il fut rempli de colère.
3:6	Mais c'était chose méprisable à ses yeux de porter la main sur Mordekay seul, car on lui avait rapporté de quel peuple était Mordekay. Haman chercha à exterminer tous les Juifs, le peuple de Mordekay qui se trouvait dans tout le royaume d'Assuérus.
3:7	Au premier mois, c’est-à-dire le mois de Nisan, la douzième année du roi Assuérus, on jeta le Pour<!--Pour ou Pourim : Ce terme signifie « sort ». Voir commentaire de Est. 9:26.-->, c'est-à-dire le sort, devant Haman, jour après jour et mois après mois jusqu'au douzième mois, c’est-à-dire le mois d'Adar.
3:8	Haman dit au roi Assuérus : Il y a un peuple, dispersé et séparé entre les peuples dans toutes les provinces de ton royaume. Leurs décrets sont différents de ceux de tous les peuples. Il n'observe pas les décrets du roi. Il ne convient pas au roi de les laisser en repos.
3:9	Si le roi le trouve bon, qu'on écrive pour les faire périr et je pèserai 10 000 talents d'argent entre les mains de ceux qui feront le travail pour les faire rentrer au trésor du roi.
3:10	Le roi ôta son anneau de sa main et le donna à Haman fils de Hammedatha, l'Agaguite, l'adversaire des Juifs.
3:11	Le roi dit à Haman : L’argent t’est donné, le peuple aussi, pour en faire comme il sera bon à tes yeux.
3:12	Le treizième jour du premier mois, les scribes du roi furent appelés, et on écrivit selon l'ordre d'Haman, aux satrapes du roi, aux gouverneurs de chaque province et aux princes de chaque peuple, à chaque province selon son écriture et à chaque peuple selon sa langue. Ce fut au nom du roi Assuérus que l'on écrivit et on scella avec l'anneau du roi.
3:13	Les lettres furent envoyées en main des coureurs dans toutes les provinces du roi, afin qu'on extermine, qu'on tue et qu'on fasse périr tous les Juifs, jeunes et vieux, petits enfants et femmes, en un seul jour, le treizième du douzième mois, qui est le mois d'Adar, et pour que leurs biens soient livrés au pillage.
3:14	Ces lettres qui furent écrites portaient une copie du décret qui devait être publié dans chaque province, et invitaient publiquement tous les peuples à se tenir prêts pour ce jour-là.
3:15	Les coureurs partirent, pressés par la parole du roi. Le décret fut aussi publié dans Suse, la capitale. Or le roi et Haman étaient assis pour boire, pendant que la ville de Suse était dans la confusion.

## Chapitre 4

### Esther avertie du complot d'Haman

4:1	Mordekay apprit tout ce qui avait été fait. Mordekay déchira ses vêtements et se couvrit d'un sac et de cendre. Puis il alla au milieu de la ville en poussant avec force des cris amers,
4:2	et se rendit jusqu'à la porte du roi car il était interdit d'entrer dans le palais du roi revêtu d'un sac.
4:3	Dans chaque province, dans les lieux où la parole du roi et son décret parvinrent, il y eut une grande désolation parmi les Juifs : ils jeûnaient, pleuraient, poussaient des gémissements et beaucoup se couchaient sur le sac et la cendre.
4:4	Les servantes d'Esther et ses eunuques vinrent lui raconter ces choses et la reine fut très peinée. Elle envoya des vêtements à Mordekay pour le couvrir et lui faire ôter son sac, mais il ne les prit pas.
4:5	Esther appela Hathac, l'un des eunuques que le roi avait établi pour la servir et elle le chargea de demander à Mordekay ce qui s'était passé et pourquoi il agissait ainsi.
4:6	Hathac sortit vers Mordekay sur la place de la ville, devant la porte du roi.
4:7	Mordekay lui raconta tout ce qui lui était arrivé, ainsi que le détail de l'argent que Haman avait promis de peser dans le trésor du roi pour qu'on fasse périr les Juifs.
4:8	Il lui donna une copie de l’écrit du décret qui avait été donné à Suse pour les détruire, afin de le faire voir à Esther, l’informer et lui ordonner de venir vers le roi demander grâce et faire une requête en face de lui, pour son peuple.
4:9	Ainsi Hathac vint et rapporta à Esther les paroles de Mordekay.

### Esther prête à risquer sa vie pour ses frères, elle demande un jeûne

4:10	Esther dit à Hathac et lui ordonna de dire à Mordekay :
4:11	Tous les serviteurs du roi, ainsi que le peuple des provinces du roi, savent que pour tout homme ou femme, qui entre chez le roi dans la cour intérieure, sans avoir été appelé, il n’y a qu’un seul décret : la mort. Seul celui à qui le roi tend son sceptre d’or vit. Et moi, je n’ai pas été appelée à entrer chez le roi voilà 30 jours.
4:12	On rapporta les paroles d'Esther à Mordekay.
4:13	Mordekay dit de répondre à Esther : N’imagine pas en ton âme que la maison du roi échappera, plutôt que tous les Juifs.
4:14	Mais si tu te tais, si tu te tais en ce temps-ci, le secours et la délivrance se lèveront d’un autre lieu pour les Juifs, mais toi et la maison de ton père, vous périrez. Et qui sait si tu n'es pas arrivée à la royauté pour un temps comme celui-ci ?
4:15	Esther dit de répondre à Mordekay :
4:16	Va, rassemble tous les Juifs qui se trouvent à Suse et jeûnez pour moi, sans manger ni boire pendant trois jours, ni la nuit ni le jour. Moi aussi et mes servantes nous jeûnerons de même, puis j'entrerai chez le roi, bien que ce soit contre le décret et, si je dois périr, je périrai !
4:17	Mordekay s'en alla et fit comme Esther lui avait ordonné.

## Chapitre 5

### Requête d'Esther auprès du roi

5:1	Quand le troisième jour fut arrivé, Esther s’habilla royalement et se tint debout dans la cour intérieure de la maison du roi, en face de la maison du roi. Le roi était assis sur le trône de son royaume dans le palais royal, vis-à-vis de la porte du palais.
5:2	Et il arriva, dès que le roi vit Esther la reine se tenant debout dans la cour, qu'elle porta grâce à ses yeux. Et le roi tendit à Esther le sceptre d'or qu'il avait en main. Esther s'approcha et toucha l'extrémité du sceptre.
5:3	Le roi lui dit : Qu'as-tu, reine Esther, et que demandes-tu ? Quand ce serait la moitié du royaume, elle te serait donnée.
5:4	Esther dit : Si le roi le trouve bon, que le roi vienne aujourd'hui avec Haman au festin que je lui ai préparé.
5:5	Alors le roi dit : Qu'on fasse venir en toute hâte Haman pour accomplir la parole d'Esther. Le roi vint avec Haman au festin qu'Esther avait préparé.
5:6	Le roi dit à Esther, pendant qu'on buvait le vin : Quelle est ta demande ? Elle te sera accordée. Quelle est ta requête ? Jusqu’à la moitié du royaume – que ce soit fait !
5:7	Esther répondit et dit : Voici ma requête et ma demande :
5:8	Si j'ai trouvé grâce aux yeux du roi, et si le roi trouve bon d'accorder ma demande et d'accomplir ma requête, que le roi et Haman viennent au festin que je leur préparerai, et je ferai demain selon la parole du roi.
5:9	Haman sortit ce jour-là, joyeux et le cœur content. Mais aussitôt qu'il vit, à la porte du roi, Mordekay, qui ne se leva pas, et ne se remua pas pour lui, Haman fut rempli de colère contre Mordekay.
5:10	Toutefois, Haman se fit violence et il alla dans sa maison. Puis il envoya chercher ses amis et Zéresh, sa femme.
5:11	Haman leur fit le compte de la gloire de ses richesses, du nombre de ses enfants, de tout ce que le roi avait fait pour l'agrandir, et comment il l'avait élevé au-dessus des princes et des serviteurs du roi.
5:12	Haman dit : Même la reine Esther n'a fait venir que moi et le roi au festin qu'elle a fait, et je suis encore convié par elle avec le roi.
5:13	Mais tout cela ne vaut rien pour moi, tout le temps où je vois Mordekay, le Juif, assis à la porte du roi.
5:14	Zéresh sa femme, et tous ses amis lui répondirent : Qu'on prépare un bois haut de 50 coudées, et demain matin dis au roi qu'on y pende Mordekay. Et tu iras joyeux au festin avec le roi. Cette parole parut bonne en face de Haman, et il prépara le bois.

## Chapitre 6

### Le roi Assuérus se souvient de Mordekay et le récompense

6:1	Cette nuit-là, le roi ne put dormir. Il ordonna qu'on lui apporte le livre de souvenirs<!--Voir Mal. 3:16.-->, les discours du jour, et on les lut en face du roi.
6:2	Et on trouva écrit ce qu’avait fait connaître Mordekay au sujet de Bigthan et de Théresh, deux eunuques du roi, de la garde du seuil, qui avaient cherché à porter la main sur le roi Assuérus.
6:3	Le roi dit : Quelle marque d'honneur et de grandeur a-t-on accordée à Mordekay pour cela ? Et les jeunes hommes du roi, qui le servaient, dirent : On ne lui a rien accordé.
6:4	Et le roi dit : Qui est dans la cour ? Or, Haman était venu dans la cour extérieure de la maison du roi, pour dire au roi de faire pendre Mordekay au bois qu'il avait préparé pour lui.
6:5	Les jeunes hommes du roi lui dirent : Voici Haman qui se tient dans la cour. Et le roi dit : Qu'il entre !
6:6	Haman entra et le roi lui dit : Que faire à un homme que le roi désire honorer ? Or Haman dit en son cœur : Qui le roi désirerait-il honorer plus que moi ?
6:7	Et Haman dit au roi : Quant à l'homme que le roi désire honorer,
6:8	qu'on lui apporte le vêtement royal dont le roi se revêt, le cheval que monte le roi et la couronne royale qui se met sur sa tête.
6:9	Et que le vêtement et le cheval soient remis aux mains de l'un des princes nobles qui sont auprès du roi, et qu'on revête l'homme que le roi prend plaisir à honorer, et qu'on le promène dans les rues de la ville, monté sur le cheval et qu'on crie devant lui : C’est ainsi qu’on fait à l’homme que le roi désire honorer !
6:10	Le roi dit à Haman : Hâte-toi de prendre le vêtement et le cheval, comme tu l'as dit, et fais ainsi pour Mordekay le Juif qui est assis à la porte du roi ! Ne laisse pas tomber une parole de tout ce dont tu as parlé !
6:11	Haman prit le vêtement et le cheval, il habilla Mordekay et le promena à cheval dans les rues de la ville, et il criait devant lui : C’est ainsi qu’on fait à l’homme que le roi désire honorer !
6:12	Mordekay retourna à la porte du roi, et Haman se retira en hâte dans sa maison, pleurant et ayant la tête couverte.
6:13	Haman raconta à Zéresh, sa femme, et à tous ses amis, tout ce qui lui était arrivé. Ses sages, et Zéresh, sa femme, lui dirent : Si Mordekay devant lequel tu as commencé à tomber, est de la postérité des Juifs, tu n'auras pas le dessus sur lui, mais tu tomberas, tu tomberas devant lui.
6:14	Comme ils parlaient encore avec lui, les eunuques du roi vinrent et se hâtèrent d'amener Haman au festin qu'Esther avait préparé.

## Chapitre 7

### La plaidoirie d'Esther

7:1	Le roi et Haman allèrent au festin chez la reine Esther.
7:2	Le roi dit encore à Esther, ce second jour, pendant qu'on buvait le vin : Quelle est ta demande, reine Esther ? Elle te sera accordée ! Et quelle est ta requête ? Quand ce serait la moitié du royaume, cela sera fait.
7:3	La reine Esther répondit et dit : Si j'ai trouvé grâce à tes yeux, roi, et si le roi le trouve bon, que mon âme me soit donnée à ma demande et mon peuple à ma requête.
7:4	Car nous avons été vendus, mon peuple et moi, pour être détruits, tués et exterminés. Si nous avions été vendus pour être esclaves et serviteurs, j'aurais gardé le silence, bien que l'oppresseur ne saurait compenser le dommage que le roi en recevrait.
7:5	Le roi Assuérus parla et dit à la reine Esther : Qui est-il, et où est-il celui dont le cœur est rempli de tels desseins ?
7:6	Esther dit : L'homme qui est notre adversaire, notre ennemi, c'est Haman, le méchant que voici ! Alors Haman fut terrifié en face du roi et de la reine.

### Haman pris à son propre piège

7:7	Le roi, dans sa colère, se leva et quitta le festin, il entra dans le jardin du palais. Mais Haman resta, afin de faire une requête pour son âme à la reine Esther, car il voyait que le mal avait été décidé contre lui par le roi.
7:8	Le roi revint du jardin du palais dans la salle du festin, or Haman s'était jeté sur le lit où était Esther et le roi dit : Va-t-il aussi violer la reine sous mes yeux, dans cette maison ? Dès que la parole fut sortie de la bouche du roi, aussitôt on couvrit le visage d'Haman.
7:9	Et Harbona, l'un des eunuques, dit en face du roi : Voici que le bois préparé par Haman pour Mordekay, qui a parlé pour le bien du roi, est dressé dans la maison d'Haman, à une hauteur de 50 coudées. Alors le roi dit : Qu'on l'y pende !
7:10	Et ils pendirent Haman au bois qu'il avait préparé pour Mordekay. Et la colère du roi fut apaisée.

## Chapitre 8

### L'échec du plan d'Haman

8:1	Ce même jour, le roi Assuérus donna à la reine Esther la maison d'Haman, l'oppresseur des Juifs et Mordekay fut introduit en face du roi, car Esther avait raconté ce qu'il était pour elle.
8:2	Le roi ôta son anneau, celui qu'il avait repris à Haman et le donna à Mordekay. Esther établit Mordekay sur la maison d'Haman.
8:3	Esther parla de nouveau en face du roi. Elle se jeta à ses pieds, elle pleura, elle implora sa faveur pour invalider la méchanceté d'Haman l'Agaguite et son projet qu’il avait imaginé contre les Juifs.
8:4	Et le roi tendit le sceptre d'or à Esther qui se releva et resta debout en face du roi.
8:5	Elle dit : Si le roi le trouve bon, et si j'ai trouvé grâce en face de lui, et si la chose est convenable en face du roi, et si je suis agréable à ses yeux, qu'on écrive pour révoquer les lettres qui concernaient le projet d'Haman fils d'Hammedatha, l'Agaguite, qu'il avait écrites pour détruire les Juifs qui sont dans toutes les provinces du roi.
8:6	Car comment pourrais-je voir le mal qui arriverait à mon peuple, et comment pourrais-je voir la destruction de ma parenté ?
8:7	Le roi Assuérus dit à la reine Esther et au Juif Mordekay : Voici, j'ai donné la maison d'Haman à Esther, et il a été pendu au bois pour avoir étendu sa main contre les Juifs.
8:8	Vous, au nom du roi, écrivez comme bon vous semble au sujet des Juifs et scellez l'écrit avec l'anneau du roi ! En effet, un document écrit au nom du roi et scellé de l'anneau du roi ne peut être révoqué.
8:9	Et en ce même temps, le vingt-troisième jour du troisième mois, qui est le mois de Sivan, les scribes du roi furent appelés et on écrivit comme Mordekay l'ordonna aux Juifs, aux satrapes, aux gouverneurs, et aux princes des 127 provinces, de l'Inde jusqu'à l'Éthiopie, à chaque province selon son écriture, à chaque peuple selon sa langue, et aux Juifs selon leur écriture et selon leur langue.
8:10	On écrivit au nom du roi Assuérus et on scella avec l'anneau du roi. On envoya les lettres par la main de coureurs à cheval, montés sur les coursiers royaux, fils de juments.
8:11	Le roi accordait aux Juifs de toutes les villes de se rassembler et de défendre leur âme, de détruire, de tuer, et d'exterminer toute force armée d'un peuple ou d'une province qui voudrait les assiéger, avec leurs petits enfants et leurs femmes, et de piller leurs biens.
8:12	Cela en un seul jour, dans toutes les provinces du roi Assuérus, le treizième jour du douzième mois, qui est le mois d'Adar.
8:13	Une copie de l’écrit devait être donnée comme décret dans chaque province et révélée à tous les peuples, afin que les Juifs soient prêts en ce jour-là pour se venger de leurs ennemis.
8:14	Les coureurs, montés sur des coursiers, sur des coursiers royaux, sortirent, hâtés et pressés par la parole du roi. Le décret fut aussi donné à Suse, la ville capitale.

### Mordekay honoré

8:15	Mordekay sortit de la présence du roi avec un vêtement royal violet, d'étoffe blanche, avec une grande couronne en or, et une robe en byssus et en pourpre rouge. Et la ville de Suse poussait des cris de réjouissance, et elle fut dans la joie.
8:16	Il y eut pour les Juifs du bonheur et de la joie, des réjouissances et des honneurs.
8:17	Dans chaque province et dans chaque ville, partout où arrivait l'ordre du roi et son décret, il y eut pour les Juifs de la joie, des réjouissances, des festins, et des fêtes. Et beaucoup de gens d'entre les peuples de la terre se faisaient Juifs, parce que la frayeur des Juifs les avait saisis.

## Chapitre 9

### Les Juifs triomphent de leurs ennemis

9:1	Le douzième mois, qui est le mois d'Adar, le treizième jour du mois, où la parole du roi et son décret devaient être exécutés, au jour où les ennemis des Juifs espéraient dominer, ce fut le contraire qui arriva. Les Juifs furent maîtres de ceux qui les haïssaient.
9:2	Les Juifs se rassemblèrent dans leurs villes, dans toutes les provinces du roi Assuérus pour mettre la main sur ceux qui cherchaient leur perte. Personne ne put leur résister, car la crainte qu'on avait d'eux avait saisi tous les peuples.
9:3	Tous les princes des provinces, les satrapes, les gouverneurs et ceux qui faisaient les affaires du roi soutenaient les Juifs, car la terreur de Mordekay était tombée sur eux.
9:4	Car Mordekay était grand dans la maison du roi, et sa renommée allait dans toutes les provinces, parce que cet homme, Mordekay, allait toujours grandissant.
9:5	Les Juifs frappèrent tous leurs ennemis à coups d'épée et en firent un grand carnage, de sorte qu'ils traitèrent selon leurs désirs ceux qui les haïssaient.
9:6	Et même dans Suse, la capitale, les Juifs tuèrent et firent périr 500 hommes.
9:7	Ils tuèrent aussi Parshandatha, Dalphon, Aspatha,
9:8	Poratha, Adalia, Aridatha,
9:9	Parmashtha, Arizaï, Aridaï, et Vayezatha,
9:10	les dix fils d'Haman, fils d'Hammedatha, l'oppresseur des Juifs. Mais ils ne mirent pas leurs mains au pillage.
9:11	Ce jour-là, on rapporta au roi le nombre de ceux qui avaient été tués dans Suse, la capitale.
9:12	Le roi dit à la reine Esther : Dans Suse, la capitale, les Juifs ont tué et détruit 500 hommes et les dix fils d'Haman, qu’ont-ils fait dans le reste des provinces du roi ? Quelle est ta demande ? Et elle te sera accordée. Et quelle est encore ta requête ? Que ce soit fait !
9:13	Esther dit : Si le roi le trouve bon qu'il soit permis aux Juifs qui sont à Suse, d'agir encore demain selon le décret d'aujourd'hui, et que l'on pende au bois les dix fils d'Haman.
9:14	Le roi ordonna de faire ainsi. Le décret fut donné à Suse, et les dix fils de Haman furent pendus.
9:15	Les Juifs qui étaient dans Suse se rassemblèrent encore le quatorzième jour du mois d'Adar, et tuèrent dans Suse 300 hommes. Mais ils ne mirent pas la main au pillage.
9:16	Le reste des Juifs qui étaient dans les provinces du roi se rassemblèrent et défendirent leur âme. Ils eurent du repos et furent délivrés de leurs ennemis, et ils tuèrent 75 000 hommes de ceux qui les haïssaient. Mais ils ne mirent pas la main au pillage.
9:17	C'était le treizième jour du mois d'Adar. Le quatorzième, ils se reposèrent et en firent un jour de festin et de joie.

### Institution de la fête des Pourim

9:18	Les Juifs qui étaient dans Suse, se réunirent le treizième et le quatorzième jour du même mois, et ils se reposèrent le quinzième jour. Ils le célébrèrent comme un jour de festin et de joie.
9:19	C'est pourquoi les Juifs des campagnes, qui habitent dans des villes sans murailles, font le quatorzième jour du mois d'Adar un jour de joie, de festin, un jour de fête, où l'on s'envoie des portions chacun à son compagnon.
9:20	Mordekay écrivit ces choses, et il envoya les lettres à tous les Juifs qui étaient dans toutes les provinces du roi Assuérus, auprès et au loin,
9:21	leur ordonnant de célébrer d'année en année le quatorzième jour et le quinzième jour du mois d'Adar
9:22	comme les jours où les Juifs avaient eu du repos de leurs ennemis, selon le mois où leur angoisse fut changée en joie, et leur deuil en jour heureux, et de faire de ces jours des jours de festin et de joie, où l'on s'envoie des portions chacun à son compagnon, et des dons aux pauvres.
9:23	Et les Juifs acceptèrent de faire ce qu'on avait commencé et ce que Mordekay leur avait écrit.
9:24	Car Haman, fils d'Hammedatha, l'Agaguite, l'oppresseur de tous les Juifs, avait machiné contre les Juifs de les détruire, et il avait jeté le Pour, c'est-à-dire le sort, pour les mettre en déroute et les faire périr.
9:25	Mais quand elle vint devant le roi, celui-ci dit dans un livre : « Que son projet mauvais qu’il a projeté contre les Juifs revienne sur sa tête », et on le pendit, lui et ses fils, au bois.
9:26	C'est pourquoi on a appelé ces jours-là Pourim, du nom de Pour<!--Pour ou Pourim : ce terme signifie « sort » (Est. 3:7). La fête de Pourim a été instituée pour célébrer la délivrance de l'extermination planifiée par Haman, à la suite de l'intervention héroïque d'Esther. Les Juifs l'observent désormais chaque année le 14 du mois d'Adar (février ou mars) depuis le temps d'Esther jusqu'à ce jour.-->. C'est pourquoi, d'après toutes les paroles de cette lettre, d'après ce qu'ils ont vu à ce sujet et ce qui leur est arrivé,
9:27	les Juifs établirent et acceptèrent pour eux, pour leur postérité et pour tous ceux qui se joindraient à eux – pour que cela ne passe pas –, de célébrer ces deux jours, selon leur écrit et selon leur temps fixé, en tout, d’année en année.
9:28	Ces jours devaient être rappelés et célébrés d'âges en âges, dans chaque famille, dans chaque province et dans chaque ville. Ces jours des Pourim ne devaient jamais être abolis parmi les Juifs, ni le souvenir s'en effacer parmi leur postérité.
9:29	La reine Esther, fille d'Abichaïl, écrivit aussi avec le Juif Mordekay, de manière pressante pour la seconde fois, pour confirmer cette lettre sur les Pourim.
9:30	On envoya des lettres à tous les Juifs, dans les 127 provinces du royaume d'Assuérus, avec des paroles de paix et de vérité,
9:31	pour établir ces jours de Pourim au temps fixé, comme Mordekay le Juif et la reine Esther les avaient établis pour eux, et comme ils les avaient établis pour leurs âmes et pour leur postérité, les paroles des jeûnes et leurs cris.
9:32	Ainsi l'ordre d'Esther confirma ces paroles de Pourim, et cela fut écrit dans le livre.

## Chapitre 10

### Mordekay : un grand homme

10:1	Le roi Assuérus fixa un impôt sur la terre et sur les îles de la mer.
10:2	Tous ses actes de puissance et de bravoure, ainsi que les détails de la grandeur de Mordekay à laquelle le roi l'éleva, ces choses ne sont-elles pas écrites dans le livre des discours du jour des rois de Médie et de Perse ?
10:3	Car Mordekay le Juif fut le second après le roi Assuérus, et il fut grand parmi les Juifs, et agréable à la multitude de ses frères, cherchant le bonheur de son peuple et parlant de paix pour toute sa postérité.
