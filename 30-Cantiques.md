# Shir Hashirim (Cantique des cantiques) (Ca.)

Signification : Cantique des cantiques

Auteur : Shelomoh (Salomon)

Thème : L'amour

Date de rédaction : 10ème siècle av. J.-C.

Shir Hashirim est une suite de poèmes célébrant l'amour. Il présente la relation d'un homme et d'une femme, dont l'attrait réciproque les pousse à dévoiler leurs sentiments avec douceur. L'union de Shelomoh et de la Shoulamite exprime différents aspects de l'amour tels que l'attente, le désir, la souffrance ou la passion, et ce dans toute la pureté et la sainteté qui conviennent à la maison d'Elohîm.

Au-delà de l'image du couple, on peut découvrir au travers de ces écrits l'expression de l'amour d'Elohîm à l'égard de l'être humain, tout comme celui de l'Époux divin envers l'Assemblée (Église).

## Chapitre 1

### La fiancée et le fiancé expriment leur amour mutuel

1:1	Cantique des cantiques de Shelomoh.
1:2	[La Shoulamite :] Qu'il m'embrasse des baisers de sa bouche ! [Les filles de Yeroushalaim :] Car tes amours sont plus agréables que le vin,
1:3	l'odeur de tes huiles est si agréable ! Ton nom est une huile qui se répand. C'est pourquoi les vierges<!--Jeunes filles, jeunes femmes. Voir Es. 7:14.--> t'aiment.
1:4	[La Shoulamite :] Entraîne-moi derrière toi ! [Les filles de Yeroushalaim :] Nous courrons après toi ! [La Shoulamite :] Le roi m'a fait entrer dans ses chambres. [Les filles de Yeroushalaim :] Nous nous égaierons, nous nous réjouirons en toi ; nous célébrerons tes amours plus que le vin. C'est avec droiture que l'on t'aime.
1:5	[La Shoulamite :] Je suis noire, je suis belle, filles de Yeroushalaim, comme les tentes de Qedar, comme les tapis de Shelomoh.
1:6	Ne me regardez pas, moi, la noirâtre, car le soleil m'a regardée. Les fils de ma mère se sont mis en colère contre moi, ils m'ont faite gardienne des vignes et je n'ai pas gardé la vigne qui était à moi.
1:7	Déclare-moi, toi que mon âme aime, où tu fais paître ton troupeau, et où tu les fais reposer à midi. Car pourquoi serais-je comme une femme errante vers les troupeaux de tes compagnons ?
1:8	[Shelomoh :] Si tu ne le sais pas, toi, la plus belle des femmes, sors sur les traces du troupeau et fais paître tes chevrettes près des tabernacles des bergers.
1:9	Ma grande amie, je te compare au plus beau couple de chevaux que j'ai aux chars de pharaon.
1:10	Tes joues ont bonne grâce avec les atours, et ton cou avec les colliers.
1:11	[Les filles de Yeroushalaim :] Nous te ferons des colliers en or avec des points en argent.
1:12	[La Shoulamite :] Tandis que le roi est assis à table, mon nard donne son parfum.
1:13	Mon bien-aimé est pour moi comme un sachet de myrrhe. Il passe la nuit entre mes seins.
1:14	Mon bien-aimé est pour moi comme une grappe de henné dans les vignes d'En-Guédi.
1:15	[Shelomoh :] Te voilà belle, ma grande amie, te voilà belle ! Tes yeux sont comme ceux des colombes.
1:16	[La Shoulamite :] Te voilà beau, mon bien-aimé, que tu es agréable ! Combien verdoyant est notre lit !
1:17	Les poutres de nos maisons sont de cèdre, et nos chevrons de cyprès.

## Chapitre 2

### L'amour entre la fiancée et le fiancé

2:1	[La Shoulamite :] Je suis la rose de Sharôn, le lis des vallées.
2:2	[Shelomoh :] Comme le lis au milieu des épines<!--L'Assemblée du Seigneur est au milieu des loups (Mt. 10:16).-->, telle est ma grande amie entre les filles.
2:3	[La Shoulamite :] Comme le pommier au milieu des arbres de la forêt, tel est mon bien-aimé entre les jeunes hommes. J'ai désiré son ombre et m'y suis assise, et son fruit a été doux à mon palais.
2:4	Il m'a fait entrer dans la maison du vin<!--La maison du vin ou des festins (Ap. 19:7-9).-->, et sa bannière qu'il dresse sur moi, c'est l'amour<!--Yéhoshoua ha Mashiah (Jésus-Christ) est notre bannière (Ex. 17:15).-->.
2:5	Soutenez-moi avec des gâteaux de raisins, fortifiez-moi avec des pommes, car je suis malade d'amour.
2:6	Que sa main gauche soit sous ma tête et que sa droite m'embrasse !
2:7	[Shelomoh :] Filles de Yeroushalaim, je vous en conjure, par les gazelles et par les biches des champs, ne réveillez pas, ne réveillez pas l'amour avant qu'il ne le veuille !

### La Shoulamite parle de Shelomoh

2:8	[La Shoulamite :] C'est la voix de mon bien-aimé ! Le voici qui vient, sautant sur les montagnes et bondissant sur les collines.
2:9	Mon bien-aimé est semblable à la gazelle ou au faon des biches. Le voici qui se tient derrière notre muraille, il regarde par les fenêtres, il se fait voir par les treillis.
2:10	[La Shoulamite rapporte les paroles de Shelomoh :] Mon bien-aimé a répondu et m'a dit : Lève-toi pour toi<!--Leikh leikha. Voir commentaire en Genèse 12:1.-->, ma grande amie, ma belle, et va pour toi !
2:11	Car voici, l'hiver est passé, la pluie a cessé, elle s'en est allée.
2:12	Les fleurs paraissent sur la terre, le temps des chants est venu et la voix de la tourterelle se fait déjà entendre dans notre contrée.
2:13	Le figuier produit ses premiers fruits et les vignes en fleurs donnent leur parfum. Lève-toi pour toi, ma grande amie, ma belle, et va pour toi<!--Leikh leikha. Voir commentaire en Genèse 12:1.--> !
2:14	Ma colombe qui te tiens dans les fentes du rocher<!--Yéhoshoua ha Mashiah (Jésus-Christ) est notre Rocher (Es. 8:13-17). L'Assemblée véritable est fondée sur le Roc, c'est-à-dire Yéhoshoua ha Mashiah lui-même (Mt. 16:18).-->, dans les lieux secrets<!--Les lieux secrets (Mt. 6:6).--> et escarpés, fais-moi voir ton apparence, fais-moi entendre ta voix, car ta voix est douce et ton apparence est belle.
2:15	Prenez-nous les renards, et les petits renards qui ravagent les vignes, car nos vignes produisent des grappes.
2:16	[La Shoulamite :] Mon bien-aimé est à moi, et je suis à lui, il paît parmi les lis.
2:17	Avant que le jour se rafraîchisse et que les ombres s'enfuient, reviens mon bien-aimé, comme la gazelle ou le faon des biches, sur les montagnes de Bether<!--Montagnes de séparation : région montagneuse de Kena'ân (Canaan), site inconnu.-->.

## Chapitre 3

### La fiancée recherche son fiancé et le trouve

3:1	[La Shoulamite :] J'ai cherché pendant les nuits, sur ma couche, celui que mon âme aime. Je l'ai cherché, mais je ne l'ai pas trouvé.
3:2	Je me lèverai maintenant, et je ferai le tour de la ville, par les carrefours et par les places, et je chercherai celui que mon âme aime. Je l'ai cherché, mais je ne l'ai pas trouvé.
3:3	Les gardes qui font le tour de la ville m'ont trouvée : Avez-vous vu celui que mon cœur aime ?
3:4	À peine les avais-je dépassés, que j'ai trouvé celui que mon âme aime. Je l'ai saisi et je ne l'ai pas lâché jusqu'à ce que je l'aie amené dans la maison de ma mère, et dans la chambre de celle qui m'a conçue.
3:5	[Shelomoh :] Filles de Yeroushalaim, je vous en conjure par les gazelles et par les biches des champs, ne réveillez pas, ne réveillez pas l'amour avant qu'il ne le veuille !

### Shelomoh entre avec sa fiancée dans Sion

3:6	[La Shoulamite :] Qui est celle qui monte du désert comme des colonnes de fumée en forme de palmiers, parfumée de myrrhe et d'encens et de tous les aromates du parfumeur ?
3:7	[Un officier des gardes du roi Shelomoh :] Voici le lit de Shelomoh, autour duquel il y a 60 vaillants hommes, des plus vaillants d'Israël.
3:8	Tous maniant l'épée et très bien exercés à la guerre. Chacun porte son épée sur sa cuisse à cause des frayeurs de la nuit.
3:9	Le roi Shelomoh s'est fait un lit en bois du Liban.
3:10	Il a fait ses colonnes en argent, ses appuis en or, son siège en écarlate. Le milieu a été dessiné avec amour par les filles de Yeroushalaim.
3:11	[Les filles de Yeroushalaim :] Sortez, filles de Sion ! Regardez le roi Shelomoh avec la couronne dont sa mère l'a couronné le jour de ses noces, le jour de la joie de son cœur !

## Chapitre 4

### Le fiancé déclare son amour

4:1	[Shelomoh :] Te voilà belle, ma grande amie, te voilà belle ! Tes yeux sont comme ceux des colombes derrière ton voile. Tes cheveux sont comme un troupeau de chèvres sur les pentes de la montagne de Galaad.
4:2	Tes dents sont comme un troupeau de brebis tondues qui remontent du lavoir : toutes portent des jumeaux et aucune n'est sans enfants.
4:3	Tes lèvres sont comme un fil teint d'écarlate et ta bouche est gracieuse. Ta tempe est comme une pièce de pomme de grenade, derrière ton voile.
4:4	Ton cou est comme la tour de David, bâtie pour les armes à laquelle sont suspendus mille boucliers et tous les boucliers des hommes vaillants.
4:5	Tes deux seins sont comme deux faons, comme les jumeaux d'une gazelle qui paissent au milieu des lis.
4:6	Jusqu’à ce que le vent du jour souffle et que les ombres s'enfuient, j’irai pour moi vers la montagne de myrrhe et vers la colline de l'encens.
4:7	Tu es toute belle, ma grande amie, en toi, pas de tache<!--Le mot hébreu signifie aussi « défaut ». Voir Lé. 21:17-18 ; No. 19:2 ; Ep. 5:27.-->.
4:8	Viens du Liban avec moi, mon épouse, viens du Liban avec moi ! Regarde du sommet de l'Amanah, du sommet du Sheniyr et de l'Hermon, des repaires des lions et des montagnes des léopards !
4:9	Tu me ravis le cœur, ma sœur, mon épouse, tu me ravis le cœur par l’un de tes yeux, par l’un des colliers de ton cou.
4:10	Que ton amour est beau, ma sœur, mon épouse ! Que ton amour est meilleur que le vin, et l'odeur de tes huiles, que tous les aromates !
4:11	Tes lèvres, mon épouse, distillent des rayons de miel. Le miel et le lait sont sous ta langue, et l'odeur de tes vêtements est comme l'odeur du Liban.
4:12	Ma sœur, mon épouse, tu es un jardin fermé, une source fermée, une fontaine scellée<!--La virginité de l'Assemblée (Église) : « un jardin clos », « une source close » et « une fontaine scellée ». Nous avons ici une belle image de l'Assemblée (Ep. 5:25-27) comparée par Paulos (Paul) à une vierge pure destinée à être présentée à son Mashiah (Christ), son Époux (2 Co. 11:2). Selon la torah de Moshé (loi de Moïse), un homme qui séduisait une vierge devait l'épouser (Ex. 22:15) et ne pouvait plus jamais la répudier (De. 22:28-29). Tout comme la femme adultère, la vierge fiancée qui se rendait coupable d'infidélité était lapidée (De. 22:22-24), sauf si elle n'avait pas pu appeler à l'aide lors de son viol (De. 22:25-27). Un homme qui accusait faussement sa femme de ne pas avoir été vierge lors du mariage était sévèrement puni, mais si l'accusation se révélait exacte, la femme était lapidée (De. 22:13-21). Si une femme était faussement accusée, ses parents pouvaient produire, pour la disculper, les « signes de sa virginité » (De. 22:15), c'est-à-dire le drap nuptial portant les traces sanglantes de la perforation de l'hymen. Notons que le grand-prêtre n'avait le droit d'épouser qu'une vierge (Lé. 21:13-14). De même, Yéhoshoua ha Mashiah, le Grand-Prêtre par excellence, désire aussi une Assemblée vierge.-->.
4:13	Tes rejetons sont un jardin clos de grenadiers avec des fruits excellents : le henné avec les nards,
4:14	le nard et le safran, le roseau aromatique et le cinnamome, avec tous les arbres d'encens, la myrrhe et l'aloès, avec tous les principaux aromates.
4:15	Fontaine des jardins ! Source d'eaux vives ! Ruisseaux coulant du Liban !
4:16	Réveille-toi, nord<!--Aquilon : vent du nord.-->, et viens, midi<!--Autan : vent du sud.--> ! Soufflez sur mon jardin afin que ses drogues aromatiques distillent ! [La Shoulamite :] Que mon bien-aimé entre dans son jardin et qu'il mange de ses fruits excellents !

## Chapitre 5

5:1	[Shelomoh :] J'entre dans mon jardin, ma sœur, mon épouse. Je cueille ma myrrhe avec mes drogues aromatiques, je mange mes rayons de miel et mon miel, je bois mon vin et mon lait. Mes amis, mangez, buvez, enivrez-vous d'amour, mes bien-aimés !

### La Shoulamite raconte son rêve

5:2	[La Shoulamite :] Je dormais, mais mon cœur veillait<!--Mt. 25:1-13.-->. La voix de mon aimé ! Il frappe : [La Shoulamite rapporte les propos de Shelomoh :] Ouvre-moi, ma sœur, ma grande amie, ma colombe, ma parfaite, car ma tête est pleine de rosée et mes cheveux de l'humidité de la nuit !
5:3	[La Shoulamite :] J'ai enlevé ma tunique, comment la revêtirais-je ? J'ai lavé mes pieds, comment les souillerais-je ?
5:4	Mon bien-aimé a avancé la main par le trou et mes entrailles ont été troublées à cause de lui.
5:5	Je me suis levée pour ouvrir à mon bien-aimé, et la myrrhe a distillé de mes mains, et la myrrhe coulante de mes doigts sur la poignée du verrou.
5:6	J'ai ouvert à mon bien-aimé, mais mon bien-aimé s'était retiré, il était parti au loin. Mon âme était hors de moi quand il me parlait ! Je l'ai cherché, mais je ne l'ai pas trouvé. Je l'ai appelé, mais il ne m'a pas répondu.
5:7	Les gardes qui font le tour de la ville m'ont trouvée. Ils m'ont battue, ils m'ont blessée, ils m'ont enlevé mon voile, les gardes des murailles.
5:8	Filles de Yeroushalaim, je vous en conjure, si vous trouvez mon bien-aimé, que lui direz-vous ? Que je suis malade d'amour.
5:9	[Les filles de Yeroushalaim :] Qu'a ton bien-aimé de plus qu'un autre bien-aimé, la plus belle d'entre les femmes ? Qu'a ton bien-aimé de plus qu'un autre bien-aimé pour que tu nous conjures ainsi ?

### La Shoulamite décrit Shelomoh

5:10	[La Shoulamite :] Mon bien-aimé est clair et rouge, un porte-bannière entre des myriades<!--Ou « il paraît entre dix-mille ».-->.
5:11	Sa tête est d'or, d'un or pur. Les boucles de ses cheveux sont flottantes, noires comme un corbeau.
5:12	Ses yeux sont des colombes près des canaux d'eau, se baignant dans du lait, se posant sur la plénitude.
5:13	Ses joues sont comme un parterre de drogues aromatiques, des fleurs parfumées. Ses lèvres sont comme des lis, elles distillent la myrrhe coulante.
5:14	Ses mains sont des anneaux d'or, remplies de chrysolithes. Son ventre est comme de l'ivoire bien poli, couvert de saphirs.
5:15	Ses jambes sont des colonnes de marbre posées sur des bases en or fin. Son apparence est comme le Liban, sélectionnée comme les cèdres.
5:16	Son palais n'est que douceur et tout ce qui est en lui est désirable<!--Mt. 11:29.-->. Tel est mon bien-aimé, tel est mon ami, filles de Yeroushalaim !

## Chapitre 6

### Les filles de Yeroushalaim (Jérusalem) aident la Shoulamite à chercher Shelomoh

6:1	[Les filles de Yeroushalaim :] Où est allé ton bien-aimé, toi la plus belle des femmes ? De quel côté est allé ton bien-aimé ? Nous le chercherons avec toi.
6:2	[La Shoulamite :] Mon bien-aimé est descendu à son jardin, au parterre de drogues aromatiques, pour paître dans les jardins et pour cueillir des lis.
6:3	Je suis à mon bien-aimé, et mon bien-aimé est à moi. Il paît parmi les lis.

### Shelomoh à la Shoulamite

6:4	[Shelomoh :] Ma grande amie, tu es belle comme Tirtsah, agréable comme Yeroushalaim, terrible comme des troupes sous leurs bannières.
6:5	Détourne de moi tes yeux, car ils me troublent. Tes cheveux sont comme un troupeau de chèvres suspendus aux flancs de Galaad.
6:6	Tes dents sont comme un troupeau de brebis qui remontent du lavoir : toutes portent des jumeaux et aucune n'est sans enfants.
6:7	Ta tempe est comme une pièce de pomme de grenade derrière ton voile.
6:8	Les reines sont 60, les concubines 80, les vierges innombrables.
6:9	Unique est ma colombe, ma parfaite. Elle est l'unique pour sa mère, la pure pour celle qui l'a enfantée. Les filles la voient et la disent heureuse, les reines et les concubines aussi, et elles la louent.
6:10	Qui est celle qui paraît comme l'aube du jour, belle comme la lune, pure comme le soleil, terrible comme des troupes sous leurs bannières ?
6:11	[La Shoulamite :] Je suis descendue au jardin des noyers pour voir les fruits de la vallée qui mûrissent, pour voir si la vigne pousse, si les grenadiers fleurissent.
6:12	Je ne sais, mais mon âme m'a jetée sur les chars d'Amminadib<!--Amminadib : mon peuple est bien disposé.--> !

## Chapitre 7

### La beauté de la Shoulamite

7:1	[Les filles de Yeroushalaim :] Reviens, reviens, Shoulamite ! Reviens, reviens, afin que nous te contemplions ! [La Shoulamite :] Qu'avez-vous à contempler la Shoulamite comme une danse de deux armées ?
7:2	[Les filles de Yeroushalaim :] Fille de prince, que tes pieds sont beaux dans tes sandales ! Le contour de ta hanche est comme un collier travaillé de la main d'un excellent artisan.
7:3	Ton nombril est une coupe arrondie où le vin mélangé ne manque pas. Ton ventre est un tas de blé entouré de lis.
7:4	Tes deux seins sont comme deux faons jumeaux d'une gazelle.
7:5	Ton cou est comme une tour d'ivoire. Tes yeux sont des étangs qui sont à Hesbon, près de la porte de Bath-Rabbim. Et ton visage est comme la tour du Liban qui regarde vers Damas.
7:6	Ta tête sur toi est comme le Carmel et les cheveux fins de ta tête sont comme de l'écarlate. Un roi est enchaîné par tes boucles.
7:7	[Shelomoh :] Que tu es belle, que tu es agréable, mon amour, mes délices !
7:8	Ta taille est semblable à un palmier, et tes seins à des grappes.
7:9	Je me dis : Je monterai sur le palmier, j'en saisirai les rameaux ! S’il te plaît, que tes seins deviennent comme des grappes de vigne, l'odeur de tes narines comme celle des pommes,
7:10	et ta bouche comme le bon vin [La Shoulamite :] ... – Qui coule en faveur de mon bien-aimé et qui fait parler les lèvres de ceux qui s'endorment.
7:11	Je suis à mon bien-aimé et son désir se porte vers moi.
7:12	Viens, mon bien-aimé, sortons dans les champs, passons la nuit dans les villages !
7:13	Levons-nous dès le matin pour aller aux vignes, nous verrons si la vigne pousse, si la fleur s'ouvre, si les grenadiers fleurissent. Là je te donnerai mes amours.
7:14	Les mandragores répandent leur parfum, et à nos portes il y a toutes sortes de fruits excellents, des fruits nouveaux et des fruits gardés que je t'ai conservés mon bien-aimé !

## Chapitre 8

8:1	[La Shoulamite :] Ah ! Si tu étais mon frère, allaité au sein de ma mère ! Je te rencontrerais dehors, je t'embrasserais et l'on ne me mépriserait même pas.
8:2	Je te conduirais, je te ferais entrer dans la maison de ma mère, tu m'instruirais et je te ferais boire du vin parfumé d'aromates et du vin doux de mon grenadier.
8:3	Que sa main gauche soit sous ma tête et que sa droite m'embrasse !
8:4	[La Shoulamite (citant Shelomoh) :] Je vous en conjure, filles de Yeroushalaim, ne réveillez pas, ne réveillez pas l'amour avant qu'il ne le veuille !
8:5	[Les frères de la Shoulamite :] Qui est celle-ci qui monte du désert, doucement appuyée sur son bien-aimé ? [Shelomoh :] Je t'ai réveillée sous le pommier. C'est là que ta mère t'a enfantée, c'est là qu'elle t'a enfantée, qu'elle t'a donné le jour.
8:6	Place-moi comme un sceau sur ton cœur<!--L'Assemblée du Seigneur est scellée du Saint-Esprit (Ep. 1:13, 4:30).-->, comme un sceau sur ton bras, car l'amour est fort comme la mort, et la jalousie est cruelle comme le shéol. Ses flammes sont des flammes de feu, une flamme de Yah<!--Yah est le diminutif de YHWH.-->.
8:7	[La Shoulamite (à Shelomoh) :] Beaucoup d'eaux ne pourraient éteindre cet amour-là et même les fleuves ne pourraient le submerger. Si un homme donnait toutes les richesses de sa maison pour cet amour, on le mépriserait, on le mépriserait.
8:8	[La Shoulamite (racontant ce que ses frères lui ont dit) :] Nous avons une petite sœur qui n'a pas encore de seins. Que ferons-nous pour notre sœur, le jour où l'on parlera d'elle ?
8:9	Si elle est une muraille, nous bâtirons sur elle des créneaux en argent. Si elle est une porte, nous la fermerons avec une planche de cèdre.
8:10	[La Shoulamite :] Je suis une muraille et mes seins sont comme des tours. J'ai été alors à ses yeux comme celle qui trouve la paix.
8:11	Shelomoh avait une vigne à Baal-Hamon. Il remit la vigne à des gardiens : chaque homme apportait pour son fruit 1 000 pièces d'argent.
8:12	Ma vigne à moi, je l'ai devant moi. Shelomoh, à toi les 1 000, et 200 à ceux qui en gardent le fruit !
8:13	[Les frères de la Shoulamite :] Toi qui habites dans les jardins, les amis sont attentifs à ta voix ! [Shelomoh :] Fais que je l'entende.
8:14	[La Shoulamite :] Fuis, mon bien-aimé ! Sois semblable à une gazelle ou au faon des biches sur les montagnes des drogues aromatiques !
