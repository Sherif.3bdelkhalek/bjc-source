# Yehezkel (Ézéchiel) (Ez.)

Signification : El fortifie

Auteur : Yehezkel

Thème : Les jugements de YHWH et le retrait de sa gloire du temple

Date de rédaction : 6ème siècle av. J.-C.

Déporté à Babel (Babylone) alors qu'il remplissait la fonction de prêtre, Yehezkel eut la particularité d'exercer le service prophétique hors de la terre d'Israël. Sa mission était en partie d'affermir la foi des déportés par la promesse du jugement de leurs ennemis et du rétablissement de la nation. Il leur rappela aussi que les péchés de leurs pères étaient la raison de leur captivité et que l'occasion leur était donnée de réformer leurs voies.

Yehezkel reçut aussi des prophéties concernant ceux qui étaient restés à Yeroushalaim (Jérusalem), dont la condition n'était guère meilleure que celle des exilés et pour lesquels le pire était à venir. Ses prophéties s'exprimaient en rêves et en visions, c'est donc ainsi qu'il vit la gloire de YHWH quittant le temple de Yeroushalaim. Il reçut plusieurs prophéties sur les derniers temps, notamment la promesse d'un cœur nouveau en vue de la conversion, le retour de la gloire d'Elohîm lors du règne millénaire et le rétablissement total d'Israël.

## Chapitre 1

### Vision de la gloire de YHWH

1:1	Il arriva, la trentième année, le cinquième jour du quatrième mois, comme j'étais parmi les exilés près du fleuve Kebar, que les cieux furent ouverts et je vis des visions d'Elohîm.
1:2	Le 5 du mois, – c'était la cinquième année d'exil du roi Yoyakiyn<!--Yehoyakiyn. Généralement traduit par Jojakin. Voir 2 R. 24:12-16.-->,
1:3	la parole de YHWH vint, elle vint à Yehezkel, fils de Bouzi, le prêtre, en terre des Chaldéens<!--Yehezkel était en exil à Babel (Babylone).-->, près du fleuve Kebar. C'est là que la main de YHWH vint sur lui.
1:4	Je regardai, et voici, un vent de tempête vint du nord, une grosse nuée, un feu qui apportait tout autour son éclat. En son milieu, comme l'œil d'ambre au milieu d’un feu.
1:5	Et au milieu il y avait la ressemblance de quatre vivants<!--Les quatre vivants représentent quatre aspects de Yéhoshoua (Jésus). La face d'homme correspond à l'humanité du Seigneur mise en exergue dans l'évangile de Loukas (Luc). La face de lion symbolise la royauté du Mashiah, mise en évidence dans l'évangile de Matthaios (Matthieu). La face de bœuf fait écho à l'évangile de Markos (Marc) où le Seigneur y est présenté comme serviteur. La face d'aigle symbolise la divinité du Mashiah mise en évidence dans l'évangile de Yohanan (Jean). Le Seigneur y est présenté comme le Fils d'Elohîm et l'Elohîm véritable.--> et voici quel était leur aspect : ils avaient la ressemblance d'un humain.
1:6	Chacun d’eux avait quatre faces et chacun quatre ailes.
1:7	Leurs pieds étaient des pieds droits, et la plante de leurs pieds était comme la plante d'un pied de veau, ils étincelaient comme l'œil de cuivre poli.
1:8	Il y avait des mains d’humain sous leurs ailes à leurs quatre côtés. Tous les quatre avaient leurs faces et leurs ailes.
1:9	Leurs ailes jointes, la femme vers sa sœur. Ils ne se tournaient pas en allant, chaque homme allait au-delà de ses faces.
1:10	Leurs faces ressemblaient à des faces d’humain ; des faces de lion vers la droite pour les quatre ; des faces de bœuf à gauche pour les quatre ; et des faces d'aigle pour les quatre.
1:11	Leurs faces et leurs ailes étaient divisées par le haut. Deux de leurs ailes se joignaient, l'homme vers l'homme, et deux couvraient leurs corps.
1:12	Chaque homme allait au-delà de ses faces. Là où l’Esprit allait apparaître, ils allaient. Ils ne se tournaient pas en allant.
1:13	Et quant à la ressemblance de ces vivants, leur aspect était comme des charbons de feu brûlants, comme l'aspect des torches allant et venant entre les vivants, et le feu avait de l'éclat, et du feu sortaient des éclairs.
1:14	Les vivants couraient et revenaient : leur aspect était celui de l'éclair.
1:15	En regardant les vivants, et voici, une roue apparut sur la Terre auprès des vivants, devant leurs quatre faces.
1:16	L'aspect et la structure de ces roues étaient comme l’œil de la chrysolithe, avec une même ressemblance pour les quatre. Leur aspect et leur structure étaient comme si une roue était au milieu d’une roue.
1:17	En allant, elles allaient de leurs quatre côtés et elles ne se tournaient pas en allant.
1:18	Elles avaient des jantes d'une hauteur terrifiante, et leurs jantes étaient pleines d'yeux tout autour des quatre roues.
1:19	Quand les vivants allaient, les roues allaient à côtés d'eux. Quand ils s'élevaient au-dessus de la Terre, les roues aussi s'élevaient.
1:20	Là où l’Esprit allait apparaître, ils allaient. Là où l'Esprit allait, les roues s'élevaient avec, car l'Esprit des vivants était dans les roues.
1:21	Quand ils allaient, elles allaient, quand ils s'arrêtaient, elles s'arrêtaient, et quand ils s'élevaient au-dessus de la Terre, les roues aussi s'élevaient avec eux, car l'Esprit des vivants était dans les roues.
1:22	Au-dessus de la tête des vivants, la ressemblance d'un firmament, un œil de cristal de glace redoutable étendu sur leurs têtes, bien au-dessus.
1:23	Sous ce firmament leurs ailes se tenaient droites, la femme vers sa sœur. Chaque homme en avait deux qui couvraient de ce côté-ci et chaque homme en avait deux qui couvraient de ce côté-là leur corps.
1:24	J’entendais, quand ils allaient, la voix de leurs ailes comme la voix des grandes eaux<!--Ez. 43:2 ; Ap. 1:15 et 14:2.-->, comme la voix de Shaddaï, une voix de rugissement comme la voix d'une armée. En s’arrêtant ils laissaient tomber leurs ailes.
1:25	Il y avait une voix au-dessus du firmament qui était sur leurs têtes. En s’arrêtant ils laissaient tomber leurs ailes.
1:26	Au-dessus du firmament qui était sur leurs têtes, il y avait comme l'aspect d'une pierre de saphir, la ressemblance d'un trône, et sur la ressemblance du trône, une ressemblance comme l'aspect d'un humain<!--Il est question ici de la manifestation du Mashiah.--> au-dessus.
1:27	Je vis comme l’œil d'ambre, comme l'aspect du feu, à l'intérieur et autour de lui. Depuis l'aspect de ses reins jusqu'en haut, et depuis l'aspect de ses reins jusqu'en bas, je vis quelque chose comme l'aspect du feu, et il y avait de l'éclat tout autour de lui.
1:28	Comme l'aspect de l'arc qui apparaît dans les nuages en un jour de pluie, tel était l'aspect de l'éclat tout autour de lui. Tel était l'aspect de la ressemblance de la gloire de YHWH. Je regardai et je tombai sur mes faces, et j'entendis une voix qui parlait.

## Chapitre 2

### Appel de Yehezkel

2:1	Il me dit : Fils d'humain, tiens-toi debout sur tes pieds, et je parlerai avec toi.
2:2	L’Esprit entra en moi quand il me parla et me fit tenir debout sur mes pieds. J’entendis celui qui me parlait.
2:3	Il me dit : Fils d'humain, je t'envoie vers les fils d'Israël, vers des nations rebelles qui se sont rebellées contre moi. Eux et leurs pères ont péché contre moi jusqu'à ce jour même<!--Jé. 3:25.-->.
2:4	Ce sont des fils durs de faces et au cœur obstiné. Je t'envoie vers eux et tu leur diras : Ainsi parle Adonaï YHWH.
2:5	Et eux, qu’ils écoutent ou qu’ils s’abstiennent, oui, eux, la maison de la rébellion, ils sauront qu'il y avait un prophète parmi eux<!--Es. 6:9-10.-->.
2:6	Et toi, fils d'humain, ne les crains pas, ne crains pas leurs paroles, parce que tu as avec toi des ronces et des épines et que tu habites parmi des scorpions. Ne crains pas leurs paroles, et ne t'effraie pas à cause d'eux, quoiqu'ils soient une maison de la rébellion<!--Jé. 1:8 ; 1 Pi. 3:14.-->.
2:7	Tu leur déclareras mes paroles, qu’ils écoutent ou qu’ils s’abstiennent, car ils ne sont que rébellion.
2:8	Et toi, fils d'humain, écoute ce que je te dis, et ne deviens pas rebelle comme la maison de la rébellion ! Ouvre ta bouche et mange ce que je vais te donner<!--Jé. 15:16 ; Ap. 10:9.-->.
2:9	Je regardai, et voici une main envoyée vers moi, et voici, en elle, un livre en rouleau.
2:10	Elle le déploya devant moi : il était écrit devant et derrière. Il y était écrit : des chants funèbres, des gémissements et des lamentations.

## Chapitre 3

### YHWH établit Yehezkel comme sentinelle

3:1	Il me dit : Fils d'humain, mange ce que tu trouveras, mange ce rouleau, et va, parle à la maison d'Israël !
3:2	J'ouvris ma bouche, et il me fit manger ce rouleau.
3:3	Il me dit : Fils d'humain, nourris ton ventre et remplis tes entrailles de ce rouleau que je te donne ! Je le mangeai, et il fut doux dans ma bouche comme du miel<!--Ps. 119:103.-->.
3:4	Il me dit : Fils d'humain, marche, va vers la maison d'Israël pour leur déclarer mes paroles.
3:5	Tu n'es pas envoyé vers un peuple au langage inconnu, ou à la langue barbare, c'est vers la maison d'Israël.
3:6	Non, ce n'est pas vers beaucoup de peuples ayant un langage inconnu ou une langue barbare, dont tu ne puisses comprendre les paroles. Si je t'envoyais vers eux, ils t'écouteraient.
3:7	Mais la maison d'Israël ne voudra pas t'écouter parce qu'ils ne veulent pas m'écouter. Car toute la maison d'Israël a le front dur et le cœur obstiné.
3:8	Voici, j'endurcirai tes faces contre leurs faces, et j'endurcirai ton front contre leurs fronts<!--Jé. 1:17-19 ; Mi. 3:8.-->.
3:9	J'ai rendu ton front semblable à un diamant, plus dur que le roc. Ne les crains pas, et n'aie pas peur de leurs faces – car c'est la maison de la rébellion.
3:10	Il me dit : Fils d'humain, reçois dans ton cœur et écoute de tes oreilles toutes les paroles que je te dirai.
3:11	Marche et va vers les exilés, vers les fils de ton peuple ! Tu leur parleras et, qu'ils écoutent ou qu’ils s’abstiennent, tu leur diras : Ainsi parle Adonaï YHWH.
3:12	L'Esprit m'enleva, et j'entendis derrière moi la voix d'un grand tremblement : Bénie soit la gloire de YHWH du lieu de sa demeure !
3:13	La voix des ailes des vivants, embrassant la femme sa sœur, la voix des roues auprès d'eux et la voix d'un grand tremblement.
3:14	L'Esprit me prit et me transporta. J'allai, l'esprit rempli d'amertume et de colère, mais la main de YHWH me fortifia.
3:15	J'arrivai à Thel-Abib, chez les exilés qui habitaient près du fleuve Kebar, là où ils habitaient. Je restai là sept jours, stupéfait, au milieu d'eux.
3:16	Il arriva qu’au bout de sept jours la parole de YHWH vint à moi, en disant :
3:17	Fils d'humain, je t'ai donné comme sentinelle à la maison d'Israël. Tu écouteras la parole de ma bouche, et tu les avertiras de ma part<!--Es. 52:8, 62:6 ; Jé. 6:17.-->.
3:18	Quand je dirai au méchant : Tu mourras, tu mourras ! Si tu ne l'avertis pas, et si tu ne parles pas pour l'avertir de se détourner de ses mauvaises voies, pour le faire vivre, le méchant, lui, mourra dans son iniquité, mais je demanderai son sang de ta main.
3:19	Et si tu avertis le méchant, et qu'il ne se détourne pas de sa méchanceté ni de ses mauvaises voies, il mourra dans son iniquité, mais toi, tu sauveras ton âme<!--Ez. 18:23-24, 33:6.-->.
3:20	Si le juste se détourne de sa justice et commet l'injustice, je mettrai une pierre d'achoppement devant lui et il mourra. Parce que tu ne l'auras pas averti, il mourra dans son péché et l'on ne se souviendra plus de la justice qu'il avait pratiquée, mais je demanderai son sang de ta main.
3:21	Et si tu avertis le juste de ne pas pécher, et qu'il ne pèche pas, il vivra, il vivra parce qu'il aura été averti, et toi, tu sauveras ton âme.
3:22	La main de YHWH vint sur moi, et il me dit : Lève-toi, sors vers la vallée et là je te parlerai.
3:23	Je me levai et sortis dans la vallée. Et voici, la gloire de YHWH se tenait là, telle que je l'avais vue près du fleuve Kebar, et je tombai sur mes faces.
3:24	L'Esprit entra en moi et me fit tenir debout sur mes pieds. Il me parla et dit : Entre, et enferme-toi dans ta maison.
3:25	Fils d'humain, voici, on mettra des cordes sur toi, on te liera, et tu ne sortiras pas pour aller parmi eux.
3:26	Et je collerai ta langue à ton palais et tu seras muet. Tu ne seras plus pour eux l'homme qui corrige, parce qu'ils sont une maison de la rébellion<!--Lu. 1:20-22.-->.
3:27	Et quand je parlerai avec toi, j'ouvrirai ta bouche et tu leur diras : Ainsi parle Adonaï YHWH : Que celui qui écoute, écoute, et que celui qui s’abstient, s’abstienne ! Car c'est une maison de la rébellion.

## Chapitre 4

### Signes annonciateurs du jugement de Yeroushalaim (Jérusalem)

4:1	Toi, fils d'humain, prends une brique et place-la devant toi, et traces-y la ville de Yeroushalaim.
4:2	Tu mettras le siège contre elle, tu bâtiras contre elle des retranchements, tu élèveras contre elle des tertres, tu mettras des camps contre elle, et tu mettras autour d'elle des béliers<!--Bélier pour casser une porte. 2 R. 25:1.--> tout autour.
4:3	Tu prendras aussi une plaque de fer, et tu la mettras comme un mur de fer entre toi et la ville, et tu dresseras tes faces contre elle. Elle sera assiégée, car tu l'assiégeras. Ce sera un signe pour la maison d'Israël.
4:4	Ensuite, couche-toi sur ton côté gauche, mets-y l'iniquité de la maison d'Israël. Le nombre de jours où tu te coucheras dessus, tu porteras leur iniquité.
4:5	Et moi, je te donne les années de leur iniquité pour le nombre des jours : 390 jours. Tu porteras ainsi l'iniquité de la maison d'Israël.
4:6	Et quand tu auras accompli ces jours-là, tu te coucheras une seconde fois sur ton côté droit, et tu porteras l'iniquité de la maison de Yéhouda pendant 40 jours. Je te donne un jour pour une année, un jour pour une année.
4:7	Tu tourneras tes faces et ton bras nu vers Yeroushalaim assiégée, et tu prophétiseras contre elle.
4:8	Et voici, j'ai mis sur toi des cordes, afin que tu ne te tournes pas d’un côté à ton autre côté, jusqu'à ce que tu aies accompli les jours de ton siège.

### Le pain impur

4:9	Et toi, prends du blé, de l'orge, des fèves, des lentilles, du millet et de l'épeautre : tu les mettras dans un vase, et tu t’en feras du pain pour le nombre des jours où tu seras couché sur ton côté. Tu en mangeras pendant 390 jours.
4:10	Le poids de la viande que tu mangeras sera de 20 sicles<!--200 grammes.--> par jour. Tu en mangeras de temps en temps.
4:11	Tu boiras l'eau avec mesure : un sixième de hin<!--Un litre.-->. Tu en boiras de temps en temps.
4:12	Et tu mangeras aussi des gâteaux d'orge, que tu feras cuire avec des excréments humains en leur présence.
4:13	YHWH dit : C'est ainsi que les fils d'Israël mangeront leur pain souillé, parmi les nations vers lesquelles je les bannirai<!--Os. 9:3 ; Da. 1:8.-->.
4:14	Et je dis : Ah ! Adonaï YHWH, voici, mon âme n'est pas impure. Depuis ma jeunesse jusqu'à présent, je n'ai jamais mangé un cadavre ou une bête déchirée, et aucune chair impure n'est entrée dans ma bouche<!--Lé. 17:15 ; De. 14:3 ; Ac. 10:14.-->.
4:15	Il me dit : Voici, je te donne des excréments de bœuf au lieu d'excréments humains, et tu feras cuire ton pain dessus.
4:16	Il me dit : Fils d'humain, voici, je vais rompre le bâton du pain dans Yeroushalaim. Ils mangeront leur pain au poids et avec anxiété, et ils boiront de l'eau par mesure et avec horreur<!--Lé. 26:26 ; Es. 3:1 ; Ps. 105:16 ; La. 5:4.-->.
4:17	Parce que le pain et l'eau leur manqueront, ils seront épouvantés, chaque homme et son frère, ils pourriront à cause de leur iniquité.

## Chapitre 5

### Les cheveux coupés et divisés en trois

5:1	Et toi, fils d'humain, prends un couteau tranchant, prends un rasoir de barbier, et fais-le passer sur ta tête et sur ta barbe. Prends ensuite une balance à peser et partage-les<!--Lé. 21:5 ; Ez. 44:20.-->.
5:2	Tu brûleras un tiers dans le feu, au milieu de la ville, lorsque les jours du siège seront accomplis. Tu prendras un tiers pour le frapper avec l'épée tout autour de la ville. Tu disperseras un tiers au vent, car je tirerai l'épée derrière eux<!--Lé. 26:25 ; La. 1:20.-->.
5:3	Tu prendras une petite quantité que tu serreras dans les pans de ton manteau.
5:4	De ceux-là tu prendras encore quelques-uns que tu jetteras au feu et que tu brûleras dans le feu. De là sortira un feu contre toute la maison d'Israël.
5:5	Ainsi parle Adonaï YHWH : C'est là cette Yeroushalaim que j'avais placée au milieu des nations et des terres qui sont autour d'elle.
5:6	Elle a changé mes ordonnances et s'est rendue plus coupable que les nations et les terres d'alentour. En effet, ils ont rejeté mes statuts et n'ont pas marché dans mes statuts.
5:7	C'est pourquoi ainsi parle Adonaï YHWH : Parce que vous avez murmuré plus que les nations qui vous entourent, que vous n'avez pas marché dans mes ordonnances, que vous n'avez pas agi d'après mes statuts et que vous n'avez pas agi selon les ordonnances des nations qui vous entourent,
5:8	à cause de cela, ainsi parle Adonaï YHWH : Voici, je m'oppose moi-même à toi ! J'exécuterai au milieu de toi des jugements, sous les yeux des nations.
5:9	Je te ferai, à cause de toutes tes abominations, des choses que je n'ai jamais faites et ce que je ne ferai jamais plus<!--Da. 9:12 ; Mt. 24:21.-->.
5:10	Des pères mangeront leurs fils au milieu de toi, et des fils mangeront leurs pères. J'exécuterai des jugements sur toi, et je disperserai à tous les vents tout ce qui restera de toi<!--Lé. 26:33 ; De. 28:64 ; Jé. 9:15 ; Za. 2:10.-->.
5:11	C'est pourquoi, je suis vivant ! – déclaration d'Adonaï YHWH –, parce que tu as rendu mon sanctuaire impur par toutes tes idoles et par toutes tes abominations, moi-même je te raserai et mon œil ne t'épargnera pas. Je n'aurai pas de compassion<!--Jé. 7:9-11.-->.
5:12	Un tiers d'entre vous mourra de la peste ou sera consumé par la famine au milieu de toi. Un tiers tombera par l'épée autour de toi, et je disperserai à tous les vents l'autre tiers, je tirerai l'épée derrière eux.
5:13	Car ma colère s'accomplira, je ferai reposer ma fureur sur eux et je me donnerai satisfaction. Ils sauront que moi, YHWH, j'ai parlé dans ma jalousie, quand j'aurai accompli ma fureur sur eux.
5:14	Je ferai de toi un lieu laissé en ruines, une insulte parmi les nations qui sont autour de toi, aux yeux de tous les passants<!--Lé. 26:31-32 ; Né. 2:17.-->.
5:15	Tu deviendras une insulte, un reproche, un châtiment et une horreur pour les nations qui t'entourent, quand j'aurai exécuté mes jugements sur toi, avec colère, avec fureur et avec des châtiments de fureur. Moi, YHWH, j'ai parlé<!--De. 28:37 ; 1 R. 9:7 ; Es. 26:9 ; Jé. 24:9 ; Ps. 79:4.-->.
5:16	Quand je lancerai sur eux les flèches mauvaises de la famine qui seront pour la destruction. Je les lancerai pour vous détruire : j'augmenterai sur vous la famine et je briserai pour vous le bâton du pain<!--De. 32:24.-->.
5:17	J'enverrai contre vous la famine et les bêtes féroces qui vous priveront d'enfants ; la peste et le sang passeront au milieu de vous et je ferai venir l'épée sur vous. Moi, YHWH, j'ai parlé.

## Chapitre 6

### Grâce de YHWH pour quelques rescapés d'Israël

6:1	La parole de YHWH vint à moi en disant :
6:2	Fils d'humain, mets tes faces contre les montagnes d'Israël et prophétise contre elles !
6:3	Et dis : Montagnes d'Israël, écoutez la parole d'Adonaï YHWH. Ainsi parle Adonaï YHWH aux montagnes et aux collines, aux ravins et aux vallées : Me voici, je vais faire venir l'épée sur vous, et je détruirai vos hauts lieux<!--Lé. 26:30.-->.
6:4	Vos autels seront dévastés, vos autels d'encens seront brisés et je ferai tomber vos blessés mortellement devant vos idoles.
6:5	Car je mettrai les cadavres des fils d'Israël devant leurs idoles et je disperserai vos os autour de vos autels<!--2 R. 23:14-20.-->.
6:6	Dans tous vos lieux d'habitation, les villes seront en ruine et les hauts lieux ruinés, afin que vos autels soient en ruine et abandonnés, que vos idoles soient brisées et disparaissent, que vos autels d'encens soient abattus et que vos ouvrages soient effacés.
6:7	Les blessés mortellement tomberont au milieu de vous et vous saurez que je suis YHWH.
6:8	Mais je laisserai un reste, pour que vous ayez des rescapés de l’épée parmi les nations, quand vous serez dispersés parmi les terres.
6:9	Vos rescapés se souviendront de moi<!--Jé. 51:50.--> parmi les nations où ils auront été emmenés captifs, quand j’aurai brisé leur cœur prostitué qui s’est détourné de moi, et leurs yeux qui se prostituent après leurs idoles. Ils se prendront eux-mêmes en dégoût, à cause du mal qu'ils ont commis, à cause de leurs abominations.
6:10	Ils sauront que je suis YHWH : que ce n'est pas en vain que j'ai parlé de leur faire ce mal.

### Sentence envers les idolâtres

6:11	Ainsi parle Adonaï YHWH : Frappe de ta paume et bats de ton pied, et dis : Hélas ! À cause de toutes les abominations, des maux de la maison d'Israël ! Car ils tomberont par l'épée, par la famine et par la peste.
6:12	Celui qui sera loin mourra de la peste, et celui qui sera près tombera par l'épée, et celui qui restera et sera assiégé, mourra par la famine. Ainsi j'accomplirai ma fureur sur eux<!--Am. 4:10.-->.
6:13	Vous saurez que je suis YHWH quand les blessés mortellement seront au milieu de leurs idoles, autour de leurs autels, sur toute colline élevée, sur tous les sommets des montagnes, sous tout arbre vert et sous tout chêne touffu, là où ils offraient des parfums apaisants à toutes leurs idoles<!--Os. 4:13.-->.
6:14	J'étendrai ma main sur eux et je ferai de leur terre une dévastation et une horreur depuis le désert jusqu'à Diblah dans tous leurs lieux d'habitation. Et ils sauront que je suis YHWH.

## Chapitre 7

### Attaque babylonienne imminente

7:1	La parole de YHWH vint à moi en disant :
7:2	Toi, fils d'humain, ainsi parle Adonaï YHWH au sol d'Israël : La fin, la fin vient sur les quatre extrémités de la Terre !
7:3	Maintenant la fin vient sur toi, j'enverrai sur toi ma colère, je te jugerai selon ta voie, et je mettrai sur toi toutes tes abominations<!--Ro. 2:6.-->.
7:4	Mon œil ne t'épargnera pas, et je n'aurai pas de compassion. Oui, je mettrai tes voies sur toi et tes abominations seront au milieu de toi. Vous saurez alors que je suis YHWH.
7:5	Ainsi parle Adonaï YHWH : Voici un mal, un seul mal qui vient !
7:6	La fin vient, la fin vient, elle se réveille contre toi ! La voici qui vient !
7:7	La couronne vient sur toi, habitant de la terre ! Le temps vient, le jour est proche ! C'est la confusion, il n'y a plus de cris de joie dans les montagnes<!--So. 1:14-15.--> !
7:8	Maintenant, je répandrai bientôt ma fureur sur toi, et j'accomplirai ma colère sur toi. Je te jugerai selon ta voie, je mettrai sur toi toutes tes abominations.
7:9	Mon œil ne t'épargnera pas et je n'aurai aucune compassion. Je mettrai tes voies sur toi et tes abominations seront au milieu de toi. Vous saurez alors que je suis YHWH qui frappe.
7:10	Voici le jour ! Voici il vient ! Ta couronne sort ! La verge fleurit ! L'orgueil bourgeonne !
7:11	La violence s'élève pour servir de verge à la méchanceté : il ne reste plus rien d'eux, de leur multitude et de leur abondance. Ils n'auront plus de distinction.
7:12	Le temps vient, le jour est tout proche : Que celui qui achète ne se réjouisse pas, et que celui qui vend ne se lamente pas, car il y a une ardente colère sur toute leur multitude.
7:13	Car le vendeur ne retournera pas à ce qu'il a vendu, même lorsqu’ils vivraient encore entre les vivants, car la vision concernant toute cette foule ne sera pas révoquée. Et l’homme qui vit dans son iniquité ne se fortifiera pas.
7:14	On sonne de la trompette, tout est prêt, mais il n'y a personne pour aller au combat, parce que l'ardeur de ma colère est sur toute leur multitude.
7:15	L'épée est au-dehors, la peste et la famine au-dedans ! Celui qui est aux champs mourra par l'épée et celui qui est dans la ville sera dévoré par la famine et par la peste.
7:16	Les rescapés s'enfuiront et seront sur les montagnes comme les pigeons des vallées, tous gémissants, chacun sur son iniquité.
7:17	Toutes les mains deviendront lâches, et tous les genoux se fondront en eau<!--Es. 13:7 ; Jé. 6:24.-->.
7:18	Ils se ceindront de sacs et le tremblement les couvrira. La confusion sera sur tous leurs visages et leurs têtes deviendront chauves<!--Es. 3:24 ; Jé. 48:37 ; Am. 8:10.-->.
7:19	Ils jetteront leur argent dans les rues et leur or deviendra une impureté. Leur argent et leur or ne pourront pas les délivrer le jour de la grande colère de YHWH<!--Pr. 11:4 ; So. 1:18.--> ; ils ne rassasieront pas leurs âmes, et ne rempliront pas leurs entrailles, car ce fut là la pierre d'achoppement de leur iniquité.

### Violation du temple

7:20	Ils mettaient leur orgueil dans la beauté de leurs ornements, ils en ont fait leurs images abominables, leurs idoles. C'est pourquoi j'en ai fait pour eux une impureté.
7:21	Je l'ai livrée en proie aux mains des étrangers, et pour butin aux méchants de la Terre qui la profaneront<!--Jé. 20:5.-->.
7:22	Je détournerai d'eux mes faces, on profanera mon lieu secret, des violents y entreront et le profaneront.
7:23	Fabrique des chaînes ! Car la terre est pleine de jugements sanguinaires, de meurtres, et la ville est pleine de violence.
7:24	Je ferai venir les plus méchantes des nations et elles prendront possession de leurs maisons. Je ferai cesser l'orgueil des puissants et leurs saints lieux seront profanés.
7:25	La destruction vient, et ils chercheront la paix, mais il n'y en aura pas.
7:26	Il viendra malheur sur malheur, et il y aura rumeur sur rumeur. Ils demanderont la vision aux prophètes<!--La. 2:9.-->, la torah périra chez le prêtre et le conseil chez les anciens.
7:27	Le roi se lamentera, les princes se vêtiront de désolation, et les mains du peuple de la terre seront terrifiées. Je les traiterai selon leur voie, je les jugerai d'après leur jugement, et ils sauront que je suis YHWH.

## Chapitre 8

### Visions d'Elohîm

8:1	Il arriva, la sixième année, le cinquième jour du sixième mois, comme j’étais assis dans ma maison et que les anciens de Yéhouda étaient assis devant moi, que la main d'Adonaï YHWH tomba là sur moi.
8:2	Je regardai et voici une ressemblance, comme un aspect de feu. Depuis l'aspect de ses reins, en bas, un feu, et depuis ses reins, en haut, comme un aspect brillant, comme l'œil d'ambre.
8:3	Il envoya une forme de main et me prit par les cheveux de ma tête. L'Esprit m'enleva entre la terre et les cieux et me fit venir à Yeroushalaim, dans des visions d'Elohîm, à l'entrée de la porte intérieure, du côté nord, là où se trouve le siège de l'idole de jalousie<!--L'idole de la jalousie : dans le temple de Yeroushalaim (Jérusalem) à l'époque de Yehezkel, l'idolâtrie s'y développait sans retenue (2 R. 21-23). Il y avait dans ce temple les idoles d'Astarté et les autels de Baal. Le temple était souillé.--> qui provoque la jalousie.
8:4	Voici, la gloire de l'Elohîm d'Israël était là, telle que je l'avais vue en vision dans la vallée.

### Abominations dans le temple

8:5	Il me dit : Fils d'humain, lève maintenant tes yeux vers le chemin du nord ! J'élevai mes yeux vers le chemin du nord, et voici, du côté nord, à la porte de l'autel, à l'entrée, cette idole de jalousie.
8:6	Il me dit : Fils d'humain, vois-tu ce qu'ils font ? Les grandes abominations que la maison d'Israël commet ici pour que je m'éloigne de mon sanctuaire ? Tu retourneras encore et tu verras de grandes abominations.
8:7	Il me fit venir à l'entrée du parvis. Je regardai, et voici un trou dans le mur.
8:8	Il me dit : Fils d'humain, s’il te plaît, creuse le mur ! Je creusai le mur, et voici une entrée.
8:9	Il me dit : Entre et regarde les méchantes abominations qu'ils commettent ici !
8:10	J'entrai, je regardai, et voici toutes sortes de figures de reptiles et de bêtes abominables, et toutes les idoles de la maison d'Israël, gravées sur le mur, autour, autour<!--Ex. 20:4 ; De. 4:16-18 ; Ro. 1:23.-->.
8:11	70 hommes des anciens de la maison d'Israël, au milieu desquels était Ya`azanyah, fils de Shaphan, se tenaient debout devant ces idoles. Chaque homme avait l'encensoir à la main, d'où s'élevait une épaisse nuée d'encens.
8:12	Il me dit : Fils d'humain, vois-tu ce que les anciens de la maison d'Israël font dans les ténèbres, chaque homme dans sa chambre pleine de figures ? Car ils disent : YHWH ne nous voit pas, YHWH a abandonné la terre<!--Es. 29:15.-->.
8:13	Il me dit : Retourne encore, tu verras les grandes abominations qu'ils commettent.
8:14	Il me fit venir à l'entrée de la porte de la maison de YHWH, vers le nord, et voici que les femmes y étaient assises, pleurant Tammouz<!--Tammouz ou Adonis. Faux elohîm assyro-babylonien du Printemps et de la Fertilité.-->.
8:15	Il me dit : As-tu vu, fils d'humain ? Retourne encore, tu verras des abominations plus grandes que celles-ci.
8:16	Il me fit entrer au parvis intérieur de la maison de YHWH. Et voici, à l'entrée du temple de YHWH, entre le portique et l'autel, environ 25 hommes, tournant le dos au temple de YHWH et les faces vers l'orient. Ils se prosternaient vers l'orient, devant le soleil<!--De. 4:19.-->.
8:17	Il me dit : As-tu vu, fils d'humain ? Est-ce une chose légère à la maison de Yéhouda de commettre ces abominations qu'ils commettent ici ? Car ils remplissent la terre de violence et ils reviennent pour m'irriter. Les voilà maintenant qui approchent une branche de leur nez.
8:18	Et moi, j'agirai dans ma fureur. Mon œil n'épargnera pas, je n'aurai aucune compassion. Quand ils crieront à grande voix à mes oreilles, je ne les écouterai pas<!--Es. 1:15 ; Jé. 11:11 ; Pr. 1:28 ; Mi. 3:4 ; Za. 7:13.-->.

## Chapitre 9

### Marque de YHWH sur les justes ; extermination des impies

9:1	Il cria à mes oreilles à grande voix en disant : Faites approcher ceux qui châtient la ville, chaque homme avec son instrument de destruction à la main !
9:2	Et voici que six hommes venaient par le chemin de la porte supérieure qui fait face au nord. Chaque homme avait dans sa main son instrument de destruction. Il y avait au milieu d'eux, un homme vêtu de lin, avec une écritoire de scribe à ses reins. Ils entrèrent et se tinrent debout près de l'autel de cuivre.
9:3	La gloire de l'Elohîm d'Israël s'éleva du chérubin sur lequel elle était, vers le seuil de la maison. Il cria à l'homme qui était vêtu de lin et qui avait une écritoire de scribe sur ses reins.
9:4	YHWH lui dit : Passe par le milieu de la ville, par le milieu de Yeroushalaim et marque la lettre Tav sur les fronts des hommes qui gémissent et qui soupirent à cause de toutes les abominations qui s'y commettent<!--Ex. 12:7-23 ; Ap. 7:3, 9:4, 13:16-17, 20:4.-->.
9:5	À ceux-là, il dit à mes oreilles : Passez dans la ville après lui et frappez ! Que votre œil soit sans pitié et n'ayez aucune compassion !
9:6	Vieillards, jeunes hommes, vierges, enfants et femmes, tuez-les tous jusqu'à la destruction<!--2 Ch. 36:17.-->, mais n'approchez pas de ceux qui ont la lettre Tav<!--La lettre Tav symbolise le sceau d'Elohîm. Selon la Bible, la marque des chrétiens est représentée par : le Saint-Esprit, le nom de Yéhoshoua ha Mashiah (Jésus-Christ) (Ep. 1:13-14, 4:30 ; Ap. 14:1), le nom de la nouvelle Yeroushalaim (Jérusalem) (Ap. 3:12) et le nom du Père. Les chrétiens fidèles à Elohîm sont marqués par l'Esprit d'Elohîm qui est le sceau envoyé par Elohîm.-->, et commencez par mon sanctuaire<!--Le jugement commencera par la maison d'Elohîm (1 Pi. 4:17-18).-->. Ils commencèrent par les hommes, les vieillards qui étaient devant la maison.
9:7	Il leur dit : Profanez la maison et remplissez de blessés mortellement les parvis ! Sortez ! Ils sortirent et frappèrent dans la ville.
9:8	Il arriva que comme ils frappaient, je restai là, je tombai sur mes faces et je dis en criant : Ah ! Adonaï YHWH ! Vas-tu détruire tous les restes d'Israël en répandant ta fureur sur Yeroushalaim ?
9:9	Il me dit : L'iniquité de la maison d'Israël et de Yéhouda est très, très grande. La terre est remplie de sang et la ville remplie de perversions, car ils disent : YHWH a abandonné la terre, YHWH ne voit pas.
9:10	Quant à moi, mon œil sera sans pitié et je n'aurai aucune compassion. Je mettrai leur voie sur leur tête.
9:11	Et voici, l'homme vêtu de lin et portant une écritoire à ses reins retourna faire son rapport en disant : J'ai fait tout ce que tu m'as ordonné.

## Chapitre 10

### La gloire de YHWH quitte le temple

10:1	Je regardai, et voici, sur le firmament au-dessus de la tête des chérubins, comme une pierre de saphir, comme l’aspect d’une ressemblance de trône qui se voyait au-dessus d’eux.
10:2	On parla à l'homme vêtu de lin, et on lui dit : Va entre les roues, sous les chérubins et remplis tes poignées de charbons de feu d’entre les chérubins, et jette-les sur la ville<!--Es. 6:6 ; Ap. 8:5.--> ! Il y entra devant mes yeux.
10:3	Les chérubins se tenaient debout à la droite de la maison quand l'homme entra, et une nuée remplit le parvis intérieur<!--1 R. 8:10-11.-->.
10:4	La gloire de YHWH s'éleva de dessus les chérubins pour venir sur le seuil de la maison. La maison fut remplie d'une nuée et le parvis fut rempli de la splendeur de la gloire de YHWH.
10:5	La voix des ailes des chérubins se fit entendre jusqu'au parvis extérieur, pareil à la voix de El Shaddaï lorsqu'il parle.
10:6	Et il arriva que quand il eut donné cet ordre à l'homme qui était habillé de lin en disant : Prends du feu entre les roues et entre les chérubins, il y alla et se tint debout auprès des roues.
10:7	Un chérubin étendit sa main entre les chérubins, vers le feu qui était entre les chérubins. Il le prit et le mit entre les poignées de l'homme habillé de lin. Celui-ci le prit et sortit.
10:8	On voyait aux chérubins une forme de main d'humain sous leurs ailes.
10:9	Je regardai et voici quatre roues près des chérubins, une roue à côté d'un chérubin et une roue à côté d'un autre chérubin. L'aspect des roues était comme l'œil de la pierre de chrysolithe.
10:10	Par leur aspect, toutes les quatre avaient une même ressemblance, comme si une roue était au milieu d’une roue.
10:11	Lorsqu'elles allaient, elles allaient de leurs quatre côtés, et elles ne se tournaient pas dans leur marche ; mais elles allaient dans la direction de la tête, sans se tourner dans leur marche.
10:12	Toute leur chair, leur dos, leurs mains, leurs ailes et les roues étaient pleins d’yeux tout autour, leurs quatre roues<!--Ap. 4:6-8.-->.
10:13	J'entendis qu'on appela les roues tourbillon.
10:14	Chacun avait quatre faces. Les faces de l’un étaient les faces de chérubin, les faces du second des faces d'humain, pour le troisième des faces de lion, et pour le quatrième des faces d'aigle<!--Ez. 1 ; Ap. 4:7.-->.
10:15	Les chérubins s'élevèrent - c'étaient les vivants que j'avais vus près du fleuve Kebar<!--Ez. 1:1.-->.
10:16	Quand les chérubins allaient, les roues aussi allaient à côté d'eux. Quand les chérubins levaient leurs ailes pour s'élever de terre, les roues ne se détournaient pas d'eux.
10:17	Quand ils s'arrêtaient, elles s'arrêtaient, et quand ils s'élevaient, elles s'élevaient, car l'Esprit des vivants était dans les roues.
10:18	La gloire de YHWH se retira de dessus le seuil de la maison, et se tint au-dessus des chérubins.
10:19	Les chérubins levèrent leurs ailes et s'élevèrent de terre sous mes yeux quand ils sortirent avec les roues. Ils s'arrêtèrent à l'entrée de la porte orientale de la maison de YHWH, et la gloire de l'Elohîm d'Israël était sur eux en haut.
10:20	C'étaient les vivants que j'avais vus sous l'Elohîm d'Israël près du fleuve Kebar, et j’ai su que c'étaient des chérubins.
10:21	Chacun avait quatre faces et chacun quatre ailes, avec une ressemblance de mains d’humain sous leurs ailes.
10:22	Quant à la ressemblance de leurs faces, c'étaient les faces que j'avais vues près du fleuve Kebar, c'était leur aspect, c'étaient eux. Chaque homme allait au-delà de ses faces.

## Chapitre 11

### Sentences sur les princes infidèles

11:1	L'Esprit m'enleva et me fit venir à la porte orientale de la maison de YHWH, à celle qui fait face à l'orient. Et voici, à l'entrée de la porte, 25 hommes. Je vis au milieu d'eux Ya`azanyah, fils d'Azzour, et Pelatyah, fils de Benayah, les princes du peuple.
11:2	Il me dit : Fils d'humain, ce sont les hommes qui ont des pensées d'iniquité, et qui donnent un mauvais conseil dans cette ville<!--Mi. 2:1.-->.
11:3	Ils disent : Ce n'est pas le moment ! Bâtissons des maisons ! La ville est la chaudière et nous, la chair.
11:4	C'est pourquoi prophétise contre eux, prophétise, fils d'humain !
11:5	L'Esprit de YHWH tomba sur moi. Et il me dit : Ainsi parle YHWH : Vous parlez de la sorte, maison d'Israël, et je connais toutes les pensées de votre esprit.
11:6	Vous avez multiplié les blessés mortellement dans cette ville, et vous avez rempli ses rues avec les blessés mortellement.
11:7	C'est pourquoi, ainsi parle Adonaï YHWH : Les blessés mortellement que vous avez mis au milieu d'elle, c'est la chair, et elle, c'est la chaudière, mais je vous ferai sortir du milieu d'elle<!--Mi. 3:3.-->.
11:8	Vous avez eu peur de l'épée, mais je ferai venir l'épée sur vous, – déclaration d'Adonaï YHWH<!--Jé. 42:16.-->.
11:9	Je vous ferai sortir du milieu d’elle, je vous livrerai entre les mains des étrangers, et j'exécuterai mes jugements contre vous.
11:10	Vous tomberez par l'épée et je vous jugerai en terre d'Israël. Vous saurez que je suis YHWH.
11:11	Elle ne sera pas une chaudière pour vous, et vous ne serez pas la chair au milieu d'elle : c'est à la frontière d'Israël que je vous jugerai.
11:12	Et vous saurez que je suis YHWH, car vous n’avez pas marché dans mes ordonnances, et vous n'avez pas observé mes lois, mais vous avez agi selon les ordonnances des nations qui sont autour de vous.
11:13	Or il arriva comme je prophétisais, que Pelatyah, fils de Benayah, mourut. Je tombai sur mes faces et je criai à grande voix, et dis : Ah ! Adonaï YHWH ! Veux-tu anéantir le reste d'Israël ?

### Restauration d'Israël et de ses exilés

11:14	La parole de YHWH vint à moi, en disant :
11:15	Fils d'humain, tes frères, tes frères, les hommes de ta parenté, et la maison d'Israël tout entière, à qui les habitants de Yeroushalaim ont dit : Éloignez-vous de YHWH ! C'est à nous que la terre a été donnée en possession.
11:16	C'est pourquoi dis-leur : Ainsi parle Adonaï YHWH : Oui, je les ai éloignés des nations, oui, je les ai dispersés sur les terres, mais je deviendrai un peu pour eux un sanctuaire sur les terres où ils sont venus.
11:17	C'est pourquoi dis-leur : Ainsi parle Adonaï YHWH : Je vous rassemblerai du milieu des peuples, je vous recueillerai des terres où vous avez été dispersés et je vous donnerai le sol d'Israël<!--Es. 11:11-16 ; Jé. 24:6 ; Ez. 28:25, 34:13, 36:24.-->.
11:18	C'est là qu'ils iront, ils ôteront hors d'elle toutes ses idoles et toutes ses abominations.
11:19	Je leur donnerai un même cœur et je mettrai en eux un esprit nouveau. J'ôterai de leur corps le cœur de pierre et je leur donnerai un cœur de chair<!--Il s'agit d'une allusion à la nouvelle alliance (Jé. 31:31-34 ; Hé. 8).-->,
11:20	afin qu’ils marchent dans mes statuts, qu’ils gardent mes ordonnances et les pratiquent. Ils deviendront mon peuple, et moi, je deviendrai leur Elohîm.
11:21	Quant à ceux dont le cœur suit le cœur de leurs idoles et de leurs abominations, je mettrai leur voie sur leur tête, – déclaration d'Adonaï YHWH.

### La gloire d'Elohîm en mouvement vers le Mont des Oliviers<!--Ez. 43:1-4.-->

11:22	Les chérubins élevèrent leurs ailes, et les roues étaient vis-à-vis d'eux. La gloire de l'Elohîm d'Israël était sur eux, en haut.
11:23	La gloire de YHWH monta d’au-dessus du milieu de la ville<!--Le départ de la gloire d'Elohîm du temple de Yeroushalaim (Jérusalem) marque la fin de la théocratie (règne d'Elohîm) en Israël. Cet événement, comparable au retrait de l'Esprit d'Elohîm en Ge. 6:3, fut consécutif à la décadence morale d'Israël (voir Ez. 8) qui fut désormais livré aux nations. Certains estiment que la théocratie a cessé au moment où les Israélites ont demandé un roi (voir 1 S. 8). Or bien que cette demande déplut à YHWH, il continua néanmoins à diriger Israël au travers des souverains tels que David, qu'il établissait à la tête de son peuple. Les Hébreux avaient déjà reçu un sérieux avertissement avec la destruction du temple lors de la première déportation babylonienne (2 R. 24). Cet événement, bien que traumatisant pour beaucoup, n'avait cependant pas provoqué une réelle repentance, c'est pourquoi les Israélites retombèrent rapidement dans leurs travers. Ainsi, comme en témoigne Mal. 2:17 qui rapporte les propos de certains Juifs : « Où est l'Elohîm du jugement ? » ; en dépit de la reconstruction du temple sous Nehemyah (Néhémie) et Ezra (Esdras), la gloire d'Elohîm ne s'y manifestait plus depuis longtemps. Yehezkel ne fait donc qu'assister à la conséquence de plusieurs siècles d'infidélité des Juifs à l'égard de leur Elohîm.--> et se tint au-dessus de la montagne qui est à l'orient de la ville.
11:24	L'Esprit m'enleva et me fit venir en Chaldée, auprès des exilés, dans la vision, par l'Esprit d'Elohîm. Et au-dessus de moi monta la vision que j’avais vue.
11:25	Je dis aux exilés toutes les paroles que YHWH m'avait révélées.

## Chapitre 12

### Fuite de Yehezkel, un signe pour Israël

12:1	La parole de YHWH vint à moi en disant :
12:2	Fils d'humain, tu habites au milieu de la maison de la rébellion ! Eux qui ont des yeux pour voir, ils n’ont pas vu, des oreilles pour entendre, ils n’ont pas entendu. Oui, ils sont la maison de la rébellion<!--Es. 6:9, 49:19-20 ; Jé. 5:21 ; Ac. 28:26.-->.
12:3	Toi, fils d'humain, fais-toi des bagages d'un homme qui s'exile et pars en exil de jour, sous leurs yeux. Pars en exil de ton lieu pour aller dans un autre lieu, sous leurs yeux. Peut-être verront-ils, oui, ils sont la maison de la rébellion.
12:4	Tu mettras dehors pendant le jour tes bagages comme les bagages d'un homme qui s'exile, sous leurs yeux, et le soir, tu sortiras sous leurs yeux, comme quand on sort pour s'exiler.
12:5	Sous leurs yeux, perce un trou dans le mur, par où tu sortiras.
12:6	Tu les porteras sur tes épaules, sous leurs yeux, et tu sortiras tes bagages pendant l'obscurité. Tu couvriras tes faces, afin que tu ne voies pas la terre, car je t’ai donné pour signe à la maison d'Israël.
12:7	Je fis ce qui m'avait été ordonné : je portai dehors pendant le jour mes bagages comme des bagages d'exil. Le soir je perçai le mur avec la main, je les sortis pendant l'obscurité, je les portai sur l'épaule, sous leurs yeux.
12:8	Au matin, la parole de YHWH vint à moi en disant :
12:9	Fils d'humain, la maison d'Israël, la maison de la rébellion, ne t'a-t-elle pas dit : Qu'est-ce que tu fais ?
12:10	Dis-leur : Ainsi parle Adonaï YHWH : Ce fardeau dont je suis chargé s'adresse au prince qui est à Yeroushalaim, et à toute la maison d'Israël qui s'y trouve.
12:11	Dis : Je suis pour vous un signe. Ce que j'ai fait, c'est ce qui leur sera fait : ils iront en exil, en captivité.
12:12	Et le prince qui est au milieu d'eux chargera son épaule dans l'obscurité et sortira. On percera le mur pour le faire sortir par là. Il se couvrira le visage afin que ses yeux ne voient pas la terre<!--2 R. 25:4.-->.
12:13	J'étendrai mon rets sur lui, et il sera pris dans mes filets. Je le ferai entrer dans Babel<!--Babylone.-->, en terre des Chaldéens, mais il ne la verra pas, et il y mourra.
12:14	Je disperserai à tout vent tout ce qui est autour de lui, son secours et tous ses corps d'armées et je tirerai l'épée sur eux.
12:15	Ils sauront que je suis YHWH, quand je les aurai répandus parmi les nations et que je les aurai dispersés sur diverses terres.
12:16	Je laisserai d’eux un nombre d’hommes échappés à l’épée, à la famine, à la peste, pour qu’ils racontent toutes leurs abominations parmi les nations où ils iront. Ils sauront que je suis YHWH.

### La captivité du peuple imminente<!--Cp. 2 R. 25:1-10.-->

12:17	La parole de YHWH vint à moi en disant :
12:18	Fils d'humain, mange ton pain dans l'agitation, et bois ton eau en tremblant et avec anxiété.
12:19	Tu diras au peuple du sol : Ainsi parle Adonaï YHWH, sur les habitants de Yeroushalaim, à la terre d'Israël : Ils mangeront leur pain avec anxiété, et ils boiront leur eau avec horreur, parce que sa terre sera ruinée, privée de sa plénitude, à cause de la violence de tous ceux qui y habitent.
12:20	Les villes peuplées seront désertes et la terre ne sera que désolation. Vous saurez que je suis YHWH.
12:21	La parole de YHWH vint à moi en disant :
12:22	Fils d'humain, que signifie ce proverbe que vous tenez sur le sol d'Israël en disant : Les jours se prolongent et toute vision s'évanouit<!--Es. 5:19 ; Am. 6:3 ; 2 Pi. 3:3-4.--> ?
12:23	C'est pourquoi dis-leur : Ainsi parle Adonaï YHWH : Je ferai cesser ce proverbe, et on ne s'en servira plus comme proverbe en Israël. Mais dis-leur : Les jours approchent, ainsi que la parole de toute vision.
12:24	Car il n'y aura plus de vision vaine ni de divination flatteuse au milieu de la maison d'Israël.
12:25	Oui, moi, YHWH, je parlerai, et la parole que j'aurai prononcée s'accomplira, elle ne sera plus différée. Oui, de vos jours, maison de la rébellion, je prononcerai une parole et je l'accomplirai, – déclaration d'Adonaï YHWH.
12:26	La parole de YHWH vint à moi en disant :
12:27	Fils d'humain, voici ce que dit la maison d'Israël : La vision qu'il voit est pour des jours lointains, et il prophétise pour des temps éloignés.
12:28	C'est pourquoi dis-leur : Ainsi parle Adonaï YHWH : Aucune de mes paroles ne traînera plus. La parole que je prononcerai s'accomplira, – déclaration d'Adonaï YHWH.

## Chapitre 13

### Jugement sur ceux qui égarent le peuple d'Elohîm

13:1	La parole de YHWH vint à moi en disant :
13:2	Fils d'humain, prophétise contre les prophètes d'Israël qui prophétisent, et dis à ces prophètes qui prophétisent selon leur propre cœur : Écoutez la parole de YHWH !
13:3	Ainsi parle Adonaï YHWH : Malheur aux prophètes insensés vont derrière leur esprit sans rien voir !
13:4	Israël, tes prophètes ont été comme des renards dans les lieux laissés en ruine.
13:5	Vous n'êtes pas montés devant les brèches, et vous n'avez pas réparé les murs pour la maison d'Israël, afin de vous tenir debout pour le combat au jour de YHWH.
13:6	Ils ont eu des visions vaines et des divinations de mensonge. Ils disent : Déclaration de YHWH ! Or YHWH ne les a pas envoyés, et ils font espérer que leur parole s'accomplira<!--Les faux prophètes : Jé. 23, 14:14, 28:15.-->.
13:7	N'avez-vous pas vu des visions de vanité, et prononcé des divinations de mensonge ? Cependant vous dites : Déclaration de YHWH ! Or je n'ai pas parlé.
13:8	C'est pourquoi ainsi parle Adonaï YHWH : Parce que vous avez prononcé des choses vaines, et que vous avez eu des visions de mensonge, à cause de cela, je m'oppose à vous ! – déclaration d'Adonaï YHWH.
13:9	Ma main sera sur les prophètes qui ont des visions de vanité et des divinations de mensonge. Ils ne seront plus dans le conseil de mon peuple, ils ne seront plus écrits dans les registres de la maison d'Israël, ils ne viendront plus vers le sol d'Israël. Vous saurez que je suis Adonaï YHWH.
13:10	Parce que, oui, parce qu'ils égarent mon peuple en disant : « Paix ! » alors qu'il n'y a pas de paix<!--Jé. 6:14, 8:11.-->, et que, si celui-ci bâtit une muraille, ils la recouvrent de la chaux blanche, 
13:11	dis à ceux qui la recouvrent de la chaux blanche qu'elle tombera ! Il y aura une pluie débordante, et vous, pierres de grêle, vous tomberez, et un vent de tempête la fendra.
13:12	Et voici, le mur est tombé ! Ne vous dira-t-on pas : Où est l’enduit dont vous l'avez recouvert ?
13:13	C'est pourquoi, ainsi parle Adonaï YHWH : Je ferai éclater un vent de tempête dans ma fureur, et dans ma colère, il surviendra une pluie débordante, des pierres de grêle dans la fureur, pour détruire entièrement.
13:14	Je renverserai le mur que vous avez recouvert de la chaux blanche, je lui ferai toucher la terre et ses fondements seront découverts. Il tombera et vous serez consumés là au milieu. Vous saurez que je suis YHWH.
13:15	J'accomplirai ma colère contre le mur et contre ceux qui l'ont recouvert de la chaux blanche et je vous dirai : Le mur n’est plus, ni ceux qui le replâtraient !
13:16	Les prophètes d'Israël qui prophétisent sur Yeroushalaim, et qui voient pour elle des visions de paix alors qu'il n'y a pas de paix, – déclaration d'Adonaï YHWH.
13:17	Toi, fils d'humain, mets tes faces contre les filles de ton peuple qui prophétisent selon leur propre cœur, prophétise contre elles !
13:18	Dis : Ainsi parle Adonaï YHWH : Malheur à celles qui cousent des faux phylactères<!--Le mot « faux phylactères » vient du terme hébreu « keceth », et signifie « bande », « filet », « faux phylactères ». Il s'agissait d'un tissu utilisé par les fausses prophétesses en Israël dans le but de se faire passer pour de vrais servantes de YHWH, et ainsi tromper le peuple.--> pour toutes les jointures des mains, et qui font des voiles sur la tête des personnes de toute taille, pour faire la chasse aux âmes ! Vous feriez la chasse aux âmes de mon peuple, et vos âmes, à vous, vivraient !
13:19	Vous me profanez devant mon peuple pour des poignées d'orge et pour des morceaux de pain, en tuant les âmes qui ne devraient pas mourir et en faisant vivre les âmes qui ne devraient pas vivre, en mentant à mon peuple qui écoute le mensonge.
13:20	C'est pourquoi ainsi parle Adonaï YHWH : Voici, je m'oppose à vos faux phylactères, par lesquels vous faites la chasse aux âmes. Je les ferai s'envoler en les arrachant de vos bras et je ferai échapper les âmes auxquelles vous donnez la chasse, les âmes, afin qu’elles s’envolent<!--1 Co. 6:10 ; 2 Pi. 2:14 ; Ap. 18:11-13.-->.
13:21	Je déchirerai aussi vos voiles, et je délivrerai mon peuple d'entre vos mains : ils ne seront plus entre vos mains pour en faire votre proie. Vous saurez que je suis YHWH.
13:22	Parce que vous découragez le cœur du juste par le mensonge, quand moi-même je ne lui ai pas causé de douleur, et parce que vous fortifiez les mains du méchant afin qu’il ne revienne pas de sa mauvaise voie pour vivre.
13:23	C'est pourquoi vous n'aurez plus aucune vision de vanité ni aucune divination, mais je délivrerai mon peuple d'entre vos mains. Vous saurez que je suis YHWH.

## Chapitre 14

### Jugement sur les anciens d'Israël

14:1	Quelques-uns des anciens d'Israël vinrent auprès de moi et s'assirent devant moi.
14:2	La parole de YHWH vint à moi en disant :
14:3	Fils d'humain, ces hommes élèvent leurs idoles dans leurs cœurs, et ils attachent les regards sur ce qui les fait tomber dans l'iniquité. Me laisserai-je consulter, consulter par eux ?
14:4	C'est pourquoi parle-leur et dis-leur : Ainsi parle Adonaï YHWH. Un homme, un homme de la maison d'Israël qui élève ses idoles dans son cœur et qui place devant ses faces la pierre d'achoppement de son iniquité, s'il vient vers le prophète, je suis YHWH, je lui répondrai moi-même à cause de la multitude de ses idoles.
14:5	Ainsi sera saisie la maison d'Israël par son cœur, eux qui se sont éloignés de moi par leurs idoles.
14:6	C'est pourquoi dis à la maison d'Israël : Ainsi parle Adonaï YHWH : Revenez, et détournez-vous de vos idoles, détournez vos faces de toutes vos abominations<!--Es. 55:6-7.-->.
14:7	Car un homme, un homme de la maison d'Israël ou tout étranger en séjour en Israël qui se sépare de moi, qui élève ses idoles dans son cœur et qui place la pierre d'achoppement de son iniquité devant ses faces, s'il vient vers le prophète pour me consulter par lui, je suis YHWH, je lui répondrai moi-même.
14:8	Je mettrai mes faces contre cet homme<!--Lé. 17:10, 20:3-6 ; Jé. 44:11.-->, et je ferai de lui un signe et un proverbe<!--No. 26:10 ; De. 28:37.-->. Je le retrancherai du milieu de mon peuple. Vous saurez que je suis YHWH.
14:9	S'il arrive que le prophète soit séduit et qu'il profère quelque parole, moi, YHWH, je séduirai ce prophète-là<!--1 R. 22:23 ; Job 12:16 ; 2 Th. 2:11.-->. J'étendrai ma main sur lui, et je l'exterminerai du milieu de mon peuple d'Israël.
14:10	Et ils porteront leur iniquité. L'iniquité du prophète sera comme l'iniquité de celui qui l'aura consulté,
14:11	afin que la maison d'Israël ne s'égare plus loin de moi, et qu’elle ne se souille plus dans toutes ses transgressions<!--Jé. 31:18-19 ; Hé. 12:11 ; Ja. 1:1-3.-->. Ils deviendront mon peuple, et moi, je deviendrai leur Elohîm, – déclaration d'Adonaï YHWH.

### Châtiments d'Israël ; YHWH épargne un reste

14:12	La parole de YHWH vint à moi en disant :
14:13	Fils d'humain, si une terre pèche contre moi en commettant un délit, une transgression, et si j'étends ma main sur elle en brisant pour elle le bâton du pain, et si je lui envoie la famine, en exterminant les humains et les bêtes,
14:14	s'il s'y trouve ces trois hommes : Noah<!--Ge. 6:8.-->, Daniye'l<!--Da. 1:8-12.--> et Iyov<!--Job 1:8.-->, ils sauveraient leurs âmes par leur justice, – déclaration d'Adonaï YHWH.
14:15	Si je fais passer les bêtes féroces par cette terre-là et qu'elles la privent d'enfants, si elle devenait un désert où personne ne passe face aux bêtes,
14:16	et que ces trois hommes-là s'y trouvent, moi, le Vivant, – déclaration d'Adonaï YHWH –, ils ne sauveraient ni fils ni filles, eux seulement seraient sauvés, et la terre deviendrait un désert.
14:17	Si je faisais venir l'épée sur cette terre-là et si je disais : Que l'épée passe par la terre et qu'elle en retranche les humains et les bêtes !
14:18	Si ces trois hommes-là se trouvent au milieu de la terre, moi, le Vivant, – déclaration d'Adonaï YHWH –, ils ne sauveraient ni fils ni filles, mais eux seulement seraient sauvés.
14:19	Ou si j'envoyais la peste sur cette terre et que je répandais ma colère contre elle jusqu'à faire ruisseler le sang, au point de retrancher du milieu d'elle les humains et les bêtes,
14:20	et que Noah, Daniye'l et Iyov, s'y trouvent, moi, le Vivant, – déclaration d'Adonaï YHWH –, ils ne sauveraient ni fils ni filles, ils sauveraient leurs âmes par leur justice.
14:21	Car ainsi parle Adonaï YHWH : Même si j'envoie contre Yeroushalaim mes quatre terribles jugements, l'épée, la famine, les bêtes féroces et la peste, pour en retrancher les humains et les bêtes<!--Jé. 15:2-3.-->,
14:22	voici, il y aura néanmoins un reste qui échappera, qui fera sortir de la ville des fils et des filles. Voici, ils viendront vers vous et vous verrez leur voie et leurs actions. Vous serez consolés du malheur que je fais venir contre Yeroushalaim, et de tout ce que j'aurais fait venir contre elle.
14:23	Vous serez consolés, lorsque vous verrez leur voie et leurs actions, et vous saurez que ce n'est pas sans cause que je fais tout ce que je lui fais, – déclaration d'Adonaï YHWH<!--Jé. 22:8-9.-->.

## Chapitre 15

### Infidélité d'Israël<!--Cp. Es. 5:1-24.-->

15:1	La parole de YHWH vint à moi en disant :
15:2	Fils d'humain, qu’adviendra-t-il du bois de la vigne parmi tous les bois, du sarment qui est parmi les arbres de la forêt ?
15:3	Prendra-t-on de ce bois pour en faire quelque ouvrage ? Ou prendra-t-on un clou pour y pendre quelque chose ?
15:4	Voici, on le met au feu pour être consumé. Le feu en dévore les deux bouts et le milieu brûle. Serait-il bon pour un ouvrage ?
15:5	Voici, quand il est entier, on n'en fait aucun ouvrage, à plus forte raison quand le feu l'aura consumé et qu'il sera brûlé, sera-t-il bon pour un ouvrage ?
15:6	C'est pourquoi ainsi parle Adonaï YHWH : Comme le bois de la vigne est parmi les arbres d'une forêt, que j'ai livré au feu pour être consumé, ainsi je livrerai les habitants de Yeroushalaim.
15:7	Je mettrai mes faces contre eux. Ils sont sortis du feu, mais le feu les dévorera. Ainsi vous saurez que je suis YHWH, quand je mettrai ma face contre eux.
15:8	Je ferai de cette terre une désolation, parce qu'ils ont commis un délit, une transgression, – déclaration d'Adonaï YHWH.

## Chapitre 16

### Bonté de YHWH ; prostitutions d'Israël

16:1	La parole de YHWH vint à moi en disant :
16:2	Fils d'humain, fais connaître à Yeroushalaim ses abominations !
16:3	Et dis : Ainsi parle Adonaï YHWH à Yeroushalaim : Par ton origine et ta naissance, tu es de la terre de Kena'ân<!--Canaan.-->. Ton père était Amoréen, et ta mère une Héthienne.
16:4	Quant à ta naissance, le jour où tu as été enfantée, ton cordon ombilical n'a pas été coupé, tu n'as pas été lavée dans l'eau pour être nettoyée. De sel tu n'as pas été salée, et d'enveloppe tu n'as pas été enveloppée.
16:5	Aucun œil n'avait pitié de toi pour te faire une seule de ces choses, par compassion pour toi. Au contraire, tu as été jetée sur les faces des champs le jour où tu as été enfantée, par dégoût pour ton âme.
16:6	Et passant près de toi, je te vis gisante par terre, dans ton sang, et je te dis : Vis dans ton sang ! Je te dis : Vis dans ton sang !
16:7	Myriade, je t’avais donnée comme les pousses des champs. Tu te multiplias et tu devins grande, tu entrais avec le plus beau des ornements. Tes seins se formèrent, du poil te poussa, mais tu étais nue, entièrement nue.
16:8	Je passai près de toi, je te regardai, et voici, le temps était là, le temps des amours. J'étendis sur toi le pan de ma robe, et je couvris ta nudité. Je te jurai, j'entrai en alliance avec toi, – déclaration d'Adonaï YHWH –, et tu devins mienne.
16:9	Je te lavai dans l'eau en t'y plongeant, j'ôtai le sang de dessus toi, et je t'oignis d'huile.
16:10	Je te revêtis de vêtements brodés, je te chaussai de peaux de taisson, je te ceignis de fin lin et je te couvris de soie.
16:11	Je te parai d'ornements, je mis des bracelets sur tes mains, et un collier à ton cou.
16:12	Je mis un anneau à ton nez, des pendants à tes oreilles, et une couronne de gloire sur ta tête.
16:13	Tu fus parée d'or et d'argent, et ton vêtement était de fin lin, de soie, et de broderie. Tu mangeas la fleur de farine, le miel, et l'huile. Tu devins extrêmement belle, et tu prospéras jusqu'à régner.
16:14	Ta renommée se répandit parmi les nations à cause de ta beauté, car elle était parfaite, à cause de ma gloire que j'avais mise sur toi, – déclaration d'Adonaï YHWH.
16:15	Mais tu t'es confiée en ta beauté, et tu t'es prostituée à cause de ta renommée. Tu as versé ta prostitution sur tous les passants<!--Es. 1:21 ; Jé. 2:20, 3:2-6 ; Os. 1:2.--> en devenant leur chose.
16:16	Tu as pris tes vêtements pour t'en faire des hauts lieux de diverses couleurs, tels qu'il n'y en a pas eu et n'y en aura jamais, et tu t'y es prostituée.
16:17	Tu as pris les objets de ta splendeur, faits de mon or et de mon argent que je t'avais donnés, et tu t'en es fait des images d'hommes, tu as commis la fornication avec elles.
16:18	Tu as pris tes vêtements brodés, tu les en as couvertes, et tu as mis mon huile et mon encens devant elles.
16:19	Mon pain que je t'avais donné, la fleur de farine, l'huile et le miel que je t'avais donnés à manger, tu les as déposés devant elles en parfum tranquillisant. Voilà ce qui est arrivé, – déclaration d'Adonaï YHWH.
16:20	Tu as aussi pris tes fils et tes filles que tu m'avais enfantés, et tu les as sacrifiés pour être mangés<!--Lé. 18:21, 20:2 ; Es. 57:5 ; Jé. 19:5, 32:35.-->. N'était-ce pas assez de tes prostitutions ?
16:21	Tu as tué mes fils, tu les as livrés en les faisant passer devant eux<!--2 R. 17:17.-->.
16:22	Au milieu de toutes tes abominations et de tes prostitutions, tu ne t'es pas souvenue du temps de ta jeunesse, quand tu étais nue, entièrement nue, et gisante par terre dans ton sang.
16:23	Et il arriva qu’après toute ta méchanceté, malheur, malheur à toi ! déclaration d'Adonaï YHWH.
16:24	Tu t'es bâti un tertre, et tu t'es fait des hauts lieux dans toutes les places.
16:25	À l'entrée de chaque chemin tu as bâti un haut lieu, et tu as rendu ta beauté abominable, tu as écarté tes pieds pour chaque passant, tu as multiplié tes prostitutions.
16:26	Tu t'es prostituée aux fils de l'Égypte, tes voisins au corps avantageux et tu as multiplié tes prostitutions pour m'irriter.
16:27	Et voici, j'ai étendu ma main sur toi, j'ai diminué la portion que je t'avais prescrite, et je t'ai livrée à l'âme de celles qui te haïssaient, des filles des Philistins, lesquelles ont honte de tes voies qui ne sont que méchanceté.
16:28	Tu t'es prostituée aux fils des Assyriens<!--2 R. 16:7-10 ; Jé. 2:18-36.-->, parce que tu n'étais pas encore rassasiée. Et après avoir commis l'adultère avec eux, tu n'as pas encore été rassasiée.
16:29	Tu as multiplié tes prostitutions en terre de Kena'ân jusqu'en Chaldée, et avec cela tu n'as pas encore été rassasiée.
16:30	Quelle faiblesse de cœur tu as eue, – déclaration d'Adonaï YHWH –, d'avoir fait toutes ces choses-là, qui sont l'œuvre d'une femme qui se prostitue avec arrogance !
16:31	De t'être bâti un tertre à la tête de chaque chemin, et d'avoir fait ton haut lieu sur toutes les places. Et tu n'as pas été comme une prostituée, en te moquant du salaire de prostituée.
16:32	Femme adultère, tu prends des étrangers au lieu de ton homme.
16:33	On donne un don à toutes les prostituées, mais toi, tu as fait des cadeaux à tous ceux qui t’aiment<!--Es. 57:8-9 ; Os. 8:9-10.-->. Tu leur as donné des présents afin qu'ils viennent de partout se livrer à la fornication avec toi.
16:34	Il t'est arrivé, dans tes prostitutions, le contraire des femmes, parce qu'on ne te suivait pas pour commettre la fornication. On ne te donnait pas de salaire de prostituée, mais c'est toi qui donnais un salaire de prostituée. Ainsi tu as été le contraire des autres.

### Conséquences de l'infidélité de Yeroushalaim (Jérusalem)

16:35	C'est pourquoi, adultère, écoute la parole de YHWH !
16:36	Ainsi parle Adonaï YHWH : Parce que ta luxure s'est répandue, et que ta nudité s'est découverte dans tes prostitutions envers ceux qui t’aiment et envers toutes tes idoles abominables, et à cause du sang de tes enfants que tu leur as donnés,
16:37	à cause de cela, voici, je vais rassembler tous ceux qui t’aiment, avec lesquels tu te plaisais, et tous ceux que tu as aimés, avec tous ceux que tu as haïs. Je les rassemblerai de toutes parts contre toi, je découvrirai ta nudité à leurs yeux et ils verront ta nudité.
16:38	Et je te jugerai comme on juge les femmes adultères, et celles qui répandent le sang<!--Lé. 20:10 ; De. 22:22-29, 23:1.-->, et je te mettrai en sang par ma fureur et ma jalousie.
16:39	Je te livrerai entre leurs mains. Ils détruiront ton tertre et renverseront tes hauts lieux. Ils te dépouilleront de tes vêtements, emporteront ta magnifique parure et te laisseront nue, entièrement nue.
16:40	Ils feront monter contre toi une assemblée, ils te lapideront avec des pierres et te perceront avec leurs épées.
16:41	Ils brûleront tes maisons par le feu et exécuteront sur toi des jugements aux yeux de beaucoup de femmes. Je ferai cesser tes prostitutions et tu ne donneras plus de salaires de prostituée.
16:42	J'apaiserai chez toi ma colère et ma jalousie se détournera de toi : je serai en repos et je ne m'irriterai plus.
16:43	Parce que tu ne t'es pas souvenue du temps de ta jeunesse, et que tu m'as provoqué par toutes ces choses, à cause de cela, voici, j'ai fait tomber ta voie sur ta tête, – déclaration d'Adonaï YHWH –, et tu ne feras plus de méchants desseins avec toutes tes abominations.
16:44	Voici, tous ceux qui usent de proverbes feront un proverbe de toi, en disant : Telle mère, telle fille !
16:45	Tu es la fille de ta mère, qui a rejeté avec dégoût son homme et ses fils. Tu es la sœur de chacune de tes sœurs, qui ont rejeté avec dégoût leurs hommes et leurs fils. Votre mère était une Héthienne, et votre père était un Amoréen.
16:46	Ta grande sœur qui demeure à ta gauche, c'est Samarie avec ses filles. Ta petite sœur qui demeure à ta droite, c'est Sodome avec ses filles.
16:47	Et tu n'as pas marché dans leurs voies et agi selon leurs abominations : c'était trop peu ! Mais tu t'es corrompue plus qu'elles dans toutes tes voies.
16:48	Moi, le Vivant – déclaration d'Adonaï YHWH – ta sœur Sodome et ses filles n'ont pas fait ce que vous avez fait, toi et tes filles.
16:49	Voici quelle a été l'iniquité de Sodome, ta sœur : Elle avait de l'orgueil, elle vivait dans l'abondance de pain et dans une insouciante tranquillité, elle et ses filles, et elle ne fortifiait pas la main du pauvre et de l'indigent.
16:50	Elles se sont élevées, elles ont commis des abominations devant moi, et je me suis détourné quand j'ai vu cela.
16:51	Quant à Samarie, elle n'a pas commis la moitié de tes péchés car tu as multiplié tes abominations plus qu'elle, et tu as justifié tes sœurs par toutes les abominations que tu as commises.
16:52	Toi aussi, supporte ta confusion, toi qui as jugé chacune de tes sœurs, à cause de tes péchés, par lesquels tu as été rendue plus abominable qu'elles, elles sont plus justes que toi ! C'est pourquoi sois dans la honte et supporte ta confusion, puisque tu as justifié tes sœurs.
16:53	Je ramènerai leurs captifs, les captifs de Sodome et de ses filles, les captifs de Samarie et de ses filles, ainsi que tes propres captifs au milieu des leurs,
16:54	afin que tu supportes ta confusion, et que tu sois confuse à cause de tout ce que tu as fait, et que tu les consoles.
16:55	Ta sœur Sodome et les villes de son ressort retourneront à leur état précédent, Samarie et les villes de son ressort retourneront à leur état précédent, toi aussi, et les villes de ton ressort retournerez à votre état précédent.
16:56	Or ta bouche n'a pas fait mention de ta sœur Sodome, dans le temps de ton orgueil,
16:57	avant que ta méchanceté ne soit découverte, lorsque tu as reçu les insultes des filles de Syrie et de tous ses alentours, des filles des Philistins, qui te pillèrent de tous côtés !
16:58	Tu portes sur toi tes méchants desseins et tes abominations, – déclaration de YHWH.
16:59	Car ainsi parle Adonaï YHWH : J'agirai envers toi comme tu as agi, toi qui as méprisé le serment en rompant l'alliance.

### Fidélité de YHWH à son alliance

16:60	Mais je me souviendrai de l'alliance que j'ai traitée avec toi dans les jours de ta jeunesse, et j'établirai avec toi une alliance éternelle<!--Lé. 26:42-45 ; Ps. 106:45.-->.
16:61	Tu te souviendras de tes voies et tu en seras confuse, lorsque tu recevras tes sœurs, tant les aînées que les cadettes. Je te les donnerai pour filles, mais non à cause de ton alliance.
16:62	Moi, j’établirai mon alliance avec toi, et tu sauras que je suis YHWH,
16:63	afin que tu te souviennes et que tu aies honte et que tu n'ouvres plus la bouche face à ta confusion, quand je ferai pour toi la propitiation de tout ce que tu as fait, – déclaration d'Adonaï YHWH.

## Chapitre 17

### Énigme de YHWH

17:1	La parole de YHWH vint à moi en disant :
17:2	Fils d'humain, propose une énigme, une parabole<!--Ou un proverbe.--> à la maison d'Israël.
17:3	Tu diras : Ainsi parle Adonaï YHWH : Le grand aigle aux grandes ailes, aux longues ailes, couvert de plumes de toutes les couleurs, vint au Liban, et enleva la cime d'un cèdre.
17:4	Il arracha la tête de ses jeunes pousses, l'emmena dans une terre de marchands, et la mit dans une ville de trafiquants.
17:5	Il prit de la semence de la terre et la mit dans un champ à semence. L'ayant ainsi prise, il la mit comme un saule près des eaux abondantes.
17:6	Elle poussa et devint un cep de vigne étendu, de taille basse. Ses branches étaient tournées vers l'aigle et ses racines étaient sous lui. Il devint une vigne, produisit des rejetons et poussa des branches.
17:7	Il vint ensuite un autre grand aigle, aux grandes ailes, et au plumage abondant. Et voici que cette vigne dirigea avec avidité ses racines vers lui pour qu'il l'arrose. Elle tendit vers lui ses branches, hors de la terrasse où elle était plantée.
17:8	Elle était plantée dans une bonne terre, près des eaux abondantes, de manière à produire des sarments et à porter du fruit<!--Mt. 13:8-23 ; Mc. 4:8-20 ; Lu. 8:8-15.--> pour devenir une vigne magnifique.
17:9	Dis : Ainsi parle Adonaï YHWH : Prospérera-t-elle ? N’arrachera-t-il pas ses racines ? Ne coupera-t-il pas son fruit en sorte qu’elle sèche ? Toutes les jeunes feuilles de ses pousses sécheront. Ce n’est ni par un grand bras ni par un peuple nombreux qu’il faudra l’enlever à ses racines.
17:10	La voici plantée, prospérera-t-elle ? Quand le vent d'orient l'aura touchée, ne séchera-t-elle pas, ne séchera-t-elle pas ? Elle séchera sur la terrasse où elle était plantée.

### Jugement d'Elohîm sur Tsidqiyah (Sédécias)<!--2 R. 24:17-20, 25:1-10.-->

17:11	La parole de YHWH vint à moi en disant :
17:12	Parle maintenant à la maison de la rébellion : Ne savez-vous pas ce que veulent dire ces choses ? Dis : Voici, le roi de Babel est venu à Yeroushalaim. Il a pris le roi, et les princes, et les a emmenés avec lui à Babel.
17:13	Il en a pris un de la postérité royale, il a traité alliance avec lui, il lui a fait prêter serment, et il a pris les puissants de la terre,
17:14	afin que le royaume devienne bas, sans pouvoir s'élever, et qu'il garde son alliance pour subsister.
17:15	Mais celui-ci s'est rebellé contre lui, envoyant ses messagers en Égypte, pour qu'on lui donne des chevaux et un grand peuple. Celui qui fait de telles choses prospérera-t-il, échappera-t-il ? Ayant rompu l'alliance, échappera-t-il ?
17:16	Moi, le Vivant – déclaration d'Adonaï YHWH – c'est en terre du roi qui l'a établi pour roi, envers qui il a méprisé son serment et dont il a rompu l'alliance, c'est près de lui, au milieu de Babel, qu'il mourra<!--Référence à Neboukadnetsar. Tsidqiyah (Sédécias) eut les yeux crevés avant d'être emmené captif (2 R. 25:7 ; Jé. 34:3, 52:11).-->.
17:17	Pharaon n'agira pas pour lui avec une grande armée et une assemblée nombreuse dans la guerre, quand on élèvera des tertres et qu'on bâtira des retranchements pour exterminer beaucoup d'âmes.
17:18	Il a méprisé le serment en rompant l'alliance. Voici, il avait donné sa main et quand même il a fait toutes ces choses. Il n'échappera pas !
17:19	C'est pourquoi, ainsi parle Adonaï YHWH : Je suis vivant, si je ne fais tomber sur sa tête mon serment qu'il a méprisé, et mon alliance qu'il a rompue.
17:20	J'étendrai mon rets sur lui et il sera pris dans mes filets. Je le ferai entrer dans Babel, et là j'entrerai en jugement avec lui pour le délit, la transgression qu'il a commise envers moi.
17:21	Et tous ses fugitifs avec toutes ses armées tomberont par l'épée, et ceux qui resteront seront dispersés à tout vent. Vous saurez que moi, YHWH, j'ai parlé.
17:22	Ainsi parle Adonaï YHWH : Je prendrai moi-même la cime d'un cèdre élevé, et je la mettrai en place. J'arracherai du sommet de ses jeunes branches une pousse tendre et je la planterai moi-même sur une montagne haute et éminente.
17:23	Je la planterai sur la haute montagne d'Israël. Elle produira des branches et produira du fruit, et elle deviendra un excellent cèdre. Tout oiseau, toute aile habitera sous elle. Ils habiteront à l'ombre de ses branches.
17:24	Et tous les bois des champs sauront que moi, YHWH, j'aurai abaissé le grand arbre, et élevé le petit arbre, fait sécher le bois vert, et fait reverdir le bois sec. Moi, YHWH, j'ai parlé, et je le ferai.

## Chapitre 18

### Chacun responsable de son péché

18:1	La parole de YHWH vint à moi en disant :
18:2	D'où vient que vous tournez entre vous cette parabole en proverbe sur le sol d'Israël, en disant : Les pères ont mangé des raisins verts et les dents des enfants ont été agacées<!--Jé. 31:29 ; La. 5:7.--> ?
18:3	Moi, le Vivant – déclaration d'Adonaï YHWH – il ne vous arrivera plus de prononcer ce proverbe en Israël.
18:4	Voici, toutes les âmes sont à moi ; l'âme du fils est à moi comme l'âme du père. L'âme qui pèche mourra.
18:5	Mais l'homme qui est juste, c'est celui qui pratique le droit et la justice.
18:6	Il ne mange pas sur les montagnes et ne lève pas ses yeux vers les idoles de la maison d'Israël. Il ne souille pas la femme de son prochain et ne s'approche pas d'une femme pendant ses menstruations<!--Lé. 18:19, 20:18.-->.
18:7	Cet homme n'opprime personne, il rend le gage à son débiteur<!--Ex. 22:25 ; De. 24:12-13.-->, il ne prend rien de force par le vol. Il donne son pain à celui qui a faim et couvre d'un vêtement celui qui est nu<!--De. 15:11 ; Es. 58:7.-->.
18:8	Il ne prête pas à usure et ne prend pas d'intérêt. Il détourne sa main de l'injustice et rend son jugement selon la vérité entre homme et homme<!--Ex. 22:24 ; Lé. 25:35-37 ; De. 23:20.-->.
18:9	Il marche dans mes statuts et garde mes ordonnances pour agir avec fidélité. Celui-là est juste et il vivra, il vivra, – déclaration d'Adonaï YHWH.
18:10	Et s'il a engendré un fils qui est un meurtrier, qui répand du sang et qui commet envers son frère l'une de ces choses,
18:11	mais lui-même n'a pas fait toutes ces choses, s'il mange sur les montagnes, s'il déshonore la femme de son prochain,
18:12	s'il opprime le malheureux et le pauvre, s'il arrache des choses de force par le vol, s'il ne rend pas le gage, s'il lève ses yeux vers les idoles et commet des abominations,
18:13	s'il prête avec usure et prend des intérêts, ce fils-là vivrait-il ? Il ne vivra pas. Il a commis toutes ces abominations : il mourra, il mourra, et son sang retombera sur lui.
18:14	Mais voici qu’il engendre un fils, qui voit tous les péchés que commet son père, il les voit et n'agit pas de la même manière :
18:15	s'il ne mange pas sur les montagnes et qu'il ne lève pas ses yeux vers les idoles de la maison d'Israël, s'il ne déshonore pas la femme de son prochain,
18:16	s'il n'opprime aucun homme, s'il ne prend pas de gages, s'il ne prend rien de force par le vol, s'il donne de son pain à celui qui a faim et couvre celui qui est nu,
18:17	s'il retire sa main du pauvre, s'il n'exige ni usure ni intérêt, s'il garde mes ordonnances, et s'il marche dans mes statuts, il ne mourra pas pour l'iniquité de son père, mais il vivra, il vivra.
18:18	Mais son père, parce qu'il a pratiqué l'extorsion, qu'il a, par le vol, arraché quelque chose de force à son frère, et a fait parmi son peuple ce qui n'est pas bon, voici, il mourra pour son iniquité.
18:19	Vous dites : Pourquoi le fils ne porte-t-il pas l'iniquité de son père<!--Ex. 20:5 ; De. 5:9.--> ? Parce que le fils a fait ce qui était juste et droit, et qu'il a gardé tous mes statuts et les a observés. Il vivra, il vivra.
18:20	L'âme qui pèche est celle qui mourra. Le fils ne portera pas l'iniquité de son père, et le père ne portera pas l'iniquité de son fils. La justice du juste sera sur lui, et la méchanceté du méchant sera sur lui.
18:21	Si le méchant se détourne de tous ses péchés qu'il aura commis, et qu'il garde tous mes statuts, et pratique ce qui est juste et droit, il vivra, il vivra, il ne mourra pas.
18:22	Il ne lui sera pas fait mention de toutes ses transgressions qu'il aura commises. Il vivra à cause de la justice qu'il aura pratiquée.
18:23	Prendrais-je plaisir à la mort du méchant, – déclaration d'Adonaï YHWH –, et non plutôt à ce qu'il se détourne de ses mauvaises voies et qu'il vive ?
18:24	Si le juste se détourne de sa justice et commet l'injustice, faisant selon toutes les abominations que le méchant commet, vivra-t-il ? On ne se souviendra plus de tout ce qu'il a fait pour la justice. À cause de son délit, de sa transgression qu'il a commise et de son péché par lequel il a péché, à cause de ces choses, il mourra.
18:25	Et vous, vous dites : La voie d'Adonaï n'est pas bien réglée. Écoutez maintenant maison d'Israël ! Ma voie n'est-elle pas bien réglée ? Ne sont-ce pas plutôt vos voies qui ne sont pas bien réglées ?
18:26	Si le juste se détourne de sa justice et commet l'injustice, il mourra à cause de ces choses-là. Il mourra à cause de son injustice qu'il aura commise.
18:27	Si le méchant se détourne de sa méchanceté qu'il aura commise, et pratique ce qui est juste et droit, il fera vivre son âme.
18:28	Quand il verra et qu’il reviendra de toutes ses transgressions qu’il a commises, il vivra, il vivra, il ne mourra pas.
18:29	La maison d'Israël dit : La voie d'Adonaï YHWH n'est pas bien réglée. Maison d'Israël ! Mes voies ne sont-elles pas bien réglées ? Ne sont-ce pas plutôt vos voies qui ne sont pas bien réglées ?
18:30	C'est pourquoi je vous jugerai, chaque homme selon ses voies, maison d'Israël ! – déclaration d'Adonaï YHWH. Revenez, et détournez-vous de toutes vos transgressions, afin que l'iniquité ne devienne pas pour vous une pierre d'achoppement !
18:31	Rejetez loin de vous toutes les transgressions par lesquelles vous avez péché ! Faites-vous un nouveau cœur et un esprit nouveau ! Pourquoi mourriez-vous, maison d'Israël ?
18:32	Car je ne prends pas de plaisir à la mort de celui qui meurt, – déclaration d'Adonaï YHWH. Revenez et vivez<!--Ac. 3:19-20.--> !

## Chapitre 19

### Complaintes sur les dirigeants d'Israël

19:1	Et toi, élève un chant funèbre sur les princes d'Israël !
19:2	Tu diras : Ta mère, qui était-elle ? Une lionne. Elle était couchée parmi les lions et a élevé ses petits parmi les jeunes lions.
19:3	Elle fit croître l'un de ses petits, qui devint un jeune lion. Il apprit à déchirer la proie et à dévorer les humains.
19:4	Les nations entendirent parler de lui et il fut attrapé dans leur fosse. Elles l'emmenèrent avec des crochets en terre d'Égypte<!--2 R. 23:33-34.-->.
19:5	Lorsqu'elle vit que son attente, que son espérance s'évanouissait, elle prit un de ses petits et en fit un jeune lion.
19:6	Il marcha parmi les lions et devint un jeune lion. Il apprit à déchirer la proie et à dévorer les humains.
19:7	Il connut leurs veuves et détruisit leurs villes. La terre avec sa plénitude fut ruinée par le cri de son rugissement.
19:8	Les nations des provinces d'alentour se mirent contre lui et tendirent sur lui leurs filets pour le prendre : il fut attrapé dans leur fosse<!--2 R. 24:2.-->.
19:9	Elles le mirent dans une cage avec des crochets et le conduisirent auprès du roi de Babel. Elles le conduisirent dans une forteresse afin que sa voix ne soit plus entendue sur les montagnes d'Israël.
19:10	Ta mère était comme une vigne dans ton sang, plantée auprès des eaux. Elle portait du fruit et était pleine de branches, grâce à l'abondance des eaux.
19:11	Elle avait de puissantes branches, pour des sceptres de dominateurs. Par sa taille, elle atteignait jusqu'aux branches touffues. Elle était visible par sa hauteur et par la multitude de ses branches.
19:12	Mais elle a été arrachée avec fureur et jetée par terre. Le vent d'orient a desséché son fruit. Ses puissantes branches ont été arrachées et se sont desséchées ; le feu les a dévorées.
19:13	Maintenant elle est plantée dans le désert, dans une terre sèche et aride.
19:14	Le feu est sorti d'une verge de ses branches et a dévoré son fruit. Elle n'a plus de branches puissantes, de sceptre de domination. C'est là un chant funèbre, et cela deviendra un chant funèbre.

## Chapitre 20

### Compassions de YHWH face aux infidélités d'Israël

20:1	Or il arriva la septième année, au dixième jour du cinquième mois, que quelques-uns des anciens d'Israël vinrent pour consulter YHWH, et s'assirent devant moi.
20:2	La parole de YHWH vint à moi en disant :
20:3	Fils d'humain, parle aux anciens d'Israël, et dis-leur : Ainsi parle Adonaï YHWH : Est-ce pour me consulter que vous venez ? Moi, le Vivant – déclaration d'Adonaï YHWH – je ne me laisserai pas consulter par vous !
20:4	Ne les jugeras-tu pas, ne les jugeras-tu pas, fils d'humain ? Fais-leur connaître les abominations de leurs pères.
20:5	Et dis-leur : Ainsi parle Adonaï YHWH : Le jour où j'ai choisi Israël, j'ai levé ma main vers la postérité de la maison de Yaacov et je me suis fait connaître à eux en terre d'Égypte. J'ai levé ma main vers eux en disant : Je suis YHWH, votre Elohîm.
20:6	En ce jour, j'ai levé ma main vers eux pour les faire sortir de la terre d'Égypte vers une terre que j'avais cherchée pour eux, une terre où coulent le lait et le miel, et qui est la beauté de toutes les terres<!--Ex. 3:8, 6:7.-->.
20:7	Je leur dis : Que chaque homme rejette les abominations de ses yeux ! Ne vous souillez pas avec les idoles d'Égypte ! Je suis YHWH, votre Elohîm<!--Jos. 24:14-23.-->.
20:8	Mais ils se sont rebellés contre moi et n'ont pas voulu m'écouter. Les hommes n’ont pas rejeté les abominables de leurs yeux, ils n'ont pas abandonné les idoles de l'Égypte. J'ai parlé de répandre ma fureur sur eux, d'accomplir ma colère contre eux au milieu de la terre d'Égypte.
20:9	Mais j'ai agi à cause de mon Nom, afin qu'il ne soit pas profané aux yeux des nations parmi lesquelles ils se trouvaient. En effet, c'était sous leurs yeux que je m'étais fait connaître à eux pour les faire sortir de la terre d'Égypte.
20:10	Je les ai fait sortir d'Égypte et je les ai conduits dans le désert.
20:11	Je leur ai donné mes statuts et leur ai fait connaître mes ordonnances, celles que l'être humain doit mettre en pratique afin de vivre par elles<!--Lé. 18:5 ; Ro. 10:5 ; Ga. 3:12.-->.
20:12	Je leur ai donné aussi mes shabbats pour devenir un signe entre moi et eux, afin qu'ils sachent que je suis YHWH qui les sanctifie<!--Ex. 20:8, 31:13.-->.
20:13	Mais la maison d'Israël se rebella contre moi dans le désert. Ils n'ont pas marché dans mes statuts, et ils ont rejeté mes ordonnances, celles que l'être humain doit mettre en pratique afin de vivre par elles, et ils ont profané extrêmement mes shabbats. C'est pourquoi j'ai parlé de répandre sur eux ma colère dans le désert pour les consumer<!--Ex. 16:28.-->.
20:14	J'ai agi à cause de mon Nom, afin de ne pas le profaner aux yeux des nations sous les yeux desquelles je les avais fait sortir<!--Ex. 32:12 ; No. 14:13-14 ; De. 9:28 ; Jos. 7:9.-->.
20:15	Dans le désert, c'est aussi moi qui leur ai juré, à main levée, de ne pas les faire venir sur la terre que je leur avais donnée, terre où coulent le lait et le miel, et qui est la beauté de toutes les terres,
20:16	parce qu'ils avaient rejeté mes ordonnances, qu'ils n’avaient pas marché dans mes statuts et qu'ils avaient profané mes shabbats, car leur cœur marchait après leurs idoles.
20:17	Toutefois mon œil les a épargnés pour ne pas les détruire et je n'ai pas fait d'eux une destruction complète dans le désert.
20:18	Cependant, j'ai dit à leurs fils dans le désert : Ne marchez pas dans les statuts de vos pères, ne gardez pas leurs ordonnances et ne vous souillez pas par leurs idoles.
20:19	Je suis YHWH, votre Elohîm. Marchez dans mes statuts, gardez mes ordonnances et accomplissez-les !
20:20	Sanctifiez mes shabbats, qu'ils deviennent un signe entre moi et vous, pour qu’on sache que je suis YHWH, votre Elohîm.
20:21	Mais leurs fils se sont rebellés contre moi. Ils n'ont pas marché dans mes statuts et n'ont pas gardé mes ordonnances pour les mettre en pratique, celles que l'être humain doit accomplir afin de vivre par elles et ils ont profané mes shabbats. C'est pourquoi j'ai parlé de répandre sur eux ma colère, d'accomplir ma colère contre eux dans le désert.
20:22	Néanmoins j'ai retiré ma main et j'ai agi à cause de mon Nom, afin de ne pas le profaner aux yeux des nations sous les yeux desquelles je les avais fait sortir.
20:23	Dans le désert, c'est aussi moi qui leur ai juré, à main levée, de les éparpiller parmi les nations, de les disperser sur toutes les terres<!--Lé. 26:13-33.-->,
20:24	parce qu'ils n'ont pas accompli mes ordonnances, qu'ils ont rejeté mes statuts, profané mes shabbats et que leurs yeux se sont attachés aux idoles de leurs pères.
20:25	Et moi aussi, je leur ai donné des statuts qui n'étaient pas bons et des ordonnances par lesquelles ils ne pouvaient pas vivre.
20:26	Je les ai souillés par leurs dons, quand ils faisaient passer par le feu tout ce qui sort le premier du sein maternel, pour les frapper de stupeur afin qu'ils sachent que je suis YHWH.
20:27	C'est pourquoi, toi fils d'humain, parle à la maison d'Israël et dis-leur : Ainsi parle Adonaï YHWH : Vos pères m'ont encore blasphémé en commettant un délit, une transgression contre moi.
20:28	Je les ai fait venir vers la terre que j'avais juré, à main levée, de leur donner, et ils ont regardé toute colline élevée et tout arbre touffu, et là ils ont sacrifié leurs sacrifices, et là ils ont donné leurs offrandes pour m'irriter, et là ils ont mis leurs parfums apaisants, et là ils ont répandu leurs libations.
20:29	Je leur ai dit : Que veulent dire ces hauts lieux où vous allez ? Et le nom de hauts lieux leur a été donné jusqu'à ce jour.
20:30	C'est pourquoi dis à la maison d'Israël : Ainsi parle Adonaï YHWH : Ne vous souillez-vous pas en suivant les voies de vos pères et ne vous prostituez-vous pas à leurs idoles abominables ?
20:31	En apportant vos dons, en faisant passer vos fils par le feu, en vous souillant par toutes vos idoles jusqu'à ce jour, est-ce ainsi que vous me consultez, maison d'Israël ? Moi, le Vivant – déclaration d'Adonaï YHWH – vous ne me consultez pas.
20:32	Ce qui vous monte à l'esprit n'arrivera nullement, quand vous dites : Devenons comme les nations, comme les familles des terres, en servant le bois et la pierre.

### Restauration future d'Israël

20:33	Moi, le Vivant – déclaration d'Adonaï YHWH. Je règnerai sur vous d'une main forte, d'un bras étendu, en déversant ma colère.
20:34	Je vous sortirai du milieu des peuples et je vous rassemblerai hors des terres où vous êtes dispersés, d'une main forte, d'un bras étendu, en déversant ma colère.
20:35	Je vous ferai venir dans le désert des peuples et là, je vous jugerai faces à faces,
20:36	comme j'ai jugé vos pères dans le désert de la terre d'Égypte, ainsi vous jugerai-je, – déclaration d'Adonaï YHWH.
20:37	Je vous ferai passer sous la verge et je vous introduirai dans le lien de l'alliance<!--Es. 65:12.-->.
20:38	Je séparerai de vous les rebelles, ceux qui se révoltent contre moi. Je les ferai sortir de la terre où ils séjournent, mais ils ne viendront pas sur le sol d'Israël. Vous saurez que je suis YHWH.
20:39	Vous, maison d'Israël, ainsi parle Adonaï YHWH : Que chaque homme aille servir ses idoles ! Mais après cela, ne m'écouterez-vous pas ? Ainsi vous ne profanerez plus mon saint Nom par vos dons et par vos idoles.
20:40	Mais ce sera sur ma sainte montagne, sur la haute montagne d'Israël, – déclaration d'Adonaï YHWH –, que toute la maison d'Israël me servira, sur la terre<!--Jn. 4:21-24.-->. Là, je prendrai plaisir en eux, et là je demanderai vos contributions et le meilleur de vos dons, et tout ce que vous me consacrerez.
20:41	Je vous accueillerai favorablement, comme un sacrifice dont le parfum est tranquillisant, quand je vous aurai fait sortir du milieu des peuples et que je vous aurai rassemblés des terres où vous êtes dispersés. Je serai sanctifié par vous, aux yeux des nations.
20:42	Vous saurez que je suis YHWH, quand je vous aurai fait revenir sur le sol d'Israël, sur la terre au sujet de laquelle j'ai levé ma main pour la donner à vos pères.
20:43	Et là, vous vous souviendrez de vos voies, et de toutes vos actions par lesquelles vous vous êtes rendus impurs. Vous vous prendrez vous-mêmes en dégoût à cause de tout le mal que vous avez commis.
20:44	Vous saurez que je suis YHWH, quand j'agirai avec vous à cause de mon Nom, et non pas selon vos méchantes voies et vos actions corrompues, maison d'Israël ! – déclaration d'Adonaï YHWH.

## Chapitre 21

### L'épée de YHWH

21:1	La parole de YHWH vint à moi en disant :
21:2	Fils d'humain, mets tes faces vers le chemin de Théman<!--sud ou midi.-->, distille contre le sud, prophétise contre la forêt du champ du sud !
21:3	Dis à la forêt du sud : Écoute la parole de YHWH ! Ainsi parle Adonaï YHWH : Voici, je vais allumer au-dedans de toi un feu qui consumera tout bois vert et tout bois sec au-dedans de toi. La flambée des flammes ne s'éteindra pas et tout le dessus en sera brûlé, depuis le sud jusqu'au nord<!--Jé. 21:14, 22:7, 46:23 ; Lu. 23:31.-->.
21:4	Toute chair verra que moi, YHWH, j'ai allumé le feu, il ne s'éteindra pas.
21:5	Je dis : Ah ! Adonaï YHWH ! Ils disent de moi : Celui-ci ne fait que parler en paraboles !

### Parabole de l'épée de YHWH

21:6	La parole de YHWH vint à moi en disant :
21:7	Fils d'humain, mets tes faces vers Yeroushalaim, distille contre le sanctuaire, prophétise contre le sol d'Israël !
21:8	Dis au sol d'Israël : Ainsi parle YHWH : Voici, je m'oppose à toi, je tirerai mon épée de son fourreau, et je retrancherai du milieu de toi le juste et le méchant.
21:9	Parce que je retrancherai du milieu de toi le juste et le méchant, à cause de cela mon épée sortira de son fourreau contre toute chair, depuis le sud jusqu'au nord.
21:10	Toute chair saura que moi, YHWH, j’ai sorti mon épée de son fourreau, elle n'y retournera plus.
21:11	Et toi, fils d'humain, gémis, les reins brisés, gémis avec amertume sous leurs yeux.
21:12	Et il arrivera quand ils te diront : Pourquoi gémis-tu ? Tu diras : C'est à cause d'une nouvelle, car elle vient, tout cœur se fondra, toutes les mains seront baissées, tout esprit sera affaibli et tous les genoux s’en iront comme de l’eau. Voici, elle vient, elle arrive ! – déclaration d'Adonaï YHWH<!--Jé. 6:24, 49:23.-->.
21:13	La parole de YHWH vint à moi en disant :
21:14	Fils d'humain, prophétise, et dis : Ainsi parle YHWH : Dis : L'épée, l'épée est aiguisée, elle est polie !
21:15	Elle est aiguisée pour massacrer au massacre, elle est polie pour lancer des éclairs ! Nous réjouirons-nous ? C'est la verge de mon fils, elle dédaigne tout bois.
21:16	On l'a donnée à polir pour la saisir avec la paume. L'épée est aiguisée, elle est polie pour la donner à la main du tueur.
21:17	Crie et hurle, fils d'humain ! Car elle est contre mon peuple, elle est contre tous les princes d'Israël : ils sont livrés à l'épée à cause de mon peuple. C'est pourquoi frappe sur ta cuisse !
21:18	Car l'épreuve est faite ! Et quoi ? Si ce sceptre qui méprise tout n'est plus, – déclaration d'Adonaï YHWH.
21:19	Toi, fils d'humain, prophétise, et frappe paume contre paume, et que l’épée redouble jusqu’à la troisième fois, c'est l'épée des blessés mortellement, l’épée qui a blessé mortellement les grands et qui les environne.
21:20	C’est pour fondre les cœurs, pour multiplier les occasions de trébucher à toutes les portes, que j'ai mis l'épée pour abattre. Ah ! Elle est faite pour l’éclair, elle est aiguisée pour le massacre.
21:21	Sois tranchante ! À droite ! Place toi ! À gauche ! Où tes faces sont-elles assignées ?
21:22	Moi aussi, je frapperai ma paume contre ma paume, je donnerai du repos à ma colère. Moi, YHWH, j'ai parlé.
21:23	La parole de YHWH vint à moi en disant :
21:24	Toi, fils d'humain, fais-toi deux chemins pour la venue de l'épée du roi de Babel. Que ces deux chemins sortent d'une même terre. Crée une main, crée-la en tête du chemin de la ville.
21:25	Tu feras un chemin pour que l’épée aille à Rabbath des fils d'Ammon, et en Yéhouda, contre Yeroushalaim, la ville fortifiée.
21:26	Car le roi de Babel se tient au carrefour, à l'entrée des deux chemins, pour pratiquer la divination, la sorcellerie : il aiguise les flèches, il interroge les théraphim, il examine le foie.
21:27	Dans sa main droite est la divination contre Yeroushalaim, pour y dresser des béliers, pour publier le carnage, pour faire retentir l'alarme de guerre, pour ranger les béliers contre les portes, pour élever des tertres et construire des remparts.
21:28	Mais cela paraîtra à leurs yeux une divination mensongère, eux qui ont juré par des serments. Mais lui se souvient de leur iniquité, de sorte qu'ils seront capturés.
21:29	C'est pourquoi ainsi parle Adonaï YHWH : Parce que vous rappelez le souvenir de votre iniquité en mettant à découvert vos transgressions, en faisant apparaître vos péchés dans toutes vos actions, parce que vous en rappelez le souvenir, vous serez saisis par la paume.

### Quand l'iniquité arrive à son terme<!--Ap. 19:11-20:6.-->

21:30	Et toi, profane, méchant, prince d'Israël, dont le jour arrive au temps où l'iniquité prend fin !
21:31	Ainsi parle Adonaï YHWH : Qu'on ôte cette tiare ! Qu'on enlève cette couronne ! Les choses vont changer. J'élèverai ce qui est abaissé, et j'abaisserai ce qui est élevé<!--Job 5:11 ; 1 Co. 1:27.-->.
21:32	J'en ferai une ruine, une ruine, une ruine, et elle ne sera plus. Mais cela n'aura lieu qu'à la venue de celui<!--Il est question du Mashiah (Christ). Voir Jn. 5:27, 13:3 ; 2 Ti. 4:1.--> à qui appartient le jugement et à qui je le donnerai.
21:33	Toi, fils d'humain, prophétise, et dis : Ainsi parle Adonaï YHWH, au sujet des fils d'Ammon, et de leur insulte. Dis : L'Épée, l'épée est ouverte pour le massacre, elle est polie pour dévorer et pour briller comme l’éclair !
21:34	Pendant que tu contemples la vanité, en pratiquant la divination de mensonge, afin de te mettre sur le cou des méchants qui sont blessés mortellement, dont le jour est venu au temps où l'iniquité prend fin.
21:35	La remettrait-on dans son fourreau ? Je te jugerai sur le lieu où tu as été créé, en terre de ta naissance.
21:36	Je répandrai ma colère sur toi, j'allumerai sur toi le feu de ma fureur, et je te livrerai entre les mains d'hommes brûlants, les artisans de destruction<!--Jé. 25:11, 52:30.-->.
21:37	Tu deviendras la nourriture du feu. Ton sang sera au milieu de la terre : On ne se souviendra plus de toi, car c'est moi, YHWH, qui parle.

## Chapitre 22

### Les péchés d'Israël

22:1	La parole de YHWH vint à moi en disant :
22:2	Et toi, fils d’humain, jugeras-tu, jugeras-tu la ville des sangs ? Lui feras-tu connaître toutes ses abominations<!--Ez. 24:6-9 ; Na. 3:1-4 ; Ha. 1:13.--> ?
22:3	Tu diras, ainsi parle Adonaï YHWH : Ville qui répands le sang au milieu de toi pour faire venir ton temps, et qui te fais des idoles afin de devenir impure !
22:4	Tu t'es rendue coupable par ton sang que tu as répandu, tu t'es rendue impure par tes idoles que tu as faites. Tu as fait approcher tes jours et tu es venue au terme de tes années. C'est pourquoi je fais de toi une insulte pour les nations et une moquerie pour toutes les terres<!--2 R. 21:16 ; Jé. 26:21-23.-->.
22:5	Celles qui sont près de toi comme celles qui sont loin se moqueront de toi, car ton nom est impur et grande est ta confusion.
22:6	Voici les princes d'Israël : chaque homme, de son bras, est contre toi pour répandre le sang.
22:7	Au dedans de toi, on traite légèrement père et mère, on use d'oppression à l'égard de l'étranger, on opprime l'orphelin et la veuve.
22:8	Tu méprises mes choses saintes et tu profanes mes shabbats.
22:9	Il y a chez toi des hommes qui calomnient pour répandre le sang. Chez toi, on mange sur les montagnes. On agit avec méchanceté au milieu de toi<!--Es. 57:7 ; Jé. 2:20.-->.
22:10	Chez toi, on découvre la nudité du père, chez toi, on humilie une femme impure à cause de ses menstruations<!--Lé. 18:6-9 ; Ge. 9:22-23.-->.
22:11	Chez toi, l'homme fait l'abomination avec la femme de son ami : l'homme se souille par l'inceste avec sa belle-fille, l'homme humilie sa sœur, fille de son père<!--Ge. 19:32-36 ; Lé. 18:15-20 ; Jé. 5:8.-->.
22:12	Chez toi, on accepte des pots-de-vin pour répandre le sang. Tu exiges un intérêt et une usure, tu dépouilles ton prochain par l'extorsion, et tu m'oublies, – déclaration d'Adonaï YHWH<!--Ex. 23:8 ; De. 27:25.-->.
22:13	Voici, je frappe de ma paume à cause de ton gain injuste que tu as acquis, et à cause du sang qui est au milieu de toi.
22:14	Ton cœur tiendra-t-il ferme ? Tes mains seront-elles fortes aux jours où j'agirai contre toi ? Moi, YHWH, j'ai parlé, et je le ferai.
22:15	Je te disperserai parmi les nations, je t'éparpillerai dans diverses terres et je mettrai fin à ton impureté jusqu'à ce qu'il n'y en ait plus en toi.
22:16	Tu t'es profanée toi-même aux yeux des nations, et tu sauras que je suis YHWH.

### La fureur de YHWH

22:17	La parole de YHWH vint à moi en disant :
22:18	Fils d'humain, la maison d'Israël est devenue pour moi des scories. Ils sont tous du cuivre, de l'étain, du fer et du plomb dans un creuset, ils sont devenus des scories d'argent.
22:19	C'est pourquoi ainsi parle Adonaï YHWH : Parce que vous êtes tous devenus de l'écume, voici, je vais à cause de cela vous rassembler au milieu de Yeroushalaim,
22:20	comme on rassemble de l'argent, du cuivre, du fer, du plomb et de l'étain dans un creuset, afin d'y souffler le feu pour les fondre, je vous rassemblerai ainsi dans ma colère et dans ma fureur, et je vous fondrai.
22:21	Je vous rassemblerai, je soufflerai contre vous le feu de ma fureur, et vous serez fondus au milieu d'elle.
22:22	Comme l'argent se fond dans le creuset, ainsi vous serez fondus au milieu d'elle. Vous saurez que moi, YHWH, j'ai répandu ma fureur sur vous.

### Se tenir à la brèche devant YHWH

22:23	La parole de YHWH vint à moi en disant :
22:24	Fils d'humain, dis-lui : Tu es une terre qui n'est pas purifiée ni arrosée de pluie au jour de la colère.
22:25	Il y a un complot de ses prophètes au milieu d'elle. Ils seront comme des lions rugissants qui ravissent la proie, ils dévorent les âmes, ils emportent les richesses et la gloire, ils multiplient les veuves au milieu d'elle<!--Mt. 23:13 ; 1 Pi. 5:8.-->.
22:26	Ses prêtres font violence à ma torah et profanent mes choses saintes. Ils ne distinguent pas la chose sainte de la chose profane, la chose impure et de la chose pure, ils ne le font pas connaître. Ils ferment leurs yeux sur mes shabbats et je suis profané au milieu d'eux.
22:27	Ses princes sont au milieu d'elle comme des loups qui ravissent la proie, pour répandre le sang et pour détruire les âmes, pour s'adonner au gain injuste<!--Mi. 3:11 ; Mt. 10:16 ; 2 Pi. 2:16.-->.
22:28	Ses prophètes les recouvrent de la chaux blanche, ayant des visions de vanité et devinant pour eux le mensonge. Ils disent : Ainsi parle Adonaï YHWH ! Et cependant YHWH n'a pas parlé.
22:29	Le peuple de la terre use de violence, commet des extorsions, emporte des choses de force par le vol, opprime l'affligé et le pauvre, il use de violence envers l'étranger contre toute justice.
22:30	J'ai cherché parmi eux un homme<!--Elohîm n'a pas besoin d'une foule de gens avant d'agir. Une seule personne suffit.--> qui élèverait la clôture, et qui se tiendrait à la brèche devant moi pour la terre, afin que je ne la détruise pas, mais je n'en ai pas trouvé.
22:31	C'est pourquoi je répandrai sur eux ma colère, et je les consumerai par le feu de ma fureur. Je mettrai leur voie sur leur tête, – déclaration d'Adonaï YHWH.

## Chapitre 23

### Prostitutions d'Israël et de Yéhouda

23:1	La parole de YHWH vint à moi en disant :
23:2	Fils d'humain, il y a eu deux femmes, filles d'une même mère,
23:3	qui se sont prostituées en Égypte, elles se sont prostituées dans leur jeunesse. Là-bas on a pressé leurs seins, là on a caressé les poitrines de leur virginité.
23:4	Et voici leurs noms : Ohola, la plus grande, et Oholiba<!--Ohola signifie « sa propre tente », et Oholiba « la femme de la tente » (2 R. 17:23-24).--> sa sœur. Elles étaient à moi, et elles ont enfanté des fils et des filles. Voici leurs noms : Ohola, c'est Samarie, et Oholiba, c'est Yeroushalaim.
23:5	Or Ohola a commis l'adultère étant ma femme. Elle a eu une affection désordonnée pour ceux qui l’aimaient, pour les Assyriens, ses voisins,
23:6	vêtus d'étoffes teintes en violet, gouverneurs et magistrats, tous jeunes hommes et désirables, tous cavaliers, montés sur des chevaux.
23:7	Elle mettait ses prostitutions sur eux tous, les meilleurs fils des Assyriens. Elle s'est rendue impure avec toutes les idoles de tous ceux pour qui elle a eu une affection désordonnée.
23:8	Elle n'a pas abandonné ses prostitutions d'Égypte, car ils avaient couché avec elle dans sa jeunesse, ils avaient caressé la poitrine de sa virginité et s'étaient livrés à la fornication avec elle<!--Ac. 7:42.-->.
23:9	C'est pourquoi je l'ai livrée entre les mains de ceux qui l’aimaient, entre les mains des fils des Assyriens, pour qui elle avait eu une affection désordonnée.
23:10	Ils l'ont couverte d'opprobre, ils ont enlevé ses fils et ses filles, et l'ont tuée elle-même avec l'épée. Elle est devenue renommée parmi les femmes après les jugements exercés sur elle.
23:11	Quand sa sœur Oholiba a vu cela, elle s'est corrompue plus qu'elle dans sa sensualité, et ses prostitutions ont dépassé les prostitutions de sa sœur.
23:12	Elle a eu une affection désordonnée pour les fils des Assyriens, des gouverneurs et des magistrats, ses voisins, vêtus magnifiquement, et des cavaliers montés sur des chevaux, tous jeunes hommes et désirables.
23:13	J'ai vu qu'elle s'était rendue impure, qu'elles avaient toutes les deux une même voie.
23:14	Mais elle est allée même plus loin dans ses prostitutions, quand elle a vu les peintures d'hommes sur la muraille, les images de Chaldéens peints en rouge,
23:15	portant des ceintures autour de leurs reins, ayant des turbans teints de couleurs variées flottant sur leurs têtes, eux tous ayant l'apparence de grands seigneurs, et la ressemblance des fils de Babel en Chaldée, terre de leur naissance.
23:16	Elle a eu une affection désordonnée pour eux en les voyant de ses yeux, et elle a envoyé des messagers vers eux en terre des Chaldéens.
23:17	Les fils de Babel se rendirent auprès d'elle pour partager le lit des amours, et ils la souillèrent par leurs prostitutions. Elle s'est aussi souillée avec eux, et après cela son âme s'est détachée<!--S'est disloqué.--> d'eux.
23:18	Elle a manifesté ses prostitutions et fait connaître son opprobre. Mon âme s'est détachée d'elle, comme mon âme s'était détachée<!--Disloqué.--> de sa sœur.
23:19	Elle a multiplié ses prostitutions, se souvenant des jours de sa jeunesse quand elle se prostituait en terre d'Égypte.
23:20	Elle a eu une affection désordonnée pour ses concubins, dont la chair est chair d'ânes, et l’éjaculation éjaculation des chevaux.
23:21	Tu es revenue à la prostitution de ta jeunesse, lorsque les Égyptiens pressaient ta poitrine à cause des seins de ta jeunesse.
23:22	C'est pourquoi, Oholiba, ainsi parle Adonaï YHWH : Voici, je m'en vais réveiller contre toi ceux qui t’aimaient, ceux dont ton âme s'est détachée, et je les amènerai contre toi de toutes parts.
23:23	Les fils de Babel, et tous les Chaldéens, Pekod, Shoa, Koa, et tous les fils d'Assour avec eux, tous jeunes hommes désirables, gouverneurs et magistrats, grands seigneurs et renommés, tous montant à cheval.
23:24	Ils viendront contre toi avec des armes, des chars, et des roues, avec une multitude de peuples, avec le grand bouclier et le petit bouclier, avec les casques, et je leur mettrai le jugement en face, ils te jugeront selon leurs ordonnances.
23:25	Je mettrai ma jalousie contre toi, et ils agiront contre toi avec fureur. Ils te retrancheront le nez et les oreilles, et ce qui restera de toi tombera par l'épée. Ils enlèveront tes fils et tes filles, et ce qui restera de toi sera dévoré par le feu.
23:26	Ils te dépouilleront de tes vêtements, et t'enlèveront les ornements dont tu te pares.
23:27	Je ferai cesser tes méchancetés et tes prostitutions de la terre d'Égypte. Tu ne lèveras plus tes yeux vers eux, et tu ne te souviendras plus de l'Égypte.
23:28	Car ainsi parle Adonaï YHWH : Voici, je te livre entre les mains de ceux que tu hais, entre les mains de ceux dont ton âme s'est détachée.
23:29	Ils te traiteront avec haine, ils enlèveront tout le fruit de ton travail et te laisseront nue, entièrement nue. La nudité de tes adultères, tes prostitutions et tes fornications seront découvertes.
23:30	On te fera ces choses-là parce que tu t'es prostituée aux nations avec lesquelles tu t'es rendue impure par leurs idoles.
23:31	Tu as marché dans la voie de ta sœur, c'est pourquoi je mets sa coupe dans ta main.
23:32	Ainsi parle Adonaï YHWH : Tu boiras la coupe profonde et large de ta sœur. Elle sera une coupe d'une grande mesure, tu seras un sujet de rire et de moquerie<!--Ps. 75:9 ; Es. 51:17 ; Jé. 25:15.-->.
23:33	Tu seras remplie d'ivresse et de douleur, par la coupe de dévastation et de désolation, la coupe de ta sœur Samarie.
23:34	Tu la boiras et la videras, tu briseras ce pot de terre et tu déchireras ton sein. Car j'ai parlé, – déclaration d'Adonaï YHWH.
23:35	C'est pourquoi ainsi parle Adonaï YHWH : Parce que tu m'as oublié, et que tu m'as jeté derrière ton dos, aussi porteras-tu la peine de ta méchanceté et de tes prostitutions.

### Jugement sur Israël et Yéhouda

23:36	YHWH me dit : Fils d'humain, ne jugeras-tu pas Ohola et Oholiba ? Déclare-leur leurs abominations.
23:37	Car elles ont commis l'adultère et il y a du sang sur leurs mains : elles ont commis l'adultère avec leurs idoles, et même leurs fils qu'elles m'avaient enfantés, elles les leur ont fait passer pour être dévorés.
23:38	Elles m’ont fait encore ceci : Elles ont souillé mon sanctuaire ce même jour et elles ont profané mes shabbats.
23:39	Elles ont tué leurs fils pour leurs idoles, elles sont entrées ce même jour dans mon sanctuaire pour le profaner. Voilà ce qu'elles ont fait au milieu de ma maison<!--2 R. 21:4.-->.
23:40	Bien plus, elles ont envoyé chercher des hommes venant d'une terre éloignée, elles leur ont envoyé des messagers, et voici, ils sont venus. Pour eux tu t'es lavée, tu as mis du fard à tes yeux, et tu t'es parée d'ornements.
23:41	Tu t'es assise sur un lit magnifique, devant lequel une table était préparée, sur laquelle tu as mis mon encens et mon huile.
23:42	On entendait le bruit d'une foule tranquille. Parmi cette multitude d'hommes, on a fait venir du désert des Sabéens, qui leur ont mis des bracelets aux mains et des couronnes de beauté sur leurs têtes.
23:43	J'ai dit au sujet de celle qui avait vieilli dans les adultères : C’est elle maintenant qui se livre<!--se prostitue.--> à ses prostitutions !
23:44	Et on est venu vers elle comme on vient vers une femme prostituée. C'est ainsi qu'ils sont venus vers Ohola et vers Oholiba, femmes pleines de méchanceté.
23:45	Les hommes justes les jugeront comme on juge les femmes adultères, et comme on juge celles qui répandent le sang. Car elles sont adultères et le sang est sur leurs mains.
23:46	C'est pourquoi ainsi parle Adonaï YHWH : Qu'on fasse monter l'assemblée contre elles, et qu'elles soient abandonnées au tumulte et au pillage.
23:47	Que l'assemblée les lapide avec des pierres et les taille en pièces avec leurs épées ! Qu'ils tuent leurs fils et leurs filles, et qu'ils brûlent au feu leurs maisons !
23:48	Je ferai cesser la prostitution sur la terre. Et toutes les femmes recevront une leçon et elles n'imiteront pas votre prostitution.
23:49	On mettra sur vous votre prostitution, et vous porterez les péchés de vos idoles. Vous saurez que je suis Adonaï YHWH.

## Chapitre 24

### Malheur à la ville sanguinaire

24:1	La neuvième année, au dixième jour du dixième mois, la parole de YHWH vint à moi en disant :
24:2	Fils d'humain, écris, écris pour toi le nom du jour, ce jour même<!--Littéralement : dans l'os de ce jour.--> ! Car en ce même jour le roi de Babel s'approche contre Yeroushalaim<!--2 R. 25:1.-->.
24:3	Propose une parabole à la maison de la rébellion, et dis-leur : Ainsi parle Adonaï YHWH : Place la chaudière, place-la et verses-y aussi de l'eau.
24:4	Rassembles-y les morceaux, tous les bons morceaux, la cuisse, et l'épaule, et remplis-la des meilleurs os.
24:5	Prends la meilleure bête du troupeau, entasse aussi des os en dessous ! Fais bouillir ses bouillons ! Dedans, fais cuire aussi ses os !
24:6	C’est pourquoi ainsi parle Adonaï YHWH : Malheur à la ville des sangs, à la chaudière pleine de rouille et dont la rouille n'est pas sortie ! Vide-la morceau par morceau et que le sort ne soit pas jeté sur elle.
24:7	Parce que son sang est au milieu d'elle, qu'elle l'a mis sur le rocher brillant, et qu'elle ne l'a pas répandu sur la terre pour le couvrir de poussière,
24:8	j'ai mis son sang sur un rocher brillant afin qu'il ne soit pas couvert, pour faire monter ma fureur, et pour me venger en exécutant la vengeance.
24:9	C'est pourquoi ainsi parle Adonaï YHWH : Malheur à la ville des sangs ! J'en ferai aussi un grand tas de bois à brûler !
24:10	Amasse beaucoup de bois, allume le feu, fais cuire complètement la viande, ajoute les épices et que les os soient brûlés.
24:11	Place-la vide sur les charbons, afin qu’elle s’échauffe et que son cuivre devienne brûlant, que sa souillure soit fondue à l'intérieur et que sa rouille soit consumée.
24:12	Elle a lassé les efforts, et la rouille dont elle est pleine n'est pas sortie d'elle, au feu sa rouille !
24:13	L'impureté est dans ta méchanceté : puisque je t'ai purifiée et que tu n'as pas été pure, tu ne seras pas purifiée de ta souillure jusqu'à ce que j'aie assouvi sur toi ma fureur.
24:14	Moi, YHWH, j'ai parlé. Cela arrivera et je le ferai. Je ne reculerai pas, je n'épargnerai pas non plus, ni ne me repentirai. On t'a jugée selon tes voies et selon tes actions, – déclaration d'Adonaï YHWH.

### La vie de Yehezkel, un signe pour Israël

24:15	La parole de YHWH vint à moi en disant :
24:16	Fils d'humain, voici, je vais t’enlever par une plaie<!--« Coup », « massacre », « fléau », « pestilence », « frapper » ; « coup fatal » ; « massacre (d'une bataille) » ; « peste », « fléau », « plaie (jugement divin) ».--> les délices de tes yeux. Ne mène pas de deuil, ne pleure pas, ne fais pas couler tes larmes<!--Jé. 16:6-7.-->.
24:17	Gémis en silence, ne fais pas le deuil des morts, noue ton turban<!--Ex. 39:28.-->, mets tes sandales à tes pieds, ne te couvre pas la barbe et ne mange pas le pain des hommes<!--Lé. 10:6.-->.
24:18	J'avais parlé au peuple le matin, et ma femme mourut le soir. Le matin je fis comme il m'avait été ordonné.
24:19	Le peuple me dit : Ne nous déclareras-tu pas ce que signifient ces choses-là que tu fais ?
24:20	Je leur dis : La parole de YHWH est venue à moi en disant :
24:21	Parle à la maison d'Israël : Ainsi parle Adonaï YHWH : Voici, je vais profaner mon sanctuaire, l'orgueil de votre force, les délices de vos yeux et qui est l'objet de compassion de votre âme. Vos fils et vos filles que vous avez abandonnés tomberont par l'épée.
24:22	Vous ferez comme j'ai fait. Vous ne couvrirez pas vos barbes et vous ne mangerez pas le pain des hommes.
24:23	Turbans en tête et sandales aux pieds, vous ne mènerez pas de deuil et vous ne pleurerez, mais vous pourrirez à cause de vos iniquités et vous gémirez, chaque homme près de son frère.
24:24	Yehezkel deviendra pour vous un signe. Tout ce qu’il a fait, vous le ferez. Quand cela sera arrivé, vous saurez que je suis Adonaï YHWH.
24:25	Quant à toi, fils d'humain, le jour où je leur enlèverai leur force, leur joie et leur gloire, les délices de leurs yeux et le désir de leurs âmes, leurs fils et leurs filles,
24:26	en ce jour-là, un rescapé viendra chez toi pour le faire entendre aux oreilles.
24:27	En ce jour-là, ta bouche s'ouvrira avec le rescapé et tu parleras, tu ne seras plus muet. Tu deviendras un signe pour eux et ils sauront que je suis YHWH.

## Chapitre 25

### Jugement d'Elohîm sur Ammon

25:1	La parole de YHWH vint à moi en disant :
25:2	Fils d'humain, mets tes faces vers les fils d'Ammon, et prophétise contre eux<!--Jé. 49:1.-->.
25:3	Dis aux fils d'Ammon : Écoutez la parole d'Adonaï YHWH ! Ainsi parle Adonaï YHWH : Parce que vous avez dit : Ah ! ah ! contre mon sanctuaire, parce qu'il était profané, contre le sol d'Israël, parce qu'il était dévasté et contre la maison de Yéhouda, parce qu'ils allaient en captivité<!--Am. 1:13 ; So. 2:8.-->,
25:4	à cause de cela, voici, je vais te donner en héritage aux fils d'orient, ils bâtiront des palais dans tes villes et ils placeront chez toi leurs tabernacles. Ce sont eux qui mangeront tes fruits et qui boiront ton lait.
25:5	Je ferai de Rabba une demeure pour les chameaux et les fils d'Ammon un endroit de repos pour les brebis. Vous saurez que je suis YHWH.
25:6	Car ainsi parle Adonaï YHWH : Parce que tu as battu des mains et frappé du pied, que tu t'es réjoui l'âme pleine de dédain au sujet du sol d'Israël,
25:7	à cause de cela, voici, j'étends ma main sur toi, je te livre en proie aux nations, je te retranche du milieu des peuples, je te fais périr d'entre les terres et je te détruis. Et tu sauras que je suis YHWH.

### Jugement sur Moab

25:8	Ainsi parle Adonaï YHWH : Parce que Moab et Séir ont dit : Voici, la maison de Yéhouda est comme toutes les autres nations ;
25:9	à cause de cela voici, j'ouvre le territoire de Moab du côté des villes, de ses villes frontières, la beauté de la terre de Beth-Yeshiymoth, de Baal-Meon et de Qiryathayim<!--No. 32:38 ; Jos. 12:3.-->,
25:10	je l'ouvre aux fils d'orient, qui sont au-delà de la terre des fils d'Ammon, je leur donne en possession, afin qu'on ne se souvienne plus des fils d'Ammon parmi les nations.
25:11	J'exercerai aussi des jugements contre Moab, et ils sauront que je suis YHWH.

### Jugement sur Édom

25:12	Ainsi parle Adonaï YHWH : À cause de ce qu'Édom a fait, se vengeant de vengeance contre la maison de Yéhouda, et parce qu'il s'est rendu coupable, il s'est rendu coupable en se vengeant d'eux<!--Ps. 137:7.-->,
25:13	à cause de cela, Adonaï YHWH dit : J'étends ma main sur Édom, j'en retranche les humains et les bêtes, et j'en fais un lieu laissé en ruines. Depuis Théman à Dedan, ils tomberont par l'épée<!--Jé. 49:7-9 ; Am. 1:12 ; Ab. 1:9.-->.
25:14	J'exercerai ma vengeance contre Édom par la main de mon peuple d'Israël. On traitera Édom selon ma colère et selon ma fureur. Ils connaîtront ma vengeance, – déclaration d'Adonaï YHWH.

### Jugement sur les Philistins

25:15	Ainsi parle Adonaï YHWH : Puisque les Philistins ont agi par vengeance, se vengeant de vengeance, dans le mépris de leurs âmes, en voulant tout détruire dans leur haine éternelle,
25:16	à cause de cela Adonaï YHWH dit : Voici, je vais étendre ma main sur les Philistins, j'exterminerai les Kéréthiens, et je ferai périr le reste sur le rivage de la mer.
25:17	J'exercerai sur eux de grandes vengeances par des châtiments de fureur. Ils sauront que je suis YHWH, quand j'aurai exécuté sur eux ma vengeance<!--Es. 14:29 ; Jé. 25:18-21 ; So. 2:5-7.-->.

## Chapitre 26

### Jugement sur Tyr

26:1	Il arriva dans la onzième année, le premier jour du mois, que la parole de YHWH vint à moi en disant :
26:2	Fils d'humain, parce que Tyr a dit au sujet de Yeroushalaim : Ah ! Elle est brisée, la porte des peuples ! On se tourne vers moi, je me remplirai, elle est en ruine<!--Am. 1:9 ; Za. 9:2-3.--> !
26:3	C’est pourquoi ainsi parle Adonaï YHWH : Voici, j'en veux à toi, Tyr ! Et je ferai monter contre toi des nations nombreuses, comme la mer fait monter ses flots<!--Jé. 51:42.-->.
26:4	Elles détruiront les murailles de Tyr, elles renverseront ses tours et j'en raclerai sa poussière. Je la rendrai semblable à un rocher nu<!--Es. 23:15.-->.
26:5	Elle deviendra au milieu de la mer un lieu où l'on étend les filets. Oui, moi, j’ai parlé, – déclaration d'Adonaï YHWH. Elle deviendra la proie des nations.
26:6	Ses filles sur sa terre seront tuées par l'épée, et elles sauront que je suis YHWH.
26:7	Car ainsi parle Adonaï YHWH : Voici, je vais faire venir du nord contre Tyr, Neboukadnetsar, roi de Babel, le roi des rois, avec des chevaux, des chars, des cavaliers, et un grand peuple assemblé de toutes parts.
26:8	Il tuera par l'épée tes filles sur ta terre, il fera des remparts contre toi, il dressera des tertres contre toi, et il lèvera les boucliers contre toi.
26:9	Il donnera des coups de bélier contre tes murs, et renversera tes tours avec ses épées.
26:10	La multitude de ses chevaux te couvrira de poussière, tes murs trembleront au bruit des cavaliers, des roues, et des chars, quand il entrera par tes portes, comme on entre dans une ville qu'on a divisée.
26:11	Il foulera toutes tes rues avec les sabots de ses chevaux, il tuera ton peuple par l'épée et les monuments de ta force tomberont par terre<!--Es. 5:8 ; Jé. 47:3.-->.
26:12	Ils feront leur butin de vos richesses, ils pilleront tes marchandises, ils renverseront tes murs, ils abattront tes maisons désirables et ils jetteront au milieu des eaux tes pierres, ton bois et ta poussière.
26:13	Je ferai cesser le bruit de tes chansons et le son de tes harpes ne sera plus entendu.
26:14	Je te rendrai semblable à un rocher nu, tu seras un lieu pour étendre les filets et tu ne seras plus rebâtie. Car moi, YHWH, j'ai parlé, – déclaration d'Adonaï YHWH.
26:15	Ainsi parle Adonaï YHWH, à Tyr : Les îles ne tremblent-elles pas du bruit de ta ruine, quand ceux qui sont blessés mortellement gémissent, quand le carnage se fait au milieu de toi ?
26:16	Tous les princes de la mer descendent de leurs trônes. Ils ôtent leurs robes et se dépouillent de leurs vêtements brodés. Ils s'enveloppent de terreur et s'asseyent sur la terre. Ils sont effrayés à chaque instant et ils sont frappés de stupeur à cause de toi.
26:17	Ils élèveront sur toi un chant funèbre et te diront : Comment as-tu péri, toi qui étais fréquentée par ceux qui vont sur la mer, ville renommée, qui étais forte dans la mer, toi et tes habitants, qui inspiraient la terreur à tous ceux qui habitent chez elle<!--Es. 23:15-16 ; Ap. 18:9.--> ?
26:18	Maintenant les îles seront effrayées au jour de ta ruine, et les îles qui sont dans la mer seront terrifiées à cause de ta fuite.
26:19	Car ainsi parle Adonaï YHWH : Quand je ferai de toi une ville déserte, comme sont les villes qui ne sont pas habitées, quand je ferai tomber sur toi l'abîme, et que les grosses eaux te couvriront ;
26:20	alors je te ferai descendre avec ceux qui descendent dans la fosse, vers le peuple d'autrefois, et je te placerai dans les parties inférieures de la Terre, dans les lieux laissés en ruines depuis longtemps, avec ceux qui descendent dans la fosse, afin que tu ne sois plus habitée, mais je donnerai la gloire pour la Terre des vivants.
26:21	Je ferai de toi une terreur et tu ne seras plus. Quand on te cherchera, on ne te trouvera plus jamais, – déclaration d'Adonaï YHWH.

## Chapitre 27

### Lamentation sur Tyr<!--Ap. 18:1-24.-->

27:1	La parole de YHWH vint à moi en disant :
27:2	Toi, fils d'humain, élève un chant funèbre sur Tyr.
27:3	Tu diras à Tyr : Toi qui demeures au bord de la mer, qui trafiques avec les peuples dans beaucoup d'îles : ainsi parle Adonaï YHWH : Tyr, tu disais : Je suis parfaite en beauté !
27:4	Ton territoire est au cœur de la mer, ceux qui t'ont bâtie t'ont rendue parfaite en beauté.
27:5	Ils ont bâti toutes tes planches en cyprès de Sheniyr, ils ont pris les cèdres du Liban pour te fabriquer des mâts.
27:6	Ils ont fait tes rames avec des chênes du Bashân et tes planches avec de l'ivoire fait dans du buis par les filles et importé d'Assyrie et des îles de Kittim.
27:7	Le fin lin d'Égypte avec des broderies te servait de voiles et de pavillon, des étoffes teintes en violet et en pourpre rouge des îles d'Éliyshah formaient tes couvertures.
27:8	Les habitants de Sidon et d'Arvad étaient tes rameurs, Tyr ! Les plus sages chez toi étaient tes pilotes.
27:9	Les anciens de Guebal et ses sages étaient chez toi pour réparer tes brèches, tous les navires de la mer et leurs mariniers étaient chez toi pour faire l'échange de tes marchandises.
27:10	La Perse, Loud, Pouth étaient dans ton armée, c'étaient des hommes de guerre. Ils suspendaient chez toi le bouclier et le casque, ils te donnaient de la splendeur.
27:11	Les fils d'Arvad avec ton armée étaient tout autour de tes murs et des hommes braves occupaient tes tours. Ils suspendaient leurs boucliers à tous tes murs, ils te rendaient parfaite en beauté.
27:12	Tarsis trafiquait avec toi à cause de la multitude de toutes tes richesses, fournissant tes marchés d'argent, de fer, d'étain et de plomb. 
27:13	Yavan, Toubal et Méshek trafiquaient avec toi, te fournissant en marchandises : des âmes humaines<!--Voir Ap. 18:13.--> et des ustensiles de cuivre.
27:14	La maison de Togarmah fournissait tes marchés de chevaux, de cavaliers et de mulets.
27:15	Les fils de Dedan trafiquaient avec toi. Tu avais dans ta main le commerce de beaucoup d'îles. On te donnait en retour des cornes d’ivoire et de l'ébène.
27:16	La Syrie trafiquait avec toi à cause de la multitude de tes produits. Elle fournissait tes marchés d'escarboucles, d'écarlate, de broderie, de byssus, de corail et d'agate.
27:17	Yéhouda et la terre d'Israël trafiquaient avec toi, te fournissant en marchandises du blé de Minnith, des pâtisseries, du miel, de l'huile et du baume.
27:18	Damas trafiquait avec toi pour la multitude de tes produits, pour la multitude de tes richesses, en vin de Helbon<!--ville à quelques kilomètres au nord-ouest de Damas, 'Helbon' moderne, encore réputée pour ses raisins.--> et en laine blanche.
27:19	Vedan et Yavan, depuis Ouzal, fournissaient tes marchés en fer luisant, en casse et en roseau. C’était pour tes marchandises.
27:20	Dedan trafiquait avec toi en couvertures pour s'asseoir à cheval.
27:21	Le commerce des Arabes et de tous les princes de Qedar passait par tes mains. Avec eux tu faisais commerce d'agneaux, de béliers et de boucs.
27:22	Les marchands de Séba et de Ra`mah trafiquaient avec toi de tous les meilleurs aromates, de toute sorte de pierres précieuses et d'or.
27:23	Charan, Canné, et Éden, les marchands de Séba, d'Assyrie, de Kilmad, trafiquaient avec toi.
27:24	Ils trafiquaient avec toi en toutes sortes de belles choses, en manteaux teints en violet, en broderie, en riches étoffes contenues dans des coffres attachés avec des cordes, faits en bois de cèdre et amenés sur tes marchés.
27:25	Les navires de Tarsis naviguaient pour ton commerce. Tu étais au comble de la force et de la richesse, au cœur des mers.
27:26	Tes rameurs t'ont amenée dans de grosses eaux, le vent d'orient t'a brisée au cœur de la mer.
27:27	Tes richesses, tes marchés et tes marchandises, tes mariniers et tes pilotes, ceux qui réparent tes brèches, et ceux qui s'occupent de ton commerce, tous tes hommes de guerre qui sont chez toi, et toute ta multitude au milieu de toi, tomberont dans le cœur de la mer au jour de ta ruine<!--Ap. 18:9.-->.
27:28	Les faubourgs trembleront au bruit du cri de tes pilotes.
27:29	Tous ceux qui manient la rame descendront de leurs navires, les mariniers et tous les pilotes de la mer se tiendront debout sur la terre.
27:30	Ils feront entendre leur voix et crieront leur amertume. Ils jetteront de la poussière sur leurs têtes, se vautreront dans la cendre,
27:31	ils se rendront chauves à cause de toi par une calvitie et se ceindront de sacs. Ils pleureront sur toi, dans l'amertume de leur âme, avec d’amers gémissements.
27:32	Ils élèveront sur toi un chant funèbre dans leur gémissement, ils chanteront un chant funèbre sur toi : Qui était comme Tyr, comme cette ville détruite au cœur de la mer ?
27:33	Quand tes marchandises sortaient des mers, tu rassasiais beaucoup de peuples. Par la multitude de tes richesses et de tes marchandises, tu enrichissais les rois de la Terre.
27:34	Quand tu as été brisée par la mer, c'est dans les profondeurs des eaux que sont tombées tes marchandises et toute l'assemblée qui était au milieu de toi.
27:35	Tous les habitants des îles sont dans la stupeur à cause de toi, leurs rois sont saisis de terreur, leur visage est bouleversé.
27:36	Les marchands parmi les peuples sifflent sur toi, tu es un objet de terreur. Tu n'existeras plus jamais !

## Chapitre 28

### YHWH réprime l'arrogance du roi de Tyr

28:1	La parole de YHWH vint à moi en disant :
28:2	Fils d'humain, dis au prince de Tyr : Ainsi parle Adonaï YHWH : Parce que ton cœur s'est élevé et que tu as dit : Je suis El, je suis assis sur le siège d'Elohîm, au cœur de la mer ! Quoique tu sois un être humain et non El, tu as donné ton cœur comme le cœur d'Elohîm !
28:3	Voici, tu es plus sage que Daniye'l, rien de secret n'est obscur pour toi.
28:4	Par ta sagesse et par ton intelligence, tu as produit de la richesse : tu as produit de l'or et de l'argent pour tes trésors<!--Za. 9:2-3.-->.
28:5	Par la multiplicité de ta sagesse et par ton commerce, tu as multiplié ta richesse, et ton cœur s'est élevé à cause de ta richesse.
28:6	C'est pourquoi ainsi parle Adonaï YHWH : Parce que tu as donné ton cœur comme cœur d'Elohîm,
28:7	c’est pourquoi, voici, je vais faire venir contre toi des étrangers, les plus terrifiants parmi les nations. Ils tireront leurs épées sur la beauté de ta sagesse et ils profaneront ta splendeur.
28:8	Ils te feront descendre dans la fosse et tu mourras d'une maladie mortelle, comme ceux qui sont blessés mortellement au cœur de la mer.
28:9	En face de ton meurtrier, diras-tu : Je suis Elohîm ? Tu es un être humain et non El, sous la main de celui qui te tuera.
28:10	Tu mourras de la mort des incirconcis par la main des étrangers. Car j'ai parlé, – déclaration d'Adonaï YHWH.

### Chute du roi de Tyr représentant Satan<!--Es. 14:12-17.-->

28:11	La parole de YHWH vint à moi en disant :
28:12	Fils d'humain, élève un chant funèbre sur le roi de Tyr, et dis-lui : Ainsi parle Adonaï YHWH : Toi, le sceau du modèle, plein de sagesse, et parfait en beauté.
28:13	Tu étais en Éden, le jardin d'Elohîm. Ta couverture était de pierres précieuses de toutes sortes : de rubis, de topaze, de diamant, de chrysolithe, d'onyx, de jaspe, de saphir, d'escarboucle, d'émeraude, et d'or. Tes tambourins et tes flûtes étaient à ton service, préparés pour le jour où tu fus créé.
28:14	Toi, un chérubin oint pour protéger. Je t'avais mis sur la sainte montagne d'Elohîm, tu y étais, tu marchais au milieu des pierres de feu.
28:15	Tu étais intègre dans tes voies dès le jour où tu fus créé, jusqu'à celui où l'injustice fut trouvée en toi.
28:16	Par la multiplication de ton commerce<!--Satan est le premier commerçant. Il avait transformé ses sanctuaires célestes en un lieu de trafic, en un marché.--> ton intérieur a été rempli de violence, et tu as péché. C'est pourquoi je te jette comme une chose souillée hors de la montagne d'Elohîm<!--Ap. 12:1-12.-->, et je te détruis du milieu des pierres de feu, chérubin protecteur !
28:17	Ton cœur s'est élevé à cause de ta beauté, tu as corrompu ta sagesse à cause de ta splendeur. Je te jette par terre, je te donne en spectacle aux rois, afin qu'ils te regardent.
28:18	Par la multitude de tes iniquités, par l'injustice de ton commerce, tu as souillé tes sanctuaires. Je fais sortir du milieu de toi un feu qui te dévore, je te réduis en cendres sur la terre, en présence de tous ceux qui te regardent.
28:19	Tous ceux qui te connaissent parmi les peuples sont frappés de stupeur à cause de toi, tu es devenu une terreur. Tu n'existeras plus jamais !

### Jugement sur Sidon

28:20	La parole de YHWH vint à moi en disant :
28:21	Fils d'humain, mets tes faces vers Sidon, et prophétise contre elle.
28:22	Tu diras : Ainsi parle Adonaï YHWH : Voici j'en veux à toi, Sidon ! Je serai glorifié au milieu de toi. Et on saura que je suis YHWH, quand j'aurai exercé des jugements contre elle et que je serai sanctifié.
28:23	J'enverrai la peste dans son sein, je ferai couler le sang dans ses rues. Les blessés mortellement tomberont au milieu d'elle par l'épée qui viendra de toutes parts sur elle. Et ils sauront que je suis YHWH.
28:24	Elle ne sera plus pour la maison d'Israël une épine qui blesse, une ronce qui cause de douleur, parmi tous ceux qui l'entourent et qui la méprisent. Et ils sauront que je suis Adonaï YHWH.

### Rétablissement d'Israël

28:25	Ainsi parle Adonaï YHWH : Quand j'aurai rassemblé la maison d'Israël d'entre les peuples parmi lesquels ils auront été dispersés, je manifesterai en elle ma sainteté aux yeux des nations. Ils habiteront sur leur sol que j'ai donné à mon serviteur Yaacov.
28:26	Ils y habiteront en sûreté. Ils bâtiront des maisons et planteront des vignes. Ils y habiteront en sûreté quand j'aurai exercé des jugements contre ceux qui les méprisent à l'entour. Et ils sauront que je suis YHWH, leur Elohîm.

## Chapitre 29

### Jugement sur l'Égypte

29:1	La dixième année, au douzième jour du dixième mois, la parole de YHWH vint à moi en disant :
29:2	Fils d'humain, mets tes faces contre pharaon, roi d'Égypte, prophétise contre lui, et contre toute l'Égypte<!--Jé. 43:8-11.--> !
29:3	Parle, et dis : Ainsi parle Adonaï YHWH : Voici, j'en veux à toi, pharaon, roi d'Égypte, grand monstre marin couché au milieu de tes fleuves, qui dis : Mon fleuve est à moi, c'est moi qui l'ai fait<!--Es. 27:1 ; Ps. 74:13-14.--> !
29:4	Je mettrai des crochets à tes mâchoires, j'attacherai à tes écailles les poissons de tes fleuves et je te ferai monter du milieu de tes fleuves, avec tous les poissons de tes fleuves, qui seront attachés à tes écailles.
29:5	Je t'abandonnerai dans le désert, toi, et tous les poissons de tes fleuves. Tu tomberas sur les faces des champs, tu ne seras ni recueilli ni rassemblé. Je te donnerai en guise de nourriture aux bêtes de la Terre et aux oiseaux des cieux.
29:6	Et tous les habitants d'Égypte sauront que je suis YHWH. C'est parce qu'ils auront été pour la maison d'Israël un bâton qui n'était qu'un roseau<!--2 R. 18:21 ; Es. 36:6.-->.
29:7	Quand ils t'ont saisi par la paume, tu t'es écrasé et tu leur as percé toute l'épaule. Quand ils se sont appuyés sur toi, tu t'es brisé et tu as rendu leurs reins immobiles.
29:8	C'est pourquoi, ainsi parle Adonaï YHWH : Voici, je ferai venir l'épée sur toi et j'exterminerai du milieu de toi les humains et les bêtes.
29:9	La terre d'Égypte deviendra une dévastation, un lieu laissé en ruines, et ils sauront que je suis YHWH, parce qu'il a dit : Les fleuves sont à moi, et je les ai faits !
29:10	C'est pourquoi voici, j'en veux à toi, et à tes fleuves. Je ferai de la terre d'Égypte un lieu laissé en ruines, une sécheresse et une dévastation, depuis Migdol jusqu'à Syène, et aux frontières de l'Éthiopie.
29:11	Aucun pied d'humain n'y passera, il n'y passera non plus aucun pied d'animal et il ne sera pas habité pendant 40 ans.
29:12	Je ferai de la terre d'Égypte une dévastation au milieu des terres dévastées, et ses villes deviendront, au milieu des villes en ruine une dévastation pendant 40 ans. Je disperserai les Égyptiens au milieu des nations, et je les répandrai au milieu des terres.
29:13	Car ainsi parle Adonaï YHWH : Au bout de 40 ans, je rassemblerai les Égyptiens d'entre les peuples parmi lesquels ils auront été dispersés.
29:14	Je ramènerai les captifs d'Égypte, et les ferai retourner en terre de Pathros, sur la terre de leur origine, et là ils deviendront un humble royaume.
29:15	Il deviendra le plus humble de tous les royaumes, et il ne s'élèvera plus au-dessus des nations, je le diminuerai, afin qu'il ne domine pas sur les nations.
29:16	Ce royaume ne sera plus pour la main d'Israël un sujet de confiance, mais il lui rappellera son iniquité quand elle se tournait vers lui. Et ils sauront que je suis Adonaï YHWH.
29:17	Il arriva la vingt-septième année, au premier jour du premier mois, que la parole de YHWH vint à moi en disant :
29:18	Fils d'humain, Neboukadnetsar, roi de Babel, a fait faire à son armée un service pénible contre Tyr : toutes les têtes en sont devenues chauves, et toutes les épaules en sont écorchées. Il n'a pas eu de salaire, ni lui ni son armée, à cause de Tyr, pour le service qu'il a fait contre elle.
29:19	C'est pourquoi, ainsi parle Adonaï YHWH : Voici, je vais donner à Neboukadnetsar, roi de Babel, la terre d'Égypte. Il en emportera les richesses, il y prendra un butin et il la pillera. Ce sera là le salaire de son armée.
29:20	En récompense de son travail contre Tyr, je lui ai donné la terre d'Égypte, parce qu'ils ont travaillé pour moi, – déclaration d'Adonaï YHWH.
29:21	En ce jour-là, je ferai germer la corne<!--Il est question du Mashiah. Voir 2 S. 22:3 ; Es. 5:1 ; Ps. 18:3 ; Lu. 1:69.--> de la maison d'Israël, et je te donnerai une bouche ouverte au milieu d'eux. Et ils sauront que je suis YHWH.

## Chapitre 30

### Disgrâce de l'Égypte

30:1	La parole de YHWH vint à moi en disant :
30:2	Fils d'humain, prophétise, et dis : Ainsi parle Adonaï YHWH : Hurlez, et dites : Hélas ! Quel jour !
30:3	Car le jour est proche, le jour de YHWH<!--Voir commentaire en Za. 14:1.--> est proche, un jour de nuée<!--Es. 19:1 ; Na. 1:3 ; Ap. 1:7.-->. Ce sera le temps des nations.
30:4	L'épée viendra sur l'Égypte et la terreur sera dans l'Éthiopie, quand ceux qui seront blessés mortellement tomberont dans l'Égypte, quand on enlèvera la multitude de son peuple et que ses fondements seront renversés.
30:5	L'Éthiopie, Pouth, Loud et tous les peuples mélangés, Koub<!--« Kuwb » qui signifie « une horde », le nom d'un peuple allié avec Neboukadnetsar et probablement situé au nord de l'Afrique, peut-être la Libye.--> et les fils de la terre de l’alliance tomberont par l'épée avec eux<!--Jé. 46:9 ; Na. 3:9-10.-->.
30:6	Ainsi parle YHWH : Ceux qui soutiendront l'Égypte, tomberont, et l'orgueil de sa force sera abaissé. Ils tomberont par l'épée de Migdol à Syène, – déclaration d'Adonaï YHWH.
30:7	Ils seront dévastés parmi les terres dévastées et ses villes seront au milieu des villes désertes.
30:8	Ils sauront que je suis YHWH, quand je mettrai le feu à l'Égypte et que tous ses soutiens seront brisés.
30:9	En ce jour-là, des messagers sortiront de ma part sur des navires pour effrayer l'Éthiopie dans sa sécurité. Il y aura de la terreur parmi eux lorsque le jour de l'Égypte sera venu, car le voici qui arrive !
30:10	Ainsi parle Adonaï YHWH : Je ferai périr la multitude d'Égypte par la main de Neboukadnetsar, roi de Babel.
30:11	Lui et son peuple avec lui, les plus terrifiants des nations, seront amenés pour détruire la terre. Ils tireront leurs épées contre les Égyptiens et rempliront la terre de blessés mortellement.
30:12	Je mettrai à sec les fleuves et je livrerai la terre entre les mains des méchants. Je dévasterai la terre et tout ce qu'elle contient, par la main des étrangers. C'est moi, YHWH, qui ai parlé.
30:13	Ainsi parle Adonaï YHWH : Je détruirai les idoles et j'exterminerai<!--Vient de « shabath » qui signifie « cesser », « se désister », « se reposer » ou « chômer ».--> les faux elohîm de Noph. Il n'y aura pas de prince qui soit de la terre d'Égypte et je mettrai la frayeur en terre d'Égypte<!--Es. 19:1-13 ; Jé. 43:12, 46:13.-->.
30:14	Je dévasterai Pathros, je mettrai le feu à Tsoan, et j'exercerai mes jugements sur No<!--Jé. 44:1.-->.
30:15	Je répandrai ma fureur sur Sîn, qui est la place forte de l'Égypte et j'exterminerai la multitude qui est à No.
30:16	Je mettrai le feu en Égypte. Sîn sera dans l'angoisse, elle sera dans l'angoisse, No sera rompue par diverses brèches et Noph sera dans la détresse en plein jour.
30:17	Les jeunes hommes d'On et de Pi-Béseth tomberont par l'épée, et ces villes iront en captivité.
30:18	Le jour s'obscurcira à Tachpanès, lorsque j'y romprai le joug de l'Égypte et que l'orgueil de sa force cessera. Un nuage la couvrira, et les villes de son ressort iront en captivité.
30:19	J'exercerai des jugements en Égypte. Et ils sauront que je suis YHWH.

### Chute et dispersion de l'Égypte

30:20	Et il arriva que dans la onzième année, au septième jour du premier mois, la parole de YHWH vint à moi en disant :
30:21	Fils d'humain, j'ai brisé le bras de pharaon, roi d'Égypte, et voici on ne l'a pas bandé en lui donnant des médicaments, on ne lui a pas mis de linges pour le bander et pour le fortifier afin qu'il puisse manier l'épée.
30:22	C'est pourquoi ainsi parle Adonaï YHWH : Voici, j'en veux à pharaon, roi d'Égypte. Je lui briserai les bras, celui qui est fort et celui qui est brisé, et je ferai tomber l'épée de sa main.
30:23	Je disperserai les Égyptiens parmi les nations, je les éparpillerai parmi les terres.
30:24	Je fortifierai les bras du roi de Babel et je lui mettrai mon épée dans la main. Mais je romprai les bras de pharaon, et il gémira devant lui des gémissements d'un blessé mortellement.
30:25	Je fortifierai les bras du roi de Babel, mais les bras de pharaon tomberont. Et on saura que je suis YHWH, quand je mettrai mon épée dans la main du roi de Babel et qu'il la tournera contre la terre d'Égypte.
30:26	Je disperserai les Égyptiens parmi les nations, je les éparpillerai parmi les terres. Et ils sauront que je suis YHWH.

## Chapitre 31

### Avertissement contre l'arrogance de pharaon

31:1	Il arriva dans la onzième année, au premier jour du troisième mois, que la parole de YHWH vint à moi en disant :
31:2	Fils d'humain, parle à pharaon, roi d'Égypte, et à la multitude de son peuple : À qui ressembles-tu dans ta grandeur ?
31:3	Voici, l'Assyrie était un cèdre du Liban, ayant de belles branches et des rameaux produisant de l'ombre, et une taille si élevée que sa cime s'élançait entre les buissons touffus.
31:4	Les eaux l'ont fait croître, l'abîme l'a fait pousser en hauteur, ses fleuves ont coulé autour de ses plantes, et il a envoyé ses eaux abondantes vers tous les arbres des champs.
31:5	C'est pourquoi il s'est élevé au-dessus de tous les autres arbres des champs. Ses branches se sont multipliées et ses rameaux croissaient par les grandes eaux qui faisaient pousser ses branches.
31:6	Tous les oiseaux des cieux ont fait leurs nids dans ses branches, toutes les bêtes des champs ont fait leurs petits sous ses rameaux, et toutes les grandes nations ont habité sous son ombre.
31:7	Il était beau par sa grandeur, et par l'étendue de ses branches, parce que sa racine était sur de grandes eaux.
31:8	Les cèdres du jardin d'Elohîm ne l'obscurcissaient pas, les cyprès ne ressemblaient pas à ses branches et les platanes n'étaient pas comme ses rameaux. Aucun arbre du jardin d'Elohîm ne lui ressemblait en beauté.
31:9	Je l’avais rendu beau par la multitude de ses rameaux, au point que tous les arbres d'Éden, qui étaient dans le jardin d'Elohîm,l’enviaient.
31:10	C'est pourquoi ainsi parle Adonaï YHWH : Parce qu'il s'est élevé, parce qu'il lançait sa cime au milieu d'épais rameaux et que son cœur était fier de sa hauteur,
31:11	je l'ai livré aux mains de l’el<!--L'hébreu « el » signifie « fort », « puissant », « divinités », « fausses divinités (démons, imaginations) ».--> des nations qui le traitera, le traitera selon sa méchanceté. Je l'ai chassé.
31:12	Les étrangers les plus terrifiants parmi les nations l'ont coupé et abandonné. Ses branches sont tombées sur les montagnes et sur toutes les vallées. Ses rameaux se sont brisés dans tous les ravins de la terre. Tous les peuples de la terre se sont retirés de dessous son ombre et l'ont abandonné.
31:13	Tous les oiseaux des cieux se sont installés sur ses ruines et toutes les bêtes des champs sont venues vivre parmi ses rameaux.
31:14	Afin que tous les arbres près des eaux n'élèvent plus leur hauteur, et qu'ils ne lancent plus leur cime au milieu d'épais rameaux, afin que tous les chênes arrosés d'eau ne gardent plus leur hauteur. Car tous sont livrés à la mort, aux parties inférieures de la Terre, parmi les fils des humains, avec ceux qui descendent dans la fosse.
31:15	Ainsi parle Adonaï YHWH : Le jour où il est descendu dans le shéol, j'ai répandu le deuil sur lui, j'ai couvert l'abîme devant lui, j'ai empêché ses fleuves de couler, et les grosses eaux ont été retenues. J'ai mis le Liban en deuil à cause de lui, et tous les arbres des champs ont été desséchés.
31:16	J'ai ébranlé les nations par le bruit de sa ruine, quand je l'ai fait descendre dans le shéol, avec ceux qui descendent dans la fosse<!--Es. 14:9.-->. Et tous les arbres d'Éden, les plus beaux et les plus agréables du Liban, tous arrosés par les eaux, ont été consolés dans les parties inférieures de la Terre.
31:17	Eux aussi sont descendus avec lui dans le shéol, vers ceux qui sont blessés mortellement par l'épée, ils étaient son bras et ils habitaient sous son ombre parmi les nations.
31:18	À qui ressembles-tu ainsi en gloire et en grandeur parmi les arbres d'Éden ? Tu seras précipité avec les arbres d'Éden dans les parties inférieures de la Terre, tu seras gisant au milieu des incirconcis, avec ceux qui sont blessés mortellement par l'épée. Voilà pharaon et toute sa multitude ! – déclaration d'Adonaï YHWH.

## Chapitre 32

### Lamentation sur la terre d'Égypte

32:1	Il arriva, la douzième année, le premier jour du douzième mois, que la parole de YHWH vint à moi en disant :
32:2	Fils d'humain, élève un chant funèbre sur pharaon, roi d'Égypte, et dis-lui : Tu ressemblais au lionceau des nations, toi, comme le dragon<!--Peut-être le dinosaure disparu 'plesiosaurus', ou une baleine.--> dans les mers. Tu t'élançais dans tes fleuves, tu troublais les eaux avec tes pieds, tu foulais aux pieds les fleuves.
32:3	Ainsi parle Adonaï YHWH : J'étendrai mon filet sur toi, dans une assemblée nombreuse de peuples, et ils te tireront dans mes filets.
32:4	Je te laisserai à terre, je te jetterai sur les faces des champs. Je ferai habiter sur toi tous les oiseaux des cieux et les bêtes de toute la terre se rassasieront de toi.
32:5	Je mettrai ta chair sur les montagnes et je remplirai les vallées de tes débris.
32:6	J'arroserai de ton sang jusqu'aux montagnes, la terre où tu nages et les ravins seront remplis de toi.
32:7	Quand je t'éteindrai, je couvrirai les cieux et j'obscurcirai leurs étoiles, je couvrirai le soleil de nuages, et la lune ne fera pas briller sa lumière<!--Es. 13:10 ; Joë. 3:4 ; Mt. 24:29.-->.
32:8	J'obscurcirai à cause de toi tous les luminaires de lumière des cieux, et je mettrai les ténèbres sur ta terre, – déclaration d'Adonaï YHWH.
32:9	J'affligerai le cœur de beaucoup de peuples en faisant venir ta ruine parmi les nations, sur des terres que tu ne connaissais pas.
32:10	Je frapperai de stupeur beaucoup de peuples à cause de toi, leurs rois seront saisis de terreur à cause de toi quand je ferai voler mon épée devant eux. Ils trembleront à tout instant, chaque homme pour son âme, lorsque le jour de ta chute sera venu.
32:11	Car ainsi parle Adonaï YHWH : L'épée du roi de Babel viendra sur toi.
32:12	J'abattrai ta multitude par les épées des hommes vaillants, qui tous sont les plus terrifiants parmi les nations. Ils détruiront l'orgueil de l'Égypte et toute la multitude de son peuple sera détruite.
32:13	Je ferai périr tout son bétail près des grandes eaux, et aucun pied d'être humain ne les troublera plus, ni aucun pied d'animal ne les agitera plus.
32:14	Alors je rendrai profondes leurs eaux, et je ferai couler leurs fleuves comme de l'huile, – déclaration d'Adonaï YHWH.
32:15	Quand j'aurai réduit la terre d'Égypte en désolation, et que la terre sera dépouillée des choses dont elle était remplie, quand je frapperai tous ceux qui y habitent, ils sauront que je suis YHWH.
32:16	C'est un chant funèbre, chantez un chant funèbre ! Filles des nations, chantez un chant funèbre sur elle, sur l'Égypte et sur toute la multitude de son peuple ! Chantez un chant funèbre sur elle, – déclaration d'Adonaï YHWH.
32:17	Il arriva dans la douzième année, le quinzième jour du mois, que la parole de YHWH vint à moi en disant :
32:18	Fils d'humain, dresse une lamentation sur la multitude d'Égypte, et fais-la descendre, elle et les filles des nations magnifiques, aux parties inférieures de la terre, avec ceux qui descendent dans la fosse<!--Jé. 1:10, 18:7.-->.
32:19	Qui surpasses-tu en beauté ? Descends et couche-toi avec les incirconcis !
32:20	Ils tomberont au milieu de ceux qui seront blessés mortellement par l'épée. L'épée a déjà été donnée : Entraînez l'Égypte et toute sa multitude !
32:21	Les plus forts d'entre les puissants lui parleront du milieu du shéol, avec ceux qui lui donnaient du secours, et diront : Ils sont descendus, ils sont couchés, les incirconcis, blessés mortellement par l'épée.
32:22	Là est l'Assyrien, et toute son assemblée, ses sépulcres sont autour de lui. Eux tous, blessés mortellement, sont tombés par l'épée.
32:23	Ses sépulcres sont posés au fond de la fosse et son assemblée autour de sa sépulture. Eux tous, qui répandaient leur terreur sur la terre des vivants, sont tombés blessés mortellement par l'épée.
32:24	Là est Éylam et toute sa multitude autour de son sépulcre. Eux tous sont tombés blessés mortellement par l'épée, ils sont descendus incirconcis dans les parties inférieures de la Terre, eux qui répandaient la terreur sur la Terre des vivants. Ils ont porté leur ignominie avec ceux qui descendent dans la fosse.
32:25	On a placé sa couche au milieu des blessés mortellement avec toute sa multitude, au milieu de ses sépulcres. Eux tous incirconcis, blessés mortellement par l'épée, quoiqu'ils aient répandu la terreur sur la Terre des vivants, toutefois ils ont porté leur ignominie avec ceux qui descendent dans la fosse. Ils ont été placés au milieu des blessés mortellement.
32:26	Là se trouvent Méshek, Toubal et toute leur multitude, leurs sépulcres sont autour d'eux. Eux tous incirconcis, tués par l'épée, quoiqu'ils aient répandu leur terreur sur la Terre des vivants.
32:27	Ils ne se sont pas couchés avec les hommes vaillants, ceux qui sont tombés parmi les incirconcis. Ils sont descendus dans le shéol avec leurs armes de guerre, on a mis leurs épées sous leurs têtes, mais leurs iniquités resteront sur leurs os parce qu'ils étaient la terreur des hommes vaillants sur la Terre des vivants.
32:28	Toi aussi tu seras brisé au milieu des incirconcis, et tu seras couché avec ceux qui sont blessés mortellement par l'épée.
32:29	Là se trouvent Édom, ses rois et tous ses princes : malgré leur force, ils ont été placés avec ceux qui sont blessés mortellement par l'épée, ils sont couchés avec les incirconcis, avec ceux qui descendent dans la fosse.
32:30	Là se trouvent tous les princes du nord et tous les Sidoniens : ils sont descendus avec ceux qui sont blessés mortellement, malgré la terreur qu'inspirait leur force. Ils sont couchés incirconcis avec ceux qui sont blessés mortellement par l'épée, ils ont porté leur ignominie avec ceux qui sont descendus dans la fosse.
32:31	Pharaon les verra, et il se consolera au sujet de toute la multitude de son peuple. Pharaon, – déclaration d'Adonaï YHWH –, verra les blessés mortellement par l'épée et toute son armée.
32:32	Car je mettrai ma terreur sur la Terre des vivants, c'est pourquoi pharaon et toute la multitude de son peuple se coucheront au milieu des incirconcis avec ceux qui sont blessés mortellement par l'épée, – déclaration d'Adonaï YHWH.

## Chapitre 33

### Yehezkel établi comme sentinelle pour avertir le pécheur

33:1	La parole de YHWH vint à moi en disant :
33:2	Fils d'humain, parle aux fils de ton peuple et dis-leur : Quand je fais venir l'épée sur une terre, et que le peuple de cette terre prend de leurs frontières un homme et se le donne pour sentinelle,
33:3	et que voyant venir l'épée sur la terre, il sonne du shofar et avertit le peuple,
33:4	et si l’entendeur entend le son du shofar, mais ne se laisse pas avertir et que l'épée vienne l'enlever, son sang sera sur sa tête.
33:5	Il a entendu le son du shofar mais ne s'est pas laissé avertir. Son sang sera sur lui. Mais s'il se laisse avertir, il sauvera son âme.
33:6	Si la sentinelle voit venir l'épée et qu'elle ne sonne pas du shofar, si le peuple n'est pas averti et que l'épée vienne enlever une âme, celle-ci sera enlevée à cause de son iniquité, mais je redemanderai son sang de la main de la sentinelle.
33:7	Toi, fils d'humain, je t’ai donné pour sentinelle à la maison d’Israël. Tu écouteras la parole qui sort de ma bouche, et tu les avertiras de ma part.
33:8	Quand je dirai au méchant : Méchant, tu mourras ! tu mourras ! Si tu ne parles pas pour avertir le méchant au sujet de sa voie, ce méchant mourra à cause de son iniquité, mais je redemanderai son sang de ta main.
33:9	Mais si tu avertis le méchant pour le détourner de sa voie et qu'il ne se détourne pas de sa voie, il mourra à cause de son iniquité, mais toi, tu sauveras ton âme.
33:10	Toi, fils d'humain, dis à la maison d'Israël : Vous avez parlé ainsi, en disant : Puisque nos transgressions et nos péchés sont sur nous, et que nous périssons à cause d'eux, comment pourrions-nous vivre<!--Lé. 26:39.--> ?
33:11	Dis-leur: Moi, le Vivant – déclaration d'Adonaï YHWH – je ne prends pas plaisir<!--Ce verbe vient de l'hébreu « chaphets » qui signifie également « se complaire dans, désirer », etc.--> à la mort du méchant, mais que le méchant se détourne de sa voie et qu'il vive. Détournez-vous ! Détournez-vous de votre méchante voie ! Pourquoi mourriez-vous, maison d'Israël ?
33:12	Toi, fils d'humain, dis aux fils de ton peuple : La justice du juste ne le sauvera pas le jour où il commettra une transgression ; le méchant ne tombera pas par sa méchanceté le jour où il s'en détournera, et le juste ne pourra pas vivre par sa justice le jour où il péchera.
33:13	Quand je dis au juste qu'il vivra, qu'il vivra, et que lui, se confiant sur sa justice et commet l'injustice, on ne se souviendra plus de tous ses actes de justice, mais il mourra à cause de l'injustice qu'il a commise.
33:14	Et quand je dis au méchant : Tu mourras, tu mourras ! S'il se détourne de son péché et fait ce qui est juste et droit,
33:15	si le méchant rend le gage, s'il restitue les choses qu'il a prises par le vol, s'il marche dans les statuts de vies sans commettre d'injustice, il vivra, il vivra, il ne mourra pas.
33:16	On ne se souviendra plus des péchés qu'il a commis. S'il fait ce qui est juste et droit, il vivra, il vivra.
33:17	Or les fils de ton peuple ont dit : La voie d'Adonaï n'est pas bien réglée, mais c'est plutôt leur voie qui n'est pas bien réglée.
33:18	Si le juste se détourne de sa justice et commet l'injustice, il mourra à cause de cela.
33:19	Si le méchant se détourne de sa méchanceté et pratique ce qui est juste et droit, il vivra à cause de cela.
33:20	Vous avez dit : La voie d'Adonaï n'est pas bien réglée ! Je vous jugerai, maison d'Israël, chaque homme selon sa voie.

### Exécution du jugement de YHWH

33:21	Or il arriva dans la douzième année de notre captivité, au cinquième jour du dixième mois, qu'un homme qui s'était échappé de Yeroushalaim vint vers moi, en disant : La ville est prise !
33:22	La main de YHWH fut sur moi le soir, avant l'arrivée du fugitif, et YHWH ouvrit ma bouche lorsqu'il vint auprès de moi le matin. Ma bouche était ouverte et je n'étais plus muet.

### Ne pas se contenter d'écouter la parole d'Elohîm

33:23	La parole de YHWH vint à moi en disant :
33:24	Fils d'humain, ceux qui habitent dans ces lieux laissés en ruine, sur le sol d'Israël, parlent en disant : Abraham n’était qu’un seul et il a pris possession de la terre<!--Ge. 15:7.-->. Mais nous, nous sommes nombreux, et la terre nous a été donnée en possession.
33:25	C'est pourquoi tu leur diras : Ainsi parle Adonaï YHWH : Vous mangez la chair avec le sang, et vous levez vos yeux vers vos idoles, vous répandez le sang et vous hériteriez de la terre<!--Ge. 9:4 ; Lé. 3:17, 17:10.--> ?
33:26	Vous vous appuyez sur votre épée, vous commettez des abominations, chaque homme souille la femme de son compagnon, et vous hériteriez de la terre ?
33:27	Tu leur diras : Ainsi parle Adonaï YHWH : Moi, le Vivant ! Ceux qui sont dans ces lieux laissés en ruine tomberont par l'épée, et je livrerai aux bêtes celui qui est dans les champs, afin qu'elles le mangent. Et ceux qui sont dans les forteresses et dans les cavernes mourront par la peste.
33:28	Ainsi je ferai de la terre une horreur et une dévastation, l'orgueil de sa force sera aboli, et les montagnes d'Israël seront désolées, en sorte qu'il n'y passera plus personne.
33:29	Ils sauront que je suis YHWH, quand je ferai de leur terre une horreur et une dévastation, à cause de toutes leurs abominations qu'ils ont commises.
33:30	Quant à toi, fils d'humain, les fils de ton peuple parlent de toi près des murs et aux entrées des maisons. Ils se parlent l'un à l'autre, chaque homme à son frère, en disant : Venez maintenant, et écoutez la parole qui vient de YHWH !
33:31	Et ils viennent vers toi comme vient un peuple, et mon peuple s'assied devant toi. Ils écoutent tes paroles, mais ils ne les pratiquent pas, car leur bouche en fait une chanson d'amour sensuelle, et leur cœur va derrière leur gain injuste.
33:32	Voici, tu es pour eux un chant d'amour sensuel, une belle voix et jouant bien d'un instrument à cordes. Ils écoutent tes paroles, mais ils ne les pratiquent pas.
33:33	Mais quand ces choses arriveront, et voici, elles arrivent ! Ils sauront qu'il y avait un prophète au milieu d'eux.

## Chapitre 34

### Jugement d'Elohîm sur les faux bergers

34:1	La parole de YHWH vint à moi en disant :
34:2	Fils d'humain, prophétise contre les bergers d'Israël ! Prophétise, et dis à ces bergers : Ainsi parle Adonaï YHWH : Malheur aux bergers d'Israël, qui se paissent eux-mêmes ! N'est-ce pas le troupeau que les bergers doivent paître ?
34:3	Vous mangez la graisse, vous vous habillez de laine, vous tuez ce qui est gras, vous ne paissez pas le troupeau !
34:4	Vous n'avez pas fortifié celles qui étaient faibles, vous n'avez pas guéri celles qui étaient malades ni pansé celles qui étaient blessées, vous n'avez pas ramené celles qui étaient bannies ni cherché celles qui étaient détruites, mais vous les avez dominées avec violence et cruauté<!--Lu. 15:4-6 ; 1 Pi. 5:1-3.-->.
34:5	Elles se sont dispersées, parce qu'elles n'avaient pas de bergers, et elles sont devenues la nourriture de toutes les bêtes des champs, elles se sont dispersées.
34:6	Mes brebis sont errantes sur toutes les montagnes et sur toutes les collines élevées, mes brebis sont dispersées sur toute les faces de la Terre : nul ne s’en enquiert, nul ne cherche<!--Za. 13:7 ; Mt. 26:31 ; Mc. 14:27.-->.
34:7	C'est pourquoi bergers, écoutez la parole de YHWH :
34:8	Moi, le Vivant<!--Jn. 6:57 ; Ap. 1:18 ; 4:9 ; 5:14 ; 10:6 ; 15:7.-->, – déclaration d'Adonaï YHWH –, parce que mes brebis sont devenues le butin, parce que mes brebis, étant sans bergers, sont devenues la nourriture de toutes les bêtes des champs, et que mes bergers n’ont pas cherché mes brebis, parce les bergers se paissaient eux-mêmes et ne paissaient pas mes brebis,
34:9	c'est pourquoi, bergers, écoutez la parole de YHWH !
34:10	Ainsi parle Adonaï YHWH : Voici, j'en veux à ces bergers-là, et je redemanderai mes brebis de leur main, ils cesseront de paître les brebis, et les bergers ne se paîtront plus eux-mêmes. Mais je délivrerai mes brebis de leur bouche, et elles ne seront plus dévorées par eux.

### YHWH, le bon berger qui restaure son troupeau<!--Jn. 10:1-18.-->

34:11	Car ainsi parle Adonaï YHWH : Me voici, je redemanderai mes brebis, et je les chercherai.
34:12	Comme le berger prend soin de son troupeau quand il est au milieu de ses brebis dispersées, ainsi je chercherai mes brebis, et les délivrerai de tous les lieux où elles auront été dispersées au jour des nuages et de profonde obscurité.
34:13	Je les ferai sortir d'entre les peuples et les rassemblerai des territoires, les ramènerai sur leur sol, et les nourrirai sur les montagnes d'Israël, près des canaux et dans toutes les demeures de la terre.
34:14	Je les ferai paître dans de bons pâturages, et leur demeure sera sur les hautes montagnes d'Israël. Là elles coucheront dans une agréable demeure, et paîtront dans de gras pâturages, sur les montagnes d'Israël.
34:15	Moi-même je paîtrai mes brebis et les ferai reposer, – déclaration d'Adonaï YHWH<!--Ps. 23.-->.
34:16	Je chercherai celle qui est détruite, je ramènerai celle qui est bannie, je banderai celle qui est brisée, et je fortifierai celle qui est faible, mais je détruirai les grasses et les fortes. Je les paîtrai avec justice.
34:17	Quant à vous, mes brebis, ainsi parle Adonaï YHWH : Voici, je vais mettre à part<!--Mt. 25:31-46.--> les brebis, les béliers, et les boucs.
34:18	Est-ce trop peu pour vous de paître dans un bon pâturage, pour que vous fouliez de vos pieds le reste de votre pâturage ? De boire des eaux claires, pour que vous troubliez le reste avec vos pieds ?
34:19	Mes brebis se nourrissaient de ce que vous aviez foulé aux pieds, et elles buvaient ce que vos pieds avaient troublé.
34:20	C'est pourquoi Adonaï YHWH leur dit : Me voici, je mettrai moi-même à part la brebis grasse et la brebis maigre.
34:21	Parce que vous poussez du côté et de l'épaule, et que vous heurtez de vos cornes toutes celles qui sont languissantes, jusqu'à ce que vous les ayez chassées dehors,
34:22	je sauverai mes brebis afin qu'elles ne soient plus livrées au pillage. Voici, je jugerai entre brebis et brebis.
34:23	J'établirai sur elles un berger qui les paîtra, mon serviteur David. Il les paîtra, et lui-même sera leur berger.
34:24	Moi, YHWH, je deviendrai leur Elohîm, et mon serviteur David, prince au milieu d'elles. C'est moi, YHWH, qui ai parlé.
34:25	Je traiterai avec elles une alliance de paix et je détruirai de la terre les mauvaises bêtes. Les brebis habiteront dans le désert en sécurité et dormiront dans les forêts.
34:26	Je ferai d’elles et des alentours de ma colline une bénédiction. J'enverrai la pluie en son temps. Ce seront des pluies de bénédiction.
34:27	Les arbres des champs produiront leur fruit et la terre rapportera son revenu. Elles seront en sécurité sur leur sol et sauront que je suis YHWH, quand j'aurai rompu les bois de leur joug et que je les aurai délivrées de la main de ceux qui se les asservissent.
34:28	Elles ne seront plus un butin pour les nations, et les bêtes de la terre ne les dévoreront plus. Mais elles habiteront en sécurité et il n'y aura personne pour les effrayer.
34:29	J'établirai pour elles une plantation de renom, elles ne mourront plus de faim sur la Terre, et ne porteront plus l'opprobre des nations.
34:30	Elles sauront que moi, YHWH, leur Elohîm, je suis avec elles et qu'elles sont mon peuple, elles, la maison d'Israël, – déclaration d'Adonaï YHWH.
34:31	Vous mes brebis, les brebis de mon pâturage, vous êtes des humains et je suis votre Elohîm, – déclaration d'Adonaï YHWH.

## Chapitre 35

### Jugement sur Édom

35:1	La parole de YHWH vint à moi en disant :
35:2	Fils d'humain, mets tes faces contre la montagne de Séir, et prophétise contre elle<!--Am. 1:11.-->.
35:3	Dis-lui : Ainsi parle Adonaï YHWH : Voici, j'en veux à toi, montagne de Séir, et j'étendrai ma main contre toi et je ferai de toi une horreur et une dévastation.
35:4	Je ferai de tes villes un lieu laissé en ruines, et toi, tu deviendras une désolation. Tu sauras que je suis YHWH.
35:5	Parce que tu as eu une haine éternelle, et que tu as versé le sang des fils d'Israël par le pouvoir de l'épée, au temps de leur détresse, au temps où l'iniquité a pris fin<!--Ps. 137:7.-->.
35:6	C'est pourquoi, moi, le Vivant, – déclaration d'Adonaï YHWH –, je te mettrai à sang, et le sang te poursuivra. Parce que tu n'as pas haï le sang, le sang aussi te poursuivra.
35:7	Je ferai de la montagne de Séir une désolation désolée, et j’en retrancherai celui qui passe et celui qui revient.
35:8	Je remplirai ses montagnes de blessés mortellement. Ceux que l'épée blesse mortellement tomberont sur tes collines, dans tes vallées et dans tous tes ravins.
35:9	Je te réduirai en désolations éternelles, et tes villes ne seront plus habitées. Vous saurez que je suis YHWH.
35:10	Parce que tu as dit : Les deux nations, les deux terres seront à moi, et nous en prendrons possession, alors que YHWH lui-même est là.
35:11	C'est pourquoi, moi, le Vivant, – déclaration d'Adonaï YHWH –, j'agirai avec la colère et la jalousie que tu as montrées dans ta haine contre eux. Je me ferai connaître au milieu d'eux, quand je te jugerai.
35:12	Tu sauras que moi, YHWH, j'ai entendu toutes les paroles insultantes que tu as prononcées contre les montagnes d'Israël, en disant : Elles sont dévastées, elles nous sont données en guise de nourriture.
35:13	Vous vous êtes de votre bouche enorgueillis contre moi, vous avez multiplié vos paroles contre moi. Moi, j'ai entendu.
35:14	Ainsi parle Adonaï YHWH : Quand toute la Terre se réjouira, je ferai de toi une désolation.
35:15	Comme tu t'es réjouie sur l'héritage de la maison d'Israël et de sa désolation, j'en ferai de même envers toi : tu deviendras une  désolation, montagne de Séir ! Ainsi qu'Édom tout entier. Ils sauront que je suis YHWH<!--Ab. 1:11-16.-->.

## Chapitre 36

### YHWH rétablit Israël

36:1	Et toi, fils d'humain, prophétise sur les montagnes d'Israël, et dis : Montagnes d'Israël, écoutez la parole de YHWH !
36:2	Ainsi parle Adonaï YHWH : Parce que l'ennemi a dit contre vous : Ah ! Tous ces hauts lieux éternels sont devenus notre possession !
36:3	Prophétise, et dis : Ainsi parle Adonaï YHWH : Parce que et puisque autour de vous ils vous ont dévastées et aspirent à ce que vous deveniez la possession du reste des nations, et que vous êtes montés sur la lèvre, la langue et la diffamation des peuples,
36:4	c’est pourquoi, montagnes d'Israël, écoutez la parole d'Adonaï YHWH : Ainsi parle Adonaï YHWH, aux montagnes, aux collines, aux canaux, aux vallées, aux lieux laissés en ruine et dévastés, et aux villes abandonnées qui sont un butin et une moquerie pour le reste des nations qui sont autour.
36:5	C’est pourquoi ainsi parle Adonaï YHWH : Je parle dans le feu de ma jalousie contre le reste des nations, et contre tout Édom, eux qui se sont donné ma terre en possession, avec toute la joie de leur cœur et le mépris de leur âme, afin de piller ses faubourgs<!--Lé. 25:23 ; Es. 14:2 ; Jé. 2:7.-->.
36:6	C’est pourquoi prophétise sur le sol d'Israël, et dis aux montagnes et aux collines, aux courants d'eau et aux vallées : Ainsi parle Adonaï YHWH : Voici, je parle avec jalousie, et avec fureur, parce que vous portez l'ignominie des nations.
36:7	C’est pourquoi ainsi parle Adonaï YHWH : J'ai levé ma main, si les nations qui sont tout autour de vous ne portent leur ignominie.
36:8	Mais vous, montagnes d'Israël, vous pousserez vos branches, et vous porterez votre fruit pour mon peuple d'Israël, car ils sont prêts à venir.
36:9	Car me voici sur vous, je retournerai vers vous et vous serez cultivées et ensemencées.
36:10	Je mettrai sur vous des humains en grand nombre, la maison d'Israël tout entière. Les villes seront habitées et les lieux laissés en ruine seront rebâtis.
36:11	Je multiplierai sur vous les humains et les animaux : ils se multiplieront et porteront du fruit. Je vous ferai habiter comme auparavant, je vous ferai plus de bien qu’au commencement et vous saurez que je suis YHWH.
36:12	Je ferai marcher sur vous des humains, mon peuple d'Israël, et ils prendront possession de toi. Vous serez leur héritage et vous ne les consumerez plus.
36:13	Ainsi parle Adonaï YHWH : Parce qu'on dit de vous : Tu es une terre qui dévore les humains et tu as consumé tes habitants,
36:14	à cause de cela, tu ne dévoreras plus les humains et tu ne priveras plus ta nation de ses enfants, – déclaration d'Adonaï YHWH.
36:15	Je ne te ferai plus entendre l'ignominie des nations, tu ne porteras plus l'insulte des peuples. Et tu ne feras plus périr tes habitants, – déclaration d'Adonaï YHWH.
36:16	La parole de YHWH vint à moi en disant :
36:17	Fils d'humain, ceux de la maison d'Israël habitant sur leur sol l'ont souillé par leur voie et par leurs actions. Leur voie est devenue devant moi comme la souillure d'une femme pendant ses menstruations<!--Lé. 12:2, 15:19.-->.
36:18	J'ai répandu ma fureur sur eux à cause du sang qu'ils ont répandu sur la terre, et parce qu'ils l'ont souillée par leurs idoles.
36:19	Je les ai dispersés parmi les nations, et ils ont été disséminés sur diverses terres. Je les ai jugés selon leur voie et selon leurs actions.
36:20	Ils sont arrivés chez les nations où ils allaient, ils ont profané mon saint Nom en sorte qu'on disait d'eux : Ceux-ci sont le peuple de YHWH, c'est de sa terre qu'ils sont sortis<!--Ro. 2:24.-->.
36:21	Mais j'ai épargné mon saint Nom, que la maison d'Israël avait profané parmi les nations où elle est allée.
36:22	C'est pourquoi dis à la maison d'Israël : Ainsi parle Adonaï YHWH : Je ne le fais pas à cause de vous, maison d'Israël ! mais à cause de mon saint Nom, que vous avez profané parmi les nations où vous êtes allés<!--De. 7:7, 9:5 ; Es. 43:25 ; Ps. 25:11.-->.
36:23	Je sanctifierai mon grand Nom, qui a été profané parmi les nations, et que vous avez profané au milieu d'elles. Et les nations sauront que je suis YHWH, – déclaration d'Adonaï YHWH –, quand je serai sanctifié par vous, sous leurs yeux.
36:24	Je vous prendrai d'entre les nations, je vous rassemblerai de toutes les terres et je vous ramènerai sur votre sol.
36:25	Je vous aspergerai d'eau<!--Il est question ici de la nouvelle alliance (Jé. 31:31-34 ; Hé. 8:7-13).--> pure et vous serez purifiés. Je vous purifierai de toutes vos impuretés et de toutes vos idoles.

### Prophétie sur la naissance d'en haut

36:26	Je vous donnerai un cœur nouveau et je mettrai au-dedans de vous un esprit nouveau. J'ôterai de votre chair le cœur de pierre et je vous donnerai un cœur de chair<!--Jé. 32:39 ; Ez. 11:19 ; 2 Co. 3:3.-->.
36:27	Je mettrai mon Esprit au dedans de vous, je vous ferai marcher dans mes ordonnances, garder et pratiquer mes lois.
36:28	Vous habiterez la terre que j'ai donnée à vos pères, vous deviendrez mon peuple, et moi, je deviendrai votre Elohîm.
36:29	Je vous sauverai de toutes vos impuretés, j'appellerai le blé, je le multiplierai et je ne vous enverrai plus la famine.
36:30	Je multiplierai le fruit des arbres et le revenu des champs afin que vous ne portiez plus l'insulte de la famine parmi les nations.
36:31	Vous vous souviendrez de votre mauvaise voie et de vos actions qui n'étaient pas bonnes. Vous vous prendrez vous-mêmes en dégoût à cause de vos iniquités et de vos abominations.
36:32	Je ne le fais pas à cause de vous, – déclaration d'Adonaï YHWH –, sachez-le ! Soyez honteux et confus à cause de votre voie, maison d'Israël !
36:33	Ainsi parle Adonaï YHWH : Le jour où je vous purifierai de toutes vos iniquités, je ferai habiter les villes, et les lieux laissés en ruine seront rebâtis.
36:34	La terre dévastée sera cultivée, alors qu'elle était déserte aux yeux de tous les passants.
36:35	Et l'on dira : Cette terre qui était dévastée est devenue comme le jardin d'Éden, et ces villes qui étaient désertes, dévastées et détruites sont fortifiées et habitées<!--Es. 33:20 ; Jé. 22:8-9.-->.
36:36	Les nations qui resteront autour de vous sauront que moi, YHWH, j'ai rebâti les lieux détruits et planté ce qui était dévasté. Moi, YHWH, j'ai parlé, et je le ferai.
36:37	Ainsi parle Adonaï YHWH : Je me laisserai consulter par la maison d'Israël. Voici ce que je ferai pour eux : Je multiplierai les humains comme un troupeau de brebis.
36:38	Les villes qui sont désertes seront remplies de troupeaux d'êtres humains, pareils aux troupeaux consacrés, aux troupeaux qu'on amène à Yeroushalaim pendant ses fêtes solennelles. Et ils sauront que je suis YHWH.

## Chapitre 37

### Vision des ossements desséchés, image de la restauration d'Israël

37:1	La main de YHWH vint sur moi. YHWH me fit sortir par son Esprit. Il me déposa au milieu d'une vallée remplie d'ossements<!--Les ossements desséchés représentent les Israélites dispersés dans les nations.-->.
37:2	Il me fit passer près d'eux autour, autour, et voici, ils étaient extrêmement nombreux sur les faces de cette vallée, et voici, ils étaient extrêmement secs.
37:3	Il me dit : Fils d'humain, ces ossements vivront-ils ? Je dis : Adonaï YHWH, c'est toi qui le sais !
37:4	Il me dit : Prophétise sur ces ossements ! Dis-leur : Ossements desséchés, écoutez la parole de YHWH !
37:5	Ainsi parle Adonaï YHWH à ces ossements : Voici, je ferai entrer l'Esprit en vous et vous vivrez<!--Ps. 71:20 ; Ro. 8:11.-->.
37:6	Je mettrai des nerfs sur vous, je ferai monter de la chair sur vous, j'étendrai de la peau sur vous, et je mettrai l'Esprit en vous et vous vivrez. Et vous saurez que je suis YHWH.
37:7	Je prophétisai comme il m’avait été ordonné et, comme je prophétisais, il y eut une voix, et voici un tremblement, et les ossements se rapprochèrent, l’ossement vers son ossement.
37:8	Je regardai et voici, sur eux des nerfs, la chair monta et une peau s'étendit par-dessus, mais il n'y avait pas en eux d'esprit.
37:9	Il me dit : Prophétise à l'Esprit ! Prophétise, fils d'humain ! Dis à l'Esprit : Ainsi parle Adonaï YHWH : Esprit, viens des quatre vents, et souffle sur ces morts, et qu'ils vivent !
37:10	Je prophétisai comme il me l’avait ordonné, et l'Esprit entra en eux, ils reprirent vie et se tinrent debout sur leurs pieds. C'était une armée extrêmement, extrêmement grande.
37:11	Il me dit : Fils d'humain, ces ossements sont toute la maison d'Israël. Voici, ils disent : Nos ossements sont desséchés, notre espérance est détruite, nous sommes exterminés !
37:12	C'est pourquoi prophétise, et dis-leur : Ainsi parle Adonaï YHWH : Mon peuple, voici, je vais ouvrir vos sépulcres, je vous ferai monter hors de vos sépulcres, et je vous ferai venir sur le sol d'Israël<!--Les sépulcres représentent les nations dans lesquelles les Israélites se sont établis. Elohîm annonce le retour de son peuple sur la terre d'Israël (Es. 26:19 ; Os. 13:14).-->.
37:13	Et vous, mon peuple, vous saurez que je suis YHWH quand j'aurai ouvert vos sépulcres et que je vous aurai fait monter hors de vos sépulcres.
37:14	Je mettrai mon Esprit en vous, vous vivrez et je vous donnerai du repos sur votre sol. Et vous saurez que moi, YHWH, j'ai parlé et que je l'ai fait, – déclaration de YHWH.

### Prophétie sur l'unité d'Israël

37:15	La parole de YHWH vint à moi en disant :
37:16	Et toi, fils d’humain, prends pour toi un morceau de bois et écris dessus : Pour Yéhouda et pour les fils d'Israël qui lui sont associés ! Prends un bois et écris dessus : Pour Yossef, bois d'Éphraïm, et toute la maison d’Israël qui lui est associée !
37:17	Rapproche-les de toi l’un vers l’autre, en un seul bois, et qu’ils deviennent un dans ta main.
37:18	Quand les fils de ton peuple demanderont, en disant : Ne nous déclareras-tu pas ce que sont ceux-là pour toi ?
37:19	Dis-leur : Ainsi parle Adonaï YHWH : Voici je prendrai le bois de Yossef qui est dans la main d'Éphraïm et les tribus d'Israël qui lui sont associées. Je les joindrai au bois de Yéhouda, et j'en formerai un seul bois, ils ne seront qu'un seul bois dans ma main.
37:20	Ainsi les bois sur lesquels tu écriras seront dans ta main, sous leurs yeux.
37:21	Dis-leur : Ainsi parle Adonaï YHWH : Voici je prendrai les fils d'Israël du milieu des nations parmi lesquelles ils sont allés, je les rassemblerai de toutes parts, et je les ferai venir sur leur sol.
37:22	Je ferai d'eux une seule nation sur la terre, sur les montagnes d'Israël. Un seul roi sera leur roi à tous, ils ne seront plus deux nations, et ils ne seront plus divisés en deux royaumes<!--Es. 11:12-13 ; Os. 2:2 ; Jn. 10:16.-->.
37:23	Ils ne se souilleront plus par leurs idoles, ni par leurs infamies, ni par toutes leurs transgressions. Je les sauverai de tous les lieux où ils habitent, les lieux où ils ont péché et je les purifierai. Ils deviendront mon peuple, et moi, je deviendrai leur Elohîm<!--Es. 1:18 ; Jé. 24:7, 32:38, 33:8 ; Za. 8:8 ; 2 Co. 6:16.-->.
37:24	David mon serviteur sera roi sur eux ; un seul berger sera pour eux tous. Ils marcheront dans mes ordonnances, ils garderont mes statuts et les mettront en pratique.
37:25	Ils habiteront sur la terre que j'ai donnée à Yaacov, mon serviteur, sur laquelle vos pères ont habité. Ils y habiteront eux, et leurs fils, et les fils de leurs fils, pour toujours, et David mon serviteur sera leur prince pour toujours.
37:26	Je traiterai avec eux une alliance de paix, et il y aura une alliance éternelle avec eux. Je les établirai, je les multiplierai et je mettrai mon sanctuaire au milieu d'eux pour toujours.
37:27	Mon tabernacle sera au milieu d'eux. Je deviendrai leur Elohîm, et eux, ils deviendront mon peuple.
37:28	Les nations sauront que je suis YHWH, qui sanctifie Israël, quand mon sanctuaire sera au milieu d'eux pour toujours.

## Chapitre 38

### Jugement sur Gog

38:1	La parole de YHWH vint à moi en disant :
38:2	Fils d'humain, mets tes faces vers Gog en terre de Magog<!--Gog est un prince et Magog la terre. Ce chapitre doit être mis en parallèle avec Za. 12:1-4, 14:1-9 ; Mt. 24:14-30 ; Ap. 14:14-20, 20:8.-->, vers le prince de Rosh, de Méshek et de Toubal, et prophétise contre lui !
38:3	Tu diras : Ainsi parle Adonaï YHWH : Voici, j'en veux à toi, Gog, prince des chefs de Méshek et de Toubal !
38:4	Je te ferai retourner et je mettrai des boucles dans tes mâchoires. Je te ferai sortir avec toute ton armée, avec les chevaux et les cavaliers, tous parfaitement vêtus, assemblée nombreuse portant le grand et le petit bouclier, maniant tous l'épée.
38:5	La Perse, l'Éthiopie et Pouth sont avec eux, tous avec des boucliers et des casques.
38:6	Gomer et toutes ses armées, la maison de Togarmah à l'extrême nord, avec toutes ses armées, et beaucoup de peuples avec toi.
38:7	Prépare-toi, tiens-toi prêt, toi et toute l'assemblée que tu as réunie auprès de toi ! Sois leur chef !
38:8	Après beaucoup de jours tu seras visité. Dans les dernières années, tu marcheras contre la terre dont l’épée s’est détournée, rassemblée d'entre beaucoup de peuples sur les montagnes d'Israël longtemps restées en ruine. Ils sont sortis d'entre les peuples et ils habitent tous en sécurité.
38:9	Tu monteras, tu viendras comme une dévastation, tu seras comme une nuée pour couvrir la terre, toi, toutes tes armées et beaucoup de peuples avec toi<!--Da. 11:40.-->.
38:10	Ainsi parle Adonaï YHWH : Il arrivera en ces jours-là, que des pensées s'élèveront dans ton cœur et que tu inventeras un mauvais plan.
38:11	Tu diras : Je monterai contre la terre dont les villes sont sans murailles, je viendrai vers ceux qui sont tranquilles, qui habitent en sécurité, qui demeurent tous dans des villes sans murs, qui n'ont ni barres ni portes<!--Jé. 49:31.-->.
38:12	Pour prendre du butin, faire un pillage et dépouiller, pour remettre ta main sur les lieux laissés en ruine de nouveau habités et sur le peuple recueilli d’entre les nations, ayant des troupeaux et des richesses et qui habite le nombril<!--« Centre », « milieu », « nombril », « partie élevée ». Voir Jg. 9:37.--> de la Terre.
38:13	Séba et Dedan, les marchands de Tarsis et tous ses lionceaux, te diront : Viens-tu pour emporter un butin et pour le pillage ? Est-ce pour piller, pour le pillage que tu as réuni ton assemblée, afin d'emporter de l'argent et de l'or, afin de prendre des troupeaux et des richesses, afin de faire un grand pillage et de prendre un butin ?
38:14	Toi, fils d'humain, prophétise, et dis à Gog : Ainsi parle Adonaï YHWH : En ce jour-là, quand mon peuple d'Israël habitera en sécurité, ne le sauras-tu pas ?
38:15	Et tu viendras de ton lieu, de l'extrême nord, toi, et des peuples nombreux avec toi, tous montés sur des chevaux, une grande assemblée, une armée nombreuse.
38:16	Tu monteras contre mon peuple d'Israël, pareil à une nuée qui couvre la terre. Cela arrivera dans les derniers jours. Je te ferai venir contre ma terre afin que les nations me connaissent, quand je serai sanctifié par toi sous leurs yeux, Gog !
38:17	Ainsi parle Adonaï YHWH : N'est-ce pas de toi que j'ai parlé autrefois par la main de mes serviteurs, les prophètes d'Israël ? En ce temps là, pendant des années ils ont prophétisé que je te ferais venir contre eux.
38:18	Mais il arrivera en ce jour-là, au jour de la venue de Gog sur le sol d'Israël, – déclaration d'Adonaï YHWH –, que la colère me montera aux narines.
38:19	Je le déclare dans ma jalousie et dans le feu de ma fureur, en ce jour-là, il y aura un grand tremblement de terre sur le sol d'Israël.
38:20	Les poissons de la mer, les oiseaux des cieux, les bêtes des champs, tous les reptiles qui rampent sur le sol et tous les humains qui sont sur les faces du sol, trembleront devant moi. Les montagnes seront renversées, les rochers escarpés tomberont et tous les murs tomberont par terre.
38:21	J'appellerai contre lui l'épée sur toutes mes montagnes, – déclaration d'Adonaï YHWH. L'épée de l'homme sera contre son frère.
38:22	J'entrerai en jugement avec lui par la peste et par le sang, par une pluie violente et par des pierres de grêle. Je ferai pleuvoir le feu et le soufre sur lui, sur ses armées et sur les peuples nombreux qui seront avec lui<!--Ps. 11:6 ; Ap. 8:7, 11:19, 16:21.-->.
38:23	Je me glorifierai, je me sanctifierai, je serai connu aux yeux de beaucoup de nations. Et elles sauront que je suis YHWH.

## Chapitre 39

### Jugement sur Gog, suite

39:1	Toi, fils d'humain, prophétise contre Gog ! Tu diras : Ainsi parle Adonaï YHWH : Voici, j'en veux à toi, Gog, prince des chefs de Méshek et de Toubal !
39:2	Je te ferai retourner, je te conduirai, je te ferai monter de l'extrême nord et je te ferai venir sur les montagnes d'Israël.
39:3	Je frapperai ton arc dans ta main gauche, et je ferai tomber tes flèches de ta main droite.
39:4	Tu tomberas sur les montagnes d'Israël, toi, toutes tes armées et les peuples qui seront avec toi. Je te donnerai en guise de nourriture aux oiseaux de proie, aux oiseaux de toute sorte et aux bêtes des champs<!--V. 17-20 ; Ap. 19:17-20.-->.
39:5	Tu tomberas sur les faces des champs, parce que j'ai parlé, – déclaration d'Adonaï YHWH.
39:6	Je mettrai le feu dans Magog, et parmi ceux qui demeurent en sécurité dans les îles. Ils sauront que je suis YHWH.
39:7	Je ferai connaître mon saint Nom au milieu de mon peuple d'Israël, et je ne profanerai plus mon saint Nom. Les nations sauront que je suis YHWH, le Saint<!--Voir commentaire en Ac. 3:14.--> d'Israël.
39:8	Voici, cela arrive et sera fait, – déclaration d'Adonaï YHWH. C'est le jour dont j'ai parlé.
39:9	Les habitants des villes d'Israël sortiront, allumeront le feu, brûleront les armes, les petits et les grands boucliers, les arcs, les flèches, les bâtons qu'on lance de la main, et les javelots. Ils en feront du feu pendant sept ans.
39:10	On n'apportera pas du bois des champs et on n'en coupera pas dans les forêts, parce qu'ils feront du feu avec ces armes. Ils dépouilleront ceux qui les ont dépouillés, ils pilleront ceux qui les ont pillés, – déclaration d'Adonaï YHWH.
39:11	Il arrivera, en ce jour-là, que je donnerai là à Gog un lieu pour sépulcre en Israël, la vallée des passants, à l’orient de la mer, elle arrêtera les passants. On y enterrera Gog et toute la multitude de son peuple et on l'appellera la vallée d'Hamon-Gog<!--« La vallée d'Hamon-Gog » signifie « La vallée de la multitude de Gog ».-->.
39:12	La maison d'Israël les enterrera afin de purifier la terre pendant sept mois.
39:13	Tout le peuple de la terre les enterrera, et cela deviendra pour eux une renommée, le jour où je serai glorifié, – déclaration d'Adonaï YHWH.
39:14	Ils mettront à part des hommes qui passeront sans cesse sur la terre et qui, avec les passants, enterreront ceux qui seront restés sur les faces de la terre afin de la purifier. Ils se mettront à la recherche au bout des sept mois.
39:15	Les passants passeront sur la terre et, quand l'un d'eux verra un ossement humain, il bâtira auprès de lui un poteau indicateur jusqu'à ce que ceux qui enterrent l'aient enterré dans la vallée d'Hamon-Gog.
39:16	Il y aura aussi une ville nommée Hamona<!--« Hamona » signifie « multitude ».-->. Et ils purifieront la terre.
39:17	Toi, fils d'humain, ainsi parle Adonaï YHWH : Dis aux oiseaux de toutes espèces, et à toutes les bêtes des champs : Réunissez-vous, venez, rassemblez-vous de tous côtés vers mon sacrifice que je sacrifie pour vous, le grand sacrifice sur les montagnes d'Israël ! Vous mangerez de la chair et vous boirez du sang<!--Job 39:30 ; Mt. 24:28 ; Lu. 17:37 ; Ap. 19:17-20.-->.
39:18	Vous mangerez la chair des hommes puissants, et vous boirez le sang des princes de la Terre, le sang des moutons, des agneaux, des boucs et des veaux engraissés sur le Bashân<!--Es. 34:6 ; Jé. 46:10 ; So. 1:7 ; Job 39:30 ; Mt. 24:28.-->.
39:19	Vous mangerez de la graisse jusqu'à en être rassasiés, et vous boirez du sang jusqu'à en être ivres, de la graisse et du sang de mon sacrifice que j'aurai sacrifié pour vous.
39:20	Vous serez rassasiés à ma table de chevaux et de bêtes d'attelage, d'hommes vaillants et de tous hommes de guerre, – déclaration d'Adonaï YHWH.
39:21	Je mettrai ma gloire parmi les nations. Toutes les nations verront mon jugement que j'exercerai et comment je mettrai ma main sur elles.
39:22	La maison d'Israël saura dès ce jour-là, et dans la suite, que je suis YHWH, leur Elohîm.
39:23	Les nations sauront que la maison d'Israël a été emmenée en captivité à cause de son iniquité, parce qu'ils ont commis un délit contre moi. Je leur ai caché mes faces et je les ai livrés entre les mains de leurs ennemis afin de les faire tous tomber par l'épée<!--De. 31:17-18 ; Ps. 13:2.-->.
39:24	Je les ai traités selon leurs impuretés, selon leurs transgressions et je leur ai caché mes faces.

### Rétablissement et conversion d'Israël

39:25	C'est pourquoi ainsi parle Adonaï YHWH : Maintenant, je ramènerai les captifs de Yaacov, j'aurai pitié de toute la maison d'Israël et je serai jaloux pour mon saint Nom.
39:26	Ils porteront leur ignominie et tous les délits, les transgressions qu'ils avaient commises contre moi, quand ils habitaient en sécurité sur leur sol, sans personne pour les effrayer.
39:27	Quand je les ramènerai d'entre les peuples, quand je les rassemblerai des terres de leurs ennemis, je serai sanctifié par eux aux yeux de beaucoup de nations.
39:28	Ils sauront que je suis YHWH, leur Elohîm, quand, après les avoir exilés parmi les nations, je les aurai réunis sur leur sol, sans laisser aucun d'eux là-bas.
39:29	Je ne leur cacherai plus mes faces, car je répandrai mon Esprit sur la maison d'Israël, – déclaration d'Adonaï YHWH<!--Joë. 3:1 ; Za. 12:10 ; Ac. 2:17.-->.

## Chapitre 40

### Mesures du futur temple

40:1	Dans la vingt-cinquième année de notre captivité, au commencement de l'année, au dixième jour du mois, la quatorzième année après que la ville fut prise, en ce même jour, la main de YHWH vint sur moi, et il me fit venir là.
40:2	Dans les visions d'Elohîm il me fit venir en terre d'Israël et me déposa sur une montagne très élevée, sur laquelle il y avait comme une construction de ville, du côté du midi.
40:3	Il me fit venir là, et voici un homme dont l'aspect était comme l'aspect du cuivre. Il avait dans sa main un cordeau en lin et une canne à mesurer. Il se tenait debout à la porte.
40:4	Cet homme me parla ainsi : Fils d'humain, regarde de tes yeux, écoute de tes oreilles ! Et applique ton cœur à toutes les choses que je vais te faire voir ! Car tu as été amené ici afin que je te les fasse voir. Raconte à la maison d'Israël toutes les choses que tu vas voir !
40:5	Et voici, un mur à l’extérieur de la maison, autour, autour. Dans la main de l'homme il y avait une canne à mesurer de 6 coudées – d’une coudée et un palme. Il mesura une canne pour la largeur du bâtiment et une canne pour sa hauteur.
40:6	Il vint vers la porte dont les faces étaient vers le chemin d'orient et monta par ses degrés. Il mesura une canne de largeur pour l'un des seuils de la porte et une canne de largeur pour l'autre seuil.
40:7	La chambre : une canne de longueur, une canne de largeur. Entre les chambres : cinq coudées. Le seuil de la porte, près du vestibule de la porte, à l’intérieur : une canne.
40:8	Il mesura le vestibule de la porte, vers l’intérieur : une canne.
40:9	Il mesura le vestibule de la porte : 8 coudées, et ses piliers : 2 coudées. Le vestibule de la porte était vers l’intérieur.
40:10	Les chambres de la porte vers le chemin d'orient étaient trois de ce côté-ci et trois de ce côté-là. Toutes les trois avaient la même mesure, et les piliers de ce côté-ci et de ce côté-là avaient une même mesure.
40:11	Il mesura la largeur de l'entrée de la porte : 10 coudées. La longueur de la porte : 13 coudées.
40:12	Il y avait une frontière en face des chambres d'une coudée. Cette frontière était d'une coudée de part et d'autre – les chambres : 6 coudées de ce côté-ci et de 6 coudées de ce côté-là.
40:13	Il mesura la porte depuis le toit d'une chambre jusqu'au toit de l'autre. Largeur : 25 coudées. Une entrée était vis-à-vis d’une entrée.
40:14	Il fit les piliers : 60 coudées. Et vers le pilier du parvis, la porte autour, autour.
40:15	Des faces de la porte d'entrée aux faces du vestibule de la porte intérieure : 50 coudées.
40:16	Il y avait des fenêtres fermées aux chambres et à leurs piliers, à l'intérieur de la porte autour, autour. Il y avait aussi des fenêtres dans les vestibules autour, autour vers l'intérieur, des palmes étaient sculptées sur les piliers.
40:17	Il me fit entrer au parvis extérieur, et voici des chambres et un pavé fait dans le parvis autour, autour. Il y avait 30 chambres sur ce pavé.
40:18	Le pavé était au côté des portes et répondait à la longueur des portes. C'était le pavé inférieur.
40:19	Il mesura la largeur, face à la porte inférieure, face au parvis intérieure, de l’extérieur : 100 coudées, à l'est et au nord.
40:20	Il mesura la longueur et la largeur de la porte nord du parvis extérieur.
40:21	Ses chambres étaient 3 de ce côté-ci et 3 de ce côté-là. Ses piliers et ses vestibules avaient la même mesure que la première porte. Longueur : 50 coudées. Largeur : 25 coudées.
40:22	Ses fenêtres, son vestibule, et ses palmes avaient la même mesure que la porte en face du chemin d'orient. On y montait par sept degrés, devant lesquels était son vestibule.
40:23	Il y avait une porte au parvis intérieur, face à la porte nord, comme à celle de l'est. Il mesura de porte à porte : 100 coudées.
40:24	Il me fit aller vers le chemin du midi, et voici : il y avait une porte vers le chemin du midi. Il en mesura les piliers et les vestibules qui avaient la même mesure.
40:25	Ses fenêtres, ses chambres autour, autour, étaient semblables à ces fenêtres-là. Longueur : 50 coudées. Largeur : 25 coudées.
40:26	On y montait par sept degrés, devant lesquels était son vestibule. Il y avait des palmes, une de ce côté-ci et une de ce côté-là, sur ses piliers.
40:27	Il y avait une porte au parvis intérieur vers le chemin du midi. Il mesura de porte à porte, depuis le chemin du midi : 100 coudées.
40:28	Il me fit entrer au parvis intérieur par la porte du midi. Il mesura la porte midi selon ces mêmes mesures.
40:29	Ses chambres, ses piliers, ses vestibules, selon ces mêmes mesures, autour, autour : 50 coudées de longueur. Largeur : 25 coudées.
40:30	Il y avait des vestibules autour, autour. Longueur : 25 coudées. Largeur : 5 coudées.
40:31	Les vestibules de la porte étaient vers le parvis extérieur. Il y avait des palmes sur ses piliers et huit degrés pour y monter.
40:32	Il me fit entrer au parvis intérieur, par le chemin d'orient. Il mesura la porte, qui avait la même mesure.
40:33	Ses chambres, ses piliers, ses vestibules avaient ces mêmes mesures. Il y avait des fenêtres, des vestibules, autour, autour. Longueur : 50 coudées. Largeur : 25 coudées.
40:34	Ses vestibules étaient vers le parvis extérieur. Il y avait des palmes sur ses piliers, de ce côté-ci et de ce côté-là et huit degrés pour y monter.
40:35	Il me fit entrer par la porte nord, et il mesura, avec les mêmes mesures que celles-là.
40:36	Ses chambres, ses piliers et ses vestibules. Elle avait des fenêtres autour, autour. Longueur : 50 coudées. Largeur : 25 coudées.
40:37	Ses vestibules étaient vers le parvis extérieur. Il y avait des palmes sur ses piliers, de ce côté-ci et de ce côté-là et huit degrés pour y monter.
40:38	Il y avait une chambre dont l'entrée donnait sur les piliers des portes. C'est là qu'on lavait l'holocauste.
40:39	Il y avait aussi dans le vestibule de la porte deux tables de ce côté-ci et deux tables de ce côté-là, pour tuer sur elles l’holocauste, le sacrifice pour le péché et le sacrifice de culpabilité.
40:40	Au côté extérieur où l’on monte, à l’entrée de la porte nord, il y avait deux tables et, de l'autre côté vers le vestibule de la porte, deux tables.
40:41	Il y avait quatre tables ici et quatre tables là-bas, à côté de la porte : huit tables sur lesquelles on tuait. 
40:42	Les quatre tables pour l’holocauste étaient en pierres de taille. La longueur était d’une coudée et demie, la largeur d’une coudée et demie, la hauteur d’une coudée. On y déposait les instruments avec lesquels on tuait l’holocauste et le sacrifice.
40:43	Des chevilles en forme de crochet, d’un palme, étaient fixées dans la maison, autour, autour et, sur les tables, de la chair de l’offrande.

### Répartition des pièces du futur temple

40:44	À l'extérieur de la porte intérieure il y avait les chambres pour les chanteurs, dans le parvis intérieur : l'une était à côté du chemin de la porte nord, face au sud, l'autre était à côté du chemin de la porte est, face au nord.
40:45	Il me dit : Cette chambre faisant face au chemin du sud est pour les prêtres qui veillent à la garde de la maison.
40:46	Les chambres qui font face au nord sont pour les prêtres qui veillent à la garde de l'autel : ce sont les fils de Tsadok, qui, parmi les fils de Lévi, s'approchent de YHWH pour faire son service.
40:47	Il mesura le parvis. Longueur : 100 coudées. Largeur : 100 coudées – un carré. L'autel était en face de la maison.
40:48	Il me fit entrer dans le vestibule de la maison. Il mesura les poteaux du vestibule : 5 coudées de ce côté-ci et 5 coudées de ce côté-là. La largeur de la porte était de 3 coudées de ce côté-ci et de 3 coudées de ce côté-là.
40:49	Longueur du vestibule : 20 coudées. Largeur : 11 coudées. On y montait par des degrés. Il y avait des colonnes près des piliers, une ici et une là-bas.

## Chapitre 41

### Description du temple

41:1	Il me fit entrer dans le temple. Il mesura les piliers : 6 coudées de largeur d'un côté et de 6 coudées de largeur de l'autre côté, la largeur de la tente.
41:2	La largeur de la porte était de 10 coudées. Il y avait 5 coudées d'un côté de la porte et 5 coudées de l'autre. Il mesura la longueur du temple : 40 coudées. La largeur : 20 coudées
41:3	Il entra à l'intérieur. Il mesura les piliers de la porte : 2 coudées, et la porte : 6 coudées, et la largeur de la porte : 7 coudées. 
41:4	Il mesura sa longueur : 20 coudées, et la largeur : 20 coudées, en face du temple. Il me dit : C'est ici le Saint des saints.
41:5	Il mesura le mur de la maison : 6 coudées. La largeur des chambres : 4 coudées, autour, autour de la maison, autour.
41:6	Les chambres latérales, chambre sur chambre, étaient trois fois trente. Les chambres entraient dans le mur de la maison des chambres latérales autour, autour, afin d’être fixées, mais elles n’étaient pas fixées au mur de la maison.
41:7	Les chambres s'élargissaient d'étage en étage, car cette galerie de la maison s'élevait étage par étage autour, autour de la maison. Il y avait plus d'espace dans le haut de la maison, et l'on passait de l'étage inférieur à l'étage supérieur par celui du milieu.
41:8	Je considérai la hauteur de la maison autour, autour. Les fondations des chambres latérales : une canne pleine, 6 grandes coudées.
41:9	La largeur du mur extérieur des chambres latérales : 5 coudées. L'espace libre entre les chambres latérales de la maison.
41:10	Entre les chambres, largeur : 20 coudées, autour de la maison, autour, autour.
41:11	L'entrée des chambres latérales, vers l'espace libre : une entrée au nord et une autre entrée au sud. La largeur de l'espace libre : 5 coudées autour, autour.
41:12	Le bâtiment en face de la place vide, du côté du chemin de l'occident, avait une largeur de 70 coudées. Le mur du bâtiment : 5 coudées de largeur autour, autour. Sa longueur : 90 coudées.
41:13	Il mesura la maison. Longueur : 100 coudées. La place vide, le bâtiment et les murs, longueur : 100 coudées.
41:14	La largeur de la face de la maison et de la place vide, du côté de l'orient : 100 coudées.
41:15	Il mesura la longueur du bâtiment en face de la place vide, sur le derrière et ses galeries de côté et d'autre : 100 coudées. Le temple intérieur et les allées du parvis.
41:16	Les seuils, les fenêtres closes, les galeries du pourtour aux trois étages, en face des seuils, étaient recouverts de bois autour, autour. Depuis le sol jusqu'aux fenêtres, et les fenêtres étaient couvertes,
41:17	jusqu'au-dessus des entrées, et jusqu'à la maison au-dedans comme au-dehors, tout le mur du pourtour, à l'intérieur et à l'extérieur, tout était d'après la mesure,
41:18	et fait de chérubins et de palmes. Il y avait une palme entre deux chérubins, et chaque chérubin avait deux faces.
41:19	Des faces d’humain vers la palme d'un côté, et des faces de jeune lion vers la palme de l'autre côté. On avait fait ainsi sur toute la maison, autour, tout autour.
41:20	Depuis le sol jusqu'au-dessus des entrées il y avait des chérubins et des palmes et aussi sur le mur du temple.
41:21	Les poteaux du temple étaient carrés, et les faces du lieu saint avaient la même apparence.
41:22	L'autel était de bois, de la hauteur de 3 coudées, et de 2 coudées de longueur. Ses angles, ses pieds et ses côtés étaient de bois. Puis il me dit : C'est ici la table qui est face à YHWH.
41:23	Le temple et le lieu saint avaient deux portes.
41:24	Il y avait deux portes, deux battants, qui tous deux tournaient sur les portes, deux battants pour une porte et deux pour l'autre.
41:25	Il y avait aussi des chérubins et des palmes façonnés sur les portes du temple, comme sur les murs. Un entablement en bois était sur le front du vestibule en dehors.
41:26	Il y avait des fenêtres fermées, et des palmes ici et là-bas, ainsi qu'aux côtés du vestibule, aux chambres latérales de la maison et aux entablements.

## Chapitre 42

### Mesures supplémentaires du temple

42:1	Il me fit sortir vers le parvis extérieur, par le chemin qui est le chemin du nord. Il me fit entrer dans les chambres qui étaient vis-à-vis de la place vide et vis-à-vis du bâtiment, au nord.
42:2	Sur la face où se trouvait une entrée au nord, il y avait une longueur de 100 coudées, et la largeur était de 50 coudées.
42:3	En face des 20 du parvis intérieur et en face du pavé du parvis extérieur, les galeries étaient en face des galeries, au troisième étage.
42:4	Face aux chambres, il y avait une promenade large de 10 coudées vers l'intérieur et un chemin d'une coudée. Leurs ouvertures étaient au nord.
42:5	Les chambres supérieures étaient plus étroites que les inférieures et que celles du milieu du bâtiment, parce que les galeries en mangeaient une partie.
42:6	Car elles étaient à trois étages, et n'avaient pas de colonnes, comme les colonnes des parvis. C'est pourquoi, à partir du sol, les chambres du haut étaient plus étroites que celles du bas et du milieu.
42:7	Le mur extérieur parallèle aux chambres, du côté du parvis extérieur face aux chambres, avait 50 coudées de long.
42:8	Car la longueur des chambres du côté du parvis extérieur était de 50 coudées. Mais sur la face du temple, il y avait 100 coudées.
42:9	Au-dessous de ces chambres, l’entrée était à l'orient, quand on y venait du parvis extérieur.
42:10	Il y avait des chambres sur la largeur du mur du parvis, sur le chemin d'orient, en face de la place vide et en face du bâtiment.
42:11	Il y avait un chemin en face d'elles comme l’aspect des chambres qui étaient vers le chemin du nord. La longueur et la largeur étaient les mêmes. Leurs issues, leurs dispositions et leurs ouvertures étaient semblables.
42:12	L’ouverture en tête du chemin était semblable aux ouvertures des chambres qui étaient vers le chemin du midi. Le chemin en face du mur approprié était le chemin d'orient par où l'on y entrait.
42:13	Il me dit : Les chambres du nord et les chambres du sud, en face de la place vide, ce sont les chambres du lieu saint où les prêtres qui s'approchent de YHWH mangeront du saint des saints. Ils y déposeront les saints des saints, les offrandes de grain, les sacrifices pour le péché et les sacrifices de culpabilité, car ce lieu est saint.
42:14	Quand les prêtres seront entrés, ils ne sortiront pas du lieu saint vers le parvis extérieur, mais ils déposeront là leurs vêtements avec lesquels ils font le service, car ces vêtements sont sacrés. Ils en mettront d'autres pour s'approcher du peuple.
42:15	Lorsqu'il eut achevé de mesurer la maison intérieure, il me fit sortir sur le chemin de la porte, face au chemin d'orient. Il la mesura autour, autour.
42:16	Il mesura le côté du vent oriental avec la canne à mesurer : 500 cannes, avec la canne à mesurer autour, autour.
42:17	Il mesura le côté du vent du nord : 500 cannes, avec la canne à mesurer autour, autour.
42:18	Il mesura le côté du vent du sud : 500 cannes, avec la canne à mesurer.
42:19	Il se tourna du côté du vent occidental pour le mesurer : 500 cannes de la canne à mesurer.
42:20	Il mesura aux quatre vents, son mur autour, autour : la longueur était de 500 et la largeur de 500, pour séparer ce qui était saint et ce qui était profane.

## Chapitre 43

### La gloire de YHWH remplit la maison<!--Cp. Ez. 11:22-24.-->

43:1	Il me fit aller vers la porte, la porte qui fait face au chemin d'orient.
43:2	Et voici, la gloire de l'Elohîm d'Israël venait par le chemin d'orient. Sa voix était pareille au bruit des grandes eaux<!--Ez. 1:24 ;Ap. 1:15 et 14:2.-->, et la terre brillait de sa gloire.
43:3	Ce que je voyais était, aspect pour aspect, comme l’aspect que j’avais vu quand j'étais venu pour détruire la ville. C’étaient des aspects comme l’aspect que j’avais vu près du fleuve Kebar. Et je tombai sur mes faces.
43:4	La gloire de YHWH entra dans la maison par le chemin de la porte, en face du chemin d'orient.
43:5	L'Esprit m'enleva et me fit entrer dans le parvis intérieur, et voici la gloire de YHWH remplissait la maison.

### Le trône de YHWH

43:6	J'entendis qu'on me parlait depuis la maison, un homme était debout à côté de moi.
43:7	Il me dit : Fils d'humain, c'est ici le lieu de mon trône et le lieu des plantes de mes pieds. C'est là que je ferai ma demeure éternellement parmi les fils d'Israël. La maison d'Israël et ses rois ne souilleront plus mon saint Nom par leurs prostitutions et par les cadavres de leurs rois sur leurs hauts lieux.
43:8	Ils ont mis leur seuil près de mon seuil, et leur poteau près de mon poteau, il y avait un mur entre eux et moi. Ils ont souillé mon saint Nom par leurs abominations qu'ils ont faites. C'est pourquoi je les ai exterminés dans ma colère.
43:9	Maintenant, ils éloigneront de moi leurs adultères et les cadavres de leurs rois, et je ferai ma demeure éternellement parmi eux.
43:10	Toi, fils d'humain, montre cette maison à la maison d’Israël ! Qu'ils aient honte de leurs iniquités et mesurent le modèle !
43:11	S'ils ont honte de tout ce qu'ils ont fait, fais-leur connaître le dessin de cette maison, sa disposition, avec ses sorties et ses entrées, tous ses dessins, tous ses statuts, tous ses dessins et toute sa torah. Mets-les par écrit sous leurs yeux afin qu'ils gardent tous ses dessins, tous ses statuts et qu'ils les pratiquent.
43:12	Telle est la torah de la maison. Sur le sommet de la montagne, tout le territoire est le saint des saints autour, autour. Voilà la torah de la maison.

### L'autel pour les holocaustes et les sacrifices

43:13	Voici les mesures de l’autel en coudées, la coudée étant d’une coudée et d’un palme. Le fond : une coudée et une coudée de largeur. Sa frontière sur le bord autour : un empan. Voilà le dos de l’autel.
43:14	Depuis le fond sur le sol jusqu'à l'encadrement inférieur : 2 coudées et une coudée de largeur. Depuis le petit jusqu'au grand encadrement : 4 coudées et une coudée de largeur.
43:15	L'autel : 4 coudées et, sur l'autel en haut, quatre cornes.
43:16	L'autel a 12 coudées de longueur, 12 coudées de largeur, et forme un carré par ses quatre côtés.
43:17	L'encadrement a 14 coudées de longueur sur 14 coudées de largeur à ses quatre côtés. La frontière autour de lui a une demi-coudée, le fond a une coudée autour, autour, et les degrés font face à l'orient.
43:18	Il me dit : Fils d'humain, ainsi parle Adonaï YHWH : Voici les statuts de l'autel pour le jour où il sera fait, afin d'y faire monter les holocaustes et d'y faire l'aspersion du sang.
43:19	Tu donneras aux prêtres, aux Lévites, qui sont de la postérité de Tsadok, et qui s'approchent de moi, – déclaration d'Adonaï YHWH –, afin qu'ils y fassent mon service, un jeune taureau en sacrifice pour le péché.
43:20	Tu prendras de son sang et en mettras sur les quatre cornes de l'autel, sur les quatre angles de l'encadrement et sur le rebord qui l'entoure, tu purifieras l'autel et tu feras la propitiation pour lui<!--Ex. 29:36-39.-->.
43:21	Tu prendras le jeune taureau expiatoire, et on le brûlera dans un lieu désigné de la maison, en dehors du sanctuaire.
43:22	Le second jour, tu présenteras un bouc sans défaut en sacrifice pour le péché, et on purifiera l'autel comme on l'aura purifié avec le jeune taureau.
43:23	Quand tu auras achevé de purifier l'autel, tu présenteras un jeune taureau sans défaut, et un bélier du troupeau sans défaut.
43:24	Tu les présenteras devant YHWH : les prêtres jetteront du sel par-dessus et les feront monter en holocauste à YHWH<!--Lé. 2:13.-->.
43:25	Pendant sept jours, tu présenteras chaque jour un bouc en sacrifice pour le péché, ils sacrifieront un jeune taureau et un bélier du troupeau sans défaut.
43:26	Pendant sept jours on fera la propitiation pour l'autel, on le purifiera et on remplira ses mains<!--Voir Jg. 17:5,12.-->.
43:27	Lorsque ces jours seront accomplis, il arrivera que, dès le huitième jour et plus tard, les prêtres présenteront sur cet autel vos holocaustes et vos sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->, et je serai apaisé envers vous, – déclaration d'Adonaï YHWH.

## Chapitre 44

### La porte fermée du sanctuaire

44:1	Il me ramena par le chemin de la porte du sanctuaire, qui fait face à l'orient. Elle était fermée.
44:2	YHWH me dit : Cette porte-ci sera fermée. Elle ne s'ouvrira plus et plus personne n'y passera, parce que YHWH, l'Elohîm d'Israël, est entré par elle. Elle sera fermée<!--Ap. 3:8.-->.
44:3	Mais le prince, puisqu'il est prince, s'y assiéra pour manger le pain devant YHWH. Il entrera par le chemin du vestibule de la porte et il sortira par le même chemin.

### La gloire dans la maison de YHWH

44:4	Il me fit venir par le chemin de la porte nord, face à la maison. Je regardai, et voici, la gloire de YHWH avait rempli la maison de YHWH, et je tombai sur mes faces.
44:5	YHWH me dit : Fils d'humain, applique ton cœur, et regarde de tes yeux ! Écoute de tes oreilles tout ce que je te dirai au sujet de tous les statuts de la maison de YHWH et de toute sa torah. Applique ton cœur en ce qui concerne l'entrée de la maison et toutes les sorties du sanctuaire.
44:6	Tu diras à la rébellion, à la maison d'Israël : Ainsi parle Adonaï YHWH : Maison d'Israël ! Assez de toutes vos abominations !
44:7	Vous avez fait entrer les fils de l'étranger, incirconcis de cœur et incirconcis de chair pour être dans mon sanctuaire, pour profaner ma maison. Vous avez présenté mon pain, la graisse et le sang à toutes vos abominations, vous avez rompu mon alliance<!--Lé. 3:11-16, 22:25 ; No. 28:2.-->.
44:8	Et vous n'avez pas ordonné que mes choses saintes soient observées, mais vous avez établi comme il vous a plu dans mon sanctuaire, des gens pour y être les gardes des choses que j'avais commandé de garder.

### Recommandations aux prêtres du futur temple

44:9	Ainsi parle Adonaï YHWH : Aucun fils d'étranger, incirconcis de cœur et incirconcis de chair, n'entrera dans mon sanctuaire, aucun de tous les fils d'étrangers qui se trouvent au milieu des fils d'Israël.
44:10	Mais les Lévites qui se sont éloignés de moi, lorsque Israël s'est égaré, et qui se sont égarés de moi pour suivre leurs idoles, porteront leur iniquité.
44:11	Ils seront dans mon sanctuaire faisant le service de garde des portes de la maison, et faisant le service de la maison. Ils tueront pour le peuple l'holocauste et les autres sacrifices, et ils se tiendront debout<!--Hé. 10:11.--> devant lui pour être à son service.
44:12	Parce qu’ils ont fait leur service devant leurs idoles et qu’ils sont devenus pour la maison d’Israël une occasion de trébucher dans l'iniquité, à cause de cela j'ai levé ma main contre eux, – déclaration d'Adonaï YHWH –, qu'ils porteront la peine de leur iniquité.
44:13	Ils n'approcheront plus de moi pour exercer la prêtrise et ils ne s'approcheront pas de mon Lieu saint, près du saint des saints, mais ils porteront leur confusion et leurs abominations qu'ils ont commises.
44:14	Je les donnerai pour gardiens à la garde de la maison, pour tout son service, et pour tout ce qui s'y fait.
44:15	Mais quant aux prêtres et aux Lévites, fils de Tsadok, qui ont gardé les obligations de mon sanctuaire lorsque les fils d'Israël se sont éloignés de moi, ceux-là se présenteront devant moi pour faire mon service. Ils se tiendront debout devant moi pour me présenter la graisse et le sang, – déclaration d'Adonaï YHWH.
44:16	Ceux-là entreront dans mon sanctuaire et s'approcheront de ma table pour faire mon service, ils garderont mes obligations.
44:17	Et il arrivera que lorsqu'ils franchiront les portes des parvis intérieurs, ils se vêtiront de robes de lin. Il n'y aura pas de laine sur eux pendant qu'ils feront le service aux portes des parvis intérieurs et dans la maison.
44:18	Ils auront des turbans en lin sur la tête et des caleçons de lin sur les reins. Ils ne se ceindront pas de manière à provoquer la sueur<!--Voir Ge. 3:17.-->.
44:19	Quand ils sortiront pour aller dans le parvis extérieur, dans le parvis extérieur, vers le peuple, ils se dépouilleront de leurs vêtements avec lesquels ils font le service et les poseront dans les chambres saintes. Ils se revêtiront d'autres vêtements afin qu'ils ne sanctifient pas le peuple avec leurs habits.
44:20	Ils ne se raseront pas la tête et ne laisseront pas leur chevelure libre. Ils se tondront, ils se tondront la tête<!--Lé. 19:27.-->.
44:21	Aucun prêtre ne boira du vin lorsqu'il entrera dans le parvis intérieur.
44:22	Ils ne prendront pas pour femme une veuve, ni une répudiée, mais ils prendront des vierges de la postérité de la maison d'Israël, ou une veuve qui soit veuve d'un prêtre<!--Lé. 21:13-14.-->.
44:23	Ils enseigneront à mon peuple la différence qu'il y a entre le saint et le profane. Ils leur feront entendre la différence qu'il y a entre ce qui est impur et ce qui est pur.
44:24	En cas de procès, ce sont eux qui se tiendront debout pour juger, et ils jugeront suivant les ordonnances que j'ai données. Ils garderont ma torah et mes statuts dans toutes mes fêtes, et ils sanctifieront mes shabbats.
44:25	Ils n'approcheront pas d'un être humain mort, de peur de se rendre impurs. Ils pourront se rendre impurs que pour un père, pour une mère, pour un fils, pour une fille, pour un frère et pour une sœur qui n’a pas été à un homme<!--Lé. 21:1-3.-->.
44:26	Après qu'il se sera purifié, on lui comptera sept jours.
44:27	Le jour où il entrera dans le lieu saint, dans le parvis intérieur pour faire le service dans le lieu saint, il présentera son sacrifice pour son péché, – déclaration d'Adonaï YHWH.
44:28	Ce sera pour eux un héritage : c'est moi qui suis leur héritage. Vous ne leur donnerez aucune propriété en Israël : c'est moi qui suis leur propriété<!--No. 18:20 ; De. 18:1-2.-->.
44:29	Ils se nourriront de l'offrande de grain, des sacrifices pour le péché et des sacrifices de culpabilité. Tout ce qui sera voué à une entière destruction en Israël leur appartiendra.
44:30	Les prémices de tous les premiers fruits de tout, et toute contribution de tout, parmi toutes vos contributions appartiendront aux prêtres. Vous donnerez aussi les prémices de votre pâte aux prêtres, afin que la bénédiction repose sur votre maison.
44:31	Les prêtres ne mangeront aucune créature volante, aucun animal mort ou déchiré<!--Ex. 22:30 ; Lé. 22:8.-->.

## Chapitre 45

### Zone réservée à YHWH et aux prêtres

45:1	Quand vous partagerez par le sort la terre en héritage, vous prélèverez une contribution pour YHWH une part de la terre qui sera sacrée. La longueur : 25 000 de longueur, et la largeur : 10 000. Elle sera sacrée sur tout son territoire, partout.
45:2	De celle-ci il y aura, pour le lieu saint, 500 sur 500 en carré, et 50 coudées tout autour pour ses faubourgs.
45:3	Avec cette mesure, tu mesureras une longueur de 25 000, une largeur de 10 000. Là sera le sanctuaire et le Saint des saints.
45:4	Cette terre ainsi sanctifiée sera pour les prêtres qui font le service du sanctuaire, qui s'approchent de YHWH pour le servir. C'est là que seront leur maison, et ce sera un sanctuaire pour le sanctuaire.
45:5	25 000 en longueur et 10 000 en largeur formeront la propriété des Lévites, serviteurs de la maison, avec 20 chambres.
45:6	Comme propriété de la ville vous mettrez 5 000 coudées de largeur et une longueur de 25 000, parallèles à la contribution sainte prélevée. Elle sera pour toute la maison d'Israël.

### Zone réservée au prince

45:7	Et pour le prince il y aura, de ce côté-ci et de ce côté-là de la contribution sainte et de la propriété de la ville, le long de la contribution sainte et le long de la propriété de la ville, quelque chose du côté de l'occident vers l'occident, et quelque chose du côté de l'orient vers l'orient, sur une longueur parallèle à l'une des parts, depuis la limite de l'occident jusqu'à la limite de l'orient.
45:8	Ce sera sa terre, sa propriété en Israël. Mes princes que j'établirai ne fouleront plus mon peuple, mais ils distribueront la terre à la maison d'Israël, selon leurs tribus.

### Le prince, exemple au milieu du peuple ; prescriptions sur les offrandes

45:9	Ainsi parle Adonaï YHWH : Assez, princes d'Israël ! Rejetez la violence et le ravage, jugez avec justice, levez vos exactions de dessus mon peuple ! – déclaration d'Adonaï YHWH.
45:10	Ayez la balance juste, l'épha juste, et le bath juste<!--Lé. 19:35-36.-->.
45:11	L'épha et le bath seront de même mesure. On prendra un bath pour la dixième partie d'un omer, et l'épha sera la dixième partie d'un omer, la mesure de l'un et de l'autre se rapportera à l'omer.
45:12	Le sicle sera de 20 guéras ; 20 sicles, 25 sicles et 15 sicles feront la mine<!--Ex. 30:13 ; Lé. 27:25.-->.
45:13	Voici la contribution élevée que vous offrirez : La sixième partie d'un épha d'un omer de blé, et vous donnerez la sixième partie d'un épha d'un omer d'orge.
45:14	Le bath est la mesure pour l'huile, l'offrande ordonnée pour l'huile sera la dixième partie d'un bath sur un cor, qui est égal à un omer de 10 baths, car 10 baths feront un omer.
45:15	Une brebis sur un troupeau de 200 dans les pâturages arrosés d'Israël sera donnée pour l'offrande de grain, l'holocauste et l'offrande de paix pour faire la propitiation pour vous, – déclaration d'Adonaï YHWH.
45:16	Tout le peuple de la terre sera tenu à cette contribution élevée pour celui qui sera prince en Israël.
45:17	Le prince aura la charge des holocaustes, des offrandes de grain et des libations, lors des fêtes, des nouvelles lunes, des shabbats, lors de toutes les solennités de la maison d’Israël. C'est lui qui présentera le sacrifice d'expiation, l'offrande de grain, l'holocauste et l'offrande de paix pour faire la propitiation pour la maison d'Israël.
45:18	Ainsi parle Adonaï YHWH : Le premier du premier mois, tu prendras un jeune taureau, un fils de bétail, sans défaut, et tu feras l'expiation du sanctuaire.
45:19	Le prêtre prendra du sang de ce sacrifice d'expiation et en mettra sur les poteaux de la maison, sur les quatre angles de l'encadrement de l'autel et sur les poteaux de la porte du parvis intérieur.
45:20	Tu en feras ainsi au septième jour du même mois, pour l'homme qui a péché involontairement ou par naïveté. Vous ferez ainsi la propitiation pour la maison.
45:21	Au premier mois, au quatorzième jour du mois, ce sera pour vous la Pâque. C'est une fête de sept jours : on mangera des pains sans levain<!--Lé. 23:5 ; No. 9:3 ; Ex. 12.-->.
45:22	En ce jour-là, le prince présentera un taureau pour le sacrifice d'expiation, tant pour lui que pour tout le peuple de la terre.
45:23	Pendant les sept jours de la fête il présentera, comme holocauste pour YHWH, sept taureaux et sept béliers sans défaut, chaque jour, pendant les sept jours et, comme sacrifice pour le péché, un bouc chaque jour.
45:24	Il présentera comme offrande de grain un épha par taureau, un épha par bélier et un hin d'huile par épha.
45:25	Au septième mois, le quinzième jour du mois, à la fête, il présentera durant sept jours les mêmes choses, le même sacrifice d'expiation, le même holocauste et la même offrande de grain avec l'huile.

## Chapitre 46

### Le service le jour du shabbat et les jours de fêtes

46:1	Ainsi parle Adonaï YHWH : La porte du parvis intérieur, face à l'orient, sera fermée les six jours de travail. Elle sera ouverte le jour du shabbat, elle sera aussi ouverte le jour de la nouvelle lune.
46:2	Le prince y entrera par le chemin du vestibule de la porte du parvis extérieur et se tiendra près de l'un des poteaux de l'autre porte. Les prêtres présenteront son holocauste et ses sacrifices d'offrande de paix. Il se prosternera sur le seuil de cette porte, ensuite il sortira, et la porte ne sera pas fermée jusqu'au soir.
46:3	Le peuple de la terre se prosternera devant YHWH à l'entrée de cette porte, les shabbats et les nouvelles lunes.
46:4	L'holocauste que le prince présentera à YHWH le jour du shabbat sera de 6 agneaux sans défaut et d'un bélier sans défaut.
46:5	Comme offrande de grain, un épha pour le bélier, et pour les agneaux, une offrande de grain, selon le don de sa main, avec un hin d'huile par épha.
46:6	Le jour de la nouvelle lune, ce sera un jeune taureau sans défaut, 6 agneaux et un bélier sans défaut.
46:7	D’un épha pour le jeune taureau et d’un épha pour le bélier il présentera une offrande de grain, et pour les agneaux, selon ce que sa main atteindra, avec un hin d'huile par épha.
46:8	Lorsque le prince entrera, il entrera par le chemin du vestibule de la porte et il sortira par ce chemin.
46:9	Quand le peuple de la terre viendra en face de YHWH, aux fêtes solennelles, celui qui y entrera par le chemin de la porte nord pour y adorer YHWH, sortira par le chemin de la porte sud, et celui qui y entrera par le chemin de la porte sud, sortira par le chemin de la porte nord. Personne ne retournera par le chemin de la porte par laquelle il sera entré, mais il sortira par celle qui lui est opposée.
46:10	Le prince, au milieu d’eux, entrera quand ils entreront, et sortira quand ils sortiront.
46:11	Lors des fêtes et des solennités, l'offrande de grain sera d'un épha pour le jeune taureau, d'un épha pour le bélier, et pour les agneaux, selon le don de sa main, avec un hin d'huile par épha.
46:12	Si le prince présente un sacrifice volontaire, quelque holocauste, soit quelques sacrifices d'offrande de paix en offrande volontaire à YHWH, on lui ouvrira la porte qui fait face à l'orient et il présentera son holocauste et ses sacrifices d'offrande de paix comme il les présente le jour du shabbat. Puis il sortira, et après qu'il sera sorti, on fermera cette porte.
46:13	Tu présenteras chaque jour en holocauste pour YHWH un agneau d'un an sans défaut. Tu le présenteras matin après matin.
46:14	Tu lui présenteras, matin après matin, l'offrande de grain, faite de la sixième partie d'un épha, et de la troisième d'un hin d'huile pour pétrir la farine. C'est là l'offrande de YHWH. Ce sont des statuts perpétuels, pour toujours.
46:15	Ainsi on présentera, matin après matin, en holocauste perpétuel cet agneau et l'offrande de grain avec cette huile.
46:16	Ainsi parle Adonaï YHWH : Si le prince donne un don à un homme de ses fils, ce sera l’héritage de celui-ci pour ses fils, leur propriété comme héritage.
46:17	Quand il donnera un don de sa possession à l’un de ses serviteurs, il sera à lui jusqu’à l’année de la liberté, puis il retournera au prince. Mais son héritage sera à ses fils<!--Lé. 25:10.-->.
46:18	Le prince ne prendra rien sur l'héritage du peuple en les opprimant dans leur propriété. C'est de sa propre propriété qu'il fera hériter ses fils, afin qu'aucun homme de mon peuple ne soit dispersé loin de sa propriété.
46:19	Il me conduisit par l'entrée qui était vers le côté de la porte, aux chambres saintes qui appartenaient aux prêtres, vers le nord. Et voici, il y avait un certain lieu dans le fond du côté occidental.
46:20	Il me dit : Voici le lieu où les prêtres feront bouillir la chair des sacrifices de culpabilité et pour le péché, et où ils feront cuire les offrandes de grain afin de ne pas les transporter dans le parvis extérieur pour sanctifier le peuple<!--No. 18:9.-->.
46:21	Il me fit sortir vers le parvis extérieur et me fit traverser vers les quatre angles du parvis, et voici, il y avait une cour à chacun des angles du parvis.
46:22	Aux quatre angles de ce parvis, il y avait des cours fermées. Longueur : 40 ; largeur : 30. Toutes les quatre avaient la même mesure dans les angles.
46:23	Un mur les entourait toutes les quatre, et des endroits pour cuire étaient faits au bas du campement tout autour.
46:24	Il me dit : C'est ici la maison des cuisines. C'est là que ceux qui font le service de la maison cuiront les sacrifices du peuple.

## Chapitre 47

### Les eaux pures du sanctuaire<!--Cp. Za. 14:8-9 ; Ap. 22:1-2.-->

47:1	Il me ramena vers l'entrée de la maison, et voici que de l'eau sortait sous le seuil de la maison, vers l'orient, car les faces de la maison étaient vers l'orient. L'eau descendait du côté droit de la maison, du côté sud de l'autel<!--Joë. 4:18 ; Za. 13:1, 14:8 ; Ps. 46:5 ; Ap. 22:1.-->.
47:2	Il me fit sortir par le chemin de la porte nord. Il me fit tourner par le chemin extérieur, jusqu'à la porte extérieure, sur le chemin qui fait face à l'orient. Et voici que de l'eau coulait du côté droit.
47:3	Quand l’homme sortit vers l’orient, il avait dans sa main un cordeau. Il mesura 1 000 coudées, puis il me fit traverser l'eau : j'avais de l'eau jusqu'aux chevilles.
47:4	Il mesura 1 000 autres coudées, et me fit traverser l'eau : j'avais de l'eau jusqu'aux genoux. Il mesura 1 000 autres coudées et me fit traverser : j'avais de l'eau jusqu'aux reins.
47:5	Il mesura 1 000 autres coudées : c'était un torrent que je ne pouvais traverser, car l'eau avait monté, de l'eau pour la baignade, un torrent qui ne se traverse pas.
47:6	Il me dit : As-tu vu, fils d'humain ? Il me fit aller et revenir vers le bord du torrent.
47:7	Quand il m'eut ramené, voici que, sur le bord du torrent, il y avait des arbres très nombreux, de ce côté-ci et de ce côté-là.
47:8	Il me dit : Cette eau sortira vers le district oriental, descendra dans la région aride et entrera dans la mer. Elle en sortira et l'eau de la mer sera guérie<!--2 R. 2:21.-->.
47:9	Il arrivera que toute âme vivante qui grouille en tout endroit où arrivera le torrent vivra. Il arrivera qu’il y aura de très nombreux poissons, car cette eau arrivera là, et l’on sera guéri, et tout vivra où arrivera ce torrent.
47:10	Il arrivera que des pêcheurs se tiendront là. Depuis En-Guédi jusqu'à En-Églaïm, on étendra les filets. Il y aura des poissons de diverses espèces, comme les poissons de la grande mer, et ils seront très nombreux.
47:11	Ses marais et ses fosses ne seront pas guéris, ils seront livrés au sel.
47:12	Sur le torrent, sur ses bords, de ce côté-ci et de ce côté-là, pousseront toutes sortes d'arbres fruitiers. Leur feuillage ne se flétrira pas et leur fruit ne finira pas. Tous les mois, ils produiront leurs primeurs, car cette eau sort du sanctuaire. Leurs fruits seront pour la nourriture et leur feuille pour la guérison<!--Ap. 22:2.-->.

### Délimitations de la terre<!--Cp. Ge. 15:18-21.-->

47:13	Ainsi parle Adonaï YHWH : Voici les frontières de la terre que vous aurez en héritage, selon les douze tribus d'Israël. Yossef aura deux portions.
47:14	Vous en hériterez, chaque homme comme son frère. J'ai levé ma main de la donner à vos pères. Cette terre-là vous sera échue en héritage<!--Ge. 12:7, 17:8.-->.
47:15	Voici la frontière de la terre, du côté nord, depuis la grande mer, le chemin de Hethlon jusqu'à Tsedad,
47:16	Hamath, Bérotha, et Sibraïm, entre la frontière de Damas et la frontière de Hamath, Hatzer-Hatthicon, vers la frontière de Havran.
47:17	La frontière sera depuis la mer, Hatsar-Énon : la frontière de Damas, Tsaphon au nord et la frontière de Hamath. Ce sera le côté nord.
47:18	Le côté oriental sera le Yarden<!--Jourdain.--> entre Havran, Damas et Galaad, et la terre d'Israël. Vous mesurerez depuis la frontière nord jusqu'à la mer orientale. Ce sera le côté oriental.
47:19	Du côté du Néguev, vers le Théman : de Thamar jusqu'aux eaux de Meriybah à Qadesh, jusqu'au torrent vers la grande mer. Ce sera le côté méridional.
47:20	Le côté occidental sera la grande mer, depuis la frontière jusque vis-à-vis de Hamath. Ce sera le côté occidental.
47:21	Vous partagerez cette terre entre vous, selon les tribus d'Israël.
47:22	Il arrivera que vous le partagerez par le sort, en héritage, pour vous et pour les étrangers qui séjourneront au milieu de vous, qui engendreront des enfants au milieu de vous. Vous les regarderez comme les autochtones des fils d'Israël. Ils partageront au sort l'héritage avec vous parmi les tribus d'Israël.
47:23	Il arrivera que dans la tribu où l’étranger séjournera, là vous lui donnerez son héritage,– déclaration d'Adonaï YHWH.

## Chapitre 48

### Héritage (portions) de sept tribus<!--Cp. Jos. 13:1-19:51.-->

48:1	Voici les noms des tribus. Depuis la frontière du nord, vers la main du chemin de Hetlôn à l’entrée de Hamath, Hatsar-Eïnôn, la frontière de Damas, vers le nord, vers la main de Hamath, du côté oriental au côté occidental : Dan, une.
48:2	Sur la frontière de Dan, du côté oriental au côté occidental : Asher, une.
48:3	Sur la frontière d'Asher, du côté oriental au côté occidental : Nephthali, une.
48:4	Sur la frontière de Nephthali, du côté oriental au côté occidental : Menashè, une.
48:5	Sur la frontière de Menashè, du côté oriental au côté occidental : Éphraïm, une.
48:6	Sur la frontière d'Éphraïm, du côté oriental au côté occidental : Reouben, une.
48:7	Sur la frontière de Reouben, du côté oriental au côté occidental : Yéhouda, une.
48:8	Tout le long de la frontière de Yéhouda, du côté oriental au côté occidental, sera la contribution que vous prélèverez : elle aura 25 000 cannes de largeur et de longueur, comme l'une des portions du côté oriental au côté occidental. Le sanctuaire sera au milieu.
48:9	La contribution que vous prélèverez pour YHWH et qui sera offerte en offrande élevée, sera de 25 000 en longueur, et de 10 000 en largeur.

### Territoire réservé aux prêtres et aux Lévites

48:10	Cette contribution sainte sera pour les prêtres : 25 000 de longueur au nord, et 10 000 de largeur à l'occident, 10 000 de largeur à l'orient, et 25 000 de longueur au sud, et le sanctuaire de YHWH sera au milieu.
48:11	Elle sera pour les prêtres, et quiconque aura été sanctifié d'entre les fils de Tsadok, qui ont fait ce que j'ai ordonné, et qui ne se sont pas égarés quand les fils d'Israël se sont égarés, comme se sont égarés les autres Lévites.
48:12	Ils auront une portion prélevée sur la contribution de la terre, le saint des saints, à côté de la frontière des Lévites.
48:13	Les Lévites auront, parallèlement à la frontière des prêtres, 25 000 de longueur et 10 000 de largeur, 25 000 pour toute la longueur et 10 000 pour toute la largeur.
48:14	Ils n'en pourront ni vendre ni échanger, et la meilleure partie de la terre ne passera pas en d'autres mains, car elle est consacrée à YHWH.
48:15	Les 5 000 qui resteront de la largeur en face des 25 000 seront un espace profane pour la ville, pour les habitations et pour le faubourg, et la ville sera au milieu.
48:16	En voici les mesures : Du côté nord, 4 500, du côté sud, 4 500, du côté oriental, 4 500, et du côté occidental, 4 500.
48:17	La ville aura un faubourg de 250 au nord, de 250 au sud, de 250 à l'orient et de 250 à l'occident.
48:18	Quant à ce qui restera sur la longueur, parallèlement à la contribution sainte, soit 10 000 à l'orient, et 10 000 à l'occident, parallèlement à la contribution sainte, le revenu qu'on en tirera servira à nourrir ceux qui travailleront pour la ville.
48:19	Les travailleurs de la ville y travailleront, de toutes les tribus d’Israël.
48:20	Toute la contribution prélevée sera de 25 000 sur 25 000. Vous séparerez un quart de cette contribution sainte pour la propriété de la ville.
48:21	Le reste sera pour le prince aux deux côtés de la contribution sainte et de la propriété de la ville, des 25 000 de la contribution prélevée jusqu'à la frontière de l'orient, et à l'occident, des 25 000 coudées jusqu'à la frontière de l'occident, le long des parts, sera pour le prince, et la contribution sainte et le sanctuaire de la maison seront au milieu de toute la terre.
48:22	La propriété des Lévites et la propriété de la ville seront au milieu de ce qui appartiendra au prince. Entre la frontière de Yéhouda et la frontière de Benyamin, cela appartiendra au prince.

### Héritage de cinq tribus (les portions)

48:23	Le reste sera pour les autres tribus, du côté oriental au côté occidental : Benyamin, une.
48:24	Sur la frontière de Benyamin, du côté oriental au côté occidental : Shim’ôn, une.
48:25	Sur la frontière de Shim’ôn, du côté oriental au côté occidental : Yissakar, une.
48:26	Sur la frontière de Yissakar, du côté oriental au côté occidental : Zebouloun, une.
48:27	Sur la frontière de Zebouloun, du côté oriental au côté occidental : Gad, une.
48:28	Sur la frontière de Gad, du côté du Néguev, vers le Théman, la frontière ira depuis Thamar jusqu'aux eaux de Meriybah à Qadesh, jusqu'au torrent, vers la grande mer.
48:29	C'est là la terre que vous partagerez par le sort en héritage aux tribus d'Israël, et ce sont là leurs portions, – déclaration d'Adonaï YHWH.
48:30	Voici les sorties de la ville : Du côté du nord, il y aura 4 500 mesures.
48:31	Et les portes de la ville seront selon les noms des tribus d'Israël<!--Cp. Ap. 21:12-14.--> : 3 portes vers le nord, une porte pour Reouben, une porte pour Yéhouda, une porte pour Lévi.
48:32	Et du côté oriental, 4 500 mesures, et 3 portes : Une porte pour Yossef, une porte pour Benyamin, une porte pour Dan.
48:33	Du côté du midi, 4 500 mesures, et 3 portes : Une porte pour Shim’ôn, une porte pour Yissakar, une porte pour Zebouloun.
48:34	Du côté occidental, 4 500 mesures, avec leurs 3 portes : Une porte pour Gad, une porte pour Asher, une porte pour Nephthali.
48:35	Son circuit sera de 18 000 mesures. Le nom de la ville dès ce jour sera : YHWH-Shammah<!--« YHWH est ici ». YHWH-Shammah est un nom symbolique pour Yeroushalaim.-->.
